# Open Game Engine

Originally developed at https://sourceforge.net/projects/oge/ by [lazaruslong](http://sourceforge.net/u/lazaruslong/), [petrocket](http://sourceforge.net/u/petrocket/) and published under the LGPL-2.1 license.