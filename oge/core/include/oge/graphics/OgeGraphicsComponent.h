/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_GRAPHICSCOMPONENT_H__
#define __OGE_GRAPHICSCOMPONENT_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/object/OgeComponent.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"

namespace oge
{
    /**  
     * The 'GraphicsComponent' type component which is the base graphical 
     * component. It enables to make call such as:
     *      ((GraphicsComponent*) component)->render();
     *
     * @author Steven 'lazalong' Gay, Mehdi Toghianifar
     */
    class OGE_CORE_API GraphicsComponent : public Component
    {
    protected:
    // TODO TO REMOVE? useful for oged?    String mMeshName;

    public:
        virtual ~GraphicsComponent()
        {}

        /**
         * Implement Serialisable methods
         * @note Actually the serialisation methods read/write the whole object.
         * @todo Only the differences from the object template should be serialised.
         */
        virtual void write(Serialiser* serialiser);
        virtual void read(Serialiser* serialiser);
        virtual bool fixup();

        // ------- Specific method to the Graphics Family ----------
    //    virtual const String& getMeshName() const = 0;
        virtual void showBoundingBox(bool show) = 0;
        virtual Vector3 getDimensions() const = 0;

		// need these for graphical attachments like cameras
		virtual const Quaternion getOrientation() const { return Quaternion::IDENTITY; }
		virtual const Vector3 getPosition() const { return Vector3::ZERO; }

		virtual void setPosition(const Vector3 position) = 0;
		virtual void setOrientation(const Quaternion orientation) = 0;

    protected:
        inline GraphicsComponent(const ComponentType& type, const ComponentType& family)
            : Component(type, family)
        {}
    };
} // namespace
#endif
