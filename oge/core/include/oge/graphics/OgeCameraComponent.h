/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_CAMERACOMPONENT_H__
#define __OGE_CAMERACOMPONENT_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/object/OgeComponent.h"

namespace oge
{
    /**  
     * The 'CameraComponent' type component which is the base camera 
     * component.
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API CameraComponent : public Component
    {
    protected:
        ObjectId mOwner;

    public:
        /// CameraMode are used by specialised camera
        enum CameraMode
        {
            DEFAULT_MODE, // used if no parent object is set or no mode
            TOGGLE_MODE,  // used to toggle through the different modes
            FIRST_PERSON,
            THIRD_PERSON_CHASED,
            THIRD_PERSON_FIXED
        };

    public:
        virtual ~CameraComponent()
        {}

        /// (Re)set the default values
    // TODO REMOVE?...   virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
    //    virtual void _destroy( SceneManager* sceneManager );
    //    virtual void _update();

        /**
         * Implement Serialisable methods
         * @note Actually the serialisation methods read/write the whole object.
         * @todo Only the differences from the object template should be serialised.
         */
        virtual void write(Serialiser* serialiser);
        virtual void read(Serialiser* serialiser);
        virtual bool fixup();

        // ------- Specific method to the camera Family ----------

        virtual void showBoundingBox( bool show ) = 0;
        /// Return the library camera (for example Ogre::Camera*)
        virtual Any getCamera() = 0;

        // TODO: here or in LocationComp???...

        virtual Quaternion getOrientation() const = 0;
        virtual Vector3 getPosition() const = 0;
        virtual void setPosition( const Vector3& position ) = 0;
        virtual void setOrientation( const Quaternion& orientation ) = 0;

        virtual void moveCameraRelative(float x, float y, float z) = 0;
        /// 1 deg =~ 0.0175 rad
        virtual void yaw(float degree) = 0;
        virtual void pitch(float degree) = 0;
        virtual void roll(float degree) = 0;
        virtual void resetOrientation() = 0;

        /**
         * Set the camera mode
         * @see The ExtendedCameraComponent in the Ogre plugin
         */
        virtual void setCameraMode(int mode) {}

        /// Wip Set the owner of this camera TODO should we use a LinkComponent?
        void setOwner(const ObjectId& id) { mOwner = id; }

    protected:
        inline CameraComponent(const ComponentType& type, const ComponentType& family)
            : Component(type, family), mOwner("")
        {}
    };
} // namespace
#endif
