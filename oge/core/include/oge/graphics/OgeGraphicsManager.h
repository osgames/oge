/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_GRAPHICSMANAGER_H__
#define __OGE_GRAPHICSMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/system/OgeSystem.h"
#include "oge/graphics/OgeGraphicsSceneManager.h"

namespace oge
{
    /**
     * Graphics Manager Interface
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API GraphicsManager : public System
    {
    private:
        static GraphicsManager* mManager;

    protected:
        /// Active graphics SceneManager
        GraphicsSceneManager* mSceneManager;
        /// Contains the scene manager & their factories
        SceneManagerEnumerator* mSceneManagerEnumerator;

        /// Used mostly for debugging
        TextRenderer* mTextRenderer;

    public:
        GraphicsManager(const String& name, int systemInitOrdering = 10, int systemRunOrdering = 80);
        virtual ~GraphicsManager();

        /**
         * Returns the manager
         * @note The graphics manager is a singleton so this method is equivalent
         *       to getSingletonPtr() however the instantiation i.e. GraphicsManagerOGRE
         *       is also a singleton hence getManager() is to avoid name conflict.
         * @see getSingletonPtr() which returns the actual implementation
         */
        inline static GraphicsManager* getManager() { return mManager; }

        /// Returns the active scene manager
        inline GraphicsSceneManager* getActiveSceneManager() { return mSceneManager; }
        inline void setActiveSceneManager( GraphicsSceneManager* sm ) { assert(sm); mSceneManager = sm; }

        /**
         * Returns the component with id in the active scene manager, 0 otherwise.
         * @note returns a 'ptr* const' aka you can't change the pointer
         */
        inline Component* const getComponent(const ObjectId& id, const ComponentType& family)
        {
            return mSceneManager->getComponent( id, family );
        }

        // System methods
        // TODO: Move these to protected? so that they can only be accessed
        // through the System interface and not GraphicsManager
        virtual bool initialise() = 0;
        virtual void shutdown() = 0;
        virtual void tick(double currentTime) = 0;
		virtual void preTick(double currentTime);
		virtual void postTick(double currentTime);

        /// Creates a scene manager of type 'type' named 'name'
        SceneManager* createSceneManager( const String& name, const String& type );
        SceneManagerEnumerator* getSceneManagerEnumerator() { return mSceneManagerEnumerator; } 

        /**
         * Return the size of the render window as a vector
         * with x = width and y = height
         *
         * @note This method is necessary to set the ois mouse devices!
         */
        virtual Vector3 getRenderWindowSize() const = 0;
        virtual bool isFullscreen() const = 0;

        /**
         * Toggle the bounding box of the entities
         * If no id is passed all the object will be affected
         */
        void showBoundingBox(bool show, const ObjectId& object = "")
        {
            mSceneManager->showBoundingBox( show, object );
        }

        // ------------ Optional methods --------------------------
        /** 
         * Create a screenshot
         * @param filename Name of the resulting screenshot file. The extension 
         *     will determine the type of the generated file (png, bmp, jpg, ...)
         * @timestamped If true (default) the file will have its name timestamped
         *              in the format screenshot_MMDDYYY_HHMMSSmmm.xxx
         */
        virtual void saveScreenshot(const String& filename, bool timestamped = true) {}
        /// Toggle the debug overlay
        virtual void toggleDebugOverlay() {}


        /// Simple text output: mostly used for debugging
        void addTextBox( const String& id, const String& text,
            Real x, Real y, Real width, Real height);
        void removeTextBox(const String& id);
        void setText(const String& id, const String& text);
        
        /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        virtual const String getLibrariesVersion() { return "Graphics: No special library used.\n"; }

        /**
         * Return the heightfield
         *
         * @param filename
         * @param ymin Used to return the lowest y
         * @param ymax Used to return the highest y
         * @param size Used to return the grid size (must be (2^N) + 1)
         * @param step (default 1)
         * @param xz Used to parse the image either xz or zx. NOTE BULLET NEEDS xz = false !
         * @param heightMultiplier Multiply the height by a factor
         * @param group Used to tell in which ressource group the file is.
         */
        virtual float* getHeightfield(const String& filename, 
            float& ymin, float& ymax,  unsigned int& size,
            int step = 1, bool xz = false, float heightMultiplier = 8.0, 
            String group = "General") = 0;

        /**
         * Display debug shapes (mostly used for the physical plugin)
         * @see drawDebugLine() and drawDebugContactPoint() to add lines and contact points
         */
        virtual void setDebugLinesMode(bool mode) = 0;
        virtual void drawDebugLine(const Vector3& from, const Vector3& to, const Vector3& color) = 0;
        virtual void drawDebugContactPoint(const Vector3& point, const Vector3& normal,
            float distance, int lifeTime, const Vector3& color) = 0;
    };
}
#endif // __OGE_GRAPHICSMANAGER_H__
