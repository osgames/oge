/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_GRAPHICSSCENEMANAGER_H__
#define __OGE_GRAPHICSSCENEMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/containers/OgeMap.h"
#include "oge/scene/OgeSceneManager.h"
#include "oge/OgeAsyncValue.h"

namespace oge
{
    /**
     * Default Graphics SceneManager
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API GraphicsSceneManager : public SceneManager
    {
        friend class ObjectManager;

    protected:
        /// Active camera object
        ObjectId mCameraObject;

    public:
        GraphicsSceneManager(const String& name);
        virtual ~GraphicsSceneManager();

        // SceneManager methods
        virtual Vector3* doCameraRayQuery(Real x, Real y, const unsigned int queryMask = 0xFFFFFFFF) = 0;
        virtual String doObjectIdRayQuery(float x, float y, const unsigned int queryMask = 0xFFFFFFFF) = 0;
        /**
         * Raycast from a point in the scene to Ogre::Entities meshes! Not simply bouding boxes
         * On success the point is returned in the result.
         *
         * @note Pretty expensive: this method will even get the mesh information
         */
        virtual bool raycastFromPoint(const Vector3& from, const Vector3& normal,
            Vector3& result, unsigned long& target, float& closestDistance,
            const unsigned int queryMask = 0xFFFFFFFF) = 0;

        /**
         * Return true if collides with an entity
         */
        virtual bool collidesWithEntity(const Vector3& fromPoint, const Vector3& toPoint, 
            float padding = 3.5f, const unsigned int queryMask = 0xFFFFFFFF) = 0;

        /**
        * Set the active camera
        * Of course this means the object must contain a CameraComponent
        */
        virtual void setActiveCamera(const ObjectId& camera) = 0;
        inline const ObjectId& getActiveCamera() { return mCameraObject; }

        /**
         * Clamp the position passed as pointer to the terrain
         * Of course this method will do something only 
         * if there is a terrain below the point.
         *
         * @param aboveGround Delta above the terrain (used for jump for example)
         * @note Expensive method should be used with parcimony... 1/10 steps?
         */
        virtual void clampToTerrain(Vector3* position, float aboveGround = 0) = 0;
        /**
         * Asynchronous method to create the 'world' geometry.
         * Sets the source of the 'world' geometry, i.e. the large,
         * mainly static geometry making up the world e.g. rooms, landscape etc.
         * @returns An asynchronous bool - true in case of success
         */
        virtual AsyncBool setWorldGeometry(String filename) = 0;
        /**
         * Toggle the bounding box of the entities
         * If no id is passed all the objects in the current scene will be affected
         */
        void showBoundingBox(bool show, const ObjectId& = "");
        
    protected:
        bool processMessage(const Message& message);
    };

}
#endif // __OGE_GRAPHICSSCENEMANAGER_H__
