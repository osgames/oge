/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_GAMEMANAGER_H__
#define __OGE_GAMEMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/engine/OgeEngineSettings.h"
#include "oge/OgeSingleton.h"
#include "oge/system/OgeSystem.h"
#include "oge/containers/OgeMap.h"
#include "oge/game/OgeGameClock.h"

namespace oge
{
    /**
     * Manage the game states such as an introduction, an options panel,
     * an in-game state, a paused state, an endgame sequence, ...
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API GameManager :    public System, 
                                        public Singleton<GameManager>
    {
    private:
        typedef Map<std::map<String, GameState*>, String, GameState*> GameStateMap;
        static const String mDefaultGameStateName;

        // Games state
        GameStateMap  mGameStates;
        GameState*    mGameState; // current
        GameState*    mNextGameState;
        Any           mNextGameStateParam;

        /// The game clock
        GameClock*    mGameClock;

    public:
        // Singleton methods
        static GameManager* createSingleton();
        static void destroySingleton();
        static GameManager* getSingletonPtr();
        static GameManager& getSingleton();
        // System methods
        bool initialise();
        void shutdown();
		void postTick(double systemTime);
		void preTick(double systemTime);
        void tick(double systemTime);

        // Game state methods
        bool addGameState(GameState* gameState);
        void removeGameState(const String& name);
        void removeAllGameStates();
        /** Set the game state that will be 
         *  set current at the next systems tick
         */
        bool setNextGameState(const String& name, const Any& param = Any());
        String getCurrentGameStateName();
        GameState* getCurrentGameState();
        GameState* getGameState(const String& name);
        void swapState();

        inline double getGameTime() { return mGameClock->getTime(); }  

    private:
        GameManager();
        ~GameManager();
    };
}

#endif // __OGE_GAMEMANAGER_H__
