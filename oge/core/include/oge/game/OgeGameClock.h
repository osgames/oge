/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_GAMECLOCK_H__
#define __OGE_GAMECLOCK_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/OgeSingleton.h"

namespace oge
{
    /**
     * Game clock used to count the time since the start of the game
     * The game clock can be reset and adjusted by the server game time.
     *
     * The clock is create and destroyed in the GameManager instance.
     *
     * The clock can also be set from the server via two methods:
     * setServerTimeNow() which set the time immediately and
     * setServerTime() which will adjust the time over several updates.
     * The number of updates until the server & client are in sync is set
     * via setServerAdjustRate().
     *
     * @note If you _update() the clock right after you start the clock
     *       you might get a very big elapsed time because the last time 
     *       stored is 0. Hence you should at least call _update() once 
     *       before calling setStarted(true).
     *
     * @note Don't confuse with the engine system time.
     */
    class OGE_CORE_API GameClock : public Singleton<GameClock>
    {
    public:
        /// Clock mode to update game time
        enum ClockMode
        {
            OGE_CLOCK_REALTIME,
            OGE_CLOCK_CONSTANT,
            OGE_CLOCK_CLAMPED
        };

    private:
        bool         mStarted;
        ClockMode    mClockMode;

        double       mTime;
        double       mLastSystemTime;
        double       mTimeElapsed;
        /// Permits to change the flow of time (>1 accelerated speed, <1 slower time).
        double       mTimeSpeed;

        /// Time update send by a server. The local time is either set immediately or over several updates
        double       mServerTime;
        /// Nb of updates until the local time and server time are in sync (default 60)
        unsigned int mNbUpdatesToSyncOver;
        /// Nb of updates to sync
        unsigned int mNbUpdatesSync;

        /// The clock is update by constant step at each _update() call
        double       mConstantStep;
        /// default = 1/15 fps
        double       mClampedMinStep;
        /// default = 1/60 fps
        double       mClampedMaxStep;
        /// Total system time since the game was started, inclusive when the game is paused!
        double       mTotalSystemTimeElapsed;

    public:
        // singleton methods
        static GameClock* createSingleton();
        static void destroySingleton();
        static GameClock* getSingletonPtr();
        static GameClock& getSingleton();

        /// Permits to start and pause the clock
        void setStarted(bool start);
        void setModeRealtime();
        void setModeConstantStep(double step);
        void setModeClampedStep(double min=0.05, double max=0.01666f);
        void setModeClampedFps(double minFps=20.0f, double maxFps=60.0f);
        /// Permits to change the flow of time (>1 accelerated speed, <1 slower time).
        void setTimeSpeed(double speed);
        // NEXT Try if it is possible to use a method like: changeTimeSpeed( startSpeed, finishSpeed, duration )

        // TODO ? setGameClock( struct clockData )
        // TODO ? struct clockData getGameClock()

        void setTime(double gameTime);
        /// The local time will be synced smoothly over several updates
        void setServerTime(double serverTime);
        /// The local time will be synced immediately
        void setServerTimeNow(double serverTime);
        void setServerAdjustRate(unsigned int nbUpdates);

        inline double getTime() { return mTime; }
        /// Returns the elapsed time since the last update
        inline double getElapsedTime() { return mTimeElapsed; }

        double getClampedMaxStep();
        double getClampedMinStep();
        double getConstantStep();
        double getLastSystemTime();
        /// Returns the total system time since the game was started
        double getTotalSystemTimeElapsed();

        /**
         * @note This method is called by GameManager::tick() and
         *       shouldn't be called except if you want the game time
         *       to be updated.
         * @note The game time is updated with the engine system time!
         *
         * @todo Method too complex consider making several simplified clock versions
         */
        double _update(double systemTime);

    private:
        GameClock();
        virtual ~GameClock();
    };
}
#endif
