/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_ENGINESETTINGS_H__
#define __OGE_ENGINESETTINGS_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/system/OgeSystemSettings.h"
#include "oge/config/OgeOption.h"

namespace oge
{
    class OGE_CORE_API ThreadSettings
    {
    protected:
        String mName;
        OSThread::Priority mPriority;
    public:
        ThreadSettings();
        void parseFromOptions(OptionPtr rootOption);
        void setToDefault();
        inline const String& getName() const { return mName; }
        inline OSThread::Priority getPriority() const { return mPriority; }
    };

    class OGE_CORE_API ThreadingSettings
    {
    public:
        typedef std::vector<ThreadSettings> ThreadSettingsVector;
    protected:
        bool mAutoDetect;
        unsigned int mThreadCount;
        String mNamePrefix;
        String mNameSuffix;
        ThreadSettingsVector mThreads;
    public:
        ThreadingSettings();
        void parseFromOptions(OptionPtr rootOption);
        void setToDefault();
        inline bool getAutoDetect() const { return mAutoDetect; }
        inline unsigned int getThreadCount() const { return mThreadCount; }
        inline const String& getNamePrefix() const { return mNamePrefix; }
        inline const String& getNameSuffix() const { return mNameSuffix; }
        const ThreadSettingsVector& getThreadsSettings() const { return mThreads; }
    };

    class OGE_CORE_API EngineSettings
    {
    protected:
        String  mProfileName;
        String  mTimingHandlerName;
        String  mPluginsFolder;
        ThreadingSettings mThreadingSettings;
        SystemSettingsVector mSystemsSettings;
    public:
        EngineSettings();
        EngineSettings(const EngineSettings& settings);
        ~EngineSettings();
        void parseFromOptions(OptionPtr rootOption);
        void setToDefault();
        EngineSettings& operator = (const EngineSettings& settings);

        const String& getProfileName() const { return mProfileName; }
        const String& getTimingHandlerName() const { return mTimingHandlerName; }
        const String& getPluginsFolder() const { return mPluginsFolder; }
        const ThreadingSettings& getThreadingSettings() const
        {
            return mThreadingSettings;
        }
        const SystemSettingsVector& getSystemsSettings() const 
        {
            return mSystemsSettings; 
        }

        void setProfileName(String name) { mProfileName = name; }
        void setTimingHandlerName(const String& name) 
        {
            mTimingHandlerName = name; 
        }
        void setPluginsFolder(const String& folder) { mPluginsFolder = folder; }
        void setSystemsSettings(const SystemSettingsVector& settings)
        {
            mSystemsSettings = settings;
        }

        void addSystemsSettings(const SystemSettings& settings);
        void clearSystemsSettings();
    protected:
        void copyFrom(const EngineSettings& settings);
    };

}

#endif // __OGE_ENGINESETTINGS_H__
