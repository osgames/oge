/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_ENGINEMANAGER_H__
#define __OGE_ENGINEMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/engine/OgeEngineSettings.h"
#include "oge/containers/OgeMap.h"
#include "oge/OgeSingleton.h"
#include "oge/gui/OgeGuiManager.h"

namespace oge
{
    class OGE_CORE_API EngineManager : public Singleton<EngineManager>
    {
    public:
        typedef Map<std::map<String, String>, String, String> DependencyInfoMap;
        typedef std::vector<TaskThread*> ThreadVector;
    private:
        typedef Map<std::map<String, DependencyInfoMap>, 
                    String, DependencyInfoMap> DependencyMap;

        typedef Map<std::map<String, EngineTimingHandlerFactory*>, 
                    String, EngineTimingHandlerFactory*> TimingHandlerFactoryMap;
    private:
        bool mInitialised;
        DependencyMap mDependencies;
        Mutex mDependenciesMutex;
        ThreadVector mThreads;
        TimingHandlerFactoryMap mTimingHandlerFactories;
        EngineSettings mEngineSettings;
        EngineTimingHandler* mTimingHandler;
        EngineListener* mListener;
        SystemManager* mSystemManager;
        
        /**
         * Default managers when a plugin is not present
         * those managers have basic features
         */
        GuiManager*     mDefaultGuiManager;
        PhysicsManager* mDefaultPhysicsManager;
        ScriptManager*  mDefaultScriptManager;

        /// Used to pass an external window handle before initialisation (cf. OGEd)
        oge::String mExternalWindowHandle;

    public:
        bool initialise(const String configFile, const String& profile, 
            EngineListener* listener = 0);
        void shutdown(); 
        bool run();
        void stop();

		/**
		 * Get the current time from our timing handler.
		 * This returns the current tick time unless you set the update
		 * parameter to true, in which case it will return the actual time.
		 * @param update Whether to update the timer to the actual time.
		 * @return the current engine time
		 */
		double getCurrentTime(bool update = false);

        bool registerTimingHandlerFactory(const String& name, 
            EngineTimingHandlerFactory* factory);
        void unregisterTimingHandlerFactory(const String& name);
        /** Returns the engine timing handler
         * @note Mostly used by editors (like OGEd) to update during paint() calls
         */
        EngineTimingHandler* const getEngineTimingHandler() { return mTimingHandler; }

        bool registerDependency(const DependencyInfoMap& dependencyInfo);

        const EngineSettings& getSettings() const { return mEngineSettings; }

        static EngineManager* createSingleton();
        static void destroySingleton();
        static EngineManager* getSingletonPtr();
        static EngineManager& getSingleton();
        
        /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        const String getLibrariesVersion();
        /**
         *  Return the OGE version
         *  in the form "OGE - Brigit 0.2.12 - debug version 512"
         */
        const String getOgeVersion();

        bool isInitialised() { return mInitialised; }

        /// Used to pass an external window handle before initialisation (cf. OGEd)
        void setExternalWindowHandle(oge::String handle) { mExternalWindowHandle = handle; }
        oge::String getExternalWindowHandle() { return mExternalWindowHandle; }

    private:
        EngineManager();
        ~EngineManager();

        void registerCoreDependencies();
        void createBuiltInFactories();
        void destroyBuiltInFactories();
        void registerBuiltInManagers();
        void unregisterBuiltInManagers();
        void loadPlugins();
        /**
         * Initialise the main managers.
         *
         * If one is not found -aka no plugin- a default is created.
         * @note The default manager can be a dummy one
         *       Such as a GUI manager which does nothing but is
         *       necessary for class like the InputEventHandler
         */
        void initialiseManagers();
        /// This method start all the 'system group' threads.
        void setupThreads();
        void shutdownThreads();
        bool setupTimingHandler();
        void destroyTimingHandler();
        void deleteDefaultManagers();
    };
}

#endif // __OGE_ENGINEMANAGER_H__
