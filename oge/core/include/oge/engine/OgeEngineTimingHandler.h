/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_ENGINETIMINGHANDLER_H__
#define __OGE_ENGINETIMINGHANDLER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/system/OgeSystemGroup.h"
#include "oge/OgeClock.h"

namespace oge
{
    class OGE_CORE_API EngineTimingHandler
    {
    protected:
        EngineTimingHandlerFactory* mCreator;
        const EngineSettings* mEngineSettings;
    public:
        EngineTimingHandler(EngineTimingHandlerFactory* creator);
        virtual ~EngineTimingHandler() {}
        EngineTimingHandlerFactory* getCreator() const { return mCreator; }
		virtual double getCurrentTime(bool update = false) { return 0.0; }
        virtual void run() = 0;
        virtual void stop() = 0;
        virtual bool setSystemGroups(
            const std::vector<SystemGroup*>& groups) = 0;
    protected:
        virtual void notifyAllGroups() = 0;
    };

    class OGE_CORE_API EngineTimingHandlerFactory
    {
    public:
        virtual EngineTimingHandler* createHandler() = 0;
        virtual void destroyHandler(EngineTimingHandler* handler) = 0;
    };

    class OGE_CORE_API SimpleEngineTimingHandler : public EngineTimingHandler
    {
    public:
        typedef std::vector<SystemGroup*> SystemGroupVector;
    protected:
        bool mRunning;
        SystemGroupVector mGroups;
        Clock mClock;
    public:
        SimpleEngineTimingHandler(EngineTimingHandlerFactory* creator);
        ~SimpleEngineTimingHandler();
		double getCurrentTime(bool update = false) { return update ? mClock.updateAndGetTime() : mClock.getTime(); }
        void run();
        void stop();
        bool setSystemGroups(const std::vector<SystemGroup*>& groups);
    protected:
        void notifyAllGroups();
    };

    class OGE_CORE_API SimpleEngineTimingHandlerFactory : public EngineTimingHandlerFactory
    {
    public:
        EngineTimingHandler* createHandler() 
        { 
            return new SimpleEngineTimingHandler(this);
        }
        void destroyHandler(EngineTimingHandler* handler)
        {
            if (handler)
            {
                delete handler;
            }
        }
    };

    /**
    * WIP OGEd timing handler - permits the wx paint() to update
    */
    class OGE_CORE_API OGEdEngineTimingHandler : public EngineTimingHandler
    {
    public:
        typedef std::vector<SystemGroup*> SystemGroupVector;
    protected:
        bool mRunning;
        SystemGroupVector mGroups;
        Clock mClock;
        double mNextTime;

    public:
        OGEdEngineTimingHandler(EngineTimingHandlerFactory* creator);
        ~OGEdEngineTimingHandler();
		double getCurrentTime(bool update = false) { return update? mClock.updateAndGetTime() : mClock.getTime(); }
        void run();
        void stop();
        bool setSystemGroups(const std::vector<SystemGroup*>& groups);
        /// Called by the editor during the paint() call
        void update();

    protected:
        void notifyAllGroups();
    };

    class OGE_CORE_API OGEdEngineTimingHandlerFactory : public EngineTimingHandlerFactory
    {
    public:
        EngineTimingHandler* createHandler() 
        { 
            return new OGEdEngineTimingHandler(this);
        }
        void destroyHandler(EngineTimingHandler* handler)
        {
            if (handler)
            {
                delete handler;
            }
        }
    };
}

#endif // __OGE_ENGINETIMINGHANDLER_H__
