/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_INPUTMANAGER_H__
#define __OGE_INPUTMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/system/OgeSystem.h"
#include "oge/containers/OgeMap.h"
#include "oge/input/OgeInputDevice.h"

namespace oge
{
    /**
     * Input Manager Interface
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API InputManager : public System
    {
    public:
        typedef Map<OgeHashMap<String, InputDevice*>, 
            String, InputDevice*> DeviceMap;
    private:
        static InputManager* mManager;

    protected:       
        DeviceMap mInputDevices;

        // ---------- Input specific ---------------------
        /// Window handle created by the graphics manager.
        size_t mWindowHandle; 
        bool mIsFullscreen;

    public:
        InputManager(const String& name, int systemInitOrdering = 100, int systemRunOrdering = 10);
        virtual ~InputManager();
        /**
         * Returns the manager
         * @note The graphics manager is a singleton so this method is equivalent
         *       to getSingletonPtr() however the instantiation i.e. GraphicsManagerOGRE
         *       is also a singleton hence getManager() is to avoid name conflict.
         * @see getSingletonPtr() which returns the actual implementation
         */
        inline static InputManager* getManager() { return mManager; }

        // ---------- System methods ---------------------
        virtual bool initialise() = 0;
        virtual void shutdown() = 0;
        virtual void tick(double currentTime) = 0;
		virtual void preTick(double currentTime) {}
		virtual void postTick(double currentTime) {}

        // ---------- Input specific ---------------------
        /**
         * Virtual method that must be implemented to
         * creates an input device of the specified type (keyboard, mouse, etc).
         *
         * @param Name must be unique.
         * @param DeviceType enumeration and adding it to the device list. 
         */
        virtual InputDevice* createInputDevice(const String& name, DeviceType devType) = 0;

        /** Called by RenderTargetManager::windowResized()
         */
        virtual void windowResized(unsigned int width, unsigned int height) = 0;

        /**
         * @note The window handle must be set before the OIS::InputManager
         *       can be created. A handler must be provided.
         * @note This method will assert if a null pointer is given.
         *       Use clearWindowHandle() instead with cautious.
         */
        void setWindowHandle( size_t windowHnd )
        {
            assert(windowHnd);
            mWindowHandle = windowHnd;
        }
        void clearWindowHandle()
        {
            mWindowHandle = 0;
        }
        void setFullscreen( bool fullscreen )
        {
            mIsFullscreen = fullscreen;
        }
        /**
         * Adds an already created input device to the list.
         */
        InputDevice* addInputDevice(InputDevice *device);
        /**
         * Removes an input device from the list, making it not possible 
         * to receive input. The device can be re-attached using 
         * the addInputDevice method, or the device interface attach method.
         */
        void removeDevice(const String& name);
        /**
         * Delete an input device.
         */
        void deleteDevice(const String& name);
        /**
         * Returns the pointer to a valid, created input device. 
         * Type casting will be needed to the correct input type. 
         * 0 is returned if device cannot be found.
         */
        InputDevice* getDevice(const String& name);
        /**
         * Returns the pointer to the first keyboard found in the list.
         * @note You can should static_cast<> it to the appropriate
         *       Device supported by your Input plugin such as
         *       KeyboardDevice, MouseDevice, JoystickDevice, ...
         */
        InputDevice* getFirstKeyboard();
        /**
         * Returns the pointer to the first mouse found in the list.
         * @note You can should static_cast<> it to the appropriate
         *       Device supported by your Input plugin such as 
         *       KeyboardDevice, MouseDevice, JoystickDevice, ...
         */
        InputDevice* getFirstMouse();
        /**
         * Returns the pointer to the first joystick found in the list.
         * @note You can should static_cast<> it to the appropriate
         *       Device supported by your Input plugin  such as
         *       KeyboardDevice, MouseDevice, JoystickDevice, ...
         */
        InputDevice* getFirstJoystick();
        /**
         * Returns the pointer of the nieme device of a certain 
         * type found in the list. 0 if not.
         */
        InputDevice* getDeviceOfType(DeviceType type, unsigned int nb = 1);

        /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        virtual const String getLibrariesVersion() { return "Input: No special library used.\n"; }
    };
}
#endif // __OGE_INPUTMANAGER_H__
