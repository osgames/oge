/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_INPUTEVENTHANDLER_H__
#define __OGE_INPUTEVENTHANDLER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/input/OgeInputDevice.h"
#include "oge/input/OgeInputEvent.h"

#include "oge/gui/OgeGuiManager.h"

namespace oge
{
    /**
     * The implementation of the IEH (InputEventHandler) is used to react 
     * to input event. Each game will determine how to react
     * to those input events (key, mouse, etc).
     * The ActionMap is a specialised IEH permitting to associate events to actions
     * 
     * @see The DummyInputEventHandler for some commented code showing how to implement it.
     * @see The class ActionMap
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API InputEventHandler
    {
    public:
        InputEventHandler() {}
        virtual ~InputEventHandler() {}

        // Oge event-base buffered methods
        //
        // WIP: Before the gui code the method were pure virtual ( = 0; )
        virtual void keyPressed (const KeyEvent& arg)             { GuiManager::getManager()->keyPressed( arg ); }
        virtual void keyReleased (const KeyEvent& arg)            { GuiManager::getManager()->keyReleased( arg ); }
        virtual void mousePressed (const MouseEvent& arg)         { GuiManager::getManager()->mousePressed( arg ); }
        virtual void mouseReleased (const MouseEvent& arg)        { GuiManager::getManager()->mouseReleased( arg ); }
        virtual void mouseMoved (const MouseEvent& arg)           { GuiManager::getManager()->mouseMoved( arg ); }
        virtual void joyButtonPressed (const JoystickEvent& arg)  { GuiManager::getManager()->joyButtonPressed( arg ); }
        virtual void joyButtonReleased (const JoystickEvent& arg) { GuiManager::getManager()->joyButtonReleased( arg ); }
        virtual void joyAxisMoved (const JoystickEvent& arg)      { GuiManager::getManager()->joyAxisMoved( arg ); }
        virtual void joyPovMoved (const JoystickEvent& arg)       { GuiManager::getManager()->joyPovMoved( arg ); }

        // Any-based buffered methods 
        // Those methods should be implemented and used only if you want 
        // to have direct access to the event of the underlying input library
        //
        // WIP: Before the gui code the method were only {}
        //virtual bool keyPressedAny (Any arg) { const OIS::KeyEvent* evt = FastAnyCast(const OIS::KeyEvent*, arg);
        //    GuiManager::getManager()->keyPressed( arg );}
        virtual void keyPressedAny (Any arg) {}
        virtual void keyReleasedAny (Any arg) {}
        virtual void mousePressedAny (Any arg, const int mousebutton) {}
        virtual void mouseReleasedAny (Any arg, const int mousebutton) {}
        virtual void mouseMovedAny (Any arg) {}
        virtual void joyButtonPressedAny (Any arg, int button) {}
        virtual void joyButtonReleasedAny (Any arg, int button) {}
        virtual void joyAxisMovedAny (Any arg, int axis) {}
        virtual void joyPovMovedAny (Any arg, int pov) {}
    };

    /**
     * Dummy implementation of the InputEventHandler
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API DummyInputEventHandler : public InputEventHandler
    {
    public:
        DummyInputEventHandler();

        // Oge event-base buffered methods
        void keyPressed (const KeyEvent& arg);
        void keyReleased (const KeyEvent& arg);
        void mousePressed (const MouseEvent& arg);
        void mouseReleased (const MouseEvent& arg);
        void mouseMoved (const MouseEvent& arg);
        void joyButtonPressed (const JoystickEvent& arg);
        void joyButtonReleased (const JoystickEvent& arg);
        void joyAxisMoved (const JoystickEvent& arg);
        void joyPovMoved (const JoystickEvent& arg);

        void keyPressedAny (Any arg);
        void keyReleasedAny (Any arg);
        void mousePressedAny (Any arg, const int mousebutton);
        void mouseReleasedAny (Any arg, const int mousebutton);
        void mouseMovedAny (Any arg);
        void joyButtonPressedAny (Any arg, int button);
        void joyButtonReleasedAny (Any arg, int button);
        void joyAxisMovedAny (Any arg, int axis);
        void joyPovMovedAny (Any arg, int pov);
    };
}
#endif
