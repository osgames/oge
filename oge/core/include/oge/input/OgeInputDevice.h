/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_INPUTDEVICE_H__
#define __OGE_INPUTDEVICE_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/input/OgeInputState.h"

namespace oge
{
    /// @todo add other ois type
    enum DeviceType
    {
        OGE_INPUT_NONE = 0,
        OGE_INPUT_KEYBOARD = 2,
        OGE_INPUT_MOUSE = 4,
        OGE_INPUT_JOYSTICK = 8,
        OGE_INPUT_KEYBOARD_ANY = 16,
        OGE_INPUT_MOUSE_ANY = 32,
        OGE_INPUT_JOYSTICK_ANY = 64,
        OGE_INPUT_ALL = OGE_INPUT_KEYBOARD | OGE_INPUT_MOUSE | OGE_INPUT_JOYSTICK,
        OGE_INPUT_ALL_ANY = OGE_INPUT_KEYBOARD_ANY | OGE_INPUT_MOUSE_ANY | OGE_INPUT_JOYSTICK_ANY
    };


    /**
     * Interface for the input devices (keyboard, mouse, joystick, ...)
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API InputDevice
    {
    protected:
        DeviceType mDevType;
        String mDeviceName;
        bool mCanUpdate;
        InputEventHandler* mEventHandler;

    public:
        virtual ~InputDevice();
        /**
         * Returns the device name.
         */
        String& getName();
        /**
         * Updates device data. To be overriden by device implementations.
         */
        virtual void update() = 0;
        /**
         * Dettaches the input device from the input manager's list. 
         * Dettached devices will not receive input feedback and will
         * not update their status.
         */
        void dettach();
        /**
         * Attach the input device to the input manager list.
         */
        void attach();
        /**
         * Returns the device type. Device type must be set exclusively 
         * in each device constructor.
         */
        DeviceType getDeviceType() const;
        /**
         * Sets the device update status.
         */
        void setDeviceUpdate(bool enable);
        /**
         * Returns the device update status.
         */
        bool getDeviceUpdate() const;
        /**
         * Sets the input event handler instance for buffered mode devices.
         */
        void setInputEventHandler(InputEventHandler* eventHandler);
        /**
         * Get the state information of the device
         */
        virtual const InputState* getState() = 0;

    protected:
        InputDevice(DeviceType type, const String& name);
    };

}
#endif // __OGE_INPUTDEVICE_H__
