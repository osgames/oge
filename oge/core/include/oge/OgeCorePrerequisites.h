/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_COREPREREQUISITES_H__
#define __OGE_COREPREREQUISITES_H__

#include "oge/OgeUtilitiesPrerequisites.h"

// Dynamic library import/export macro
#ifndef OGE_CORE_API
#    if OGE_PLATFORM == OGE_PLATFORM_WIN32
#        ifdef OGE_CORE_EXPORTS
#            define OGE_CORE_API __declspec(dllexport)
#        else
#            if !defined(__MINGW32__) && !defined(__CYGWIN__)
#                define OGE_CORE_API __declspec(dllimport)
#            else
#                define OGE_CORE_API
#            endif
#        endif
#    elif OGE_PLATFORM == OGE_PLATFORM_LINUX
#      define OGE_CORE_API __attribute__ ((visibility("default")))
#    else
#        define OGE_CORE_API
#    endif
#endif

#include "oge/OgeCoreConfig.h"

namespace oge
{
    // Forward Declaration of OGE Core Classes
    class AnimationComponent;
    class AnimationComponentTemplate;
    class Audio;
    class AudioManager;
    class ActionMap;
    class CameraComponent;
    class CameraComponentTemplate;
    class Component;
    class ComponentTemplate;
    class DefaultInputEventHandler;
    class EngineListener;
    class EngineManager;
    class EngineSettings;
    class EngineTimingHandler;
    class EngineTimingHandlerFactory;
    class Game;
    class GameClock;
    class GameState;
    class GameManager;
    class GraphicsComponent;
    class GraphicsComponentTemplate;
    class GraphicsManager;
    class GraphicsSceneManager;
    class GuiManager;
    class InputDevice;
    class InputEventHandler;
    class InputManager;
    class LinkComponent;
    class LinkComponentTemplate;
    class ObjectFactory;
    class ObjectTemplate;
    class ObjectManager;
    class ObjectSceneManager;
    class ObjectSceneManagerFactory;
    class PhysicsComponent;
    class PhysicsSceneManager;
    class PhysicsManager;
	class NetworkComponent;
    class NetworkManager;
	class NetworkSceneManager;
    class ScriptManager;
    class ScriptSceneManager;
    class SceneManager;
    class SceneManagerEnumerator; // To avoid SceneManagerManager
    class SceneManagerFactory;
    struct SceneManagerMetaData;
    class State;
    class StateComponent;
    class System;
    class SystemController;
    class SystemListener;
    class SystemGroup;
    class SystemGroupFactory;
    class SystemManager;
    class TextRenderer;

} // namespace oge


// Widely-used headers
#include "oge/logging/OgeLogManager.h"
#include "oge/OgeString.h"

namespace oge
{
    // Declaring custom types
    /**
     * Those types are for unique id to tag each object & component template.
     * The type "Object" and "Component" should not be used - it is used for
     * the base template (an empty one)
     * It could be replaced i.e. a long and use a converter to attribute
     * an unique type for each string in parameter files.
     * Or something like it if binary parameter files are used.
     */
    typedef String ObjectType;
    typedef String ComponentType;
    typedef String ObjectId;

} // namespace oge

#endif // __OGE_COREPREQUISITES_H__
