/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_SYSTEMGROUPFAST_H__
#define __OGE_SYSTEMGROUPFAST_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/system/OgeSystemGroup.h"
#include "oge/OgeClock.h"

namespace oge
{
    /**
     * Implementation of SystemGroup that provides a "fast" (less accurate)
     * approach to ticking systems
     *
     * @see     SystemGroup
     * @see     System
     * @author  Christopher Jones
     */
    class OGE_CORE_API SystemGroupFast : public SystemGroup
    {
        /// A tick task is already in waiting: no need to tick again yet.
        bool mAlreadyTicked;

    protected:
        struct SystemInfo
        {
            SystemInfo();
            System* system;
            double nextTickTime;
            double tickInterval;
			double curProcessTime;
			double lastProcessTime;
			double avgProcessTime;
        };
        typedef std::vector<SystemInfo*> SystemInfoVector;
    protected:
        SystemInfoVector mScheduledSystems;
        SystemInfoVector mRegisteredSystems;
		Clock mClock;
    public:
        SystemGroupFast(const String& name = "", SystemGroupFactory* creator = 0);
        ~SystemGroupFast();
        AsyncBool requestSetTickInterval(System* system, double tickInterval);
        void tick(double currentTime);
		/**
		 * Get the tick interval for a system in this group
		 * @param system The system to look for
		 * @return The tick interval
		 */
		double getTickInterval(System* system);

		/**
		 * Get the system stats for a system in this group
		 * @param system The system to look for
		 * @return The system info
		 */
        SystemStats getSystemStats(System* system);

        void notifyCurrentTime(double initialTime);
    protected:
        void addRegisteredSystem(System* system, double tickInterval);
        void removeRegisteredSystem(System* system);
        void addScheduledSystem(System* system);
        void removeScheduledSystem(System* system);
        bool isSystemRegistered(System* system) const;
		SystemInfo* getSystemInfo(System* system);
        void tickAll(double currentTime);
        void notifyDettachedFromThread();
        void updateSystemsCurrentTime(double currentTime);
        bool setTickInterval(System* system, double tickInterval);
    };

    class OGE_CORE_API SystemGroupFastFactory : public SystemGroupFactory
    {
    public:
        SystemGroup* createSystemGroup(const String& name) 
        {
            return new SystemGroupFast(name, this); 
        }
        void destroySystemGroup(SystemGroup* group)
        {
            if (group)
            {
                delete group;
            }
        }
    };
}

#endif // __OGE_SYSTEMGROUPFAST_H__
