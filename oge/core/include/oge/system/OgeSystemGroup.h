/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_SYSTEMGROUP_H__
#define __OGE_SYSTEMGROUP_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/thread/OgeTaskThread.h"
#include "oge/OgeAsyncValue.h"

namespace oge
{
    /**
     * Manages 0 or more System's, providing each with an enforced start up
     * and shutdown sequence as well as periodical updates (ticking).
     *
     * This is a partial implementation, where subclasses provides the 
     * implementation for storing System's as well as ticking them.
     *
     * This class can be attached to a single thread where all of its
     * operations are performed. This guarantees the thread-safety for
     * this class as well as thread-safety for the start up, shutdown sequences 
     * and ticking of Systems.
     *
     * The sequence for systems is:
     *  Registration:   Register a System to a SystemGroup
     *  Initialisation: Initialises the System
     *  Scheduling:     The system will be ticked/updated periodically
     *
     * The opposite sequence is used for tearing it all down; unschedules the
     * System, shuts the System down and then unregister.
     *
     * This sequence is managed and doesn't have be handled manually, e.g.
     * A system can be scheduled whilst it is not even registered, the
     * SystemGroup will register it, initialise then schedule it in order.
     * This also goes for tearing systems down, by unregistering a system it
     * will be unscheduled and shutdown before being unregistered.
     *
     * @see     System
     * @author  Christopher Jones
     */
    class OGE_CORE_API SystemGroup : public TaskThread::Listener
    {
	public:
        struct SystemStats
        {
			double lastProcessTime;
			double avgProcessTime;
        };
    protected:
        double mCurrentTime;
        SystemGroupFactory* mCreator;
    public:
        SystemGroup(const String& name = "", SystemGroupFactory* creator = 0);
        virtual ~SystemGroup();
        /**
         * This creates a request to tick all scheduled systems.
         *
         * This should only be called by the EngineTimingHandler
         */
        virtual void tick(double currentTime) = 0;
        /**
         * Creates a request to register the system to this group
         *
         * This will only succeed if the system is not already registered
         * to a system group
         */
        virtual AsyncBool requestRegisterSystem(System* system, 
            double tickInterval = 20);
        /**
         * Creates a request to initialise the system
         *
         * If the system is registered to another group then this will fail.
         * If the system is not registered to any group it will be registered
         * to this group first.
         */
        virtual AsyncBool requestInitialiseSystem(System* system);
        /**
         * Creates a request to schedule the system (so it will be ticked).
         *
         * If the system is registered to another group then this will fail.
         * If the system is not registered to any group it will be registered
         * to this group first. If it is not already initialised, it will be
         * initialised.
         */
        virtual AsyncBool requestScheduleSystem(System* system);
        /**
         * Creates a request to unschedule the system
         *
         * If the system is registered to another group then this will fail.
         * This will only work if the system is already scheduled, otherwise
         * it will fail
         */
        virtual AsyncBool requestUnscheduleSystem(System* system);
        /**
         * Creates a request to shutdown the system
         *
         * If the system is registered to another group then this will fail.
         * If the system is scheduled then it will be unscheduled before being
         * shutdown.
         */
        virtual AsyncBool requestShutdownSystem(System* system);
        /**
         * Creates a request to unregister the system
         *
         * If the system is registered to another group then this will fail.
         * If the system is scheduled, it will be unscheduled, if the system
         * is initialised, it will be shutdown, then it will be unregistered.
         */
        virtual AsyncBool requestUnregisterSystem(System* system);
        /**
         * Creates a request to set the tick interval for the system
         *
         * If the system is not registered to this group, it will fail
         */
        virtual AsyncBool requestSetTickInterval(System* system, 
            double tickInterval) = 0;
        /// Get the factory that created this group, if any.
        SystemGroupFactory* getCreator() const { return mCreator; }

		/**
		 * Get the tick interval for a system in this group
		 * @param system The system to look for
		 * @return The tick interval
		 */
		virtual double getTickInterval(System* system) = 0;

		/**
		 * Get the system stats for a system in this group
		 * @param system The system to look for
		 * @return The system info
		 */
        virtual SystemStats getSystemStats(System* system) = 0;

        /**
         * Tell the group the current time. This should only be called by the
         * EngineTimingHandler
         */
        virtual void notifyCurrentTime(double initialTime) = 0;
    protected:
        /// Implementation for registering a system
        bool registerSystem(System* system, double tickInterval = 20);
        /// Implementation for Initialising a system
        bool initialiseSystem(System* system);
        /// Implementation for scheduling a system
        bool scheduleSystem(System* system);
        /// Implementation for unscheduling a system
        bool unscheduleSystem(System* system);
        /// Implementation for shutting down a system
        bool shutdownSystem(System* system);
        /// Implementation for unregistered a system
        bool unregisterSystem(System* system);
        
        /**
         * Tells the subclass about the new system that has been registered
         *
         * This allows the subclass to store the system and any extra data
         * in any way it wants to
         */
        virtual void addRegisteredSystem(System* system, double tickInterval) = 0;
        /**
         * Tells the subclass that the system has been unregistered
         *
         * The subclass should remove the system and the related data it stored
         * when it was registered
         */
        virtual void removeRegisteredSystem(System* system) = 0;
        /**
         * Tells the subclass that the system has been scheduled
         *
         * The subclass can store this how it wants to so that it can
         * handle ticking the system
         */
        virtual void addScheduledSystem(System* system) = 0;
        /**
         * Tells the subclass that the system has been unscheduled
         *
         * The subclass can remove this system from however it stores the
         * scheduled systems
         */
        virtual void removeScheduledSystem(System* system) = 0;
        /**
         * Asks the subclass if the given system is already registered with
         * this group.
         */
        virtual bool isSystemRegistered(System* system) const = 0;
    };

    class OGE_CORE_API SystemGroupFactory
    {
    public:
        virtual SystemGroup* createSystemGroup(const String& name) = 0;
        virtual void destroySystemGroup(SystemGroup* group) = 0;
    };

}

#endif // __OGE_SYSTEMGROUP_H__
