/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_SYSTEM_H__
#define __OGE_SYSTEM_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/OgeNonCopyable.h"
#include "oge/OgeAsyncValue.h"

namespace oge
{
    /**
     * Base class used with a SystemGroup to provide periodical updates and
     * a strict set up and shutdown sequence
     *
     * This class allows a subclass to register itself to a SystemGroup which 
     * handles the start up and shutdown sequences, as well as the periodical 
     * updates (ticking).
     *
     * As a SystemGroup is attached to only 1 thread, all ticking, start up
     * and shutdown sequences are called from only that thread.
     * 
     * @see     SystemGroup
     * @author  Christopher Jones
     */
    class OGE_CORE_API System : public NonCopyable
    {
    public:
        enum State
        {
            CREATED = 0,
            REGISTERED,
            INITIALISED,
            SCHEDULED
        };
        static String state2string(State state) {
            switch (state)
            {
            case CREATED:
                return String("CREATED");
            case REGISTERED:
                return String("REGISTERED");
            case INITIALISED:
                return String("INITIALISED");
            case SCHEDULED:
                return String("SCHEDULED");
            default:
                return String("UNKNOWN");
            }
        }
        enum Operation
        {
            NONE = 0,
            REGISTER,
            INITIALISE,
            SCHEDULE,
            UNSCHEDULE,
            SHUTDOWN,
            UNREGISTER
        };
        static String operation2string(Operation operation) {
            switch (operation)
            {
            case NONE:
                return String("NONE");
            case REGISTER:
                return String("REGISTER");
            case INITIALISE:
                return String("INITIALISE");
            case SCHEDULE:
                return String("SCHEDULE");
            case UNSCHEDULE:
                return String("UNSCHEDULE");
            case SHUTDOWN:
                return String("SHUTDOWN");
            case UNREGISTER:
                return String("UNREGISTER");
            default:
                return String("UNKNOWN");
            }
        }
        class Listener
        {
        public:
            virtual void notifyRegistered() {}
            virtual void notifyInitialised() {}
            virtual void notifyScheduled(double systemTime) {}
            virtual void notifyUnscheduled() {}
            virtual void notifyShutdown() {}
            virtual void notifyUnregistered() {}
        };
    protected:
		double mLastTickTime;
        State mState;
        SystemGroup* mSystemGroup;
        Listener* mSystemListener;
        mutable SpinRWMutex mStateAndGroupMutex;
        String mName;
        int mSystemInitOrdering;
		int mSystemRunOrdering;
/*
#ifdef __PROFILE
        struct ProfileInfo
        {
            System* system;
			//Vector3 position;
			double time;
        };
        typedef std::vector<ProfileInfo*> ProfileInfoVector;
		ProfileInfoVector mProfileInfo;
#endif
		*/
    public:
        System(const String& name, int systemInitOrdering = 0, int systemRunOrdering = 0);
        virtual ~System();
        /// Calls SystemManager::registerSystemToGroup()
        AsyncBool requestRegister(double tickInterval = 20);
        /// Calls SystemManager::controlSystem
        AsyncBool requestInitialise();
        /// Calls SystemManager::controlSystem
        AsyncBool requestSchedule();
        /// Calls SystemManager::controlSystem
        AsyncBool requestUnschedule();
        /// Calls SystemManager::controlSystem
        AsyncBool requestShutdown();
        /// Calls SystemManager::controlSystem
        AsyncBool requestUnregister();
        /// This can only be called once the system is registered to a group
        AsyncBool requestSetTickInterval(double tickInterval);
        /**
         * Initialises the System.
         *
         * Override this function to have custom initialisation code.
         * This function should not be called directly.
         * 
         * @return  true if initialisation was successful
         */
        virtual bool initialise();
        /**
         * Shuts down the System.
         *
         * Override this function to have custom shutdown code.
         * This function should not be called directly.
         */
        virtual void shutdown();
		/**
		 * Called before tick any system in the group ticks.
		 *
		 * This function should not be called directly.
		 * @param currentTime The time that will be used for tick()
		 */
		virtual void preTick(double currentTime) = 0;
        /**
         * Update the System with the current time.
         *
         * This function should not be called directly.
         * Override this function to provide custom update code.
         */
		virtual void tick(double currentTime) = 0;
		/**
		 * Called after all the systems in the group have ticked.
		 *
		 * This function should not be called directly.
		 * @param currentTime The time that was used for tick()
		 */
		virtual void postTick(double currentTime) = 0;
        /**
         * Tells the system that it has been registered
         *
         * This function should not be called directly.
         */
        virtual void notifyRegistered(SystemGroup* group);
        /**
         * Tells the system that it has been unregistered
         *
         * This function should not be called directly.
         */
        virtual void notifyUnregistered();
         /**
         * Tells the system that it has been scheduled and will be ticked
         *
         * This function should not be called directly.
         */
        virtual void notifyScheduled(double currentTime);
        /**
         * Tells the system that it has been unscheduled
         *
         * This function should not be called directly.
         */
        virtual void notifyUnscheduled();

		/// Set the last tick time
		void setLastTickTime(double time) { mLastTickTime = time; }
        /// Set a System::Listener on this system to receive notifications
        void setListener(Listener* listener);
		/// Retrieve the last tick time for this system
		double getLastTickTime() const { return mLastTickTime; }
		/// Retrieve the tick interval
		double getTickInterval();
        /// Retrieve the systems name
        const String& getName() const { return mName; }
        /// Retrieve the group that this is registered to
        SystemGroup* getSystemGroup() const;
        /// Retrieve the current state of the system
        State getState() const;
		/// Retrieve the system's ordering value used in start up/shutdown
        int getSystemInitOrdering() const { return mSystemInitOrdering; }
        /// Retrieve the system's ordering value used in running
        int getSystemRunOrdering() const { return mSystemRunOrdering; }
    protected:
        void setState(State state);
        void setSystemGroup(SystemGroup* group);
    };
}

#endif // __OGE_SYSTEM_H__
