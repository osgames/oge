/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_SYSTEMMANAGER_H__
#define __OGE_SYSTEMMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/system/OgeSystemSettings.h"
#include "oge/system/OgeSystem.h"
#include "oge/containers/OgeMap.h"
#include "oge/OgeAsyncValue.h"
#include "oge/OgeSingleton.h"

namespace oge
{
    /**
     * Manages Systems and SystemGroups and provides functions to control
     * their state.
     *
     * @see System
     * @see SystemGroup
     *
     * @author Christopher Jones
     */
    class OGE_CORE_API SystemManager : public Singleton<SystemManager>
    {
    public:
        typedef Map<std::map<String, SystemGroupFactory*>, String, 
            SystemGroupFactory*> SystemGroupFactoryMap;

        typedef Map<std::map<String, SystemGroup*>, String, SystemGroup*> 
            SystemGroupMap;

        typedef Map<std::map<String, System*>, String, System*> SystemMap;

        /**
         * Used to recieve notifications of particular events
         */
        class OGE_CORE_API Listener
        {
        public:
            /**
             * Called for each system being initially set up by the
             * SystemManager.
             *
             * Provides the system, the operating being performed and whether
             * it was succeeded or failed. Defaults to returning true on
             * success, false on failure.
             *
             * @return true to continue or false to abort
             */
            virtual bool notifyRequestCompleted(System* system, 
                System::Operation op, bool success) { return success; }
            /**
             * Called when a system can't be found by its name.
             *
             * Allows providing the system manually (and returning true) or
             * aborting the operation by returning false (default).
             */
            virtual bool notifySystemNotFound(const String& name, 
                System*& system) { return false; }
        };
    private:
        typedef std::vector<TaskThread*> ThreadVector;
        struct SystemEntry
        { 
            System* system; 
            double tickInterval;
            System::Operation previousOp;
            System::Operation targetOp;
            AsyncBool result; 
            bool operator()(const SystemEntry* a, const SystemEntry* b) const
            {
                return  a->system->getSystemInitOrdering() <
                        b->system->getSystemInitOrdering();
            }
        };
        typedef std::vector<SystemEntry*> SystemEntryVector;
        typedef std::vector<SystemEntryVector> OrderedEntriesVector;
    private:
        SpinRWMutex mSystemsMutex;
        SystemMap mSystems;
        SystemGroupFactoryMap mSystemGroupFactories;
        SystemGroupMap mSystemGroups;
        unsigned int mNextSystemGroup;
        Listener* mListener;
        bool mOwnsListener;
    public:
        /// Makes the system known by name to the manager
        bool registerSystem(System* system);
        /// Removes the system from the known systems map
        void unregisterSystem(System* system);
        /// Removes the system from the known systems map
        void unregisterSystem(const String& name);
        /// Retrieves a system given its name, if it has been registered
        System* getSystem(const String& name);

        /// Performs the actions on the systems given in the settings
        bool setupSystems(const SystemSettingsVector& settings);
        /// Unregisters every known system from their system groups
        void shutdownSystems();

        /// Creates a system group for each thread
        void setupSystemGroups(const ThreadVector& threads);
        /// Shuts down all system groups
        void shutdownSystemGroups();
        /// Deletes all system groups
        void destroySystemGroups();
        /// Retrieves all system groups
        std::vector<SystemGroup*> getGroupsAsVector();

        /**
         * Attempts to register a system to a system group
         *
         * The tick interval should be provided too.
         * @return Asynchronous true/false as this occurs in a separate thread
         */
        AsyncBool registerSystemToGroup(System* system,
            double tickInterval = 20);
        /**
         * Attempts to register a system by name to a system group
         *
         * The tick interval should be provided too.
         * @return Asynchronous true/false as this occurs in a separate thread
         */
        AsyncBool registerSystemToGroup(const String& name,
            double tickInterval = 20);

        /// Perform an operation on the given system
        AsyncBool controlSystem(System* system, System::Operation op);
        /// Perform an operation on the system with the given name
        AsyncBool controlSystem(const String& name, System::Operation op);

        /// Set a listener to receive notifications
        void setListener(Listener* listener);
        /// Retrieve the current listener (not thread safe)
        inline Listener* getListener() const { return mListener; }

        /// Register a group factory to a given name
        bool registerSystemGroupFactory(const String& name, 
            SystemGroupFactory* factory);
        /// Unregister a group factory by its name
        void unregisterSystemGroupFactory(const String& name);

        /// Creates built in system group factories, should not be called.
        void createBuiltinFactories();
        /// Deletes built in system group factories, should not be called.
        void destroyBuiltinFactories();

        static SystemManager* createSingleton();
        static void destroySingleton();
        static SystemManager* getSingletonPtr();
        static SystemManager& getSingleton();
    private:
        SystemManager();
        ~SystemManager();
        /**
         * Creates entries from the provided settings which are ordered by 
         * System::getSystemInitOrdering().
         *
         * The entries must be deleted using destroyEntries() if this function
         * succeeds.
         */
        bool buildEntries(const SystemSettingsVector& settings, 
            OrderedEntriesVector& entries);
        /**
         * Creates entries from all known systems and are ordered by
         * System::getSystemInitOrdering().
         *
         * The entries must be deleted using destroyEntries()
         */
        void buildEntriesForShutdown(OrderedEntriesVector& entries);
        /**
         * Frees the memory used by the contained entries
         */
        void destroyEntries(OrderedEntriesVector& entries);
        /**
         * Frees the memory used by the contained entries
         */
        void destroyEntries(SystemEntryVector& entries);
        /**
         * Iterates through all entries and registers them to system groups
         */
        bool registerEntriesToGroups(OrderedEntriesVector& entries);
        /**
         * Temporary solution to retrieve a system group to register a system to
         */
        SystemGroup* getNextSystemGroup();
    };
}

#endif // __OGE_SYSTEMMANAGER_H__
