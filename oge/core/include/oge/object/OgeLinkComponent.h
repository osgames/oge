/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_LINKCOMPONENT_H__
#define __OGE_LINKCOMPONENT_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/object/OgeComponent.h"

namespace oge
{
    /**  
     * The 'LinkComponent' permits to link object together.
     *
     * In this base class the object linked to the others will
     * merely forward the messages to its linked 'child' objects.
     * For example, a bar life that is attached to an entity.
     * 
     * @note The LinkComponent is managed by the ObjectSceneManager
     * @note An object can only be added once (adding an object several time will be ignored)
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API LinkComponent : public Component
    {
        friend class LinkComponentTemplate;

    private:
        // TODO Should we use a std::set (which would simplify several methods).
        /// List of all linked objects
        std::vector<ObjectId> mObjects;

    public:
        virtual ~LinkComponent()
        {}

        /// Set the default values TODO rename to _init()
        virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
        /// Destroy the component but DOESN'T delete
        virtual void _destroy(SceneManager* sceneManager);
        ///
        virtual void _activate(bool activate, SceneManager* sceneManager);
        /**
         *
         * For example return Component::EVERY_TICK;
         */
        virtual UpdateRate _getUpdateRate() const { return Component::EVERY_TICK; }
        /** 
         * @note We do NOT pass the delta time: those component who need it will 
         *       get it from the object
         */
        virtual void _update(double deltaTime);
        /**
         * Implement Serialisable methods
         * @note Actually the serialisation methods read/write the whole object.
         * @todo Only the differences from the object template should be serialised.
         */
        virtual void write(Serialiser* serialiser);
        virtual void read(Serialiser* serialiser);
        virtual bool fixup();

        /**
         * This method redirect the messages to the actual methods to be used.
         *
         *    switch (message.getType())
         *    {
         *        case ObjectMessage::SET_POSITION: _setPositionByMessage( message ); return;
         *        case ObjectMessage::SET_SCALE: _setScaleByMessage( message ); return;
         *        etc...
         *    }
         */
        virtual void _processMessage(const Message& message);

        // ------- Specific method to this Family ----------
        void addObject(const ObjectId& id);
        void removeObject(const ObjectId& id);
        const std::vector<ObjectId>& getObjects() const { return mObjects; }
        bool isPresent(const ObjectId& id);

    protected:
        inline LinkComponent(const ComponentType& type, const ComponentType& family)
            : Component(type, family)
        {}
    };

    /**
     * Builder that is used to create the Links Components based on a template.
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API LinkComponentTemplate : public ComponentTemplate
    {
    public:
        LinkComponentTemplate()
        {
            setFamily("Object");         // NOTE: only in the base component of a family!
            // TODO create a list of types that must be implemented
            setType("LinkComponent");
        }
        /// Create the component
        virtual Component* createComponent()
        {
            return new LinkComponent( getType(), getFamily() );
        }
    };
}
#endif
