/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_OBJECTMANAGER_H__
#define __OGE_OBJECTMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/engine/OgeEngineSettings.h"
#include "oge/OgeSingleton.h"
#include "oge/system/OgeSystem.h"
#include "oge/containers/OgeMap.h"
#include "oge/scene/OgeSceneManagerEnumerator.h"
#include "oge/object/OgeObject.h"
#include "oge/message/OgeMessageScheduler.h"
#include "oge/message/OgeMessageList.h"

namespace oge
{
    // TODO replace by a OgeHashMap once it is cross-platform
    typedef Map<std::map<String, SceneManagerEnumerator*>,
        String, SceneManagerEnumerator*> SceneManagerEnumeratorMap;

    typedef Map<std::map<String, ObjectSceneManager*>,
        String, ObjectSceneManager*> ObjectSceneManagerMap;

    /**
     * Manage all game objects of the game
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API ObjectManager :  public System,
                                        public MessageScheduler, // WIP: Or as a stand alone singleton (eg StateManager)? then could be included in the .h files and calls will be inlined.
                                        public Singleton<ObjectManager>
    {
    private:
        /// Active ObjectSceneManager
        ObjectSceneManager* mObjectSceneManager;
        ObjectSceneManagerMap mObjectSceneManagers;

        SceneManagerEnumerator* mSceneManagerEnumerator;
        SceneManagerEnumeratorMap mSceneManagerEnumerators;

        /// The factory into which all game object templates are registered
        ObjectFactory mObjectFactory;

        /**
         * Used to generate an unique id i.e. "_3343". Hence you can generate
         * unique names such as "node_3345", "camera_3345", etc.
         * @note The _ will probably be removed later it is just here for readability
         */
        unsigned int mUniqueId;
        //  TODO Better solution to manage paused time?
        volatile bool mIsPaused;
        double mLastTime;

    public:
        // ------------ Singleton methods --------------------
        static ObjectManager* createSingleton();
        static void destroySingleton();
        static ObjectManager* getSingletonPtr();
        static ObjectManager& getSingleton();

        // ------------ System methods -----------------------
        bool initialise();
        void shutdown();
        /**
         * Notifies the scene manager that we have ticked and 
		 * the time of the tick.
		 * @param currentTime The tick time
         */
		virtual void postTick(double currentTime);

        /**
         * Notifies the scene manager that we are about to tick 
		 * the time of the tick.
		 * @param currentTime The tick time
         */
		virtual void preTick(double currentTime);

        void tick(double tickTime);

        // ----------- SceneManager methods ------------------
        void createDefaultSceneManager();
        bool setActiveSceneManager(const String& name);
        /** 
         * Creating a scene manager (and add it to mObjectSceneManagers)
         */
        ObjectSceneManager* createSceneManager(const String& name, const String& type);
        /// Returns the active object scene manager
        ObjectSceneManager* getObjectSceneManager() { return mObjectSceneManager; }
        /// Returns the object scene manager or 0 if no SM with this name
        ObjectSceneManager* getObjectSceneManager(const String& name);

        // ------------ Object methods ------------------------
        /**
         * Create an object in the current scene mgr.
         * An object instance of type 'type' will be created
         * but only if an adequate object template was registered before.
         *
         * An important point is that an internal unique id will be assigned
         * to the object in order to create unique id in sub libraries (such as unique
         * Ogre::Node, Ogre::Mesh, etc). Hence this unique id is only valid
         * in this engine instance, NOT across a multiplayer game.
         * To identify an object you should use the "name" of the object that you
         * set with Object::setName().
         *
         * "Create" it in the current scene mgr and the sub SMs. This phase will:
         *   1) Generate an unique id (which identifies the components composing the object
         *   2) register the components in their SM
         *   3) Create the sub-library objects (mesh, opal::solid, etc).
         *
         * @note Crucial method be sure to understand what it does!
         * @todo Good documentation especially a sequence digaram
         * 
         * @param type
         * @param params List of initial parameters
         * @see registerObjectTemplate()
         */
        ObjectId createObject(const ObjectType& type, const MessageList& params);
        /**
         * Destroy an object identified by its internal unique id
         * 
         * @note The name of the scenenode containing the object is identical to the id.
         */
        void destroyObject(const ObjectId& id);
		/**
		 * Get an existing object template
		 * @return the template or null if it doesn't exist
		 */
		ObjectTemplate* getObjectTemplate(const ObjectType& objectType);
		/**
		 * Get an existing object's type
		 * @return the type or null if it doesn't exist
		 */
		ObjectType getObjectType(const ObjectId& id);
        /**
         * Register an object template.
         * All the object template must be registerd here before the method
         * createObject() can be used to create an instance of an object.
         * @param objectTemplate An object template
         */
        bool registerObjectTemplate(ObjectTemplate* objectTemplate);
        /**
         * Register an component template.
         * All the component template must be registerd here before the method
         * createObject() can be used to create an instance of an object.
         *
         * @param sceneManagerType is the SM which manage it such as "Graphics", "Physics", "Object", ...
         * @param componentTemplate A component template
         */
        bool registerComponentTemplate(const String& sceneManagerType, ComponentTemplate* componentTemplate);
        /**
         * This method is called from SceneManager::_createComponent()
         * @note This method shouldn't be called directly.
         */
        inline Component* _createComponent(const ComponentType& type)
        {
            return mObjectFactory.createComponent( type );
        }

        /** WIP
         * Pause the game
         */
        void pause(bool value);
        bool isPaused() { return mIsPaused; }

        /*
         * Dispatch depending on their type to the registered MessageDelegate
         * @param message The Message
		 * @param immediate If immediate then the message should be processed immediately
         * @note Implements the MessageDispatcher pure virtual method.
         */
         void _dispatchMessage(const Message& message, bool immediate = false);

        inline const String& getSceneManagerFamily(const ComponentType& type)
        {
            return mObjectFactory.getSceneManagerFamily( type );
        }

    private:
        ObjectManager();
        virtual ~ObjectManager();

        /**
         * Register the in-build SceneManager
         * The different ObjectSceneManager type available are:
         *   'Generic SM'       : implement an ObjectSceneManager (default SM)
         *   'Terrain Close SM' : TerrainObjectSceneManager
         */
        void createSceneManagement();

        /** Returns an unique id of the form "0_".
         * @note The id is only unique on this instance of the engine. In other
         *       word - it is NOT universally unique and should not be used to
         *       identify objects between networked objects.
         */
        ObjectId getUniqueId();
    };
}

#endif // __OGE_OBJECTMANAGER_H__
