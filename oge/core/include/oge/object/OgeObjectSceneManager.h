/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_OBJECTSCENEMANAGER_H__
#define __OGE_OBJECTSCENEMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/containers/OgeMap.h"
#include "oge/scene/OgeSceneManager.h"
#include "oge/object/OgeObject.h"

namespace oge
{
    /**
     * Default Object SceneManager
     * Manage only a default graphics, input and gui scene managers
     *
     * @author Steven 'lazalong' Gay, Mehdi Toghianifar
     */
    class OGE_CORE_API ObjectSceneManager : public SceneManager
    {
        friend class ObjectManager;

	public :
        typedef Map<std::map< String, SceneManager*>,
             String, SceneManager*> SceneManagerMap;

        typedef std::vector<SceneManager*> SceneManagerPriorityMap;
		typedef std::map<ObjectId,ObjectType> ObjectTypesMap;
    protected:


        /// Stores the sub scene managers by family map<family, sm*>
        /// @note This scene manager is also added to the map
        SceneManagerMap         mSceneManagers;
        SceneManagerPriorityMap mSceneManagerPriority;
        GraphicsSceneManager*   mGraphicsSceneManager;
        PhysicsSceneManager*    mPhysicsSceneManager;
		NetworkSceneManager*    mNetworkSceneManager;
        ScriptSceneManager*     mScriptSceneManager;

        bool mShowBoundingBox;    

       /**
         *  All the game objects. 
         *  TODO Of course we will need one or more better containers
         *       for spatial optimisation, by object type, etc.
         */
        std::vector<ObjectId> mObjects;

		ObjectTypesMap mObjectTypes;

    public:
        // TODO WIP MAKE Private!
        double mLastMaxMessageProcessingTime;

    public:
        ObjectSceneManager(const String& name);
        virtual ~ObjectSceneManager();

        // SceneManager methods
        virtual void init();
        virtual void initMetaData() const;
        virtual void update(double deltaTime);

		/**
		 * Process an immediate message.  This will have all
		 * scene managers process this message immediately
		 * @param msg The message to process
		 */
		virtual bool processMessageImmediate(const Message& msg);

		/**
		 * WIP update all active scene managers by querying the managers
		 * @author Alex
		 */
		virtual void updateSceneManagers();

        /**
         * WIP Delete all component directly (not via deleteObject())
         * TODO Consider calling destroyObject() on all objects
         *      and leave the sub-scenemanager::clearScene() being called
         *      by the sm destructor to be sure all components where deleted.
         */
        virtual void clearScene();

        // Specific methods
        /// Will return the first SM of the defined family found
        SceneManager* getSceneManagerByFamily( const String& family );
        inline GraphicsSceneManager* getGraphicsSceneManager() { return mGraphicsSceneManager; }

        /**
         * Destroy an object identified by its internal unique id
         * 
         * @note The name of the scenenode containing the object is identical to the id.
         */
        void destroyObject(const ObjectId& object);
        /**
         * De-/activate components of the object and remove/add them from/to update lists
         * of their owner scene managers.
         */
        void activateObject(const ObjectId& object, bool activate);
		/**
		 * Get the object type for this object
		 * @return the object type or empty string if not found
		 */
		ObjectType getObjectType(const ObjectId& object);

		/**
		 * Get all objects of a particular type
		 * @param type The object type
		 * @return the object ids map
		 */
		StringVector getObjectsOfType(ObjectType type) const;

		/**
		 * Get all object types
		 * @return the object ids
		 */
		const ObjectTypesMap& getObjectsTypes() const;

        /**
         * Toggle the bounding box of the entities
         * If no id is passed all the object will be affected
         */
        void showBoundingBox(bool show, const ObjectId& object = "");

        /** 
         * Return the id of the object that is pointed by the cursor
         *
         * @return The nearest object the ray intersaction with. 
         *         If none is found 0 is returned.
         * @note Expensive as it do a ray query
         * @todo Update with http://www.ogre3d.org/wiki/index.php/Raycasting_to_the_polygon_level
         * @todo Only works for the First Mouse!
         * @todo Pass a list of objects to ignore (windows, player mesh, transparent triggers, ...)
         *       Probably a list of tag should be passed (all windows ignored, ...)
         */
        ObjectId doObjectIdRayQuery();

        /**
         * Dispatch the message to all sub-scene managers
         *
         * @todo Or put in Objectmgr and post to other mgrs?
         */
        inline void _dispatchMessage(const Message& message)
        {
            // TODO Optimise & make thread-safe...
            for (SceneManagerMap::Iterator iter = mSceneManagers.begin();
                mSceneManagers.end() != iter; ++iter)
            {
                iter->second->postMessage(message);
            }
        }

    protected:
        /** 
         * Set the active SM for the different sub-scene managers (gui, ...)
         * Sould be called by ObjectManager::setActiveSceneManager()
         */
        bool setActiveSceneManager();

        /**
         * "Create" it in the current scene mgr (we use create only to better follow
         * the creation sequence.
         * The object is activated by default.
         *
         * @note Crucial method. Programmers must be sure to understand what it does.
         */
        bool createObject(const ObjectId& id, ObjectTemplate* objectTemplate,
            const MessageList& params, bool activate = true);

        bool _destroyObjectHook(const Message& message);    // TODO FIXME: IT'S A MSG HOOK NOW

        bool processMessage(const Message& message);

    private:
        void destroyObjectWithId(const String& id);
    };

    /** 
     * Factory to create ObjectSceneManager
     * 
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API ObjectSceneManagerFactory : public SceneManagerFactory
    {
    public:
        ObjectSceneManagerFactory();
        virtual ~ObjectSceneManagerFactory();
        /** 
         * @note The factory is not responsibly of deleting the instance
         */
        virtual SceneManager* createInstance(const String& name);
        virtual bool checkAvailability();
    };

    /**
     * Specialisation of the ObjectSceneManager
     * The differences are the use of a TerrainGraphicsSceneManager instead 
     * of a default GraphicsSceneManager
     * 
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API TerrainObjectSceneManager : public ObjectSceneManager
    {
    public:
        TerrainObjectSceneManager(const String& name);
        virtual ~TerrainObjectSceneManager();
        virtual void init();
        virtual void initMetaData() const;
    };

    /**
     * Factory to create TerrainObjectSceneManager
     * 
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API TerrainObjectSceneManagerFactory : public SceneManagerFactory
    {
    public:
        TerrainObjectSceneManagerFactory();
        virtual ~TerrainObjectSceneManagerFactory();
        /** 
         * @note The factory is not responsibly of deleting the instance
         */
        virtual SceneManager* createInstance(const String& name);
        virtual bool checkAvailability();
    };

    /**
     * Specialisation of the ObjectSceneManager
     * The differences are the use of a PCZGraphicsSceneManager instead 
     * of a default GraphicsSceneManager
     * 
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API PCZObjectSceneManager : public ObjectSceneManager
    {
    public:
        PCZObjectSceneManager(const String& name);
        virtual ~PCZObjectSceneManager();
        virtual void init();
        virtual void initMetaData() const;
    };

    /**
     * Factory to create a PCZObjectSceneManager
     * 
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API PCZObjectSceneManagerFactory : public SceneManagerFactory
    {
    public:
        PCZObjectSceneManagerFactory();
        virtual ~PCZObjectSceneManagerFactory();
        /** 
         * @note The factory is not responsibly of deleting the instance
         */
        virtual SceneManager* createInstance(const String& name);
        virtual bool checkAvailability();
    };

}
#endif // __OGE_OBJECTSCENEMANAGER_H__
