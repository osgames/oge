/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_GUIMANAGER_H__
#define __OGE_GUIMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/system/OgeSystem.h"
#include "oge/input/OgeInputEvent.h"

namespace oge
{
    class OGE_CORE_API GuiManager : public System
    {
    private:
        static GuiManager* mManager;

    protected:
        // ---------- Gui specific ---------------------
        /**
         * Render window created by the graphics manager.
         * @note must be cast in the plugin to the correct type
         */
        void* mRenderWindow; 

    public:
        GuiManager(const String& name, int systemInitOrdering = 20, int systemRunOrdering = 110);
        virtual ~GuiManager();

        /// @see System::tick()
        virtual void tick(double currentTime) {}
		virtual void preTick(double currentTime) {}
		virtual void postTick(double currentTime) {}

        /**
         * Returns the manager
         * @note The gui manager is a singleton so this method is equivalent
         *       to getSingletonPtr() however the instantiation i.e. GuiManagerMyGUI
         *       is also a singleton hence getManager() is to avoid name conflict.
         * @see getSingletonPtr() which returns the actual implementation
         */
        inline static GuiManager* getManager() { return mManager; }

		/**
		 * Show or hide the cursor
		 * @param visible Whether the cursor should be visible or not
		 */
		virtual void setCursorVisible(bool visible) {}

        /**
         * @note The Render Window must be set before most Gui Manager
         *       can be created. As this method must work for any gui
         *       and grapics libraries we pass a size_t that must 
         *       be cast afterwards.
         * @note This method will assert if a null pointer is given.
         */
        void setRenderWindow( void* renderWindow )
        {
            mRenderWindow = renderWindow;
        }

        virtual void keyPressed (const KeyEvent& arg) {}
        virtual void keyReleased (const KeyEvent& arg) {}
        virtual void mousePressed (const MouseEvent& arg) {}
        virtual void mouseReleased (const MouseEvent& arg) {}
        virtual void mouseMoved (const MouseEvent& arg) {}
        virtual void joyButtonPressed (const JoystickEvent& arg) {}
        virtual void joyButtonReleased (const JoystickEvent& arg) {}
        virtual void joyAxisMoved (const JoystickEvent& arg) {}
        virtual void joyPovMoved (const JoystickEvent& arg) {}

        /**
         * Load a gui layout from file.
         * Only for the gui library supporting this method will this
         * method do something.
         */
        virtual void loadLayout(const String& filename, const String& layout) {}

        /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        virtual const String getLibrariesVersion() { return "Gui: No special library used.\n"; }
    };
}

#endif // __OGE_GUIMANAGER_H__
