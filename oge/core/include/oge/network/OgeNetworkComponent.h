/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_NETWORKCOMPONENT_H__
#define __OGE_NETWORKCOMPONENT_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/object/OgeComponent.h"
#include "oge/network/OgeNetworkClientCommand.h"
#include "oge/network/OgeNetworkTick.h"
#include "oge/math/OgeQuaternion.h"
#include "oge/math/OgeVector3.h"

namespace oge
{
    /**  
     * Interface for NetworkComponent's type.
     *
     * This component type permits receiving information about this 
	 * object over a network.
     *
     * @author Alex Peterson
     */
    class OGE_CORE_API NetworkComponent : public Component
    {
    public:
		typedef NetworkTick_t<unsigned short> NetworkTick;

		// this struct defines data we interpolate per tick
		typedef struct {
			NetworkTick tick;
			Vector3 position;
			Quaternion orientation;
		} TickData;

		// non tick data - stuff that changes less frequently but is still really common
		// kept to less than 32 values so we can use an int for dirty flags
		typedef struct {
			unsigned short health;  // health
			unsigned short power; // power
			unsigned short ammo[8]; // ammo amount for up to 8 weapons
			unsigned short armour[8]; // armour amount for up to 8 armour pieces
			unsigned short damage[8]; // amount of dmg accumulated since last send (gets cleared on send)

		} NonTickData;

		// enum for dirty flags
		enum {
			HEALTH = 0,
			POWER,
			AMMO,
			ARMOUR = 10,
			DAMAGE = 18
		};

        virtual ~NetworkComponent() {}

        /**
         * Implement Serialisable methods
         * @note Actually the serialisation methods read/write the whole object.
         * @todo Only the differences from the object template should be serialised.
         */
        virtual void write(Serialiser* serialiser);
        virtual void read(Serialiser* serialiser);
        virtual bool fixup();
		
		/**
		 * Add tick data for this network component
		 * Tick data is used on the server to send accurate updates.
		 * Tick data is used on the client to interpolate position
		 * and orientation.
		 * @param tickData The tick data
		 */
		void addTickData(const NetworkComponent::TickData& tickData);

		/**
		 * Get the latest client command for this object
		 * @return the client command
		 */
		const NetworkClientCommand& getClientCommand() { return mClientCommand; }

		/**
		 * Get the dirty flags/bits for non tick data that has changed
		 * since we last sent an update.
		 * @return the dirty flags/bits
		 */
		unsigned int getDirtyFlags() { return mDirtyFlags; }

		/**
		 * Get the most recent tick value
		 * @return the most recent tick value
		 */
		const NetworkTick getLastTick() { return mLastTick; }

		/**
		 * get the owner network id  (will be -1 if none)
		 * @param id The network id of the owner
		 */
		int getNetworkOwnerId() { return mOwnerNetworkId; }

		/**
		 * Get the non tick data
		 * @return The non tick data
		 */
		const NetworkComponent::NonTickData& getNonTickData() { return mNonTickData; }

		/**
		 * Get tick data for a particular tick
		 * @param tick (optional)
		 * @return the tick data struct or an empty one
		 */
		const NetworkComponent::TickData& getTickData(NetworkTick tick = 0);

		/**
		 * Get the update rate for this component
		 * @return The update rate for this component
		 */
		Component::UpdateRate _getUpdateRate() const;

		/**
		 * This function should be called after the manager for this
		 * component runs tick().  This allows the component to 
		 * update based on the tick timing settings for the manager that
		 * owns this component.
		 * @param currentTime The time that was used for tick()
		 */
		virtual void _postTick(double currentTime);

		/**
		 * Process a message
		 * @param message The message
		 */
		virtual void _processMessage(const Message& message);

		/**
		 * Set the client command for this object
		 * @param command The client command
		 */
		void setClientCommand(const NetworkClientCommand& command) { mClientCommand = command; }

		/**
		 * Set the dirty flags/bits for non tick data that has changed
		 * since we last sent an update.
		 * @param newDirtyFlays the dirty flags/bits
		 */
		void setDirtyFlags(unsigned int newDirtyFlags) { mDirtyFlags = newDirtyFlags; }

		/**
		 * Set the non tick data for this component
		 * @param data The non tick data
		 */
		void setNonTickData(const NonTickData& data) { mNonTickData = data; }

		/**
		 * Set the owner network id 
		 * @param id The network id of the owner
		 */
		void setNetworkOwnerId(int networkId) { mOwnerNetworkId = networkId; }

    protected:
        NetworkComponent(const ComponentType& type, const ComponentType& family);

		// the latest client command for this object
		NetworkClientCommand mClientCommand;

		// dirty flags for non tick data
		// dirty data needs to be sent to clients
		unsigned int mDirtyFlags;

		// the number of tick data slots to allocate
		unsigned short mNumSlots;

		// the data for each tick (used on client)
		std::vector<TickData> mTickData;

		// the most recent tick data for this component
		NetworkTick mLastTick;

		// non tick data - stuff that changes less frequently but is still really common
		NonTickData mNonTickData;

		// the owner/controller of this object
		int mOwnerNetworkId;
    };

    class OGE_CORE_API NetworkComponentDefault : public NetworkComponent
    {
		friend class NetworkComponentTemplateDefault;
    public:
        virtual ~NetworkComponentDefault() {}

        virtual void _activate(bool activate, SceneManager* sceneManager);
        virtual void _destroy(SceneManager* sceneManager);
        virtual UpdateRate _getUpdateRate() const;
        virtual void _processMessage(const Message& message);
        virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
        virtual void _update(double deltaTime);

        /** Implement Serialisable methods
         * @note Actually the serialisation methods read/write the whole object.
         * @todo Only the differences from the object template should be serialised.
         */
        virtual void write(Serialiser* serialiser);
        virtual void read(Serialiser* serialiser);
	protected:
        inline NetworkComponentDefault(const ComponentType& type, const ComponentType& family)
            : NetworkComponent(type, family)
        {}
	};

    /**
     * Builder that is used to create the Network Component based on a template.
     *
     * @author Alex Peterson
     */
    class OGE_CORE_API NetworkComponentTemplateDefault : public ComponentTemplate
    {
    public:
        NetworkComponentTemplateDefault() 
        {
            setFamily("Network");
            setType("NetworkComponent");
        }

        /// Create the component
        virtual Component* createComponent()
        {
            return new NetworkComponentDefault( getType(), getFamily() );
        }
    };
}
#endif
