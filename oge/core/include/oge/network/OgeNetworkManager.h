/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_NETWORKMANAGER_H__
#define __OGE_NETWORKMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/system/OgeSystem.h"
#include "oge/input/OgeInputEvent.h"
#include "oge/message/OgeMessageList.h"
#include "oge/network/OgeNetworkClientCommand.h"
#include "oge/network/OgeNetworkSceneManager.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"

namespace oge
{
    /**
     * Network Manager Interface
     *
     * @author Alex 'petrocket' Peterson
     */
    class OGE_CORE_API NetworkManager : public System
    {
    public:
        NetworkManager(const String& name, int systemInitOrdering = 10, int systemRunOrdering = 30);
        virtual ~NetworkManager();

		// network client id ranges from 0 to the number of clients allowed 
		// on the server. Each client has their own network client id.  
		// The first player to connect will get id 0, the next will get id 1 
		// and so on.  -1 denotes an invalid client or a non-existant one.
		typedef int NetworkClientId;

		// network host id is a unique id for a network adapter.  This network
		// id is unique to the adapter. Mostly here as a temp solution for 
		// mapping OGE network clients to RakNet clients
		typedef unsigned long long int NetworkAdapterId;

		// our basic client info struct
		typedef struct NetworkClientStruct {
			NetworkAdapterId id;
			std::string name;
		} NetworkClient;

		typedef std::map<ObjectId, ObjectId> ObjectIdMap;

		// the index in the NetworkClientObjects vector is the NetworkClientId
		typedef std::vector<std::set<ObjectId> > NetworkClientObjects;

		// the index in the NetworkClients vector is the NetworkClientId
		typedef std::vector<NetworkClient> NetworkClients;

        /**
         * Used to receive notifications of network events
         */
        class OGE_CORE_API NetworkListener
        {
        public:
            /**
             * Called when a client connects.
             *
			 * @param id The network client id of the client
			 * @param thisClient True if the network id is for this client
             */
            virtual void notifyClientConnected(const NetworkClientId id, const bool thisClient) { }

			/**
             * Called when a client disconnects
			 * @param id The network client id of the client that disconnected
             */
            virtual void notifyClientDisconnected(const NetworkClientId id) { }

            /**
             * Called when a client info is updated.
             *
			 * @param id The network client id of the client
			 * @param thisClient True if the network id is for this client
             */
            virtual void notifyClientInfo(const NetworkClientId id, const bool thisClient) { }

			/**
             * Called when a custom packet has arrived
             *
			 * @param id The network client id of the client that sent the packet
			 * @param messageType The type of message received
			 * @param data The data we recieved
			 * @param length The length of the data
             */
            virtual void notifyCustomPacket(const NetworkClientId id, unsigned int messageType, const unsigned char* data, unsigned int length) { }

            /**
             * Called when a network object is created
             *
			 * @param id The network client id of the client
			 * @param ownerId The network id of the owner of the object
             */
			virtual void notifyObjectCreated(const ObjectId& id, const NetworkClientId ownerId) { }

            /**
             * Called when a pong packet is received in 
			 * response to a ping this machine sent
             *
			 * @param host The host
			 * @param port The port
			 * @param ping The ping
			 * @param serverName The server name
			 * @param maxClients The max number of clients
			 * @param numClients The number of clients
			 * @param passwordRequired Whether a password is required to connect
             */
			virtual void notifyPong(const String& host, unsigned int port, unsigned int ping, const String& serverName, unsigned int maxClients, unsigned int numClients, bool passwordRequired) { }

            /**
             * Called when a custom message has arrived for this client
             *
			 * @param from The network client id of the client that sent the message
			 * @param data The data
			 * @param length Length of the data
             */
            virtual void notifyMessageRecieved(const NetworkClientId from, const char* data, const int length) { }
        };

        enum ConnectionStatus
        {
            NOT_CONNECTED = 0,
			CONNECTING,
			CONNECTED,
			CONNECTION_FAILED,
			CONNECTION_DISCONNECTED
        };

		enum ServerType
		{
			SERVER = 0,
			LISTEN_SERVER,
			PEER_TO_PEER
		};

		/**
		 * Add a client controlled object
		 * @param id the ObjectId of the object
		 * @param networkId The network id of the client
		 */
		void addClientControlledObject(const ObjectId& id, NetworkClientId networkId);

        /** 
         * Connect to a server or other computer over the network.
         * @note Because we are connecting over a network, we won't know if
         *       the connection has failed until the timeout expires.
         * @param host The ip address or dns name of the host to connect to
         * @param remotePort The port on the remote host to connect to
         * @param numAttempts The number of connection attempts to make
         * @param attemptWaitTime The amount of time to wait between connection
         *                        attempts in milliseconds.
         * @return true on successful connection initiation, false on failure or if
         *         already connected to the specified host
         */
        virtual bool connect(const String& host, unsigned short remotePort, 
            unsigned int numAttempts=12, unsigned int attemptWaitTime=500) { return false; }

		/**
		 * Create the default scene manager
		 */
		virtual void createDefaultSceneManager();

        /**
		 * Creates a scene manager of type 'type' named 'name'
		 * @return the new scene manager
		 */
        SceneManager* createSceneManager(const String& name, const String& type);

        /**
         * Disconnect from a server or other computer over the network
         * @param host The ip address or dns name of the host to disconnect to.  
         *             If empty disconnect from the first active connection.
         * @return true on successful disconnect initiation, false on failure or if
         *         not connected.
         */
        virtual bool disconnect(const String& host) { return false; }

        /**
		 * Returns the active scene manager
		 * @return the active scene manager
		 */
        NetworkSceneManager* getActiveSceneManager() { return mSceneManager; }

		/**
		 * Get the client controlled objects
		 * @return client controlled object map
		 */
		const NetworkClientObjects& getClientControlledObjects() { return mClientControlledObjects; }

		/**
		 * Get the clients
		 * @return the clients
		 */
		const NetworkClients& getClients() { return mClients; }

		/**
		 * Get the first client controlled object
		 * @param clientId The client id to look for
		 * @return The ObjectId of the client controlled object
		 */
		ObjectId getClientControlledObject(NetworkClientId clientId);

		/**
		 * Get the corresponding ObjectId for the specified server ObjectId
		 * @return the ObjectId on this machine that corresponds to the server ObjectId
		 */
		ObjectId getClientObjectId(const ObjectId& objectId);

         /**
         * Returns the component with id in the active scene manager, 0 otherwise.
         * @note returns a 'ptr* const' aka you can't change the pointer
         */
        inline Component* const getComponent(const ObjectId& id, const ComponentType& family);

		/**
		 * Return the client connection status. Does not apply to servers.
		 *
		 * @return The current client connection status. Does not apply to servers
		 */
		int getConnectionStatus() { return mConnectionStatus; }

        /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        virtual const String getLibrariesVersion() { return "Network: No special library used. "; }

		/**
		 * Get the network id of the owner for an object.
		 * @param id The ObjectId of the object
		 * @return The network id of the client that owns this object or -1 if no one
		 */
		NetworkClientId getObjectNetworkOwner(const ObjectId& id);

		/** 
		 * Get the current tick value
		 * @return The current tick
		 */
		unsigned short getNetworkTick() { return mNetworkTick; }

		/**
		 * Get the current tick rate (number of ticks per second)
		 * @return The current tick rate
		 */
		unsigned int getNetworkTickRate() { return mNetworkTickRate; }

        /**
         * Returns the manager
         * @note The manager is a singleton so this method is equivalent
         *       to getSingletonPtr() however the instantiation i.e. NetworkManagerRakNet
         *       is also a singleton hence getManager() is to avoid name conflict.
         * @see getSingletonPtr() which returns the actual implementation
         */
        inline static NetworkManager* getManager() { return mManager; }

		/**
		 * Return the network id of this client/server
		 * @return The network id of this client/server
		 */
		NetworkClientId getNetworkId() { return mNetworkId; }

		/**
		 * Get Scene manager enumerator for this manager
		 * @return the scene manager enumerator
		 */
        SceneManagerEnumerator* getSceneManagerEnumerator() { return mSceneManagerEnumerator; } 

		/**
		 * Get the server name
		 * @return The new server name
		 */
		const String& getServerName() { return mServerName; }

		/** 
		 * Get the current tick value on the server
		 * @return The current tick
		 */
		unsigned short getServerTick() { return mNetworkTick + mNetworkTickDelta; }

		/**
		 * Get the zero tick time
		 * @return The zero tick time
		 */
		double getZeroTickTime() { return mZeroTickTime; }

		/**
		 * Initialize this network manager - registers components
		 * @return true on success initialization or false on failure
		 */
		virtual bool initialise();

		/**
		 * Returns true if this is a server and not a client
		 * @return true if this is a server
		 */
		bool isServer() { return mIsServer; }

        /**
         * Start a server and listen for connections on the given port.
         * @param port The port to listen on
         * @param maxConnections The maximum number or connections allowed
		 * @param serverType The server type - SERVER, LISTEN_SERVER, PEER_TO_PEER
         * @return true on successful initialization or false on failure
         */
        virtual bool listen( unsigned short port, unsigned short maxConnections = 255, int serverType = SERVER) {return false; }

		/**
		 * Send a ping to a remote host/port.  The network listener will be
		 * notified when a pong is received.
		 * @param host The host to ping
		 * @param remotePort The remote port
		 */
		virtual void ping(const String& host, unsigned short remotePort) {}

        /**
         * Notifies the scene manager that we have ticked and 
		 * the time of the tick.
		 * @param currentTime The tick time
         */
		virtual void postTick(double currentTime);

        /**
         * Notifies the scene manager that we are about to tick 
		 * the time of the tick.
		 * @param currentTime The tick time
         */
		virtual void preTick(double currentTime);

		/**
		 * Remove a client controlled object
		 * @param id The ObjectId to remove
		 * @param networkId The network id of the client to remove the object from
		 * @return true if removed false if not found or failed
		 */
		bool removeClientControlledObject(const ObjectId& id, NetworkClientId networkId = 0);

        /**
         * Send data over the network.
		 * @param recipient The networkclientid of the recipient
		 * @param messageType The type of message
		 * @param data The data to send
		 * @param length The length of the data we're sending
         */
		virtual void send(NetworkClientId recipient, unsigned int messageType, const unsigned char* data, const int length) {}

        /**
         * Send data over the network to all clients
		 * @param messageType The type of message
		 * @param data The data to send
		 * @param length The length of the data we're sending
         */
		virtual void sendToAll(unsigned int messageType, const unsigned char* data, const int length) {}

		/**
		 * Set the active scene manager
		 * @param sm The new active scene manager
		 */
		inline void setActiveSceneManager(NetworkSceneManager* sm) { assert(sm); mSceneManager = sm; }

        /**
		 * Set a listener to receive network notifications
		 * @param listener
		 */
        virtual void setNetworkListener(NetworkListener* listener);

		/**
		 * Set the tick rate - number of ticks per second.  Default is 66.
		 * @param rate The new tick rate 
		 */
		virtual void setNetworkTickRate(unsigned int rate) { if(rate <= 1000) mNetworkTickRate = rate; }

		/**
		 * Set the server name
		 * @param name The new server name
		 */
		virtual void setServerName(const String& name) { mServerName = name; }

        // @see System::tick()
        virtual void tick(double currentTime) {}

    protected:
		// a vector of clients - index is the NetworkClientId
		NetworkClients mClients;

		// a map of objects each client controls
		NetworkClientObjects mClientControlledObjects;

		// connection status
		int mConnectionStatus;

		// true if this is a server
		bool mIsServer;

		// the last time our network ticked
		double mLastNetworkTickTime;

		// number of maximum connections allowed if this is a server
		unsigned int mMaxConnections;

		// the network listener
		NetworkListener* mNetworkListener;

		// the network id of this server/client
		NetworkClientId mNetworkId;

		// current number of network clients
		unsigned short mNumClients;

        // the active sceneManager
        NetworkSceneManager* mSceneManager;

		// contains the scene managers & their factories
        SceneManagerEnumerator* mSceneManagerEnumerator;

		// a mapping of client object ids to server object ids
		// (not used on the server)
		ObjectIdMap mNetworkObjectIds;

		// current tick
		unsigned short mNetworkTick;

		// tick different between server and client
		unsigned short mNetworkTickDelta;

		// number of ticks per second
		int mNetworkTickRate;

		// the name of this server
		String mServerName;

		// the type of server - server (default), listen server, peer to peer
		int mServerType;

		// number of ticks per second on the server
		int mServerNetworkTickRate;

		// the time of the first server tick
		double mZeroTickTime;

    private:
        static NetworkManager* mManager;
    };
}
#endif // __OGE_NETWORKMANAGER_H__
