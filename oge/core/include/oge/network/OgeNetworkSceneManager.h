/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_NETWORKSCENEMANAGER_H__
#define __OGE_NETWORKSCENEMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/containers/OgeMap.h"
#include "oge/scene/OgeSceneManager.h"
#include "oge/OgeAsyncValue.h"
#include "oge/network/OgeNetworkManager.h"

namespace oge
{
	typedef std::map<ObjectId, unsigned char> NetworkAwareStatus;
	typedef std::map<ObjectId, MessageList> ObjectParams;

    /**
     * Default Network SceneManager
     *
     * @author Alex "petrocket" Peterson
     */
    class OGE_CORE_API NetworkSceneManager : public SceneManager
    {
        friend class ObjectManager;

    public:
		enum {
			NETWORK_OBJECT_NOCHANGE = 0,
			NETWORK_OBJECT_CREATE,
			NETWORK_OBJECT_UPDATE,
			NETWORK_OBJECT_DESTROY,
			NETWORK_OBJECT_PROCESSED
		};

        NetworkSceneManager(const String& name);
        virtual ~NetworkSceneManager();

        /**
		 * Internal method used once to initialise the metadata, must be implemented
         * @note Also used by factories to know which type of sm it will create.
         */
        virtual void initMetaData() const;

		/**
		 * Init
		 */
        virtual void init();

		/**
		 * Clear the scene
		 */
        virtual void clearScene();

        /**
         * "Intermediary" method in the object creation sequence.  We override
		 * this method so that we can save the params for re-creating the object
		 * @param id The object's id
		 * @param type The component type to create
		 * @param params Initialization params
		 * @return true on success - must call AsyncBool::isValid() before using
         */
        virtual AsyncBool createComponent(const ObjectId& id, const ComponentType& type,
            const MessageList& params);

		/**
		 * Get the creation params for an object
		 * @param id The object's id
		 * @return the params used when creating this object
		 */
		const MessageList& getObjectCreationParams(const ObjectId& id);

		/**
		 * Get the objects this client is aware of
		 * @param clientId the network client id
		 * @return The objects the client is aware of
		 */
		const NetworkAwareStatus& getClientAwareObjects(int clientId);

		/**
		 * Remove a client aware object
		 * @param id The object id of the object o remove
		 * @param clientId The client network id
		 */
		void removeClientAwareObject(const ObjectId& id, int clientId = -1);

		/**
		 * Remove all client aware objects - used when a player connects/disconnects
		 * @param clientId The client network id
		 */
		void removeAllClientAwareObjects(int clientId);

		/**
		 * Set the aware status for an object and a client - or all clients
		 * if the clientId is set to -1.
		 * @param id The ObjectId of the object to change the status for
		 * @param status The new status
		 * @param clientId The (optional)network client id. 
		 */
		void setClientObjectAwareStatus(const ObjectId& id, int status, int clientId = -1);

		/**
		 * Update method called every tick
		 * @param deltaTime The elapsed time since the last call to update()
		 */
        virtual void update(double deltaTime);

    protected:
        bool processMessage(const Message& message);

		ObjectParams mObjectCreationParams;

		std::vector<NetworkAwareStatus> mClientAwareObjects;
    };

    //-------------------------------------------------------------------------

    /**
     * Factory instanciating the scene manager
     * @author Alex "petrocket" Peterson
     */
    class OGE_CORE_API NetworkSceneManagerFactory : public SceneManagerFactory
    {
    public:
        NetworkSceneManagerFactory();
        virtual ~NetworkSceneManagerFactory();

        virtual SceneManager* createInstance(const String& name);
        virtual bool checkAvailability();
    };
}
#endif
