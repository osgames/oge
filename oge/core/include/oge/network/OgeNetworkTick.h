/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef _OGE_NETWORKTICK_H_
#define _OGE_NETWORKTICK_H_

#include "oge/OgeCorePrerequisites.h"

namespace oge
{
	template <class T>
	class NetworkTick_t
	{
	public:
		inline NetworkTick_t()
		{
		}

		NetworkTick_t(const T& rhs)
		{
			this->tick = rhs.tick;
		}

		NetworkTick_t(const int rhs)
		{
			this->tick = rhs % std::numeric_limits<T>::max();
		}

		T tick;

        inline T& operator = ( const T& rhs )
        {
            this->tick = rhs;
            return this->tick;
        }

        inline T& operator = ( const int rhs )
        {
            this->tick = rhs % std::numeric_limits<T>::max();
            return *this;
        }

		bool operator==(const T& rhs) const {
            return (tick == rhs);
        }

        bool operator==(const int& rhs) const
        {
            return (tick == rhs % std::numeric_limits<T>::max());
        }
        inline T operator + (const T& rhs) const
        {
            return (this->tick + rhs.tick);
        }

        inline T operator + (const int rhs ) const
        {
            return this->tick + (rhs % std::numeric_limits<T>::max()) ;
        }

        inline T operator - (const T& rhs ) const
        {
            return (this->tick - rhs.tick);
        }

        inline T operator - (const int rhs ) const
        {
            return this->tick - (rhs % std::numeric_limits<T>::max()) ;
        }

        /** 
		 * This whole template exists for this logic.
		 * A tick value of max() is actually "less" than a tick
		 * value of 0 because max() would wrap around if u incremented
		 * it by one.  
		 * @param rhs The right hand side argument
		 * @return true if the input tick is 
         */
        inline bool operator > ( const T& rhs ) const
        {
			if(this->tick > 10 || rhs < std::numeric_limits<T>::max() - 10) {
				// use regular logic
				return (this->tick > rhs);
			}

			// if tick <= 10 && rhs >= max - 10 then tick IS greater than rhs
			return true;
        }

        inline bool operator >= ( const T& rhs ) const
        {
			if(this->tick > 10 || rhs < std::numeric_limits<T>::max() - 10) {
				// use regular logic
				return (this->tick >= rhs);
			}

			// if tick <= 10 && rhs >= max - 10 then tick IS greater than rhs
			return true;
        }

        inline bool operator < ( const T& rhs ) const
        {
			if(rhs > 10 || this->tick < std::numeric_limits<T>::max() - 10) {
				// use regular logic
				return (this->tick < rhs);
			}

			// if tick >= max - 10 && rhs <= 10 then tick IS less than rhs
			return true;
        }

        inline bool operator <= ( const T& rhs ) const
        {
			if(rhs > 10 || this->tick < std::numeric_limits<T>::max() - 10) {
				// use regular logic
				return (this->tick <= rhs);
			}

			// if tick >= max - 10 && rhs <= 10 then tick IS less than rhs
			return true;
        }

        inline T operator % ( const T& rhs ) const
        {
			return this->tick % rhs;
        }

		inline operator double () const
        {
            return (double)(this->tick);
        }
	};
}

#endif