/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_NETWORKCLIENTCOMMAND_H__
#define __OGE_NETWORKCLIENTCOMMAND_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"

namespace oge
{

#define BIT(x) (1<<(x))

    /**  
     * Network client command object
     *
     * This object is used to translate client commands into a form suitable
	 * for sending over the network to the server
     *
     * @author Alex Peterson
     */
    class OGE_CORE_API NetworkClientCommand
    {
    public:
		NetworkClientCommand() : mCmdBits(0), mTick(0), mMouseX(0), mMouseY(0) {}
        virtual ~NetworkClientCommand() {}

		// these are the possible command bits, used for sending
		// the players input to the server
		enum CommandBits {
			// translation
			FORWARD = BIT(0),
			BACK    = BIT(1),
			LEFT    = BIT(2),
			RIGHT   = BIT(3),
			UP      = BIT(4),
			DOWN    = BIT(5),

			// rotation
			ROLL_LEFT   = BIT(6),
			ROLL_RIGHT  = BIT(7),
			YAW_LEFT    = BIT(8),
			YAW_RIGHT   = BIT(9),
			PITCH_UP    = BIT(10),
			PITCH_DOWN  = BIT(11),

			// other - attack, jump, duck , use, etc.
			BTN1 = BIT(12),
			BTN2 = BIT(13),
			BTN3 = BIT(14),
			BTN4 = BIT(15)

			// more - unused
		};

		bool isSet(unsigned int cmd) { return (mCmdBits & cmd) != 0; }
		void setCommand(unsigned int cmd) { mCmdBits |= cmd; }
		void unsetCommand(unsigned int cmd) { mCmdBits &= ~cmd; }

		/// public cmd variable
		unsigned int mCmdBits;

		/// public tick value (time value) that this command is for
		unsigned short mTick;

		/// public mouse position
		float mMouseX;
		float mMouseY;

		// public position
		Vector3 mPosition;

		// public orientation
		Quaternion mOrientation;
    };
}
#endif
