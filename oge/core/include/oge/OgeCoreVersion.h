/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_CORE_VERSION_H__
#define __OGE_CORE_VERSION_H__

namespace oge
{
    #define OGE_CORE_VERSION_MAJOR 0
    #define OGE_CORE_VERSION_MINOR 5
    #define OGE_CORE_VERSION_PATCH 7
    #define OGE_CORE_VERSION_REVISION 225
    #define OGE_CORE_VERSION_NUMBER "0.5.7"
    #define OGE_CORE_VERSION_NAME "Bile"
    #define OGE_CORE_VERSION_FULL  ((OGE_CORE_VERSION_MAJOR << 16) | (OGE_CORE_VERSION_MINOR << 8) | OGE_CORE_VERSION_PATCH)

    #define OGE_COPYRIGHT "Copyright (c) 2006-2009 The OGE Team"
    #define OGE_ABOUT     "The Open Game Engine (OGE) is a project to create \
                  a fully featured game engine, including graphics, physics, \
                  input, networking, scripting and much more.\
                  See www.opengameengine.org for more information"

    #define OGE_TEAM_MEMBERS "Steven 'lazalong' GAY, Christopher JONES, Mehdi TOGHIANIFAR"
    #define OGE_OLD_TEAM_MEMBERS "Alkis LEKAKIS"
    #define OGE_CONTRIBUTORS "Roger 'fraggleonlinux' ZOELLNER"

    #if defined(_DEBUG)
    #    define OGE_CORE_BUILD "release"
    #elif defined(NDEBUG)
    #    define OGE_CORE_BUILD "debug"
    #else
    #    define OGE_CORE_BUILD "Error: neither _DEBUG nor NDEBUG defined!"
    #endif

} // namespace
#endif
