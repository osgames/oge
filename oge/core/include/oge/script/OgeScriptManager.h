/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_SCRIPTMANAGER_H__
#define __OGE_SCRIPTMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/system/OgeSystem.h"
#include "oge/input/OgeInputEvent.h"
#include "oge/script/OgeScriptSceneManager.h"

namespace oge
{
    class OGE_CORE_API ScriptManager : public System
    {
    private:
        static ScriptManager* mManager;

    protected:
        /// Active script scene manager
        ScriptSceneManager* mSceneManager;
        /// Contains the scene managers & their factories
        SceneManagerEnumerator* mSceneManagerEnumerator;

    public:
        ScriptManager(const String& name, int systemInitOrdering = 10, int systemRunOrdering = 40);
        virtual ~ScriptManager();

        /**
         * Returns the manager
         * @note The script manager is a singleton so this method is equivalent
         *       to getSingletonPtr() however the instantiation i.e. ScriptManagerLua
         *       is also a singleton hence getManager() is to avoid name conflict.
         * @see getSingletonPtr() which returns the actual implementation
         */
        inline static ScriptManager* getManager() { return mManager; }

        virtual bool initialise();
        /**
         * Notifies the scene manager that we have ticked and 
		 * the time of the tick.
		 * @param currentTime The tick time
         */
		virtual void postTick(double currentTime);

        /**
         * Notifies the scene manager that we are about to tick 
		 * the time of the tick.
		 * @param currentTime The tick time
         */
		virtual void preTick(double currentTime);
        /// @see System::tick()
        virtual void tick(double currentTime) {}

        virtual void createDefaultSceneManager();

        /// Creates a scene manager of type 'type' named 'name'
        SceneManager* createSceneManager(const String& name, const String& type);
        SceneManagerEnumerator* getSceneManagerEnumerator() { return mSceneManagerEnumerator; } 

        /// Returns the active scene manager
        inline ScriptSceneManager* getActiveSceneManager() { return mSceneManager; }
        inline void setActiveSceneManager(ScriptSceneManager* sm) { assert(sm); mSceneManager = sm; }

        /**
         * Register an component template.
         * The component template will also be registerd in the object mgr
         *
         * @note You need to register your template before the method
         * createObject() can be used to create an instance of an object.
         *
         * @param componentTemplate A component template
         */
        void registerComponentTemplate(ComponentTemplate* componentTemplate);
        /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        virtual const String getLibrariesVersion() { return "Script: No special library used.\n"; }

    };
}

#endif // __OGE_SCRIPTMANAGER_H__
