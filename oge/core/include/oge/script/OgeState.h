/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_STATE_H__
#define __OGE_STATE_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/serialisation/OgeSerialisable.h"

namespace oge
{
    /**
     * Interface for classes needing to implement states
     *
     * @author Steven 'lazalong' GAY
     */
    class OGE_CORE_API State
    {
    protected:
        String mName;
        bool mActive;

    public:
        State(const String& name);
        virtual ~State();

		/**
		 * Activate this state
		 * @return true on successful activation
		 */
        virtual bool activate();

		/**
		 * Deactivate this state
		 * @return true on successful deactivation
		 */
        virtual bool deactivate();

		/**
		 * Get the name of this state
		 * @return the name of this state
		 */
		const String& getName() { return mName; }

		/**
		 * Tick function, called every tick
		 * @param deltaTime The elapsed time since the last tick
		 */
        virtual void tick( double deltaTime );

		/**
		 * Process incoming messages - should this be here?
		 * @param message The incoming message
		 */
		virtual void _processMessage(const Message& message) {}
    };

	typedef Poco::SharedPtr<State, oge::ReferenceCounter> StatePtr;
}
#endif // __OGE_STATE_H__
