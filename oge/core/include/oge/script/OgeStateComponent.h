/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_STATECOMPONENT_H__
#define __OGE_STATECOMPONENT_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/object/OgeComponent.h"
#include "oge/serialisation/OgeSerialisable.h"
#include "oge/containers/OgeMap.h"
#include "oge/script/OgeState.h"

namespace oge
{
    /**  
     * The 'StateComponent' type component which is the base component used
     * for behavior.
     *
     * This component is in fact an interface: most of the methods must be
     * implemented by the user.
     *      
     * @note The state component is managed by the ScriptManager hence 
     *       you can't have a state & a script component at the same time
     *       they are exclusive.
     *
     * @note For more complex behavior it best to use a ScriptComponent or an 
     *       AIComponent.
     *
     * @author Steven 'lazalong' GAY
     */
    class OGE_CORE_API StateComponent : public Component
    {
        friend class StateComponentTemplate;
    private:
        typedef Map<std::map<String, StatePtr>, String, StatePtr> StateMap;

        // --- Game state related -----
        StateMap mStates;
        StatePtr mState;
        StatePtr mNextState;

    public:
        virtual ~StateComponent();

        /// (Re)set the default values
        virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
        virtual void _destroy(SceneManager* sceneManager);
        virtual void _activate(bool activate, SceneManager* sceneManager);
        virtual UpdateRate _getUpdateRate() const;
        virtual void _update(double deltaTime);

        /** Implement Serialisable methods
         * @note Actually the serialisation methods read/write the whole object.
         * @todo Only the differences from the object template should be serialised.
         */
        virtual void write(Serialiser* serialiser);
        virtual void read(Serialiser* serialiser);
        virtual bool fixup();

        // --- Specific methods ---
        /**
         * @note The state added will not be destroyed by the StateComponent
         *       this is the responsability of the class that created it.
         */
        bool addState(StatePtr state);
        /**
         * Remove a state EXCEPT if it is the active one
         */
        void removeState(const String& name);
        void removeAllStates();
        StatePtr getState(const String& stateid);
        bool setNextState(const String& name);    

        String getCurrentStateName();
        StatePtr getCurrentState();
        
        void swapState();
        
		/**
		 * Process a message - delegated to the current state, if any
		 * @param message The incoming message
		 */
        virtual void _processMessage(const Message& message);

    protected:
        StateComponent(const ComponentType& type, const ComponentType& family);
    };

    /**
     * Builder that is used to create State Components based on a template.
     *
     * @author Steven 'lazalong' GAY
     */
    class OGE_CORE_API StateComponentTemplate : public ComponentTemplate
    {
    public:
        StateComponentTemplate()
        {
            // Note Script and not State as we don't have a state managers!
            // This also means that you can't have a Script & State component
            // at the same time.
// TODO replace by "State"
            setFamily("Script"); // NOTE: only in the base component of a family!
            setType("StateComponent");
        }
        /// Create the component
        virtual Component* createComponent()
        {
            StateComponent *comp = new StateComponent( getType(), getFamily() );
            return comp;
        }
    };

}
#endif // __OGE_STATECOMPONENT_H__
