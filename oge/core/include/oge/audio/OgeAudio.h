/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_AUDIO_H__
#define __OGE_AUDIO_H__

#include "oge/OgeCorePrerequisites.h"

namespace oge
{
    /**
     * Base audio class
     *
     * @note This is a dummy instanciation
     *
     * @author Steven 'lazalong' GAY
     */
    class OGE_CORE_API Audio
    {

    public:
        //Audio(AudioFactory* creator, const String& filename);
        virtual ~Audio();

        ///// Returns the factory having created this audio
        //AudioFactory* getCreator() const { return mCreator; }

        ///** 
        // * Relase the AudioSource - this method is used as callback from
        // * the context which is testing which sources are AL_STOPPED and
        // * in this case will deactivate it.
        // *
        // * @note be careful in using this method be sure to understand its purpose
        // * @note Consider making it protected
        // */
        //inline void inactivateSource() { mSource = 0; }

        /**
         * Must be called it will prepare the audio.
         *  That is it will set the AL source
         *  and set the source position, velocity, etc as zero.
         */
        virtual bool reset() { return true; }
        virtual bool load() { return true; }

        /**
         * Will play an audio.
         * In case it is already playing and the restart wasn't forced 
         * then it will simply return.
         * If the restart is set for this audio the playing audio will
         * first be stopped and the audio played from start again.
         * @see setRestart()
         */
        virtual void play();
        virtual void pause();
        virtual void stop();

        /**
         * This method must usually be called only for streamed audios
         */
        virtual void update() {}

        ///// TODO make this a full debug method : switch AL_OUT_OF_MEMORY, etc.
        //inline bool checkALerror() { return alGetError() != AL_NO_ERROR; }
        //bool isPlaying();

        ///// If true will force the audio to restart when play() is called
        //void setRestart( bool restart ) { mRestart = restart; }

        //virtual void setAudio( Vector3 position, Vector3 velocity, 
        //   Vector3 direction, float maxDistance, float referenceDistance,
        //   float minGain, float maxGain, float rolloff );
    
        //virtual void setLocation( Vector3 position, Vector3 velocity,
        //    Vector3 direction );
        //virtual void setPosition( Vector3 position );

        //virtual void setPitch( Real pitch = 1.0 );
        //virtual void setGain( Real gain = 1.0 );
        //virtual void setMaxDistance( Real distance = 3000.0 );
        //virtual void setRolloffFactor( Real rolloffFactor = 1.0 );
        //virtual void setReferenceDistance( Real refDistance = 150.0 );
        //virtual void setDistanceValues( Real maxDistance, Real rolloffFactor, Real refDistance );
        //virtual void setMaxGain( Real max = 1.0 );
        //virtual void setMinGain( Real min = 0.0 );
        //virtual void setConeOuterGain( Real coneOuterGain = 0.0 );
        //virtual void setConeInnerAngle( Real coneInnerAngle  = 360.0 );
        //virtual void setConeOuterAngle( Real coneOuterAngle = 360.0 );
        //virtual void setRelativeToListener( bool relative = false);
        //virtual void setLoop( bool loop = false);
        //virtual void setSecondOffset( Real offset = 0.0 );
        //virtual Real getSecondOffset();

    protected:
        Audio() {}
    };

    /// Shared ptr to an Audio instance - will delete itself when no one reference it anymore
    typedef Poco::SharedPtr<Audio, oge::ReferenceCounter> AudioPtr;

    /**
     * Factory creating AudioWav instances
     */
    class OGE_CORE_API AudioFactory
    {
    protected:
        std::vector<String> mExtensions;
    public:
        virtual ~AudioFactory() { mExtensions.clear(); }

        std::vector<String> getExtensions() { return mExtensions; }

        virtual AudioPtr createAudio(const String& filename, 
            bool streaming, unsigned int nbBuffers) = 0;

    };
}

#endif // __OGE_AUDIO_H__
