/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_AUDIOMANAGER_H__
#define __OGE_AUDIOMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/system/OgeSystem.h"
#include "oge/audio/OgeAudio.h"

namespace oge
{
    class OGE_CORE_API AudioManager : public System
    {
    private:
        static AudioManager* mManager;

    public:
        AudioManager(const String& name, int systemInitOrdering = 10, int systemRunOrdering = 90);
        virtual ~AudioManager();
        /**
         * Returns the manager
         * @note The graphics manager is a singleton so this method is equivalent
         *       to getSingletonPtr() however the instantiation i.e. GraphicsManagerOGRE
         *       is also a singleton hence getManager() is to avoid name conflict.
         * @see getSingletonPtr() which returns the actual implementation
         */
        inline static AudioManager* getManager() { return mManager; }

        /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        virtual const String getLibrariesVersion() { return "Audio: No special library used.\n"; }

        /**
         * Creates an audio in the active context.
         * This method doesn't load the file!
         *
         * @see Audio::load() & Audio::play()
         * @returns SharedPtr to the audio or zero if a problem occurred.
         * @param The name of the audio file
         * @param Load as stream or not (default = false = loaded in memory buffer)
         * @param Nb of buffers to use when streaming (2 by default, 3 or 4 can increase audio quality)
         * @note The nb buffers is not used when not streaming.
         *
         * @note This implementation return an empty ptr.
         *        You need to override this method. 
         */
        virtual AudioPtr createAudio( const String& filename, bool streaming = false, 
            unsigned int nbBuffers = 2 ) { return AudioPtr(); }


    };
}

#endif // __OGE_AUDIOMANAGER_H__
