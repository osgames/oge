/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_PHYSICSCOMPONENT_H__
#define __OGE_PHYSICSCOMPONENT_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/object/OgeComponent.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"

namespace oge
{
    /**
     * Structure to identify the component during collisions
     */
    struct CollisionData
    {
        ObjectId mObjectId;
        String mType;
    };


    /**  
     * Interface for the 'PhysicsComponent' classes
     *
     * @author Steven 'lazalong' GAY
     */
    class OGE_CORE_API PhysicsComponent : public Component
    {
    protected:
        // Should this be added to CollisionData?
        /** Bitwise group 
         * @note We use a short because Bullet only support short!
         */
        short mCollisionGroup;
        /** Bitwise mask 
         * @note We use a short because Bullet only support short!
         */
        short mCollisionMask;
    public:
        /**
         * Shape Types: box sphere convex, cylinder, cone, capsule, 
         *              compound, triangle mesh
         * The box should be the default shape
         * @note Those shapes are based on the Bullet shape types. 
         */
        enum ShapeType
        {
            OGE_BOX = 0, // default
            OGE_SPHERE,
            OGE_CONVEX,
            OGE_CYLINDER,
            OGE_CONE,
            OGE_CAPSULE,
            OGE_COMPOUND,
            OGE_MESH,
            OGE_TERRAIN_HEIGHTFIELD // oge custom, not bullet
        } mShapeType;

    public:
        virtual ~PhysicsComponent()
        {}

        virtual const Vector3& getAngularVelocity() const = 0;
        const short getCollisionGroup() { return mCollisionGroup; }
        const short getCollisionMask() { return mCollisionMask; }
        virtual const Vector3& getDimensions() const = 0;
		virtual const Real getDrag() const = 0;
        virtual const Real getMaxAngularVelocity() const = 0;
		virtual const Real getMaxVelocity() const = 0;
        virtual Vector3 getEulerOrientation() const = 0;
        virtual const Quaternion& getOrientation() const = 0;
        virtual const Vector3& getPosition() const = 0;
        virtual const Vector3& getScale() const = 0;
        virtual const Vector3& getVelocity() const = 0;

        virtual void setAngularVelocity(const Vector3& angularVelocity) = 0;
        void setCollisionGroup(const short group) { mCollisionGroup = group; }
        void setCollisionMask(const short mask) { mCollisionMask = mask;}

		/**
         * @note Must be called on components that their owner objects has either
         *        Graphics or Physics components.
         */
        virtual void setDimensions(const Vector3& dimensions) = 0;
		virtual void setDrag(const Real drag) = 0;
		virtual void setMaxAngularVelocity(const Real maxAngularVelocity) = 0;
		virtual void setMaxVelocity(const Real maxVelocity) = 0;
        virtual void setOrientation(const Quaternion& orientation) = 0;
        /**
         * @note The object is first reset to an upright(?) position
         *        and then yaw (around y axis), pitch (x) and roll (z)
         *        are applied in this order.
         */
        virtual void setOrientationEuler(const Vector3& orientation) = 0;
        virtual void setPosition(const Vector3& position) = 0;
        virtual void setPositionOrientation(const Vector3& position, const Quaternion& orientation) = 0;
        virtual void setScale(const Vector3& scale) = 0;
		virtual void setVelocity(const Vector3& velocity) = 0;

        virtual void _processMessage(const Message& message) = 0;

        void _setShapeType(ShapeType shapeType) { mShapeType = shapeType; }
    protected:
        inline PhysicsComponent(const ComponentType& type, const ComponentType& family)
            : Component(type, family), mCollisionGroup(0), mCollisionMask(0),
            mShapeType(OGE_BOX)
        {}

        virtual void _setAngularVelocityByMessage(const Message& message) = 0;
        virtual void _setDimensionsByMessage(const Message& message) = 0;
        virtual void _setEulerOrientationByMessage(const Message& message) = 0;
        virtual void _setOrientationByMessage(const Message& message) = 0;
        virtual void _setPositionByMessage(const Message& message) = 0;
        virtual void _setPositionOrientationByMessage(const Message& message) = 0;
        virtual void _setScaleByMessage(const Message& message) = 0;
        virtual void _setVelocityByMessage(const Message& message) = 0;
    };

    /**  
     * The 'PhysicsComponent' type component which is the base component used
     * for static world object it only contains: 
     *      position, scale and orientation 
     *
     * @author Steven 'lazalong' GAY
     */
    class OGE_CORE_API PhysicsComponentDefault : public PhysicsComponent
    {
        friend class PhysicsComponentTemplateDefault;

    protected:
        Vector3    mAngularVelocity;
		Real       mMaxAngularVelocity;
        Vector3    mPosition;
        Quaternion mOrientation;
        Vector3    mDimensions;
        Vector3    mScale;
        Vector3    mVelocity;
		Real       mMaxVelocity;
		Real	   mDrag;
    public:
        virtual ~PhysicsComponentDefault()
        {}

        /// (Re)set the default values
        virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
        virtual void _destroy(SceneManager* sceneManager);
        virtual void _activate(bool activate, SceneManager* sceneManager);
        virtual UpdateRate _getUpdateRate() const;
        virtual void _update(double deltaTime);

        /** Implement Serialisable methods
         * @note Actually the serialisation methods read/write the whole object.
         * @todo Only the differences from the object template should be serialised.
         */
        virtual void write(Serialiser* serialiser);
        virtual void read(Serialiser* serialiser);
        virtual bool fixup();

        virtual void setAngularVelocity(const Vector3& angularVelocity);
        /**
         * @note Must be called on components that their owner objects has either
         *        Graphics or Physics components.
         */
        virtual void setDimensions(const Vector3& dimensions);
		virtual void setDrag(const Real drag);
		virtual void setMaxAngularVelocity(const Real maxAngularVelocity);
		virtual void setMaxVelocity(const Real maxVelocity);
        virtual void setOrientation(const Quaternion& orientation);
        /**
         * @note The object is first reset to an upright(?) position
         *        and then yaw (around y axis), pitch (x) and roll (z)
         *        are applied in this order.
         */
        virtual void setOrientationEuler(const Vector3& orientation);
        virtual void setPosition(const Vector3& position);
        virtual void setPositionOrientation(const Vector3& position, const Quaternion& orientation);
        virtual void setScale(const Vector3& scale);
        virtual void setVelocity(const Vector3& velocity);

        /** TODO
        virtual void moveRelative(Real x, Real y, Real z);
        virtual void yaw(const Radian& angle);
        virtual void pitch(const Radian& angle);
        virtual void roll(const Radian& angle);
        virtual void resetOrientation();
        ... see: Ogre::Camera */

        inline const Vector3& getAngularVelocity() const { return mAngularVelocity; }
        /**
         * @note Must be called on components that their owner objects has either
         *        Graphics or Physics components.
         */
        virtual const Vector3& getDimensions() const;
		inline const Real getDrag() const { return mDrag; }
        virtual Vector3 getEulerOrientation() const;
		inline const Real getMaxAngularVelocity() const { return mMaxAngularVelocity; }
		inline const Real getMaxVelocity() const { return mMaxVelocity; }
        inline const Quaternion& getOrientation() const { return mOrientation; };
        inline const Vector3& getPosition() const { return mPosition; }
        inline const Vector3& getScale() const { return mScale; };
        inline const Vector3& getVelocity() const { return mVelocity; }

        virtual void _processMessage(const Message& message);

    protected:
        inline PhysicsComponentDefault(const ComponentType& type, const ComponentType& family)
            : PhysicsComponent(type, family)
			, mAngularVelocity(Vector3::ZERO)
            , mPosition(Vector3::ZERO)
            , mOrientation(Quaternion::IDENTITY)
            , mScale(Vector3::UNIT_SCALE)
            , mVelocity(Vector3::ZERO)
            , mDrag(0)

        {
			Real max = std::numeric_limits<Real>::max();

			mMaxAngularVelocity = max;
			mMaxVelocity = max;
		}

        virtual void _setAngularVelocityByMessage(const Message& message);
        virtual void _setDimensionsByMessage(const Message& message);
		virtual void _setDragByMessage(const Message& message);
        virtual void _setEulerOrientationByMessage(const Message& message);
		virtual void _setMaxAngularVelocityByMessage(const Message& message);
		virtual void _setMaxVelocityByMessage(const Message& message);
        virtual void _setOrientationByMessage(const Message& message);
        virtual void _setPositionByMessage(const Message& message);
        virtual void _setPositionOrientationByMessage(const Message& message);
        virtual void _setScaleByMessage(const Message& message);
        virtual void _setVelocityByMessage(const Message& message);
    };

    /**
     * Builder that is used to create Graphics Components based on a template.
     *
     * @author Steven 'lazalong' GAY
     */
    class OGE_CORE_API PhysicsComponentTemplateDefault : public ComponentTemplate
    {
    public:
        PhysicsComponentTemplateDefault()
        {
            setFamily("Physics"); // NOTE: only in the base component of a family!
            setType("Location");
        }
        /// Create the component
        virtual Component* createComponent()
        {
            return new PhysicsComponentDefault( getType(), getFamily() );  
        }
    };
} // namespace
#endif
