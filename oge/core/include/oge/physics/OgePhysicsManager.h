/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_PHYSICSMANAGER_H__
#define __OGE_PHYSICSMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/system/OgeSystem.h"
#include "oge/input/OgeInputEvent.h"
#include "oge/physics/OgePhysicsSceneManager.h"

namespace oge
{
    class OGE_CORE_API PhysicsManager : public System
    {
    private:
        static PhysicsManager* mManager;
		
    protected:
        /// Active graphics SceneManager
        PhysicsSceneManager* mSceneManager;
        /// Contains the scene managers & their factories
        SceneManagerEnumerator* mSceneManagerEnumerator;

    public:
        PhysicsManager(const String& name, int systemInitOrdering = 10, int systemRunOrdering = 60);
        virtual ~PhysicsManager();

        /**
         * Returns the manager
         * @note The manager is a singleton so this method is equivalent
         *       to getSingletonPtr() however the instantiation i.e. PhysicsManagerBullet
         *       is also a singleton hence getManager() is to avoid name conflict.
         * @see getSingletonPtr() which returns the actual implementation
         */
        inline static PhysicsManager* getManager() { return mManager; }

        /// @see System::tick()
        virtual bool initialise();
        /**
         * Notifies the scene manager that we have ticked and 
		 * the time of the tick.
		 * @param currentTime The tick time
         */
		virtual void postTick(double currentTime);

        /**
         * Notifies the scene manager that we are about to tick 
		 * the time of the tick.
		 * @param currentTime The tick time
         */
		virtual void preTick(double currentTime);
        virtual void tick(double currentTime) {}

        virtual void createDefaultSceneManager();

        /// Creates a scene manager of type 'type' named 'name'
        SceneManager* createSceneManager(const String& name, const String& type);
        SceneManagerEnumerator* getSceneManagerEnumerator() { return mSceneManagerEnumerator; } 

        /// Returns the active scene manager
        inline PhysicsSceneManager* getActiveSceneManager() { return mSceneManager; }
        inline void setActiveSceneManager(PhysicsSceneManager* sm) { assert(sm); mSceneManager = sm; }

         /**
         * Returns the component with id in the active scene manager, 0 otherwise.
         * @note returns a 'ptr* const' aka you can't change the pointer
         */
        inline Component* const getComponent(const ObjectId& id, const ComponentType& family)
        {
            return mSceneManager->getComponent(id, family);
        }

        /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        virtual const String getLibrariesVersion() { return "Physics: No special library used.\n"; }
   };
}

#endif // __OGE_PHYSICSMANAGER_H__
