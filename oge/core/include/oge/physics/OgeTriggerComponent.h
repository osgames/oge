/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_TRIGGERCOMPONENT_H__
#define __OGE_TRIGGERCOMPONENT_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/object/OgeComponent.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"

namespace oge
{
    /**  
     * Interface for the 'TriggerComponent' classes
     *
TODO
     * 3d distance (aka spherical)
     * "2d distance" (aka if you approach from above nothing happens but if you approach on the floor it will trigger at a distance).
     * infinite plan
     * aabb
     * mesh-based (for example a round stargate)

and have features like:

     * going-in & out event, 
     * can send regularly events while IN the zone
     * active once,
     * during a pre-set duration,
     * triggered after a defined time,
     * active only when inside the zone

     * @author Steven 'lazalong' GAY
     */
    class OGE_CORE_API TriggerComponent : public Component
    {
    public:
        /**
         * Shape Types: box sphere convex, cylinder, cone, capsule, 
         *              compound, triangle mesh
         * The box should be the default shape
         * @note Those shapes are based on the Bullet shape types. 
         */
        enum ShapeType
        {
            OGE_BOX = 0, // default
            OGE_SPHERE,
            OGE_CONVEX,
            OGE_CYLINDER,
            OGE_CONE,
            OGE_CAPSULE,
            OGE_COMPOUND,
            OGE_MESH,
            OGE_TERRAIN_HEIGHTFIELD // oge custom, not bullet
        } mShapeType;

        enum EventType
        {
            /// Event send when object enter the trigger zone
            OnEnter = 0,
            /// Event send when object is inside the trigger zone
            OnInside,
            /// Event send when object exit the trigger zone
            OnExit,
            /// Used to verify range of event type
            OnLast
        };

        /**
         * Callback used to trigger an event
		 * 1st param is object id for this trigger component
		 * 2nd param is object id for the object that triggered the event
         */
        typedef fastdelegate::FastDelegate2<ObjectId, ObjectId, void> Callback;

    private:
        std::map<EventType, Callback> mCallbacks;
        /// Store the objects in the zone
        std::set<ObjectId> mObjects;
        /// Store the object in the zone at this cycle (cleared after each _update())
        std::set<ObjectId> mObjectsNow;

    public:
        virtual ~TriggerComponent()
        {}

        // --- Specific methods to the Trigger Family ---

        virtual void setPosition(const Vector3& position) = 0;
        virtual void setScale(const Vector3& scale) = 0;
        /**
         * @note Must be called on components that their owner objects has either
         *        Graphics or Trigger components.
         */
        virtual void setDimensions(const Vector3& dimensions) = 0;
        virtual void setOrientation(const Quaternion& orientation) = 0;
        /**
         * @note The object is first reset to an upright(?) position
         *        and then yaw (around y axis), pitch (x) and roll (z)
         *        are applied in this order.
         */
        virtual void setOrientationEuler(const Vector3& orientation) = 0;
        virtual void setPositionOrientation(const Vector3& position, const Quaternion& orientation) = 0;

        virtual const Vector3& getPosition() const = 0;
        virtual const Vector3& getScale() const = 0;
        /**
         * @note Must be called on components that their owner objects has either
         *        Graphics or Trigger components.
         */
        virtual const Vector3& getDimensions() const = 0;
        virtual const Quaternion& getOrientation() const = 0;
        virtual Vector3 getEulerOrientation() const = 0;

        virtual void _processMessage(const Message& message) = 0;

        void _setShapeType(ShapeType shapeType) { mShapeType = shapeType; }

        // -------- Callbacks --------------
        /**
         * @note Callbacks are called by the scheduler.
         */
        void addCallback( EventType type, const Callback& callback)
        {
            LOGEC( type > OnLast, "Wrong event type: this callback will never be called!");
            mCallbacks[type] = callback;
        }
        void removeAllCallbacks()
        {
            mCallbacks.clear();
        }
        bool isCallbackPresent(EventType eventType)
        {
            return mCallbacks.end() != mCallbacks.find(eventType);
        }

    protected:
        inline TriggerComponent(const ComponentType& type, const ComponentType& family)
            : Component(type, family), mShapeType(OGE_BOX)
        {}

        virtual void _setPositionByMessage(const Message& message) = 0;
        virtual void _setScaleByMessage(const Message& message) = 0;
        virtual void _setDimensionsByMessage(const Message& message) = 0;
        virtual void _setOrientationByMessage(const Message& message) = 0;
        virtual void _setEulerOrientationByMessage(const Message& message) = 0;
        virtual void _setPositionOrientationByMessage(const Message& message) = 0;

        /**
         * Triggers callbacks depending on the object history in the zone.
         * If entering the zone an "OnEnter" event is send, when inside
         * an "OnInside" and when leaving (aka the update doesn't detect 
         * the object anymore) an event "OnExit" is send. 
         *
         * @note That on exit the history is erased. 
         *
         * @note This method should only be called in the _update() method
         */
        virtual void triggerCallbacks();

        /**
         * Remember an object present in the zone during this update()
         * @param The object id that triggered the event
         *
         * @note This method should only be called in the _update() method
         */
        virtual inline void rememberObject(ObjectId id)
        {
            mObjectsNow.insert(id);
        }

        /**
         * Triggers a specific callback
         *
         * @param The object id that triggered the event
         * @param The event type (OnEnter, ...)
         *
         * @note This method should only be called in the _update() method
         */
        virtual void triggerCallback(ObjectId id, EventType type);

    };


// TODO Put in its own file?

    /**  
     * The 'TriggerComponent' type component which is the base component used
     * for triggers it only contains: 
     *      position, scale and orientation 
     *
     * @author Steven 'lazalong' GAY
     */
    class OGE_CORE_API TriggerComponentDefault : public TriggerComponent
    {
        friend class TriggerComponentTemplateDefault;

    private:
        Vector3    mPosition;
        Quaternion mOrientation;
        Vector3    mDimensions;
        Vector3    mScale;

    public:
        virtual ~TriggerComponentDefault()
        {}

        /// (Re)set the default values
        virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
        virtual void _destroy(SceneManager* sceneManager);
        virtual void _activate(bool activate, SceneManager* sceneManager);
        virtual UpdateRate _getUpdateRate() const;
        virtual void _update(double deltaTime);

        /** Implement Serialisable methods
         * @note Actually the serialisation methods read/write the whole object.
         * @todo Only the differences from the object template should be serialised.
         */
        virtual void write(Serialiser* serialiser);
        virtual void read(Serialiser* serialiser);
        virtual bool fixup();

        // --- Specific methods to the Trigger Family ---

        virtual void setPosition(const Vector3& position);
        virtual void setScale(const Vector3& scale);
        /**
         * @note Must be called on components that their owner objects has either
         *        Graphics or Trigger components.
         */
        virtual void setDimensions(const Vector3& dimensions);
        virtual void setOrientation(const Quaternion& orientation);
        /**
         * @note The object is first reset to an upright(?) position
         *        and then yaw (around y axis), pitch (x) and roll (z)
         *        are applied in this order.
         */
        virtual void setOrientationEuler(const Vector3& orientation);
        virtual void setPositionOrientation(const Vector3& position, const Quaternion& orientation);

        inline const Vector3& getPosition() const { return mPosition; }
        inline const Vector3& getScale() const { return mScale; };
        /**
         * @note Must be called on components that their owner objects has either
         *        Graphics or Trigger components.
         */
        virtual const Vector3& getDimensions() const;
        inline const Quaternion& getOrientation() const { return mOrientation; };
        virtual Vector3 getEulerOrientation() const;

        virtual void _processMessage(const Message& message);

    protected:
        inline TriggerComponentDefault(const ComponentType& type, const ComponentType& family)
            : TriggerComponent(type, family)
            , mPosition(Vector3::ZERO), mOrientation(Quaternion::IDENTITY), mScale(Vector3::UNIT_SCALE)
        {}

        virtual void _setPositionByMessage(const Message& message);
        virtual void _setScaleByMessage(const Message& message);
        virtual void _setDimensionsByMessage(const Message& message);
        virtual void _setOrientationByMessage(const Message& message);
        virtual void _setEulerOrientationByMessage(const Message& message);
        virtual void _setPositionOrientationByMessage(const Message& message);
    };

    /**
     * Builder that is used to create Trigger Components based on a template.
     *
     * @author Steven 'lazalong' GAY
     */
    class OGE_CORE_API TriggerComponentTemplateDefault : public ComponentTemplate
    {
    public:
        TriggerComponentTemplateDefault()
        {
            setFamily("Trigger"); // NOTE: only in the base component of a family!
            setType("TriggerComponent");
        }
        virtual Component* createComponent()
        {
            return new TriggerComponentDefault( getType(), getFamily() );  
        }
    };
} // namespace
#endif
