/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_SCENEMANAGER_H__
#define __OGE_SCENEMANAGER_H__

#include "oge/OgeCorePrerequisites.h"
#include "oge/object/OgeComponent.h"
#include "oge/containers/OgeMap.h"
#include "oge/object/OgeObjectManager.h"
#include "oge/task/OgeDelegateTask.h"
#include "oge/containers/OgeQueue.h"
#include "oge/OgeClock.h"

namespace oge
{
    /**
     * Structure containing informations about a scene manager. 
     *
     * @author Steven 'lazalong' Gay
     */
    struct SceneManagerMetaData
    {
        /// Name of the scene manager (used only with messages)
        String name;
        /// A globally unique string identifying the scene manager type
        String type;
        /// A globally unique string identifying in which family the scene manager belongs
        String family;
        /// A text description of the scene manager
        String description;
        /** 
         * A mask describing which sorts of scenes this manager can handle
         * See Ogre::DefaultSceneManagerFactory::FACTORY_TYPE_NAME  
         *  
         *  sceneTypeMask         typeName                           Class
         *  ST_GENERIC            DefaultSceneManager   
         *  ST_INTERIOR           BspSceneManager                    BspSceneManager
         *  ST_GENERIC            OctreeSceneManager                 OctreeSceneManager
         *  ST_EXTERIOR_CLOSE     TerrainSceneManager                TerrainSceneManager
         *  ST_EXTERIOR_REAL_FAR  PagingLandScapeOctreeSceneManager  Paging_Scene_Manager
         *  0xFFFF                PCZSceneManager                    PCZSceneManager
         * 
         * @note 0xFFFF means all types are supported !? Not sure that this is not a bug
         * @note ST_GENERIC == Generic scene manager (Octree if Plugin_OctreeSceneManager
         *       is loaded, DotScene if Plugin_DotSceneManager is loaded)
         */
        String typeName;
        unsigned int sceneTypeMask;
        /// Flag indicating whether world geometry is supported (see Ogre3D)
        bool worldGeometrySupported;
    };

    /** 
     * Base scene manager class
     * @author Steven 'lazalong' Gay, Mehdi Toghianifar
     */
    class OGE_CORE_API SceneManager
    {
    protected:
        /// Instance name
        mutable String mName;
        /// Meta data of this scene manager type
        mutable SceneManagerMetaData mMetaData;

        typedef std::set<Component*> ComponentSet;
        typedef std::set<Component*>::iterator ComponentIter;
        // NEXT: Separate by family? 
        // TODO: list by object id?
        ComponentSet mUpdatableComponents[Component::NUM_UPDATE_RATES];

        typedef std::multimap<ObjectId, Component*> ComponentMap;
        typedef std::multimap<ObjectId, Component*>::iterator ComponentMapIter;
        typedef std::pair<ObjectId, Component*> ComponentPair;
        // See http://codeguru.earthweb.net/forum/showthread.php?t=297891
        typedef std::pair<ComponentMapIter, ComponentMapIter> ComponentMapPairIter;
        ComponentMap mComponents;

        ///
        Clock mClock;

        typedef ConcurrentQueue<Message> MessageQueue;
        MessageQueue mMessageQueue;
        size_t mLastProcessedMessagesCount;
        double mLastMessageProcessingTime;

    public:
        enum SM_METHOD_IDS
        {
            CLEAR_SCENE = 0,
            LAST = CLEAR_SCENE
        };

        SceneManager(const String& name)
            : mName(name), mLastProcessedMessagesCount(0), mLastMessageProcessingTime(0)
        {}
        virtual ~SceneManager() {}

        inline const String& getName() const { return mName; }
        inline const String& getType() const { return mMetaData.type; }
        inline const String& getFamily() const { return mMetaData.family; }

        /**
         * Internal method used once to initialise the metadata, must be implemented
         *  @note Also used by factories to know which type of sm it will create.
         */
        virtual void initMetaData() const = 0;
        virtual void init() = 0;

        /**
         * WIP Delete all component directly (not via deleteObject())
         * @note ObjectSceneManager::clearScene() should be called
         */
        virtual void clearScene();
		/**
		 * Called right before our manager ticks.
		 * @param currentTime The tick time
		 */
		virtual void preTick(double currentTime);
		/**
		 * Process an immediate message.  This will have all
		 * scene managers process this message immediately
		 * @param msg The message to process
		 */
		virtual bool processMessageImmediate(const Message& msg);
		/**
		 * Called right after our manager ticks.
		 * @param currentTime The tick time
		 */
		virtual void postTick(double currentTime);
		/** 
		 * Called by ObjectSceneManager each time ObjectManager ticks
		 * @param deltaTime The time since the last call to update
		 */
        virtual void update(double deltaTime);

        /// Adds a component to this scene manager
        bool addComponent(Component* component);

        /**
         * "Intermediary" method in the object creation sequence
         *
         * @note This method shouldn't be called directly
         * @note This method is called by ObjectSceneManager::createObject() and
         *       it will call ComponentFactory::createComponent() via the main manager
         * @see destroyComponent() which should be used to destroy the component
         */
        virtual AsyncBool createComponent(const ObjectId& id, const ComponentType& type,
            const MessageList& params);

        /**
         * @param id the ObjectId
         * @param family the Component family (not the SM type)
         */
        Component* getComponent(const ObjectId& id, const ComponentType& family);

        /**
         * @param id the ObjectId
         * @param family the Component family
		 * @param type the Component type
         */
        Component* getComponent(const ObjectId& id, const ComponentType& family, const ComponentType& type);

        /// Returns the first found only
        Component* getFirstComponent(const ObjectId& id);

        void activateComponent(bool activate, const ObjectId& id);

		/**
		 * Activate or deactivate a specific component by type for a specific object
		 * @param activate Activate or Deactivate
		 * @param id The object id
		 * @param type The component type
		 */
		void activateComponent(bool activate, const ObjectId& id, const ComponentType& type);

        /// This method is thread-safe
        inline void postMessage(const Message& message)
        {
            mMessageQueue.push(message);
        }
        ///
        inline size_t getLastProcessedMessagesCount()
        {
            return mLastProcessedMessagesCount;
        }
        inline double getLastMessageProcessingTime()
        {
            return mLastMessageProcessingTime;
        }

    protected:
        /**
         * Called asynchronously by SceneManager::createComponent()
         * @see SceneManager::createComponent()
         */
        bool _createComponent(const ObjectId& id, const ComponentType& type, 
            const MessageList& params);
        /**
         * Destroy a component with a certain id
         * @note this method is not-thread safe and is used internally
         * @see createComponent()
         * @see SceneManager::destroyComponent()
         */
        bool destroyComponent(const ObjectId id);
        ///
        void registerUpdatableComponent(Component* component);
        /// @see SceneManager::registerUpdatableComponent(Component* component)
        void unregisterUpdatableComponent(Component* component);
        ///
        void updateComponents(double deltaTime);
        /**
         * Process all the messages
         * todo Add some explanations
         */
        void processMessages();
        /**
         * Process a general (without a particular receiver) message... Can be redeclared to...
         * @return True if the message is processd.
         */
        virtual bool processMessage(const Message& message);
    };

    /** 
     * Base factory to instanciate a specific scene manager
     * @author Steven 'lazalong' Gay
     */
    class OGE_CORE_API SceneManagerFactory
    {
    protected:
        String mType;
        bool mAvailable;

    public:
        SceneManagerFactory() : mType("Default"), mAvailable(false) {}
        virtual ~SceneManagerFactory() {}

        const String& getType() const { return mType; }
        /** 
         * @note The factory is not responsible of deleting the instance
         */
        virtual SceneManager* createInstance(const String& name) = 0;
        virtual bool checkAvailability() = 0;
        bool isAvailable() { return mAvailable; }
    };
}
#endif // __OGE_SCENEMANAGER_H__
