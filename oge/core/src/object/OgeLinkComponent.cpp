/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/object/OgeLinkComponent.h"
#include "oge/object/OgeComponent.h"
#include "oge/object/OgeObject.h"
#include "oge/serialisation/OgeSerialiser.h"
#include "oge/object/OgeObjectManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    void LinkComponent::write(Serialiser* serialisePtr)
    {
//#pragma OGE_WARN("TODO/FIXME: The initialiser MsgList of the object should be de-/serialised")
//        serialisePtr->writeString(getMeshName());
    }
    //-------------------------------------------------------------------------
    void LinkComponent::read(Serialiser* serialisePtr)
    {
//        mMeshName = serialisePtr->readString();  TODO: Should be deserialised into a MsgList
    }
    //-------------------------------------------------------------------------
    bool LinkComponent::fixup()
    {
        // no fixup to do
        return true;
    }
    //-------------------------------------------------------------------------
    bool LinkComponent::_reset(const MessageList& params, SceneManager* sceneManager)
    {
        // no fixup to do
        return true;
    }
    //-------------------------------------------------------------------------
    void LinkComponent::_update(double deltaTime)
    {
    }
    //-------------------------------------------------------------------------
    void LinkComponent::_destroy(SceneManager* sceneManager)
    {
        mObjects.clear();
    }
    //-------------------------------------------------------------------------
    void LinkComponent::_activate(bool activate, SceneManager* sceneManager)
    {
    }
    //-------------------------------------------------------------------------
    void LinkComponent::_processMessage(const Message& message)
    {
        std::vector<ObjectId>::iterator iter;
        for (iter = mObjects.begin(); iter != mObjects.end(); ++iter)
        {
            Message messagecopy;
            messagecopy.setReceiver( *iter );
            ObjectManager::getSingleton().postMessage( messagecopy, message.IMMEDIATE );
        }
    }
    //-------------------------------------------------------------------------
    void LinkComponent::addObject(const ObjectId& id)
    {
        if (id.empty())
        {
            LOGI("Invalid ObjectId");
            return;
        }
        if (isPresent(id))
            return;

        mObjects.push_back(id);
    }
    //-------------------------------------------------------------------------
    void LinkComponent::removeObject(const ObjectId& id)
    {
        std::vector<ObjectId>::iterator iter;
        for (iter = mObjects.begin(); iter != mObjects.end(); ++iter)
        {
            if (*iter == id)
            {
                mObjects.erase( iter );
                return;
            }
        }
    }
    //-------------------------------------------------------------------------
    bool LinkComponent::isPresent(const ObjectId& id)
    {
        std::vector<ObjectId>::iterator iter;
        for (iter = mObjects.begin(); iter != mObjects.end(); ++iter)
        {
            if (*iter == id)
                return true;
        }
        return false;
    }
    //-------------------------------------------------------------------------
}
