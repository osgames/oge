/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/object/OgeObject.h"
#include "oge/object/OgeComponent.h"

namespace oge
{
    //-------------------------------------------------------------------------
    //-------------- ObjectTemplate -----------------------------------------------
    //-------------------------------------------------------------------------
    void ObjectTemplate::addComponent(const ComponentType& type)
    {
        if (type.empty())
        {
            LOGW("Can't add an empty component!");
            assert(!type.empty());
            return;
        }

        ComponentTypeListIter iter =
            std::find(mComponentTypes.begin(), mComponentTypes.end(), type);
        if (iter != mComponentTypes.end())
        {
            LOGW("There already is a component of this type: " + type 
                + " in this ObjectTemplate: " + this->getType());
            return;
        }

        mComponentTypes.push_back( type );
    }
    //-------------------------------------------------------------------------


    //-------------------------------------------------------------------------
    //-------------- ObjectFactory ------------------------------------------------
    //-------------------------------------------------------------------------
    ObjectFactory::~ObjectFactory()
    {
        ObjectTemplateIter iter = mObjectTemplateMap.begin();
        for (iter = mObjectTemplateMap.begin(); 
            iter != mObjectTemplateMap.end(); ++iter)
        {
            ObjectTemplate* value = (*iter).second;
            if (value)
                delete value;
        }

        mObjectTemplateMap.clear();

        ComponentTemplateIter iter2 = mComponentsTemplates.begin();
        for (iter2 = mComponentsTemplates.begin(); 
            iter2 != mComponentsTemplates.end(); ++iter2)
        {
            ComponentTemplate* value = (*iter2).second;
            if (value)
                delete value;
        }
        mComponentsTemplates.clear();

        mSceneManagerTypePerComponentType.clear();
    }
    //-------------------------------------------------------------------------
    bool ObjectFactory::registerObjectTemplate( ObjectTemplate* objectTemplate )
    {
        assert(objectTemplate);
        if (objectTemplate == 0)
        {
            LOGW("Can't register an empty template!");
            return false;
        }

        ObjectType type = objectTemplate->getType();

        ObjectTemplateIter it = mObjectTemplateMap.find(type);
        if (it != mObjectTemplateMap.end())
        {
            LOGW("Rejection of a duplicated object template of type: "
                + (String) objectTemplate->getType());
            return false;
        }

        mObjectTemplateMap[type] = objectTemplate;

        return true;
    }
    //-------------------------------------------------------------------------
    ObjectTemplate* ObjectFactory::getObjectTemplate( const ObjectType& type )
    {
       ObjectTemplateIter it = mObjectTemplateMap.find(type);
        if (it == mObjectTemplateMap.end())
            return 0;

        return it->second;
    }
    //-------------------------------------------------------------------------
    const String& ObjectFactory::getSceneManagerFamily(const ComponentType& type )
    {
        SceneManagerTypePerComponentTypeIter it = 
            mSceneManagerTypePerComponentType.find(type);
        if (it == mSceneManagerTypePerComponentType.end())
            return StringUtil::BLANK;

        return it->second;
    }
    //-------------------------------------------------------------------------
    bool ObjectFactory::registerComponentTemplate(
        const String& scenemManagerType, ComponentTemplate* componentTemplate)
    {
        assert(componentTemplate);
        if (0 == componentTemplate)
        {
            LOGW("Can't register an empty template!");
            return false;
        }

        if (scenemManagerType.empty())
        {
            LOGW(String("Is this intended? The scene manager type is empty for ") +
                "the componentTemplate " + componentTemplate->getType() );
        }

        ComponentType type = componentTemplate->getType();

        ComponentTemplateIter it = mComponentsTemplates.find(type);
        if (it != mComponentsTemplates.end())
        {
            LOGI("A component template of type '"+type+"' is already registered.");
            delete componentTemplate;
			componentTemplate = 0;
            return false;
        }

        mComponentsTemplates[type] = componentTemplate;
        mSceneManagerTypePerComponentType[type] = scenemManagerType;

        return true;
    }
    //-------------------------------------------------------------------------
    Component* ObjectFactory::createComponent(const ComponentType& type)
    {
        ComponentTemplateIter it = mComponentsTemplates.find( type );
        if (it == mComponentsTemplates.end())
            return 0;

        return ((*it).second)->createComponent();
    }
    //-------------------------------------------------------------------------
}
