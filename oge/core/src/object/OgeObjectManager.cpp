/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/object/OgeObjectManager.h"
#include "oge/object/OgeObjectSceneManager.h"
#include "oge/message/OgeMessageScheduler.h"
#include "oge/object/OgeLinkComponent.h"
#include "oge/OgeProfiler.h"

namespace oge
{
    //-------------------------------------------------------------------------
    template<> ObjectManager* Singleton<ObjectManager>::mSingleton = 0;
    ObjectManager* ObjectManager::getSingletonPtr() 
    { 
        return mSingleton; 
    }
    ObjectManager& ObjectManager::getSingleton()
    {
        assert(mSingleton);
        return *mSingleton;
    }    
    //-------------------------------------------------------------------------
    ObjectManager::ObjectManager() : System("ObjectManager", 30, 70),
        mObjectSceneManager(0), mUniqueId(0), mIsPaused(false), mLastTime(0)
    {
        LOG("ObjectManager created");
    }
    //-------------------------------------------------------------------------
    ObjectManager::~ObjectManager()
    {
        LOG("ObjectManager destroyed");
    }
    //-------------------------------------------------------------------------
    ObjectManager* ObjectManager::createSingleton()
    {
        if (!mSingleton)
            mSingleton = new ObjectManager();
        
        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void ObjectManager::destroySingleton()
    {
        if (mSingleton)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //-------------------------------------------------------------------------
    bool ObjectManager::initialise()
    {
        LOG("Initialise ObjectManager...");

        setSchedulerTimeLimit(20.0); // NEXT : to set?

        createSceneManagement();

        // Register the in-build templates
        registerComponentTemplate( "Object", new LinkComponentTemplate() );

        if (!System::initialise())
        {
            LOGE("Initialisation failed!");
            return false;
        }

        return true;
    }
    //-------------------------------------------------------------------------
    void ObjectManager::shutdown()
    {
        LOG("Shutdown ObjectManager");
        System::shutdown();

        removeAllMessageHooks();

        // TODO Seems unsafe... we should probably call a sm.shutdown() method
        mObjectSceneManagers.deleteAll();

        delete mSceneManagerEnumerator;
        mSceneManagerEnumerator = 0;
    }
    //-------------------------------------------------------------------------
    void ObjectManager::tick(double tickTime)
    {
		OgeProfileGroup("ObjectManager TickD", OGEPROF_SYSTEMS);
		// this call posts messages to be handled by scene managers below
		processMessages(tickTime);

        // Update the active scene manager
        // TODO One way to avoid this 'if (paused)' is to switch to a "dummy" osm
        //TODO Manage first tick! because value too big! mLastTime = tickTime;
        if (!mIsPaused)
            mObjectSceneManager->update(tickTime - mLastTime);
         mLastTime = tickTime; // not in the if!
    }
    //-------------------------------------------------------------------------
    void ObjectManager::createSceneManagement()
    {
        mSceneManagerEnumerator = new SceneManagerEnumerator("Object");

        mSceneManagerEnumerators.addEntry(mSceneManagerEnumerator->getType(),
            mSceneManagerEnumerator);

        // Registering the in-build SceneManagerFactories
        // TODO which factory being registered must be customisable by script 
        // as we don't need to register all types of SM all the time.
        mSceneManagerEnumerator->registerSceneManagerFactory(
            new ObjectSceneManagerFactory()); // type "Generic SM" = default SM

        mSceneManagerEnumerator->registerSceneManagerFactory(
            new TerrainObjectSceneManagerFactory()); // type "Terrain Close SM"

        mSceneManagerEnumerator->registerSceneManagerFactory(
            new PCZObjectSceneManagerFactory()); // type "PCZ SM"
        
        // Create the default object sm and set it active
        createDefaultSceneManager();
        setActiveSceneManager("OGE_Default_SceneManager");
    }
    //-------------------------------------------------------------------------
    void ObjectManager::createDefaultSceneManager()
    {
        LOGI("Creating the default scene manager.");

        mObjectSceneManager = static_cast<ObjectSceneManager*>(
            mSceneManagerEnumerator->createSceneManager(
                "OGE_Default_SceneManager", "Generic SM"));

        mObjectSceneManagers.addEntry( "OGE_Default_SceneManager", 
            mObjectSceneManager);

        LOGEC( 0 == mObjectSceneManager, 
            "A SceneManager of type 'Generic SM' couldn't be created.");
    }
    //-------------------------------------------------------------------------
    bool ObjectManager::setActiveSceneManager(const String& name)
    {
        if (!mObjectSceneManagers.entryExists(name))
        {
            LOGE(String("No object scene manager named: '")+name+"' was found."); 
            return false;
        }

        mObjectSceneManager = mObjectSceneManagers.getEntry(name);
        mObjectSceneManager->setActiveSceneManager();

        // TODO Verify if this works NOTE: We want to be able to switch from one SM to another and back.
        resetScheduler();    //  NEXT: Should also be ina clearScene method.

        return true;
    }
    //-------------------------------------------------------------------------
    ObjectSceneManager* ObjectManager::createSceneManager(const String& name, 
        const String& type)
    {
        ObjectSceneManager* sceneManager = getObjectSceneManager( name );
        if (sceneManager)
            return sceneManager;

        LOGI("Creating a scene manager... and add it to mObjectSceneManagers");
        
        sceneManager = static_cast<ObjectSceneManager*>(
            mSceneManagerEnumerator->createSceneManager( name, type ));

        mObjectSceneManagers.addEntry( name, sceneManager );

        LOGEC( sceneManager == 0, String("Couldn't create a SceneManager of type: '") + type+"'");
        
        return sceneManager;
    }
    //-------------------------------------------------------------------------
    ObjectSceneManager* ObjectManager::getObjectSceneManager(const String& name)
    {
        if (!mObjectSceneManagers.entryExists(name))
        {
            LOGW(String("No scene manager named: '")+name+"'");
            return 0;
        }
        return mObjectSceneManagers.getEntry(name);
    }
    //-------------------------------------------------------------------------
	ObjectTemplate* ObjectManager::getObjectTemplate(const ObjectType& objectType)
	{
		return mObjectFactory.getObjectTemplate(objectType);
	}
    //-------------------------------------------------------------------------
	ObjectType ObjectManager::getObjectType(const ObjectId& id)
	{
		return mObjectSceneManager->getObjectType(id);
	}
    //-------------------------------------------------------------------------
    ObjectId ObjectManager::createObject(const ObjectType& type, const MessageList& params)
    {
        LOGI("Creating a '" + type + "' game object...");

        if (type.empty())
        {
            LOGW("Can't create an object as no type was provided.");
            return "";
        }
        
        ObjectTemplate* objectTemplate = mObjectFactory.getObjectTemplate( type );

        if (0 == objectTemplate)
        {
            LOGW("Can't find an object template of type '" + type + "' : Aborting object creation:");
            return "";
        }

        // "Create" it in the current scene mgr and the sub SMs
        // This phase will:
        //   1) Generate an unique id (which identifies the components composing the object
        ObjectId id = getUniqueId();
		id += type;

        //   2) register the components in their SM
        //   3) Create the sub-library objects (mesh, opal::solid, etc).
        if (!mObjectSceneManager->createObject(id, objectTemplate, params))
        {
            LOGW( "Game object of type '" + type + "' couldn't be created with id: '" + id + "'" );
            return "";
        }

        LOGI( "...game object of type '" + type + "' created with id: '" + id + "'" );
        return id;
    }
    //-------------------------------------------------------------------------
    ObjectId ObjectManager::getUniqueId()
    {
        return StringUtil::toString( mUniqueId++ ) + String("_");
    }
    //-------------------------------------------------------------------------
    void ObjectManager::destroyObject(const ObjectId& id)
    {
        mObjectSceneManager->destroyObject( id );
    }
    //-------------------------------------------------------------------------
    bool ObjectManager::registerObjectTemplate(ObjectTemplate* objectTemplate)
    {
        assert(objectTemplate);
        if (objectTemplate == 0)
        {
            LOGW("Can't register an empty template!");
            return false;
        }

        if (!mObjectFactory.registerObjectTemplate( objectTemplate ))
        {
            LOGW("The object template for the type '" + objectTemplate->getType()
                + "' couldn't be registered.");
            return false;
        }
        return true;
    }
    //-------------------------------------------------------------------------
    bool ObjectManager::registerComponentTemplate( 
        const String& sceneManagerType,
        ComponentTemplate* componentTemplate)
    {
        if (componentTemplate == 0)
        {
            LOGW("Can't register an empty template!");
            assert(componentTemplate);
            return false;
        }
//_ TODO NOW After the next commit !
//  If we would store in ObjectMgr a map<componenttype, sceneManagerType
//      we wouldn't be obliged to pass the sceneManagerType when adding the component type
//      wo the objectTemplate !!! 
		ComponentType compFamily = componentTemplate->getFamily();
		ComponentType compType = componentTemplate->getType();

        if (!mObjectFactory.registerComponentTemplate( sceneManagerType, componentTemplate ))
        {
            LOGW("Couldn't register the component template of family '"
                + compFamily + "' & type '"
                + compType + "' for the scene manager type '"
                + sceneManagerType + "'" );
            return false;
        }
        return true;
    }
    //-------------------------------------------------------------------------
    void ObjectManager::pause(bool value)
    {
        LOG("Pausing...");
        mIsPaused = value;
    }
	//-------------------------------------------------------------------------
	void ObjectManager::postTick(double currentTime)
	{
		if(mObjectSceneManager) {
			mObjectSceneManager->postTick(currentTime);
		}
	}
	//-------------------------------------------------------------------------
	void ObjectManager::preTick(double currentTime)
	{
		if(mObjectSceneManager) {
			mObjectSceneManager->preTick(currentTime);
		}
	}
    //-------------------------------------------------------------------------
    void ObjectManager::_dispatchMessage(const Message& message, bool immediate) 
    {
		if(immediate && !mIsPaused) {
			mObjectSceneManager->processMessageImmediate(message);
		}
		else {
			mObjectSceneManager->_dispatchMessage(message);
		}
    }
    //-------------------------------------------------------------------------
}
