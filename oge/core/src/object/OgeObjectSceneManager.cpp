/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/object/OgeObjectSceneManager.h"
#include "oge/object/OgeObject.h"
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/graphics/OgeGraphicsComponent.h"
#include "oge/input/OgeInputDevice.h"
#include "oge/input/OgeInputManager.h"
#include "oge/message/OgeMessage.h"

#include "oge/object/OgeObjectManager.h"  // WIP // because of SceneManagerMap
#include "oge/network/OgeNetworkSceneManager.h"
#include "oge/network/OgeNetworkManager.h"
#include "oge/graphics/OgeGraphicsSceneManager.h"
#include "oge/physics/OgePhysicsSceneManager.h"
#include "oge/physics/OgePhysicsManager.h"
#include "oge/script/OgeScriptManager.h"
#include "oge/script/OgeScriptSceneManager.h"
#include "oge/network/OgeNetworkSceneManager.h"
#include "oge/network/OgeNetworkManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    ObjectSceneManager::ObjectSceneManager(const String& name) :
        SceneManager(name),
        mGraphicsSceneManager(0),
        mPhysicsSceneManager(0),
        mScriptSceneManager(0),
        mShowBoundingBox(false),
        mLastMaxMessageProcessingTime(0)
    {
        initMetaData();
    }
    //-------------------------------------------------------------------------
    ObjectSceneManager::~ObjectSceneManager()
    {
        // Deletes all objects
        /* Useful? destroyObjectWithId is empty as we destroy the components!
        for(unsigned int i=0; i<mObjects.size(); i++)
        {
           destroyObjectWithId( mObjects[i] );
        }
        */

        // this->clearScene(); if I put this it segfault at mSceneManagers.deleteAll();
        SceneManager::clearScene();
        mSceneManagers.removeEntry(getFamily()); // Remove itself first to avoid problems
        mSceneManagers.deleteAll();        
    }
    //-------------------------------------------------------------------------
    void ObjectSceneManager::init()
    {
        // ----------- Add methods to process messages --------------------
        // Note: DON'T FORGET TO ADD THEM ALSO IN DERIVED OSMs !!!
        ObjectManager::getSingleton().addMessageHook( ObjectMessage::DESTROY_OBJECT,
            MessageHook(this, &ObjectSceneManager::_destroyObjectHook) );

		// update scene managesr
		updateSceneManagers();


        // Gui todo
    ///    GuiSceneManagerFactory guiFactory;
    ///    mGuiSceneManager = static_cast<GuiSceneManager*>(guiFactory.createInstance(mName));
    ///    mSceneManagers.addObject(mGuiSceneManager->getFamily(), mGuiSceneManager);

    ///    InputSceneManagerFactory inputFactory;
    ///    mInputSceneManager = static_cast<InputSceneManager*>(inputFactory.createInstance(mName));
    ///    mSceneManagers.addObject(mInputSceneManager->getFamily(), mInputSceneManager);

    }
    //-------------------------------------------------------------------------
    void ObjectSceneManager::initMetaData() const
    {
        mMetaData.type = "Generic SM";
        mMetaData.family = "Object";
        mMetaData.description = "Generic Object scene manager";
        mMetaData.sceneTypeMask = 0; // irrelevant - used in graphical SM
        mMetaData.worldGeometrySupported = true; // irrelevant - used in graphical SM
    }
	//-------------------------------------------------------------------------
	bool ObjectSceneManager::processMessageImmediate(const Message& msg)
	{
		processMessage(msg);

        // Update the sub-SMs
        for(size_t i = 0; i < mSceneManagerPriority.size(); ++i) {
            if(this != mSceneManagerPriority[i]) {
                mSceneManagerPriority[i]->processMessageImmediate(msg);
            }
        }

		return true;
	}
    //-------------------------------------------------------------------------
    void ObjectSceneManager::update(double deltaTime)
    {
        SceneManager::update(deltaTime);

        // NEXT Optimise how can we avoid passing in each objects and each components
        // See "Implementation details" http://cowboyprogramming.com/2007/01/05/evolve-your-heirachy/

        double lastMaxMsgProcessingTime = 0;

        // Update the sub-SMs
        for(size_t i = 0; i < mSceneManagerPriority.size(); ++i) {
            lastMaxMsgProcessingTime = std::max(
                lastMaxMsgProcessingTime, 
                mSceneManagerPriority[i]->getLastMessageProcessingTime() 
            );

            if(this != mSceneManagerPriority[i]) {
                mSceneManagerPriority[i]->update(deltaTime);
            }
        }

        mLastMaxMessageProcessingTime = lastMaxMsgProcessingTime;

        // FIXME: SHOULD NOT post each message to ALLL of the managers!!!
        //   --> use filters --> How to find/set the proper time limit? Game-dependent?
        ObjectManager::getSingleton().setAvgMessageProcessingTime(
            mLastMaxMessageProcessingTime / (mLastProcessedMessagesCount+1) );  // WIP - It is ok for now
    }
	//-------------------------------------------------------------------------
	void ObjectSceneManager::updateSceneManagers()
	{
		LOGI("updateSceneManagers");
        // -------- Create the sub SMs contained by this Object SM ---------
        // Note: The order is important - we want to update scene managers in a particular order
        // so that they can override important settings.  For example, update physics before graphics
        // so that the physics can alter the position that the graphics and camera will use.
		mSceneManagerPriority.clear();

		// can we do this??? threading issues? need to mutex first?
		mSceneManagers.removeAll();

        mSceneManagers.addEntry(getFamily(), this);
        mSceneManagerPriority.push_back(this);

        // Script (sm of type 'Generic SM')
		mScriptSceneManager = ScriptManager::getManager()->getActiveSceneManager();
        if (!mScriptSceneManager)
        {
			mScriptSceneManager = static_cast<ScriptSceneManager*>
				(ScriptManager::getManager()->createSceneManager("OGE_Default_SceneManager", "Generic SM"));
			if(!mScriptSceneManager) {
				LOGE("No script scene manager of type 'Generic SM' could be created! Factory not registered?");
				return;
			}
        }
        mSceneManagers.addEntry(mScriptSceneManager->getFamily(), mScriptSceneManager);
        mSceneManagerPriority.push_back(mScriptSceneManager);

        // Network (sm of type 'Generic SM')
		if(NetworkManager::getManager()) {
			mNetworkSceneManager = NetworkManager::getManager()->getActiveSceneManager();
			if (!mNetworkSceneManager)
			{
				mNetworkSceneManager = static_cast<NetworkSceneManager*>
					(NetworkManager::getManager()->createSceneManager("OGE_Default_SceneManager", "Generic SM"));
				if (!mNetworkSceneManager)
				{
					LOGE("No network scene manager of type 'Generic SM' could be created! Factory not registered?");
					return;
				}
			}
			mSceneManagers.addEntry(mNetworkSceneManager->getFamily(), mNetworkSceneManager);
			mSceneManagerPriority.push_back(mNetworkSceneManager);
		}

        // Physics (sm of type 'Generic SM')
		mPhysicsSceneManager = PhysicsManager::getManager()->getActiveSceneManager();
        if (!mPhysicsSceneManager)
        {
			mPhysicsSceneManager = static_cast<PhysicsSceneManager*>
				(PhysicsManager::getManager()->createSceneManager("OGE_Default_SceneManager", "Generic SM"));
			if (!mPhysicsSceneManager)
			{
				LOGE("No physics scene manager of type 'Generic SM' could be created! Factory not registered?");
				return;
			}
        }
        mSceneManagers.addEntry(mPhysicsSceneManager->getFamily(), mPhysicsSceneManager);
        mSceneManagerPriority.push_back(mPhysicsSceneManager);

		mGraphicsSceneManager = GraphicsManager::getManager()->getActiveSceneManager();
        if (!mGraphicsSceneManager)
        {
			mGraphicsSceneManager = static_cast<GraphicsSceneManager*>
				(GraphicsManager::getManager()->createSceneManager("OGE_Default_SceneManager", "Generic SM"));
			if (!mGraphicsSceneManager)
			{
				LOGE("No graphics scene manager of type 'Generic SM' could be created! Factory not registered?");
				return;
			}
        }
        mSceneManagers.addEntry(mGraphicsSceneManager->getFamily(), mGraphicsSceneManager);
        mSceneManagerPriority.push_back(mGraphicsSceneManager);
	}
    //-------------------------------------------------------------------------
    void ObjectSceneManager::clearScene()
    {
        SceneManager::clearScene();

        // NEXT We are deleting the object/component "directly" and not via messages.
        //      Study (mem leaks) if we should use messages instead but we could miss some object/components
        //      Safer this way? If we send messages is there a problem of timing as we are "shutting down"
        SceneManagerMap::Iterator iter = mSceneManagers.begin();
        for (; iter != mSceneManagers.end(); ++iter)
        {
            if (this != iter->second)
                iter->second->clearScene();
        }

        mObjects.clear();
    }
    //-------------------------------------------------------------------------
    bool ObjectSceneManager::setActiveSceneManager()
    {
        // NEXT Depending on when this method is called most of the ptrs are empty
        //      Be sure to call this when all sub-sm where created.
        // NEXT Should we only pass a generic name identical in each sm (like Default_SM)?
        if (mGraphicsSceneManager)
            GraphicsManager::getManager()->setActiveSceneManager( mGraphicsSceneManager );
        if (mPhysicsSceneManager)
            PhysicsManager::getManager()->setActiveSceneManager( mPhysicsSceneManager );
        if (mScriptSceneManager)
            ScriptManager::getManager()->setActiveSceneManager( mScriptSceneManager );

        // AudioManager::getManager()->setActiveSceneManager( mAudioSceneManager );

        // GuiManager::getManager()->setActiveSceneManager( mGuiSceneManager );
        // Note: With CEGUI I had to do this (one of the reason I dumped CEGUI):
        //     GuiManager::getSingletonPtr()->setTargetSceneManager( getGraphicsSceneManager()->getOgreSceneManager() );

        return true;
    }
    //-------------------------------------------------------------------------
    SceneManager* ObjectSceneManager::getSceneManagerByFamily( const String& family )
    {
        return mSceneManagers.getEntry( family );
    }
    //-------------------------------------------------------------------------
    bool ObjectSceneManager::createObject(const ObjectId& id, 
        ObjectTemplate* objectTemplate, const MessageList& params, bool activate)
    {
        assert(!id.empty());
        assert(0 != objectTemplate);

        bool initialised = true;    // TODO WIP: will be replaced by async results...

        // For each component we call the sm of the same family
        // and create the component in it
        const ObjectTemplate::ComponentTypeList& components = 
            objectTemplate->getComponents();

        for (ObjectTemplate::ComponentTypeListIter it = components.begin();
            components.end() != it; ++it)
        {
            const String& type = (*it);
            const String& smfamily = 
                ObjectManager::getSingletonPtr()->getSceneManagerFamily(type);

            SceneManager* sm = this->getSceneManagerByFamily( smfamily );
            if (0 != sm)
            {
                bool created = sm->createComponent( id, type, params ).get();

                LOGWC(!created, "A component of type '"+type+"' couldn't be created"
                        + " for the scenemanager of family '"+smfamily+
                        + "' The object with id '" + id + "' might be incomplete!");
                    // TODO shall we return false ? delete other component? continue?
                initialised &= created;
            }
            else
            {
                LOGW("No scene manager of family '" + smfamily
                    + "' was found for the object type '" + type
                    + "' hence the object with id '" + id + "' might be incomplete!");
                // TODO Shall we return false? delete other component? continue?
            }
        }

        // Store the object
        mObjects.push_back( id );
		mObjectTypes[id] = objectTemplate->getType();

        // NEXT: We can force the msg scheduler to process msgs of this object, and then set
        //        the object enable/visible. Because some msgs handlers like SET_DIMENSION or SET_ORIENTATION_EULER
        //        send another msg.

        if (!initialised)
        {
            LOGW("Failed to create game object of type: " + objectTemplate->getType());
            assert(!"Failed to create game object!");
            // TODO object->deleteInstance(this);    // Ok?
            return false;
        }

        // Activate the object when initialised completely to ensure that during
        // creation/initialisation, incorrect visual/physical/etc data aren't used.

        ObjectManager::getSingleton().postMessage(
			ObjectMessage::SET_OBJECT_ACTIVE, id, activate, 0, 0, Message::IMMEDIATE );

        //activateObject(id, activate);

        return true;
    }
    //-------------------------------------------------------------------------
    void ObjectSceneManager::activateObject(const ObjectId& id, bool activate)
    {
        // TODO Use a message

        LOGI( String(activate ? "Activating" : "Deactivating")
            + " game object with Id: " + id );

        // TODO Check to see if the object exists in this SM
        // TODO Should we get the 'object template' and only send to each sm
        //      or simply as below simply send to each sub-sm?
        SceneManagerMap::Iterator iter = mSceneManagers.begin();
        for (; iter != mSceneManagers.end(); ++iter)
        {
            if (this != iter->second)
                iter->second->activateComponent(activate, id);
        }
    }
    //-------------------------------------------------------------------------
    void ObjectSceneManager::destroyObject(const ObjectId& id)
    {
        LOGI( "Destruction of a game object requested. Id: '" + id + "'" );

        ObjectManager::getSingleton().postMessage(
            ObjectMessage::DESTROY_OBJECT, id,/* TODO: ReplyTask/Delegate */Any(), 0, 0, Message::IMMEDIATE );
    }
    //-------------------------------------------------------------------------
    bool ObjectSceneManager::_destroyObjectHook(const Message& message)
    {
        const ObjectId id = FastAnyCast(const ObjectId, message.getParam_1());

        // Discard other messages that was sent to this object. Note that current
        // task of the scheduler which sent this message, will be also affected.
//#pragma OGE_WARN("TODO TEMP: MsgSheduler::mSchedulerMutex should be recursive.")
        ///ObjectManager::getSingleton().discardMessages(id);
 
        return true;
    }
    //-------------------------------------------------------------------------
	ObjectType ObjectSceneManager::getObjectType(const ObjectId& object)
	{
		if(mObjectTypes.find(object) == mObjectTypes.end()) {
			return "";
		}

		return mObjectTypes[object];
	}
    //-------------------------------------------------------------------------
	StringVector ObjectSceneManager::getObjectsOfType(ObjectType type) const
	{
		StringVector objects;
		ObjectTypesMap::const_iterator iter = mObjectTypes.begin();
		for(; iter != mObjectTypes.end(); iter++) {
			if(iter->second == type) {
				objects.push_back(iter->first);
			}
		}

		return objects;
	}
    //-------------------------------------------------------------------------
	const ObjectSceneManager::ObjectTypesMap& ObjectSceneManager::getObjectsTypes() const
	{
		return mObjectTypes;
	}
    //-------------------------------------------------------------------------
    bool ObjectSceneManager::processMessage(const Message& message)
    {
        switch (message.getType()) {
			case ObjectMessage::DESTROY_OBJECT:
			{
				const ObjectId *id = FastAnyCast(ObjectId, &message.getParam_1());
				LOGI( "Destroying a game object with id: '" + *id + "'" );
				destroyObjectWithId(*id);
			}
			break;
        }
        
        return SceneManager::processMessage(message);
    }
    //-------------------------------------------------------------------------
    void ObjectSceneManager::showBoundingBox(bool show, const ObjectId& id)
    {
        GraphicsManager::getManager()->showBoundingBox( show, id );
    }
    //-------------------------------------------------------------------------
    void ObjectSceneManager::destroyObjectWithId(const String& id)
    {
        // done in the sm:  activateObject( id, false );
    }
    //-------------------------------------------------------------------------
    ObjectId ObjectSceneManager::doObjectIdRayQuery()
    {
        InputDevice* mouse = InputManager::getManager()->getFirstMouse();

        if (mouse == 0) // NEXT only assert(mouse); as we will never use this method without a mouse
            return StringUtil::BLANK;

        MouseState* state = (MouseState*) (mouse->getState());

        return mGraphicsSceneManager->doObjectIdRayQuery( 
            (float) state->absX / (float) state->width,
            (float) state->absY / (float) state->height );
    }
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    //--------------- ObjectSceneManagerFactory -------------------------------
    //-------------------------------------------------------------------------
    ObjectSceneManagerFactory::ObjectSceneManagerFactory()
        : SceneManagerFactory()
    {
        // Temporary instance to get the type name
        ObjectSceneManager sm("temp");
        sm.initMetaData();
        mType = sm.getType();
    }
    //-------------------------------------------------------------------------
    ObjectSceneManagerFactory::~ObjectSceneManagerFactory()
    {
    }
    //-------------------------------------------------------------------------
    SceneManager* ObjectSceneManagerFactory::createInstance(const String& name)
    {
        ObjectSceneManager* sm = new ObjectSceneManager(name);
        sm->init();
        assert(sm->getType() == mType);
        return sm;
    }
    //-------------------------------------------------------------------------
    bool ObjectSceneManagerFactory::checkAvailability()
    {
        // Nothing special in objectscenemanger for now
        mAvailable = true;
        return true;
    }
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    //---------------------- TerrainObjectSceneManager ------------------------
    //-------------------------------------------------------------------------
    TerrainObjectSceneManager::TerrainObjectSceneManager(const String& name)
        : ObjectSceneManager( name )
    {
        initMetaData();
    }
    //-------------------------------------------------------------------------
    TerrainObjectSceneManager::~TerrainObjectSceneManager()
    {
        // note: ~ObjectScenemanaer is called
    }
    //-------------------------------------------------------------------------
    void TerrainObjectSceneManager::init()
    {
        // --------- Add methods to process messages -----------------------
        // Note: DON'T FORGET TO ADD THEM ALSO IN DERIVED OSMs !!!
        ObjectManager::getSingleton().addMessageHook( ObjectMessage::DESTROY_OBJECT,
            MessageHook(this, &TerrainObjectSceneManager::_destroyObjectHook) );

        mSceneManagers.addEntry(getFamily(), this);

        // -------- Create the sub SMs contained by this Object SM ---------

        // Graphics (sm of type 'Terrain Close SM')
        mGraphicsSceneManager = static_cast<GraphicsSceneManager*>
            (GraphicsManager::getManager()->createSceneManager(mName, "Terrain Close SM"));
        if (!mGraphicsSceneManager)
        {
            LOGE("No graphics scene manager of type 'Terrain Close SM' could be created! Factory not registered?");
            return;
        }
        mSceneManagers.addEntry(mGraphicsSceneManager->getFamily(), mGraphicsSceneManager);

        // Physics (sm of type 'Generic SM')
        // Note that this default sm should already have been created in PhysicsManager::createDefaultSceneManager()
        mPhysicsSceneManager = static_cast<PhysicsSceneManager*>
            (PhysicsManager::getManager()->createSceneManager(mName, "Generic SM"));
        if (!mPhysicsSceneManager)
        {
            LOGE("No physics scene manager of type 'Generic SM' could be created! Factory not registered?");
            return;
        }
        mSceneManagers.addEntry(mPhysicsSceneManager->getFamily(), mPhysicsSceneManager);

        // Script (sm of type 'Generic SM')
        // Note that this default sm should already have been created in ScriptManager::createDefaultSceneManager()
        mScriptSceneManager = static_cast<ScriptSceneManager*>
            (ScriptManager::getManager()->createSceneManager(mName, "Generic SM"));
        if (!mScriptSceneManager)
        {
            LOGE("No script scene manager of type 'Generic SM' could be created! Factory not registered?");
            return;
        }
        mSceneManagers.addEntry(mScriptSceneManager->getFamily(), mScriptSceneManager);

        // Gui todo
    ///    GuiSceneManagerFactory guiFactory;
    ///    mGuiSceneManager = static_cast<GuiSceneManager*>(guiFactory.createInstance(mName));
    ///    mSceneManagers.addObject(mGuiSceneManager->getFamily(), mGuiSceneManager);

    ///    InputSceneManagerFactory inputFactory;
    ///    mInputSceneManager = static_cast<InputSceneManager*>(inputFactory.createInstance(mName));
    ///    mSceneManagers.addObject(mInputSceneManager->getFamily(), mInputSceneManager);

    }
    //-------------------------------------------------------------------------
    void TerrainObjectSceneManager::initMetaData() const
    {
        mMetaData.type = "Terrain Close SM";
        mMetaData.family = "Object";
        mMetaData.description = "Terrain Object SM";
        mMetaData.sceneTypeMask = 0; // irrelevant - used in graphical SM
        mMetaData.worldGeometrySupported = true; // irrelevant - used in graphical SM
    }
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    //------------------ TerrainObjectSceneManagerFactory ---------------------
    //-------------------------------------------------------------------------
    TerrainObjectSceneManagerFactory::TerrainObjectSceneManagerFactory()
    {
        // Temporary sm to get the type name
        TerrainObjectSceneManager sm("temp");
        sm.initMetaData();
        mType = sm.getType();
    }
    //-------------------------------------------------------------------------
    TerrainObjectSceneManagerFactory::~TerrainObjectSceneManagerFactory()
    {
    }
    //-------------------------------------------------------------------------
    SceneManager* TerrainObjectSceneManagerFactory::createInstance(const String& name)
    {
        ObjectSceneManager* sm = new TerrainObjectSceneManager(name);
        sm->init();
        assert(sm->getType() == mType);
        return sm;
    }
    //-------------------------------------------------------------------------
    bool TerrainObjectSceneManagerFactory::checkAvailability()
    {
        // nothing special in objectscenemanger for now
        mAvailable = true;
        return true;
    }
    //-------------------------------------------------------------------------

    //-----------------------------------------------------------------------------
    //---------------------- PCZObjectSceneManager ----------------------------
    //-----------------------------------------------------------------------------
    PCZObjectSceneManager::PCZObjectSceneManager(const String& name)
        : ObjectSceneManager( name )
    {
        initMetaData();
    }
    //-----------------------------------------------------------------------------
    PCZObjectSceneManager::~PCZObjectSceneManager()
    {
        // note: ~ObjectScenemanaer is called
    }
    //-----------------------------------------------------------------------------
    void PCZObjectSceneManager::init()
    {
        // --------- Add methods to process messages -----------------------
        // Note: DON'T FORGET TO ADD THEM ALSO IN DERIVED OSMs !!!
//-        ObjectManager::getSingleton().addMessageHook( ObjectMessage::DESTROY_OBJECT,
//-            MessageHook(this, &TerrainObjectSceneManager::_destroyObjectHook) );

        mSceneManagers.addEntry(getFamily(), this);

        // -------- Create the sub SMs contained by this Object SM ---------

        // Graphics (sm of type 'Terrain Close SM')
        mGraphicsSceneManager = static_cast<GraphicsSceneManager*>
            (GraphicsManager::getManager()->createSceneManager(mName, "PCZ SM"));
        if (!mGraphicsSceneManager)
        {
            LOGE("No graphics scene manager of type 'PCZ SM' could be created! Factory not registered?");
            return;
        }
        mSceneManagers.addEntry(mGraphicsSceneManager->getFamily(), mGraphicsSceneManager);

        // Physics (sm of type 'Generic SM')
        // Note that this default sm should already have been created in PhysicsManager::createDefaultSceneManager()
        mPhysicsSceneManager = static_cast<PhysicsSceneManager*>
            (PhysicsManager::getManager()->createSceneManager(mName, "Generic SM"));
        if (!mPhysicsSceneManager)
        {
            LOGE("No physics scene manager of type 'Generic SM' could be created! Factory not registered?");
            return;
        }
        mSceneManagers.addEntry(mPhysicsSceneManager->getFamily(), mPhysicsSceneManager);

        // Script (sm of type 'Generic SM')
        // Note that this default sm should already have been created in ScriptManager::createDefaultSceneManager()
        mScriptSceneManager = static_cast<ScriptSceneManager*>
            (ScriptManager::getManager()->createSceneManager(mName, "Generic SM"));
        if (!mScriptSceneManager)
        {
            LOGE("No script scene manager of type 'Generic SM' could be created! Factory not registered?");
            return;
        }
        mSceneManagers.addEntry(mScriptSceneManager->getFamily(), mScriptSceneManager);

        // Gui todo
    ///    GuiSceneManagerFactory guiFactory;
    ///    mGuiSceneManager = static_cast<GuiSceneManager*>(guiFactory.createInstance(mName));
    ///    mSceneManagers.addObject(mGuiSceneManager->getFamily(), mGuiSceneManager);

    ///    InputSceneManagerFactory inputFactory;
    ///    mInputSceneManager = static_cast<InputSceneManager*>(inputFactory.createInstance(mName));
    ///    mSceneManagers.addObject(mInputSceneManager->getFamily(), mInputSceneManager);

    }
    //-----------------------------------------------------------------------------
    void PCZObjectSceneManager::initMetaData() const
    {
        mMetaData.type = "PCZ SM";
        mMetaData.family = "Object";
        mMetaData.description = "PCZ Object SM";
        mMetaData.typeName = "PCZ Object SceneManager";
        mMetaData.sceneTypeMask = 0; // irrelevant - used in graphical SM
        mMetaData.worldGeometrySupported = true; // irrelevant - used in graphical SM
    }
    //-----------------------------------------------------------------------------

    //-----------------------------------------------------------------------------
    //------------------ PCZObjectSceneManagerFactory -------------------------
    //-----------------------------------------------------------------------------
    PCZObjectSceneManagerFactory::PCZObjectSceneManagerFactory()
    {
        // Temporary sm to get the type name
        PCZObjectSceneManager sm("temp");
        sm.initMetaData();
        mType = sm.getType();
    }
    //-----------------------------------------------------------------------------
    PCZObjectSceneManagerFactory::~PCZObjectSceneManagerFactory()
    {
    }
    //-----------------------------------------------------------------------------
    SceneManager* PCZObjectSceneManagerFactory::createInstance(const String& name)
    {
        ObjectSceneManager* sm = new PCZObjectSceneManager(name);
        sm->init();
        assert( sm->getType() == mType);
        return sm;
    }
    //-----------------------------------------------------------------------------
    bool PCZObjectSceneManagerFactory::checkAvailability()
    {
        // nothing special in objectscenemanger for now
        mAvailable = true;
        return true;
    }
    //-----------------------------------------------------------------------------
}
