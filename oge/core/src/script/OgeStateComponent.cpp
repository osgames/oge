/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/script/OgeStateComponent.h"
#include "oge/script/OgeState.h"
#include "oge/containers/OgeMap.h"
#include "oge/object/OgeObject.h"

namespace oge
{
    //-----------------------------------------------------------------------------
    StateComponent::StateComponent(const ComponentType& type, const ComponentType& family)
        : Component(type, family),
        mNextState(0)
    {
        // dummy so that process will not segfault
        mState = new State("DefaultState");
        addState(mState);
    }
    //-----------------------------------------------------------------------------
    StateComponent::~StateComponent()
    {
        mStates.deleteEntry("DefaultState");
    }
    //-----------------------------------------------------------------------------
    bool StateComponent::_reset(const MessageList& params, SceneManager* sceneManager)
    {
        for (MessageList::ConstIter it = params.list.begin(); it != params.list.end(); ++it) {
            _processMessage( (*it) );
        }

        return true;
    }
    //-----------------------------------------------------------------------------
    void StateComponent::_destroy(SceneManager* sceneManager)
    {
		mStates.deleteAll();
    }
    //-----------------------------------------------------------------------------
    void StateComponent::_activate(bool activate, SceneManager* sceneManager)
    {
        // TODO: De-/Activate current state???
    }
    //-----------------------------------------------------------------------------
    Component::UpdateRate StateComponent::_getUpdateRate() const
    {
        return Component::EVERY_TICK;    // TODO: Tooo fast! Make lesser.
    }
    //-----------------------------------------------------------------------------
    void StateComponent::_update(double deltaTime)
    {
        if (mNextState)
            swapState();
        // TODO how to optimise? Not all state need to receive the time !
        // but we don't want either to have each state in all component have a ptr to the object
        mState->tick(deltaTime);
    }
    //-----------------------------------------------------------------------------
    void StateComponent::write(Serialiser* serialisePtr)
    {
    }
    //-----------------------------------------------------------------------------
    void StateComponent::read(Serialiser* serialisePtr)
    {
    }
    //-----------------------------------------------------------------------------
    bool StateComponent::fixup()
    {
        // no fixup to do
        return true;
    }
    //-----------------------------------------------------------------------------
    void StateComponent::swapState()
    {
        if (mNextState && mState->deactivate())
        {
            mState = mNextState;
            mNextState = 0;
            LOGI( "State of a '" + this->getType()
                + "' game object with Id '" + getObjectId()
                + "' is now changed to: " + mState->getName() );
            mState->activate();
        }
    }
    //-----------------------------------------------------------------------------
    bool StateComponent::setNextState(const String& name)
    {
        mNextState = mStates.getEntry(name);
		if (mNextState.isNull())
            return false;
        return true;
    }
    //-----------------------------------------------------------------------------
    bool StateComponent::addState( StatePtr state )
    {
        const String& name = state->getName();
        if (mStates.entryExists(name))
            return false;
        mStates.addEntry( name, state );
        return true;
    }
    //-----------------------------------------------------------------------------
    void StateComponent::removeState(const String& name)
    {
        if (name != mState->getName())
        {
            mStates.removeEntry(name);
        }
    }
    //-----------------------------------------------------------------------------
    void StateComponent::removeAllStates()
    {
        mStates.removeAll();
    }
    //-----------------------------------------------------------------------------
    String StateComponent::getCurrentStateName()
    {
        return mState->getName();
    }
    //-----------------------------------------------------------------------------
    StatePtr StateComponent::getCurrentState()
    {
        return mState;
    }
    //-----------------------------------------------------------------------------
    StatePtr StateComponent::getState(const String& stateid)
    {
        StateMap::Iterator iter = mStates.find(stateid);
        if (iter != mStates.end())
            return (*iter).second;
        return StatePtr(0);
    }
    //-----------------------------------------------------------------------------
	void StateComponent::_processMessage(const Message& message)
	{
		switch(message.getType()) {
			case ObjectMessage::SET_NEXT_STATE:
				{
					const String *stateId = FastAnyCast(String, &message.getParam_1());
					const bool *shouldSwapStates = FastAnyCast(bool, &message.getParam_2());

					// set the next state
					setNextState(*stateId);

					// swap if requested
					if(*shouldSwapStates) {
						swapState();
					}
				}
				break;
		}

		if(mState) {
			mState->_processMessage(message);
		}
	}
} // namespace
