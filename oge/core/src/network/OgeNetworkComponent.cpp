/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/network/OgeNetworkComponent.h"
#include "oge/network/OgeNetworkManager.h"

namespace oge
{
	NetworkComponent::NetworkComponent(const ComponentType& type, const ComponentType& family)
		: Component(type, family),
		mDirtyFlags(0),
		mLastTick(0),
		mOwnerNetworkId(-1)
	{ 
		mClientCommand.mTick = 0; 
		mClientCommand.mCmdBits = 0; 

		// @TODO get number of slots from param or setting not hard coded!

		// need enough slots to hold all the data sent 
		// from the server for the amount of latency
		// server just needs one
		mNumSlots = NetworkManager::getManager()->isServer() ? 1 : 100;

		TickData t;
		t.orientation = Quaternion::IDENTITY;
		t.position = Vector3::ZERO;
		t.tick = (unsigned short)0;

		mTickData.resize(mNumSlots,t);
	}

    //-------------------------------------------------------------------------
	void NetworkComponent::addTickData(const TickData& tickData)
	{
		// store this update in a buffer so we can use it to interpolate
		mTickData[tickData.tick % mNumSlots].tick = tickData.tick;
		mTickData[tickData.tick % mNumSlots].position = tickData.position;
		mTickData[tickData.tick % mNumSlots].orientation = tickData.orientation;

		mLastTick = tickData.tick;
	}
    //-------------------------------------------------------------------------
	const NetworkComponent::TickData& NetworkComponent::getTickData(NetworkTick tick)
	{
		return mTickData[tick % mNumSlots];
	}
    //-------------------------------------------------------------------------
    void NetworkComponent::write(Serialiser* serialisePtr)
    {
    }
    //-------------------------------------------------------------------------
    void NetworkComponent::read(Serialiser* serialisePtr)
    {
    }
    //-------------------------------------------------------------------------
    bool NetworkComponent::fixup()
    {
        return true;
    }
    //-------------------------------------------------------------------------
    Component::UpdateRate NetworkComponent::_getUpdateRate() const
    {
		return Component::NO_UPDATE;
    }
	//-------------------------------------------------------------------------
	void NetworkComponent::_postTick(double currentTime)
	{
		// don't clear the flags here - let the network manager do that after
		// sending the updated data
		//mDirtyFlags = 0;
	}
    //-------------------------------------------------------------------------
	void NetworkComponent::_processMessage(const Message& message)
	{
		unsigned short tmp = 0;
		switch(message.getType()) {
			case ObjectMessage::SET_HEALTH:
				tmp = (unsigned short)FastAnyCast(int, message.getParam_1());
				if(mNonTickData.health != tmp) {
					mNonTickData.health = tmp;
					mDirtyFlags |= (1 << NetworkComponent::HEALTH);
				}
				break;
			case ObjectMessage::SET_POWER:
				tmp = (unsigned short)FastAnyCast(int, message.getParam_1());
				if(mNonTickData.power != tmp) {
					mNonTickData.power = tmp;
					mDirtyFlags |= (1 << NetworkComponent::POWER);
				}
				break;
			case ObjectMessage::SET_ARMOUR:
				{
					unsigned short type = (unsigned short)FastAnyCast(int, message.getParam_2());
					// max of 8 armour types
					if(type < 8) {
						tmp = (unsigned short)FastAnyCast(int, message.getParam_1());
						if(mNonTickData.armour[type] != tmp) {
							mNonTickData.armour[type] = tmp;
							mDirtyFlags |= (1 << (NetworkComponent::ARMOUR + tmp));
						}
					}
				}
				break;
			case ObjectMessage::SET_AMMO:
				{
					unsigned short type = (unsigned short)FastAnyCast(int, message.getParam_2());
					// max of 8 ammo types
					if(type < 8) {
						tmp = (unsigned short)FastAnyCast(int, message.getParam_1());
						if(mNonTickData.ammo[type] != tmp) {
							mNonTickData.ammo[type] = tmp;
							mDirtyFlags |= (1 << (NetworkComponent::AMMO + type));
						}
					}
				}
				break;
			case ObjectMessage::SET_DAMAGE:
				{
					unsigned short type = (unsigned short)FastAnyCast(int, message.getParam_2());
					// max of 8 ammo types
					if(type < 8) {
						tmp = (unsigned short)FastAnyCast(int, message.getParam_1());
						if(mNonTickData.damage[type] != tmp) {
							mNonTickData.damage[type] = tmp;
							mDirtyFlags |= (1 << (NetworkComponent::DAMAGE + type));
						}
					}
				}
				break;
			case ObjectMessage::SET_NETWORK_OWNER:
				mOwnerNetworkId = FastAnyCast(int, message.getParam_1());
				break;
			case ObjectMessage::CLIENT_COMMAND:
				const NetworkClientCommand *cmd = FastAnyCast(NetworkClientCommand, &message.getParam_1());
				assert(cmd);
				mClientCommand.mTick = cmd->mTick;
				mClientCommand.mCmdBits = cmd->mCmdBits;
				mClientCommand.mMouseX = cmd->mMouseX;
				mClientCommand.mMouseY = cmd->mMouseY;
			break;
		}
	}
    //-------------------------------------------------------------------------
    void NetworkComponentDefault::write(Serialiser* serialisePtr)
    {
    }
    //-------------------------------------------------------------------------
    void NetworkComponentDefault::read(Serialiser* serialisePtr)
    {
    }
	//-------------------------------------------------------------------------
    void NetworkComponentDefault::_processMessage(const Message& message)
    {
		NetworkComponent::_processMessage(message);
    }
    //-------------------------------------------------------------------------
    bool NetworkComponentDefault::_reset(const MessageList& params, SceneManager* sceneManager)
    {
        MessageList::ConstIter it = params.list.begin();

        for (; it != params.list.end(); ++it)
        {
            _processMessage( (*it) );
        }

        return true;
    }
    //-------------------------------------------------------------------------
    void NetworkComponentDefault::_destroy(SceneManager* sceneManager)
    {
    }
    //-------------------------------------------------------------------------
    void NetworkComponentDefault::_activate(bool activate, SceneManager* sceneManager)
    {
    }
    //-------------------------------------------------------------------------
    Component::UpdateRate NetworkComponentDefault::_getUpdateRate() const
    {
        return Component::NO_UPDATE;
    }
    //-------------------------------------------------------------------------
    void NetworkComponentDefault::_update(double deltaTime)
    {
        LOGW("Should not be called! This component being set as NO_UPDATE.");
    }
}
