/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/network/OgeNetworkSceneManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    NetworkSceneManagerFactory::NetworkSceneManagerFactory()
    {
        // Temporary sm to get the type name
        NetworkSceneManager sm("temp");
        sm.initMetaData();
        mType = sm.getType();
    }
    //-------------------------------------------------------------------------
    NetworkSceneManagerFactory::~NetworkSceneManagerFactory()
    {
    }
    //-------------------------------------------------------------------------
    SceneManager* NetworkSceneManagerFactory::createInstance(const String& name)
    {
        NetworkSceneManager* sm = new NetworkSceneManager(name);
        sm->init();
        assert(sm->getType() == mType);
        return sm;
    }
    //-------------------------------------------------------------------------
    bool NetworkSceneManagerFactory::checkAvailability()
    {
        mAvailable = true;
        return true;
    }
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    NetworkSceneManager::NetworkSceneManager(const String& name) :
        SceneManager(name)
    {
        initMetaData();
    }
     //-------------------------------------------------------------------------
    NetworkSceneManager::~NetworkSceneManager()
    {
        clearScene();
    }
    //-------------------------------------------------------------------------
    void NetworkSceneManager::init()
    {
        // Override this method to set your SM in your plugin
    }
    //-------------------------------------------------------------------------
    void NetworkSceneManager::initMetaData() const
    {
        mMetaData.type = "Generic SM";
        mMetaData.family = "Network";
        mMetaData.description = "Generic network scene manager";
        mMetaData.sceneTypeMask = 0; // Not used
        mMetaData.typeName = "DefaultSceneManager"; // used to initialise ogre3d
        mMetaData.worldGeometrySupported = false;
    }
    //-------------------------------------------------------------------------
    void NetworkSceneManager::update(double deltaTime)
    {

        SceneManager::update(deltaTime);
    }
    //-------------------------------------------------------------------------
    void NetworkSceneManager::clearScene()
    {
        SceneManager::clearScene();
    }
	//-------------------------------------------------------------------------
	AsyncBool NetworkSceneManager::createComponent(const ObjectId& id, const ComponentType& type,
            const MessageList& params)
	{
		// save the creation params
		mObjectCreationParams[id] = params;

		return SceneManager::createComponent(id, type, params);
	}
	//-------------------------------------------------------------------------
	const NetworkAwareStatus& NetworkSceneManager::getClientAwareObjects(int clientId)
	{
		if(mClientAwareObjects.size() <= clientId) {
			mClientAwareObjects.resize(clientId + 1);
		}

		return mClientAwareObjects[clientId];
	}
	//-------------------------------------------------------------------------
	const MessageList& NetworkSceneManager::getObjectCreationParams(const ObjectId& id)
	{
		// the params list will be empty if the id doesn't exist
		return mObjectCreationParams[id];
	}
    //-------------------------------------------------------------------------
    bool NetworkSceneManager::processMessage(const Message& message)
    {
        return SceneManager::processMessage(message);
    }
    //-------------------------------------------------------------------------
	void NetworkSceneManager::removeAllClientAwareObjects(int clientId)
	{
		if(mClientAwareObjects.size() > clientId) {
			mClientAwareObjects[clientId].clear();
		}
	}
    //-------------------------------------------------------------------------
	void NetworkSceneManager::removeClientAwareObject(const ObjectId& id, int clientId)
	{
		if(mClientAwareObjects.size() > clientId) {
			mClientAwareObjects[clientId].erase(id);
		}
	}
    //-------------------------------------------------------------------------
	void NetworkSceneManager::setClientObjectAwareStatus(const ObjectId& id, int status, int clientId)
	{
		if(mClientAwareObjects.size() <= clientId) {
			mClientAwareObjects.resize(clientId + 1);
		}

		mClientAwareObjects[clientId][id] = status;
	}
}
