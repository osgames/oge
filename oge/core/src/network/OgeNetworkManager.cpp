/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/network/OgeNetworkManager.h"
#include "oge/network/OgeNetworkComponent.h"
#include "oge/network/OgeNetworkSceneManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    NetworkManager* NetworkManager::mManager = 0;
	static const ObjectId EmptyObjectId = "";
    //-------------------------------------------------------------------------
    NetworkManager::NetworkManager(const String& name, int systemInitOrdering, int systemRunOrdering) : 
        System(name, systemInitOrdering, systemRunOrdering),
		mConnectionStatus(NetworkManager::NOT_CONNECTED),
		mIsServer(false),
		mLastNetworkTickTime(0),
		mMaxConnections(255),
		mNetworkListener(NULL),
		mNetworkId(-1),
		mNumClients(0),
		mNetworkTick(0),
		mNetworkTickDelta(0),
		mNetworkTickRate(66),
		mServerName("no name server"),
		mServerNetworkTickRate(66),
		mZeroTickTime(0)
    {
        mManager = this;
        mSceneManagerEnumerator = new SceneManagerEnumerator("Network");

        LOG("Created " + name);
    }
    //-------------------------------------------------------------------------
    NetworkManager::~NetworkManager()
    {
        LOG("Destroyed ");
    }
    //-------------------------------------------------------------------------
    SceneManager* NetworkManager::createSceneManager( 
        const String& name, const String& type )
    {
        return mSceneManagerEnumerator->createSceneManager( name, type );
    }
    //-------------------------------------------------------------------------
	void NetworkManager::addClientControlledObject(const ObjectId& id, NetworkClientId networkId)
	{
		if(mClientControlledObjects.size() <= networkId) {
			mClientControlledObjects.resize(networkId + 1);
		}

		mClientControlledObjects[networkId].insert(id);
	}
    //-------------------------------------------------------------------------
    void NetworkManager::createDefaultSceneManager()
    {
        LOGI("Creating the default scene manager...");
        
        // This SM will be destroyed by the default ObjectSceneManager
        // as it will add it to its enumerator
        mSceneManager = static_cast<NetworkSceneManager*>(
            mSceneManagerEnumerator->createSceneManager(
                "OGE_Default_SceneManager", "Generic SM"));

        if (mSceneManager == 0) {
            LOGE("A SceneManager of type 'Generic SM' couldn't be created.");
            return;
        }
    }
	//-------------------------------------------------------------------------
	ObjectId NetworkManager::getClientControlledObject(NetworkClientId clientId)
	{
		if(!mClientControlledObjects[clientId].empty()) {
			return *(mClientControlledObjects[clientId].begin());
		}

		return EmptyObjectId;
	}
	//-------------------------------------------------------------------------
	ObjectId NetworkManager::getClientObjectId(const ObjectId& objectId)
	{
		ObjectIdMap::iterator ii = mNetworkObjectIds.find(objectId);
		if(ii != mNetworkObjectIds.end()) {
			return ii->second;
		}

		return EmptyObjectId;
	}
	//-------------------------------------------------------------------------
	Component* const NetworkManager::getComponent(const ObjectId& id, const ComponentType& family)
    {
		if(id.empty()) return NULL;
        return mSceneManager->getComponent(id, family);
    }
    //-------------------------------------------------------------------------
	NetworkManager::NetworkClientId NetworkManager::getObjectNetworkOwner(const ObjectId& id)
	{
		if(id.empty()) return -1;

		NetworkComponent* comp = static_cast<NetworkComponent*>(mSceneManager->getComponent(id, "Network"));
		if(comp) {
			return comp->getNetworkOwnerId();
		}

		return -1;
	}
    //-------------------------------------------------------------------------
    bool NetworkManager::initialise()
    {
        // Register the in-build component templates
        ObjectManager* objectMgr = ObjectManager::getSingletonPtr();
        objectMgr->registerComponentTemplate( "Network", 
			new NetworkComponentTemplateDefault() );

        // Registering the in-build SceneManagerFactories
        mSceneManagerEnumerator->registerSceneManagerFactory(
            new NetworkSceneManagerFactory());

		// Create the default scene manager
        createDefaultSceneManager();

        if (!System::initialise()) {
            LOGE("Initialisation failed!");
            return false;
        }

        return true;
    }
	//-------------------------------------------------------------------------
	void NetworkManager::postTick(double currentTime)
	{
		if(mSceneManager) {
			mSceneManager->postTick(currentTime);
		}
	}
	//-------------------------------------------------------------------------
	void NetworkManager::preTick(double currentTime)
	{
		if(mSceneManager) {
			mSceneManager->preTick(currentTime);
		}
	}
	//-------------------------------------------------------------------------
	bool NetworkManager::removeClientControlledObject(const ObjectId& id, NetworkClientId networkId)
	{
		if(networkId != -1) {
			mClientControlledObjects[networkId].erase(id);
		}
		else {
			for(int i = 0; i < mClientControlledObjects.size(); ++i) {
				mClientControlledObjects[i].erase(id);
			}
		}
		return true;
	}
    //-------------------------------------------------------------------------
    void NetworkManager::setNetworkListener(NetworkListener* listener)
    {
        mNetworkListener = listener;
    }
}
