/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/game/OgeGameManager.h"
#include "oge/game/OgeGameState.h"
#include "oge/graphics/OgeGraphicsManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    template<> GameManager* Singleton<GameManager>::mSingleton = 0;
    
    GameManager* GameManager::getSingletonPtr() 
    { 
        return mSingleton; 
    }
    GameManager& GameManager::getSingleton()
    {
        assert(mSingleton);
        return *mSingleton;
    }    
    const String GameManager::mDefaultGameStateName = "OGE_DEFAULT_GAME_STATE";
    //-------------------------------------------------------------------------
    GameManager::GameManager() : System("GameManager", 40, 50), mNextGameState(0)
    {
        mGameStates.deleteAll();
        
        mGameClock = GameClock::createSingleton();

        // There must be at least one game state (even if we don't use it)
        mGameState = new GameState(mDefaultGameStateName);
        addGameState(mGameState);
        LOG("GameManager created");
    }
    //-------------------------------------------------------------------------
    GameManager::~GameManager()
    {
        GameClock::destroySingleton();

        LOG("GameManager destroyed");
    }
    //-------------------------------------------------------------------------
    bool GameManager::initialise()
    {
        LOG("Initialise GameManager...");

        if (!System::initialise())
        {
            LOGE("Initialisation failed!");
            return false;
        }

        mGameClock->setModeClampedFps( 20.0f, 60.0f ); // TODO to test

        return true;
    }
    //-------------------------------------------------------------------------
    void GameManager::shutdown()
    {
        LOG("Shutdown GameManager");
        mGameClock->setStarted(false);

        System::shutdown();
        // Delete the default game state
        mGameStates.deleteEntry(mDefaultGameStateName);
    }
	//-------------------------------------------------------------------------
	void GameManager::postTick(double systemTime)
	{
		// do nothing
	}
	//-------------------------------------------------------------------------
	void GameManager::preTick(double systemTime)
	{
		// do nothing
	}
    //-------------------------------------------------------------------------
    void GameManager::tick(double systemTime)
    {
        // The game time is updated (if not in pause mode)
        mGameClock->_update(systemTime);

        // TODO if the swap() is too slow there is a risk of calling it twice
        //      add a recursive mutex in swapState() ?
        if (mNextGameState)
            swapState(); 
        mGameState->tick(systemTime);
    }
    //-------------------------------------------------------------------------
    void GameManager::swapState()
    {
// TMP THIS IS TO AVOID AN MT problem the ogre and its plugins where not initialised yet
/*
if (!GraphicsManager::getManager() || !GraphicsManager::getManager()->_getSystemController())
    return;
// || GraphicsManager::getManager()->getState() == SystemController::SCHEDULED
if (GraphicsManager::getManager()->getState() != SystemController::INITIALISED)
    return;*/


        if (mNextGameState == 0)
        {
            LOGE("TODO fix this! This shouldn't happen. There should be a mutex.");
        }

        // It will also test if the state is in a "deactivable" state
        if (mNextGameState && mGameState->deactivate())
        {
            mGameState = mNextGameState;
            mNextGameState = 0;

            if (mGameState->activate(mNextGameStateParam))
                LOG("Activate game state: " + mGameState->getName())
            else
                LOGE("Failed to activate game state: " + mGameState->getName())


/* todo prospective code but risk of infinite loop
            if (mNextGameState->activate(mNextGameStateParam))
            {
                mGameState = mNextGameState;
                mNextGameState = 0;
                LOG("Game State is now " + mGameState->getName());
            }
            else // Activation failed!
            {
                LOGE("Failed to activate game state: " + mNextGameState->getName());
                mGameState->activate();
                mNextGameState = 0;
            }
*/
        }
    }
    //-------------------------------------------------------------------------
    bool GameManager::setNextGameState(const String& name, const Any& param)
    {
        mNextGameState = mGameStates.getEntry(name);
        if (mNextGameState == 0)
            return false;
        mNextGameStateParam = param;
        return true;
    }
    //-------------------------------------------------------------------------
    bool GameManager::addGameState(GameState* gameState)
    {
        const String& name = gameState->getName();

        if (mGameStates.entryExists(name))
            return false;

        mGameStates.addEntry(name, gameState);
        return true;
    }
    //-------------------------------------------------------------------------
    void GameManager::removeGameState(const String& name)
    {
        if (name != mGameState->getName())
        {
            mGameStates.removeEntry(name);
        }
    }
    //-------------------------------------------------------------------------
    void GameManager::removeAllGameStates()
    {
        mGameStates.removeAll();
    }
    //-------------------------------------------------------------------------
    String GameManager::getCurrentGameStateName()
    {
        return mGameState->getName();
    }
    //-------------------------------------------------------------------------
    GameState* GameManager::getCurrentGameState()
    {
        return mGameState;
    }
    //-------------------------------------------------------------------------
    GameState* GameManager::getGameState(const String& name)
    {
        return mGameStates.getEntry(name);
    }
    //-------------------------------------------------------------------------
    GameManager* GameManager::createSingleton()
    {
        if (!mSingleton)
            mSingleton = new GameManager();
        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void GameManager::destroySingleton()
    {
        if (mSingleton)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //-------------------------------------------------------------------------
}
