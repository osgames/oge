/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/game/OgeGameClock.h"

namespace oge
{
    //-------------------------------------------------------------------------
    template<> GameClock* Singleton<GameClock>::mSingleton = 0;
    //-------------------------------------------------------------------------
    GameClock* GameClock::getSingletonPtr() 
    { 
        return mSingleton; 
    }
    GameClock& GameClock::getSingleton()
    {
        assert(mSingleton);
        return *mSingleton;
    }
    //-------------------------------------------------------------------------
    GameClock::GameClock()
        : mStarted(false), mClockMode(OGE_CLOCK_REALTIME),
        mTime(0.0f), mLastSystemTime(0), mTimeElapsed(0), mTimeSpeed(1.0f),
        mServerTime(0.0f), mNbUpdatesToSyncOver(60), mNbUpdatesSync(60),
        mConstantStep(1.0f/60.0f), mClampedMinStep(1.0f/15.0f),
        mClampedMaxStep(1.0f/60.0f), mTotalSystemTimeElapsed(0)
    {
        LOG("GameClock created");
    }
    //-------------------------------------------------------------------------
    GameClock::~GameClock()
    {
        LOG("GameClock destroy");
    }
    //-------------------------------------------------------------------------
    GameClock* GameClock::createSingleton()
    {
        if (!mSingleton)
            mSingleton = new GameClock();
        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void GameClock::destroySingleton()
    {
        if (mSingleton)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //-------------------------------------------------------------------------
    void GameClock::setModeRealtime()
    {
        mClockMode = OGE_CLOCK_REALTIME;
    }
    //-------------------------------------------------------------------------
    void GameClock::setModeConstantStep(double step)
    {
        mConstantStep = step;
        mClockMode = OGE_CLOCK_CONSTANT;
    }
    //-------------------------------------------------------------------------
    void GameClock::setModeClampedStep(double min, double max)
    {
        mClampedMinStep = min;
        mClampedMaxStep = max;
        mClockMode = OGE_CLOCK_CLAMPED;
    }
    //-------------------------------------------------------------------------
    void GameClock::setModeClampedFps(double minFps, double maxFps)
    {
        mClampedMinStep = 1.0f/minFps;
        mClampedMaxStep = 1.0f/maxFps;
        mClockMode = OGE_CLOCK_CLAMPED;
    }
    //-------------------------------------------------------------------------
    void GameClock::setTimeSpeed(double speed)
    {
        LOGW("Time speed was set to "+StringUtil::toString(oge::Real(speed)));
        mTimeSpeed = speed;
    }
    //-------------------------------------------------------------------------
    void GameClock::setTime(double gameTime)
    {
        mTime = gameTime;
    }
    //-------------------------------------------------------------------------
    void GameClock::setServerTime(double serverTime)
    {
        LOGWC(mServerTime < serverTime, "The received server time is inferior to the stored serverTime!");
        mServerTime = serverTime;
        // Until nbUpdatesToSyncRemaining is zero the time will be adjusted
        mNbUpdatesSync = 0;
    }
    //-------------------------------------------------------------------------
    void GameClock::setServerTimeNow(double serverTime)
    {
        LOGWC(mServerTime < serverTime, "The received server time is inferior to the stored serverTime!");
        mServerTime = serverTime;
        mTime = serverTime;
    }
    //-------------------------------------------------------------------------
    void GameClock::setStarted(bool start)
    {
        mStarted = start;
    }
    //-------------------------------------------------------------------------
    void GameClock::setServerAdjustRate(unsigned int nbUpdates)
    {
        mNbUpdatesToSyncOver = nbUpdates;
    }
    //-------------------------------------------------------------------------
    double GameClock::_update(double systemTime)
    {
        // If not started or in pause mode then no time has passed.
        if (!mStarted)
        {
            mLastSystemTime = systemTime;
            return mTime;
        }

        mTimeElapsed = systemTime - mLastSystemTime;
        mLastSystemTime = systemTime;
        mTotalSystemTimeElapsed += mTimeElapsed;

        switch(mClockMode)
        {
        case OGE_CLOCK_REALTIME:
            mTimeElapsed *= mTimeSpeed;
            break;
        case OGE_CLOCK_CONSTANT:
            mTimeElapsed = mConstantStep;
            break;
        case OGE_CLOCK_CLAMPED:
            if (mTimeElapsed < mClampedMaxStep)
            {
                mTimeElapsed = mClampedMaxStep;
            }
            else if (mTimeElapsed > mClampedMinStep)
            {
                mTimeElapsed = mClampedMinStep;
            }
            mTimeElapsed *= mTimeSpeed;
            break;
        default:
            mTimeElapsed *= mTimeSpeed;
            mClockMode = OGE_CLOCK_REALTIME;
            LOGW("Invalid clock mode defined: setting it to OGE_CLOCK_REALTIME");
        }

        mTime += mTimeElapsed;

        // WIP Needs to be tested more :)
        if (mNbUpdatesSync < mNbUpdatesToSyncOver)
        {
            // By putting it here (instead of the end of the calcul
            // we ensure that the division will have 1 as result
            // hence that mSever & mTime will be in sync
            ++mNbUpdatesSync;

            mServerTime += mTimeElapsed;
            double diff = mServerTime - mTime;
             // last must have *1  aka diff * 1
            mTime += diff * (mNbUpdatesSync / mNbUpdatesToSyncOver);
        }

        return mTime;
    }
    //-------------------------------------------------------------------------
    double GameClock::getClampedMaxStep()
    {
        return mClampedMaxStep;
    }
    //-------------------------------------------------------------------------
    double GameClock::getClampedMinStep()
    {
        return mClampedMinStep;
    }
    //-------------------------------------------------------------------------
    double GameClock::getConstantStep()
    {
        return mConstantStep;
    }
    //-------------------------------------------------------------------------
    double GameClock::getLastSystemTime()
    {
        return mLastSystemTime;
    }
    //-------------------------------------------------------------------------
    double GameClock::getTotalSystemTimeElapsed()
    {
        return mTotalSystemTimeElapsed;
    }
    //-------------------------------------------------------------------------
}
