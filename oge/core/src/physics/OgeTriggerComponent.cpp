/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/physics/OgeTriggerComponent.h"
#include "oge/physics/OgePhysicsManager.h"
#include "oge/serialisation/OgeSerialiser.h"
#include "oge/object/OgeObject.h"
#include "oge/object/OgeObjectManager.h"
#include "oge/math/OgeMath.h"
#include "oge/graphics/OgeGraphicsComponent.h"
#include "oge/message/OgeMessageList.h"

namespace oge
{
    //-------------------------------------------------------------------------
    void TriggerComponent::triggerCallbacks()
    {
        // Check if an object is not present anymore and triggers an OnExit event
        std::set<ObjectId>::iterator iter;
        for ( iter = mObjects.begin(); iter != mObjects.end(); )
        {
            if (mObjectsNow.find(*iter) == mObjectsNow.end())
            {
				// tell other components of this object about this trigger event
		        ObjectManager::getSingleton().postMessage(
					ObjectMessage::TRIGGER_ON_EXIT, *iter, getObjectId(), 0, getObjectId() );

                triggerCallback( *iter, OnExit );
                mObjects.erase(iter++);
            }
            else
                ++iter;
        }

        // Compare the actual objects in the zone and those in the history
        std::set<ObjectId>::const_iterator citer;
        for ( citer = mObjectsNow.begin(); citer != mObjectsNow.end(); ++citer)
        {
            // Find it in history trigger an OnInside event
            if (mObjects.find(*citer) != mObjects.end())
            {
                triggerCallback( *citer, OnInside );
            }
            else // Adds it to history and triggers a OnEnter event
            {
				// tell other components of this object about this trigger event
		        ObjectManager::getSingleton().postMessage(
					ObjectMessage::TRIGGER_ON_ENTER, *citer, getObjectId(), 0, getObjectId() );

                mObjects.insert( *citer );
                triggerCallback( *citer, OnEnter );
            }
        }

        mObjectsNow.clear();
    }
    //-------------------------------------------------------------------------
    void TriggerComponent::triggerCallback(ObjectId id, EventType type)
    {
        LOGIC(type > OnLast, "Invalid event type: event ignored")
        std::map<EventType, Callback>::const_iterator iter =
            mCallbacks.find(type);
        if (mCallbacks.end() != iter)
            (iter->second)(getObjectId(), id);
    }
    //-------------------------------------------------------------------------


    //-------------------------------------------------------------------------
    bool TriggerComponentDefault::_reset(const MessageList& params, SceneManager* sceneManager)
    {
        MessageList::ConstIter it = params.list.begin();

        for (; it != params.list.end(); ++it)
        {
            _processMessage( (*it) );
        }

        return true;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::_destroy(SceneManager* sceneManager)
    {
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::_activate(bool activate, SceneManager* sceneManager)
    {
    }
    //-------------------------------------------------------------------------
    Component::UpdateRate TriggerComponentDefault::_getUpdateRate() const
    {
        return Component::NO_UPDATE; // EVERY_TICK; // TODO only for dynamic comp? also for static?
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::_update(double deltaTime)
    {
        LOGW("Should not be called! This component being set as NO_UPDATE.");
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::write(Serialiser* serialisePtr)
    {
        serialisePtr->writeVector3(mPosition);
        serialisePtr->writeVector3(mScale);
        serialisePtr->writeQuaternion(mOrientation);
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::read(Serialiser* serialisePtr)
    {
        setPosition(serialisePtr->readVector3());
        setScale(serialisePtr->readVector3());
        setOrientation(serialisePtr->readQuaternion());
    }
    //-------------------------------------------------------------------------
    bool TriggerComponentDefault::fixup()
    {
        // no fixup to do
        return true;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::setPosition(const Vector3& position)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_POSITION, position, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::setScale(const Vector3& scale)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_SCALE, scale, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::setDimensions(const Vector3& dimensions)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_DIMENSIONS, dimensions, Any(), 0, getObjectId() );
/*_ old code
        Vector3 scale = dimensions / getDimensions();
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_SCALE, scale, Any(), 0, getObjectId() );
*/
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::setOrientation(const Quaternion& orientation)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_ORIENTATION, orientation, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::setOrientationEuler(const Vector3& angles)
    {
//#pragma OGE_WARN("FIXME/TODO: This code perhaps works in Ogre. Similar functions should be added to OGE.")
    //    Matrix3 rot;
    //    rot.FromEulerAnglesYXZ(Degree(angles.y), Degree(angles.x), Degree(angles.z));
    //    Quaternion qrot;
    //    qrot.FromRotationMatrix(rot);
    // 
    //    ObjectManager::getSingleton().postMessage(
    //        ObjectMessage::SET_ORIENTATION, qrot, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::setPositionOrientation(const Vector3& position, const Quaternion& orientation)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_POSITION_ORIENTATION, position, orientation, 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    const Vector3& TriggerComponentDefault::getDimensions() const
    {
        return mDimensions;
    }
    //-------------------------------------------------------------------------
    Vector3 TriggerComponentDefault::getEulerOrientation() const
    {
//#pragma OGE_WARN("TODO: This code perhaps works in Ogre. Functions should be added to OGE")
    //    Matrix3 rot;
    //    mOrientation.ToRotationMatrix(rot);
    //    Radian yAngle, pAngle, rAngle;
    //    rot.ToEulerAnglesYXZ(yAngle, pAngle, rAngle);
    //    return Vector3(yAngle.valueDegrees(), pAngle.valueDegrees(), rAngle.valueDegrees());
          return Vector3();
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::_setPositionByMessage(const Message& message)
    {
// TODO really useful this assert() ? Would we not anyway segfault hence unncessary assert?
        const Vector3 *pos = FastAnyCast(Vector3, &message.getParam_1());
        assert(pos);
        mPosition = *pos;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::_setScaleByMessage(const Message& message)
    {
        const Vector3 *scale = FastAnyCast(Vector3, &message.getParam_1());
        assert(scale);
        mScale = *scale;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::_setDimensionsByMessage(const Message& message)
    {
        const Vector3 *dim = FastAnyCast(Vector3, &message.getParam_1());
        assert(dim);
//_        // Do what setDimensions() does, but request to process the message immediately.
//_        // NOTE: mScale will be set again.
//_        mScale = *dim / getDimensions();
//_        ObjectManager::getSingleton().postMessage( 
//_            ObjectMessage::SET_SCALE, mScale, Any(), 0, getObjectId(), Message::IMMEDIATE );
        mDimensions = *dim;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::_setOrientationByMessage(const Message& message)
    {
        const Quaternion *orient = FastAnyCast(Quaternion, &message.getParam_1());
        assert(orient);
        mOrientation = *orient;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::_setEulerOrientationByMessage(const Message& message)
    {
        const Vector3 *angles = FastAnyCast(Vector3, &message.getParam_1());
        assert(angles);
        setOrientationEuler(*angles); // NOTE: Will send another message.
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::_setPositionOrientationByMessage(const Message& message)
    {
        const Vector3 *pos = FastAnyCast(Vector3, &message.getParam_1());
        const Quaternion *orient = FastAnyCast(Quaternion, &message.getParam_2());
        assert(pos && orient);
        mPosition = *pos;
        mOrientation = *orient;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentDefault::_processMessage(const Message& message)
    {
        switch (message.getType())
        {
            case ObjectMessage::SET_POSITION: _setPositionByMessage( message ); return;
            case ObjectMessage::SET_SCALE: _setScaleByMessage( message ); return;
            case ObjectMessage::SET_DIMENSIONS: _setDimensionsByMessage( message ); return;
            case ObjectMessage::SET_ORIENTATION: _setOrientationByMessage( message ); return;
            case ObjectMessage::SET_ORIENTATION_EULER: _setEulerOrientationByMessage( message ); return;
            case ObjectMessage::SET_POSITION_ORIENTATION: _setPositionOrientationByMessage( message ); return;
            // NEXT default: ++counterOfUnnecessaryMessageProcessedByAComponent;
        }
    }
    //-------------------------------------------------------------------------
}
