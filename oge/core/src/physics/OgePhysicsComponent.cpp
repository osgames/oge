/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/physics/OgePhysicsComponent.h"
#include "oge/physics/OgePhysicsManager.h"
#include "oge/engine/OgeEngineManager.h"
#include "oge/serialisation/OgeSerialiser.h"
#include "oge/object/OgeObject.h"
#include "oge/object/OgeObjectManager.h"
#include "oge/math/OgeMath.h"
#include "oge/graphics/OgeGraphicsComponent.h"
#include "oge/message/OgeMessageList.h"

namespace oge
{
    //-------------------------------------------------------------------------
    bool PhysicsComponentDefault::_reset(const MessageList& params, SceneManager* sceneManager)
    {
        MessageList::ConstIter it = params.list.begin();

        for (; it != params.list.end(); ++it) {
            _processMessage( (*it) );
        }

        return true;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_destroy(SceneManager* sceneManager)
    {
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_activate(bool activate, SceneManager* sceneManager)
    {
    }
    //-------------------------------------------------------------------------
    Component::UpdateRate PhysicsComponentDefault::_getUpdateRate() const
    {
        return Component::NO_UPDATE;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_update(double deltaTime)
    {
        LOGW("Should not be called! This component being set as NO_UPDATE.");
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::write(Serialiser* serialisePtr)
    {
        serialisePtr->writeVector3(mPosition);
        serialisePtr->writeVector3(mScale);
        serialisePtr->writeQuaternion(mOrientation);
		serialisePtr->writeVector3(mAngularVelocity);
		serialisePtr->writeFloat(mMaxAngularVelocity);
		serialisePtr->writeVector3(mVelocity);
		serialisePtr->writeFloat(mMaxVelocity);
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::read(Serialiser* serialisePtr)
    {
        setPosition(serialisePtr->readVector3());
        setScale(serialisePtr->readVector3());
        setOrientation(serialisePtr->readQuaternion());
		setAngularVelocity(serialisePtr->readVector3());
		setMaxAngularVelocity(serialisePtr->readFloat());
		setVelocity(serialisePtr->readVector3());
		setMaxVelocity(serialisePtr->readFloat());
    }
    //-------------------------------------------------------------------------
    bool PhysicsComponentDefault::fixup()
    {
        // no fixup to do
        return true;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::setAngularVelocity(const Vector3& angularVelocity)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_ANGULAR_VELOCITY, angularVelocity, Any(), 0, getObjectId() );
    }
	//-------------------------------------------------------------------------
	void PhysicsComponentDefault::setMaxAngularVelocity(const Real maxAngularVelocity)
	{
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_MAX_ANGULAR_VELOCITY, maxAngularVelocity, Any(), 0, getObjectId() );
	}
	//-------------------------------------------------------------------------
	void PhysicsComponentDefault::setMaxVelocity(const Real maxVelocity)
	{
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_MAX_VELOCITY, maxVelocity, Any(), 0, getObjectId() );
	}
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::setPosition(const Vector3& position)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_POSITION, position, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::setScale(const Vector3& scale)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_SCALE, scale, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::setDrag(const Real drag)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_DRAG, drag, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::setDimensions(const Vector3& dimensions)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_DIMENSIONS, dimensions, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::setOrientation(const Quaternion& orientation)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_ORIENTATION, orientation, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::setOrientationEuler(const Vector3& angles)
    {
//#pragma OGE_WARN("FIXME/TODO: This code perhaps works in Ogre. Similar functions should be added to OGE.")
    //    Matrix3 rot;
    //    rot.FromEulerAnglesYXZ(Degree(angles.y), Degree(angles.x), Degree(angles.z));
    //    Quaternion qrot;
    //    qrot.FromRotationMatrix(rot);
    // 
    //    ObjectManager::getSingleton().postMessage(
    //        ObjectMessage::SET_ORIENTATION, qrot, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::setPositionOrientation(const Vector3& position, const Quaternion& orientation)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_POSITION_ORIENTATION, position, orientation, 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::setVelocity(const Vector3& velocity)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_VELOCITY, velocity, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    const Vector3& PhysicsComponentDefault::getDimensions() const
    {
        return mDimensions;
    }
    //-------------------------------------------------------------------------
    Vector3 PhysicsComponentDefault::getEulerOrientation() const
    {
//#pragma OGE_WARN("TODO: This code perhaps works in Ogre. Functions should be added to OGE")
    //    Matrix3 rot;
    //    mOrientation.ToRotationMatrix(rot);
    //    Radian yAngle, pAngle, rAngle;
    //    rot.ToEulerAnglesYXZ(yAngle, pAngle, rAngle);
    //    return Vector3(yAngle.valueDegrees(), pAngle.valueDegrees(), rAngle.valueDegrees());
          return Vector3();
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_setPositionByMessage(const Message& message)
    {
        const Vector3 *pos = FastAnyCast(Vector3, &message.getParam_1());
        assert(pos);
        mPosition = *pos;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_setScaleByMessage(const Message& message)
    {
        const Vector3 *scale = FastAnyCast(Vector3, &message.getParam_1());
        assert(scale);
        mScale = *scale;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_setDragByMessage(const Message& message)
    {
        const double *drag = FastAnyCast(double, &message.getParam_1());
        assert(drag);
		mDrag = (oge::Real)*drag;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_setDimensionsByMessage(const Message& message)
    {
        const Vector3 *dim = FastAnyCast(Vector3, &message.getParam_1());
        assert(dim);
        mDimensions = *dim;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_setOrientationByMessage(const Message& message)
    {
        const Quaternion *orient = FastAnyCast(Quaternion, &message.getParam_1());
        assert(orient);
        mOrientation = *orient;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_setEulerOrientationByMessage(const Message& message)
    {
        const Vector3 *angles = FastAnyCast(Vector3, &message.getParam_1());
        assert(angles);
        setOrientationEuler(*angles);
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_setPositionOrientationByMessage(const Message& message)
    {
        const Vector3 *pos = FastAnyCast(Vector3, &message.getParam_1());
        const Quaternion *orient = FastAnyCast(Quaternion, &message.getParam_2());
        assert(pos && orient);
        mPosition = *pos;
        mOrientation = *orient;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_setAngularVelocityByMessage(const Message& message)
    {
        const Vector3 *angularVelocity = FastAnyCast(Vector3, &message.getParam_1());
        assert(angularVelocity);
        mAngularVelocity = *angularVelocity;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_setMaxAngularVelocityByMessage(const Message& message)
    {
        const double *max = FastAnyCast(double, &message.getParam_1());
		assert(max);
		mMaxAngularVelocity = (oge::Real)*max;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_setMaxVelocityByMessage(const Message& message)
    {
        const double *max = FastAnyCast(double, &message.getParam_1());
		assert(max);
		mMaxVelocity = (oge::Real)*max;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_setVelocityByMessage(const Message& message)
    {
        const Vector3 *velocity = FastAnyCast(Vector3, &message.getParam_1());
        assert(velocity);
        mVelocity = *velocity;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentDefault::_processMessage(const Message& message)
    {
        switch (message.getType())
        {
            case ObjectMessage::SET_ANGULAR_VELOCITY: _setAngularVelocityByMessage( message ); return;
            case ObjectMessage::SET_DIMENSIONS: _setDimensionsByMessage( message ); return;
			case ObjectMessage::SET_DRAG: _setDragByMessage( message ); return;
            case ObjectMessage::SET_MAX_ANGULAR_VELOCITY: _setMaxAngularVelocityByMessage( message ); return;
			case ObjectMessage::SET_MAX_VELOCITY: _setMaxVelocityByMessage( message ); return;
            case ObjectMessage::SET_ORIENTATION: _setOrientationByMessage( message ); return;
            case ObjectMessage::SET_ORIENTATION_EULER: _setEulerOrientationByMessage( message ); return;
            case ObjectMessage::SET_POSITION: _setPositionByMessage( message ); return;
            case ObjectMessage::SET_POSITION_ORIENTATION: _setPositionOrientationByMessage( message ); return;
            case ObjectMessage::SET_SCALE: _setScaleByMessage( message ); return;
            case ObjectMessage::SET_VELOCITY: _setVelocityByMessage( message ); return;
        }
    }
}