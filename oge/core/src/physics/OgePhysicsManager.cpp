/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/physics/OgePhysicsManager.h"
#include "oge/physics/OgePhysicsComponent.h"
#include "oge/object/OgeObjectManager.h"
#include "oge/scene/OgeSceneManager.h"
#include "oge/physics/OgePhysicsSceneManager.h"
#include "oge/physics/OgeTriggerComponent.h"

namespace oge
{
    //-------------------------------------------------------------------------
    PhysicsManager* PhysicsManager::mManager = 0;
    //-------------------------------------------------------------------------
    PhysicsManager::PhysicsManager(const String& name, int systemInitOrdering, int systemRunOrdering) : 
        System(name, systemInitOrdering, systemRunOrdering)
    {
        mManager = this;
        mSceneManagerEnumerator = new SceneManagerEnumerator("Physics");

        LOG("Created " + name);
    }
    //-------------------------------------------------------------------------
    PhysicsManager::~PhysicsManager()
    {
        delete mSceneManagerEnumerator;
        mSceneManagerEnumerator = 0;
        LOG("Destroyed ");
    }
    //-------------------------------------------------------------------------
    bool PhysicsManager::initialise()
    {
        // Register the in-build component templates
        ObjectManager* objectMgr = ObjectManager::getSingletonPtr();
        objectMgr->registerComponentTemplate( "Physics", new PhysicsComponentTemplateDefault() );
        // TODO but we will also need to implement PhysicsManager::tick() { test &process collisions }
        //      The default physcis SM will need to cycle through each trigger and test if object he must react to are in the radius
        //objectMgr->registerComponentTemplate( "Physics", new TriggerComponentTemplateDefault() );

        // Registering the in-build SceneManagerFactories
        mSceneManagerEnumerator->registerSceneManagerFactory(
            new PhysicsSceneManagerFactory());


        //if (mCreateDefaultSceneManager) // NEXT can be set by config?
            createDefaultSceneManager();

        if (!System::initialise())
        {
            LOGE("Initialisation failed!");
            return false;
        }

        return true;
    }
    //-------------------------------------------------------------------------
    SceneManager* PhysicsManager::createSceneManager( 
        const String& name, const String& type )
    {
        return mSceneManagerEnumerator->createSceneManager( name, type );
    }
    //-------------------------------------------------------------------------
    void PhysicsManager::createDefaultSceneManager()
    {
        LOGI("Creating the default scene manager...");
        
        // This SM will be destroyed by the default ObjectSceneManager
        // as it will add it to its enumerator
        mSceneManager = static_cast<PhysicsSceneManager*>(
            mSceneManagerEnumerator->createSceneManager(
                "OGE_Default_SceneManager", "Generic SM"));

        if (mSceneManager == 0)
        {
            LOGE("A SceneManager of type 'Generic SM' couldn't be created.");
            return;
        }
    }
    //-------------------------------------------------------------------------
	void PhysicsManager::postTick(double currentTime)
	{
		if(mSceneManager) {
			mSceneManager->postTick(currentTime);
		}
	}
	//-------------------------------------------------------------------------
	void PhysicsManager::preTick(double currentTime)
	{
		if(mSceneManager) {
			mSceneManager->preTick(currentTime);
		}
	}
}
