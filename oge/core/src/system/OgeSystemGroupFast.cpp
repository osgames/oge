/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/system/OgeSystemGroupFast.h"
#include "oge/task/OgeDelegateTask.h"
#include "oge/system/OgeSystem.h"
#include "oge/OgeProfiler.h"

namespace oge
{
    //-------------------------------------------------------------------------
    SystemGroupFast::SystemGroupFast(const String& name, 
        SystemGroupFactory* creator) :  SystemGroup(name, creator),
        mAlreadyTicked(false)
    {

    }
    //-------------------------------------------------------------------------
    SystemGroupFast::~SystemGroupFast()
    {

    }
    //-------------------------------------------------------------------------
    void SystemGroupFast::notifyCurrentTime(double initialTime)
    {
        typedef DelegateTask1<double, void> DT;
        DT* fn = new DT(initialTime, 
            DT::Delegate(this, &SystemGroupFast::updateSystemsCurrentTime));

        mThread->addTask(fn);
    }
    //-------------------------------------------------------------------------
    void SystemGroupFast::updateSystemsCurrentTime(double currentTime)
    {
        mCurrentTime = currentTime;

        size_t size = mScheduledSystems.size();
        for (unsigned int index = 0; index < size; ++index)
        {
            SystemInfo& info = *mScheduledSystems[index];
            info.system->notifyScheduled(mCurrentTime);
            info.nextTickTime = mCurrentTime + info.tickInterval;
        }
    }
    //-------------------------------------------------------------------------
    void SystemGroupFast::notifyDettachedFromThread()
    {
        while(mRegisteredSystems.size() > 0)
        {
            // Try to unregister the system and if it fails, remove it manually
            // to prevent it causing any further problems
            if (!unregisterSystem(mRegisteredSystems[0]->system))
            {
                LOGW("Unregistering a system failed: erasing it manually.");
                mRegisteredSystems.erase(mRegisteredSystems.begin());
            }
        }
    }
    //-------------------------------------------------------------------------
    void SystemGroupFast::tick(double currentTime)
    {
        if (mAlreadyTicked)
            return;

        mAlreadyTicked = true;

        typedef DelegateTask1<double, void> DT;

        DT* fn = new DT(currentTime,
            DT::Delegate(this, &SystemGroupFast::tickAll));

        mThread->addTask(fn);
    }
    //-------------------------------------------------------------------------
    void SystemGroupFast::tickAll(double currentTime)
    {
        // TODO: Necessary to keep the time in this class?
        mCurrentTime = currentTime; 
		OgeProfileGroup("tickAll", OGEPROF_SYSTEMS);

        size_t size = mScheduledSystems.size();
		double startTime = mClock.updateAndGetTime();
		// pre-tick
        for (unsigned int index = 0; index < size; ++index)
        {
            SystemInfo& info = *mScheduledSystems[index];
            if (currentTime >= info.nextTickTime)
            {
				info.lastProcessTime = info.curProcessTime;
				info.curProcessTime = 0;
				startTime =  mClock.getTime();
				info.system->preTick(mCurrentTime);
				mClock.update();
				info.curProcessTime += mClock.getTime() - startTime;
            }
        }

		// tick
        for (unsigned int index = 0; index < size; ++index)
        {
            SystemInfo& info = *mScheduledSystems[index];
            if (currentTime >= info.nextTickTime)
            {
				startTime =  mClock.getTime();
                info.system->tick(mCurrentTime);
				mClock.update();
				info.curProcessTime += mClock.getTime() - startTime;
            }
        }

		// post-tick
        for (unsigned int index = 0; index < size; ++index)
        {
            SystemInfo& info = *mScheduledSystems[index];
            if (currentTime >= info.nextTickTime)
            {
				startTime =  mClock.getTime();
				info.system->postTick(mCurrentTime);
				info.system->setLastTickTime(mCurrentTime);
                info.nextTickTime = currentTime + info.tickInterval;

				mClock.update();
				info.curProcessTime += mClock.getTime() - startTime;
				info.avgProcessTime = (info.avgProcessTime + info.curProcessTime) * 0.5;
            }
        }
        mAlreadyTicked = false;
    }
    //-------------------------------------------------------------------------
    AsyncBool SystemGroupFast::requestSetTickInterval(System* system, 
        double tickInterval)
    {
        typedef DelegateTask2<System*, double, bool> DT;
        DT* fn = new DT(system, tickInterval,
            DT::Delegate(this, &SystemGroupFast::setTickInterval));

        AsyncBool result = fn->getResult();
        mThread->addTask(fn);
        return result;
    }
    //-------------------------------------------------------------------------
    bool SystemGroupFast::setTickInterval(System* system, double tickInterval)
    {
        SystemInfo* info = getSystemInfo(system);

        // Check if the system has been registered with this group
        if (!info) {
            return false;
        }

        // Set the tick interval and update the when the system should be ticked
        info->tickInterval = tickInterval;
        info->nextTickTime = mCurrentTime + tickInterval;

        return true;
    }
    //-------------------------------------------------------------------------
    void SystemGroupFast::addRegisteredSystem(System* system, 
        double tickInterval)
    {
        SystemInfo* info = new SystemInfo();
        info->system = system;
        info->tickInterval = tickInterval;
        info->nextTickTime = mCurrentTime;
		info->avgProcessTime = 0;
		info->lastProcessTime = 0;
		info->curProcessTime = 0;
        mRegisteredSystems.push_back(info);
    }
    //-------------------------------------------------------------------------
    void SystemGroupFast::removeRegisteredSystem(System* system)
    {
        size_t size = mRegisteredSystems.size();
        for (unsigned int index = 0; index < size; ++index)
        {
            SystemInfo* info = mRegisteredSystems[index];
            if (info->system == system)
            {
                mRegisteredSystems.erase(mRegisteredSystems.begin() + index);
                delete info;
                break;
            }
        }
    }
    //-------------------------------------------------------------------------
    void SystemGroupFast::addScheduledSystem(System* system)
    {
        size_t size = mRegisteredSystems.size();
        for (unsigned int index = 0; index < size; ++index)
        {
            SystemInfo* info = mRegisteredSystems[index];
            if (info->system == system)
            {
				if(mScheduledSystems.empty()) {
					mScheduledSystems.push_back(info);
				}
				else {
					// insert into the schedule based on the run order
					SystemInfoVector::iterator ii;
					for(ii = mScheduledSystems.begin(); ii != mScheduledSystems.end(); ++ii) {
						if((*ii)->system->getSystemRunOrdering() > system->getSystemRunOrdering()) {
							break;
						}
					}
					mScheduledSystems.insert(ii, info);
				}
                break;
            }
        }
    }
    //-------------------------------------------------------------------------
    void SystemGroupFast::removeScheduledSystem(System* system)
    {
        size_t size = mScheduledSystems.size();
        for (unsigned int index = 0; index < size; ++index)
        {
            SystemInfo* info = mScheduledSystems[index];
            if (info->system == system)
            {
                mScheduledSystems.erase(mScheduledSystems.begin() + index);
                break;
            }
        }
    }
    //-------------------------------------------------------------------------
    bool SystemGroupFast::isSystemRegistered(System* system) const
    {
        size_t size = mRegisteredSystems.size();
        for (unsigned int index = 0; index < size; ++index)
        {
            SystemInfo* info = mRegisteredSystems[index];
            if (info->system == system)
            {
                return true;
            }
        }

        return false;
    }
	//-------------------------------------------------------------------------
	double SystemGroupFast::getTickInterval(System* system)
	{
        size_t size = mRegisteredSystems.size();
        for(size_t index = 0; index < size; ++index)
        {
            SystemInfo* info = mRegisteredSystems[index];
            if (info->system == system) {
				return info->tickInterval;
            }
        }

        return 0.0;
	}
    //-------------------------------------------------------------------------
    SystemGroupFast::SystemInfo* SystemGroupFast::getSystemInfo(System* system)
    {
        size_t size = mRegisteredSystems.size();
        for(size_t index = 0; index < size; ++index)
        {
            SystemInfo* info = mRegisteredSystems[index];
            if (info->system == system)
            {
                return info;
            }
        }

        return 0;
    }
    //-------------------------------------------------------------------------
    SystemGroup::SystemStats SystemGroupFast::getSystemStats(System* system)
	{
		SystemGroup::SystemStats stats;
		stats.avgProcessTime = 0;
		stats.lastProcessTime = 0;

		SystemInfo *info = this->getSystemInfo(system);
		if(info) {
			stats.avgProcessTime = info->avgProcessTime;
			stats.lastProcessTime = info->lastProcessTime;
		}

		return stats;
	}
    //-------------------------------------------------------------------------
    SystemGroupFast::SystemInfo::SystemInfo()
    {

    }
    //-------------------------------------------------------------------------
}
