/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/system/OgeSystemGroup.h"
#include "oge/task/OgeDelegateTask.h"
#include "oge/system/OgeSystem.h"

namespace oge
{
    //-------------------------------------------------------------------------
    SystemGroup::SystemGroup(const String& name, SystemGroupFactory* creator) :
        mCurrentTime(0), mCreator(creator)
    {

    }
    //-------------------------------------------------------------------------
    SystemGroup::~SystemGroup()
    {
        
    }
    //-------------------------------------------------------------------------
    AsyncBool SystemGroup::requestRegisterSystem(System* system, 
        double tickInterval)
    {
        typedef DelegateTask2<System*, double, bool> DT;

        DT* fn = new DT(system, tickInterval, 
            DT::Delegate(this, &SystemGroup::registerSystem));

        AsyncBool result = fn->getResult();
        mThread->addTask(fn);
        return result;
    }
    //-------------------------------------------------------------------------
    AsyncBool SystemGroup::requestInitialiseSystem(System* system)
    {
        typedef DelegateTask1<System*, bool> DT;
        DT* fn = new DT(system, 
            DT::Delegate(this, &SystemGroup::initialiseSystem));

        AsyncBool result = fn->getResult();
        mThread->addTask(fn);
        return result;
    }
    //-------------------------------------------------------------------------
    AsyncBool SystemGroup::requestScheduleSystem(System* system)
    {
        typedef DelegateTask1<System*, bool> DT;
        DT* fn = new DT(system, 
            DT::Delegate(this, &SystemGroup::scheduleSystem));

        AsyncBool result = fn->getResult();
        mThread->addTask(fn);
        return result;
    }
    //-------------------------------------------------------------------------
    AsyncBool SystemGroup::requestUnscheduleSystem(System* system)
    {     
        typedef DelegateTask1<System*, bool> DT;
        DT* fn = new DT(system, 
            DT::Delegate(this, &SystemGroup::unscheduleSystem));

        AsyncBool result = fn->getResult();
        mThread->addTask(fn);
        return result;
    }
    //-------------------------------------------------------------------------
    AsyncBool SystemGroup::requestShutdownSystem(System* system)
    {    
        typedef DelegateTask1<System*, bool> DT;
        DT* fn = new DT(system, 
            DT::Delegate(this, &SystemGroup::shutdownSystem));

        AsyncBool result = fn->getResult();
        mThread->addTask(fn);
        return result;
    }
    //-------------------------------------------------------------------------
    AsyncBool SystemGroup::requestUnregisterSystem(System* system)
    {
        typedef DelegateTask1<System*, bool> DT;
        DT* fn = new DT(system, 
            DT::Delegate(this, &SystemGroup::unregisterSystem));

        AsyncBool result = fn->getResult();
        mThread->addTask(fn);
        return result;
    }
    //-------------------------------------------------------------------------
    bool SystemGroup::registerSystem(System* system, double tickInterval)
    {
        // Don't accept systems that have already been registered to a group
        if (system->getState() != System::CREATED)
        {
            LOGE("System '" + system->getName() + "' was already registered to a group!");
            return false;
        }

        // Tell the implementation to store the newly registered system
        addRegisteredSystem(system, tickInterval);
        system->notifyRegistered(this);

        return true;
    }
    //-------------------------------------------------------------------------
    bool SystemGroup::initialiseSystem(System* system)
    {
        // Don't allow systems that have already been initialised
        if (system->getState() >= System::INITIALISED)
        {
            LOGE("System '" + system->getName() + "' already initialised: aborting!");
            return false;
        }

        // Abort if this group doesn't or can't own the system
        if (!isSystemRegistered(system) && !registerSystem(system))
        {
            LOGE("System '" + system->getName() + "' can't be owned by this group: aborting!");
            return false;
        }

        bool success = system->initialise();
        LOGEC(!success, "The system '"+system->getName()+"' couldn't yet initialise.");
        return success;
    }
    //-------------------------------------------------------------------------
    bool SystemGroup::scheduleSystem(System* system)
    {
        LOGI("Scheduling " + system->getName() + " starting");
        System::State state = system->getState();

        if (state == System::SCHEDULED)
        {
            LOGI("Scheduling " + system->getName() + "abort, this system is scheduled");
            return false;
        }

        // Intialise the system if it hasn't already been done
        if (state < System::INITIALISED)
        {
            // Abort if initialisation couldn't be done
            if (!initialiseSystem(system))
            {
                LOGE("System '" + system->getName() + "' couldn't initialise: aborting!");
                return false;
            }
        }

        // Tell the implementation to set up the system to be ticked
        addScheduledSystem(system);
        system->notifyScheduled(mCurrentTime);

        LOGI("Scheduling " + system->getName() + " sucessful done");
        return true;
    }
    //-------------------------------------------------------------------------
    bool SystemGroup::unscheduleSystem(System* system)
    {
        System::State state = system->getState();

        // Abort if the system isn't scheduled or this group doesn't own it
        if (state != System::SCHEDULED || !isSystemRegistered(system))
            return false;

        // Tell the implementation that this system won't be ticked anymore
        removeScheduledSystem(system);
        system->notifyUnscheduled();
        
        return true;
    }
    //-------------------------------------------------------------------------
    bool SystemGroup::shutdownSystem(System* system)
    {
        System::State state = system->getState();

        // Abort if the system isn't initialised or this group doesn't own it
        if (state < System::INITIALISED || !isSystemRegistered(system))
            return false;      

        // Unschedule the system if it is currently scheduled
        if (state == System::SCHEDULED)
        {
            // Abort if the system couldn't be unscheduled
            if (!unscheduleSystem(system))
            {
                LOGE("System '" + system->getName() + "' couldn't be unscheduled: aborting!");
                return false;
            }
        }

        system->shutdown();
        return true;
    }
    //-------------------------------------------------------------------------
    bool SystemGroup::unregisterSystem(System* system)
    {
        System::State state = system->getState();

        // Abort if the system hasn't been registered or the group doesn't own it
        if (state == System::CREATED || !isSystemRegistered(system))
            return false;

        // Shutdown the system if it is currently initialised
        if (state > System::REGISTERED)
        {
            // Abort if the system could not be shutdown
            if(!shutdownSystem(system))
            {
                LOGE("System '" + system->getName() + "' couldn't be shutdown: aborting!");
                return false;
            }
        }

        // Tell the implementation that the system is no longer registered
        removeRegisteredSystem(system);
        system->notifyUnregistered();

        return true;
    }
    //-------------------------------------------------------------------------
}
