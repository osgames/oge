/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/system/OgeSystem.h"
#include "oge/system/OgeSystemGroup.h"
#include "oge/system/OgeSystemManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    System::System(const String& name, int systemInitOrdering, int systemRunOrdering) :
        mLastTickTime(0),
		mState(CREATED),
        mSystemGroup(0),
        mSystemListener(0),
		mName(name),
        mSystemInitOrdering(systemInitOrdering),
		mSystemRunOrdering(systemRunOrdering)
    {

    }
    //-------------------------------------------------------------------------
    System::~System()
    {

    }
    //-------------------------------------------------------------------------
    AsyncBool System::requestRegister(double tickInterval)
    {
        return SystemManager::getSingleton().registerSystemToGroup(this, 
            tickInterval);
    }
    //-------------------------------------------------------------------------
    AsyncBool System::requestInitialise()
    {
        return SystemManager::getSingleton().controlSystem(this, 
            System::INITIALISE);
    }
    //-------------------------------------------------------------------------
    AsyncBool System::requestSchedule()
    {
        return SystemManager::getSingleton().controlSystem(this, 
            System::SCHEDULE);
    }
    //-------------------------------------------------------------------------
    AsyncBool System::requestUnschedule()
    {
        return SystemManager::getSingleton().controlSystem(this, 
            System::UNSCHEDULE);
    }
    //-------------------------------------------------------------------------
    AsyncBool System::requestShutdown()
    {
        return SystemManager::getSingleton().controlSystem(this, 
            System::SHUTDOWN);
    }
    //-------------------------------------------------------------------------
    AsyncBool System::requestUnregister()
    {
        return SystemManager::getSingleton().controlSystem(this, 
            System::UNREGISTER);
    }
    //-------------------------------------------------------------------------
    AsyncBool System::requestSetTickInterval(double tickInterval)
    {
        SpinRWMutex::ScopedLock lock(mStateAndGroupMutex, false);

        // The system must be registered before setting a tick interval
        if (!mSystemGroup)
            return AsyncBool(false, true);

        return mSystemGroup->requestSetTickInterval(this, tickInterval);
    }
    //-------------------------------------------------------------------------
    void System::notifyRegistered(SystemGroup* group)
    {
        setSystemGroup(group);

        {
            SpinRWMutex::ScopedLock lock(mStateAndGroupMutex, true);
            mState = REGISTERED;
        }

        if (mSystemListener)
            mSystemListener->notifyRegistered();
    }
    //-------------------------------------------------------------------------
    void System::notifyUnregistered()
    {   
        {
            SpinRWMutex::ScopedLock lock(mStateAndGroupMutex, true);
            mState = CREATED;
        }

        setSystemGroup(0);

        if (mSystemListener)
            mSystemListener->notifyUnregistered();
    }
    //-------------------------------------------------------------------------
    bool System::initialise()
    {
        {
            SpinRWMutex::ScopedLock lock(mStateAndGroupMutex, true);
            mState = INITIALISED;
        }

        if (mSystemListener)
            mSystemListener->notifyInitialised();

        return true;
    }
    //-------------------------------------------------------------------------
    void System::shutdown()
    {
        {
            SpinRWMutex::ScopedLock lock(mStateAndGroupMutex, true);
            mState = REGISTERED;
        }

        if (mSystemListener)
            mSystemListener->notifyShutdown();
    }
    //-------------------------------------------------------------------------
    void System::notifyScheduled(double currentTime)
    {
        {
            SpinRWMutex::ScopedLock lock(mStateAndGroupMutex, true);
            mState = SCHEDULED;
        }

        if (mSystemListener)
            mSystemListener->notifyScheduled(currentTime);
    }
    //-------------------------------------------------------------------------
    void System::notifyUnscheduled()
    {
        {
            SpinRWMutex::ScopedLock lock(mStateAndGroupMutex, true);
            mState = INITIALISED;
        }

        if (mSystemListener)
            mSystemListener->notifyUnscheduled();
    }
    //-------------------------------------------------------------------------
    void System::setListener(Listener* listener)
    {
        SpinRWMutex::ScopedLock lock(mStateAndGroupMutex, true);
        mSystemListener = listener;
    }
    //-------------------------------------------------------------------------
    void System::setSystemGroup(SystemGroup* group)
    {
        SpinRWMutex::ScopedLock lock(mStateAndGroupMutex, true);
        mSystemGroup = group;
    }
    //-------------------------------------------------------------------------
    SystemGroup* System::getSystemGroup() const
    {
        SpinRWMutex::ScopedLock lock(mStateAndGroupMutex, false);
        return mSystemGroup;
    }
    //-------------------------------------------------------------------------
	double System::getTickInterval()
	{
		SystemGroup* group = getSystemGroup();
		if(group) {
			return group->getTickInterval(this);
		}

		return 0.0;
	}
    //-------------------------------------------------------------------------
    void System::setState(State state)
    {
        SpinRWMutex::ScopedLock lock(mStateAndGroupMutex, true);
        mState = state;
    }
    //-------------------------------------------------------------------------
    System::State System::getState() const
    {
        SpinRWMutex::ScopedLock lock(mStateAndGroupMutex, false);
        return mState;
    }
    //-------------------------------------------------------------------------
}
