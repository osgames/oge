/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/system/OgeSystemManager.h"
#include "oge/system/OgeSystemGroup.h"
#include "oge/system/OgeSystemGroupFast.h"
#include "oge/engine/OgeEngineSettings.h"
#include "oge/engine/OgeEngineManager.h"
#include "oge/input/OgeInputManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    template<> SystemManager* Singleton<SystemManager>::mSingleton = 0;
    SystemManager* SystemManager::getSingletonPtr() 
    { 
        return mSingleton; 
    }
    SystemManager& SystemManager::getSingleton() 
    { 
        assert(mSingleton);
        return *mSingleton; 
    }    
    //-------------------------------------------------------------------------
    SystemManager::SystemManager() : mNextSystemGroup(0), 
        mListener(new Listener), mOwnsListener(true)
    {

    }
    //-------------------------------------------------------------------------
    SystemManager::~SystemManager()
    {
        if (mOwnsListener && mListener)
        {
            delete mListener;
        }
    }
    //-------------------------------------------------------------------------
    bool SystemManager::registerSystem(System* system)
    {
        SpinRWMutex::ScopedLock lock(mSystemsMutex, true);
        return mSystems.addEntry(system->getName(), system);
    }
    //-------------------------------------------------------------------------
    void SystemManager::unregisterSystem(System* system)
    {
        SpinRWMutex::ScopedLock lock(mSystemsMutex, true);
		if(system) {
			mSystems.removeEntry(system->getName());
		}
    }
    //-------------------------------------------------------------------------
    void SystemManager::unregisterSystem(const String& name)
    {
        SpinRWMutex::ScopedLock lock(mSystemsMutex, true);
        mSystems.removeEntry(name);
    }
    //-------------------------------------------------------------------------
    System* SystemManager::getSystem(const String& name)
    {
        SpinRWMutex::ScopedLock lock(mSystemsMutex, false);
        return mSystems.getEntry(name);
    }
    //-------------------------------------------------------------------------
    void SystemManager::setupSystemGroups(const ThreadVector& threads)
    {
        SystemGroupFactory* factory = mSystemGroupFactories.getEntry("Fast");

        typedef std::vector<AsyncBool> ResultVector;
        ResultVector results;
        // By default, create 1 SystemGroup per TaskThread
        size_t count = threads.size();
        for (unsigned int index = 0; index < count; ++index)
        {
            TaskThread* thread = threads[index];
            String name = thread->getName();
            SystemGroup* group = factory->createSystemGroup(name);
			AsyncBool result = group->attachToThread(thread);
            results.push_back(result);
            mSystemGroups.addEntry(name, group);
        }

        // wait for all groups to be attached
        count = results.size();
        for (unsigned int index = 0; index < count; ++index)
        {
            while(!results[index].isValid())
            {
                OSThread::yield();
            }
        }

		// @TODO why do we crash here sometimes when releasing the AsyncBool
		// results vector???
		// - added hack to OgeTaskThread to not delete the first task to get
		// around this issue
    }
    //-------------------------------------------------------------------------
    void SystemManager::shutdownSystemGroups()
    {
        SystemGroupMap::Iterator iter = mSystemGroups.begin();
        for (; iter != mSystemGroups.end(); ++iter)
        {
            iter->second->dettachFromThread();
        }
    }
    //-------------------------------------------------------------------------
    void SystemManager::destroySystemGroups()
    {
        SystemGroupMap::Iterator iter = mSystemGroups.begin();
        for (; iter != mSystemGroups.end(); ++iter)
        {
            SystemGroup* group = iter->second;
            SystemGroupFactory* factory = group->getCreator();
            if (factory)
            {
                factory->destroySystemGroup(group);
            }
            else
            {
                delete group;
            }
        }

        mSystemGroups.removeAll();
    }
    //-------------------------------------------------------------------------
    std::vector<SystemGroup*> SystemManager::getGroupsAsVector()
    {
        std::vector<SystemGroup*> groups;

        SystemGroupMap::Iterator iter = mSystemGroups.begin();
        for (; iter != mSystemGroups.end(); ++iter)
        {
            groups.push_back(iter->second);
        }

        return groups;
    }
    //-------------------------------------------------------------------------
    AsyncBool SystemManager::registerSystemToGroup(System* system, 
        double tickInterval)
    {
        if (!system)
            return AsyncBool(false, true);

        SystemGroup* group = system->getSystemGroup();

        // return true because its already registered, 
        //OR return false because it can't register?
        if (group)
            return true;

        // TODO: Remove getNextSystemGroup
        group = getNextSystemGroup();
        if (!group)
            return AsyncBool(false, true);

        return group->requestRegisterSystem(system, tickInterval);
    }
    //-------------------------------------------------------------------------
    AsyncBool SystemManager::registerSystemToGroup(const String& name, 
        double tickInterval)
    {
        System* system = mSystems.getEntry(name);
        if (!system)
            return AsyncBool(false, true);

        return registerSystemToGroup(system, tickInterval);
    }
    //-------------------------------------------------------------------------
    AsyncBool SystemManager::controlSystem(System* system, System::Operation op)
    {
        if (!system)
            return AsyncBool(false, true);

        // Check whether the system is already registered to a group
        SystemGroup* group = system->getSystemGroup();

        // If the group isn't registered and the operation is to unregistered
        // it, abort early as nothing further needs to be done
        if (!group)
        {
            if (op == System::UNREGISTER)
            {
                return AsyncBool(true, true);
            }

            // get a group to register this system to
            group = getNextSystemGroup();
        }

        // shouldn't happen but check anyway
        if (!group)
        {
            LOGE("This shouldn't happen: no group for '"+system->getName()+"'!");
            return AsyncBool(false, true);
        }

        AsyncBool result;

        // Perform the desired operation
        switch (op)
        {
        case System::REGISTER:
            result = group->requestRegisterSystem(system);
            break;
        case System::INITIALISE:
            result = group->requestInitialiseSystem(system);
            break;
        case System::SCHEDULE:
            result = group->requestScheduleSystem(system);
            break;
        case System::UNSCHEDULE:
            result = group->requestUnscheduleSystem(system);
            break;
        case System::SHUTDOWN:
            result = group->requestShutdownSystem(system);
            break;
        case System::UNREGISTER:
            result = group->requestUnregisterSystem(system);
            break;
        default:
            return false;
        }

        LOGDC(!result.get(), "The operation "+System::operation2string(op)+\
            " returned false for the system '"+system->getName()+"'");
        // false in this point is normal - we start delegate task
        return result;
    }
    //-------------------------------------------------------------------------
    AsyncBool SystemManager::controlSystem(const String& name, 
        System::Operation op)
    {
        System* system = getSystem(name);
        if (!system)
            return AsyncBool(false, true);;

        return controlSystem(system, op);
    }
    //-------------------------------------------------------------------------
    bool SystemManager::registerSystemGroupFactory(const String& name, 
        SystemGroupFactory* factory)
    {
        return mSystemGroupFactories.addEntry(name, factory);
    }
    //-------------------------------------------------------------------------
    void SystemManager::unregisterSystemGroupFactory(const String& name)
    {
        mSystemGroupFactories.removeEntry(name);
    }
    //-------------------------------------------------------------------------
    void SystemManager::createBuiltinFactories()
    {
        SystemGroupFactory* groupFactory = new SystemGroupFastFactory();

        if (!registerSystemGroupFactory("Fast", groupFactory))
        {
            delete groupFactory;
        }
    }
    //-------------------------------------------------------------------------
    void SystemManager::destroyBuiltinFactories()
    {
        mSystemGroupFactories.deleteAll();
    }
    //-------------------------------------------------------------------------
    void SystemManager::setListener(Listener* listener)
    {
        // There must always be a listener assigned so no null pointer
        // checks have to be made

        if (listener)
        {
            if (mOwnsListener)
                delete mListener;

            mListener = listener;
            mOwnsListener = false;
        }
        else
        {
            if (!mOwnsListener)
            {
                mListener = new Listener();
                mOwnsListener = true;
            }
        }
    }
    //-------------------------------------------------------------------------
    bool SystemManager::setupSystems(const SystemSettingsVector& settings)
    {
        OrderedEntriesVector entries;

        // 1) Create a set of entries based off of the provided settings

        // Entries are grouped by their system init ordering
        if (!buildEntries(settings, entries))
        {
            LOGE("Build entries failed: aborting");
            return false;
        }

        // 2) Register all of the systems to groups
        if (!registerEntriesToGroups(entries))
        {
            LOGE("Registering all systems to groups failed: aborting");
            destroyEntries(entries);
            return false;
        }

        // 3) Request the target operations for each level of entries

        // Iterate through all ordering levels
        size_t levelCount = entries.size();
        for(size_t level = 0; level < levelCount; ++level)
        {
            // perform the target operations on each of the systems
            size_t indexCount = entries[level].size();
            for(size_t index = 0; index < indexCount; ++index)
            {
                SystemEntry& entry = *(entries[level][index]);
                entry.previousOp = entry.targetOp;
                entry.result = controlSystem(entry.system, entry.previousOp);
            }

            // Wait for all operations to be completed as they are asynchronous
            for(size_t index = 0; index < indexCount; ++index)
            {
                SystemEntry& entry = *(entries[level][index]);
                // wait until the operation has been completed
                // TODO: handle infinite waits, How long to wait?
                while(!entry.result.isValid())
                    OSThread::sleep(5);

                // notify the listener of the completed request
                if (!mListener->notifyRequestCompleted(entry.system,
                    entry.previousOp, entry.result.get()))
                {
                    // abort this sequence
                    LOGE("System '"+entry.system->getName()
                        +"' couldn't complete the operation "
                        +StringUtil::toString(entry.targetOp) + " : aborting");
                    destroyEntries(entries);
                    return false;
                }
            }
        }

        // 4) Clean up the entries that were allocated
        destroyEntries(entries);
        return true;
    }
    //-------------------------------------------------------------------------
    void SystemManager::shutdownSystems()
    {
        OrderedEntriesVector entries;

        // 1) Create groups of systems based on their system init ordering.

        // This includes every systems known by this manager
        buildEntriesForShutdown(entries);

        if (entries.size() == 0)
            return;
        
        // 2) Unregister each set of systems, level by level

        // Iterate through ordering levels in reverse order
        for(size_t level = entries.size() - 1; ; --level)
        {
            // For each system in this ordering level, unregister it
            size_t indexCount = entries[level].size();
            for(size_t index = 0; index < indexCount; ++index)
            {
                SystemEntry& entry = *(entries[level][index]);
                entry.result = controlSystem(entry.system, System::UNREGISTER);
            }

            // Wait for all systems in this ordering level to be unregistered
            for(size_t index = 0; index < indexCount; ++index)
            {
                SystemEntry& entry = *(entries[level][index]);
                // TODO: Handle infinite waiting/deadlock, wait for how long?
                while(!entry.result.isValid())
                {
                    OSThread::sleep(5);
                }
            }

            if (level == 0)
                break;
        }

        // 3) Clean up the entries that were allocated
        destroyEntries(entries);
    }
    //-------------------------------------------------------------------------
    bool SystemManager::buildEntries(const SystemSettingsVector& settings,
        OrderedEntriesVector& sortedEntries)
    {
        SystemEntryVector unsortedEntries;

        // 1) Create an unsorted list of system entries:
        
        // For each system specified in the system settings
        size_t settingsCount = settings.size();
        for (size_t index = 0; index < settingsCount; ++index)
        {
            const SystemSettings& entrySettings = settings[index];
            System* system = getSystem(entrySettings.getName());
            if (!system)
            {
                // Notify the listener that the system wasn't found
                // If it returns false or the system pointer still isn't valid 
                // clean up and abort
                if (!mListener->notifySystemNotFound(entrySettings.getName(),
                    system) || !system)
                {
                    LOGW("System '"+entrySettings.getName()+"' asking to abort.");
                    destroyEntries(unsortedEntries);
                    return false;
                }
            }

            // Create an entry and provide the settings given
            SystemEntry* entry = new SystemEntry;
            entry->system = system;
            entry->targetOp = entrySettings.getOperation();
            entry->previousOp = System::NONE;
            entry->tickInterval = entrySettings.getTickInterval();
            entry->result = AsyncBool(false, false);

            unsortedEntries.push_back(entry);
        }

        // 2) Sort all of the entries by their system's "system ordering":

        std::sort(unsortedEntries.begin(), unsortedEntries.end(), SystemEntry());
        
        // 3) Split all of the "unsorted" entries into their own levels
        size_t sortedIndex = 0;
        sortedEntries.push_back(SystemEntryVector());

        if (unsortedEntries.empty())
            return true; // Not an error simply no settings hence using default settings.

        size_t previousOrdering = unsortedEntries[0]->system->getSystemInitOrdering();

        for (size_t index = 0; index < settingsCount; ++index)
        {
            size_t ordering = unsortedEntries[index]->system->getSystemInitOrdering();
            // When the ordering has changed from the previous iteration
            // create a new level
            if (ordering > previousOrdering)
            {
                // Move up an index
                sortedEntries.push_back(SystemEntryVector());
                previousOrdering = ordering;
                ++sortedIndex;
            }

            // push the entry onto the current level
            sortedEntries[sortedIndex].push_back(unsortedEntries[index]);
        }

        return true;
    }
    //-------------------------------------------------------------------------
    void SystemManager::buildEntriesForShutdown(
        OrderedEntriesVector& sortedEntries)
    {
        SystemEntryVector unsortedEntries;

        // 1) Create entries for each system
        SystemMap::Iterator iter = mSystems.begin();
        for (; iter != mSystems.end(); ++iter)
        {
            System* system = iter->second;
            SystemEntry* entry = new SystemEntry;
            
            entry->system = system;
            entry->targetOp = System::UNREGISTER;
            entry->previousOp = System::NONE;
            entry->tickInterval = 0;
            entry->result = AsyncBool(false, false);

            unsortedEntries.push_back(entry);
        }

        // 2) Sort all of the entries by their system ordering
        std::sort(unsortedEntries.begin(), unsortedEntries.end(), SystemEntry());
      
        // 3) Split the different orderings off into their own levels
        size_t sortedIndex = 0;
        sortedEntries.push_back(SystemEntryVector());
        size_t previousOrdering = unsortedEntries[0]->system->getSystemInitOrdering();

        size_t count = unsortedEntries.size();
        for (size_t index = 0; index < count; ++index)
        {
            size_t ordering = unsortedEntries[index]->system->getSystemInitOrdering();
            if (ordering > previousOrdering)
            {
                // Move up an index
                sortedEntries.push_back(SystemEntryVector());
                previousOrdering = ordering;
                ++sortedIndex;
            }

            sortedEntries[sortedIndex].push_back(unsortedEntries[index]);
        }
    }
    //-------------------------------------------------------------------------
    void SystemManager::destroyEntries(OrderedEntriesVector& entries)
    {
        // Iterate through every entry in every level and delete it
        size_t levelCount = entries.size();
        for (size_t level = 0; level < levelCount; ++level)
        {
            size_t entryCount = entries[level].size();
            for (size_t index = 0; index < entryCount; ++index)
            {
                delete entries[level][index];
            }
        }

        entries.clear();
    }
    //-------------------------------------------------------------------------
    void SystemManager::destroyEntries(SystemEntryVector& entries)
    {
        // Iterate through every entry and delete it
        size_t count = entries.size();
        for (size_t index = 0; index < count; ++index)
        {
            delete entries[index];
        }

        entries.clear();
    }
    //-------------------------------------------------------------------------
    bool SystemManager::registerEntriesToGroups(OrderedEntriesVector& entries)
    {
        // 1) Iterate through all entries and register the systems to groups
        size_t levelCount = entries.size();
        for(size_t level = 0; level < levelCount; ++level)
        {
            size_t indexCount = entries[level].size();
            for(size_t index = 0; index < indexCount; ++index)
            {
                SystemEntry& entry = *(entries[level][index]);
                entry.previousOp = System::REGISTER;
                entry.result = registerSystemToGroup(entry.system, 
                    entry.tickInterval);
            }
        }

        // 2) Wait for all systems to be registered before continuing
        for(size_t level = 0; level < levelCount; ++level)
        {
            size_t indexCount = entries[level].size();
            for(size_t index = 0; index < indexCount; ++index)
            {
                SystemEntry& entry = *(entries[level][index]);
                // TODO: handle infinite waits, How long to wait?
                while(!entry.result.isValid())
                    OSThread::sleep(1);

                // Only notify the listener if this failed or registration
                // was the target operation
                if (!entry.result.get() || entry.previousOp == entry.targetOp)
                {
                    if (!mListener->notifyRequestCompleted(entry.system,
                        entry.previousOp, entry.result.get()))
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }
    //-------------------------------------------------------------------------
    SystemGroup* SystemManager::getNextSystemGroup()
    { 
        SystemGroupMap::Iterator iter = mSystemGroups.begin();
        for (size_t count = 0; iter != mSystemGroups.end(); ++iter, ++count)
        {
            if (count == mNextSystemGroup)
            {
                ++mNextSystemGroup;
                return iter->second;
            }
        }

        iter = mSystemGroups.begin();
        if (iter == mSystemGroups.end())
            return 0;

        mNextSystemGroup = 1;
        return iter->second;
    }
    //-------------------------------------------------------------------------
    SystemManager* SystemManager::createSingleton()
    {
        if (!mSingleton)
            mSingleton = new SystemManager();
        
        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void SystemManager::destroySingleton()
    {
        if (mSingleton)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //-------------------------------------------------------------------------
}
