/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/input/OgeInputEventHandler.h"
#include "oge/gui/OgeGuiManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    DummyInputEventHandler::DummyInputEventHandler()
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::keyPressed (const KeyEvent& arg)
    {
        // TODO Should we put also here the InputEventHandler::keyPressed( arg );
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::keyReleased (const KeyEvent& arg)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::mousePressed (const MouseEvent& arg)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::mouseReleased (const MouseEvent& arg)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::mouseMoved (const MouseEvent& arg)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::joyButtonPressed (const JoystickEvent& arg)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::joyButtonReleased (const JoystickEvent& arg)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::joyAxisMoved (const JoystickEvent& arg)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::joyPovMoved (const JoystickEvent& arg)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::keyPressedAny(Any arg)
    {
        /* This dummy shouldn't do anything!
        if (arg.keyEvent.key == OIS::KC_ESCAPE)
        {
            EngineManager::getSingletonPtr()->stop();
        }
        else if (arg.keyEvent.key == OIS::KC_K)
        {
            KeyboardDeviceAny* dev = InputManager::getSingletonPtr()->getFirstKeyboard();
            assert(dev);
            if (dev)
                dev->setBuffered(false);
        }

        else if (arg.keyEvent.key == OIS::KC_SYSRQ)
        {
            GraphicsManager::getSingletonPtr()->saveScreenshot("OGE_Screenshot.jpg");
        }
        else if (arg.keyEvent.key == OIS::KC_F1)
        {
            GraphicsManager::getSingletonPtr()->toggleDebugOverlay();
        }
        */
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::keyReleasedAny(Any arg)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::mousePressedAny(Any arg, const int mousebutton)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::mouseReleasedAny(Any arg, const int mousebutton)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::mouseMovedAny(Any arg)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::joyButtonPressedAny(Any arg, int button)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::joyButtonReleasedAny(Any arg, int button)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::joyAxisMovedAny(Any arg, int axis)
    {
    }
    //-------------------------------------------------------------------------
    void DummyInputEventHandler::joyPovMovedAny(Any arg, int pov)
    {
    }
    //-------------------------------------------------------------------------
} // namespace
