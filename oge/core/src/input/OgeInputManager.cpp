/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/input/OgeInputManager.h"
#include "oge/input/OgeInputDevice.h"

namespace oge
{
    //-------------------------------------------------------------------------
    InputManager* InputManager::mManager = 0;
    //-------------------------------------------------------------------------
    InputManager::InputManager(const String& name, int systemInitOrdering, int systemRunOrdering) : 
        System(name, systemInitOrdering, systemRunOrdering), mWindowHandle(0), mIsFullscreen(false)
    {
        LOG(String("Created ") + name);
        mManager = this;
    }
    //-------------------------------------------------------------------------    
    InputManager::~InputManager()
    {
        LOG("Destroyed ");
    }
    //-------------------------------------------------------------------------    
    InputDevice* InputManager::addInputDevice(InputDevice *device)
    {
        String name = device->getName();
        mInputDevices.addEntry(name, device);
        return device;
    }
    //-------------------------------------------------------------------------
    InputDevice* InputManager::getDevice(const String& name)
    {
        InputDevice *inp = 0;
        DeviceMap::Iterator iter;
        iter = mInputDevices.find(name);
        if (iter != mInputDevices.end())
            inp = iter->second;
        return inp;
    }
    //-------------------------------------------------------------------------
    void InputManager::removeDevice(const String& name)
    {
        DeviceMap::Iterator iter = mInputDevices.find(name);
        if (iter != mInputDevices.end())
        {
            mInputDevices.removeEntry(name);
        }
    }
    //-------------------------------------------------------------------------
    void InputManager::deleteDevice(const String& name)
    {
        DeviceMap::Iterator iter = mInputDevices.find(name);
        if (iter != mInputDevices.end())
        {
            mInputDevices.deleteEntry(name);
        }
    }
    //-------------------------------------------------------------------------
    InputDevice* InputManager::getFirstKeyboard()
    {
        for (DeviceMap::Iterator i = mInputDevices.begin();
            i != mInputDevices.end(); ++i)
        {
            if (i->second->getDeviceType() == OGE_INPUT_KEYBOARD)
            {
                return (i->second);
            }
        }
        return 0;
    }
    //-------------------------------------------------------------------------
    InputDevice* InputManager::getFirstMouse()
    {
        for (DeviceMap::Iterator i = mInputDevices.begin();
            i != mInputDevices.end(); ++i)
        {
            if (i->second->getDeviceType() == OGE_INPUT_MOUSE)
            {
               return (i->second);
            }
        }
        return 0;
    }
    //-------------------------------------------------------------------------
    InputDevice* InputManager::getFirstJoystick()
    {
        for (DeviceMap::Iterator i = mInputDevices.begin();
            i != mInputDevices.end(); ++i)
        {
            if (i->second->getDeviceType() == OGE_INPUT_JOYSTICK)
            {
                return (i->second);
            }
        }
        return 0;
    }
    //-------------------------------------------------------------------------
    InputDevice* InputManager::getDeviceOfType(DeviceType type, unsigned int nb)
    {
        unsigned int count = nb;
        for (DeviceMap::Iterator i = mInputDevices.begin();
            i != mInputDevices.end(); ++i)
        {
            if (i->second->getDeviceType() == type)
            {
                if (count <= 1)
                    return (i->second);
                count--;
            }
        }
        return 0;
    }
    //-------------------------------------------------------------------------
}
