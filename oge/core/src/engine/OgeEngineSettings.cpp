/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/engine/OgeEngineSettings.h"

namespace oge
{
    //-------------------------------------------------------------------------
    ThreadSettings::ThreadSettings()
    {
        setToDefault();
    }
    //-------------------------------------------------------------------------
    void ThreadSettings::parseFromOptions(OptionPtr rootOption)
    {
        mName = rootOption->getName();
        const OptionPtr priorityOpt = rootOption->getOption("priority");
        if (priorityOpt)
        {
            String priority = priorityOpt->getValue();
            if (priority == "" || priority == "normal")
            {
                mPriority = OSThread::PRIO_NORMAL;
            }
            else
            {
                if (priority == "high")
                    mPriority = OSThread::PRIO_HIGH;
                else if (priority == "highest")
                    mPriority = OSThread::PRIO_HIGHEST;
                else if (priority == "low")
                    mPriority = OSThread::PRIO_LOW;
                else if (priority == "lowest")
                    mPriority = OSThread::PRIO_LOWEST;
                else
                    mPriority = OSThread::PRIO_NORMAL;
            }
        }
    }
    //-------------------------------------------------------------------------
    void ThreadSettings::setToDefault()
    {
        mName = "";
        mPriority = OSThread::PRIO_NORMAL;
    }
    //-------------------------------------------------------------------------
    ThreadingSettings::ThreadingSettings()
    {
        setToDefault();
    }
    //-------------------------------------------------------------------------
    void ThreadingSettings::parseFromOptions(OptionPtr rootOption)
    {
        String type = rootOption->getOption("type", true, false)->getValue();

        // Default to automatic unless manual explicitly stated
        if(type == "manual")
            mAutoDetect = false;
        else
            mAutoDetect = true;

        OptionPtr prefixOption = rootOption->getOption("prefix");
        if (prefixOption)
        {
            mNamePrefix = prefixOption->getValue();
        }

        OptionPtr suffixOption = rootOption->getOption("suffix");
        if (suffixOption)
        {
            mNameSuffix = suffixOption->getValue();
        }

        OptionPtr countOption = rootOption->getOption("count");
        if (countOption)
        {
            String countStr = countOption->getValue();
            if (countStr != "")
            {
                mThreadCount = StringUtil::toUnsignedInt(countStr);
            }
        }

        OptionPtr threadOption = rootOption->getOption("thread");
        if (threadOption)
        {
            Option::Iterator iter = threadOption->getOptionIterator();
            for(; iter.hasNext(); iter.next())
            {
                ThreadSettings settings;
                settings.parseFromOptions(iter.getOption());
                mThreads.push_back(settings);
            }
        }
    }
    //-------------------------------------------------------------------------
    void ThreadingSettings::setToDefault()
    {
        mAutoDetect = true;
        mThreadCount = 0;
        mNamePrefix = "OGE_Thread_";
        mNameSuffix = "";
        mThreads.clear();
    }
    //-------------------------------------------------------------------------
    EngineSettings::EngineSettings()
    {
        setToDefault();
    }
    //-------------------------------------------------------------------------
    EngineSettings::EngineSettings(const EngineSettings& settings)
    {
        copyFrom(settings);
    }
    //-------------------------------------------------------------------------
    EngineSettings::~EngineSettings() 
    {

    } 
    //-------------------------------------------------------------------------
    void EngineSettings::parseFromOptions(OptionPtr rootOption)
    {
        if (!rootOption)
            return;

        OptionPtr selectedProfileOption = 
            rootOption->getOption("profile.selected", true);
        String selectedProfileName = selectedProfileOption->getValue();

        if (selectedProfileName != "")
            mProfileName = selectedProfileName;

        // log warning? profile name must not be empty
        if (mProfileName == "")
            mProfileName = "Client";

        selectedProfileOption = rootOption->getOption("profile." + mProfileName);
        if (selectedProfileOption)
        {
            OptionPtr option = selectedProfileOption->getOption("timingHandler");
            if (option)
            {
                const String& timingHandler = option->getValue();

                if (timingHandler != "")
                {
                    mTimingHandlerName = timingHandler;
                }
            }

            OptionPtr systemsOption = selectedProfileOption->getOption("system");
            if (systemsOption)
            {
                Option::Iterator iter = systemsOption->getOptionIterator();

                for (iter.begin(); iter.hasNext(); iter.next())
                {
                    SystemSettings settings;
                    settings.parseFromOptions(iter.getOption());
                    mSystemsSettings.push_back(settings);
                }
            }
        }
        else
        {
            LOGW("Didn't find a profile name '"+mProfileName+"' : using in-build settings.");
        }

        OptionPtr threadingOption = rootOption->getOption("threading");
        if (threadingOption)
        {
            mThreadingSettings.parseFromOptions(threadingOption);
        }
        else
        {
            LOGW("Didn't find threading option: using in-build settings.");
        }

        OptionPtr pluginsOption = rootOption->getOption("plugins", true, false);
        const String& pluginsFolder = 
            pluginsOption->getOption("folder", true, false)->getValue();

        if (pluginsFolder != "")
            mPluginsFolder = pluginsFolder;
        LOG("Plugins folder set to '"+pluginsFolder+"'");
    }
    //-------------------------------------------------------------------------
    void EngineSettings::setToDefault()
    {
        mProfileName = "Client";
        mTimingHandlerName = "Simple";
        mThreadingSettings.setToDefault();
        mPluginsFolder = ".";
        mSystemsSettings.clear();
    }
    //-------------------------------------------------------------------------
    void EngineSettings::addSystemsSettings(const SystemSettings& settings)
    {
        mSystemsSettings.push_back(settings);
    }
    //-------------------------------------------------------------------------
    void EngineSettings::clearSystemsSettings()
    {
        mSystemsSettings.clear();
    }
    //-------------------------------------------------------------------------
    EngineSettings& EngineSettings::operator = (const EngineSettings& settings)
    {
        copyFrom(settings);
        return *this;
    }
    //-------------------------------------------------------------------------
    void EngineSettings::copyFrom(const EngineSettings& settings)
    {
        mProfileName = settings.getProfileName();
        mTimingHandlerName = settings.getTimingHandlerName();
        mThreadingSettings = settings.getThreadingSettings();
        mPluginsFolder = settings.getPluginsFolder();
        mSystemsSettings = settings.getSystemsSettings();
    }
    //-------------------------------------------------------------------------
}
