/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/engine/OgeEngineTimingHandler.h"
#include "oge/engine/OgeEngineManager.h"

#include "oge/object/OgeObjectManager.h" // WIP
#include "oge/object/OgeObjectSceneManager.h" // WIP
#include "oge/OgeProfiler.h"

namespace oge
{
    //-------------------------------------------------------------------------
    EngineTimingHandler::EngineTimingHandler(
        EngineTimingHandlerFactory* creator) : mCreator(creator), 
        mEngineSettings(0)
    {

    }
    //-------------------------------------------------------------------------
    SimpleEngineTimingHandler::SimpleEngineTimingHandler(
        EngineTimingHandlerFactory* creator) : EngineTimingHandler(creator), 
        mRunning(false)
    {

    }
    //-------------------------------------------------------------------------
    SimpleEngineTimingHandler::~SimpleEngineTimingHandler()
    {

    }
    //-------------------------------------------------------------------------
    void SimpleEngineTimingHandler::run()
    {
        notifyAllGroups();

        mRunning = true;
        size_t groupCount = mGroups.size();
        double currentTime = mClock.updateAndGetTime();

		Profiler::getSingletonPtr()->setClock(&mClock);

		// supposed to prevent one thread from starving others - 
		// not necessary when there is only one thread
        //double tickDelay = 5; // todo: allow customisation
		double tickDelay = 0;
        double nextTime = currentTime + tickDelay;
		double elapsedTime = 0;

		ObjectManager* objManager = ObjectManager::getSingletonPtr();
		ObjectSceneManager* objSceneManager = NULL;
        while (mRunning)
        {
            currentTime = mClock.updateAndGetTime();

            if (currentTime >= nextTime)
            {
                for (size_t index = 0; index < groupCount; ++index)
                {
                    mGroups[index]->tick(currentTime);
                }

                nextTime += tickDelay;
            }

			double newTime = mClock.updateAndGetTime();
			// @TODO do something useful with the elapsed time for stats
			elapsedTime = newTime - currentTime;

            double dT = nextTime - newTime;
//=============================================== WIP
			objSceneManager = objManager->getObjectSceneManager();
			if (objSceneManager)
                objManager->affectSchedulerTimeLimit(objSceneManager->mLastMaxMessageProcessingTime + dT );
//=============================================== WIP
			// these are in here to prevent one thread from starving another
            if (dT>1)
            {
                OSThread::sleep((long)dT);
            }
            else
            {
                OSThread::yield();
            }
        }
    }
    //-------------------------------------------------------------------------
    void SimpleEngineTimingHandler::stop()
    {
        mRunning = false;
    }
    //-------------------------------------------------------------------------
    bool SimpleEngineTimingHandler::setSystemGroups(
        const std::vector<SystemGroup*>& groups)
    {
        if (mRunning)
            return false;

        mGroups = groups;

        return true;
    }
    //-------------------------------------------------------------------------
    void SimpleEngineTimingHandler::notifyAllGroups()
    {
        size_t count = mGroups.size();
        double currentTime = mClock.updateAndGetTime();

        for (size_t index = 0; index < count; ++index)
        {
            mGroups[index]->notifyCurrentTime(currentTime);
        }
    }
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    OGEdEngineTimingHandler::OGEdEngineTimingHandler(
        EngineTimingHandlerFactory* creator) : EngineTimingHandler(creator), 
        mRunning(false)
    {
    }
    //-------------------------------------------------------------------------
    OGEdEngineTimingHandler::~OGEdEngineTimingHandler()
    {
    }
    //-------------------------------------------------------------------------
    void OGEdEngineTimingHandler::run()
    {
        notifyAllGroups();
        mNextTime = mClock.updateAndGetTime() + 5;
    }
    //-------------------------------------------------------------------------
    void OGEdEngineTimingHandler::stop()
    {
        mRunning = false;
    }
    //-------------------------------------------------------------------------
    bool OGEdEngineTimingHandler::setSystemGroups(
        const std::vector<SystemGroup*>& groups)
    {
        if (mRunning)
            return false;

        mGroups = groups;

        return true;
    }
    //-------------------------------------------------------------------------
    void OGEdEngineTimingHandler::notifyAllGroups()
    {
        size_t count = mGroups.size();
        double currentTime = mClock.updateAndGetTime();

        for (size_t index = 0; index < count; ++index)
        {
            mGroups[index]->notifyCurrentTime(currentTime);
        }
    }
    //-------------------------------------------------------------------------
    void OGEdEngineTimingHandler::update()
    {
        double currentTime = mClock.updateAndGetTime();
        if (currentTime >= mNextTime)
        {
            for (size_t index = 0; index < mGroups.size(); ++index)
            {
                mGroups[index]->tick(currentTime);
            }
            mNextTime += 5;
        }
        double dT = mNextTime - mClock.updateAndGetTime();
        if (ObjectManager::getSingleton().getObjectSceneManager())
            ObjectManager::getSingleton().affectSchedulerTimeLimit(
                ObjectManager::getSingleton().getObjectSceneManager()->mLastMaxMessageProcessingTime + dT );
    }
    //-------------------------------------------------------------------------
}
