/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/engine/OgeEngineManager.h"
#include "oge/engine/OgeEngineListener.h"
#include "oge/filesystem/OgeFileSystem.h"
#include "oge/resource/OgeResourceRegistry.h"
#include "oge/config/OgeOptionManager.h"
#include "oge/plugin/OgePluginManager.h"
#include "oge/plugin/OgePlugin.h"
#include "oge/engine/OgeEngineTimingHandler.h"
#include "oge/system/OgeSystemManager.h"
#include "oge/system/OgeSystemGroup.h"
#include "oge/system/OgeSystem.h"
#include "oge/game/OgeGameManager.h"
#include "oge/object/OgeObjectManager.h"
#include "oge/hardware/OgeCPUDetect.h"
#include "oge/hardware/OgeCPUInfo.h"
#include "oge/script/OgeScriptManager.h"
#include "oge/physics/OgePhysicsManager.h"
#include "oge/OgeProfiler.h"

#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/audio/OgeAudioManager.h"
#include "oge/input/OgeInputManager.h"
#include "oge/OgeCoreVersion.h"

#include "zzip/zzip.h"
#include "Poco/Version.h"

namespace oge
{
    //-------------------------------------------------------------------------
    template<> EngineManager* Singleton<EngineManager>::mSingleton = 0;
    EngineManager* EngineManager::getSingletonPtr() 
    { 
        return mSingleton; 
    }
    EngineManager& EngineManager::getSingleton()
    {
        assert(mSingleton);
        return *mSingleton;
    }    
    //-------------------------------------------------------------------------
    EngineManager::EngineManager() : mInitialised(false), mTimingHandler(0),
        mListener(0), mDefaultGuiManager(0), mDefaultPhysicsManager(0), 
        mDefaultScriptManager(0), mExternalWindowHandle("")
    {
        //ogeInitMemoryCheck();
        mSystemManager = SystemManager::createSingleton();
        OptionManager::createSingleton();
        PluginManager::createSingleton();
        FileSystem::createSingleton();
        ResourceRegistry::createSingleton();
        GameManager::createSingleton();
        ObjectManager::createSingleton();
		Profiler::createSingleton();

        LOG("EngineManager created");
    }
    //-------------------------------------------------------------------------
    EngineManager::~EngineManager()
    {
        LOG("Destroying EngineManager...");

        deleteDefaultManagers();

        ObjectManager::destroySingleton();
        GameManager::destroySingleton();
        ResourceRegistry::destroySingleton();
        FileSystem::destroySingleton();
        PluginManager::destroySingleton();
        OptionManager::destroySingleton();
        SystemManager::destroySingleton();
		Profiler::destroySingleton();

        LOG("EngineManager destroyed");
    }
	//-------------------------------------------------------------------------
	double EngineManager::getCurrentTime(bool update)
	{
		if(mTimingHandler) {
			return mTimingHandler->getCurrentTime(update);
		}

		return 0;
	}
    //-------------------------------------------------------------------------
    bool EngineManager::initialise(const String configFile, 
        const String& profile, EngineListener* listener)
    {
        if (mInitialised)
            return true;

        mListener = listener;
        if (!mListener)
            mListener = new EngineListener();

        if (profile != "")
            mEngineSettings.setProfileName(profile);

        registerCoreDependencies(); // TODO delete this?


        if (mListener && !mListener->notifyPreInitialise())
            return false;

        OptionManager::getSingleton().initialise();
        FileSystem::getSingleton().initialise();

        createBuiltInFactories(); 
        registerBuiltInManagers();
        
        if (configFile != "")
        {
            if (!OptionManager::getSingleton().loadFromFilename(configFile))
            {
                LOGW(String("The options couldn't be loaded correctly from the file '")
                    + configFile + "'" );
                return false;
            }
            mEngineSettings.parseFromOptions(OptionManager::getSingleton().
                getOption("oge.engine", true));
        }
        else
        {
            // TODO Verify that all necessary default values are present when no configFile is present
            LOG("No configuration file (such as oge.xml) was provided. Using in-build settings.");
        }

        loadPlugins();

        // Loads the archives and archive groups from config
        FileSystem::getSingleton().setupFromOptions(
            OptionManager::getSingleton().getOption("oge.filesystem"));

        initialiseManagers();

        if (mListener && !mListener->notifyInitialise())
            return false;

        setupThreads();
        mSystemManager->setupSystemGroups(mThreads);
        if (!mSystemManager->setupSystems(mEngineSettings.getSystemsSettings()))
        {
            return false;
        }

        setupTimingHandler();

        mInitialised = true;

        // Log all libraries version
        LOG( "----------------------------------------" );
        LOGW( getLibrariesVersion() );
        LOG( "----------------------------------------" );

        if (mListener && !mListener->notifyPostInitialise())
            return false;

        return true;
    }
    //-------------------------------------------------------------------------
    void EngineManager::shutdown()
    {
        if (mListener && !mListener->notifyPreShutdown())
            return;

        mInitialised = false;

        destroyTimingHandler();
        mSystemManager->shutdownSystems();
        mSystemManager->shutdownSystemGroups();
        shutdownThreads();
        mSystemManager->destroySystemGroups();
        unregisterBuiltInManagers();
        destroyBuiltInFactories();

        FileSystem::getSingleton().shutdown();
        OptionManager::getSingleton().shutdown();

        if (mListener)
            mListener->notifyPostShutdown();
    }
    //-------------------------------------------------------------------------
    bool EngineManager::run()
    {
        if (!mInitialised)
            return false;

        //startThreads();

        if (mListener && !mListener->notifyPreRun())
            return false;

        // start the engines main loop
        mTimingHandler->run();

        if (mListener && !mListener->notifyPostRun())
            return false;

        return true;
    }
    //-------------------------------------------------------------------------
    void EngineManager::stop()
    {
        if (mInitialised && mTimingHandler)
        {
            mTimingHandler->stop();
        }
    }
    //-------------------------------------------------------------------------
    bool EngineManager::registerTimingHandlerFactory(const String& name,
        EngineTimingHandlerFactory* factory)
    {
        return mTimingHandlerFactories.addEntry(name, factory);
    }
    //-------------------------------------------------------------------------
    void EngineManager::unregisterTimingHandlerFactory(const String& name)
    {
        mTimingHandlerFactories.removeEntry(name);
    }
    //-------------------------------------------------------------------------
    bool EngineManager::registerDependency(
        const DependencyInfoMap& dependencyInfo)
    {
        String name = dependencyInfo.getEntry("Name");
        if (name == "")
            return false;

        StringUtil::toLowerCase(name);

        Mutex::ScopedLock lock(mDependenciesMutex);
        
        DependencyMap::Iterator iter = mDependencies.find(name);
        if (iter == mDependencies.end())
        {
            String fullName = dependencyInfo.getEntry("FullName");
            String versionMajor = dependencyInfo.getEntry("VersionMajor");
            String versionMinor = dependencyInfo.getEntry("VersionMinor");
            String build = dependencyInfo.getEntry("Build");
            String unicode = dependencyInfo.getEntry("Unicode");
            String compiler = dependencyInfo.getEntry("Compiler");
            String other = dependencyInfo.getEntry("Other");

            mDependencies[name] = dependencyInfo;
        }

        return true;
    }
    //-------------------------------------------------------------------------
    void EngineManager::registerCoreDependencies()
    {
        DependencyInfoMap pocoInfo;
        pocoInfo["Name"] = "Poco";
        pocoInfo["FullName"] = "C++ Portable Components";
        // TODO add version pocoInfo["VersionMajor"] = ...; 
        // http://sourceforge.net/tracker2/?func=detail&aid=2489240&group_id=132964&atid=725712
        registerDependency(pocoInfo);

        DependencyInfoMap tbbInfo;
        tbbInfo["Name"] = "TBB";
        tbbInfo["FullName"] = "Intel� Threading Building Blocks";
        // TODO add version tbbInfo["VersionMajor"] = ...;
        registerDependency(tbbInfo);

        DependencyInfoMap zzipInfo;
        zzipInfo["Name"] = "ZZip";
        zzipInfo["FullName"] = "ZZip";
        zzipInfo["VersionMajor"] = ZZIP_VERSION;
        registerDependency(zzipInfo);
    }
    //-----------------------------------------------------------------------------
    const String EngineManager::getOgeVersion()
    {
        String version("Core: OGE ");
        version += OGE_CORE_VERSION_NAME;
        version += " ";
        version += OGE_CORE_VERSION_NUMBER;
        version += " - ";
        version += StringUtil::toString(OGE_CORE_VERSION_FULL);
        version += " build: ";
        version += OGE_CORE_BUILD;
        version += " \n";
        return version;
    }
    //-----------------------------------------------------------------------------
    const String EngineManager::getLibrariesVersion()
    {
        String version("Libraries version: \n");

        version += getOgeVersion();

        if (GraphicsManager::getManager())
        {
            version += GraphicsManager::getManager()->getLibrariesVersion();
        }
        if (AudioManager::getManager())
        {
            version += AudioManager::getManager()->getLibrariesVersion();
        }
        if (GuiManager::getManager())
        {
            version += GuiManager::getManager()->getLibrariesVersion();
        }
        if (InputManager::getManager())
        {
            version += InputManager::getManager()->getLibrariesVersion();
        }
        if (PhysicsManager::getManager())
        {
            version += PhysicsManager::getManager()->getLibrariesVersion();
        }
        if (ScriptManager::getManager())
        {
            version += ScriptManager::getManager()->getLibrariesVersion();
        }

        // OTHERS libraries
        /*
        version += "Boost ";
        version += BOOST_LIB_VERSION;
        version += "\n";

        version += "Ticpp ";
        version += StringUtil::toString(TIXML_MAJOR_VERSION);
        version += ".";
        version += StringUtil::toString(TIXML_MINOR_VERSION);
        version += ".";
        version += StringUtil::toString(TIXML_PATCH_VERSION);
        version += "\n";
        */

        // NEXT http://sourceforge.net/tracker2/?func=detail&aid=2489240&group_id=132964&atid=725712
    #ifdef POCO_VERSION
        version += "POCO: " + StringUtil::toString(POCO_VERSION) + "\n";
    #else
        version += "POCO: No method or #define exist - Isn't your POCO too old? \n";
    #endif

        version += "zzip ";
        version += ZZIP_VERSION;
        version += " \n";

        version += "Note: If no libs used perhaps the plugin was not loaded!\n";

        return version;
    }
    //-------------------------------------------------------------------------
    void EngineManager::createBuiltInFactories()
    {
        EngineTimingHandlerFactory* timingFactory = 
            new SimpleEngineTimingHandlerFactory();
        EngineTimingHandlerFactory* ogedTimingFactory = 
            new OGEdEngineTimingHandlerFactory();

        if (!registerTimingHandlerFactory("Simple", timingFactory))
        {
            delete timingFactory;
        }
        if (!registerTimingHandlerFactory("OGEd", ogedTimingFactory))
        {
            delete ogedTimingFactory;
        }

        SystemManager::getSingleton().createBuiltinFactories();
    }
    //-------------------------------------------------------------------------
    void EngineManager::destroyBuiltInFactories()
    {
        SystemManager::getSingleton().destroyBuiltinFactories();
        mTimingHandlerFactories.deleteAll();
    }
    //-------------------------------------------------------------------------
    void EngineManager::registerBuiltInManagers()
    {
        mSystemManager->registerSystem(GameManager::getSingletonPtr());
        mSystemManager->registerSystem(ObjectManager::getSingletonPtr());
    }
    //-------------------------------------------------------------------------
    void EngineManager::unregisterBuiltInManagers()
    {
        mSystemManager->unregisterSystem(GameManager::getSingletonPtr());
        mSystemManager->unregisterSystem(ObjectManager::getSingletonPtr());
    }
    //-------------------------------------------------------------------------
    void EngineManager::loadPlugins()
    {
    #ifdef _DEBUG
        OptionPtr pluginsOption = OptionManager::getSingleton().
            getOption("oge.engine.plugins.debug");
    #else
        OptionPtr pluginsOption = OptionManager::getSingleton().
            getOption("oge.engine.plugins.release");
    #endif

        if (!pluginsOption)
            return;

        String pluginsFolder = pluginsOption->
            getOption("folder", true, false)->getValue();

        if (pluginsFolder == "")
            pluginsFolder = "./";

        if (pluginsFolder.at( pluginsFolder.size() -1 ) != '/' )
            pluginsFolder += "/";

        OptionPtr pluginRoot = pluginsOption->getOption("plugin", true, false);
        Option::Iterator iter = pluginRoot->getOptionIterator();

        for (iter.begin(); iter.hasNext(); iter.next())
        {
            // temporary due to bug in XmlOptionReader
            #ifdef WIN32
                String filename = pluginsFolder + iter.getName();
            #else
                String filename = pluginsFolder + "lib" + iter.getName();
            #endif

            if (!PluginManager::getSingleton().loadPlugin(filename))
            {
                LOGE(String("Couldn't load the plugin '")+filename+"'");
            }
        }
    }
    //-------------------------------------------------------------------------
    void EngineManager::initialiseManagers()
    {
    #ifdef _DEBUG
        OptionPtr pluginsOption = OptionManager::getSingleton().
            getOption("oge.engine.plugins.debug");
    #else
        OptionPtr pluginsOption = OptionManager::getSingleton().
            getOption("oge.engine.plugins.release");
    #endif

        if (!pluginsOption)
            return;

        String pluginsFolder = pluginsOption->
            getOption("folder", true, false)->getValue();

        if (pluginsFolder == "")
            pluginsFolder = "./";

        if (pluginsFolder.at( pluginsFolder.size() -1 ) != '/' )
            pluginsFolder += "/";

        OptionPtr pluginRoot = pluginsOption->getOption("plugin", true, false);
        Option::Iterator iter = pluginRoot->getOptionIterator();

        for (iter.begin(); iter.hasNext(); iter.next())
        {
            if (!PluginManager::getSingletonPtr()->initialisePlugin(iter.getName()))
            {
                LOGE(iter.getName() + " couldn't be initialised.");
            }
            {
                LOGI(iter.getName() + " successful initialised.");
            }
        }
        
	#ifdef _DEBUG
        if (PluginManager::getSingletonPtr()->getPlugin("OgeGuiMyGUI_d") == 0)
	#else
		if (PluginManager::getSingletonPtr()->getPlugin("OgeGuiMyGUI") == 0)
	#endif
        {
            LOGW("OgeGuiMyGUI not present or couldn't be initialised: creating default one");
            mDefaultGuiManager = new GuiManager("Default Gui Manager");
            mSystemManager->registerSystem(mDefaultGuiManager);
            mDefaultGuiManager->initialise();
        }

	#ifdef _DEBUG
        if (PluginManager::getSingletonPtr()->getPlugin("OgeScriptLua_d") == 0)
	#else
		if (PluginManager::getSingletonPtr()->getPlugin("OgeScriptLua") == 0)
	#endif
        {
            LOGW("OgeScriptLua not present or couldn't be initialised: creating default one");
            mDefaultScriptManager = new ScriptManager("Default Script Manager");
            mSystemManager->registerSystem(mDefaultScriptManager);
            mDefaultScriptManager->initialise();
        }

	#ifdef _DEBUG
        if (PluginManager::getSingletonPtr()->getPlugin("OgePhysicsBullet_d") == 0)
	#else
		if (PluginManager::getSingletonPtr()->getPlugin("OgePhysicsBullet") == 0)
	#endif
        {
            LOGW("OgePhysicsBullet not present or couldn't be initialised: creating default one");
            mDefaultPhysicsManager = new PhysicsManager("Default Physics Manager");
            mSystemManager->registerSystem(mDefaultPhysicsManager);
            mDefaultPhysicsManager->initialise();
        }
    }
    //-------------------------------------------------------------------------
    void EngineManager::setupThreads()
    {
        size_t threadCount = 0;
        const ThreadingSettings& settings = mEngineSettings.getThreadingSettings();

        // Set the number of threads to create either automatically or from
        // explicit settings
        if (settings.getAutoDetect())
        {
            CPUInfo info = CPUDetect::getCPUInfo();

            String message = "CPU Information: ";
            if (info.packageCount > 1) { message += "Multi-Package; "; }

            if ((info.coreCount / info.packageCount) > 1)
                message += "Multi-Core; ";
            else
                message += "Single-Core; ";

            if (info.HT) { message += "Hyper-Threaded; "; }

            message += "(p: " + StringUtil::toString((unsigned int)info.packageCount) + 
                ", c: " + StringUtil::toString((unsigned int)info.coreCount) +
                ", l: " + StringUtil::toString((unsigned int)info.logicalCount) + ")";

            LOGI(message)

            // TODO: Use these values better
            threadCount = info.logicalCount;

            LOG("Thread Count: " + StringUtil::toString((unsigned int)threadCount))
        }
        else
        {
            threadCount = settings.getThreadCount();
        }

        const ThreadingSettings::ThreadSettingsVector& explicitThreads = 
            settings.getThreadsSettings();

        // Create each thread that has been specifically defined
        for (size_t index = 0;  
                index < explicitThreads.size() && index < threadCount; 
                ++index)
        {
            // Name = Global Prefix + Specific Name + Global Suffix
            String name =   settings.getNamePrefix() + 
                            explicitThreads[index].getName() + 
                            settings.getNameSuffix();

            OSThread* osThread = new OSThread(name);
            TaskThread* thread = new TaskThread(osThread);

            osThread->setPriority(explicitThreads[index].getPriority());
            mThreads.push_back(thread);
        }

        // Create the remaining threads
        size_t remaining = threadCount - mThreads.size();
        for (size_t index = 0; index < remaining; ++index)
        {
            String name = settings.getNamePrefix() + 
                StringUtil::toString((unsigned int) index + 1) + settings.getNameSuffix();

            TaskThread* thread = new TaskThread(name);
            mThreads.push_back(thread);
        }

        // Start all threads
        for (size_t index = 0; index < mThreads.size(); ++index)
        {
            mThreads[index]->start();
        }
    }
    //-------------------------------------------------------------------------
    void EngineManager::shutdownThreads()
    {
        // Tell each thread to stop
        size_t count = mThreads.size();
        for (size_t index = 0; index < count; ++index)
        {
            mThreads[index]->stop();
        }

        // Wait for each thread to fully shutdown then delete each
        for (size_t index = 0; index < count; ++index)
        {
            TaskThread* thread = mThreads[index];
            // TODO: Change for try join so not to stall if problem occurs
            thread->join();
            delete thread;
        }

        mThreads.clear();
    }
    //-------------------------------------------------------------------------
    bool EngineManager::setupTimingHandler()
    {
        // retrieve the timingFactory for the chosen timing handler
        const String& name = mEngineSettings.getTimingHandlerName();
        EngineTimingHandlerFactory* timingFactory = mTimingHandlerFactories.getEntry(name);

        // attempt to create the timing handler instance
        if (!timingFactory || !(mTimingHandler = timingFactory->createHandler()))
            return false;

        std::vector<SystemGroup*> systemGroups = SystemManager::getSingleton().
            getGroupsAsVector();

        return mTimingHandler->setSystemGroups(systemGroups);
    }
    //-------------------------------------------------------------------------
    void EngineManager::destroyTimingHandler()
    {
        if (mTimingHandler)
        {
            mTimingHandler->getCreator()->destroyHandler(mTimingHandler);
            mTimingHandler = 0;
        }
    }
    //-------------------------------------------------------------------------
    EngineManager* EngineManager::createSingleton()
    {
        if (!mSingleton)
            mSingleton = new EngineManager();
        
        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void EngineManager::destroySingleton()
    {
        if (mSingleton)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //-------------------------------------------------------------------------
    void EngineManager::deleteDefaultManagers()
    {
        // TODO refactor so that mgr do register automatically at creation/destruction
        if (mDefaultGuiManager)
        {
            mSystemManager->unregisterSystem(mDefaultGuiManager);
            delete mDefaultGuiManager;
            mDefaultGuiManager = 0;
        }
        if (mDefaultPhysicsManager)
        {
            mSystemManager->unregisterSystem(mDefaultPhysicsManager);
            delete mDefaultPhysicsManager;
            mDefaultPhysicsManager = 0;
        }
        if (mDefaultScriptManager)
        {
            mSystemManager->unregisterSystem(mDefaultScriptManager);
            delete mDefaultScriptManager;
            mDefaultScriptManager = 0;
        }
    }
    //-------------------------------------------------------------------------
}
