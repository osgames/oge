/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/scene/OgeSceneManager.h"
#include "oge/message/OgeMessage.h"

namespace oge
{
    //-------------------------------------------------------------------------
    void SceneManager::clearScene()
    {
        ComponentMapIter iter = this->mComponents.begin();
        while (iter != this->mComponents.end())
        {
            destroyComponent( (iter->second)->getObjectId() );
            iter = this->mComponents.begin();
        }
        mUpdatableComponents->clear();
    }
    //-------------------------------------------------------------------------
    void SceneManager::update(double deltaTime)
    {
        // NEXT: Called here or a few lines below?
        processMessages();
        // NEXT: Called here or a few lines above or below?
        updateComponents(deltaTime);
    }
    //-------------------------------------------------------------------------
    bool SceneManager::addComponent(Component* component)
    {
        assert(component);
        if (0 == component)
            return false;

        const ObjectId& id = component->getObjectId();

        ComponentMapPairIter itp = mComponents.equal_range(id);
        for (ComponentMapIter it = itp.first; it != itp.second; ++it)
        {
            if (component == (*it).second)
            {
                LOGW(String("Can't add a component twice: ") + component->getType());
                // TODO Do something else than return? Such as deleting the component? Mem leak?
                return false;
            }
        }

        // Add to list of ALL components in this scene manager
        // old code mComponents[id] = component;
        mComponents.insert( ComponentPair( id , component ) );

        // Try to add it to list of updatable components
        // NO! It is done when activating with _registerUpdatableComponent( component );

        return true;
    }
    //-------------------------------------------------------------------------
    Component* SceneManager::getComponent(const ObjectId& id,
        const ComponentType& family)
    {
        ComponentMapPairIter itp = mComponents.equal_range(id);
        for (ComponentMapIter it = itp.first; it != itp.second; ++it) {
            if (((*it).second)->getFamily() == family) {
                return (*it).second;
            }
        }
        return 0;
    }
	//-------------------------------------------------------------------------
	Component* SceneManager::getComponent(const ObjectId& id, 
		const ComponentType& family, const ComponentType& type)
	{
        ComponentMapPairIter itp = mComponents.equal_range(id);
        for (ComponentMapIter it = itp.first; it != itp.second; ++it) {
            if (((*it).second)->getFamily() == family && ((*it).second)->getType() == type) {
                return (*it).second;
            }
        }
        return 0;
	}
    //-------------------------------------------------------------------------
    Component* SceneManager::getFirstComponent(const ObjectId& id)
    {
        ComponentMapIter it = mComponents.find(id);
        if (it != mComponents.end())
        {
            return (*it).second;
        }
        return 0;
    }
    //-------------------------------------------------------------------------
    void SceneManager::activateComponent(bool activate, const ObjectId& id)
    {
        ComponentMapPairIter itp = mComponents.equal_range(id);
        for (ComponentMapIter it = itp.first; it != itp.second; ++it)
        {
            // TODO Is activation and updatable always related?
            if (activate)
                registerUpdatableComponent((*it).second);
            else
                unregisterUpdatableComponent((*it).second);

            (*it).second->_activate(activate, this);
        }
    }
    //-------------------------------------------------------------------------
	void SceneManager::activateComponent(bool activate, const ObjectId& id, 
		const ComponentType& type)
    {
        ComponentMapPairIter itp = mComponents.equal_range(id);
        for (ComponentMapIter it = itp.first; it != itp.second; ++it)
        {
			if(((*it).second)->getType() == type) {
				if (activate)
					registerUpdatableComponent((*it).second);
				else
					unregisterUpdatableComponent((*it).second);

				(*it).second->_activate(activate, this);
			}
        }
    }
    //-------------------------------------------------------------------------
    bool SceneManager::_createComponent(const ObjectId& id, const ComponentType& type, 
        const MessageList& params)
    {
        // Note this will call ComponentFactory::createComponent()
        Component* component = 
            ObjectManager::getSingleton()._createComponent(type);
//_ NOW
//__LOG(String("New component ")+ StringUtil::toString((void*)component) + " " + type + " " +id);
        if (0 == component)
        {
            LOGI("Couldn't create a component of type '"+type+"' for the object id '"+id+"'");
            return false;
        }

        // TODO Should we set it in the constructor?
        component->setObjectId( id );

        // NEXT A test could be put here to check if a component is incompatible with a certain SM
        // if (this->getName() == objectTemplate.incompatibleSM())
        // {
        //     LOGW("This component type '"+type+"' is not supported by the scene manager "+this->getName());
        //     return 0;
        // }

        // Store component
        if (!addComponent( component ))
        {
            LOGW("The component couldn't be stored in the SceneMgr! Potential mem leak!");
        }

        // Reset it ( ... , const MessageList& params, SceneManager* sm)
        if (!component->_reset(params, this))
        {
            LOGE("The component couldn't be reset."); // TODO delete component here? more log?
            return false;
        }

        // Inactivate the component
        // NOTE: it will be activated at the end of ObjectSceneManager::createObject()
        // TODO Shouldn't it be before create component or at least before addcomponent
        //      but the problem is that some component need to execute _reset first
        //      see GraphicsComponentOGRE for example.
        component->_activate(false, this);  // TODO Should be default in contructor, no?

        return true;
    }
    //-------------------------------------------------------------------------
    bool SceneManager::destroyComponent(const ObjectId id)
    {
        ComponentMapIter iter = this->mComponents.find(id);
        bool result = false;
        // TODO optimise: there must be a better loop to erase
        while (iter != this->mComponents.end())
        {
            Component* component = iter->second;
            
            // TODO/FIXME: 'unsigned int' is not enough for x64!!! + a compiler warn! AND ios::hex does not work!!!
            LOGI( "Destroying a game component with address: "
                + StringUtil::toString(component, 0, ' ', std::ios::hex | std::ios::showbase) )

            // aka activateComponent(false, id);
            unregisterUpdatableComponent(component);
            component->_activate(false, this); // TODO: Evaluate... REMOVE???
            component->_destroy(this);

            mComponents.erase(iter);
            delete component;

            result = true;

            iter = this->mComponents.find(id);
        }
        return result; // not found!
    }
    //-------------------------------------------------------------------------
    void SceneManager::registerUpdatableComponent(Component* component)
    {
        LOGI( "Registering an updatable component of type: " + component->getType() + ", Address: "
            + StringUtil::toString(component, 0, ' ', std::ios::hex | std::ios::showbase) );

        for (int i = 0; Component::NUM_UPDATE_RATES > i; ++i)
        {
            if (mUpdatableComponents[i].find(component) != mUpdatableComponents[i].end())
            {
                LOGW( "The component at address '"
                    + StringUtil::toString(component, 0, ' ', std::ios::hex | std::ios::showbase)
                    + "' was already registered in updatable list #" + StringUtil::toString(i) + "!" );

                // Log type of component in another line, so we have something logged before a
                // crash happens due to a null/invalid address as it should work in final releases.
                LOGW( "Component type: " + component->getType()
                    + " -- Scene manager type: " + getType() + ", Name: " + getName() );
            }
        }

        int updateRate = component->_getUpdateRate();
        if (updateRate > Component::NO_UPDATE &&
            updateRate < Component::NUM_UPDATE_RATES)
        {
            mUpdatableComponents[updateRate].insert(component);
        }
        else
        {
            LOGW( "Could not register a component of type '" + component->getType() + "' (address: "
                + StringUtil::toString(component, 0, ' ', std::ios::hex | std::ios::showbase)
                + ") in update lists because its update rate is invalid: " + StringUtil::toString(updateRate) );
        }
    }
    //-------------------------------------------------------------------------
    void SceneManager::unregisterUpdatableComponent(Component* component)
    {
        LOGI( "Unregistering an updatable component of type: " + component->getType() + ", Address: "
            + StringUtil::toString(component, 0, ' ', std::ios::hex | std::ios::showbase) );

        int updateRate = component->_getUpdateRate();
        if (updateRate > Component::NO_UPDATE &&
            updateRate < Component::NUM_UPDATE_RATES)
        {
            ComponentSet &compList = mUpdatableComponents[updateRate];
            if (compList.find(component) != compList.end())
            {
                mUpdatableComponents[updateRate].erase(component);
            }
            else
            {
                LOGW( "Could not unregister a component of type '" + component->getType() + "' (address: "
                    + StringUtil::toString(component, 0, ' ', std::ios::hex | std::ios::showbase)
                    + ") from update list #" + StringUtil::toString(updateRate)
                    + " because it was not already registered!" );
            }
        }
        else
        {
            LOGW( "Could not unregister a component of type '" + component->getType() + "' (address: "
                + StringUtil::toString(component, 0, ' ', std::ios::hex | std::ios::showbase)
                + ") from update lists because its update rate is invalid: " + StringUtil::toString(updateRate) );
        }
    }

    //-------------------------------------------------------------------------
    AsyncBool SceneManager::createComponent(const ObjectId& id, 
        const ComponentType& type, const MessageList& params)
    {
        typedef DelegateTask3<const ObjectId&, const ComponentType&, const MessageList&, bool> DT;
        DT* dt = new DT(id, type, params, 
            DT::Delegate(this, &SceneManager::_createComponent));

        AsyncBool result = dt->getResult();
        dt->run();
        delete dt;
        return result;
    }
	//-------------------------------------------------------------------------
	void SceneManager::postTick(double currentTime)
	{
        for (int i = 0; Component::NUM_UPDATE_RATES > i; ++i)
        {
            ComponentSet &compList = mUpdatableComponents[i];
			ComponentIter iter, iterCopy;
            for (iter = compList.begin(); compList.end() != iter;)
            {
				// copy the current iterator
				iterCopy = iter;

				// advance iter BEFORE calling _postTick() so if a component
				// removes itself from the queue our iterator won't be broken
				++iter;

				// call update from the copied iterator
                (*iterCopy)->_postTick(currentTime);
            }
        }
	}
	//-------------------------------------------------------------------------
	void SceneManager::preTick(double currentTime)
	{
        for (int i = 0; Component::NUM_UPDATE_RATES > i; ++i)
        {
            ComponentSet &compList = mUpdatableComponents[i];
			ComponentIter iter, iterCopy;
            for (iter = compList.begin(); compList.end() != iter;)
            {
				// copy the current iterator
				iterCopy = iter;

				// advance iter BEFORE calling _preTick() so if a component
				// removes itself from the queue our iterator won't be broken
				++iter;

				// call update from the copied iterator
                (*iterCopy)->_preTick(currentTime);
            }
        }
	}
    //-------------------------------------------------------------------------
    void SceneManager::updateComponents(double deltaTime)
    {
        for (int i = 0; Component::NUM_UPDATE_RATES > i; ++i)
        {
            ComponentSet &compList = mUpdatableComponents[i];
			ComponentIter iter, iterCopy;
            for (iter = compList.begin(); compList.end() != iter;)
            {
				// copy the current iterator
				iterCopy = iter;

				// advance iter BEFORE calling update() so if a component
				// removes itself from the queue our iterator won't be broken
				++iter;

				// call update from the copied iterator
                (*iterCopy)->_update(deltaTime);
            }
        }
    }
    //-------------------------------------------------------------------------
    void SceneManager::processMessages()
    {
        double startTime = mClock.updateAndGetTime();
        Message message;

        mLastProcessedMessagesCount = mMessageQueue.size();
        while( !mMessageQueue.empty()) {
            mMessageQueue.pop(message);

            if (message.getReceiver().type() == typeid(ObjectId))  {
                ComponentMapPairIter itp = mComponents.equal_range(FastAnyCast(ObjectId, message.getReceiver()));
                for (ComponentMapIter it = itp.first; it != itp.second; ++it) {
                    if (message.getSender() != (*it).second) {
                        (*it).second->_processMessage(message);
					}
                }
            }
            else {
                // TODO: should we do something with the return? [RL]
                (void)processMessage(message);
            }
        }

        mLastMessageProcessingTime = mClock.updateAndGetTime() - startTime;
    }    
    //-------------------------------------------------------------------------
    bool SceneManager::processMessage(const Message& message)
    {
        switch (message.getType())
        {
			case ObjectMessage::SET_OBJECT_ACTIVE:
			{
				const ObjectId *id = FastAnyCast(ObjectId, &message.getParam_1());
				bool active = FastAnyCast(bool, message.getParam_2());
				LOGI( "Activating a game object with id: '" + *id + "'" );
				activateComponent(active,*id);
			}
			break;
			case ObjectMessage::DESTROY_OBJECT:
            {
				const ObjectId *id = FastAnyCast(ObjectId, &message.getParam_1());

				// TODO: destroyed throws a non-used warning for GCC where LOGWC compiled out.
				// Figure a way to silence this. [RL]
				bool destroyed = destroyComponent(*id);
				LOGWC( !destroyed, "Could not destroy the game object with Id '"
					+ *id + "' in scene manager: " + getName() );
            }
            break;
			default: return false;
         }

        return true;
    }
    //-------------------------------------------------------------------------
    bool SceneManager::processMessageImmediate(const Message& message)
    {
		bool processed =  false;
        if (message.getReceiver().type() == typeid(ObjectId))  {
            ComponentMapPairIter itp = mComponents.equal_range(FastAnyCast(ObjectId, message.getReceiver()));
            for (ComponentMapIter it = itp.first; it != itp.second; ++it) {
                if (message.getSender() != (*it).second) {
					(*it).second->_processMessage(message);
				}
            }
        }
        else {
            processed = processMessage(message);
        }

		return processed;
	}
	//-------------------------------------------------------------------------
}
