/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/scene/OgeSceneManagerEnumerator.h"
#include "oge/scene/OgeSceneManager.h"

namespace oge
{

    //-------------------------------------------------------------------------
    SceneManagerEnumerator::SceneManagerEnumerator(const String& type) : 
        mType(type)
    {
    }
    //-------------------------------------------------------------------------
    SceneManagerEnumerator::~SceneManagerEnumerator()
    {
        // NOTE we can't delete them here because the SM
        // must be destroyed in their own thread/plugin
        // for example an ogre sm must be deleted before 
        // the ogre root is destroyed.
        // Also it is the ObjectSceneManager responsability to destroy its 
        // 'sub' SMs.
        // mSceneManagers.deleteAll();
        mFactories.deleteAll();
    }
    //-------------------------------------------------------------------------
    bool SceneManagerEnumerator::registerSceneManagerFactory(
        SceneManagerFactory* factory)
    {
        // Check if the system SM is available
        if (!factory->checkAvailability())
        {
            LOGE(String("The scene manager of type ")
                +factory->getType()+" is not available!");
            return false;
        }

        const String& factoryType = factory->getType();

        if (mFactories.entryExists(factoryType))
        {
            LOGW(String("This factory already exist: ")+factoryType);
            return false;
        }

        mFactories.addEntry(factoryType, factory);
        return true;
    }
    //-------------------------------------------------------------------------
    bool SceneManagerEnumerator::unregisterSceneManagerFactory(
        const String& type)
    {
        SceneManagerFactoryMap::Iterator iter = mFactories.find(type);
        if (iter == mFactories.end())
            return false;

        mFactories.removeEntry(iter);
        return true;
    }
    //-------------------------------------------------------------------------
    SceneManager* SceneManagerEnumerator::createSceneManager(const String& name,
        const String& type)
    {
        LOGI(String("CreateSceneManager: ") + name + " (type: " + type +")");

        // Check if the sm already exist
        if (mSceneManagers.entryExists(name))
            return mSceneManagers.getEntry(name);

        SceneManagerFactory* factory = mFactories.getEntry(type);
        if (!factory)
        {
            LOGE("A factory of type " + type + " was not found.");
            return 0;
        }

        SceneManager* sceneManager = factory->createInstance(name);

        if(sceneManager==0)
        {
            LOGE(String("A SceneManager of type") + type + " couldn't be created.");
            return 0;
        }

        mSceneManagers.addEntry(name, sceneManager);

        return sceneManager;
    }
    //-------------------------------------------------------------------------
    SceneManager* SceneManagerEnumerator::_getSceneManager(const String& name)
    {
        return mSceneManagers.getEntry(name);
    }
    //-------------------------------------------------------------------------
    void SceneManagerEnumerator::deleteSceneManager(const String& name)
    {
        LOG(String("Deleting scene manager: ")+name);

        // TODO Test if in use ? If yes, redo when not in use ?

        mSceneManagers.deleteEntry(name);
    }
    //-------------------------------------------------------------------------
}
