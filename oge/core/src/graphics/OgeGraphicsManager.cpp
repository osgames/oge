/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/scene/OgeSceneManagerEnumerator.h"
#include "oge/graphics/OgeTextRenderer.h"
#include "oge/graphics/OgeGraphicsSceneManager.h"
#include "oge/object/OgeComponent.h"

namespace oge
{
    //-------------------------------------------------------------------------
    GraphicsManager* GraphicsManager::mManager = 0;
    //-------------------------------------------------------------------------
    GraphicsManager::GraphicsManager(const String& name, int systemInitOrdering, int systemRunOrdering) : 
        System(name, systemInitOrdering, systemRunOrdering), mSceneManager(0)
    {
        mManager = this;
        mSceneManagerEnumerator = new SceneManagerEnumerator("Graphics");

        // Create a basic text renderer mostly used for debugging
        // Note that this TextRender by default does nothing it is a dummy
        //      that must be overriden by the plugin!
        mTextRenderer = new TextRenderer();

        LOG(String("Created ") + name);
    }
    //-------------------------------------------------------------------------    
    GraphicsManager::~GraphicsManager()
    {
        delete mTextRenderer;
        delete mSceneManagerEnumerator;
        mSceneManagerEnumerator = 0;
        LOG("Destroyed ");
    }
    //-------------------------------------------------------------------------    
    SceneManager* GraphicsManager::createSceneManager( 
        const String& name, const String& type )
    {
        return mSceneManagerEnumerator->createSceneManager( name, type );
    }
    //-------------------------------------------------------------------------
    void GraphicsManager::addTextBox( const String& id, const String& text,
        Real x, Real y, Real width, Real height)
    {
        mTextRenderer->addTextBox( id, text, x, y, width, height );
    }
    //-------------------------------------------------------------------------
    void GraphicsManager::removeTextBox(const String& id)
    {
        mTextRenderer->removeTextBox( id );
    }
    //-------------------------------------------------------------------------
    void GraphicsManager::setText(const String& id, const String& text)
    {
        mTextRenderer->setText( id, text );
    }
    //-------------------------------------------------------------------------
	void GraphicsManager::preTick(double currentTime)
	{
		if(mSceneManager) {
			mSceneManager->preTick(currentTime);
		}
	}
	//-------------------------------------------------------------------------
	void GraphicsManager::postTick(double currentTime)
	{
		if(mSceneManager) {
			mSceneManager->postTick(currentTime);
		}
	}
}
