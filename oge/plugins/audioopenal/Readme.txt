
  USAGE
 -------
 
 Simple for now :)

 This will play a streamed ogg file:
   
    AudioManagerOpenAL* am = AudioManagerOpenAL::getSingletonPtr();
    AudioPtr audio = am->createAudio("./Epoq-Lepidoptera.ogg", true);
    audio->load();
    audio->play();
    audio->pause();
    audio->play();
    audio->stop();

 If you want to restart an audio call setRestart(true) then play().
 I didn't want to pass an additional parameter to play() if it is not needed.

 If streamed the audio::update() must be called regularly
 but this should be done automatically by the AudioContext
  

 TODOs & Documentation about OpenAL, OpenAL Soft and vorbis/ogg
------------------------------------------------------------------
   
 TODO URGENT
=============
 
 * BIG/LOW Endian See OgeAudioWav.cpp  #define OGE_BIG_ENDIAN 0
 * Use oge::FileSystemStream....  Careful about unsigned char we need to reinterpret_cast
 * Use of the ResourceManager but how...? Be sure to study "ogg streaming" first 
   to understand the difficulties. At least we shouldn't use the full paths!
 * The audios & listener are not "automatically" updated for position, location, etc 
   but this should be the AudioComponent task anyway.

 TODO
======

 * For now I commented out the code to limit the number of buffers (but it works)
   Make it an optional method: limitNbBuffers() or something like this.
 * Implement a playOnce() aka the audio is played once then the source
   is released, the buffer cleared, etc. 
 * Add a fade mode (FADE_NONE, FADE_IN & FADE_OUT) aka change the gain over time
   until max or min gain.

 * Study buffer reuse & ogg support http://www.ogre3d.org/phpBB2/viewtopic.php?t=26235
 * Should we put the vorbis.lib & ogg.lib file in /vorbis or /AL ?
 * Profile to determine if the alGetError() must be removed from time critical methods 
   like AudioListener::setPosition() and such. Or at least only in debug
 * Take advantage of the alcSuspendContext method (see page 103 of OpenAL_Programmers_Guide.pdf)
 * Study the OpenAL Extension (see appendix of the specification pdf)


 TODO DIFFICULT
================

 * If not enough sources are available stop the least important ones
   based on volume & priority. For example a small bird chipping is less important
   than a big explosion or a NPC whispering. 
   Player sounds and audiotracks must have very high priority
   the rest is based on the distanc from the listener and/or how long it has 
   been since that sound was started.
 * Same as above for the buffers. 
 * Make the audios wait in a waiting list with an importance ranking and a time-out
   if not important.
 * Create a save/load the audio state: context, device, audio, buffer, sources, etc.
   This permits to reload a level at exactly the same point... :/
 * The later should also be used for recreating a device
   
   
  Documentation
=================
 
 * Reasons to switch to OpenAL Soft: 
   http://opensource.creative.com/pipermail/openal/2008-February/010910.html

 * http://www.gamedev.net/community/forums/topic.asp?topic_id=351251

 OpenAL
 ------
 * http://opensource.creative.com/pipermail/openal-devel/
 * http://opensource.creative.com/pipermail/openal/

 * Official webiste http://connect.creativelabs.com/openal/default.aspx
 * OpenAL Documentation:
     http://connect.creativelabs.com/openal/Documentation/Forms/AllItems.aspx
     http://connect.creativelabs.com/openal/Documentation/OpenAL%201.1%20Specification.pdf
 * Enumeration http://connect.creativelabs.com/openal/OpenAL%20Wiki/Enumeration%20with%20OpenAL%20on%20Windows.aspx

 * DevMaster Tutorial http://www.devmaster.net/articles.php?catID=6
            OggVorbis Streaming http://www.devmaster.net/articles/openal-tutorials/lesson8.php

 OpenAL Soft
 -----------
 * http://kcat.strangesoft.net/openal.htlm
   - Fixed bugs existing in OpenAL
   - Uses less memory
   - Low-pass filter improved
   - Improved EFX compliance
   - etc.


 Others
 ------
 * unsigned int / int
    char* data1;
    unsigned char* data = reinterpret_cast<unsigned char*> (data1);
   http://www.ddj.com/cpp/184403316
   http://en.wikipedia.org/wiki/Integer_(computer_science)
 
