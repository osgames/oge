/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/audio/OgeAudioListener.h"
#include "oge/logging/OgeLogManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    void AudioListener::setPosition( const Vector3& position)
    {
    #ifdef _DEBUG
        alGetError();
        alListenerfv( AL_POSITION, (const ALfloat*)position.ptr() );
        LOGWC(alGetError() != AL_NO_ERROR, "alListenerfv:AL_POSITION");
    #else
        alListenerfv( AL_POSITION, (const ALfloat*)position.ptr() );
    #endif
    }
    //-------------------------------------------------------------------------
    void AudioListener::setVolume(float volume)
    {
    #ifdef _DEBUG
        alGetError();
        alListenerf(AL_GAIN, volume);
        LOGWC(alGetError() != AL_NO_ERROR, "alListenerfv error while setting the volume");
    #else
        alListenerf(AL_GAIN, volume);
    #endif
    }
    //-------------------------------------------------------------------------
    void AudioListener::setVelocity(const Vector3& velocity)
    {
    #ifdef _DEBUG
        alGetError();
        alListenerfv(AL_VELOCITY, (const ALfloat *)velocity.ptr());
        LOGWC(alGetError() != AL_NO_ERROR, "alListenerfv:AL_VELOCITY");
    #else
        alListenerfv(AL_VELOCITY, (const ALfloat*)velocity.ptr());
    #endif
    }
    //-------------------------------------------------------------------------
    void AudioListener::setOrientation(const Quaternion& orientation)
    {
    #ifdef _DEBUG
        alGetError();
        Vector3 axis(Vector3::UNIT_Z);
// TODO   Vector3 axis;
//        axis.x = orientation.getYaw().valueRadians();
//        axis.y = orientation.getPitch().valueRadians();
//        axis.z = orientation.getRoll().valueRadians();
        ALfloat dir[] = { axis.x, axis.y, axis.z };
        alListenerfv( AL_ORIENTATION, dir );
        LOGWC(alGetError() != AL_NO_ERROR, "alListenerfv:AL_DIRECTION");
    #else
        Vector3 axis(Vector3::UNIT_Z);
// TODO   Vector3 axis;
//        axis.x = orientation.getYaw().valueRadians();
//        axis.y = orientation.getPitch().valueRadians();
//        axis.z = orientation.getRoll().valueRadians();
        ALfloat dir[] = { axis.x, axis.y, axis.z };
        alListenerfv( AL_ORIENTATION, dir );
    #endif
    }
    //-------------------------------------------------------------------------
    void AudioListener::setLocation( const Vector3& position, 
        const Vector3& velocity, const Quaternion& orientation)
    {
    #ifdef _DEBUG
        alGetError();

        alListenerfv( AL_POSITION, (const ALfloat *)position.ptr() );
        LOGWC(alGetError() != AL_NO_ERROR, "alListenerfv:AL_POSITION");

        alListenerfv( AL_VELOCITY, (const ALfloat *)velocity.ptr() );
        LOGWC(alGetError() != AL_NO_ERROR, "alListenerfv:AL_VELOCITY");

        Vector3 axis(Vector3::UNIT_Z);
// TODO   Vector3 axis;
//        axis.x = orientation.getYaw().valueRadians();
//        axis.y = orientation.getPitch().valueRadians();
//        axis.z = orientation.getRoll().valueRadians();
        ALfloat dir[] = { axis.x, axis.y, axis.z };
        alListenerfv( AL_ORIENTATION, dir );
        LOGWC(alGetError() != AL_NO_ERROR, "alListenerfv:AL_DIRECTION");
    #else
        alListenerfv( AL_POSITION, (const ALfloat*)position.ptr() );
        alListenerfv( AL_VELOCITY, (const ALfloat*)velocity.ptr() );

        Vector3 axis(Vector3::UNIT_Z);
// TODO   Vector3 axis;
//        axis.x = orientation.getYaw().valueRadians();
//        axis.y = orientation.getPitch().valueRadians();
//        axis.z = orientation.getRoll().valueRadians();
        ALfloat dir[] = { axis.x, axis.y, axis.z };
        alListenerfv( AL_ORIENTATION, dir );
    #endif
    }
    //-------------------------------------------------------------------------
    void AudioListener::setListener( const Vector3& position, 
        const Vector3& velocity, const Quaternion& orientation,
        float gain, float maxDistance, float minGain, float maxGain )
    {
    #ifdef _DEBUG
        alGetError();

        alListenerfv( AL_POSITION, (const ALfloat *)position.ptr() );
        LOGWC(alGetError() != AL_NO_ERROR,"alListenerfv:AL_POSITION");

        alListenerfv( AL_VELOCITY, (const ALfloat *)velocity.ptr() );
        LOGWC(alGetError() != AL_NO_ERROR, "alListenerfv:AL_VELOCITY");

        alListenerf( AL_GAIN, gain ); // TODO Are we obliged to do it before orientation?

        Vector3 axis(Vector3::UNIT_Z);
// TODO   Vector3 axis;
//        axis.x = orientation.getYaw().valueRadians();
//        axis.y = orientation.getPitch().valueRadians();
//        axis.z = orientation.getRoll().valueRadians();
        ALfloat dir[] = { axis.x, axis.y, axis.z };
        alListenerfv( AL_ORIENTATION, dir );
        LOGWC(alGetError() != AL_NO_ERROR, "alListenerfv:AL_DIRECTION");

        alListenerf( AL_MAX_DISTANCE, maxDistance );
        alListenerf( AL_MIN_GAIN, minGain );
        alListenerf( AL_MAX_GAIN, maxGain );
        LOGWC(alGetError() != AL_NO_ERROR, "alListenerfv error in setting distance or gain");
    #else
        alListenerfv( AL_POSITION, (const ALfloat*)position.ptr() );
        alListenerfv( AL_VELOCITY, (const ALfloat*)velocity.ptr() );
        alListenerf( AL_GAIN, gain );

        Vector3 axis(Vector3::UNIT_Z);
// TODO   Vector3 axis;
//        axis.x = orientation.getYaw().valueRadians();
//        axis.y = orientation.getPitch().valueRadians();
//        axis.z = orientation.getRoll().valueRadians();
        ALfloat dir[] = { axis.x, axis.y, axis.z };
        alListenerfv( AL_ORIENTATION, dir );

        alListenerf( AL_MAX_DISTANCE, maxDistance );
        alListenerf( AL_MIN_GAIN, minGain );
        alListenerf( AL_MAX_GAIN, maxGain );
    #endif
    }
    //-------------------------------------------------------------------------
}
