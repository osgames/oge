/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/audio/OgeAudioBuffer.h"
#include "oge/audio/OgeAudioManagerOpenAL.h"
#include "oge/audio/OgeAudioBufferManager.h"
#include "oge/datastreams/OgeFileSystemStream.h"
#include "oge/logging/OgeLogManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    AudioBuffer::AudioBuffer(unsigned int nbBuffers) : 
        mBuffer(AL_NONE), mNbBuffers( nbBuffers )
    {
        LOGWC((mNbBuffers < 1), "The nb of buffers asked must be bigger than 0!")
        // if (nbBuffers == 0) nbBuffers = 1; this shouldn't be necesssary ;)

        loadImpl();
    }
    //-------------------------------------------------------------------------
    AudioBuffer::~AudioBuffer()
    {
        unloadImpl();
    }
    //-------------------------------------------------------------------------
    void AudioBuffer::unloadImpl()
    {
        if (mBuffer != AL_NONE)
        {
            alDeleteBuffers(mNbBuffers, mBuffer);
            delete [] mBuffer;
            mBuffer = AL_NONE;
        }
    }
    //-------------------------------------------------------------------------
    bool AudioBuffer::loadImpl()
    {
        try
        {
            alGetError();
            mBuffer = new unsigned int[mNbBuffers];
            alGenBuffers(mNbBuffers, mBuffer);
        }
        catch (...)
        {
        }

        ALenum error = alGetError();
        if (error != AL_NO_ERROR || mBuffer == AL_NONE)
        {
            if (error == AL_INVALID_VALUE)
                LOGE("Could not generate "+StringUtil::toString(mNbBuffers)+" OpenAL buffer: AL_INVALID_VALUE")
            else if (error == AL_OUT_OF_MEMORY)
                LOGE("Could not generate "+StringUtil::toString(mNbBuffers)+" OpenAL buffer: AL_OUT_OF_MEMORY")
            else
                LOGE("Could not generate "+StringUtil::toString(mNbBuffers)+" OpenAL buffer: unknown cause.")

            mBuffer = AL_NONE;
            return false;
        }
        return true;
    }
    //-------------------------------------------------------------------------


    //-------------------------------------------------------------------------
    //AudioBuffer::AudioBuffer(const String& name, ResourceManager* owner) : 
    //    Resource(name, owner), mBufferHandle(0)
    //{

    //}
    ////-------------------------------------------------------------------------
    //bool AudioBuffer::loadImpl()
    //{
    //    FileSystemStream* fileStream = new FileSystemStream();
    //    if (!fileStream->openRead(mName))
    //    {
    //        delete fileStream;
    //        return false;
    //    }

    //    DataStreamPtr dataStream(fileStream);

    //    AudioBufferManager* manager = 
    //        AudioManagerOpenAL::getSingletonPtr()->getAudioBufferManager();

    //    AudioCodec* codec = manager->getAudioCodec(dataStream);
    //    
    //    if (!codec)
    //        return false;

    //    bool success = false;

    //    alGenBuffers(1, &mBufferHandle);
    //    if (alGetError() == AL_NO_ERROR && mBufferHandle != AL_NONE)
    //    {
    //        int format;
    //        std::vector<char> data;
    //        size_t size;
    //        size_t frequency;

    //        if (codec->load(dataStream, format, data, size, frequency))
    //        {
    //            alBufferData(mBufferHandle, format, &data[0], size, frequency);
    //            if(alGetError() == AL_NO_ERROR)
    //            {
    //                success = true;
    //            }
    //        }
    //    }

    //    AudioCodecFactory* factory = codec->getCreator();
    //    if (factory)
    //        factory->destroyCodec(codec);
    //    
    //    return success;
    //    return true;
    //}
    //-------------------------------------------------------------------------
}
