/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/audio/OgeAudioPrerequisitesOpenAL.h"
#include "oge/audio/codecs/OgeAudioOgg.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/datastreams/OgeFileSystemStream.h"
#include "oge/audio/OgeAudioBufferManager.h"
#include "oge/audio/OgeAudioManagerOpenAL.h"
#include "oge/audio/codecs/OgeAudioOggStream.h"

namespace oge
{
    #define OGE_BIG_ENDIAN 0  // as this will not work on LOW endian machine !
    #define OGG_BUFFER_SIZE   32768     // 32 KB buffers TODO should we use the same as in OgeAudioWav.cpp
    
    // Note NOT the same usage as for OGG_BUFFER_SIZE !!
    //#define BUFFER_SIZE (4096*6) crash if the console windows is grabbed
    #define BUFFER_SIZE (4096*12) // some cliks if the console is grabbed !
    //#define BUFFER_SIZE (4096*24) not better

/*    // TODO THIS SHOULDN'T BE HERE !!!

    static unsigned short read16Byte(const unsigned char buffer[2])
    {
        #if OGE_BIG_ENDIAN
            return (buffer[0] << 8) + buffer[1];
        #else
            return (buffer[1] << 8) + buffer[0];
        #endif
        }
        static unsigned long read32Byte(const unsigned char buffer[4]) {
        #if OGE_BIG_ENDIAN
            return (buffer[0] << 24) + (buffer[1] << 16) + (buffer[2] << 8) + buffer[3];
        #else
            return (buffer[3] << 24) + (buffer[2] << 16) + (buffer[1] << 8) + buffer[0];
        #endif
    }
*/
    //-------------------------------------------------------------------------
    AudioOgg::AudioOgg(AudioFactory* creator, const String& filename) : 
        AudioOpenAL(creator, filename), mBuffer(0)
    {
        LOGE(" ====== OGG ====== ");
    }
    //-------------------------------------------------------------------------
    AudioOgg::~AudioOgg()
    {
        LOGE(" ====== DELETE OGG ====== ");
    }
    //-------------------------------------------------------------------------
    bool AudioOgg::load()
    {
        LOGI("Loading a ogg audio from file...");

        // Test if the filename was already loaded WITH THE SAME NUMBER OF BUFFERS!
        mBuffer = AudioBufferManager::getSingletonPtr()->
            getBuffer( mFilename, 1 );
            
        if (mBuffer)
            return true;

        return _load();
    }
    //-------------------------------------------------------------------------
    bool AudioOgg::_load()
    {
        // This filename is not stored in a buffer -> create the buffer and load it
        alGetError(); // clear alerrors

        FILE* file = 0;
        if(!(file = fopen(mFilename.c_str(), "rb")))
        {
            LOGW("Could not open ogg file: '"+mFilename+"'");
            return false;
        }

        int result;
        OggVorbis_File oggFile;

        //    if((result = ov_open(file, &oggFile, NULL, 0)) < 0)
        if((result = ov_open_callbacks(file, &oggFile, NULL, 0, OV_CALLBACKS_DEFAULT)) < 0)
        {
            fclose(file);
            //throw string("Could not open Ogg stream. ") + errorString(result);
            LOGW("Could not open Ogg stream for: " + mFilename+ " - "
                + getOggErrorString(result));
            return false;
        }

        // Some formatting data
        vorbis_info* vorbisInfo = ov_info(&oggFile, -1);
        // User comments
        vorbis_comment* vorbisComment = ov_comment(&oggFile, -1);

        // Get nb of channels
        // There is also AL_FORMAT_MONO8 and AL_FORMAT_STEREO8 !
        switch (vorbisInfo->channels)
        {
            case 1:
                mFormat = AL_FORMAT_MONO16;
                break;
            case 2:
                mFormat = AL_FORMAT_STEREO16;
                break;
            default:
                LOGW(String("Format of ogg file not supported! Format=")
                    + StringUtil::toString(mFormat));
                ov_clear(&oggFile); // ogg calls fclose(f);
                return false;
        }

        char* array = new char[OGG_BUFFER_SIZE];

        long bytes = 0;
        int bitStream; // ?? what is his use?
        std::vector<char> data;
        do
        {
            // read until buffer's full of decoded sound data
            bytes = ov_read(&oggFile, array, BUFFER_SIZE, OGE_BIG_ENDIAN, 2, 1, &bitStream);
            // append to end of buffer
            data.insert(data.end(), array, array+bytes);
        } while(bytes > 0);

        delete[] array;
        
        LOGD("Vorbis : " + getParameters(vorbisInfo, vorbisComment));
        (void) vorbisComment; // To avoid an unused variable with gcc

        // Must be set before ov_clear!!!
        size_t rate = vorbisInfo->rate;

        // NOTE: this will clear also vorbis_info & vorbis_comment!
        ov_clear(&oggFile); // ogg calls fclose(f);

        // At last we generate the AL buffer
        mBuffer = AudioBufferManager::getSingletonPtr()->createBuffer(mFilename);
        if (mBuffer.isNull())
            return false;

        ALuint* buffer = mBuffer->getBuffer();
        
        // ... and put data in it.
        alBufferData(*buffer, mFormat, &data[0], 
            (ALsizei)(data.size()), (ALsizei)rate);

        if( alGetError() != AL_NO_ERROR)
        {
            LOGW("Could not load buffer data '"+mFilename+"'");
            return false;
        }

        return true;
    }
    //-------------------------------------------------------------------------
    bool AudioOgg::reset()
    {
        // If no openal source then get one
        if (mSource.isNull())
        {
            mSource = AudioManagerOpenAL::getSingletonPtr()->getNextFreeSource(this);

            if (mSource.isNull()) // Should be unnecessary =>   || mSource->mSource == AL_NONE)
                return false;

            alSourcei (mSource->mSource, AL_BUFFER, *mBuffer->getBuffer());
        }

        // This test shouldn't be necessary
        if (mSource->mSource == AL_NONE)
            return false;

        alSource3f(mSource->mSource, AL_POSITION,    mPosition.x, mPosition.y, mPosition.z);
        alSource3f(mSource->mSource, AL_VELOCITY,    mVelocity.x, mVelocity.y, mVelocity.z);
        alSource3f(mSource->mSource, AL_DIRECTION,   mDirection.x, mDirection.y, mDirection.z);

// NEXT openal bug? alSourcei (mSource->mSource, AL_LOOP,   mLoop);
        alSourcef (mSource->mSource, AL_PITCH,              mPitch);
        alSourcef (mSource->mSource, AL_GAIN,               mGain);
        alSourcef (mSource->mSource, AL_MAX_DISTANCE,       mMaxDistance);
        alSourcef (mSource->mSource, AL_ROLLOFF_FACTOR,     mRolloffFactor);
        alSourcef (mSource->mSource, AL_REFERENCE_DISTANCE, mReferenceDistance);
        alSourcef (mSource->mSource, AL_MAX_GAIN,           mMaxGain);
        alSourcef (mSource->mSource, AL_MIN_GAIN,           mMinGain);
        alSourcef (mSource->mSource, AL_CONE_OUTER_GAIN,    mConeOuterGain);
        alSourcef (mSource->mSource, AL_CONE_INNER_ANGLE,   mConeInnerAngle);
        alSourcef (mSource->mSource, AL_CONE_OUTER_ANGLE,   mConeOuterAngle);
        alSourcei (mSource->mSource, AL_SOURCE_RELATIVE,    mSourceRelative);

        return true;
    }
    //-------------------------------------------------------------------------
    String AudioOgg::getParameters(vorbis_info* vorbisInfo, vorbis_comment* vorbisComment)
    {
        String str("OGG audio\n");
        str+= "version         ";
        str+= StringUtil::toString(vorbisInfo->version);
        str+= "\nchannels        ";
        str+= StringUtil::toString(vorbisInfo->channels);
        str+= "\nrate (hz)       ";
        str+= StringUtil::toString(vorbisInfo->rate);
        str+= "\nbitrate upper   ";
        str+= StringUtil::toString(vorbisInfo->bitrate_upper);
        str+= "\nbitrate nominal ";
        str+= StringUtil::toString(vorbisInfo->bitrate_nominal);
        str+= "\nbitrate lower   ";
        str+= StringUtil::toString(vorbisInfo->bitrate_lower);
        str+= "\nbitrate window  ";
        str+= StringUtil::toString(vorbisInfo->bitrate_window);
        str+= "\nvendor ";
        str+= vorbisComment->vendor;
        str+= "\n";

        for(int i = 0; i < vorbisComment->comments; i++)
        {
            str+= "\n   ";
            str+= vorbisComment->user_comments[i];
        }
        return str;
    }
    //-------------------------------------------------------------------------
    String AudioOgg::getOggErrorString(int code)
    {
        switch(code)
        {
            case OV_EREAD:
                return String("Read from media.");
            case OV_ENOTVORBIS:
                return String("Not Vorbis data.");
            case OV_EVERSION:
                return String("Vorbis version mismatch.");
            case OV_EBADHEADER:
                return String("Invalid Vorbis header.");
            case OV_EFAULT:
                return String("Internal logic fault (bug or heap/stack corruption.");
            default:
                return String("Unknown Ogg error.");
        }
    }
    //-----------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    AudioOggFactory::AudioOggFactory()
    {
        mExtensions.push_back("ogg");
    }
    //-------------------------------------------------------------------------
    AudioOggFactory::~AudioOggFactory()
    {
    }
    //-------------------------------------------------------------------------
    AudioPtr AudioOggFactory::createAudio(const String& filename, 
        bool streaming, unsigned int nbBuffers)
    {
        AudioPtr audio;
        if (!streaming)
            audio = new AudioOgg(this, filename);
        else
            audio = new AudioOggStream(this, filename, nbBuffers);

        return audio;
    }
    //-------------------------------------------------------------------------
}
