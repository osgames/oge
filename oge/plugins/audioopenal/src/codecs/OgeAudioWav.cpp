/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/audio/OgeAudioPrerequisitesOpenAL.h"
#include "oge/audio/codecs/OgeAudioWav.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/datastreams/OgeFileSystemStream.h"
#include "oge/audio/OgeAudioBufferManager.h"
#include "oge/audio/OgeAudioManagerOpenAL.h"

namespace oge
{
    // TODO THIS SHOULDN'T BE HERE !!!
    #define OGG_BUFFER_SIZE   32768     // 32 KB buffers
    #define OGE_BIG_ENDIAN 0  // as this will not work on LOW endian machine !

    static unsigned short read16Byte(const unsigned char buffer[2])
    {
        #if OGE_BIG_ENDIAN
            return (buffer[0] << 8) + buffer[1];
        #else
            return (buffer[1] << 8) + buffer[0];
        #endif
        }
        static unsigned long read32Byte(const unsigned char buffer[4]) {
        #if OGE_BIG_ENDIAN
            return (buffer[0] << 24) + (buffer[1] << 16) + (buffer[2] << 8) + buffer[3];
        #else
            return (buffer[3] << 24) + (buffer[2] << 16) + (buffer[1] << 8) + buffer[0];
        #endif
    }
    //-------------------------------------------------------------------------
    AudioWav::AudioWav(AudioFactory* creator, const String& filename) : 
        AudioOpenAL(creator, filename), mBuffer(0)
    {
        // LOGE(" ====== WAV ====== ");
    }
    //-------------------------------------------------------------------------
    AudioWav::~AudioWav()
    {
//std::cout << "AudioWav::~AudioWav()" << std::endl;
        // LOGE(" ====== DELETE WAV ====== ");
    }
    //-------------------------------------------------------------------------
    bool AudioWav::load()
    {
        LOGI("Loading a wav audio from file...");
//std::cout << "load ..." << std::endl;

        // Test if the filename was already loaded WITH THE SAME NUMBER OF BUFFERS!
        mBuffer = AudioBufferManager::getSingletonPtr()->
            getBuffer( mFilename, 1 ); // Only 1 buffer for a wav file
//std::cout << "... load ..." << std::endl;

        if (mBuffer)
            return true;
//std::cout << "...load" << std::endl;

        // This filename is not stored in a buffer -> create the buffer and load it
        return _load();
    }
    //-------------------------------------------------------------------------
    bool AudioWav::_load()
    {
        // This filename is not stored in a buffer -> create the buffer and load it
        alGetError(); // clear alerrors

// TODO Use the ressource mgr and not open it directly !

        FILE* file = 0;
        if(!(file = fopen(mFilename.c_str(), "rb")))
        {
            LOGW("Could not open wav file: '"+mFilename+"'");
            return false;
        }

        char magic[5];
        magic[4] = '\0';
        unsigned char buffer32[4];
        unsigned char buffer16[2];

        // Check magic
        if(!fread(magic, 4, 1, file))
        {
            LOGW("Cannot read wav file: '"+mFilename+"'");
            if (file)
                fclose(file);
            return false;
        }
        if (std::string(magic) != "RIFF")
        {
            LOGW("Wrong wav file: '"+mFilename+"'");
            if (file)
                fclose(file);
            return false;
        }

        // Skip 4 bytes (file size)
        fseek(file, 4,SEEK_CUR);

        // Check file format
        if(!fread(magic, 4, 1, file))
        {
            LOGW("Cannot read wav file: '"+mFilename+"'");
            if (file)
                fclose(file);
            return false;
        }
        if (std::string(magic) != "WAVE")
        {
            LOGW("Wrong wav file: '"+mFilename+"'");
            if (file)
                fclose(file);
            return false;
        }

        // Check 'fmt'
        if(!fread(magic, 4, 1, file))
        {
            LOGW("Cannot read wav file: '"+mFilename+"'");
            if (file)
                fclose(file);
            return false;
        }
        if (std::string(magic) != "fmt ")
        {
            LOGW("Wrong wav file 2 : '"+mFilename+"'");
            if (file)
                fclose(file);
            return false;
        }

        // read size
        if (!fread(buffer32, 4, 1, file))
        {
            LOGW("cannot read wav file: '"+mFilename+"'");
            fclose(file);
            return false;
        }
        unsigned long subChunkSize = read32Byte(buffer32);
        if (subChunkSize < 16)
        {
            LOGW("This file is not a .wav file ('fmt ' chunk too small, truncated file?): '"+mFilename+"'");
            fclose(file);
            return false;
        }

        // check PCM audio format
        if (!fread(buffer16, 2, 1, file))
        {
            LOGW("cannot read wav file: '"+mFilename+"'");
            fclose(file);
            return false;
        }
        unsigned short audioFormat = read16Byte(buffer16);
        if (audioFormat != 1)
        {
            LOGW("This file is not a .wav file (audio format is not PCM): '"+mFilename+"'");
            fclose(file);
            return false;
        }

       // read number of channels
        if (!fread(buffer16, 2, 1, file))
        {
            LOGW("cannot read wav file: '"+mFilename+"'");
            fclose(file);
            return false;
        }
        unsigned short channels = read16Byte(buffer16);

        // read frequency (sample rate)
        if (!fread(buffer32, 4, 1, file))
        {
            LOGW("cannot read wav file: '"+mFilename+"'");
            fclose(file);
            return false;
        }
        unsigned long frequency = read32Byte(buffer32);

        // skip 6 bytes (Byte rate (4), Block align (2))
        fseek(file,6,SEEK_CUR);

        // read bits per sample
        if (!fread(buffer16, 2, 1, file))
        {
            LOGW("cannot read wav file: '"+mFilename+"'");
            fclose(file);
            return false;
        }
        unsigned short bps = read16Byte(buffer16);

        ALenum format;
        if (channels == 1)
            format = (bps == 8) ? AL_FORMAT_MONO8 : AL_FORMAT_MONO16;
        else
            format = (bps == 8) ? AL_FORMAT_STEREO8 : AL_FORMAT_STEREO16;

        // check 'data' chunk (2)
        if (!fread(magic, 4, 1, file))
        {
            LOGW("cannot read wav file: '"+mFilename+"'");
            fclose(file);
            return false;
        }

        if (std::string(magic) != "data")
        {
            LOGW("This file is not a .wav file (no data subchunk): '"+mFilename+"'");
            fclose(file);
            return false;
        }

        if (!fread(buffer32, 4, 1, file))
        {
            LOGW("cannot read wav file: '"+mFilename+"'");
            fclose(file);
            return false;
        }

        unsigned long dataSize = read32Byte(buffer32);

        // Frequency of the sampling rate
        ALsizei freq = frequency; // !!! use an implicit cast
        if (sizeof(freq) != sizeof(frequency))
        {
            LOGW("freq and frequency different sizes: '"+mFilename+"'");
            fclose(file);
            return false;
        }

        char* dataBuffer = new char[OGG_BUFFER_SIZE];
        size_t bytes;
        std::vector<char> data;

        while (data.size() != dataSize)
        {
            // Read up to a buffer's worth of decoded sound data
            bytes = fread(dataBuffer, 1, OGG_BUFFER_SIZE, file);

            if (bytes <= 0)
            break;

            if (data.size() + bytes > dataSize)
            bytes = dataSize - data.size();

            // Append to end of buffer
            data.insert(data.end(), dataBuffer, dataBuffer + bytes);
        };

        delete []dataBuffer;

        fclose(file);
        file = 0;

        // At last we generate the AL buffer
        mBuffer = AudioBufferManager::getSingletonPtr()->createBuffer(mFilename);
        if (mBuffer.isNull())
            return false;

        ALuint* buffer = mBuffer->getBuffer();
        
        /*
        ALuint* buffer = AL_NONE;
        alGenBuffers(1, buffer);
        if (alGetError() != AL_NO_ERROR || buffer == AL_NONE)
        {
            LOGE("Could not generate buffer '"+mFilename+"'");
            return false;
        }
        */
        // ... and put data in it.
        alBufferData(*buffer, format, &data[0], 
            (ALsizei)(data.size()), (ALsizei)freq);

        if( alGetError() != AL_NO_ERROR)
        {
            LOGW("Could not load buffer data '"+mFilename+"'");
            return false;
        }

        return true;
    }
    //-------------------------------------------------------------------------
    bool AudioWav::reset()
    {
        if (mSource.isNull())
        {
//std::cout << "reset ..." << std::endl;
            mSource = AudioManagerOpenAL::getSingletonPtr()->getNextFreeSource(this);

            if (mSource.isNull()) // Should be unnecessary =>   || mSource->mSource == AL_NONE)
            {
                LOGE("Huh... shouldn't be possible!");
                return false;
            }

            alSourcei (mSource->mSource, AL_BUFFER, *mBuffer->getBuffer());
//std::cout << "....reset..." << std::endl;
        }

        // This test shouldn't be necessary
        if (mSource->mSource == AL_NONE)
        {
            LOGE("Huh... shouldn't be possible!");
            return false;
        }

        alSource3f(mSource->mSource, AL_POSITION,  mPosition.x, mPosition.y, mPosition.z);
        alSource3f(mSource->mSource, AL_VELOCITY,  mVelocity.x, mVelocity.y, mVelocity.z);
        alSource3f(mSource->mSource, AL_DIRECTION, mDirection.x, mDirection.y, mDirection.z);

// NEXT openal bug? alSourcei (mSource->mSource, AL_LOOP,   mLoop);
        alSourcef (mSource->mSource, AL_PITCH,              mPitch);
        alSourcef (mSource->mSource, AL_GAIN,               mGain);
        alSourcef (mSource->mSource, AL_MAX_DISTANCE,       mMaxDistance);
        alSourcef (mSource->mSource, AL_ROLLOFF_FACTOR,     mRolloffFactor);
        alSourcef (mSource->mSource, AL_REFERENCE_DISTANCE, mReferenceDistance);
        alSourcef (mSource->mSource, AL_MAX_GAIN,           mMaxGain);
        alSourcef (mSource->mSource, AL_MIN_GAIN,           mMinGain);
        alSourcef (mSource->mSource, AL_CONE_OUTER_GAIN,    mConeOuterGain);
        alSourcef (mSource->mSource, AL_CONE_INNER_ANGLE,   mConeInnerAngle);
        alSourcef (mSource->mSource, AL_CONE_OUTER_ANGLE,   mConeOuterAngle);
        alSourcei (mSource->mSource, AL_SOURCE_RELATIVE,    mSourceRelative);

//std::cout << "....reset" << std::endl;
        return true;
    }
    //-------------------------------------------------------------------------
    String AudioWav::getParameters()
    {
        // TODO can we extract more info from the file?
        return String("Wav file");
    }
    //-------------------------------------------------------------------------


    //-------------------------------------------------------------------------
    AudioWavFactory::AudioWavFactory()
    {
        mExtensions.push_back("wav");
    }
    //-------------------------------------------------------------------------
    AudioWavFactory::~AudioWavFactory()
    {
    }
    //-------------------------------------------------------------------------
    AudioPtr AudioWavFactory::createAudio(const String& filename, bool streaming, 
        unsigned int nbBuffers)
    {
        AudioPtr audio = new AudioWav(this, filename);
        return audio;
    }
    //-------------------------------------------------------------------------
}
