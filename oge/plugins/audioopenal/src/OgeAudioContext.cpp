/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/audio/OgeAudioContext.h"
#include "oge/audio/OgeAudioManagerOpenAL.h"
#include "oge/audio/OgeAudioOpenAL.h"
#include "oge/logging/OgeLogManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    AudioContext::AudioContext(ALCcontext* context, const String& name)
        : mMaxSources(32), mSources(0), mContext(context), mName(name)
    {
        // The sources can only be created when the context is active!
        // NO createAllSources(); cannot be called here.
    }
    //-------------------------------------------------------------------------
    AudioContext::~AudioContext()
    {
        LOG("Destroy AudioContext...");
        mAudioSources.clear();

        delete[] mSources;
        mSources = 0;
        delete[] mAvailableSources;
        mAvailableSources = 0;
    }
    //-------------------------------------------------------------------------
    AudioPtr AudioContext::createAudio( const String& filename, 
        bool streaming, unsigned int nbBuffers )
    {
        AudioFactory* factory = 
            AudioManagerOpenAL::getSingletonPtr()->getAudioFactory( filename );
            
        if (factory == 0)
        {
            LOGW("No factory available for the audio file: "+filename);
            return 0;
        }

        AudioPtr audio = factory->createAudio( filename, streaming, nbBuffers );

        // TODO store audio for shutdown cleanup? or is the autoptr enough?

        return audio;
    }
    //-----------------------------------------------------------------------------
    bool AudioContext::createAllSources()
    {
        if (mSources)
            return true; // already created

        // TODO determine the max nb of safely created sources for this platform
        // mMaxSources = getNbSafelyCreatedSources();

        // Generate the sources
        mSources = new ALuint[mMaxSources];
        mAvailableSources = new bool[mMaxSources];

        alGetError();
        alGenSources(mMaxSources, mSources);
        if(alGetError() != AL_NO_ERROR || mSources == AL_NONE)
        {
            LOGE("Couldn't create "+StringUtil::toString(mMaxSources)+" sources!");
            delete[] mSources;
            mSources = 0;
            delete[] mAvailableSources;
            mAvailableSources = 0;
            return false;
        }

        if (mSources[0] == 0)
            LOGE("No sources genereated: Are you sure the Context is active?");

        for (unsigned int i=0;i<mMaxSources; i++)
        {
            AudioSourcePtr audioSource = new AudioSource();
            audioSource->mSource = mSources[i];
            mAudioSources.push_back( audioSource );

            mAvailableSources[i] = true;
        }
        return true;
    }
    //-----------------------------------------------------------------------------
    AudioSourcePtr AudioContext::getNextFreeSource(Audio* audio)
    {
//std::cout << "gnfs .... " << std::endl;
//std::cout << "getNextFreeSource mAudio: " << audio << std::endl;
        RWMutex::ScopedLock lock(mMutex, true);
//std::cout << " .... gnfs 1 ...." << std::endl;
        for (unsigned int i=0; i<mMaxSources; i++)
        {
//std::cout << ".... gnfs 2 .... " << i << std::endl;
            if (mAvailableSources[i] == true)
            {
//std::cout << ".... gnfs 3 .... " << std::endl;
                mAvailableSources[i] = false;
//std::cout << "getNextFreeSource mAudio 2: " << audio << std::endl;
                mAudioSources[i]->mAudio = audio;
//std::cout << ".... gnfs" << std::endl;
                return mAudioSources[i];
            }
        }
//std::cout << "NO SOURCES AVAILABLE" << std::endl;
        LOGW("No free audio sources available: consider having more sources.");
        return 0;
    }
    //-----------------------------------------------------------------------------
    void AudioContext::update()
    {
        if (mSources == 0)
            return;
//std::cout << "update .... " << std::endl;
//#pragma OGE_WARN("FIXME: If sound are generate to fast... we have mAudio being invalid AND inactivateSource fails also!!")

// TODO TEMP THE AUDIOS active should be updated -> position, etc & state !!!
        // AL_SOURCE_STATE = AL_INITIAL, AL_PLAYING, AL_PAUSED, AL_STOPPED
        // Test if the sources are in state: AL_STOPPED
        // TODO NOTE This shouldn't be called at each cycle (each seconde should be enough)
        RWMutex::ScopedLock lock(mMutex, true);
        for (unsigned int i=0; i<mMaxSources; i++)
        {
// TODO If the sounds are generated too fast you can here sometimes a small delay. 
// Why? Is this normal?
// If you remove the next test you will be able to use only the available sources (32)
// then you have no sound BUT you here that there is no delay!
// Perhaps the mAudioPtr is destroyed too slowly hence no 
            if (mAvailableSources[i] == false && !mAudioSources[i]->mAudio)
                mAvailableSources[i] = true;

            if (mAvailableSources[i] == false &&
                mAudioSources[i]->mAudio) // TODO ARGH this shouldn't be necessary!
            {
//std::cout << "... update 2.... " << i << std::endl;
                // Mostly this is used by streaming audios
                mAudioSources[i]->mAudio->update();

                int state = 0;
                alGetSourcei(mSources[i], AL_SOURCE_STATE, &state);
                if (state == AL_STOPPED)  // == 4116
                {
                    LOGV("OpenAL source "+StringUtil::toString(mSources[i])+
                        " stopped -> inactivate in Audio: "+StringUtil::toString(i));
                    // Callback telling an AudioSource that it is not active anymore
//std::cout << "... callback .... " << std::endl;
                    mAvailableSources[i] = true;
                    ((AudioOpenAL*) mAudioSources[i]->mAudio)->inactivateSource();
                    mAudioSources[i]->mAudio = 0;
//std::cout << "... callback done .... " << std::endl;
                }
            }
        }
//std::cout << "....updated" << std::endl;

    }
    //-----------------------------------------------------------------------------
}
