/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/audio/OgeAudioPrerequisitesOpenAL.h"
#include "oge/audio/OgeAudioCapabilities.h"

namespace oge
{
    ////-------------------------------------------------------------------------
    //AudioCapabilities::AudioCapabilities()
    //{
    //    fillDefaults();
    //}
    ////-------------------------------------------------------------------------
    //AudioCapabilities::~AudioCapabilities()
    //{

    //}
    ////-------------------------------------------------------------------------
    //bool AudioCapabilities::fillCapabilities()
    //{
    //    fillDefaults();

    //    if (!fillOutputDeviceNames())
    //        return false;

    //    fillEAXSupport();

    //    return true;
    //}
    ////-------------------------------------------------------------------------
    //size_t AudioCapabilities::getOutputDeviceCount() const
    //{
    //    return mOutputDeviceNames.size();
    //}
    ////-------------------------------------------------------------------------
    //String AudioCapabilities::getDefaultOutputDeviceName() const
    //{
    //    return mDefaultOutputDeviceName;
    //}
    ////-------------------------------------------------------------------------
    //String AudioCapabilities::getOutputDeviceName(size_t index) const
    //{
    //    if (index >= mOutputDeviceNames.size())
    //        return StringUtil::BLANK;

    //    return mOutputDeviceNames.at(index);
    //}
    ////-------------------------------------------------------------------------
    //bool AudioCapabilities::fillOutputDeviceNames()
    //{
    //    alGetError();
    //    if (alcIsExtensionPresent(NULL, "ALC_ENUMERATION_EXT") == AL_FALSE)
    //        return false;

    //    const ALchar* curName = alcGetString(NULL, ALC_DEVICE_SPECIFIER);
    //    if (strlen(curName) == 0)
    //        return false;

    //    int count = 0;
    //    while (curName && *curName)
    //    {
    //        alGetError();
       //     ALCdevice* device = alcOpenDevice(curName);
       //     if (device)
       //     {
          //      alcCloseDevice(device);
    //            mOutputDeviceNames.push_back(String(curName));

    //            if (count == 0)
    //            {
    //                mDefaultOutputDeviceName = String(curName);
    //            }
       //     }

       //     curName += (strlen(curName) + 1);
    //        ++count;
    //    }

    //    return true;
    //}
    ////-------------------------------------------------------------------------
    //void AudioCapabilities::fillEAXSupport()
    //{
    //    alGetError();
    //    mEAX2 = alIsExtensionPresent("EAX2.0") == AL_TRUE;
    //}
    ////-------------------------------------------------------------------------
    //void AudioCapabilities::fillDefaults()
    //{
    //    mOutputDeviceNames.clear();
    //    mDefaultOutputDeviceName = "";
    //    mEAX2 = false;
    //}
    ////-------------------------------------------------------------------------
}
