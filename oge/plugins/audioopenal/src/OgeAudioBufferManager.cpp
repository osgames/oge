/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/audio/OgeAudioBufferManager.h"
#include "oge/audio/OgeAudio.h"
#include "oge/logging/OgeLogManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    template<> AudioBufferManager* Singleton<AudioBufferManager>::mSingleton = 0;
    //-------------------------------------------------------------------------
    AudioBufferManager::AudioBufferManager() :
        mMaxBuffers( 5000 ), mBuffersInUse(0), mBuffersStored(0)
    {
        mSingleton = this;
    }
    //-------------------------------------------------------------------------
    AudioBufferManager::~AudioBufferManager()
    {
        deleteBuffers();
    }
    //-------------------------------------------------------------------------
    void AudioBufferManager::deleteBuffers()
    {
        // The SharedPtr<AudioBuffer> will take care of destroying the buffer
        // TODO If the AudioBuffer were not all released (should we force it here?)
        mBuffers.clear();
    }
    //-------------------------------------------------------------------------
    AudioBufferManager* AudioBufferManager::createSingleton()
    {
        if (!mSingleton)
            new AudioBufferManager();
        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void AudioBufferManager::destroySingleton()
    {
        if (mSingleton)
            delete mSingleton;
    }
    //-------------------------------------------------------------------------
    AudioBufferPtr AudioBufferManager::getBuffer( const String& filename,
        unsigned int nbBuffers )
    {
        // Note that the buffer must have the same nb of openal buffer
        // hence we find a buffer stored with a composite name :)
        String name = filename + StringUtil::toString(nbBuffers);
        AudioBufferPtr buffer = mBuffers[name];
        if (buffer)
            return buffer;
        return 0; // don't call createBuffer here only an Audio::load() should do it.
    }
    //-------------------------------------------------------------------------
    AudioBufferPtr AudioBufferManager::createBuffer( const String& filename,
        unsigned int nbBuffers )
    {
        Mutex::ScopedLock lock(mBufferManagerMutex);

        // We didn't put a lock at getBuffer() hence we could still enter twice this method
        String name = filename + StringUtil::toString(nbBuffers);
        AudioBufferPtr buffer = mBuffers[name];
        if (buffer)
            return buffer;
        
        // TODO update when moved to Poco 1.3.3
        // TODO put into a method that is optional?
        // Test if we don't reach our limit of buffers 
        // if we do we remove (if we can) the oldest non-used ones
        //if ( mMaxBuffers < (mBuffersInUse + nbBuffers))
        //{
        //    LOGE("Not enough buffer(s) available")
        //    return 0;
        //}
        //
        //// If stored buffers+nbBuffers > max buffers we must delete the oldest unused buffers
        //if ( mMaxBuffers < (mBuffersStored + nbBuffers))
        //{
        //    AudioBufferList::iterator iter = mBuffers.begin();
        //    for (iter; iter != mBuffers.end(); ++iter)
        //    {
        //        // If nobody is using it except mBuffers !
        //        if ((*iter)->referenceCount() <= 1)
        //        {
        //            mBuffersStored -= (*iter)->getNbBuffers();
        //            LOGEC(mBuffersStored < mBuffersInUse, "Oups... this shouldn't be possible!")
        //
        //            mBuffers.erase( iter );
        //
        //            // We break only if we deleted enough OpenAL buffers
        //            if (mMaxBuffers >= (mBuffersStored + nbBuffers))
        //                break;
        //        }
        //    }
        //
        //    LOGEC((mMaxBuffers < (mBuffersStored + nbBuffers)), "Oups... We couldn't release enough buffers this shouldn't be possible!")
        //}

        // Create the buffers
        buffer = new AudioBuffer(nbBuffers);
        if (buffer.isNull() || (buffer->getBuffer() == AL_NONE))
        {
            LOGW("Error: no new buffers could be created")
            return 0;
        }

        mBuffers[name] = buffer;

        // TODO Part of the "method" for reusing buffers see above
        //mBuffersStored += nbBuffers;
        //mBuffersInUse += nbBuffers;
        return buffer;
    }
    //-------------------------------------------------------------------------
}
