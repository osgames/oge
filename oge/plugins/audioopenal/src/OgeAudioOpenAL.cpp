/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/audio/OgeAudioOpenAL.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/math/OgeVector3.h"

namespace oge
{
    //-------------------------------------------------------------------------
    AudioOpenAL::AudioOpenAL(AudioFactory* creator, const String& filename) :
        mSource(0), mCreator(creator), mFilename(filename), mRestart(false),
        mPosition(Vector3::ZERO), mVelocity(Vector3::ZERO), 
        mDirection(Vector3::UNIT_Z), mLoop(AL_FALSE), mPitch(1.0), mGain(1.0),
        mMaxDistance(3000.0), mRolloffFactor(1.0), mReferenceDistance(150.0),
        mMaxGain(1.0), mMinGain(0.0), mConeOuterGain(0),
        mConeInnerAngle(360.0), mConeOuterAngle(360.0), 
        mSourceRelative(AL_FALSE)
    {
    }
    //-------------------------------------------------------------------------
    AudioOpenAL::~AudioOpenAL()
    {
        // Needed because if the Audio is hold by an autoPtr it will be deleted
        // at any moment
        if (mSource)
            mSource->mAudio = 0;
    }
    //-------------------------------------------------------------------------
    void AudioOpenAL::play()
    {
        // This call is obligatory as the source mgr (Context) can set the 
        // mSource to zero to reuse a source!
        if (!reset())
            return;

        // If already playing we can't play unless we want to restart
        if (isPlaying())
        {
            if (mRestart)
                stop();
           else
                return;
        }

        alSourcePlay(mSource->mSource);
    }
    //-------------------------------------------------------------------------
    void AudioOpenAL::pause()
    {
        alGetError(); // clear alerrors
        alSourcePause( mSource->mSource );

        //if ( AudioManager::getSingletonPtr()->checkALError( "pauseAudio::alSourceStop ") )
        //    return false;
        //return true;
    }
    //-------------------------------------------------------------------------
    void AudioOpenAL::stop()
    {
        alGetError(); // clear alerrors
        alSourceStop( mSource->mSource );

        //if ( AudioManager::getSingletonPtr()->checkALError( "stopAudio::alSourceStop ") )
        //    return false;
        //return true;
    }
    //-------------------------------------------------------------------------
    bool AudioOpenAL::isPlaying()
    {
        ALenum state;
        alGetSourcei(mSource->mSource, AL_SOURCE_STATE, &state);
        return (state == AL_PLAYING);
    }
    //-------------------------------------------------------------------------
    void AudioOpenAL::setAudio( Vector3 position, Vector3 velocity,
        Vector3 direction, float maxDistance, float referenceDistance,
        float minGain, float maxGain, float rolloff )
    {
        mPosition = position;
        alSourcefv( mSource->mSource, AL_POSITION, (const ALfloat *)position.ptr() );
        LOGWC(checkALerror(), "alSourcefv:AL_POSITION " + mFilename )

        mVelocity = velocity;
        alSourcefv( mSource->mSource, AL_VELOCITY, (const ALfloat *)velocity.ptr() );
        LOGWC(checkALerror(), "alSourcefv:AL_VELOCITY " + mFilename )

        mDirection = direction;
        alSourcefv( mSource->mSource, AL_DIRECTION, (const ALfloat *)direction.ptr() );
        LOGWC(checkALerror(), "alSourcefv:AL_DIRECTION " + mFilename )

        mMaxDistance = maxDistance;
        alSourcef (mSource->mSource, AL_MAX_DISTANCE, maxDistance );
        mReferenceDistance = referenceDistance;
        alSourcef( mSource->mSource, AL_REFERENCE_DISTANCE, referenceDistance );

        /// Set the MIN_GAIN ( IMPORTANT - if not set, nothing should be audible! )
        mMinGain = minGain;
        alSourcef( mSource->mSource, AL_MIN_GAIN, minGain );
        mMaxGain = maxGain;
        alSourcef( mSource->mSource, AL_MAX_GAIN, maxGain );
        mRolloffFactor = rolloff;
        alSourcef( mSource->mSource, AL_ROLLOFF_FACTOR, rolloff );
    }
    //-------------------------------------------------------------------------
    void AudioOpenAL::setLocation( Vector3 position, Vector3 velocity,
        Vector3 direction )
    {
        mPosition = position;
        alSourcefv( mSource->mSource, AL_POSITION, (const ALfloat *)position.ptr() );
        LOGWC(checkALerror(), "alSourcefv:AL_POSITION " + mFilename )

        mVelocity = velocity;
        alSourcefv( mSource->mSource, AL_VELOCITY, (const ALfloat *)velocity.ptr() );
        LOGWC(checkALerror(), "alSourcefv:AL_VELOCITY " + mFilename )

        mDirection = direction;
        alSourcefv( mSource->mSource, AL_DIRECTION, (const ALfloat *)direction.ptr() );
        LOGWC(checkALerror(), "alSourcefv:AL_DIRECTION " + mFilename )
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setPosition( Vector3 position )
    {
        mPosition = position;
        alSourcefv( mSource->mSource, AL_POSITION, (const ALfloat *)position.ptr() );
        LOGWC(checkALerror(), "alSourcefv:AL_POSITION " + mFilename )
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setPitch( Real pitch )
    {
        if (pitch <=0) return;
        mPitch = pitch;
        if (mSource->mSource != AL_NONE)
            alSourcef(mSource->mSource,AL_PITCH, mPitch);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setGain( Real gain )
    {
        if (gain <=0) return;
        mGain = gain;
        if (mSource->mSource != AL_NONE)
            alSourcef(mSource->mSource,AL_GAIN, mGain);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setMaxDistance( Real maxDistance )
    {
        if (maxDistance < 0) return;
        mMaxDistance = maxDistance;
        if (mSource->mSource != AL_NONE)
            alSourcef(mSource->mSource,AL_MAX_DISTANCE, mMaxDistance);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setRolloffFactor( Real rolloffFactor )
    {
        if (rolloffFactor <=0) return;
        mRolloffFactor = rolloffFactor;
        if (mSource->mSource != AL_NONE)
            alSourcef(mSource->mSource,AL_ROLLOFF_FACTOR, mRolloffFactor);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setReferenceDistance( Real refDistance )
    {
        if (refDistance<0) return;
        mReferenceDistance = refDistance;
        if (mSource->mSource != AL_NONE)
            alSourcef(mSource->mSource, AL_REFERENCE_DISTANCE, mReferenceDistance);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setMaxGain( Real max )
    {
        if (max < 0 || max > 1) return;
        mMaxGain = max;
        if (mSource->mSource != AL_NONE)
            alSourcef(mSource->mSource,AL_MAX_GAIN, mMaxGain);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setMinGain( Real min )
    {
        if (min < 0 || min > 1) return;
        mMinGain = min;
        if (mSource->mSource != AL_NONE)
            alSourcef(mSource->mSource,AL_MIN_GAIN, mMinGain);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setDistanceValues( Real maxDistance, Real rolloffFactor, Real refDistance )
    {
        setMaxDistance(maxDistance);
        setRolloffFactor(rolloffFactor);
        setReferenceDistance(refDistance);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setConeOuterGain( Real coneOuterGain )
    {
        if (coneOuterGain < 0 || coneOuterGain > 1) return;
        mConeOuterGain = coneOuterGain;
        if (mSource->mSource != AL_NONE)
            alSourcef(mSource->mSource, AL_CONE_OUTER_GAIN, mConeOuterGain);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setConeInnerAngle( Real coneInnerAngle )
    {
        if (coneInnerAngle < 0 || coneInnerAngle > 1) return;
        mConeInnerAngle = coneInnerAngle;
        if (mSource->mSource != AL_NONE)
            alSourcef(mSource->mSource, AL_CONE_INNER_ANGLE, mConeInnerAngle);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setConeOuterAngle( Real coneOuterAngle )
    {
        if (coneOuterAngle < 0 || coneOuterAngle > 1) return;
        mConeOuterAngle = coneOuterAngle;
        if (mSource->mSource != AL_NONE)
            alSourcef(mSource->mSource, AL_CONE_OUTER_ANGLE, mConeOuterAngle);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setRelativeToListener( bool relative )
    {
        mSourceRelative = relative;
        if (mSource->mSource != AL_NONE)
            alSourcei(mSource->mSource, AL_SOURCE_RELATIVE, mSourceRelative);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setLoop( bool loop )
    {
        mLoop = loop ? AL_TRUE : AL_FALSE;
        if (mSource->mSource != AL_NONE)
            alSourcei(mSource->mSource, AL_LOOPING, mLoop);
    }
    //-----------------------------------------------------------------------------
    void AudioOpenAL::setSecondOffset( Real offset )
    {
        if (offset < 0) return;
        if (mSource->mSource != AL_NONE)
            alSourcef(mSource->mSource, AL_SEC_OFFSET, offset);
    }
    //-----------------------------------------------------------------------------
    Real AudioOpenAL::getSecondOffset()
    {
        Real offset = 0;
        if (mSource->mSource != AL_NONE)
            alGetSourcef(mSource->mSource, AL_SEC_OFFSET, (ALfloat *)&offset);
        return offset;
    }
    //-----------------------------------------------------------------------------
}
