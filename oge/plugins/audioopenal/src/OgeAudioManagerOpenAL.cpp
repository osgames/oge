/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/audio/OgeAudioManagerOpenAL.h"
#include "oge/audio/OgeAudioListener.h"
#include "oge/resource/OgeResourceRegistry.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/audio/OgeAudio.h"
#include "oge/audio/codecs/OgeAudioWav.h"
#include "oge/audio/codecs/OgeAudioOgg.h"
#include "oge/OgeProfiler.h"

namespace oge
{
    //-------------------------------------------------------------------------
    template<> AudioManagerOpenAL* Singleton<AudioManagerOpenAL>::mSingleton = 0;
    //-------------------------------------------------------------------------
    AudioManagerOpenAL::AudioManagerOpenAL() : 
        AudioManager("AudioManager"), // Not AudioManagerOpenAL as we -for now- want only one mgr for each type
        mDevice(0), mContext(0),
        //mResourceController(0), mAudioBufferManager(0),
        mIsInitialised(false),
        mOggExtensionPresent(false), mIsEAXPresent(false)
    {

    }
    //-------------------------------------------------------------------------
    AudioManagerOpenAL::~AudioManagerOpenAL()
    {
        shutdownOpenAL();
    }
    //-------------------------------------------------------------------------
    bool AudioManagerOpenAL::initialise()
    {
        LOG("AudioManagerOpenAL initialising...");

        if (!setupResourceManagement())
            return false;

        registerInBuildCodec();

        // Create the default context (more can be created)
        // NEXT Use parameter even if linux will detect "DirectSound3D" and use a NULL device
        if (!setupAudioDevice("DirectSound3D"))
        {
            LOGE("The audio device couldn't be created.");
            return false;
        }

        // Create default context
        AudioContext* context = createAudioContext("default");
        if (context == 0)
        {
            LOGE("The audio context couldn't be created.");
            return false;
        }

        mIsInitialised = true; // here or the context and device might not be deleted

        // Set the default context active
        if (!setAudioContextActive(context->getName()))
        {
            LOGE("This audio context couldn't be activated: "+context->getName());
            return false;
        }

        if (!System::initialise())
        {
            LOGE("The AudioManager system couldn't be initialised");
            return false;
        }

        LOG("AudioManager OpenAL was initialised.");
        return true;
    }
    //-------------------------------------------------------------------------
    void AudioManagerOpenAL::shutdown()
    {
        LOG("AudioManagerOpenAL shutdown...");
        System::shutdown();

        // Delete the factories
        deleteAllCodecs();

        shutdownResourceManagement();

        if (!shutdownOpenAL())
        {
            LOGE("Error on shutting down OpenAL.");
        }
        LOG("AudioManagerOpenAL shutdown done.");
    }
    //-------------------------------------------------------------------------
	void AudioManagerOpenAL::postTick(double currentTime)
	{
		// do nothing
	}
    //-------------------------------------------------------------------------
	void AudioManagerOpenAL::preTick(double currentTime)
	{
		// do nothing
	}
    //-------------------------------------------------------------------------
    void AudioManagerOpenAL::tick(double currentTime)
    {
		OgeProfileGroup("Audio OpenAL", OGEPROF_SYSTEMS);
        mContext->update();
    }
    //-------------------------------------------------------------------------
    bool AudioManagerOpenAL::setupAudioDevice(const String& deviceName)
    {
        if (mDevice != 0)
        {
            // TODO (lazalong) AFAIK you can't simple recreate the audio device
            //        you need to recreate all : listener, buffer, sounds, etc.
            //  alGetError();
            //  alcCloseDevice(mDevice);
            return true; // Exists already hence we continue with initialisation
        }

        alGetError();
        if (deviceName == "DirectSound3D")
        {
        #if OGE_PLATFORM == OGE_PLATFORM_LINUX
            LOGE("We are trying to open a DirectSound3D audio device on linux !?");
            LOG("Opening a DirectSound3D audio device");
            mDevice = alcOpenDevice(NULL);
        #else
            LOG("Opening a DirectSound3D audio device");
#ifdef TMP_OPENALSOFT 
            mDevice = alcOpenDevice( "DirectSound Software" );
#else
            mDevice = alcOpenDevice( "DirectSound" );
#endif
        #endif
        }
        else if (deviceName == "NULL")
        {
            LOG("Opening a NULL audio device");
            mDevice = alcOpenDevice(NULL);
        }
        else
        {
            LOGE("The audio context name doesn't exist: '"+deviceName
                + "' - Opening a NULL device");
            mDevice = alcOpenDevice(NULL);
        }

        checkALCError(mDevice, "Error with alcOpenDevice(): '"+deviceName+"'");

        // If this didn't work we will try to create a default device
        if (!mDevice)
            mDevice = alcOpenDevice(NULL);

       if (checkALCError(mDevice, "Error with alcOpenDevice()") || !mDevice)
           return false;

        return true;
    }
    //-------------------------------------------------------------------------
    bool AudioManagerOpenAL::shutdownOpenAL()
    {
        LOG("AudioManager shutting down OpenAL...");

        if (!mIsInitialised)
        {
            LOGW("OpenAL has not been initialised.");
            return false;
        }

        //if (mDevice == 0 && mCurrentAudioContext == 0)
        //{
        //    LOGW("OpenAL has not been initialised.");
        //    return false;
        //}

        if (!sanityCheck())
            return false;

        if (!alcMakeContextCurrent(NULL))
        {
            LOGE("Could not release the current ALC context.");
            return false;
        }

        // Get the device... to close later.
        // Note mDevice and device should be the same
        //ALCdevice* device = alcGetContextsDevice (mCurrentAudioContext->mContext);
        ALCdevice* device = mDevice;

        // Destroy all contexts
        AudioContextMap::Iterator iter;
        for (iter = mContexts.begin(); iter!=mContexts.end(); iter = mContexts.begin() )
        {
            String name = iter->first;
            mContexts.deleteEntry(iter);

            if (alcGetError (device) != ALC_NO_ERROR)
            {
                LOGE( String("Error destroying an ALC context named :")+name);
                //return false;
            }
        }

        if (device && !alcCloseDevice (device))
        {
            LOGE( "Error closing the ALC device created by _initialise().");
            return false;
        }

        mIsInitialised = false;
        return true;
    }
    //-----------------------------------------------------------------------
    bool AudioManagerOpenAL::setupResourceManagement()
    {
// TODO Chris : AudioManagerOpenAL::setupResourceManagement()
        //mResourceController = new ResourceControllerOgeImpl("Audio");

        //if (!ResourceRegistry::getSingleton().registerController(
        //    mResourceController))
        //{
        //    LOGE("Couldn't register audio controller.");
        //    delete mResourceController;
        //    mResourceController = 0;
        //    return false;
        //}

        //mAudioBufferManager = new AudioBufferManager();

        //if (!mResourceController->registerManager("Audio", mAudioBufferManager))
        //{
        //    LOGE("Couldn't register the audio buffer manager.");
        //    delete mAudioBufferManager;
        //    mAudioBufferManager = 0;
        //    ResourceRegistry::getSingleton().unregisterController( 
        //        mResourceController->getName());
        //    delete mResourceController;
        //    mResourceController = 0;
        //    return false;
        //}
    
        return true;
    }
    //-----------------------------------------------------------------------
    void AudioManagerOpenAL::shutdownResourceManagement()
    {
        //ResourceRegistry::getSingleton().unregisterController(
        //    mResourceController->getName());

        //delete mAudioBufferManager;
        //delete mResourceController;
    }
    //-------------------------------------------------------------------------
    AudioManagerOpenAL* AudioManagerOpenAL::createSingleton()
    {
        if (!mSingleton)
            mSingleton = new AudioManagerOpenAL();
        
        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void AudioManagerOpenAL::destroySingleton()
    {
        if (mSingleton)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //-------------------------------------------------------------------------
    bool AudioManagerOpenAL::checkALError( const String& message )
    {
        ALenum error = 0;

        if ( (error = alGetError()) == AL_NO_ERROR )
            return false;

        // NEXT If log level is not <= OGE_LOG_CRITICAL we should skip the next part
        String str;
        switch ( error )
        {
            case AL_INVALID_NAME:
                str = "ERROR AudioManager:: Invalid Name : ";
                break;
            case AL_INVALID_ENUM:
                str = "ERROR AudioManager:: Invalid Enum : ";
                break;
            case AL_INVALID_VALUE:
                str = "ERROR AudioManager:: Invalid Value : ";
                break;
            case AL_INVALID_OPERATION:
                str = "ERROR AudioManager:: Invalid Operation : ";
                break;
            case AL_OUT_OF_MEMORY:
                str = "ERROR AudioManager:: Out Of Memory :";
                break;
            default:
                str = "ERROR AudioManager:: Unknown error (%i) case in testALError()";
                str += error;
                str += " ";
                break;
        };

        LOGW( str + message );
        return true;
    }
    //-------------------------------------------------------------------------
    bool AudioManagerOpenAL::checkALCError(ALCdevice* device, const String& message )
    {
        ALCenum error = alcGetError(device);

        if (error == ALC_NO_ERROR && device != 0)
            return false;

        if (device == 0)
        {
            LOGE(message + ": Invalid device");
        }
        else
        {
            LOGE(message + ": " + alcGetString(device, error));
        }

        return true;
    }
    //-------------------------------------------------------------------------
    bool AudioManagerOpenAL::sanityCheck()
    {
        ALCcontext *context;

        if (!mIsInitialised)
        {
            LOGE( "Not initialised" );
            return false;
        }

        context = alcGetCurrentContext();
        if (context == NULL)
        {
            LOGE( "No current context" );
            return false;
        }

        if (alGetError () != AL_NO_ERROR)
        {
            LOGE( "AL error on entry" );
            return false;
        }

        if (alcGetError (alcGetContextsDevice (context)) != ALC_NO_ERROR)
        {
            LOGE( "ALC error on entry" );
            return false;
        }

        return true;
    }
    //-------------------------------------------------------------------------
    const String AudioManagerOpenAL::getLibrariesVersion()
    {
        String version("Audio: OpenAl ");
        // See altest.c in openal_svn_sep06\contrib\tests\altest
        ALint major, minor;

        if (mDevice)
        {
            alcGetIntegerv (mDevice, ALC_MAJOR_VERSION, sizeof(int), &major);
            alcGetIntegerv (mDevice, ALC_MINOR_VERSION, sizeof(int), &minor);
        }
        else
        {
            alcGetIntegerv (0, ALC_MAJOR_VERSION, sizeof(int), &major);
            alcGetIntegerv (0, ALC_MINOR_VERSION, sizeof(int), &minor);
        }
        version += StringUtil::toString((int)major);
        version += ".";
        version += StringUtil::toString((int)minor);
        version += " \n";
        return version;
    }
    //-------------------------------------------------------------------------
    String AudioManagerOpenAL::listAvailableDevices()
    {
// TODO REWORK needed: the string returned has the names separated by NULL !
//      We need to parse it and create a  "'xxx', 'yyyyy', 'zzzz'" string.
        String str = "Sound Devices available : ";
        if ( alcIsExtensionPresent( NULL, "ALC_ENUMERATION_EXT" ) == AL_TRUE )
        {
            str = "List of Devices : ";
            str += (char*) alcGetString( NULL, ALC_DEVICE_SPECIFIER );
            str += "\n";
        }
        else
            str += " ... eunmeration error.\n";
        return str;
    }
    //-------------------------------------------------------------------------
    bool AudioManagerOpenAL::isEAXAndExtensionPresent()
    {
        if (alIsExtensionPresent( "EAX2.0" ) != AL_TRUE)
        {
            LOG( "EAX 2.0 Extension unavailable\n" );
            mIsEAXPresent = false;
            return false;
        }
        else
        {
            #ifdef _USEEAX
                LOG( "EAX 2.0 Extension available\n" );
                eaxSet = (EAXSet) alGetProcAddress( "EAXSet" );
                if ( eaxSet == NULL )
                    mIsEAXPresent = false;

                eaxGet = (EAXGet) alGetProcAddress( "EAXGet" );
                if ( eaxGet == NULL )
                    mIsEAXPresent = false;

                if ( !mIsEAXPresent )
                {
                    checkALError( "Failed to get the EAX extension functions adresses.\n" );
                    LOGW( "Failed to get the EAX extension functions adresses.\n" );
                }
            #else
                mIsEAXPresent = false;
                LOGW( "EAX 2.0 Extension available ... but not used: _USEEAX not set.\n" );
            #endif // _USEEAX
        }
        return mIsEAXPresent;
    }
    //-------------------------------------------------------------------------
    bool AudioManagerOpenAL::isOggExtensionPresent()
    {
        if ( alIsExtensionPresent( "AL_EXT_vorbis" ) != AL_TRUE )
        {
            LOGW( "Ogg Vorbis extension not availablee!" );
            mOggExtensionPresent = false;
            return false;
      }
      return true;
    }
    //-------------------------------------------------------------------------
    AudioContext* AudioManagerOpenAL::createAudioContext( const String& contextName )
    {
        if (mDevice == 0L)
        {
            LOGE("The sound device wasn't created... can't create a context.");
            return 0L;
        }
        if (mContexts.entryExists(contextName))
        {
            LOGW("A context of this name already exist.");
            return mContexts.getEntry(contextName);
        }

        /// NEXT: The second param is a list of attributes
        /// See http://opensource.creative.com/pipermail/openal/2006-February/009337.html
        // ALCint attribs[] = {ALC_STEREO_SOURCES, 128, 0};
        // ALCcontext* context = alcCreateContext(mAudioDevice, &attribs[0] );
        ALCcontext* context = alcCreateContext(mDevice, NULL );

        if (!context && alGetError() == ALC_INVALID_VALUE)
        {
            LOGE( "No more context can be done on this device." );
            return 0L;
        }

        if (!context)
        {
            LOGE(String("Unable to create an audio context - named: '")+contextName+"'");
            return 0L;
        }

        AudioContext* audioContext = new AudioContext( context, contextName );

        mContexts.addEntry( contextName, audioContext );

        return audioContext;
    }
    //-----------------------------------------------------------------------------
    bool AudioManagerOpenAL::setAudioContextActive( const String& contextName )
    {
        if (mContext && (mContext->getName() == contextName) )
            return true; // Already selected!

        if (!mContexts.entryExists( contextName ))
            return false;

        if (mContext)
        {
            // Stop all audio from the actual context
            alcSuspendContext(mContext->getContext()); // TODO not sure should it be stopALL()
        }

        AudioContext* context = mContexts.getEntry( contextName );

        // Make the context current and active
        alcMakeContextCurrent( context->getContext() );
        alGetError();
        if ( alGetError() == ALC_INVALID_CONTEXT )
        {
            LOGE("The audio context '"+contextName+"' is invalid");

            // Tries to reactivate the previous one.
            if (mContext)
            {
                // TODO ? alcMakeContextCurrent( mContext->getContext() );
                alcProcessContext( mContext->getContext());
            }
            return false;
        }
        else
        {
            LOG("Setting the audio context '"+contextName+"' active.");
        }

        // The sources can only be created when the context is active!
        context->createAllSources();

        mContext = context;

        return true;
    }
    //-----------------------------------------------------------------------------
    void AudioManagerOpenAL::registerInBuildCodec()
    {
        registerCodec( new AudioWavFactory() );
        registerCodec( new AudioOggFactory() );
    }
    //-----------------------------------------------------------------------------
    void AudioManagerOpenAL::registerCodec(AudioFactory* factory)
    {
        if (factory == 0)
            return;

        // Register the factory for all the extensions recoginised by the factory
        std::vector<String> extensions = factory->getExtensions();
        for (unsigned int i=0; i<extensions.size(); i++)
        {
            mAudioFactories[extensions[i]] = factory;
        }
    }
    //-----------------------------------------------------------------------------
    AudioPtr AudioManagerOpenAL::createAudio( const String& filename, 
        bool streaming, unsigned int nbBuffers )
    {
        return mContext->createAudio( filename, streaming, nbBuffers );
    }
    //-----------------------------------------------------------------------------
    AudioFactory* AudioManagerOpenAL::getAudioFactory( const String& filename ) 
    {
        Path file = filename;
        String extension = file.getExtension();
        AudioFactoryMap::iterator iter = 
            mAudioFactories.find(extension);

        if (iter == mAudioFactories.end())
        {
            LOGE("Couldn't find a factory for the audio: '"+filename+"'");
            return 0;
        }
        return mAudioFactories[extension];
    }
    //-----------------------------------------------------------------------------
    void AudioManagerOpenAL::deleteAllCodecs()
    {
        AudioFactoryMap::iterator iter;
        for (iter = mAudioFactories.begin(); iter != mAudioFactories.end(); iter++)
        {
            delete (*iter).second;
        }
        mAudioFactories.clear();
    }
    //-----------------------------------------------------------------------------
}
