/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_AUDIOOPENAL_H__
#define __OGE_AUDIOOPENAL_H__

#include "oge/audio/OgeAudioPrerequisitesOpenAL.h"
#include "oge/audio/OgeAudio.h"
#include "oge/datastreams/OgeDataStream.h"
#include "oge/audio/OgeAudioSource.h"
#include "oge/math/OgeVector3.h"

namespace oge
{
    /**
     * Class linked to an OpenAL source & one or more buffers.
     */
    class OGE_AUDIO_API AudioOpenAL : public Audio
    {
    protected:
        AudioSourcePtr mSource;
        AudioFactory* mCreator;
        const String mFilename;
        bool mRestart;

        // See page 13 of the openal 1.1 doc pdf
        Vector3 mPosition;
        Vector3 mVelocity;
        Vector3 mDirection;

        ALboolean mLoop;
        Real mPitch;
        Real mGain;
        Real mMaxDistance;
        Real mRolloffFactor;
        Real mReferenceDistance;
        Real mMaxGain;
        Real mMinGain;
        Real mConeOuterGain;
        Real mConeInnerAngle;
        Real mConeOuterAngle;
        ALboolean mSourceRelative;

    public:
        AudioOpenAL(AudioFactory* creator, const String& filename);
        virtual ~AudioOpenAL();

        /// Returns the factory having created this audio
        AudioFactory* getCreator() const { return mCreator; }

        /** 
         * Relase the AudioSource - this method is used as callback from
         * the context which is testing which sources are AL_STOPPED and
         * in this case will deactivate it.
         *
         * @note be careful in using this method be sure to understand its purpose
         * @note Consider making it protected
         */
        inline void inactivateSource() { mSource = 0; }

        /**
         * Must be called it will prepare the audio.
         *  That is it will set the AL source
         *  and set the source position, velocity, etc as zero.
         */
        virtual bool reset() = 0;
        virtual bool load() = 0;

        /**
         * Will play an audio.
         * In case it is already playing and the restart wasn't forced 
         * then it will simply return.
         * If the restart is set for this audio the playing audio will
         * first be stopped and the audio played from start again.
         * @see setRestart()
         */
        virtual void play();
        virtual void pause();
        virtual void stop();

        /**
         * This method must usually be called only for streamed audios
         */
        virtual void update() {}

        /// TODO make this a full debug method : switch AL_OUT_OF_MEMORY, etc.
        inline bool checkALerror() { return alGetError() != AL_NO_ERROR; }
        bool isPlaying();

        /// If true will force the audio to restart when play() is called
        void setRestart( bool restart ) { mRestart = restart; }

        virtual void setAudio( Vector3 position, Vector3 velocity, 
           Vector3 direction, float maxDistance, float referenceDistance,
           float minGain, float maxGain, float rolloff );
    
        virtual void setLocation( Vector3 position, Vector3 velocity,
            Vector3 direction );
        virtual void setPosition( Vector3 position );

        virtual void setPitch( Real pitch = 1.0 );
        virtual void setGain( Real gain = 1.0 );
        virtual void setMaxDistance( Real distance = 3000.0 );
        virtual void setRolloffFactor( Real rolloffFactor = 1.0 );
        virtual void setReferenceDistance( Real refDistance = 150.0 );
        virtual void setDistanceValues( Real maxDistance, Real rolloffFactor, Real refDistance );
        virtual void setMaxGain( Real max = 1.0 );
        virtual void setMinGain( Real min = 0.0 );
        virtual void setConeOuterGain( Real coneOuterGain = 0.0 );
        virtual void setConeInnerAngle( Real coneInnerAngle  = 360.0 );
        virtual void setConeOuterAngle( Real coneOuterAngle = 360.0 );
        virtual void setRelativeToListener( bool relative = false);
        virtual void setLoop( bool loop = false);
        virtual void setSecondOffset( Real offset = 0.0 );
        virtual Real getSecondOffset();

    };

    /**
     * Factory creating AudioOpenAL instances
     */
    class OGE_AUDIO_API AudioOpenALFactory : public AudioFactory
    {
    public:
        virtual AudioPtr createAudio(const String& filename, 
            bool streaming, unsigned int nbBuffers) = 0;
    };
}

#endif // __OGE_AUDIOOPENAL_H__
