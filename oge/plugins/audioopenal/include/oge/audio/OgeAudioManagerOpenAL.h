/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_AUDIOMANAGEROPENAL_H__
#define __OGE_AUDIOMANAGEROPENAL_H__

#include "oge/audio/OgeAudioPrerequisitesOpenAL.h"
#include "oge/resource/OgeResourceControllerOgeImpl.h"
#include "oge/audio/OgeAudioManager.h"
#include "oge/OgeSingleton.h"
#include "oge/containers/OgeMap.h"
#include "oge/audio/OgeAudioBufferManager.h"
#include "oge/audio/OgeAudio.h"
#include "oge/audio/OgeAudioContext.h"

namespace oge
{
    /**
     * Manage the audios
     *
     * In OpenAL the relationships between context, listener, source and buffer are:
     *
     *   "When initializing OpenAL, at least one device has to be opened.
     *    Within that device, at least one context will be created.
     *    Within that context, one listener object is implied, and
     *    a multitude of source objects can be created.
     *    Each source can have one or more buffers objects attached to it.
     *    Buffer objects are not part of a specific context
     *    - they are shared among all contexts on one device."
     *
     *    Device - context 1 - listener
     *                         source - buffer
     *                                - buffer
     *                         source, ...
     *           - context 2 - listener
     *                         source, source, ...
     *
     *           - buffer, buffer, ...
     *
     * Sequence: 1) Create a device   (defined at initialisation)
     *           2) Create context(s)
     *           3) Create buffer(s)  (in the device)
     *           4) Create source(s)  (in a context)
     *           5) Create a listener (if not the default of a context)
     *           6) Select a context  (if not the default)
     *           7) Play source(s)
     *
     * @note Several methods were directly inspired from the ALUT library.
     *
     */
    class OGE_AUDIO_API AudioManagerOpenAL : public AudioManager, 
        public Singleton<AudioManagerOpenAL>
    {
    public:
        /// Map of the AudioContexts by id 
        typedef Map<std::map<String, AudioContext*>, String, AudioContext*> AudioContextMap;
        /// Map of the codec factories by extension
        typedef std::map<String, AudioFactory*> AudioFactoryMap;

    private:
        /// Active OpenAL device
        ALCdevice*                  mDevice;
        /// Active audio context
        AudioContext*               mContext;
        bool mIsInitialised;

        /// Map all the context. mContext being the active one.
        AudioContextMap mContexts;

        bool mOggExtensionPresent;
        bool mIsEAXPresent;

        AudioFactoryMap mAudioFactories;

        AudioBufferManager mAudioBufferManager;

    public:
        //inline AudioBufferManager* const getAudioBufferManager() const
        //{
        //    return mAudioBufferManager;
        //}
        //ALCdevice* getAudioDevice() const { return mDevice; }
        //AudioContext* getAudioContext() const { return mAudioContext; }

        static AudioManagerOpenAL* createSingleton();
        static void destroySingleton();
        static AudioManagerOpenAL* getSingletonPtr() { return mSingleton; }
        static AudioManagerOpenAL& getSingleton()
        {
            assert(mSingleton);
            return *mSingleton;
        }
        /**
         * Check the sanity of the sound system
         * Code riped of the ALUT method _alutSanityCheck ()
         */
        bool sanityCheck();
        /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        const String getLibrariesVersion();

        /**
         * Returns a string containing all the devices available on this machine
         */
        String listAvailableDevices();
        /**
         * Check for EAX 2.0 support and
         * Retrieves function entry addresses to API ARB extensions, in this case,
         * for the EAX extension. See Appendix 1 (Extensions) of
         * http://connect.creativelabs.com/openal/Documentation/OpenAL%201.1%20Specification.pdf
         */
        bool isEAXAndExtensionPresent();
        /**
         * Test if Ogg Vorbis extension is present
         */
        bool isOggExtensionPresent();
        /**
         * Select a context and make it active
         * @param contextName The name of the context ("" by default which select
         *        the default context)
         */
        bool setAudioContextActive( const String& contextName = "" );

        /**
         * Creates an audio in the active context.
         * This method doesn't load the file!
         *
         * @see Audio::load() & Audio::play()
         * @returns SharedPtr to the audio or zero if a problem occurred.
         * @param The name of the audio file
         * @param Load as stream or not (default = false = loaded in memory buffer)
         * @param Nb of buffers to use when streaming (2 by default, 3 or 4 can increase audio quality)
         * @note The nb buffers is not used when not streaming.
         */
        AudioPtr createAudio( const String& filename, bool streaming = false, 
            unsigned int nbBuffers = 2 );
        /**
         * Register the factories of in-build supported codecs
         * (wave and ogg).
         */
        void registerInBuildCodec();
        void registerCodec(AudioFactory* factory);
        void deleteAllCodecs();
        AudioFactory* getAudioFactory( const String& filename );

        /// Returns the next available source or 0 if none available
        inline AudioSourcePtr getNextFreeSource(Audio* audio) { return mContext->getNextFreeSource(audio); }

    protected:
        /**
         * Create an audio device
         * @param The device name by default it is set to "NULL" which creates
         *        the default device. Use "DirectSound3D" for D3D sound.
         * @note  However the default (NULL) device can usually not have multiple
         *        contexts !
         */
        bool setupAudioDevice(const String& deviceName = "NULL");
        /**
         * Create an audio context: more than one context can be created
         * However only one can be active at a time.
         * Each context contains one listener and contains zero to many sources.
         * @param The context name by default it is set to ""
         * @return A pointer to the audio context (0 if an error occurred)
         */
        AudioContext* createAudioContext( const String& deviceName = "default");
        bool shutdownOpenAL();

        bool setupResourceManagement();
        void shutdownResourceManagement();

        // System methods
        bool initialise();
        void shutdown();
        void tick(double currentTime);
		void preTick(double currentTime);
		void postTick(double currentTime);

        /**
         * Check for ALError and logs the error.
         * @note Returns FALSE if no error this permits to use this method in ifs
         * @returns FALSE if NO error. True if error.
         * @todo Add all error code
         */
        bool checkALError( const String& message = "" );
        /**
         * Check for ALCError and logs the error.
         * @note Returns FALSE if no error this permits to use this method in ifs
         * @returns FALSE if NO error. True if error.
         * @todo Add all error code
         */
        bool checkALCError(ALCdevice* device, const String& message = "" );

    private:
        AudioManagerOpenAL();
        ~AudioManagerOpenAL();
    };
}
#endif // __OGE_AUDIOMANAGEROPENAL_H__
