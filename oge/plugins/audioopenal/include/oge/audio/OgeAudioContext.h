/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_AUDIOCONTEXT_H__
#define __OGE_AUDIOCONTEXT_H__

#include "oge/audio/OgeAudioPrerequisitesOpenAL.h"
#include "oge/audio/OgeAudioListener.h"
#include "oge/OgeNonCopyable.h"
#include "oge/OgeString.h"
#include "oge/audio/OgeAudio.h"

namespace oge
{
    /**
     * @note Although this is in the openal specification only the 
     *       'OpenAL Soft' implementation can create safely contexts!
     *       Hence you should only use ONE context when using OpenAL
     *       (Or you know what you are doing)
     *       In this case this class should be more seen as a SourceManager !
     *
     * Audio contexts can be represent a whole scene or the sounds that 
     * one player ears. This makes it easy to switch places or persona during
     * the game without unloading/reloading all the audios.
     *
     * Of course, if you don't have to much audios, you could simply 
     * add all audios in a context and set the listener according
     * to the persona actually 'listening'.
     *
     * A context contain one listener and multiple sources. Each source is
     * related to a buffer. Note that the buffers are common to all contexts
     * and are managed by the AudioBufferManager.
     *
     * @note  However the default (NULL) device can usually not have multiple
     *        contexts !
     */
    class OGE_AUDIO_API AudioContext : public NonCopyable
    {
    public:
        friend class AudioManagerOpenAL;

    private:
        mutable RWMutex mMutex;
        /**
         * Maximum number of OpenAL sources (64 by default)
         * @note The number of sources is platform & hardware dependent
         *       Some platform only support 32 or even 16 of them!
         *       So you will need to experiment to find the right number for you.
         */
        unsigned int mMaxSources;
        ALuint*      mSources;
        bool*        mAvailableSources;
        std::vector<AudioSourcePtr> mAudioSources;

        /**
         * There is only one active listener at a time (and one per context).
         * @note Don't forget to set it when you move the listener 
         *       or you switch persona.
         */
        AudioListener mListener;
        ALCcontext* mContext;
        /// Must be unique among the contexts
        String mName;

    public:
        AudioContext(ALCcontext* context, const String& name);
        virtual ~AudioContext();

        inline AudioListener* const getListener() { return &mListener; }
        inline ALCcontext* const getContext() { return mContext; }
        inline const String& getName() { return mName; }

        /**
         * This method must be called regularly (at least 1/sec)
         * Its main purpose is to update streaming audios 
         * and - more important - to detect the OpenAL sources inactive
         * (AL_STOPPED) to permit them to be reused by another AudioSource
         */
        void update();

        /**
         * Returns an audio. This method doesn't load the file!
         *
         * @see Audio::load() & Audio::play()
         * @returns SharedPtr to the audio or zero if a problem occurred.
         * @param The name of the audio file
         * @param Load as stream or not (default = false = loaded in memory buffer)
         * @param Nb of buffers to use when streaming (2 by default, 3 or 4 can increase audio quality)
         * @note The nb buffers is not used when not streaming.
         */
        AudioPtr createAudio( const String& filename, 
            bool streaming = false, unsigned int nbBuffers = 2 );

        // ----------- listener methods -----------------------
        inline void setListenerPosition( const Vector3& position)
        {
            mListener.setPosition( position );
        }
        inline void setListenerVelocity( const Vector3& velocity )
        {
            mListener.setVelocity( velocity );
        }
        inline void setListenerVolume( float volume )
        {
            mListener.setVolume( volume );
        }

        /// @note Expensive avoid if not necessary
        inline void setListenerOrientation( const Quaternion& orientation )
        {
            mListener.setOrientation( orientation );
        }
        /// @note Expensive avoid if not necessary
        inline void setListenerLocation( const Vector3& position, 
            const Vector3& velocity, const Quaternion& orientation)
        {
            mListener.setLocation( position, velocity, orientation );
        }
        /// @note Expensive avoid if not necessary
        inline void setListener( const Vector3& position, 
            const Vector3& velocity, const Quaternion& orientation,
            float gain, float maxDistance, float minGain, float maxGain )
        {
            mListener.setListener( position, velocity, orientation, 
                gain, maxDistance, minGain, maxGain );
        }

        // ----------- sources management --------------
        /**
         * Set the maximum number of OpenAL sources (64 by default)
         * @note The number of sources is platform & hardware dependent
         *       Some platform only support 32 or even 16 of them!
         *       So you will need to experiment to find the right number for you.
         * @note Of course this must be called before createAllSources()
         */
        void setMaxSources( unsigned int max ) { mMaxSources = max; }
        /// Returns the next available source or 0 if none available
        AudioSourcePtr getNextFreeSource(Audio* audio);

    protected:
        /**
         * @note The OpenAL sources can only be created when the context is active!
         *       (I think that this method must be recalled when the context is reactivated!)
         * @see setMaxSources()
         */
        bool createAllSources();

    private:
    };
}
#endif // __OGE_AUDIOCONTEXT_H__
