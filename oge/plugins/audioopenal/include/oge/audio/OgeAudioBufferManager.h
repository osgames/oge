/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_AUDIOBUFFERMANAGER_H__
#define __OGE_AUDIOBUFFERMANAGER_H__

#include "oge/audio/OgeAudioPrerequisitesOpenAL.h"
#include "oge/OgeSingleton.h"
#include "oge/audio/OgeAudioBuffer.h"
#include "oge/datastreams/OgeDataStream.h"
#include "oge/OgeString.h"
#include "oge/OgeSingleton.h"

namespace oge
{
    class OGE_AUDIO_API AudioBufferManager : public Singleton<AudioBufferManager>
    {
    protected:
        typedef std::map<const String, AudioBufferPtr> AudioBufferMap;

    private:
        Mutex mBufferManagerMutex;
        /**
         * Maximum number of OpenAL buffers (5000 by default)
         *
         * @note Even if we can have a virtually illimited number of buffers
         *       we must not forget that the audios can be stored in-memory
         *       Hence decreasing the number of buffers is a simple way 
         *       to limit the memory usage.
         *       Of course, decreasing this number too much will increase
         *       the number of audios re-loading.
         */
        unsigned int mMaxBuffers;
        /// Nb of OpenAL buffers actually used by an audio
        unsigned int mBuffersInUse;
        /// Nb of OpenAL buffers NOT used by an audio but not deleted now
        unsigned int mBuffersStored;
        /// Stores the buffers - most recently added at the end
        AudioBufferMap mBuffers;

    public:
        AudioBufferManager();
        virtual ~AudioBufferManager();

        static AudioBufferManager* createSingleton();
        static void destroySingleton();
        static inline AudioBufferManager* getSingletonPtr()
        {
            return mSingleton;
        }
        static inline AudioBufferManager& getSingleton()
        {
            assert(mSingleton);
            return *mSingleton;
        }

        //void setMaxBuffers(unsigned int max) { mMaxBuffers = max; }
        unsigned int getMaxBuffers() { return mMaxBuffers; }

        /**
         * Create OpenAL buffer(s)
         * @param nbBuffers the number of buffers (1 by default, must not be 0)
         * @returns A shared ptr to the buffer. Empty if not enough buffers are
         *          available now.
         */
        AudioBufferPtr createBuffer( const String& filename, unsigned int nbBuffers = 1 );
    
        /** If a file was already buffered returns a pointer to it
         * An empty pointer if not
         */
        AudioBufferPtr getBuffer( const String& filename, unsigned int nbBuffers = 1 );

    private:
        void deleteBuffers();
    };
}

#endif // __OGE_AUDIOBUFFERMANAGER_H__
