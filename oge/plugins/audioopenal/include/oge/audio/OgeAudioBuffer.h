/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_AUDIOBUFFER_H__
#define __OGE_AUDIOBUFFER_H__

#include "oge/audio/OgeAudioPrerequisitesOpenAL.h"
#include "oge/resource/OgeResource.h"
#include "oge/OgeReferenceCounter.h"
#include "oge/OgeString.h"

namespace oge
{
    /**
     * This class abstract away the OpenAL buffer(s)
     * One AudioBuffer can be linked to 1 or more OpenAL buffers
     *
     * When more than one buffers is created the buffers are accessed
     * like this:
     *     ALuint* buffer = audioBuffer->getBuffer();
     *     alBufferData( buffer[0], ... );
     *     alBufferData( buffer[1], ... );
     *
     * @note It would have been easier one AudioBuffer == one OpenAL buffer
     *       but ogg streaming requires that it 
     */
    class OGE_AUDIO_API AudioBuffer // : public Resource
    {
    protected:
        /// The id of the OpenAL buffer
        ALuint* mBuffer;
        unsigned int mNbBuffers;

    public:
        AudioBuffer(unsigned int nbBuffers = 1);
        ~AudioBuffer();

        inline ALuint* getBuffer() { return mBuffer; }
        inline unsigned int getNbBuffers() { return mNbBuffers; }

    protected:
        bool loadImpl();
        void unloadImpl();
    private:
        /// Delete all buffers
        void deleteBuffers();
    };

    /// Shared ptr to an AudioBuffer instance - will delete itself when no one reference it anymore
    typedef Poco::SharedPtr<AudioBuffer, oge::ReferenceCounter> AudioBufferPtr;
    //typedef Poco::SharedPtr<AudioBuffer, oge::ReferenceCounter, 
    //    Poco::ReleasePolicy<Resource>> AudioBufferPtr;
}

#endif // __OGE_AUDIOBUFFER_H__
