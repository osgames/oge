/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_AUDIOOGGSTREAM_H__
#define __OGE_AUDIOOGGSTREAM_H__

#include "oge/audio/OgeAudioPrerequisitesOpenAL.h"
#include "oge/audio/OgeAudioOpenAL.h"
#include "oge/audio/OgeAudioBuffer.h"

#include <ogg/ogg.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

namespace oge
{
    class OGE_AUDIO_API AudioOggStream : public AudioOpenAL
    {
    private:
        AudioBufferPtr mBuffer;
        unsigned int mNbBuffers;
        OggVorbis_File mOggFile; // stream handle

        /// Some formatting data
        ALenum mFormat;
        long mRate;
        bool mIsActive; // TODO not used... remove or use?

    public:
        AudioOggStream(AudioFactory* creator, const String& filename,
            unsigned int nbBuffers = 2);
        virtual ~AudioOggStream();

        virtual void play();
        virtual void stop();
        /**
         * This method must be called regularly or the streaming will stop
         */
        virtual void update();

        bool load();
        bool reset();


    private:
        bool _load();
        String getOggErrorString(int code);

        /**
         * @note This method CANNOT be called after load()!?!
         *       but only inside it before the oggfile is closed!!!
         */
        String getParameters(vorbis_info* vorbisInfo, vorbis_comment* vorbisComment);

        void unqueueBuffers();
        /**
         * Stream a file to a source
         *
         * @note Loop until the file is read !!! TODO Should spawn a sub-process!
         *
         * @note If you are using a source for streaming never bind a buffer to it
         *       using 'alSourcei'. Always use 'alSourceQueueBuffers' consistently.
         */
        bool stream(unsigned int buffer);
    };

    // ----- NO FACTORY as this class is created by the OgeOggFactory! ----
}

#endif // __OGE_AUDIOOGGSTREAM_H__
