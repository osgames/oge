/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_LABELCOMPONENTOGRE_H__
#define __OGE_LABELCOMPONENTOGRE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/OgeString.h"
#include "oge/graphics/OgeLabelComponent.h"

#include "oge/graphics/OgeMovableTextOGRE.h"
#include "oge/graphics/OgeMovableOverlayOGRE.h"

namespace oge
{
    /**  
     * This component type permits to add text attached to a graphical object
     * such as "player name", health bar, picture, faction color, ...
     *
     * Properties:
     * 
     *  - setable font, color, size
     *  - size fixed or z-dependent, min-max
     *  - translucide or not
     *  - views tags - used for multiple .... euh.. properties. For example faction, profession, health, all each with its own image/text...
     *  - image or a list if you want to switch between different object "tags"
     *  - 1 or 2 bar life
     *  - camera oriented
     *  - overlap enabled - that is if not enabled the text will move to not overlap other ones
     *
     * @note This component MUST be attached to a GraphicsComponent (which contain a getEntity() method)
     * @todo Make the class independent of the above restriction (aka not dependent on an entity).
     *       If not entity then use a SceneNode and an offset.
     *
     * @note This component is of the family "Label"... but it is updated as 
     *       a graphical element.
     *
     * @todo WIP Extend to make more versatile
     * @todo Study if we should make a template of this class
     *       InfoComponent<MovableText> and InfoComponent<MovableOverlay>
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API LabelComponentOGRE : public LabelComponent
    {
        friend class LabelComponentTemplateOGRE;
    protected:
        // Movable text
        Ogre::MovableTextOGRE* mMovableText;
        Ogre::SceneNode* mNode;

        // Movable overlay
        Ogre::MovableTextOverlayOGRE* mMovableOverlay;
        Ogre::MovableTextOverlayAttributes* mAttrs;

    public:
        virtual ~LabelComponentOGRE()
        {
        }

        /// (Re)set the default values
        virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
        virtual void _destroy(SceneManager* sceneManager);
        virtual void _activate(bool activate, SceneManager* sceneManager);
        virtual UpdateRate _getUpdateRate() const;
        virtual void _update(double deltaTime);

        virtual void _processMessage(const Message& message);

        // ------- Similar methods to the Graphics Family ----------
        //virtual Vector3 getDimensions() const;
        virtual void showBoundingBox(bool show);

        void _setNode(Ogre::SceneNode* node);    // WIP
        inline Ogre::SceneNode* _getNode() { return mNode; }

        // ------- Specific method to the Label Family ----------
        /// @param id must be unique
        virtual void createMovableText(const String& id, const String& caption);
        /// @param id must be unique
        virtual void createMovableOverlay(const String& id, const String& caption);

        /// Show the info defined by 'index'
        virtual void showInfo( int index );


    protected:
        inline LabelComponentOGRE(const ComponentType& type, const ComponentType& family)
            : LabelComponent(type, family)
            , mMovableText(0), mNode(0), mMovableOverlay(0), mAttrs(0)
        {}

        void _setPositionByMessage(const Message& message);
        void _setScaleByMessage(const Message& message);
        void _setOrientationByMessage(const Message& message);
        void _setPositionOrientationByMessage(const Message& message);
        //_ void _setDimensionsByMessage(const Message& message);
    };

    /**
     * Builder that is used to create the OGRE Label Components based on a template.
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API LabelComponentTemplateOGRE : public ComponentTemplate
    {
    public:
        LabelComponentTemplateOGRE() 
        {
            setFamily("Label");           // NOTE: only in the base component of a family!
            // TODO create a list of types that must be implemented
            setType("LabelComponent");    // NOTE: Not GraphicsComponentOGRE
        }
        /// Create the component
        virtual Component* createComponent()
        {
            return new LabelComponentOGRE( getType(), getFamily() );
        }
    };

}
#endif
