/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_RENDERTARGETMANAGER_H__
#define __OGE_RENDERTARGETMANAGER_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/containers/OgeMap.h"
#include "oge/OgeString.h"
#include "oge/object/OgeObject.h"
#include "oge/input/OgeInputManager.h"

#include "OgrePrerequisites.h"
#include "OgreCommon.h"
#include "OgreWindowEventUtilities.h"
#include "OgreRenderWindow.h"

/*
 TODO:
    - http://www.ogre3d.org/phpBB2/viewtopic.php?t=24861&highlight=remove+viewport
  NOTES:
   - Use oge::String because the compiler sometimes mixes it with the Ogre::String!
*/

namespace oge
{
    /** 
     * Viewport settings
     *
     * @note (Oge developer) One difficulty is that ogre::Viewport don't have 
     *     a unique id. They can only be retrieved either by Z-Order !? 
     *     or by an index - that can change if a viewport is removed.
     *     Hence we must keep in the settings a ptr to the viewport itself!
     * 
     * @author Steven 'lazalong' Gay
     */
    struct OGE_GRAPHICS_API ViewportSettings
    {
        /// Viewport name id - unique across all viewports
        oge::String name;
        /// Pointer to the actual viewport
        Ogre::Viewport* viewport;
        /// Render target name (window or texture)
        oge::String renderTargetName;
        /// Ogre Camera displaying on this overlay
        Ogre::Camera* camera;
        String cameraName;
        /** Relative Z-order on the target. Lower = further to the front.
         *  @note Must be unique for the same rendertarget. This prevents overlap
         *        and used to delete the viewport individually.
         */
        int zOrder;
        /// Relative dimensions to the target
        float left, top, width, height;

        /// Constructor
        ViewportSettings() : name(""), renderTargetName(""), camera(0),
            cameraName(""), zOrder(0), left(0), top(0), width(1.0f), height(1.0f)
        {
        }
    };

    /// Map of < viewport name, viewport settings >  
    typedef Map<std::map<String, ViewportSettings>, 
            String, ViewportSettings> ViewportMap;

    /**
     * Used to store settings about Ogre render window
     * @author Steven 'lazalong' Gay
     */
    struct OGE_GRAPHICS_API RenderWindowSettings : public Ogre::WindowEventListener
    {
        /// RenderWindow name id
        String name;
        /// Fullscreen
        bool fullscreen;
        /// Width in pixel
        unsigned int width;
        /// Height in pixel
        unsigned int height;
        /// Automatically creation at initialisation phase
        bool automatic;
        /// Tells if this render window is the primary or not. Set automatically.
        bool primary;
        /// Ogre parameters list - see Ogre doc
        Ogre::NameValuePairList miscParams;

        /// Constructor
        RenderWindowSettings() : 
            name(""), fullscreen(false), width(4), height(4),
            automatic(false), primary(false)
        {
        }
        /// Ogre::WindowEventListener methods
        virtual void windowResized(Ogre::RenderWindow* rw)
        {
            if (InputManager::getManager())
                InputManager::getManager()->windowResized( rw->getWidth(), rw->getHeight() );
/*_            // TODO ?
            //if (GuiManager::getSingletonPtr())
            //    GuiManager::getSingletonPtr()->windowResized( rw->getWidth(), rw->getHeight() );
            // if (GraphicsManager::getSingletonPtr())
            //    GraphicsManager::getSingletonPtr()->windowResized( rw->getWidth(), rw->getHeight() );        
*/
        }
        virtual void windowClosed(Ogre::RenderWindow* rw)
        {
            // TODO
        }
    };

    /*
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API RenderTargetManager
    {
    public:
        /// Map of < render target name, render window ptr >
        typedef Map<std::map<String, RenderWindowSettings>, 
                String, RenderWindowSettings> RenderWindowMap;

    private:
        Ogre::Root* mOgreRoot;
        /**
         * Returns true if a primary windows is already created
         * @note If the render window is destroyed the GL context or
         *       D3D renderer is lost!
         */
        bool primaryRenderWindowExist;
        /// List all the render windows
        RenderWindowMap mRenderWindowMap;
        /// List all the viewports (makes it easier to find them again :)
        ViewportMap mViewportMap;

    public:
        RenderTargetManager(Ogre::Root* ogreRoot);
        ~RenderTargetManager();

        Ogre::RenderWindow* createRenderWindow(RenderWindowSettings settings);
        /**
         * Add a viewport to the specified target.
         *
         * @param name Name of the viewport
         * @param targetName Name of the target (window or texture)
         * @param camera Pointer to the OGRE camera
         * @param zOrder Relative Z-order on the target. Lower = 
         *        further to the front. NOTE: Must be unique per target!
         * @note Must be unique for the same rendertarget. This prevents overlap
         *       and permits us to delete the viewport individually.
         */
        Ogre::Viewport* addViewport(const oge::String& name, 
            const oge::String& targetName, Ogre::Camera* camera, 
            int zOrder = 0, float left = 0.0f, float top = 0.0f,
            float width = 1.0f, float height = 1.0f);

        void setCameraToViewport(GraphicsSceneManager* sm, const ObjectId camera,
            const String& viewportName);
        /** 
         * Sets the initial background colour of the viewport (before rendering).
         */
        void setBackgroundColour( const String& viewportName, 
            const Ogre::ColourValue& colour);
        /**
         * Remove a viewport from the render target
         * @note The viewport settings are *NOT* deleted, only the viewport
         *       is removed from the render target (and needs to be added
         *       again to be reused).
         */
        void removeViewport(const String& viewportName);
        /**
         * Remove all viewports of a target
         * @note The viewport settings are *NOT* deleted, only the viewports
         *       are removed from the render target (and needs to be added
         *       again to be reused).
         */
        void removeAllViewports(const String& targetName);

    private:
        Ogre::Viewport* addViewport( ViewportSettings settings );
        /**
         * Returns false if an existing viewport on the same target 
         * has an identical zorder
         */
        bool isZOrderUnique( const ViewportSettings& settings );
    };
}
#endif // __OGE_RENDERTARGETMANAGER_H__
