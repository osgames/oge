/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_GRAPHICSPLUGIN_H__
#define __OGE_GRAPHICSPLUGIN_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/plugin/OgePlugin.h"
#include "oge/plugin/OgePluginManager.h"

#if defined(PLUGIN_OGRE_STATIC_LIB)
    extern "C" OGE_GRAPHICS_API bool GraphicsOGREPluginStaticInit(const oge::PluginServices* mgr, void* hdll);
#endif

namespace oge
{
    /**
     * OGRE based Graphics Plugin
     * 
     * @author Steven 'lazalong' Gay
     */
    class GraphicsPlugin : public Plugin
    {
    private:
        PluginManager& mPluginManager;

        String mName;
        String mVersion;

        GraphicsManagerOGRE* mGraphicsManager;

    public:
        GraphicsPlugin(PluginManager& manager);
        virtual ~GraphicsPlugin();

        const String& getName() const;
        const String& getVersion() const;
        bool initialise();
        bool shutdown();

        // ------------- Static plugin interface ---------------
        static Plugin* createInstance(Plugin_Params* params)
        {
            return new GraphicsPlugin(*params->mgr); //NEXT use params
        }
        static int destroyInstance(Plugin* plugin)
        {
            if (!plugin)
                return -1;
            delete (GraphicsPlugin*) plugin;
            return 0;
        }
    };
}
#endif // __OGE_GRAPHICSPLUGIN_H__
