/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_DYNAMICLINES_H__
#define __OGE_DYNAMICLINES_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/graphics/OgeDynamicRenderable.h"
#include "oge/math/OgeVector3.h"

#include "OgreRenderSystem.h"

namespace oge
{
    /**
     * Implements a way to draw lines dynamically.
     * Based on the DynamicRenderable code and hence can dynamically grow or
     * shrink the hardware buffer according to the number of points.
     * This version also has a public interface for switching between lines 
     * strips and line segments. 
     *
     * @doc Based on http://www.ogre3d.org/wiki/index.php/DynamicLineDrawing
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API DynamicLines : public DynamicRenderable
    {
//_        typedef Ogre::Vector3 Vector3;
//_        typedef Ogre::Quaternion Quaternion;
//_        typedef Ogre::Camera Camera;
//_        typedef Ogre::Real Real;
        typedef Ogre::RenderOperation::OperationType OperationType;

        enum 
        {
            POSITION_BINDING,
            TEXCOORD_BINDING
        };

    private:
        bool mDirty;
        std::vector<Ogre::Vector3> mPoints;
        /// Vertex colors of the points
        std::vector<Ogre::ColourValue> mColors;
        Ogre::RenderSystem* mOgreRenderSystem;

    public:
        /// Constructor - see setOperationType() for description of argument.
        DynamicLines(OperationType opType=Ogre::RenderOperation::OT_LINE_STRIP);
        virtual ~DynamicLines();

        /**
         * Add a point to the point list (white by default)
         */
        void addPoint(const Vector3& point, const Vector3& colour = Vector3::UNIT_SCALE);
        /**
         * Add a point to the point list (white by default)
         */
        void addPoint(Real x, Real y, Real z, Real red=1.0f, Real green=1.0f, Real blue=1.0f);

        /// Change the location of an existing point in the point list
        void setPoint(unsigned short index, const Ogre::Vector3 &value);

        /// Return the location of an existing point in the point list
        const Ogre::Vector3& getPoint(unsigned short index) const;

        /// Return the total number of points in the point list
        unsigned short getNumPoints() const;

        /// Remove all points from the point list
        void clear();

        /// Call this to update the hardware buffer after making changes.  
        void update();

        /**
        * Set the type of operation to draw with.
        * @param opType Can be one of 
        *    - RenderOperation::OT_LINE_STRIP
        *    - RenderOperation::OT_LINE_LIST
        *    - RenderOperation::OT_POINT_LIST
        *    - RenderOperation::OT_TRIANGLE_LIST
        *    - RenderOperation::OT_TRIANGLE_STRIP
        *    - RenderOperation::OT_TRIANGLE_FAN
        *    The default is OT_LINE_STRIP.
        */
        void setOperationType(OperationType opType);
        OperationType getOperationType() const;

    protected:
        /// Implementation DynamicRenderable, creates a simple vertex-only decl
        virtual void createVertexDeclaration();
        /// Implementation DynamicRenderable, pushes point list out to hardware memory
        virtual void fillHardwareBuffers();
    };
}
#endif
