/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt .
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_GRIDOGRE_H__
#define __OGE_GRIDOGRE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"

#include "OgreVector3.h"

namespace oge
{
    /**
     * Debug XZ-Grid, in other word an horizonzal grid
     * X axis in red, Z axis in blue
     *
     * @todo Rotation not implemented
     * @note This Grid is the same as in the OGE Editor (OGEd).
     * @note CPU expensive
     * @author Steven 'lazalong' GAY
     */
    class OGE_GRAPHICS_API Grid
    {
    public:
        static Ogre::String gMajorMaterialName;
        static Ogre::String gMediumMaterialName;
        static Ogre::String gMinorMaterialName;

    private:
        Ogre::ManualObject* mGrid;

        Ogre::Vector3 mDimension;
        Ogre::Vector3 mSpacing;
        Ogre::Vector3 mPosition;
        Ogre::Vector3 mOrientation;

    public:
        Grid();
        virtual ~Grid();

        /**
         * Dimension of the grid in each axis
         * Spacing of the red, blue and white lines (default 100,10,1) 100,10,2 is also nice.
         * Position in the world space
         * @todo Orientation & rotation of the grid plane
         */
        Ogre::ManualObject* createGrid(
            Ogre::SceneManager*  const scenManager,
            const Ogre::String& name,
            const Ogre::Vector3& dimension = Ogre::Vector3(1000,1000,1000),
            const Ogre::Vector3& spacing = Ogre::Vector3(100,10,1),
            const Ogre::Vector3& position = Ogre::Vector3(0,0,0),
            const Ogre::Vector3& orientation = Ogre::Vector3(1,0,0) );
        void update();

    private:
        void createMaterials();
        /** 
         * @todo Make the alpha proportional to the camera distance AND the scale
         */
        Ogre::Real getAlpha( Ogre::Real x, const Ogre::Vector3& scale);
    };
}
#endif
