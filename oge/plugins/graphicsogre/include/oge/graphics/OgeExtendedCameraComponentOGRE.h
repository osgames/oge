/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_EXTENDEDCAMERACOMPONENTOGRE_H__
#define __OGE_EXTENDEDCAMERACOMPONENTOGRE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/OgeString.h"
#include "oge/graphics/OgeCameraComponent.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"

#include "OgreCamera.h"
#include "OgreSceneManager.h"
#include "OgreSceneNode.h"

namespace oge
{
    /**
     * Extended camera - camera with several modes:
     *   FIRST_PERSON: the camera moves along with the target (default)
     *   THIRD_PERSON_CHASED: the camera follow a target with an offset
     *   THIRD_PERSON_FIXED: the camera has a fixed position and tracks a target
     *
     * The default being FIRST_PERSON mode. The mode can be set via setCameraMode(). 
     *
     * @todo The behavior of the First person mode is different from the simpler
     *       CameraComponentOGRE. We should unifiy them (aka make the camera rotate with
     *       the mouse if we want to.
     * @todo NEXT We could use 3 nodes (yaw, roll, pitch) for easier rotation. Useful? Performance?
     *       See http://www.ogre3d.org/wiki/index.php/Creating_a_simple_first-person_camera_system
     * @todo Consider making subclasses for the different modes 
     *       to suppress the switch in update... but lots of code.
     * @todo There are several minor todos in the code to add functionalities and flexibility
     *
     * @reference http://www.ogre3d.org/wiki/index.php/3rd_person_camera_system_tutorial
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API ExtendedCameraComponentOGRE : public CameraComponent
    {
        friend class ExtendedCameraComponentTemplateOGRE;

    protected:
        int mCameraMode;

        Ogre::Camera* mCamera;
        String mCameraName;
        Ogre::SceneManager* mSceneManager;

        /// Main node usually linked to the character
        Ogre::SceneNode* mViewerNode;
        /// The camera is always looking at this node
        Ogre::SceneNode* mSightNode;
        /// Node used for the chase mode. If not in chase mode mChaseNode
        Ogre::SceneNode* mChaseNode;

        Ogre::Vector3 mFixedPosition;

        /**
         * Determines the movement of the camera: the camear will move to
         * the mChaseOffset position
         * If mThightness == 1 means tight movement (aka instantaneous movement)
         * while 0 means no movement (0.5 by default)
         */
        float mTightness;
        bool mFixedYaw;
		double mLastTick;
        /**
         * Offset the camera, i.e. the eye position
         */
        Ogre::Vector3 mViewerOffset;
        /**
         * Offset the sight position (aka where the camera is looking at)
         * This is usually used along with mViewerOffset, i.e. the eye position
         */
        Ogre::Vector3 mViewerSightOffset;

        /// Sight offset
        Ogre::Vector3 mSightOffset;
        /// Chase offset (position where the camera will try to be)
        Ogre::Vector3 mChaseOffset;

    public:
        virtual ~ExtendedCameraComponentOGRE();

        /// (Re)set the default values
        virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
        virtual void _destroy(SceneManager* sceneManager);
        virtual void _activate(bool activate, SceneManager* sceneManager);
        virtual UpdateRate _getUpdateRate() const { return Component::EVERY_TICK; } // NEXT NO_UPATE when possible!
		virtual void _preTick(double currentTime);
        virtual void _update(double deltaTime);

        /**
         * Implement Serialisable methods
         * @note Actually the serialisation methods read/write the whole object.
         * @todo Only the differences from the object template should be serialised.
         */
        virtual void write(Serialiser* serialiser);
        virtual void read(Serialiser* serialiser);
        virtual bool fixup();

        // TODO/FIXME: The follwing methods are NOT THREAD-SAFE!! ---> should be done by messages.
        // ------- Specific method to the camera Family ----------
        virtual void setPosition( const Vector3& position )
        {
            mViewerNode->setPosition( position.x, position.y, position.z );
        }
        inline void setOrientation( const Quaternion& ori )
        {
            mViewerNode->setOrientation( ori.w, ori.x, ori.y, ori.z );
        }
		void setFixedYawAxis(bool fixedYaw, const Vector3& axis) 
		{ 
			mViewerNode->setFixedYawAxis(fixedYaw, Ogre::Vector3(axis.x,axis.y,axis.z)); 
		}
        inline void moveCameraRelative(float x, float y, float z)
        {
            mViewerNode->translate(Ogre::Vector3(x,y,z), Ogre::Node::TS_LOCAL);
        }
        /// 1 deg =~ 0.0175 rad
        inline void yaw(float degree) { mViewerNode->yaw(Ogre::Degree(degree)); }
        inline void pitch(float degree) { mViewerNode->pitch(Ogre::Degree(degree)); }
        inline void roll(float degree) { mViewerNode->roll(Ogre::Degree(degree)); }
        inline void resetOrientation() { mViewerNode->resetOrientation(); }
		
		void rotateChaseOffset(float yaw, float pitch);

        void showBoundingBox( bool show )
        {
            // TODO mCameraNode->showBoundingBox(show);
            // mChaseNode shows better the position of the camera
            mChaseNode->showBoundingBox(show);
        }

        /// Return the library camera (for example Ogre::Camera*)
        inline Any getCamera() { return Any(mCamera); }

        Quaternion getOrientation() const;
        Vector3 getPosition() const;

        virtual void _processMessage(const Message& message)
        {
            // TODO
        }

		/**
		 * Get the camera mode
         *   FIRST_PERSON: the camera moves along with the target
         *   THIRD_PERSON_CHASED: the camera follow a target with an offset
         *   THIRD_PERSON_FIXED: the camera has a fixed position and tracks a target
		 * @return the camera mode
		 */
		int getCameraMode() { return mCameraMode; }

        /**
         * Set the camera modes:
         *   FIRST_PERSON: the camera moves along with the target
         *   THIRD_PERSON_CHASED: the camera follow a target with an offset
         *   THIRD_PERSON_FIXED: the camera has a fixed position and tracks a target
         *   TOGGLE_MODE: will cycle through the modes
         */
        void setCameraMode(int mode);

        //---------- fps camera specific ----------------

        void setTightness(float tightness) { mTightness = tightness; }
        float getTightness() const { return mTightness; }
        void setFixedPosition(const Vector3& position)
        {
            mFixedPosition.x = position.x;
            mFixedPosition.y = position.y;
            mFixedPosition.z = position.z;
        }

        inline void setChasePosition(const Vector3& position)
        {
            // TODO Should we use translate( ... , Ogre::Node::TS_LOCAL); ?
            mChaseNode->setPosition( position.x, position.y, position.z );
        }
        inline void setChaseOrientation(const Quaternion& ori)
        {
            mChaseNode->setOrientation( ori.w, ori.x, ori.y, ori.z );
        }
        /**
         * Set the viewer offset, i.e. the eye position
         * @note This will ALSO set the viewer sight offset
         * @see setSightOffset
         */
        void setViewerOffset(const Vector3& offset)
        {
            mViewerOffset.x = offset.x,
            mViewerOffset.y = offset.y,
            mViewerOffset.z = offset.z;
            mViewerSightOffset.x = offset.x,
            mViewerSightOffset.y = offset.y,
            mViewerSightOffset.z = offset.z;
        }
        void setViewerSightOffset(const Vector3& offset)
        {
            mViewerSightOffset.x = offset.x,
            mViewerSightOffset.y = offset.y,
            mViewerSightOffset.z = offset.z;
        }

		Vector3 getChaseOffset() { return Vector3(mChaseOffset.x,mChaseOffset.y,mChaseOffset.z); }
		void setChaseOffset(const Vector3& offset)
		{
			mChaseOffset.x = offset.x;
			mChaseOffset.y = offset.y;
			mChaseOffset.z = offset.z;

			// @TODO if the chase offset changes we need to update
			// the position of the chase node and the orientation so it looks
			// at the sight node
			if(mCameraMode == THIRD_PERSON_CHASED) {
				mChaseNode->setPosition(mChaseOffset);
				mChaseNode->lookAt(Ogre::Vector3::ZERO,Ogre::Node::TS_PARENT);
			}
		}

    protected:
        ExtendedCameraComponentOGRE(const ComponentType& type, const ComponentType& family);

        void instantUpdate(const Ogre::Vector3& target);
        void update(const Ogre::Vector3& target);

		/**
		 * Get the owner's position.
		 * If we have no owner it returns our current position
		 *@return The owner's position
		 */
		Ogre::Vector3 getOwnerPosition(const ObjectId& id);

		/**
		 * Get the owner's orientation.
		 * If we have no owner it returns our current orientation.
		 *@return The owner's orientation
		 */
		Ogre::Quaternion getOwnerOrientation(const ObjectId& id);
    };

    /**
     * Builder that is used to create the OGRE Camera Components based on a template.
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API ExtendedCameraComponentTemplateOGRE : public ComponentTemplate
    {
    public:
        ExtendedCameraComponentTemplateOGRE()
        {
            setFamily("Camera"); // NOTE: only in the base component of a family!
            // TODO create a list of types that must be implemented by any plugin
            setType("ExtendedCamera");  // NOTE: Not CameraOGRE
        }
        /// Create the component
        virtual Component* createComponent()
        {
            ExtendedCameraComponentOGRE* comp = new ExtendedCameraComponentOGRE( getType(), getFamily() );
            return comp;
        }
    };

}
#endif
