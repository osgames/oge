/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_ANIMATIONCOMPONENTOGRE_H__
#define __OGE_ANIMATIONCOMPONENTOGRE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/graphics/OgeGraphicsComponentOGRE.h"
#include "oge/math/OgeVector3.h"

namespace oge
{
    /**  
     * The 'AnimationComponent' type component used for animated world object
     * it contains: 
     * 
     * @doc http://www.ogre3d.org/wiki/index.php/Intermediate_Tutorial_1
     *
     * @author Steven 'lazalong' Gay, Mehdi Toghianifar
     */
    class OGE_GRAPHICS_API AnimationComponentOGRE : public GraphicsComponentOGRE
    {
        friend class AnimationComponentTemplateOGRE;
    private:
        Ogre::AnimationState* mAnimationState; // The current animation state of the object

    public:
        virtual ~AnimationComponentOGRE()
        {}

        virtual UpdateRate _getUpdateRate() const;
        virtual void _update(double deltaTime);

        /**
         * Implement Serialisable methods
         * @note Actually the serialisation methods read/write the whole object.
         * @todo Only the differences from the object template should be serialised.
         */
    //    virtual void write(Serialiser* serialiser) {} // todo
    //    virtual void read(Serialiser* serialiser) {} // todo
    //    virtual bool fixup() { return true; } // todo

        // ------- Specific method to the Graphics Family ----------
        /**
         * Set the animation
         *  Does nothing if the animation doesn't exist in the mesh
         */
        virtual void setAnimationState( const String& stateName, bool loop = true,
            bool enable = true);

        virtual void _processMessage(const Message& message);

    protected:
        inline AnimationComponentOGRE(const ComponentType& type, const ComponentType& family)
             : GraphicsComponentOGRE(type, family), mAnimationState(0)
        {}

        void _setAnimationStateByMessage(const Message& message);
    };

    /**
     * Builder that is used to create the OGRE Animation Components based on a template.
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API AnimationComponentTemplateOGRE : public GraphicsComponentTemplateOGRE
    {
    public:
        AnimationComponentTemplateOGRE()
        {
            // TODO create a list of types that must be implemented
            setType("Animation"); // NOTE: Not AnimationOGRE
        }
        /// Create the component
        virtual Component* createComponent()
        {
            AnimationComponentOGRE* comp = new AnimationComponentOGRE( getType(), getFamily() );
            return comp;
        }
    };

} // namespace
#endif
