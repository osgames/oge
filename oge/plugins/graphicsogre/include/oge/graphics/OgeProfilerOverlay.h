/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/

#ifndef __OGE_PROFILEROVERLAY_H__
#define __OGE_PROFILEROVERLAY_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/OgeString.h"
#include "oge/OgeString.h"
#include "OgreOverlay.h"
#include "OgreOverlayElement.h"
#include "OgreOverlayContainer.h"

namespace oge {

    class ProfilerOverlay {

        public:
            ProfilerOverlay();
            ~ProfilerOverlay();

            /** Clears the profiler statistics */
            void reset();

			enum DisplayMode
			{
				/// Display % frame usage on the overlay
				DISPLAY_PERCENTAGE,
				/// Display milliseconds on the overlay
				DISPLAY_MILLISECONDS
			};

            /** Prints the profiling results of each frame */
            void displayResults();

			/** Set the display mode for the overlay. 
			*/
			void setDisplayMode(DisplayMode d) { mDisplayMode = d; }
			/** Get the display mode for the overlay. 
			*/
			DisplayMode getDisplayMode() const { return mDisplayMode; }

            /** Sets the Profiler so the display of results are updated every n frames*/
            void setUpdateDisplayFrequency(unsigned int freq);

            /** Gets the frequency that the Profiler display is updated */
            unsigned int getUpdateDisplayFrequency() const;

			/** Set the size of the profiler overlay, in pixels. */
			void setOverlayDimensions(Real width, Real height);

			/** Set the position of the profiler overlay, in pixels. */
			void setOverlayPosition(Real left, Real top);

			/** Set the overlay visible or not */
			void setOverlayVisible(bool visible);

			Real getOverlayWidth() const;
			Real getOverlayHeight() const;
			Real getOverlayLeft() const;
			Real getOverlayTop() const;

        protected:

            /** Initializes the profiler's GUI elements */
            void initialize();

            /** An internal function to create the container which will hold our display elements*/
			Ogre::OverlayContainer* createContainer();

            /** An internal function to create a text area */
            Ogre::OverlayElement* createTextArea(const String& name, Real width, Real height, Real top, Real left, 
                                       unsigned int fontSize, const String& caption, bool show = true);

            /** An internal function to create a panel */
            Ogre::OverlayElement* createPanel(const String& name, Real width, Real height, Real top, Real left, 
                                    const String& materialName, bool show = true);

            typedef std::list<Ogre::OverlayElement*> ProfileBarList;

            /// Holds the display bars for each profile results
            ProfileBarList mProfileBars;

            /// Whether the GUI elements have been initialized
            bool mInitialized;

            /// The max number of profiles we can display
            unsigned int mMaxDisplayProfiles;

			unsigned int mCurrentFrame;

            /// The overlay which contains our profiler results display
            Ogre::Overlay* mOverlay;

            /// The window that displays the profiler results
            Ogre::OverlayContainer* mProfileGui;

            /// The height of each bar
            Real mBarHeight;

            /// The height of the stats window
            Real mGuiHeight;

            /// The width of the stats window
            Real mGuiWidth;

			/// The horz position of the stats window
			Real mGuiLeft;

			/// The vertical position of the stats window
			Real mGuiTop;

			/// The size of the indent for each profile display bar
            Real mBarIndent;

            /// The width of the border between the profile window and each bar
            Real mGuiBorderWidth;

            /// The width of the min, avg, and max lines in a profile display
            Real mBarLineWidth;

			/// The distance between bars
			Real mBarSpacing;

            /// The number of frames that must elapse before the current
            /// frame display is updated
            unsigned int mUpdateDisplayFrequency;

			/// How to display the overlay
			DisplayMode mDisplayMode;
			bool mResetExtents;
    }; // end class

} // end namespace

#endif
