/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_MOVABLETEXTOVERLAYOGRE_H__
#define __OGE_MOVABLETEXTOVERLAYOGRE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"

// NO #include "Ogre.h"
#include "OgreFont.h"
#include "OgreFontManager.h"
#include "OgreOverlayManager.h"
#include "OgreOverlayContainer.h"
#include "OgreMovableObject.h"

namespace Ogre
{
    /**
     * This class is based on the MovableTextOverlay.h found on the Ogre wiki
     * http://www.ogre3d.org/wiki/index.php/MovableTextOverlay
     *
     * @todo For now I didn't implement the "non-overlapping feature"
     *       as I need a global overlay manager for this: see  RectLayoutManager.h
     *       I added the code below in comment
     * 
     * Material example:
     *
     *  material RedTransparent
     *  {
     *      technique
     *      {
     *          pass
     *          {
     *              scene_blend alpha_blend
     *              lighting off
     *              depth_write off
     *              texture_unit
     *              {
     *                  colour_op_ex source1 src_manual src_current 1 0 0
     *                  alpha_op_ex source1 src_manual src_current 0.4
     *              }
     *          }
     *      }
     *  }
     *
     */
    class MovableTextOverlayAttributes
    {
    public:
        const Ogre::String mName;

        Ogre::Font* mpFont;
        Ogre::String mFontName;
        Ogre::String mMaterialName;
        Ogre::ColourValue mColor;
        Ogre::Real mCharHeight;

    public:
        MovableTextOverlayAttributes(const Ogre::String & name,
                                     const Ogre::Camera *cam,
                                     const Ogre::String & fontName = "OgeBlueHighway", // See OgeFont.fontdef
                                     int charHeight = 16,
                                     const Ogre::ColourValue & color = Ogre::ColourValue::White,
                                     const Ogre::String & materialName = "");

        virtual ~MovableTextOverlayAttributes();

        void setFontName(const Ogre::String & fontName);
        void setMaterialName(const Ogre::String & materialName);
        void setColor(const Ogre::ColourValue & color);
        void setCharacterHeight(unsigned int height);

        const Ogre::String& getName() const {return mName;}
        const Ogre::Font* getFont() const {return mpFont;}
        const Ogre::String& getFontName() const {return mFontName;}
        const Ogre::String& getMaterialName() const {return mMaterialName;};
        const Ogre::ColourValue& getColor() const {return mColor;}
        const Ogre::Real getCharacterHeight() const {return mCharHeight;};
    };

    /**
     * @todo Rename this to MovableOverlay as we will add features such as a healt bar
     *  @todo Make this class derive from MovableObject so that I can implement 
     *        like in MovableText this method MovableText::_updateRenderQueue()
     *        so that it becomes invisible when behind terrain or another object !!!
     */
    class MovableTextOverlayOGRE // TODO : public MovableObject
    {
    protected:
	    const Ogre::String mName;
	    const Ogre::MovableObject* mpMov;

	    Ogre::Overlay* mpOv;
	    Ogre::OverlayContainer* mpOvContainer;
	    Ogre::OverlayElement* mpOvText;
    	
	    /// true if mpOvContainer is visible, false otherwise
	    bool mEnabled;
	    /// true if it must be visible
	    bool mVisible;
	    /// true if mTextWidth needs to be recalculated
	    bool mNeedUpdate;

	    // Text width in pixels
	    Ogre::Real mTextWidth;

	    // the Text
	    Ogre::String mCaption;

	    // true if the upper vertices projections of the MovableObject are on screen
	    bool mOnScreen;

	    // the update frequency in seconds
	    // mpOvContainer coordinates get updated each mUpdateFrequency seconds.
	    Ogre::Real mUpdateFrequency;

	    // the Font/Material/Color text attributes
	    MovableTextOverlayAttributes *mAttrs;

    public:
        MovableTextOverlayOGRE(const Ogre::String & name,
                               const Ogre::String & caption,
                               const Ogre::MovableObject *mov,
                               MovableTextOverlayAttributes *attrs);

	    virtual ~MovableTextOverlayOGRE();

	    void setCaption(const Ogre::String & caption);
	    void setUpdateFrequency(Ogre::Real updateFrequency) {mUpdateFrequency = updateFrequency;}
	    void setAttributes(MovableTextOverlayAttributes *attrs)
	    {
		    mAttrs = attrs;
		    _updateOverlayAttrs();
	    }

	    const Ogre::String&	getName() const {return mName;}
	    const Ogre::String&	getCaption() const {return mCaption;}
	    const Ogre::Real getUpdateFrequency() const {return mUpdateFrequency;}
	    const bool isOnScreen() const {return mOnScreen;}
	    const bool isVisible() const {return mVisible;}
	    const bool isEnabled() const {return mEnabled;}
	    const MovableTextOverlayAttributes* getAttributes() const {return mAttrs;}

        void setVisible(bool visible);
	    void enable(bool enable);
	    void update(Ogre::Real timeSincelastFrame);

	    // Needed for RectLayoutManager.
	    int getPixelsTop() {return Ogre::OverlayManager::getSingleton().getViewportHeight() * (mpOvContainer->getTop());}
	    int getPixelsBottom() {return Ogre::OverlayManager::getSingleton().getViewportHeight() * (mpOvContainer->getTop() + mpOvContainer->getHeight());}
	    int getPixelsLeft() {return Ogre::OverlayManager::getSingleton().getViewportWidth() * mpOvContainer->getLeft();}
	    int getPixelsRight() {return Ogre::OverlayManager::getSingleton().getViewportWidth() * (mpOvContainer->getLeft() + mpOvContainer->getWidth());}

	    void setPixelsTop(int px) {mpOvContainer->setTop((Ogre::Real)px / Ogre::OverlayManager::getSingleton().getViewportHeight());}
	    // end

    protected:
	    void _computeTextWidth();
	    void _updateOverlayAttrs();
	    void _getMinMaxEdgesOfTopAABBIn2D(Ogre::Real& MinX, Ogre::Real& MinY, Ogre::Real& MaxX, Ogre::Real& MaxY);
	    void _getScreenCoordinates(const Ogre::Vector3& position, Ogre::Real& x, Ogre::Real& y);
    };
}
#endif


/**  See the header comment above.
 In summary this class "onyl" permits to make the overlay non-overlapping.

#ifndef __RectLayoutManager_H__
#define __RectLayoutManager_H__

#include <list>
#include <algorithm>

using namespace std;

// It moves the rectangles on the Y axis so they won't overlap.
class RectLayoutManager
{
public:

	enum MethodType 
	{
		PLACE_ABOVE = 0,	// the overlapping rectangles are placed above the non-overlapping ones
		PLACE_BELOW			// the overlapping rectangles are placed below the non-overlapping ones
	};

	class Rect
	{
	public:
		Rect(short left, short top, short right, short bottom)
			: mLeft(left)
			, mTop(top)
			, mRight(right)
			, mBottom(bottom)
			, dy(0)
		{
			if (mBottom <= mTop)
				throw std::exception("Condition Failure (top < bottom) in RectLayoutManager::Rect::Rect");

			if (mRight <= mLeft)
				throw std::exception("Condition Failure (left < right) in RectLayoutManager::Rect::Rect");
		}

		inline const short getTop() const {return mTop + dy;}
		inline const short getBottom() const {return mBottom + dy;}
		inline const short getLeft() const {return mLeft;}
		inline const short getRight() const {return mRight;}

		// STL needs this
		inline bool operator <(const RectLayoutManager::Rect &R) const {return getBottom() < R.getBottom();}

		// displacement on Y axis
		short dy;

		// original rectangle coordinates
		short mBottom;
		short mTop;
		short mLeft;
		short mRight;
	};

	typedef list<RectLayoutManager::Rect> RectList;

	RectLayoutManager(	unsigned short leftBound,
						unsigned short topBound,
						unsigned short rightBound,
						unsigned short bottomBound,
						MethodType method = PLACE_ABOVE)
	: mMethod(method)
	, mBoundTop(topBound)
	, mBoundLeft(leftBound)
	, mBoundBottom(bottomBound)
	, mBoundRight(rightBound)
	, mMinDistance(1)
	, mMaxRectHeight(0)
	, mDepth(0)
	{
		if (mBoundBottom <= mBoundTop)
			throw std::exception("Condition Failure (mBoundTop < mBoundBottom) in RectLayoutManager::RectLayoutManager");

		if (mBoundRight <= mBoundLeft)
			throw std::exception("Condition Failure (mBoundLeft < mBoundRight) in RectLayoutManager::RectLayoutManager");
	}
	
	~RectLayoutManager(){clear();}

	const unsigned short getMinDistance() const {return mMinDistance;}
	const unsigned short getMaxRectHeight() const {return mMaxRectHeight;}
	const unsigned short getDepth() const {return mDepth;}
	void getBounds(	unsigned short &left,
					unsigned short &top,
					unsigned short &right,
					unsigned short &bottom)
	{
		left = mBoundLeft;
		top = mBoundTop;
		right = mBoundRight;
		bottom = mBoundBottom;
	}

	void setMinDistance(unsigned short minDistance){mMinDistance = minDistance;}
	void setDepth(unsigned short depth){mDepth = depth;}

	bool isOutOfBounds(RectLayoutManager::Rect &r)
	{
		if (r.getTop() < mBoundTop ||
			r.getBottom() > mBoundBottom ||
			r.getLeft() < mBoundLeft ||
			r.getRight() > mBoundRight)
			return true;
		else
			return false;
	}

	RectList::iterator getListBegin(){return mList.begin();}
	RectList::iterator getListEnd(){return mList.end();}

	void clear(){mList.clear();}

	RectList::iterator addData(Rect &Data)
	{
		if (isOutOfBounds(Data))
			return mList.end(); // out of bounds, error

		switch (mMethod)
		{
		case PLACE_ABOVE:
			return addDataAbove(Data);
		case PLACE_BELOW:
			return addDataBelow(Data);
		default:
			return mList.end(); // incorrect method type, error
		}
	}

protected:
	// List of orderedd rectangles
	// All items in list are assumed ordered and within established Bounds
	RectList mList;

	// The overlapping rectangles are placed at a mMinDistance from the other ones
	unsigned short mMinDistance;

	// This gets calculated "on the fly"
	unsigned short mMaxRectHeight;

	// An overlapping rectangle is moved on Y axis (Above or Below) untill
	//  a place is found where it doesn't overlap any other rectangle.
	// mDepth is the number of times an overlapping rectangle will be moved
	//  in order to find a non-overlapping place.
	//
	// (mDepth = 0) - the search will go on untill a place is found.
	// (mDepth > 0) - the search will go on <mDepth> times
	unsigned short mDepth;	

	// Don't use these directly, use addData instead
	RectList::iterator addDataAbove(Rect &Data);
	RectList::iterator addDataBelow(Rect &Data);

	// Const variables can only be set in the constructor and certify the RectList integrity.

	// Method Type
	const MethodType mMethod;

	// Bounds
	const unsigned short mBoundTop;
	const unsigned short mBoundLeft;
	const unsigned short mBoundBottom;
	const unsigned short mBoundRight;
};

static bool _fLessBottom(const RectLayoutManager::Rect &L, const RectLayoutManager::Rect &R) {return L.getBottom() < R.getBottom();}

RectLayoutManager::RectList::iterator RectLayoutManager::addDataBelow(RectLayoutManager::Rect &Data)
{
	bool MoveIt = false;
	bool FoundIt = false;
	RectList::iterator itStart;
	RectList::iterator itLastChecked;
	RectList::iterator itCurrent;
	RectList::iterator itInsert;

	unsigned short depth = 0;
	RectLayoutManager::Rect &r = Data;
	short height = r.getBottom() - r.getTop();

	if (height > mMaxRectHeight)
		mMaxRectHeight = height;

	// find the first RECT  that has .bottom > r.top
	// first possible intersect
	r.dy -= height;
	itStart = lower_bound(mList.begin(), mList.end(), r, &_fLessBottom);
	r.dy += height;

	// it's safe to add it at the back of the list
	if (itStart == mList.end())
	{
		mList.push_back(r);
		return --(mList.end());
	}

	// insert it temporarily (we will move it at the right place, afterwords)
	itInsert = mList.insert(itStart,r);

	for (itCurrent = itStart, itLastChecked = itInsert;itCurrent != mList.end();itCurrent++)
	{
		// Can't intersect r so i skip it
		if ((*itCurrent).getRight() < r.getLeft())
			continue;

		// Can't intersect r so i skip it
		if (r.getRight() < (*itCurrent).getLeft())
			continue;

		// Can't intersect r so i skip it
		if (r.getTop() > (*itCurrent).getBottom())
			continue;

		short diff = (*itCurrent).getTop() - (*itLastChecked).getBottom();
		short diff2 = mMaxRectHeight - ((*itCurrent).getBottom() - (*itCurrent).getTop());
		if (diff > 0) // above the last checked
		{
			// If no rect overlapped r, then there is no need to move it
			if (!MoveIt && (diff > diff2))
			{	
				FoundIt = true;
				itLastChecked = itStart;
				break;
			}
			else
				MoveIt = true;

			if (mDepth && (depth >= mDepth))
				break;

			// This is above r, so i check if its enought space to move r
			if (diff > height + diff2 + 2*mMinDistance)
			{
				r.dy = ((*itLastChecked).getBottom() + mMinDistance + 1) - r.getTop();
				FoundIt = true;
				break;
			}

			depth++;
		}
		else // it overlaps
			MoveIt = true;

		itLastChecked = itCurrent;
	}

	if (itCurrent == mList.end())
	{
		if (MoveIt)
			r.dy = ((*itLastChecked).getBottom() + mMinDistance + 1) - r.getTop();
		else
			itLastChecked = itStart;

		FoundIt = true;
	}

	mList.erase(itInsert);

	if (FoundIt)
	{
		if (r.getBottom() > mBoundBottom)
			return mList.end(); // out of bounds

		itInsert = lower_bound(itLastChecked, itCurrent, r);			
		itInsert = mList.insert(itInsert,r);

		return itInsert;
	};

	return mList.end();
}

static bool _fGreaterTop(const RectLayoutManager::Rect &L, const RectLayoutManager::Rect &R) {return L.getTop() > R.getTop();}

RectLayoutManager::RectList::iterator RectLayoutManager::addDataAbove(RectLayoutManager::Rect &Data)
{
	bool MoveIt = false;
	bool FoundIt = false;
	RectList::iterator itStart;
	RectList::iterator itLastChecked;
	RectList::iterator itCurrent;
	RectList::iterator itInsert;

	unsigned short depth = 0;
	RectLayoutManager::Rect &r = Data;
	short height = r.getBottom() - r.getTop();

	if (height > mMaxRectHeight)
		mMaxRectHeight = height;

	// find the first RECT  that has .bottom > r.top
	// first possible intersect
	r.dy += height;
	itStart = lower_bound(mList.begin(), mList.end(), r, &_fGreaterTop);
	r.dy -= height;

	// it's safe to add it at the back of the list
	if (itStart == mList.end())
	{
		mList.push_back(r);
		return --(mList.end());
	}

	// insert it temporarily (we will move it at the right place, afterwords)
	itInsert = mList.insert(itStart,r);

	for (itCurrent = itStart, itLastChecked = itInsert;itCurrent != mList.end();itCurrent++)
	{
		// Can't intersect r so i skip it
		if ((*itCurrent).getRight() < r.getLeft())
			continue;

		// Can't intersect r so i skip it
		if (r.getRight() < (*itCurrent).getLeft())
			continue;

		// Can't intersect r so i skip it
		if (r.getBottom() < (*itCurrent).getTop())
			continue;

		short diff = (*itLastChecked).getTop() - (*itCurrent).getBottom();
		short diff2 = mMaxRectHeight - ((*itCurrent).getBottom() - (*itCurrent).getTop());
		if (diff > 0) // above the last checked
		{
			// If no rect overlapped r, then there is no need to move it
			if (!MoveIt && (diff > diff2))
			{	
				FoundIt = true;
				itLastChecked = itStart;
				break;
			}
			else
				MoveIt = true;

			if (mDepth && (depth >= mDepth))
				break;

			// This is above r, so i check if its enought space to move r
			if (diff > height + diff2 + 2*mMinDistance)
			{
				r.dy = -(r.getBottom() - ((*itLastChecked).getTop() - mMinDistance - 1));
				FoundIt = true;
				break;
			}

			depth++;
		}
		else // it overlaps
			MoveIt = true;

		itLastChecked = itCurrent;
	}

	if (itCurrent == mList.end())
	{
		if (MoveIt)
			r.dy = -(r.getBottom() - ((*itLastChecked).getTop() - mMinDistance - 1));
		else
			itLastChecked = itStart;


		FoundIt = true;
	}

	mList.erase(itInsert);

	if (FoundIt)
	{
		if (r.getTop() < mBoundTop)
			return mList.end(); // out of bounds

		itInsert = lower_bound(itLastChecked, itCurrent, r, _fGreaterTop);			
		itInsert = mList.insert(itInsert,r);

		return itInsert;
	};

	return mList.end();
}
#endif

-----------------------------

 How to use it

Add a global vector to hold your MovableTextOverlay pointers

vector<MovableTextOverlay*> myVect;

----------------------

In your createScene

Entity *ent = mSceneMgr->createEntity( "Robot", "robot.mesh" );

// get terrain height under the camera
RaySceneQuery* raySceneQuery2 = mSceneMgr->createRayQuery(Ray(mCamera->getPosition(), Vector3::NEGATIVE_UNIT_Y));
RaySceneQueryResult& qryResult = raySceneQuery2->execute();
RaySceneQueryResult::iterator i = qryResult.begin();
if (i != qryResult.end() && i->worldFragment)
{       
	// create the attributes used by MovableTextOverlay
	MovableTextOverlayAttributes *attrs = new MovableTextOverlayAttributes("Attrs1",mCamera,"BlueHighway",16,ColourValue::White,"RedTransparent");

	// add 10 MovableTextOverlay pointers to myVect
	for (int j=0; j<10;j++)
	{
		String k = StringConverter::toString(j);
		Entity *ent2 = ent->clone("Ent"+k);
		SceneNode *node = mSceneMgr->getRootSceneNode()->createChildSceneNode( "Node"+k);
		node->setPosition(i->worldFragment->singleIntersection + Ogre::Vector3(j*10,0,0));
		node->scale(Ogre::Vector3(0.1,0.1,0.1));
		node->attachObject( ent2 );
		MovableTextOverlay*p = new MovableTextOverlay("Text"+k," Robot"+k+" ", ent2, attrs);
		p->enable(false); // make it invisible for now
		p->setUpdateFrequency(0.01);// set update frequency to 0.01 seconds
		myVect.push_back(p); 
	}
}
-------------------
In your frameStarted

RectLayoutManager m(0,0,mCamera->getViewport()->getActualWidth(),
		mCamera->getViewport()->getActualHeight());
m.setDepth(0);

int visible=0;
MovableTextOverlay *p=0;
for (vector<MovableTextOverlay*>::iterator i = myMap.begin();i<myMap.end();i++)
{
	p = *i;
	p->update(evt.timeSinceLastFrame);
	if (p->isOnScreen())
	{
		visible++;

		RectLayoutManager::Rect r(	p->getPixelsLeft(),
									p->getPixelsTop(),
									p->getPixelsRight(),
									p->getPixelsBottom());

		RectLayoutManager::RectList::iterator it = m.addData(r);
		if (it != m.getListEnd())
		{
			p->setPixelsTop((*it).getTop());
			p->enable(true);
		}
		else
			p->enable(false);
	}
	else
		p->enable(false);
}


*/
