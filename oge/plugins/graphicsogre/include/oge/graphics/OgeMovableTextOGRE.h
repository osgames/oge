/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_MOVABLETEXTOGRE_H__
#define __OGE_MOVABLETEXTOGRE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"

// NO #include "Ogre.h"
#include "OgreMovableObject.h"
#include "OgreRenderable.h"
#include "OgreString.h"
#include "OgreColourValue.h"
#include "OgreUTFString.h"
#include "OgreMaterialManager.h"

namespace Ogre
{
    /**
     * @note This code is based on the OGRE wiki MovableText article
     */
    class MovableTextOGRE : public Ogre::MovableObject, public Ogre::Renderable
    {
    public:
        enum HorizontalAlignment {H_LEFT, H_CENTER};
        enum VerticalAlignment   {V_BELOW, V_ABOVE};

    protected:
	    Ogre::String			mFontName;
	    Ogre::String			mType;
	    Ogre::String			mName;
	    Ogre::UTFString			mCaption;
	    HorizontalAlignment		mHorizontalAlignment;
	    VerticalAlignment		mVerticalAlignment;

	    Ogre::ColourValue		mColor;
	    Ogre::RenderOperation	mRenderOp;
	    Ogre::AxisAlignedBox	mAABB;
	    Ogre::LightList			mLList;

	    unsigned int			mCharHeight;
	    unsigned int			mSpaceWidth;

	    bool					mNeedUpdate;
	    bool					mUpdateColors;
	    bool					mOnTop;

	    float					mTimeUntilNextToggle;
	    float					mRadius;
        float					mAdditionalHeight;

	    Ogre::Camera		  * mpCam;
	    Ogre::RenderWindow	  * mpWin;
	    Ogre::Font			  * mpFont;
	    Ogre::MaterialPtr		mpMaterial;
        Ogre::MaterialPtr		mpBackgroundMaterial;

    public:
        MovableTextOGRE(const Ogre::String & name,
                        const Ogre::UTFString & caption,
                        const Ogre::String & fontName = "BlueHighway",
                        int charHeight = 32,
                        const Ogre::ColourValue & color = Ogre::ColourValue::White);

        virtual ~MovableTextOGRE();
        /** New Shoggoth method:allows you to get a callback to a Renderable::Visitor class 
         * for all Renderables that this object will potentially add to the render queue. 
         * Identifies LOD and debug renderables.
         */
        virtual void visitRenderables(Renderable::Visitor* visitor, bool debugRenderables = false)
        {
        }

        // Set settings
	    void setFontName(const Ogre::String & fontName);
	    void setCaption(const Ogre::UTFString & caption);
	    void setColor(const Ogre::ColourValue & color);
	    void setCharacterHeight(unsigned int height);
	    void setSpaceWidth(unsigned int width);
	    void setTextAlignment(const HorizontalAlignment & horizontalAlignment, const VerticalAlignment & verticalAlignment);
	    void setAdditionalHeight( float height );
        void showOnTop(bool show = true);

        // Get settings
	    const Ogre::String&       getFontName() const {return mFontName;}
	    const Ogre::UTFString&    getCaption() const {return mCaption;}
	    const Ogre::ColourValue&  getColor() const {return mColor;}
    	
        unsigned int getCharacterHeight() const {return mCharHeight;}
	    unsigned int getSpaceWidth() const {return mSpaceWidth;}
        float getAdditionalHeight() const {return mAdditionalHeight;}
        bool getShowOnTop() const {return mOnTop;}
        Ogre::AxisAlignedBox GetAABB(void) { return mAABB; }

    protected:

        float mViewportAspectCoef;

        // from MovableText, create the object
        void _setupGeometry();
        void _updateColors();

        // from MovableObject
        void getWorldTransforms(Ogre::Matrix4 *xform) const;
        Real getBoundingRadius(void) const {return mRadius;};
        Real getSquaredViewDepth(const Ogre::Camera *cam) const {return 0;};
        const Ogre::Quaternion&     getWorldOrientation(void) const;
        const Ogre::Vector3&        getWorldPosition(void) const;
        const Ogre::AxisAlignedBox& getBoundingBox(void) const {return mAABB;};
        const Ogre::String&         getName(void) const {return mName;};
        const Ogre::String&         getMovableType(void) const {static Ogre::String movType = "MovableText"; return movType;};

        void _notifyCurrentCamera(Ogre::Camera *cam);
        void _updateRenderQueue(Ogre::RenderQueue* queue);

        // from renderable
        void  getRenderOperation(Ogre::RenderOperation &op);
        const Ogre::MaterialPtr& getMaterial(void) const {assert(!mpMaterial.isNull());return mpMaterial;};
        const Ogre::LightList& getLights(void) const {return mLList;};
    };
} // namespace ogre
#endif
