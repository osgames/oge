/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_GRAPHICSCOMPONENTOGRE_H__
#define __OGE_GRAPHICSCOMPONENTOGRE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/OgeString.h"
#include "oge/graphics/OgeGraphicsComponent.h"

namespace oge
{
    /**
     * @author Steven 'lazalong' Gay, Mehdi Toghianifar
     */
    class OGE_GRAPHICS_API GraphicsComponentOGRE : public GraphicsComponent
    {
        friend class GraphicsComponentTemplateOGRE;
    protected:
        Ogre::Entity* mEntity;
        Ogre::SceneNode* mNode;
        Ogre::SceneNode* mOffsetNode;

    public:
        virtual ~GraphicsComponentOGRE()
        {}

        /// (Re)set the default values
        virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
        virtual void _destroy(SceneManager* sceneManager);
        virtual void _activate(bool activate, SceneManager* sceneManager);
        virtual UpdateRate _getUpdateRate() const;
        virtual void _update(double deltaTime);

        // ------- Specific method to the Graphics Family ----------
        virtual Vector3 getDimensions() const;
        virtual void showBoundingBox(bool show);

        void _setEntity(Ogre::Entity* entity);    // WIP
        void _setNode(Ogre::SceneNode* node);    // WIP

		virtual void setPosition(const Vector3 position);
		virtual void setOrientation(const Quaternion orientation);

        inline Ogre::SceneNode* _getNode() { return mNode; }
        inline Ogre::Entity* _getEntity() { return mEntity; }

		// need these for graphical attachments like cameras
		virtual const Quaternion getOrientation() const;
		virtual const Vector3 getPosition() const;

        virtual void _processMessage(const Message& message);

    protected:
        inline GraphicsComponentOGRE(const ComponentType& type, const ComponentType& family)
            : GraphicsComponent(type, family)
            , mEntity(0), mNode(0), mOffsetNode(0)
        {}

        void _setPositionByMessage(const Message& message);
        void _setScaleByMessage(const Message& message);
        void _setOrientationByMessage(const Message& message);
        void _setPositionOrientationByMessage(const Message& message);
        void _setDimensionsByMessage(const Message& message);
        void _setMaterialByMessage(const Message& message);
        void _setOffsetPositionByMessage(const Message& message);
        void _setOffsetOrientationByMessage(const Message& message);
        void _setOffsetPositionOrientationByMessage(const Message& message);

    };

    /**
     * Builder that is used to create the OGRE Graphics Components based on a template.
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API GraphicsComponentTemplateOGRE : public ComponentTemplate
    {
    public:
        GraphicsComponentTemplateOGRE() 
        {
            setFamily("Graphics");            // NOTE: only in the base component of a family!
            // TODO create a list of types that must be implemented
            setType("GraphicsComponent");    // NOTE: Not GraphicsComponentOGRE
        }
        /// Create the component
        virtual Component* createComponent()
        {
            GraphicsComponentOGRE* comp = new GraphicsComponentOGRE( getType(), getFamily() );
            return comp;
        }
    };

}
#endif
