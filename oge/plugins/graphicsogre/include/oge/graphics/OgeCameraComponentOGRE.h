/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_CAMERACOMPONENTOGRE_H__
#define __OGE_CAMERACOMPONENTOGRE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/OgeString.h"
#include "oge/graphics/OgeCameraComponent.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"

#include "OgreCamera.h"
#include "OgreSceneManager.h"
#include "OgreSceneNode.h"

namespace oge
{
    class OGE_GRAPHICS_API CameraComponentOGRE : public CameraComponent
    {
        friend class CameraComponentTemplateOGRE;

    protected:
        Ogre::Camera* mCamera;
        String mCameraName;
        Ogre::SceneManager* mSceneManager;

        Ogre::SceneNode* mNode;
        Ogre::SceneNode* mCameraNode;

    public:
        virtual ~CameraComponentOGRE();

        /// (Re)set the default values
        virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
        virtual void _destroy(SceneManager* sceneManager);
        virtual void _activate(bool activate, SceneManager* sceneManager);
        virtual UpdateRate _getUpdateRate() const { return Component::EVERY_TICK; } // NEXT NO_UPATE when possible!
        virtual void _update(double deltaTime);

        /**
         * Implement Serialisable methods
         * @note Actually the serialisation methods read/write the whole object.
         * @todo Only the differences from the object template should be serialised.
         */
        virtual void write(Serialiser* serialiser);
        virtual void read(Serialiser* serialiser);
        virtual bool fixup();

        // TODO/FIXME: The follwing methods are NOT THREAD-SAFE!! ---> should be done by messages.
        // ------- Specific method to the camera Family ----------
		virtual Quaternion getOrientation() const 
		{
			return mCameraNode->getOrientation().ptr();
		}
        virtual Vector3 getPosition() const
		{
			return mCameraNode->getPosition().ptr();
		}

		virtual void setFixedYawAxis(bool fixedYaw, const Vector3& axis) 
		{ 
			mCameraNode->setFixedYawAxis(fixedYaw, Ogre::Vector3(axis.x,axis.y,axis.z)); 
		}
        virtual void setPosition( const Vector3& position )
        {
            mCameraNode->setPosition( position.x, position.y, position.z );
        }
        inline void setOrientation( const Quaternion& ori )
        {
            mCameraNode->setOrientation( ori.w, ori.x, ori.y, ori.z );
        }
        void showBoundingBox( bool show ) { mNode->showBoundingBox(show); }

        inline void moveCameraRelative(float x, float y, float z)
        {
            mCameraNode->translate(Ogre::Vector3(x,y,z), Ogre::Node::TS_LOCAL);
        //       mCamera->moveRelative(Ogre::Vector3(x,y,z));
        }
        /// 1 deg =~ 0.0175 rad
        inline void yaw(float degree) { mCameraNode->yaw(Ogre::Degree(degree)); }
        inline void pitch(float degree) { mCameraNode->pitch(Ogre::Degree(degree)); }
        inline void roll(float degree) { mCameraNode->roll(Ogre::Degree(degree)); }
        inline void resetOrientation() { mCameraNode->resetOrientation(); }

        /// Return the library camera (for example Ogre::Camera*)
        Any getCamera() { return Any(mCamera); }

        virtual void _processMessage(const Message& message)
        {
            // TODO
        }

        /// This camera has only one mode
        void setCameraMode(int mode) {};

    protected:
        CameraComponentOGRE(const ComponentType& type, const ComponentType& family);
    };

    /**
     * Builder that is used to create the OGRE Camera Components based on a template.
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API CameraComponentTemplateOGRE : public ComponentTemplate
    {
    public:
        CameraComponentTemplateOGRE()
        {
            setFamily("Camera");        // NOTE: only in the base component of a family!
            // TODO create a list of types that must be implemented by any plugin
            setType("Camera");    // NOTE: Not CameraOGRE
        }
        /// Create the component
        virtual Component* createComponent()
        {
            CameraComponentOGRE* comp = new CameraComponentOGRE( getType(), getFamily() );
            return comp;
        }
    };

}
#endif
