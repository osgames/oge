/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_ARCHIVEOGRE_H__
#define __OGE_ARCHIVEOGRE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/filesystem/OgeArchive.h"
#include "oge/filesystem/OgeArchiveFactory.h"

#include "OgreArchive.h"
#include "OgreArchiveFactory.h"
#include "OgreDataStream.h"


namespace oge
{
    class ArchiveOGRE : public Ogre::Archive
    {
    private:
        mutable ArchivePtr mArchive;
    public:
        ArchiveOGRE(const Ogre::String& name);
        ~ArchiveOGRE();
        void load();
        void unload();
        bool isCaseSensitive(void) const;
// tmp
#if (OGRE_VERSION_MINOR < 7)
        Ogre::DataStreamPtr open(const Ogre::String& filename) const;
#else
        /*
         * @param readOnly Whether to open the file in read-only mode or not (note, 
         *         if the archive is read-only then this cannot be set to false)
         */
        Ogre::DataStreamPtr open(const Ogre::String& filename, bool readOnly) const;
#endif
        Ogre::StringVectorPtr list(bool recursive = true, bool dirs = false);
        Ogre::FileInfoListPtr listFileInfo(bool recursive = true, 
            bool dirs = false);
        Ogre::StringVectorPtr find(const Ogre::String& pattern, 
            bool recursive = true,bool dirs = false);
        bool exists(const Ogre::String& filename); 
        time_t getModifiedTime(const Ogre::String& filename); 
#if (OGRE_VERSION_MINOR <= 8)
        Ogre::FileInfoListPtr findFileInfo(const String& pattern, 
            bool recursive = true, bool dirs = false);
#else
        Ogre::FileInfoListPtr findFileInfo(const String& pattern, 
            bool recursive = true, bool dirs = false) const;
#endif
    };

    class ArchiveFactoryOGRE : public Ogre::ArchiveFactory
    {
    private:
        Ogre::String mType;
    public:
        ArchiveFactoryOGRE();
        const Ogre::String& getType() const;
        Ogre::Archive* createInstance(const Ogre::String& name);    
        void destroyInstance(Ogre::Archive* archive);    
    };
}

#endif // __OGE_ARCHIVEOGRE_H__
