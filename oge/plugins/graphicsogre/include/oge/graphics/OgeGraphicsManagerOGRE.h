/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_GRAPHICSMANAGEROGRE_H__
#define __OGE_GRAPHICSMANAGEROGRE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/config/OgeOptionManager.h"
#include "oge/scene/OgeSceneManagerEnumerator.h"
#include "oge/graphics/OgeCameraComponentOGRE.h"
#include "oge/graphics/OgeTextRendererOGRE.h"
#include "oge/graphics/OgeMeshData.h"
#include "oge/graphics/OgeProfilerOverlay.h"
#include "oge/OgeProfiler.h"

//#include "OgreRenderWindow.h"

// Forward declarations 
namespace Ogre
{
    class Root;
    class RenderSystem;
    class Camera;
    class RenderWindow; 
    class Viewport;
    class SceneManager;
};

namespace oge
{
    /**
     * Ogre based Graphics Manager
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API GraphicsManagerOGRE : public GraphicsManager, 
        public Singleton<GraphicsManagerOGRE>
    {
        friend class GraphicsSceneManagerOGRE;

    private:
        // Ogre specific
        bool mIsRendering;
        Ogre::Root* mOgreRoot;
        Ogre::RenderSystem* mRenderSystem;
        Ogre::SceneManager* mOgreSceneManager;
        Ogre::Viewport* mDefaultViewport;
        Ogre::Camera* mDefaultCamera;
        Ogre::RenderWindow* mRenderWindow;
        RenderTargetManager* mRenderTargetManager;
        ResourceControllerOGRE* mResourceController;
        ArchiveFactoryOGRE* mOgreArchiveFactory;

        // Graphics specific
        size_t mWindowHnd;

        // Config
        OptionPtr mGraphicsConfig;
        bool mCreateDefaultSceneManager;
        bool mCreateDefaultViewportAndCamera;

        // TODO temporary... I (Steven) must fix this
        // See _createRenderWindows (and shutdown)
        RenderWindowSettings* mRws;

        DebugOverlay* mDebugOverlay;
		ProfilerOverlay* mProfilerOverlay;
		Profiler* mProfiler;

    public:
        virtual ~GraphicsManagerOGRE();

        /// Used to create the manager
        static GraphicsManagerOGRE* createSingleton();
        static GraphicsManagerOGRE* getSingletonPtr();
        static GraphicsManagerOGRE& getSingleton();
        /// Used to destroy the manager
        static void destroySingleton();

        // ---------- System methods -------------------
        bool initialise();
        void shutdown();
        void tick(double currentTime);

        // -------- Graphics manager specific methods ------- 
        inline void setRendering(bool rendering) { mIsRendering = rendering; }

        // ---------- Ogre plugin specific methods ----------
        /**
         * @note Should be used with extreme caution
         */
        Ogre::Root* const getOgreRoot() { return mOgreRoot; }

        /**
         * Return true is a plugin is available for use
         * Those can be either oge or ogre plugins.
         *
         *   Ogre Plugin name                  Factory type name                 SceneTypeMask        World geometry support
         *   ----------------                  -----------------                 -------------        ----------------------
         *   not a plugin                      DefaultSceneManager               ST_GENERIC           false
         *   'BSP Scene Manager'               BspSceneManager                   ST_INTERIOR          true
         *   'Octree & Terrain Scene Manager'  OctreeSceneManager                0xFFFF               false
         *   'Octree & Terrain Scene Manager'  TerrainSceneManager               ST_EXTERIOR_CLOSE    true
         *   'PCZ Scene Manager'               PCZSceneManager                   0xFFFF               false
         *   '??'                              PagingLandScapeOctreeSceneManager ST_EXTERIOR_REAL_FAR false
         *   'Cg Program Manager'              ---
         *   'Particle FX'                     ---
         *   'D3D9 RenderSystem'               ---
         *   'GL RenderSystem'                 ---
         *
         * @param pluginName Name of the plugin type
         */
        bool isPluginAvailable(const String& pluginName);

        RenderTargetManager* getRenderTargetManager() { return mRenderTargetManager; }
		Ogre::RenderWindow* getRenderWindow() { return mRenderWindow; }

        Ogre::ParticleSystem* createParticleSystem(const String& id, 
            const String& templateName);
        void destroyParticleSystem(const String& id);
        void destroyParticleSystem(Ogre::ParticleSystem* ps);

        /*
         * Create a manual sphere mesh.
         *
         * Usage: createSphere( "SphereMesh", 100, 64, 64); // radius, nbrings, nbsegment
         * The mesh name must be unique
         * @reference (09.03.21) http://www.ogre3d.org/wiki/index.php/ManualSphereMeshes
         */
        void createSphereMesh(const String& name, const float radius,
            const int nbRings = 16, const int nbSegments = 16);
        /*
         * Create a manual cube mesh
         *
         * Usage: createCubeMesh( "CubeMesh" );
         * The mesh name must be unique
         * @reference (09.03.21) http://www.ogre3d.org/wiki/index.php/GeneratingAMesh
         *
         *  To display this cube within your Ogre application, you'll need to set up a material definition that looks like:
         *
         *  material Test/ColourTest
         *  {
         *      technique
         *      {
         *          pass
         *          {
         *              ambient vertexcolour
         *          }
         *      }
         *  }
         *
         *  Alternatively, the material can be created directly in C++:
         *
         *      MaterialPtr material = MaterialManager::getSingleton().create(
         *        "Test/ColourTest", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
         *      material->getTechnique(0)->getPass(0)->setVertexColourTracking(TVC_AMBIENT);
         *
         *  And to insert the mesh into a scene:
         *
         *      Entity* thisEntity = sceneManager->createEntity("cc", "ColourCube");
         *      thisEntity->setMaterialName("Test/ColourTest");
         *      SceneNode* thisSceneNode = sceneManager->getRootSceneNode()->createChildSceneNode();
         *      thisSceneNode->setPosition(-35, 0, 0);
         *      thisSceneNode->attachObject(thisEntity);
         *
         * @todo NEXT Convert into a createBoxMesh( const String& name, const Vector3& min, const Vector3& max)
         */
        void createCubeMesh(const String& name);

        /**
         * Display debug shapes (mostly used for the physical plugin)
         * @see drawDebugLine() and drawDebugContactPoint() to add lines and contact points
         */
        void setDebugLinesMode(bool mode);
        void drawDebugLine(const Vector3& from, const Vector3& to, const Vector3& color);
        void drawDebugContactPoint(const Vector3& point, const Vector3& normal,
            float distance, int lifeTime, const Vector3& color);

		void setProfilerOverlayVisible(bool visible);

    protected:
        /**
         * Get the mesh information for the given mesh.
         * @ref http://www.ogre3d.org/wiki/index.php/Raycasting_to_the_polygon_level
         * @note This code is better than this one: 
         *      http://www.ogre3d.org/wiki/index.php?title=RetrieveVertexData
         */
        void getMeshInformation( 
            const Ogre::MeshPtr mesh,
            size_t &vertex_count,
            Ogre::Vector3* &vertices,
            size_t &index_count,
            unsigned long* &indices,
            const Ogre::Vector3 &position,
            const Ogre::Quaternion &orient,
            const Ogre::Vector3 &scale);

    private:
        GraphicsManagerOGRE();

        bool setupResourceManagement();
        bool loadPlugins();
        void unloadAllPlugins();
        bool configureRenderer();
        Ogre::RenderSystem* findRenderSystem( const String& name );
        void configureRenderSystem();
        void initialiseRenderer();
        String convertOgeOptionToOgreOption(const String& ogeOption);


        // ----------- CLIENT METHODS -------------
        // Does methods have no use on a server
        /**
         * @note This method needs the ogre::root to check the availability
         *       of ogre scenemanager plugins. This means of course that 
         *       you can't create scene manager before. Seems obvious but
         *       because of the multi thread aspect of the engine you could
         *       have a thread trying to create them before ogre is fully intialised.
         */
        void createSceneManagement();
        void initRenderTargets();
        /** 
         * Create a render window
         * If a name is passed the config file will be parsed to find settings 
         * for this name. If no name is passed the first settings for 
         * a render window in the config file will be used. 
         * If no settings is found default values will be used.
         *
         * @param name Name of the render window. Must be unique. 
         * @param externalWindowHandle External window handle, for embedding OGE
         *     context Values: positive integer for W32 (HWND handle) 
         *     poslong:posint:poslong (display*:screen:windowHandle) or 
         *     poslong:posint:poslong:poslong (display*:screen:windowHandle:XVisualInfo*) 
         *     for GLX Default: 0 (None)
         * @param parentWindowHandle Parent window handle, for embedding the OGE
         *     context Values: positive integer for W32 (HWND handle) 
         *     poslong:posint:poslong for GLX (display*:screen:windowHandle) 
         *     Default: 0 (None)
         */
        void createRenderWindows(const String& name = "MainWindow", 
            const String& externalWindowHandle = "0", 
            const String& parentWindowHandle = "0");
        void createDefaultSceneManager();
        void createDefaultViewportAndCamera();

        Vector3 getRenderWindowSize() const;
        bool isFullscreen() const;

        /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        const String getLibrariesVersion();

        // ------------ Optional methods --------------------------
        /** 
         * Create a screenshot
         * @param filename Name of the resulting screenshot file. The extension 
         *     will determine the type of the generated file (png, bmp, jpg, ...)
         * @timestamped If true (default) the file will have its name timestamped
         *              in the format screenshot_MMDDYYY_HHMMSSmmm.xxx
         */
        virtual void saveScreenshot(const String& filename, bool timestamped = true);
        /// Toggle the debug overlay
        virtual void toggleDebugOverlay();

        /**
         * @todo wip Should it be in this calss?
         *       Make a generic one in GraphicsManager
         *       not dependend on the ogre library,
         *       and which doesn't load the file directly?
         * @todo A method that takes the mesh from the ogre mesh!
         *       Of course this means that the physics must wait 
         *       on the creation of the graphics component
         *
         * http://www.ogre3d.org/phpBB2/viewtopic.php?t=32268
         * http://www.ogre3d.org/phpBB2/viewtopic.php?t=32268&highlight=terrain+physics+collision
         * http://www.ogre3d.org/phpBB2/viewtopic.php?t=28282&highlight=terrain+physics+collision
         * http://www.saschawillems.de/?page_id=82
         * http://walaber.com/index.php?action=showitem&id=7
         * http://www.newtondynamics.com/downloads.html#
         */
        MeshData* createTerrainMeshData(String filename, 
            unsigned int step = 2, float heightMultiplier = 8);
        /**
         * Return the heightfield
         *
         * @param filename
         * @param ymin Used to return the lowest y
         * @param ymax Used to return the highest y
         * @param size Used to return the grid size (must be (2^N) + 1)
         * @param step (default 1)
         * @param xz Used to parse the image either xz or zx. NOTE BULLET NEEDS xz = false !
         * @param heightMultiplier Multiply the height by a factor
         * @param group Used to tell in which ressource group the file is.
         */
        float* getHeightfield(const String& filename, 
            float& ymin, float& ymax,  unsigned int& size,
            int step = 1, bool xz = false, float heightMultiplier = 8.0, 
            String group = "General");

    };
}
#endif // __OGE_GRAPHICSMANAGEROGRE_H__
