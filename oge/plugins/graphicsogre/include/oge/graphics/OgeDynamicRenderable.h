/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_DYNAMICRENDERABLE_H__
#define __OGE_DYNAMICRENDERABLE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/graphics/OgeDynamicRenderable.h"

#include "OgreSimpleRenderable.h"

namespace oge
{
    /**
     * Abstract base class providing mechanisms for dynamically growing hardware buffers.
     *
     * Allow for dynamically growing hardware buffers. Simply put, you tell on update 
     * how many vertices/indices you want, and it reallocates the buffers (only) if necessary.
     * It's quite flexible, as you can create your own vertex declaration etc.
     * It automatically decrease the size when there's excessive capacity.
     *
     * @see  DynamicLines sub-class
     *
     * @doc Based on http://www.ogre3d.org/wiki/index.php/DynamicGrowingBuffers
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API DynamicRenderable : public Ogre::SimpleRenderable
    {
    protected:
        /// Maximum capacity of the currently allocated vertex buffer.
        size_t mVertexBufferCapacity;
        /// Maximum capacity of the currently allocated index buffer.
        size_t mIndexBufferCapacity;

    public:
        /// Constructor
        DynamicRenderable();
        /// Virtual destructor
        virtual ~DynamicRenderable();

        /** Initializes the dynamic renderable.
         *
         * @remarks This function should only be called once. 
         * It initializes the render operation, and calls the abstract function
         * createVertexDeclaration().
         *
         * @param operationType The type of render operation to perform.
         * @param useIndices Specifies whether to use indices to determine the
         *        vertices to use as input. 
         */
        void initialize(Ogre::RenderOperation::OperationType operationType,
            bool useIndices);

        /// Implementation of Ogre::SimpleRenderable
        virtual Ogre::Real getBoundingRadius() const;
        /// Implementation of Ogre::SimpleRenderable
        virtual Ogre::Real getSquaredViewDepth(const Ogre::Camera* cam) const;

    protected:
        /**
         * Creates the vertex declaration.
         * @remarks Override and set mRenderOp.vertexData->vertexDeclaration here.
         * mRenderOp.vertexData will be created for you before this method
         * is called.
         */
        virtual void createVertexDeclaration() = 0;

        /**
         * Prepares the hardware buffers for the requested vertex and index counts.
         *
         * @remarks This function must be called before locking the buffers in
         * fillHardwareBuffers(). It guarantees that the hardware buffers
         * are large enough to hold at least the requested number of
         * vertices and indices (if using indices). The buffers are
         * possibly reallocated to achieve this.
         * @par The vertex and index count in the render operation are set to
         *      the values of vertexCount and indexCount respectively.
         * @param vertexCount The number of vertices the buffer must hold.
         * @param indexCount The number of indices the buffer must hold. This
         *        parameter is ignored if not using indices.
         */
        void prepareHardwareBuffers(size_t vertexCount, size_t indexCount);

        /**
         * Fills the hardware vertex and index buffers with data.
         *
         * @remarks This function must call prepareHardwareBuffers() 
         * before locking the buffers to ensure the they are large enough 
         * for the data to be written. Afterwards the vertex and index buffers
         * (if using indices) can be locked, and data can be written to them. 
         */
        virtual void fillHardwareBuffers() = 0;
    };
}
#endif
