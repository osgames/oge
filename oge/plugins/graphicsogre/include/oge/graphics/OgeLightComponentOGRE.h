/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_LIGHTCOMPONENTOGRE_H__
#define __OGE_LIGHTCOMPONENTOGRE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/OgeString.h"
#include "oge/graphics/OgeLightComponent.h"
#include "oge/math/OgeVector3.h"

namespace oge
{
    /**  
     * This component type permits to add a light
     *
     * The type of light exist: "Point", "Directional" and "SpotLight"
     * and it is set via the message type INIT_LIGHT_TYPE
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API LightComponentOGRE : public LightComponent
    {
        friend class LightComponentTemplateOGRE;
    protected:
        Ogre::SceneNode* mNode;
        Ogre::Light*     mLight;

        float   mIntensity;
        Vector3 mDiffuse;
        Vector3 mSpecular;

    public:
        virtual ~LightComponentOGRE()
        {
        }

        /// (Re)set the default values
        virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
        virtual void _destroy(SceneManager* sceneManager);
        virtual void _activate(bool activate, SceneManager* sceneManager);
        virtual UpdateRate _getUpdateRate() const;
        virtual void _update(double deltaTime);

        virtual void _processMessage(const Message& message);

        // ------- Similar methods to the Graphics Family ----------
        //virtual Vector3 getDimensions() const;
        virtual void showBoundingBox(bool show);

        inline Ogre::SceneNode* _getNode() { return mNode; }
        inline Ogre::Light* _getLight() { return mLight; }

        /// @note Should not be used directly. This method doesn't attach the light to the node!
        void _setNode(Ogre::SceneNode* node) {mNode = node; }
        /// @note Should not be used directly. This method doesn't attach the light to the node!
        inline void _setLight(Ogre::Light* light) { mLight = light; }

        // ------- Specific method to the Light Family ----------


    protected:
        inline LightComponentOGRE(const ComponentType& type, const ComponentType& family)
            : LightComponent(type, family)
            , mNode(0), mLight(0),
            mIntensity(0), mDiffuse(0,0,0), mSpecular(0,0,0)
        {}

        void _setPositionByMessage(const Message& message);
        void _setScaleByMessage(const Message& message);
        void _setOrientationByMessage(const Message& message);
        void _setPositionOrientationByMessage(const Message& message);
        //_ void _setDimensionsByMessage(const Message& message);
        void _setIntensityByMessage(const Message& message);
        void _setDiffuseColourByMessage(const Message& message);
        void _setSpecularColourByMessage(const Message& message);

        void resetColours();
    };

    /**
     * Builder that is used to create the OGRE Light Components based on a template.
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API LightComponentTemplateOGRE : public ComponentTemplate
    {
    public:
        LightComponentTemplateOGRE() 
        {
            setFamily("Light");           // NOTE: only in the base component of a family!
            // TODO create a list of types that must be implemented
            setType("Light");     // NOTE: Not LightOGRE
        }
        /// Create the component
        virtual Component* createComponent()
        {
            return new LightComponentOGRE( getType(), getFamily() );
        }
    };

}
#endif
