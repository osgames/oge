/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_GRAPHICSSCENEMANAGEROGRE_H__
#define __OGE_GRAPHICSSCENEMANAGEROGRE_H__

#include "oge/graphics/OgeGraphicsPrerequisites.h"
#include "oge/graphics/OgeGraphicsSceneManager.h"
#include "oge/scene/OgeSceneManager.h"
#include "oge/task/OgeTaskHandler.h"
#include "oge/graphics/OgeDynamicLines.h"
//#include "oge/OgeAsyncFunction.h"

#include "OgrePrerequisites.h"

namespace oge
{
    /**
     * Default graphical scene manager
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API GraphicsSceneManagerOGRE : public GraphicsSceneManager
    {
    protected:
        Ogre::SceneManager* mOgreSceneManager;
    //    ObjectId nameOfLastCamera;
        Ogre::Camera* mCamera;

        /// WIP Orphaned node used to create/destroy node and objects
        Ogre::SceneNode* mOrphanedSceneNode;

        // Expensive to create
        Ogre::RaySceneQuery* mRaySceneQuery;
        Ogre::RaySceneQuery* mRaySceneQuery2;
        Ogre::RaySceneQuery* mRaySceneQuery3;
        Ogre::RaySceneQuery* mRaySceneQuery4;

        //tbb::concurrent_queue<Task*> mPendingTasks;
        TaskHandler mPendingTasks;

        /// Used to draw debug lines (such as Physics Shapes & Contact point)
        DynamicLines* mDebugLines;
        Ogre::SceneNode* mDebugNode; // created in the (Terrain)GraphicsSceneManager::init() methods
        bool mDebugOn;

    public:
        GraphicsSceneManagerOGRE(const String& name);
        virtual ~GraphicsSceneManagerOGRE();

        // ------------- SceneManager methods --------------------------
        virtual void init();
        virtual void initMetaData() const;
        void update(double deltaTime);
        void clearScene();

        // ------------- Graphics SceneManager methods ------------------
       /**
        * Set the active camera
        * Of course this means the object must contain a CameraComponent
        */
       virtual void setActiveCamera(const ObjectId& camera);

        /**
         * Clamp the position passed as pointer to the terrain
         * Of course this method will do something only 
         * if there is a terrain below the point.
         *
         * @param offset Offset the position this prevents the caracter to be BELOW the terrain
         *
         * @note Expensive method should be used with parcimony... 1/10 steps?
         */
        virtual void clampToTerrain(Vector3* position, float offset = 100.0f);

        /**
         * Returns a new Vector3 that must be deleted and 0 if the query didn't
         * hit anything
         * x and y are fractions of the screen size
         *
         * @todo Add parameters: like findTerrain, excludeTerrain, sortByDistance, ...
         * @todo Merge common code of doCameraRayQuery & doObjectIdRayQuery?
         */
        Vector3* doCameraRayQuery(Real x, Real y, const unsigned int queryMask = 0xFFFFFFFF);
        String doObjectIdRayQuery(float x, float y, const unsigned int queryMask = 0xFFFFFFFF);

		/**
		* This little snippet gets the screenspace bounds for a MovableObject
		*
		* @param   object   The object to retrieve the coordidnates of.
		* @param   camera   The active camera
		* @param   min      The min coords
		* @param   max      The max coords
		*
		* @return   Returns true if the object is visible and the coordinates were
		*         retrieved, false otherwise.
		*/
		bool getScreenspaceBounds(Ogre::MovableObject* object, Vector2& min, Vector2& max);

		/**
		* This little snippet gets the screenspace coordinates for a MovableObject
		*
		* @param   object   The object to retrieve the coordidnates of.
		* @param   camera   The active camera
		* @param   result   The Vector2 to store the result in
		*
		* @return   Returns true if the object is visible and the coordinates were
		*         retrieved, false otherwise.
		*/
		bool getScreenspaceCoords(Ogre::MovableObject* object, Vector2& result);

        /**
         * Raycast from a point in the scene. 
         * On success the point is returned in the result.
         *
         * This method was modified from MOC - Minimal Ogre Collision v 1.0 beta - http://sourceforge.net/projects/moc-collision/
         * pulblised under the MIT License
         * Copyright (c) 2008 MouseVolcano (Thomas Gradl, Esa Kylli, Erik Biermann, Karolina Sefyrin)
         */
        bool raycastFromPoint(const Vector3& from, const Vector3& normal,
            Vector3& result, unsigned long& target, float& closestDistance,
            const unsigned int queryMask = 0xFFFFFFFF);

        /**
         * Return true if collides with an entity
         *
         * @reference Modified from DOC on the ogre forum
         */
        bool collidesWithEntity(const Vector3& fromPoint, const Vector3& toPoint, 
            float padding = 3.5f, const unsigned int queryMask = 0xFFFFFFFF);

        // ------------- Ogre specific methods --------------------------
        Ogre::Camera* getActiveOgreCamera() { return mCamera; }
        Ogre::SceneNode* createSceneNode(const String& name);
        Ogre::Entity* createEntity(const String& name, const String& meshName);
        Ogre::SceneManager* getOgreSceneManager() { return mOgreSceneManager; }

        /**
         * Sets the source of the 'world' geometry, i.e. the large,
         * mainly static geometry making up the world e.g. rooms, landscape etc.
         * @note This method in the basic GraphicsSceneMangerOGRE does nothing.
         * @returns An asynchronous bool - true in case of success
         */
        virtual AsyncBool setWorldGeometry(String filename) { return AsyncBool(true); }

        /// Attach the DynamicLine to a scene node and set the material
        void setupDebugLines();
        void setDebugLinesMode(bool mode);
        void drawDebugLine(const Vector3& from, const Vector3& to, const Vector3& color);
        void drawDebugContactPoint(const Vector3& point, const Vector3& normal,
            float distance, int lifeTime, const Vector3& color);
        bool getDebugLinesMode() { return mDebugOn; }

		void logProfileInfo(double currentTime);
    };

    //-------------------------------------------------------------------------

    /**
     * Factory instanciating the scene manager
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API GraphicsSceneManagerFactoryOGRE : public SceneManagerFactory
    {
    public:
        GraphicsSceneManagerFactoryOGRE();
        virtual ~GraphicsSceneManagerFactoryOGRE();

        virtual SceneManager* createInstance(const String& name);
        virtual bool checkAvailability();
    };

    //-------------------------------------------------------------------------

    /**
     * Terrain graphical scene manager
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API TerrainGraphicsSceneManager : public GraphicsSceneManagerOGRE
    {
    public:
        TerrainGraphicsSceneManager(const String& name);
        virtual ~TerrainGraphicsSceneManager();

        virtual void init();
        virtual void initMetaData() const;

        /**
         * Sets the source of the 'world' geometry, i.e. the large,
         * mainly static geometry making up the world e.g. rooms, landscape etc.
         * @note This method in the basic GraphicsSceneMangerOGRE does nothing.
         * @returns An asynchronous bool - true in case of success
         */
        virtual AsyncBool setWorldGeometry(String filename);

    private:
        /**
         * Sets the source of the 'world' geometry, i.e. the large,
         * mainly static geometry making up the world e.g. rooms, landscape etc.
         * @note This method should not be called directly but with setWorldGeometry()
         * @returns An asynchronous bool - true in case of success
         */
        virtual bool _setWorldGeometry(String filename); 
    };

    //-------------------------------------------------------------------------

    /**
     * Factory instanciating the scene manager
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API TerrainGraphicsSceneManagerFactory : public SceneManagerFactory
    {
    public:
        TerrainGraphicsSceneManagerFactory();

        virtual SceneManager* createInstance(const String& name);
        virtual bool checkAvailability();
    };

    //-------------------------------------------------------------------------

    /**
     * PCZ graphical scene manager
     * Scene manager based on the "Portal Connected Zone SM"
     *
     * @doc 
     * http://www.ogre3d.org/addonforums/viewforum.php?f=22
     * http://www.ogre3d.org/phpBB2/viewtopic.php?p=272679#272679
     * http://www.ogre3d.org/wiki/index.php/Portal_Connected_Zone_Scene_Manager
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API PCZGraphicsSceneManager : public GraphicsSceneManagerOGRE
    {
    public:
        PCZGraphicsSceneManager(const String& name);
        virtual ~PCZGraphicsSceneManager();

        virtual void init();
        virtual void initMetaData() const;

        /**
         * Sets the source of the 'world' geometry, i.e. the large,
         * mainly static geometry making up the world e.g. rooms, landscape etc.
         * @note This method in the basic GraphicsSceneMangerOGRE does nothing.
         * @returns An asynchronous bool - true in case of success
         */
        virtual AsyncBool setWorldGeometry(String filename);

    private:
        /**
         * Sets the source of the 'world' geometry, i.e. the large,
         * mainly static geometry making up the world e.g. rooms, landscape etc.
         * @note This method should not be called directly but with setWorldGeometry()
         * @returns An asynchronous bool - true in case of success
         */
        virtual bool _setWorldGeometry(String filename); 
    };

    //-------------------------------------------------------------------------

    /**
     * Factory instanciating the PCZ scene manager
     * @author Steven 'lazalong' Gay
     */
    class OGE_GRAPHICS_API PCZGraphicsSceneManagerFactory : public SceneManagerFactory
    {
    public:
        PCZGraphicsSceneManagerFactory();

        virtual SceneManager* createInstance(const String& name);
        virtual bool checkAvailability();
    };

} // namespace
#endif // __OGE_GRAPHICSSCENEMANAGEROGRE_H__
