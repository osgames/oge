/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeGraphicsManagerOGRE.h"
#include "oge/graphics/OgeGraphicsComponentOGRE.h"
#include "oge/graphics/OgeCameraComponentOGRE.h"
#include "oge/graphics/OgeExtendedCameraComponentOGRE.h"
#include "oge/graphics/OgeAnimationComponentOGRE.h"
#include "oge/graphics/resource/OgeResourceControllerOGRE.h"
#include "oge/graphics/resource/OgeArchiveOGRE.h"
#include "oge/config/OgeOptionManager.h"
#include "oge/graphics/OgeRenderTargetManager.h"
#include "oge/input/OgeInputManager.h"
#include "oge/graphics/OgeGraphicsSceneManagerOGRE.h"
#include "oge/graphics/OgeDebugOverlay.h"
#include "oge/math/OgeVector3.h"
#include "oge/object/OgeObjectManager.h"
#include "oge/filesystem/OgeFileSystem.h"
#include "oge/gui/OgeGuiManager.h"
#include "oge/graphics/OgeLabelComponentOGRE.h"
#include "oge/graphics/OgeLightComponentOGRE.h"
#include "oge/engine/OgeEngineManager.h"
#include "oge/logging/OgeGuiConsoleLogger.h"
#include "oge/OgeProfiler.h"
#include "Ogre.h"
#include "OgrePlugin.h"

namespace oge
{
    //------------------------------------------------------------------------
    template<> GraphicsManagerOGRE* Singleton<GraphicsManagerOGRE>::mSingleton = 0;
    GraphicsManagerOGRE* GraphicsManagerOGRE::getSingletonPtr() 
    { 
        return mSingleton; 
    }
    GraphicsManagerOGRE& GraphicsManagerOGRE::getSingleton()
    {
        assert(mSingleton);
        return *mSingleton;
    }    
    //------------------------------------------------------------------------
    GraphicsManagerOGRE* GraphicsManagerOGRE::createSingleton()
    {
        if (mSingleton == 0)
            mSingleton = new GraphicsManagerOGRE();
        return mSingleton;
    }
    //------------------------------------------------------------------------
    void GraphicsManagerOGRE::destroySingleton()
    {
        if (mSingleton != 0)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //-------------------------------------------------------------------------
    GraphicsManagerOGRE::GraphicsManagerOGRE() : 
        GraphicsManager("GraphicsManager"), // Not GraphicsManagerOGRE as we -for now- want only one manager of each type
        mIsRendering(true),
        mOgreRoot(0),
        mRenderSystem(0),
        mOgreSceneManager(0),
        mDefaultViewport(0),
        mDefaultCamera(0),
        mRenderWindow(0),
        mRenderTargetManager(0),
        mResourceController(0),
        mOgreArchiveFactory(0),
        mWindowHnd(0),
        mCreateDefaultSceneManager(true),
        mCreateDefaultViewportAndCamera(true),
        mRws(0),
        mDebugOverlay(0),
		mProfilerOverlay(0),
		mProfiler(0)
    {
		mProfiler = Profiler::getSingletonPtr();
        LOG("GraphicsManagerOGRE created");
//TODO: TO REMOVE??? Should instead redirect the output into cout too       std::cout << "===== GraphicsManagerOGRE created ======" << std::endl;
    }
    //-------------------------------------------------------------------------
    GraphicsManagerOGRE::~GraphicsManagerOGRE()
    {
        LOG("Destroying GraphicsManagerOGRE...");
        // NEXT: Remove this temporary cout... but very useful for quick debug
        std::cout << "===== GraphicsManagerOGRE destroyed ======" << std::endl;

        // Destroying again the ogreroot because sometimes the shutdown can miss
        if (mOgreRoot)
        {
            LOG("Destroying ogre...");
            delete mOgreRoot;
            mOgreRoot = 0;
        }

		if(mProfilerOverlay) {
			delete mProfilerOverlay;
		}

        LOG("... GraphicsManagerOGRE destroyed.");
    }
    //-------------------------------------------------------------------------
    bool GraphicsManagerOGRE::initialise()
    {
        LOG("GraphicsManagerOGRE initialise");
        // NEXT: Remove this temporary cout... but very useful for quick debug
        std::cout << "===== GraphicsManagerOGRE initialise ======" << std::endl;



        if (mOgreRoot)
            return false;
        
        // Get the ogre config
        mGraphicsConfig = OptionManager::getSingletonPtr()->getOption("oge.engine.graphics");
        if (!mGraphicsConfig)
        {
            LOG("=== No graphics config could be found: Using build-in values. ===");
        }

        mOgreRoot = new Ogre::Root("","","Ogre.log"); // TODO log name for server in order to run server + client in same folder

        setupResourceManagement();
        loadPlugins();
        configureRenderer();
        initialiseRenderer();

        createSceneManagement();

        mRenderTargetManager = new RenderTargetManager(mOgreRoot);
        initRenderTargets();

        // TODO HACK for oged? How could we avoid this?
        if (EngineManager::getSingletonPtr()->getExternalWindowHandle() != "")
            createRenderWindows("Main Window", EngineManager::getSingletonPtr()->getExternalWindowHandle());
        else
            createRenderWindows(); // Create the primary render window

        if (mCreateDefaultSceneManager) // NEXT can be set by config?
            createDefaultSceneManager();

        // Cannot be called before a primary render window was created.
        // TODO mResourceGroupManager->initialiseResourceGroup("Bootstrap");
        Ogre::ResourceGroupManager& resources = Ogre::ResourceGroupManager::getSingleton();
        resources.initialiseAllResourceGroups();

        // Create basic default Debug Overlay
        mDebugOverlay = new DebugOverlay(mOgreRoot, mRenderWindow);
        if (mGraphicsConfig)
        {
            // Note: if the param is not found it will be false
            bool debugoverlay = StringUtil::toBool(
                mGraphicsConfig->getOptionValue("rendering.DebugOverlay" ) );
            mDebugOverlay->showOverlay( debugoverlay );
        }
     
        // TODO should only be done if a window exist?
        mRenderWindow->getCustomAttribute("WINDOW", &mWindowHnd);

        LOGEC( mWindowHnd == 0, "Window handle was not created!!!");

#if OGE_PLATFORM == OGE_PLATFORM_WIN32
		Logger *l = LogManager::getSingleton().getLogger(0);
		if(l && l->getType() == LogManager::LOGTYPE_GUICONSOLE) {
			GuiConsoleLogger *gl = (GuiConsoleLogger *)l;
			mRenderWindow->getCustomAttribute("WINDOW", &gl->hWnd);
		}
#endif

        // Initialise the gui manager: can only be done when ogre is running!
        if (GuiManager::getManager())
        {
            LOGI("Sending render window to gui mgr.");
			Ogre::Viewport *vp = mRenderWindow->getViewport(0);
            GuiManager::getManager()->setRenderWindow( (void *) mRenderWindow );
        }
        else
        {
            LOGI("The GuiManager was not created: nothing to send to it.");
        }


        // Gives to the inputManager the windowhandle he needs
        if (InputManager::getManager())
        {
            InputManager::getManager()->setFullscreen(mRenderWindow->isFullScreen());
            InputManager::getManager()->setWindowHandle( mWindowHnd );
        }
        else
        {
            LOGI("The InputManager was not created: nothing to send to it.");
        }

        // Register the templates
        ObjectManager* objectMgr = ObjectManager::getSingletonPtr();
        objectMgr->registerComponentTemplate( "Graphics", new GraphicsComponentTemplateOGRE() );
        objectMgr->registerComponentTemplate( "Graphics", new CameraComponentTemplateOGRE() );
        objectMgr->registerComponentTemplate( "Graphics", new ExtendedCameraComponentTemplateOGRE() );
        objectMgr->registerComponentTemplate( "Graphics", new AnimationComponentTemplateOGRE() );
        objectMgr->registerComponentTemplate( "Graphics", new LabelComponentTemplateOGRE() );
        objectMgr->registerComponentTemplate( "Graphics", new LightComponentTemplateOGRE() );

        // Override the dummy instance created in GraphicsManager constructor
        if (mTextRenderer)
            delete mTextRenderer;
        mTextRenderer = new TextRendererOGRE();

        // NOTE If we don't call GraphicsManager::initialise()
        //      we need to do this:
        if (!System::initialise())
            return false;

        return true;
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::shutdown()
    {
        LOG("Shutting down the graphics manager...");
        // NEXT: Remove this temporary cout... but very useful for quick debug
        std::cout << "===== GraphicsManagerOGRE shutdown ======" << std::endl;

        System::shutdown();

        try {
            // No! The default sm should be destroyed by the ObjectSceneManager
            //delete mSceneManager; mSceneManager = 0;

            delete mRenderTargetManager;
            mRenderTargetManager = 0;

            if (mDebugOverlay)
            {
                LOGI("Deleting mDEBUG OVERLAY");
                delete mDebugOverlay;
                mDebugOverlay = 0;
            }

    /* TODO needed?
            if(mResourceGroupManager)
            {
                BackgroundRequestTicketPtr ticket =
                    BackgroundResourceManager::getSingletonPtr()->
                    unregisterResourceGroupManager(mResourceGroupManager, mResourceGroupManager);

                while (!ticket->getCompleted())
                {
                    boost::thread::yield();
                }
                delete mResourceGroupManager;
            }
    */

            FileSystem::getSingletonPtr()->removeListener(mResourceController);
            mResourceController->notifyAllArchiveGroupsRemoved();
            delete mResourceController;

            if (mOgreRoot)
            {
                // NEXT Temporary see below in createRenderWindow
                Ogre::WindowEventUtilities::removeWindowEventListener(mRenderWindow, mRws);
                delete mRws;

                LOGI("Ogre root present: deleting...");
                unloadAllPlugins(); // This shouldn't be necessary
                delete mOgreRoot;
                LOGI("Ogre root deleted.");
                mOgreRoot = 0;

                // Ogre Root has been deleted, its defiantly safe to delete this
                // factory
                delete mOgreArchiveFactory;

                if (mWindowHnd)
                {
                    if (InputManager::getManager())
                        InputManager::getManager()->clearWindowHandle();
                    mWindowHnd = 0;
                }
                mRenderWindow = 0;
                mWindowHnd = 0;
                mRenderSystem = 0;
                mDefaultCamera = 0;
            }

    /*
            if (mArchiveFactory)
            {
                // No way to remove the archive factory from OGRE, so delete after
                // OgreRoot has been deleted
                delete mArchiveFactory;
            }
            // Release the graphics config so it can be unloaded if necessary
            mGraphicsConfig->reset();
    */
            LOG("Graphics manager shutting down done.");
        }
        catch(...)
        {
            LOGE("Shutdown error.");
        }
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::tick(double currentTime)
    {
		OgeProfileGroup("Graphics OGRE", OGEPROF_SYSTEMS);

        Ogre::WindowEventUtilities::messagePump();
        if (mIsRendering) // WIP can we avoid this?
                mOgreRoot->renderOneFrame();
        mDebugOverlay->updateStatistics();
		
		if(mProfiler->getEnabled()) {
			mProfilerOverlay->displayResults();
		}

		// report position for every component
		//((GraphicsSceneManagerOGRE*)mSceneManager)->logProfileInfo(currentTime);
    }
    //-------------------------------------------------------------------------
    bool GraphicsManagerOGRE::isPluginAvailable(const String& pluginName)
    {
#if (OGRE_VERSION_MINOR < 7)
        typedef std::vector<Ogre::Plugin*> PluginInstanceList;
        PluginInstanceList list = mOgreRoot->getInstalledPlugins();

        for (PluginInstanceList::iterator i = list.begin(); i != list.end(); ++i)
#else
        typedef Ogre::vector<Ogre::Plugin*>::type PluginInstanceList; // see ogeroot.h
        const PluginInstanceList& list = mOgreRoot->getInstalledPlugins();
        for (PluginInstanceList::const_iterator i = list.begin(); i != list.end(); ++i)
#endif
        {
            String type = (*i)->getName();
            if (type == pluginName)
                return true;
        }
        return false;
    }
    //-------------------------------------------------------------------------
    bool GraphicsManagerOGRE::setupResourceManagement()
    {
        mOgreArchiveFactory = new ArchiveFactoryOGRE();
        mResourceController = new ResourceControllerOGRE();

        Ogre::ArchiveManager::getSingleton().
            addArchiveFactory(mOgreArchiveFactory);
        
        FileSystem::getSingletonPtr()->addListener(mResourceController);

        ArchiveGroupPtr groups = FileSystem::getSingletonPtr()->
            getArchiveGroups().cast<ArchiveGroup>();

        StringVector names = groups->getArchiveNames();
        size_t count = names.size();
        for (size_t index = 0; index < count; ++index)
        {
            mResourceController->notifyArchiveGroupAdded(names[index]);
        }


        /*
        // TODO Those are "all" locations not only those for graphics
                //  we should use the new methods created by Chris
        
                OptionPtr option = OptionManager::getSingletonPtr()
                            ->getOption("oge.graphicsystem");
                
                option = OptionManager::getSingleton().
                    getOption("oge.graphicsystem");
        
                Ogre::ResourceGroupManager& resources = Ogre::ResourceGroupManager::getSingleton();
        
                Option::Iterator iter = option->getOptionIterator();
                for (iter.begin(); iter.hasNext(); iter.next())
                {
                    // Add filesystem to the default OGRE group
                    if (iter.getName() == "archive")
                    {
                        Option::ValueIterator valueiter = iter.getOption()->getValueIterator();
                        for (valueiter.begin(); valueiter.hasNext(); valueiter.next())
                        {
                            String location = valueiter.getValue();
                            LOG(String("Add archive '") + location + "' to group 'general'");
        
                            if (location.find(".zip") != String::npos)
                                resources.addResourceLocation(location, "Zip");
                            else
                                resources.addResourceLocation(location, "FileSystem");
                        }
                    }
                    // Add filesystem to a 'virtual location' aka OGRE group
                    else if (iter.getName() == "group")
                    {
                        Option::Iterator suboptioniter = iter.getOption()->getOptionIterator();
                        for (suboptioniter.begin(); suboptioniter.hasNext(); suboptioniter.next())
                        {
                            String group = suboptioniter.getName();
        
                            if (group.empty())
                                group = "General";
        
                            LOG(String("Setup group: ") + group);
        
                            OptionPtr op = suboptioniter.getOption()->getOption("archive");
                            if (op)
                            {
                                Option::ValueIterator valueiter = op->getValueIterator();
                                for (valueiter.begin(); valueiter.hasNext(); valueiter.next())
                                {
                                    String location = valueiter.getValue();
                                    LOG(String(" - Add archive '") + location + "' to group '"+group +"'");
        
                                     if (location.find(".zip") != String::npos)
                                        resources.addResourceLocation(location, "Zip", group);
                                    else
                                        resources.addResourceLocation(location, "FileSystem", group);
                                }
                            }
                            else
                            {
                                LOGE("Malformed xml: should have found an 'archive' tag.");
                            }
                        }
                    }
                    else
                    {
                        LOGW(String("Unusable tag:") + iter.getName());
                    }
                }*/
        
        

        return true;
    }
    //-------------------------------------------------------------------------
    bool GraphicsManagerOGRE::loadPlugins()
    {
        LOGI("Loading Ogre plugins...");

        if (!mGraphicsConfig)
        {
        // TODO linux mac #if (OGE_PLATFORM == OGE_PLATFORM_WIN32)
        #ifdef _DEBUG
            mOgreRoot->loadPlugin("Plugin_BSPSceneManager_d");
            mOgreRoot->loadPlugin("Plugin_CgProgramManager_d");
            mOgreRoot->loadPlugin("Plugin_OctreeSceneManager_d");
            mOgreRoot->loadPlugin("Plugin_OctreeZone_d");
            mOgreRoot->loadPlugin("Plugin_ParticleFX_d");
            mOgreRoot->loadPlugin("Plugin_PCZSceneManager_d");
            mOgreRoot->loadPlugin("RenderSystem_Direct3D9_d");
            mOgreRoot->loadPlugin("RenderSystem_GL_d");
        #else
            mOgreRoot->loadPlugin("Plugin_BSPSceneManager");
            mOgreRoot->loadPlugin("Plugin_CgProgramManager");
            mOgreRoot->loadPlugin("Plugin_OctreeSceneManager");
            mOgreRoot->loadPlugin("Plugin_OctreeZone");
            mOgreRoot->loadPlugin("Plugin_ParticleFX");
            mOgreRoot->loadPlugin("Plugin_PCZSceneManager");
            mOgreRoot->loadPlugin("RenderSystem_Direct3D9");
            mOgreRoot->loadPlugin("RenderSystem_GL");
        #endif
        }
        else
        {
        #ifdef _DEBUG
            OptionPtr plugins = mGraphicsConfig->getOption("plugins.Debug.plugin");      
        #else
            OptionPtr plugins = mGraphicsConfig->getOption("plugins.Release.plugin");      
        #endif

            Option::ValueIterator iter = plugins->getValueIterator();
            for (iter.begin(); iter.hasNext(); iter.next())
            {
                try
                {
                    LOG(String("Load plugin in Ogre: '")+iter.getValue()+"'");
                    mOgreRoot->loadPlugin(iter.getValue());
                }
                catch (...)
                {
                    LOGE("Error while trying to load the OGRE plugin: "+
                        iter.getValue() );
                }
            }
        }

        return true;
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::unloadAllPlugins()
    {
        LOGI("Unload all Ogre plugins...");
        Ogre::Root::PluginInstanceList list = mOgreRoot->getInstalledPlugins();
        for (size_t i=0; i<list.size(); i++)
        {
            mOgreRoot->unloadPlugin( list[i]->getName() );
        }
    }
    //-------------------------------------------------------------------------
    bool GraphicsManagerOGRE::configureRenderer()
    {
        LOGI("Configuring Ogre renderer...");
        
        Ogre::RenderSystem* renderSystem;

        if (!mGraphicsConfig) // No config
        {
            #if (OGE_PLATFORM == OGE_PLATFORM_WIN32)
                renderSystem = findRenderSystem( "Direct3D9 Rendering Subsystem" );
            #else
                renderSystem = findRenderSystem( "OpenGL Rendering Subsystem" );
            #endif
        }
        else
        {
            LOG(String("Render system selected: '")+ 
                mGraphicsConfig->getOption("rendersystem")
                ->getOptionValue("selected") + "'");

            renderSystem = findRenderSystem( 
                mGraphicsConfig->getOption("rendersystem")
                ->getOptionValue("selected") 
                + " Rendering Subsystem");
        }

        if (!renderSystem)
        {
            LOGE("The selected render system does not exist!");
            return false;
        }

        mRenderSystem = renderSystem;
        mOgreRoot->setRenderSystem(renderSystem);

        configureRenderSystem();

        return true;
    }
    //-------------------------------------------------------------------------
    Ogre::RenderSystem* GraphicsManagerOGRE::findRenderSystem(const String& name)
    {
    #if ( OGRE_VERSION_MINOR < 7 ) // need for 1.6.2
        Ogre::RenderSystemList* renderSystemList = mOgreRoot->getAvailableRenderers();
        Ogre::RenderSystemList::iterator iter = renderSystemList->begin();
        for (; iter != renderSystemList->end(); iter++)
        {
            if (name == (*iter)->getName())
            {
                return *iter;
            }
        }
    #else
        const Ogre::RenderSystemList& renderSystemList = mOgreRoot->getAvailableRenderers();
        Ogre::RenderSystemList::const_iterator iter = renderSystemList.begin();
        for (; iter != renderSystemList.end(); iter++)
        {
            if (name == (*iter)->getName())
            {
                return *iter;
            }
        }
    #endif
        return 0;
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::configureRenderSystem()
    {
        LOGI("Configuring the Render System");

        String selected;
        OptionPtr config;
        if (mGraphicsConfig)
        {
            selected = mGraphicsConfig->getOption("rendersystem")
                ->getOptionValue("selected");

            if (selected.empty())
                selected = "OpenGL"; // NEXT This as a global param?

            // <rendersystem name="OpenGL"> or <rendersystem name="Direct3D9">
            config = mGraphicsConfig->getOption( 
                String("rendersystem.") + selected );
        }

        String videoMode;
        String floatingPointMode;
        String rttPreferedMode;
        String resolutionWidth, resolutionHeight, colourDepth;

        // Default values
        if (config)
        {
            resolutionWidth = config->getOptionValue("Width");
            resolutionHeight = config->getOptionValue("Height");
            colourDepth = config->getOptionValue("ColourDepth");
            videoMode = resolutionWidth + " x " + resolutionHeight + " @ " +
                        colourDepth + "-bit colour";
            mRenderSystem->setConfigOption("Video Mode", videoMode);
            Ogre::ConfigOptionMap& renderSystemConfigOptions =
                mRenderSystem->getConfigOptions();

            if (renderSystemConfigOptions.find("Floating-point mode") !=
                renderSystemConfigOptions.end())
            {
                mRenderSystem->setConfigOption("Floating-point mode", 
                    config->getOptionValue("FloatingPointMode"));
            }
            if (renderSystemConfigOptions.find("RTT Preferred Mode") !=
                renderSystemConfigOptions.end())
            {
                mRenderSystem->setConfigOption("RTT Preferred Mode", 
                    config->getOptionValue("RTT"));
            }
        }
        else if (!config && selected == "Direct3D9")
        {
            resolutionWidth = "800";
            resolutionHeight = "600";
            colourDepth = "32";
            videoMode = resolutionWidth + " x " + resolutionHeight + " @ " +
                        colourDepth + "-bit colour";
            mRenderSystem->setConfigOption("Video Mode", videoMode);
            Ogre::ConfigOptionMap& renderSystemConfigOptions =
                mRenderSystem->getConfigOptions();
            if (renderSystemConfigOptions.find("Floating-point mode") !=
                renderSystemConfigOptions.end())
            {
                mRenderSystem->setConfigOption("Floating-point mode", "Fastest");
            }
            if (renderSystemConfigOptions.find("RTT Preferred Mode") !=
                renderSystemConfigOptions.end())
            {
                mRenderSystem->setConfigOption("RTT Preferred Mode", "FBO");
            }
        }
        else // if (config == 0 && selected == "OpenGL")
        {
            resolutionWidth = "800";
            resolutionHeight = "600";
            colourDepth = "32";
            videoMode = resolutionWidth + " x " + resolutionHeight + " @ " +
                        colourDepth + "-bit colour";
            mRenderSystem->setConfigOption("Video Mode", videoMode);
            Ogre::ConfigOptionMap& renderSystemConfigOptions =
                mRenderSystem->getConfigOptions();
            if (renderSystemConfigOptions.find("Floating-point mode") !=
                renderSystemConfigOptions.end())
            {
                mRenderSystem->setConfigOption("Floating-point mode", "Fastest");
            }
            if (renderSystemConfigOptions.find("RTT Preferred Mode") !=
                renderSystemConfigOptions.end())
            {
                mRenderSystem->setConfigOption("RTT Preferred Mode", "FBO");
            }
        }
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::initialiseRenderer()
    {
        LOGI("Initialising OGRE.");
        mOgreRoot->initialise(false);
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::createSceneManagement()
    {
        LOGI("Creating scene management...");

        // Registering the in-build SceneManagerFactories
        // NOTE the 'in-build' - other factories can be added :)
        // TODO must be done by script as we don't need to register all type of SM.

        mSceneManagerEnumerator->registerSceneManagerFactory(
            new GraphicsSceneManagerFactoryOGRE());
        mSceneManagerEnumerator->registerSceneManagerFactory(
            new TerrainGraphicsSceneManagerFactory());
        mSceneManagerEnumerator->registerSceneManagerFactory(
            new PCZGraphicsSceneManagerFactory());


// TODO Steven ??? Still valid ? I don't use it in IGE ?!
        // NOTE: We register it here because GuiManager doesn't have a loop 
        //       hence it can't be an ExtensionManager which is needed for reply!
        //       We create a "Gui Generic SM" - note the 'Gui'  
    ///    mSceneManagerEnumerator.registerSceneManagerFactory(
    ///        new GuiSceneManagerFactory()); // type "Gui Generic SM"

        // TODO We register it here because we must first convert InputManager
        //      into an ExtensionManager because it can be in its own thread
    ///    mSceneManagerEnumerator.registerSceneManagerFactory(
    ///        new InputSceneManagerFactory()); // type "Input Generic SM"

    ///    ObjectManager::getSingletonPtr()->registerSceneManagerEnumerator(
    ///        mSceneManagerEnumerator);
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::initRenderTargets()
    {
        LOGI("Initialise the render targets (in fact only reset the statistics).");
        mRenderSystem->_initRenderTargets();
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::createRenderWindows(const String& name, 
        const String& externalWindowHandle, const String& parentWindowHandle)
    {
        LOGI("Creating the Render Window...");

        // TODO How to support multiple render window handling code ?
        RenderWindowSettings settings;

        OptionPtr config; 
        if (mGraphicsConfig)
            config = mGraphicsConfig->getOption(String("renderwindow.")+name);

        if (config)
        {
           settings.name = name;
            // TODO same as in _configureRenderSystem
            settings.fullscreen = StringUtil::toBool(
                config->getOptionValue( "Fullscreen") );
            settings.width = StringUtil::toUnsignedInt(
                config->getOptionValue( "Width") );
            settings.height = StringUtil::toUnsignedInt(
                config->getOptionValue( "Height") );
            settings.miscParams["title"] =  
                config->getOptionValue( "Title");
            settings.miscParams["left"] =  convertOgeOptionToOgreOption( 
                config->getOptionValue( "Left") );
            settings.miscParams["top"] =  convertOgeOptionToOgreOption(
                config->getOptionValue( "Top") );
            settings.miscParams["depthBuffer"] =  convertOgeOptionToOgreOption(
                config->getOptionValue( "DepthBuffer") );
            settings.miscParams["FSAA"] =  
                config->getOptionValue( "FSAA");
            settings.miscParams["displayFrequency"] = 
                config->getOptionValue( "VSync");
            settings.miscParams["vsync"] =  convertOgeOptionToOgreOption(
                config->getOptionValue( "DisplayFrequency") );
            settings.miscParams["border"] =  convertOgeOptionToOgreOption(
                config->getOptionValue( "Border") );
            settings.miscParams["outerDimensions"] =  convertOgeOptionToOgreOption(
                config->getOptionValue( "UseOuterDimensions") );
            settings.miscParams["useNVPerfHUD"] =  convertOgeOptionToOgreOption(
                config->getOptionValue( "UseNVPerfHUD") );
        }

        if (!config) // No config
        {
            settings.name = name;

            // TODO same as in _configureRenderSystem
            settings.fullscreen = false;
            settings.width = 800; 
            settings.height = 600;

            settings.miscParams["title"] = "Main Render Window";
            settings.miscParams["left"] = "-1";               // == "Center"
            settings.miscParams["top"] = "-1";                // == "Center"
            settings.miscParams["depthBuffer"] = "true";      // == "True"
            settings.miscParams["FSAA"] = "0";
            settings.miscParams["vsync"] = "false";           // == "False";
    // NEXT       settings.miscParams["displayFrequency"] = "Desktop vsync rate";
            settings.miscParams["border"] = "resize";         // == "Resize";
            settings.miscParams["outerDimensions"] = "false"; // == "False";
            settings.miscParams["useNVPerfHUD"] = "false";    // == "False";
        }

// TODO Something is wrong on Linux it causes a segfault in Ogre. Why? we are passing 0!
#if (OGE_PLATFORM == OGE_PLATFORM_WIN32)
        settings.miscParams["externalWindowHandle"] = externalWindowHandle;
        settings.miscParams["parentWindowHandle"] = parentWindowHandle;
#endif

        // Create the ogre render window !
        mRenderWindow = mRenderTargetManager->createRenderWindow(settings);

        // TODO ... it seems I destroy 'settings' to soon I can't pass it to ogre... 
        // or there is a segfault... 
        // Put this somehow in RenderTargetManager::createRenderWindow()
        // Ogre::WindowEventUtilities::addWindowEventListener(renderWindow, &settings);
        mRws = new RenderWindowSettings();
        Ogre::WindowEventUtilities::addWindowEventListener(mRenderWindow, mRws);
    }
    //-------------------------------------------------------------------------
    String GraphicsManagerOGRE::convertOgeOptionToOgreOption(const String& ogeOption)
    {
        if (ogeOption == "True")
            return "true";
        else if (ogeOption == "False")
            return "false";
        else if (ogeOption == "Yes")
            return "yes";
        else if (ogeOption == "No")
            return "no";
        else if (ogeOption == "Resize")
            return "resize";
        else if (ogeOption == "None")
            return "none";
        else if (ogeOption == "Fixed")
            return "fixed";
        else if (ogeOption == "Center")
            return "-1";

        return ogeOption;
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::createDefaultSceneManager()
    {
        LOGI("Creating the default scene manager...");

        /* In this method we are basically doing this:
        Ogre::SceneManager* sceneMgr = mOgreRoot->createSceneManager(Ogre::ST_GENERIC); 

        Ogre::Camera* cam = sceneMgr->createCamera("SimpleCamera"); 
        cam->setPosition(Ogre::Vector3(0.0f,0.0f,500.0f)); 
        cam->lookAt(Ogre::Vector3(0.0f,0.0f,0.0f)); 
        cam->setNearClipDistance(5.0f); 
        cam->setFarClipDistance(5000.0f); 

        Ogre::Viewport* v = mRenderWindow->addViewport(cam); 
        v->setBackgroundColour(Ogre::ColourValue(0.0,0.0,0.0)); 

        cam->setAspectRatio(Ogre::Real(v->getActualWidth())/v->getActualHeight()); 
        sceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(cam); 
        */

        // This SM will be destroyed by the default ObjectSceneManager
        // as it will add it to its enumerator
        mSceneManager = static_cast<GraphicsSceneManager*>(
            mSceneManagerEnumerator->createSceneManager(
                "OGE_Default_SceneManager", "Generic SM"));

        if (mSceneManager == 0)
        {
            LOGE("A SceneManager of type 'Generic SM' couldn't be created.");
            return;
        }

        if (mCreateDefaultViewportAndCamera)
            createDefaultViewportAndCamera();

        // TODO Can and/or must it be somewhere else !?
        // if (mCreateDefaultGui)
    ///        mSceneManagerEnumerator->createSceneManager(
    ///            "Gui_OGE_Default_SceneManager", "Gui Generic SM");
        // if (mCreateDefaultInput)
    ///        mSceneManagerEnumerator->createSceneManager(
    ///            "Input_OGE_Default_SceneManager", "Input Generic SM");
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::createDefaultViewportAndCamera()
    {
        LOGI("Creating the default viewport and camera for default scene.");

        mDefaultCamera = ((GraphicsSceneManagerOGRE*) mSceneManager)->getOgreSceneManager()
            ->createCamera("OGE_Default_Camera");

		Ogre::Viewport *vp = mRenderTargetManager->addViewport("OGE_Default_Viewport", 
            mRenderWindow->getName(), mDefaultCamera);

        // Position it at 500 in Z direction
        mDefaultCamera->setPosition(Ogre::Vector3(0,0,500));

        // Look back along the -Z axis
        mDefaultCamera->lookAt(Ogre::Vector3(0,0,-100));
        mDefaultCamera->setNearClipDistance(5);

		mDefaultCamera->setAspectRatio(Ogre::Real(vp->getActualWidth())/ Ogre::Real(vp->getActualHeight()));
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::saveScreenshot(const String& filename, bool timestamped)
    {
        if (timestamped)
        {
            String outBasename;
            String outExtention;
            Ogre::StringUtil::splitBaseFilename(filename, outBasename, outExtention);
            outExtention = "." + outExtention;
            mRenderWindow->writeContentsToTimestampedFile(outBasename, outExtention);
        }
        else
            mRenderWindow->writeContentsToFile(filename);
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::toggleDebugOverlay()
    {
    #ifndef IGE_SERVER
        if (mDebugOverlay)
            mDebugOverlay->showOverlay( !mDebugOverlay->isVisible() );
    #endif
    }
    //-------------------------------------------------------------------------
    Vector3 GraphicsManagerOGRE::getRenderWindowSize() const
    {
        return Vector3(mRenderWindow->getWidth(), mRenderWindow->getHeight(), 0);
    }
    //-------------------------------------------------------------------------
    bool GraphicsManagerOGRE::isFullscreen() const
    {
        return mRenderWindow->isFullScreen();
    }
    //-------------------------------------------------------------------------
    const String GraphicsManagerOGRE::getLibrariesVersion()
    {
        String version("Graphics: Ogre3D ");
        version += OGRE_VERSION_NAME;
        version += ".";
        version += StringUtil::toString(OGRE_VERSION_MAJOR);
        version += ".";
        version += StringUtil::toString(OGRE_VERSION_MINOR);
        version += ".";
        version += StringUtil::toString(OGRE_VERSION_PATCH);
        version += " \n";
        return version;
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::getMeshInformation(const Ogre::MeshPtr mesh,
        size_t &vertex_count, Ogre::Vector3* &vertices, size_t &index_count,
        unsigned long* &indices, const Ogre::Vector3 &position,
        const Ogre::Quaternion &orient, const Ogre::Vector3 &scale)
    {
        bool added_shared = false;
        size_t current_offset = 0;
        size_t shared_offset = 0;
        size_t next_offset = 0;
        size_t index_offset = 0;

        vertex_count = index_count = 0;

        // Calculate how many vertices and indices we're going to need
        for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
        {
            Ogre::SubMesh* submesh = mesh->getSubMesh( i );

            // We only need to add the shared vertices once
            if(submesh->useSharedVertices)
            {
                if( !added_shared )
                {
                    vertex_count += mesh->sharedVertexData->vertexCount;
                    added_shared = true;
                }
            }
            else
            {
                vertex_count += submesh->vertexData->vertexCount;
            }

            // Add the indices
            index_count += submesh->indexData->indexCount;
        }

        // Allocate space for the vertices and indices
        vertices = new Ogre::Vector3[vertex_count];
        indices = new unsigned long[index_count];

        added_shared = false;

        // Run through the submeshes again, adding the data into the arrays
        for ( unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
        {
            Ogre::SubMesh* submesh = mesh->getSubMesh(i);

            Ogre::VertexData* vertex_data = submesh->useSharedVertices
                ? mesh->sharedVertexData : submesh->vertexData;

            if((!submesh->useSharedVertices)||(submesh->useSharedVertices && !added_shared))
            {
                if(submesh->useSharedVertices)
                {
                    added_shared = true;
                    shared_offset = current_offset;
                }

                const Ogre::VertexElement* posElem =
                    vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

                Ogre::HardwareVertexBufferSharedPtr vbuf =
                    vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

                unsigned char* vertex =
                    static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

                // There is _no_ baseVertexPointerToElement() 
                // which takes an Ogre::Real or a double as second argument. 
                // So make it float, to avoid trouble when Ogre::Real will
                // be comiled/typedefed as double:
                //      Ogre::Real* pReal;
                float* pReal; // TODO put inside for()

                for( size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize())
                {
                    posElem->baseVertexPointerToElement(vertex, &pReal);

                    Ogre::Vector3 pt(pReal[0], pReal[1], pReal[2]);

                    vertices[current_offset + j] = (orient * (pt * scale)) + position;
                }

                vbuf->unlock();
                next_offset += vertex_data->vertexCount;
            }

            Ogre::IndexData* index_data = submesh->indexData;
            size_t numTris = index_data->indexCount / 3;
            Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;

            bool use32bitindexes = 
                (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);

            unsigned long*  pLong = static_cast<unsigned long*>
                (ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
            unsigned short* pShort = reinterpret_cast<unsigned short*>(pLong);

            size_t offset = 
                (submesh->useSharedVertices)? shared_offset : current_offset;

            if ( use32bitindexes )
            {
                for ( size_t k = 0; k < numTris*3; ++k)
                {
                    indices[index_offset++] = 
                        pLong[k] + 
                        static_cast<unsigned long>(offset);
                }
            }
            else
            {
                for ( size_t k = 0; k < numTris*3; ++k)
                {
                    indices[index_offset++] = 
                        static_cast<unsigned long>(pShort[k]) +
                        static_cast<unsigned long>(offset);
                }
            }

            ibuf->unlock();
            current_offset = next_offset;
        }
    }
    //-----------------------------------------------------------------------------
    MeshData* GraphicsManagerOGRE::createTerrainMeshData( String filename, 
        unsigned int step, float heightMultiplier )
    {
        Ogre::Image image;
        image.load( filename, "General" ); // TODO Only in the General group

        // Warning! this will break if passed an image that's height and width aren't the same
        unsigned int imagesize = (unsigned int) image.getWidth();

        /* Code to store the image in memory... 
        int upperRange = 0;
        float *heightData = 0;

        if( image.getFormat() == Ogre::PF_L8 )
        {
            upperRange = 255;
            heightData = new float[imagesize*imagesize];

            const Ogre::uchar *source = image.getData();

            for( int i = 0; i < imagesize * imagesize; ++i )
            {
                *heightData++ = *source++;
            }
        }
        */

        float xmin, zmin;
        float xmax, zmax;

        int xindex = 0;
        int zindex = 0;

        float xtexmin = 0;
        float xtexmax = 0;
        float ztexmin = 0;
        float ztexmax = 0;

        unsigned int size = imagesize / step;

        MeshData* data = new MeshData();
        data->vertexCount   = (size + 1) * (size + 1);
        data->triangleCount = size * size * 2; // 2 by 'square tile'
        int triangle = 0;

        data->vertices = new Vector3[ data->vertexCount ];
        data->triangles = new Vector3[ data->triangleCount ];

        for( unsigned int x = 0; x < size - 1; x += step )
        {
            xmin = x;
            xmax = x + step;

            for( unsigned int z = 0; z < size - 1; z += step )
            {
                unsigned int v[4];
                v[0] =     x*size + z;     // bottom-left
                v[1] =     x*size + z + 1; // bottom-right
                v[2] = (x+1)*size + z;     // top-left
                v[3] = (x+1)*size + z +1;  // top-right

                zmin = z;
                zmax = z + step;

                // first triangle
                // bottom-left vertice
                Ogre::ColourValue c = image.getColourAt( xmin, zmin, 0 );
                data->vertices[ v[0] ].x = xmin;
                data->vertices[ v[0] ].y = (c.r + c.g + c.b ) * heightMultiplier;
                data->vertices[ v[0] ].z = zmin;
                data->triangles[ triangle ].x = v[0];

                // bottom-right vertice
                c = image.getColourAt( xmin, zmax, 0 );
                data->vertices[ v[1] ].x = xmin;
                data->vertices[ v[1] ].y = (c.r + c.g + c.b ) * heightMultiplier;
                data->vertices[ v[1] ].z = zmax;
                data->triangles[ triangle ].y = v[1];

                // top-right vertice
                c = image.getColourAt( xmax, zmax, 0 );
                data->vertices[ v[3] ].x = xmax;
                data->vertices[ v[3] ].y = (c.r + c.g + c.b ) * heightMultiplier;
                data->vertices[ v[3] ].z = zmax;
                data->triangles[ triangle ].z = v[3];
                triangle++;

                // top-left vertice
                c = image.getColourAt( xmax, zmin, 0 );
                data->vertices[ v[2] ].x = xmax;
                data->vertices[ v[2] ].y = (c.r + c.g + c.b ) * heightMultiplier;
                data->vertices[ v[2] ].z = zmin;
                data->triangles[ triangle ].x = v[2];

                // bottom-left vertice
                c = image.getColourAt( xmin, zmin, 0 );
                data->vertices[ v[0] ].x = xmin;
                data->vertices[ v[0] ].y = (c.r + c.g + c.b ) * heightMultiplier;
                data->vertices[ v[0] ].z = zmin;
                data->triangles[ triangle ].y = v[0];

                // top-right vertice
                c = image.getColourAt( xmax, zmax, 0 );
                data->vertices[ v[3] ].x = xmax;
                data->vertices[ v[3] ].y = (c.r + c.g + c.b ) * heightMultiplier;
                data->vertices[ v[3] ].z = zmax;
                data->triangles[ triangle ].z = v[3];
                triangle++;
            }
        }
        return data;
    }
    //-------------------------------------------------------------------------
    float* GraphicsManagerOGRE::getHeightfield(const String& filename,
        float& ymin, float& ymax, unsigned int& size,
        int step, bool xz, float heightMultiplier, String group)
    {
        float* data;

        ymin = std::numeric_limits<unsigned int>::max(); // 9999999999;
        ymax = std::numeric_limits<unsigned int>::min(); // -9999999999;

        Ogre::Image image;
        image.load( filename, group); // such as "General"

        // Warning! this will break if passed an image that's height and width aren't the same
        unsigned int imagesize = (unsigned int) image.getWidth();

        size = imagesize / step;

        data = new float[size * size];
        assert( data && "Out of memory" ); // TODO We should return elegantly in release?

        for( unsigned int x = 0; x < size - 1; x += step )
        {
            for( unsigned int z = 0; z < size - 1; z += step )
            {
                Ogre::ColourValue c = image.getColourAt( x, z, 0 );
                float y = (c.r + c.g + c.b ) * heightMultiplier;

                if (xz)
                    data[x*size + z] = y;
                else
                    data[z*size + x] = y;

                if (ymin > y) ymin = y;
                if (ymax < y) ymax = y;
            }
        }
        return data;
    }
    //-------------------------------------------------------------------------
    Ogre::ParticleSystem* GraphicsManagerOGRE::createParticleSystem(
        const String& id, const String& templateName)
    {
        return ((GraphicsSceneManagerOGRE*)mSceneManager)->getOgreSceneManager()
            ->createParticleSystem( id, templateName );
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::destroyParticleSystem(const String& id)
    {
        ((GraphicsSceneManagerOGRE*)mSceneManager)->getOgreSceneManager()
            ->destroyParticleSystem( id );
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::destroyParticleSystem(Ogre::ParticleSystem* ps)
    {
        ((GraphicsSceneManagerOGRE*)mSceneManager)->getOgreSceneManager()
            ->destroyParticleSystem( ps );
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::createSphereMesh(const std::string& name,
        const float radius, const int nbRings, const int nbSegments)
    {
        // TODO test if name is unique if not log and return

        Ogre::MeshPtr pSphere = Ogre::MeshManager::getSingleton().createManual(
            name, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
        Ogre::SubMesh *pSphereVertex = pSphere->createSubMesh();

        pSphere->sharedVertexData = new Ogre::VertexData();
        Ogre::VertexData* vertexData = pSphere->sharedVertexData;

        // define the vertex format
        Ogre::VertexDeclaration* vertexDecl = vertexData->vertexDeclaration;
        size_t currOffset = 0;

        // positions
        vertexDecl->addElement(0, currOffset, Ogre::VET_FLOAT3, Ogre::VES_POSITION);
        currOffset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);

        // normals
        vertexDecl->addElement(0, currOffset, Ogre::VET_FLOAT3, Ogre::VES_NORMAL);
        currOffset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);

        // two dimensional texture coordinates
        vertexDecl->addElement(0, currOffset, Ogre::VET_FLOAT2, Ogre::VES_TEXTURE_COORDINATES, 0);
        currOffset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT2);

        // allocate the vertex buffer
        vertexData->vertexCount = (nbRings + 1) * (nbSegments+1);
        Ogre::HardwareVertexBufferSharedPtr vBuf = 
            Ogre::HardwareBufferManager::getSingleton().createVertexBuffer(
                vertexDecl->getVertexSize(0),
                vertexData->vertexCount, 
                Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY, false);
        Ogre::VertexBufferBinding* binding = vertexData->vertexBufferBinding;
        binding->setBinding(0, vBuf);
        float* pVertex = static_cast<float*>(vBuf->lock(Ogre::HardwareBuffer::HBL_DISCARD));

        // allocate index buffer
        pSphereVertex->indexData->indexCount = 6 * nbRings * (nbSegments + 1);
        pSphereVertex->indexData->indexBuffer = 
            Ogre::HardwareBufferManager::getSingleton().createIndexBuffer(
                Ogre::HardwareIndexBuffer::IT_16BIT, 
                pSphereVertex->indexData->indexCount, 
                Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY, false);
        Ogre::HardwareIndexBufferSharedPtr iBuf = pSphereVertex->indexData->indexBuffer;
        unsigned short* pIndices = 
            static_cast<unsigned short*>(iBuf->lock(Ogre::HardwareBuffer::HBL_DISCARD));

        float fDeltaRingAngle = (Ogre::Math::PI / nbRings);
        float fDeltaSegAngle = (2 * Ogre::Math::PI / nbSegments);
        unsigned short wVerticeIndex = 0 ;

        // Generate the group of rings for the sphere
        for( int ring = 0; ring <= nbRings; ring++ )
        {
            float r0 = radius * sinf (ring * fDeltaRingAngle);
            float y0 = radius * cosf (ring * fDeltaRingAngle);

            // Generate the group of segments for the current ring
            for(int seg = 0; seg <= nbSegments; seg++)
            {
                float x0 = r0 * sinf(seg * fDeltaSegAngle);
                float z0 = r0 * cosf(seg * fDeltaSegAngle);

                // Add one vertex to the strip which makes up the sphere
                *pVertex++ = x0;
                *pVertex++ = y0;
                *pVertex++ = z0;

                Vector3 vNormal = Vector3(x0, y0, z0).normalisedCopy();
                *pVertex++ = vNormal.x;
                *pVertex++ = vNormal.y;
                *pVertex++ = vNormal.z;

                *pVertex++ = (float) seg / (float) nbSegments;
                *pVertex++ = (float) ring / (float) nbRings;

                if (ring != nbRings)
                {
                    // each vertex (except the last) has six indices pointing to it
                    *pIndices++ = wVerticeIndex + nbSegments + 1;
                    *pIndices++ = wVerticeIndex;               
                    *pIndices++ = wVerticeIndex + nbSegments;
                    *pIndices++ = wVerticeIndex + nbSegments + 1;
                    *pIndices++ = wVerticeIndex + 1;
                    *pIndices++ = wVerticeIndex;
                    wVerticeIndex ++;
                }
            }; // end for seg
        } // end for ring

        // Unlock
        vBuf->unlock();
        iBuf->unlock();
        // Generate face list
        pSphereVertex->useSharedVertices = true;

        // the original code was missing this line:
        pSphere->_setBounds( Ogre::AxisAlignedBox( 
            Ogre::Vector3(-radius, -radius, -radius), 
            Ogre::Vector3(radius, radius, radius) ), 
            false );
        pSphere->_setBoundingSphereRadius(radius);
        // this line makes clear the mesh is loaded (avoids memory leaks)
        pSphere->load();
        pSphere->load();
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::createCubeMesh(const String& name)
    {
        /// Create the mesh via the MeshManager
        Ogre::MeshPtr msh = 
            Ogre::MeshManager::getSingleton().createManual(name, 
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

        /// Create one submesh
        Ogre::SubMesh* sub = msh->createSubMesh();

        const float sqrt13 = 0.577350269f; /* sqrt(1/3) */

        /// Define the vertices (8 vertices, each consisting of 2 groups of 3 floats
        const size_t nVertices = 8;
        const size_t vbufCount = 3*2*nVertices;
        float vertices[vbufCount] =
        {
            -100.0,100.0,-100.0,        //0 position
            -sqrt13,sqrt13,-sqrt13,     //0 normal
            100.0,100.0,-100.0,         //1 position
            sqrt13,sqrt13,-sqrt13,      //1 normal
            100.0,-100.0,-100.0,        //2 position
            sqrt13,-sqrt13,-sqrt13,     //2 normal
            -100.0,-100.0,-100.0,       //3 position
            -sqrt13,-sqrt13,-sqrt13,    //3 normal
            -100.0,100.0,100.0,         //4 position
            -sqrt13,sqrt13,sqrt13,      //4 normal
            100.0,100.0,100.0,          //5 position
            sqrt13,sqrt13,sqrt13,       //5 normal
            100.0,-100.0,100.0,         //6 position
            sqrt13,-sqrt13,sqrt13,      //6 normal
            -100.0,-100.0,100.0,        //7 position
            -sqrt13,-sqrt13,sqrt13,     //7 normal
        };

        Ogre::RenderSystem* rs = Ogre::Root::getSingleton().getRenderSystem();
        Ogre::RGBA colours[nVertices];
        Ogre::RGBA *pColour = colours;
        // Use render system to convert colour value since colour packing varies
        rs->convertColourValue(Ogre::ColourValue(1.0,0.0,0.0), pColour++); //0 colour
        rs->convertColourValue(Ogre::ColourValue(1.0,1.0,0.0), pColour++); //1 colour
        rs->convertColourValue(Ogre::ColourValue(0.0,1.0,0.0), pColour++); //2 colour
        rs->convertColourValue(Ogre::ColourValue(0.0,0.0,0.0), pColour++); //3 colour
        rs->convertColourValue(Ogre::ColourValue(1.0,0.0,1.0), pColour++); //4 colour
        rs->convertColourValue(Ogre::ColourValue(1.0,1.0,1.0), pColour++); //5 colour
        rs->convertColourValue(Ogre::ColourValue(0.0,1.0,1.0), pColour++); //6 colour
        rs->convertColourValue(Ogre::ColourValue(0.0,0.0,1.0), pColour++); //7 colour

        /// Define 12 triangles (two triangles per cube face)
        /// The values in this table refer to vertices in the above table
        const size_t ibufCount = 36;
        unsigned short faces[ibufCount] =
        {
            0,2,3,
            0,1,2,
            1,6,2,
            1,5,6,
            4,6,5,
            4,7,6,
            0,7,4,
            0,3,7,
            0,5,1,
            0,4,5,
            2,7,3,
            2,6,7
        };

        /// Create vertex data structure for 8 vertices shared between submeshes
        msh->sharedVertexData = new Ogre::VertexData();
        msh->sharedVertexData->vertexCount = nVertices;

        /// Create declaration (memory format) of vertex data
        Ogre::VertexDeclaration* decl = msh->sharedVertexData->vertexDeclaration;
        size_t offset = 0;
        // 1st buffer
        decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_POSITION);
        offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
        decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_NORMAL);
        offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
        /// Allocate vertex buffer of the requested number of vertices (vertexCount) 
        /// and bytes per vertex (offset)
        Ogre::HardwareVertexBufferSharedPtr vbuf = 
        Ogre::HardwareBufferManager::getSingleton().createVertexBuffer(
            offset, msh->sharedVertexData->vertexCount, Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);
        /// Upload the vertex data to the card
        vbuf->writeData(0, vbuf->getSizeInBytes(), vertices, true);

        /// Set vertex buffer binding so buffer 0 is bound to our vertex buffer
        Ogre::VertexBufferBinding* bind = msh->sharedVertexData->vertexBufferBinding; 
        bind->setBinding(0, vbuf);

        // 2nd buffer
        offset = 0;
        decl->addElement(1, offset, Ogre::VET_COLOUR, Ogre::VES_DIFFUSE);
        offset += Ogre::VertexElement::getTypeSize(Ogre::VET_COLOUR);
        /// Allocate vertex buffer of the requested number of vertices (vertexCount) 
        /// and bytes per vertex (offset)
        vbuf = Ogre::HardwareBufferManager::getSingleton().createVertexBuffer(
        offset, msh->sharedVertexData->vertexCount, Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);
        /// Upload the vertex data to the card
        vbuf->writeData(0, vbuf->getSizeInBytes(), colours, true);

        /// Set vertex buffer binding so buffer 1 is bound to our colour buffer
        bind->setBinding(1, vbuf);

        /// Allocate index buffer of the requested number of vertices (ibufCount) 
        Ogre::HardwareIndexBufferSharedPtr ibuf = Ogre::HardwareBufferManager::getSingleton().
        createIndexBuffer(
        Ogre::HardwareIndexBuffer::IT_16BIT, 
        ibufCount, 
        Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);

        /// Upload the index data to the card
        ibuf->writeData(0, ibuf->getSizeInBytes(), faces, true);

        /// Set parameters of the submesh
        sub->useSharedVertices = true;
        sub->indexData->indexBuffer = ibuf;
        sub->indexData->indexCount = ibufCount;
        sub->indexData->indexStart = 0;

        /// Set bounding information (for culling)
        msh->_setBounds(Ogre::AxisAlignedBox(-100,-100,-100,100,100,100));
        msh->_setBoundingSphereRadius(Ogre::Math::Sqrt(3*100*100));

        /// Notify Mesh object that it has been loaded
        msh->load();
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::setDebugLinesMode(bool mode)
    {
        ((GraphicsSceneManagerOGRE*)mSceneManager)->setDebugLinesMode(mode);
    }
	//-------------------------------------------------------------------------
	void GraphicsManagerOGRE::setProfilerOverlayVisible(bool visible)
	{
		if(visible) {
			if(!mProfilerOverlay) {
				mProfilerOverlay = new ProfilerOverlay();
				mProfilerOverlay->setOverlayDimensions(300,150);
				mProfilerOverlay->setOverlayPosition(0,0);
				mProfilerOverlay->setUpdateDisplayFrequency(60);
			}
			mProfilerOverlay->setOverlayVisible(true);
		}
		else if(mProfilerOverlay) {
			mProfilerOverlay->setOverlayVisible(false);
		}
	}
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::drawDebugLine(const Vector3& from, 
        const Vector3& to, const Vector3& color)
    {
        ((GraphicsSceneManagerOGRE*)mSceneManager)->drawDebugLine(from, to, color);
    }
    //-------------------------------------------------------------------------
    void GraphicsManagerOGRE::drawDebugContactPoint(const Vector3& point, 
        const Vector3& normal, float distance, int lifeTime, const Vector3& color)
    {
        ((GraphicsSceneManagerOGRE*)mSceneManager)->drawDebugContactPoint(point, normal, distance, lifeTime, color);
    }
    //-------------------------------------------------------------------------
}
