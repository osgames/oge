/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeLightComponentOGRE.h"
#include "oge/graphics/OgeGraphicsSceneManagerOGRE.h"
#include "oge/graphics/OgeGraphicsComponentOGRE.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"
#include "oge/object/OgeObject.h"
#include "oge/graphics/OgeGraphicsManager.h"

#include "OgreLight.h"
#include "OgreSceneNode.h"
#include "OgreSceneManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    bool LightComponentOGRE::_reset(const MessageList& params, SceneManager* sceneManager)
    {
        const String& objId = getObjectId();

        // Create light if not set via _setLight()
        if (0 == mLight)
        {
            Ogre::SceneManager* ogreSM = 
                ((GraphicsSceneManagerOGRE*)sceneManager)->getOgreSceneManager();

            try
            {
                mLight = ogreSM->createLight( objId + "Light" );

                if (!mNode) // set via _setNode()
                    mNode = ogreSM->getRootSceneNode()->createChildSceneNode(objId + "LightNode");

                mNode->attachObject(mLight);
            }
            catch (...)
            {
                LOGE("Failed to create OGRE light...");
                return false;
            }
        }

        // Type TODO Can the light be changed at run-time?
        const String *lightType;
        if (params.find( ObjectMessage::INIT_LIGHT_TYPE, lightType ))
        {
            assert(mLight && "No Ogre light!");
            if (*lightType == "Point")
                mLight->setType( Ogre::Light::LT_POINT );
            else if (*lightType == "Directional")
                mLight->setType( Ogre::Light::LT_DIRECTIONAL );
            else if (*lightType == "SpotLight")
                mLight->setType( Ogre::Light::LT_SPOTLIGHT );
            else
            {
                mLight->setType( Ogre::Light::LT_POINT );
                LOGW("Invalid light type: should be 'Point', 'Directional' or 'SpotLight'");
            }
        }
        else
        {
            LOGW( "INIT_LIGHT_TYPE message could not be found or its parameters are invalid "
                  "while resetting a '" + getType() + "' component! Owner object Id: " + getObjectId() );
            return false;
        }

        // Careful !!! INIT_LIGHT_TYPE must be called first!
        for (MessageList::ConstIter it = params.list.begin(); it != params.list.end(); ++it)
        {
            _processMessage(*it);
        }
        return true;
    }
    //-------------------------------------------------------------------------
    void LightComponentOGRE::_destroy(SceneManager* sceneManager)
    {
        if (mNode)
        {
            mNode->detachAllObjects();
            ((GraphicsSceneManagerOGRE*)sceneManager)->getOgreSceneManager()
                ->destroySceneNode(mNode);
            mNode = 0;
        }
        if (mLight)
        {
            // TODO In old Ogre it was necessary? And yet what is the equivalent method?
            //((GraphicsSceneManagerOGRE*)sceneManager)->getOgreSceneManager()->
            //    ->removeLight( mLight );
            mLight = 0;
        }
    }
    //-------------------------------------------------------------------------
    void LightComponentOGRE::_activate(bool activate, SceneManager* sceneManager)
    {
        // TODO Set intensity to 0 and store the old value?
    }
    //-------------------------------------------------------------------------
    Component::UpdateRate LightComponentOGRE::_getUpdateRate() const
    {
        return Component::NO_UPDATE;
    }
    //-------------------------------------------------------------------------
    void LightComponentOGRE::_update(double deltaTime)
    {
        LOGW("Should not be called!");
    }
    //-------------------------------------------------------------------------
    void LightComponentOGRE::_processMessage(const Message& message)
    {
        switch (message.getType())
        {
            case ObjectMessage::SET_POSITION: _setPositionByMessage( message ); return;
            case ObjectMessage::SET_SCALE: _setScaleByMessage( message ); return;
            //_case ObjectMessage::SET_DIMENSIONS: _setDimensionsByMessage( message ); return;
            case ObjectMessage::SET_ORIENTATION: _setOrientationByMessage( message ); return;
            case ObjectMessage::SET_POSITION_ORIENTATION: _setPositionOrientationByMessage( message ); return;
            case ObjectMessage::SET_INTENSITY: _setIntensityByMessage( message ); return;
            case ObjectMessage::SET_DIFFUSE_COLOUR: _setDiffuseColourByMessage( message ); return;
            case ObjectMessage::SET_SPECULAR_COLOUR: _setSpecularColourByMessage( message ); return;
            default:
                // NEXT ++counterOfUnnecessaryMessageProcessedByAComponent;
                return;
        }
    }
    //-----------------------------------------------------------------------------
/* GraphicsComponent code to remove or adapt?
    Vector3 LightComponentOGRE::getDimensions() const
    {
        assert(mNode && mEntity);
        // Get its AABB dimensions (AxisAlignedBox::getMaximum() - AxisAlignedBox::getMinimum())
        // It remove the extension factor that is applied to all the bounding boxes
        Ogre::AxisAlignedBox box = mEntity->getBoundingBox();
        box.scale(mNode->getScale());
        Vector3 min(box.getMinimum().ptr());
        Vector3 max(box.getMaximum().ptr());
        return Vector3(fabs(max.x-min.x), fabs(max.y-min.y), fabs(max.z-min.z));
    }
*/
    //-------------------------------------------------------------------------
    void LightComponentOGRE::showBoundingBox( bool show )
    {
        //segfault! mNode->showBoundingBox(show);
    }
    //-------------------------------------------------------------------------
    void LightComponentOGRE::_setPositionByMessage(const Message& message)
    {
        const Vector3 *position = FastAnyCast(Vector3, &message.getParam_1());
        mNode->setPosition( Ogre::Vector3(position->ptr()) );
    }
    //-------------------------------------------------------------------------
    void LightComponentOGRE::_setScaleByMessage(const Message& message)
    {
        const Vector3 *scale = FastAnyCast(Vector3, &message.getParam_1());
        mNode->setScale( Ogre::Vector3(scale->ptr()) );
    }
    //-------------------------------------------------------------------------
    void LightComponentOGRE::_setOrientationByMessage(const Message& message)
    {
        const Quaternion *orientation = FastAnyCast(Quaternion, &message.getParam_1());
        mNode->setOrientation( Ogre::Quaternion((Ogre::Real*)orientation->ptr()) );
    }
    //-------------------------------------------------------------------------
    void LightComponentOGRE::_setPositionOrientationByMessage(const Message& message)
    {
        const Vector3 *position = FastAnyCast(Vector3, &message.getParam_1());
        const Quaternion *orientation = FastAnyCast(Quaternion, &message.getParam_2());
        assert(position && orientation);
        mNode->setPosition( Ogre::Vector3(position->ptr()) );
        mNode->setOrientation( Ogre::Quaternion((Ogre::Real*)orientation->ptr()) );
    }
    //-------------------------------------------------------------------------
/*    void LightComponentOGRE::_setDimensionsByMessage(const Message& message)
    {
        const Vector3* dimensions = FastAnyCast(Vector3, &message.getParam_1());
        Vector3 scale = *dimensions / getDimensions();
        mNode->setScale( scale.x, scale.y, scale.z );
    }
*/
    //-------------------------------------------------------------------------
    void LightComponentOGRE::_setIntensityByMessage(const Message& message)
    {
        const float* intensity = FastAnyCast(float, &message.getParam_1());
        mIntensity = *intensity;
        resetColours();
    }
    //-------------------------------------------------------------------------
    void LightComponentOGRE::_setDiffuseColourByMessage(const Message& message)
    {
        const Vector3* col = FastAnyCast(Vector3, &message.getParam_1());
        mDiffuse = *col;
        resetColours();
    }
    //-------------------------------------------------------------------------
    void LightComponentOGRE::_setSpecularColourByMessage(const Message& message)
    {
        const Vector3* col = FastAnyCast(Vector3, &message.getParam_1());
        mSpecular = *col;
        resetColours();
    }
    //-------------------------------------------------------------------------
    void LightComponentOGRE::resetColours()
    {
        mLight->setDiffuseColour( mDiffuse.x * mIntensity, 
            mDiffuse.y * mIntensity, mDiffuse.z * mIntensity );

        mLight->setSpecularColour( mSpecular.x * mIntensity, 
            mSpecular.y * mIntensity, mSpecular.z * mIntensity );
    }
    //-------------------------------------------------------------------------
}
