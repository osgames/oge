/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeRenderTargetManager.h"
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/graphics/OgeCameraComponent.h"
#include "oge/object/OgeObject.h"

#include "OgreRoot.h"

namespace oge
{
    //-------------------------------------------------------------------------
    RenderTargetManager::RenderTargetManager(Ogre::Root* ogreRoot) :
        mOgreRoot(ogreRoot), primaryRenderWindowExist(false)
    {
    }
    //-------------------------------------------------------------------------
    RenderTargetManager::~RenderTargetManager()
    {
        // NEXT delete all viewport ? Seems to be done by ogre
        // NEXT delete all renderwindow ? Seems to be done by ogre
        mRenderWindowMap.deleteAll();
    }
    //-------------------------------------------------------------------------
    Ogre::RenderWindow* RenderTargetManager::createRenderWindow(
        RenderWindowSettings settings)
    {
        if (mRenderWindowMap.entryExists(settings.name))
        {
            LOGW(String("A render window of name")+settings.name+" already exist.");
            return (Ogre::RenderWindow*)(mOgreRoot->getRenderTarget(settings.name));
        }

        mRenderWindowMap.addEntry(settings.name, settings);

        // We can't let the user make a mistake :)
        if (!primaryRenderWindowExist)
        {
            settings.primary = true;
            primaryRenderWindowExist = true;
        }
        else settings.primary = false;

        Ogre::NameValuePairList list(settings.miscParams);

        Ogre::RenderWindow* renderWindow = 
            mOgreRoot->createRenderWindow(settings.name,
                settings.width,
                settings.height,
                settings.fullscreen,
                &list);

        // TODO ... it seems I destroy 'settings' to soon I can't pass it to ogre... or there is a segfault...
        // Ogre::WindowEventUtilities::addWindowEventListener(renderWindow, &settings);

        return renderWindow;
    }
    //-------------------------------------------------------------------------
    Ogre::Viewport* RenderTargetManager::addViewport(const oge::String& name,
        const oge::String& targetName, Ogre::Camera* camera, int zOrder,
        float left, float top, float width, float height)
    {
        ViewportSettings settings;
        settings.name = name;
        settings.renderTargetName = targetName;
        settings.camera = camera;
        settings.zOrder = zOrder;
        settings.left = left;
        settings.top = top;
        settings.width = width;
        settings.height = height;

        return addViewport( settings );
    }
    //-------------------------------------------------------------------------
    Ogre::Viewport* RenderTargetManager::addViewport(ViewportSettings settings)
    {
        // Does the render target settings exist?
        if (!mRenderWindowMap.entryExists(settings.renderTargetName))
        {
            LOGW(String("No render window settings named: ")+settings.renderTargetName);
            return 0;
        }

        // Get the target  TODO Should use _createRenderWindow !!!
        Ogre::RenderTarget* renderTarget = mOgreRoot->getRenderTarget(settings.renderTargetName);

        if (renderTarget == 0)
        {
            LOGW(String("No render window named: ")+settings.renderTargetName);
            return 0;
        }

        // Does the settings & viewport already exist
        if (mViewportMap.entryExists(settings.name))
        {
            ViewportSettings set = mViewportMap.getEntry(settings.name);
            // Viewport already exist
            if (set.viewport)
            {
                LOGW(String("Returning existing viewport:")+settings.name);
                return set.viewport;
            }
        }

        // Test if the zorder is unique for the target
        // If not we will add +1 until it is
        // Exceptions can happen if identical zorder viewports overlap
        while( !isZOrderUnique( settings ) )
            settings.zOrder++;

        // Finally, creating the viewport
        Ogre::Viewport* viewport = renderTarget->addViewport(settings.camera,
            settings.zOrder, settings.left, settings.top, settings.width,
            settings.height);

    // TODO Pass as params !?!
        viewport->setBackgroundColour(Ogre::ColourValue(0,0,0));
        settings.camera->setAspectRatio(Ogre::Real(viewport->getActualWidth()) /
            Ogre::Real(viewport->getActualHeight()));

        // We keep a reference to the ogre viewport
        settings.viewport = viewport;

        // Add the viewport or if already exist replace it with the new settings
        mViewportMap.addEntry( settings.name, settings);

        return viewport;
    }
    //-------------------------------------------------------------------------
    void RenderTargetManager::removeAllViewports(const String& targetName)
    {
        LOG(String("Remove viewports of the target ")+targetName);
        Ogre::RenderTarget* renderTarget =
            mOgreRoot->getRenderTarget(targetName);

        ViewportMap::Iterator iter;
        for (iter = mViewportMap.begin(); iter!=mViewportMap.end();
            iter = mViewportMap.begin() )
        {
            ViewportSettings view = iter->second;
            if (view.renderTargetName == targetName)
            {
                renderTarget->removeViewport(view.zOrder);
                view.viewport = 0;
            }
        }
    }
    //-------------------------------------------------------------------------
    void RenderTargetManager::setCameraToViewport(GraphicsSceneManager* sm,
        const ObjectId camera, const String& viewportName)
    {
        // Note: without the global mViewportMap we would be obliged to know
        // the target!
        ViewportSettings settings = mViewportMap.getEntry(viewportName);
        CameraComponent* comp = (CameraComponent*) 
            GraphicsManager::getManager()->getComponent(camera, "Camera");

        Ogre::Camera* ogreCamera = FastAnyCast(Ogre::Camera*, comp->getCamera());

        if (ogreCamera == 0)
        {
            LOGW("No camera found.");
            return;
        }
        settings.camera = ogreCamera;
        settings.cameraName = ogreCamera->getName();

        settings.viewport->setCamera( ogreCamera );

		// update the camera aspect ratio to the one that the viewport uses
		ogreCamera->setAspectRatio(
			Ogre::Real(settings.viewport->getActualWidth()) / 
			Ogre::Real(settings.viewport->getActualHeight())
		);
    }
    //-------------------------------------------------------------------------
    bool RenderTargetManager::isZOrderUnique( const ViewportSettings& settings )
    {
        ViewportMap::Iterator iter;
        for (iter = mViewportMap.begin(); iter!=mViewportMap.end();
            iter = mViewportMap.begin() )
        {
            ViewportSettings view = iter->second;

            if (view.zOrder == settings.zOrder &&
                view.renderTargetName == settings.renderTargetName)
            {
                LOGW( String("The viewport ")+settings.name+
                    " has the same zorder value as "+ view.name );
                return false;
            }
        }
        return true;
    }
    //-------------------------------------------------------------------------
    void RenderTargetManager::setBackgroundColour( const String& viewportName,
            const Ogre::ColourValue& colour )
    {
        ViewportMap::Iterator iter;
        for (iter = mViewportMap.begin(); iter!=mViewportMap.end();
            iter = mViewportMap.begin() )
        {
            ViewportSettings view = iter->second;

            if (view.name == viewportName)
            {
                view.viewport->setBackgroundColour( colour );
                return;
            }
        }
    }
    //-------------------------------------------------------------------------
    void RenderTargetManager::removeViewport(const String& viewportName)
    {
        LOG(String("Removing viewport: ")+viewportName);
        ViewportMap::Iterator iter;
        for (iter = mViewportMap.begin(); iter!=mViewportMap.end();
            iter = mViewportMap.begin() )
        {
            ViewportSettings view = iter->second;
            if (view.name == viewportName)
            {
                Ogre::RenderTarget* renderTarget =
                    mOgreRoot->getRenderTarget(view.renderTargetName);
                renderTarget->removeViewport(view.zOrder);
                view.viewport = 0;
                return;
            }
        }
    }
    //-------------------------------------------------------------------------
}
