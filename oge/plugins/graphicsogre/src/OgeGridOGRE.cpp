/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt .
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeGridOGRE.h"
#include "oge/graphics/OgeGraphicsManager.h"

#include "OgreSceneManager.h"
#include "OgreMaterialManager.h"
#include "OgreColourValue.h"
#include "OgreManualObject.h"

namespace oge
{
    //-----------------------------------------------------------------------------
    Ogre::String Grid::gMajorMaterialName  = "GridMajorMaterial";
    Ogre::String Grid::gMediumMaterialName = "GridMediumMaterial";
    Ogre::String Grid::gMinorMaterialName  = "GridMinorMaterial";
    //-----------------------------------------------------------------------------
    Grid::Grid()
    {
        createMaterials();
    }
    //-----------------------------------------------------------------------------
    Grid::~Grid()
    {
    }
    //-----------------------------------------------------------------------------
    Ogre::ManualObject* Grid::createGrid(
        Ogre::SceneManager* const sceneManager,
        const Ogre::String& name,
        const Ogre::Vector3& dimension,
        const Ogre::Vector3& spacing,
        const Ogre::Vector3& position,
        const Ogre::Vector3& orientation)
    {
        mDimension = dimension;
        mSpacing = spacing;
        mPosition = position;
        mOrientation = orientation;

        mGrid = sceneManager->createManualObject(name);

        // TODO If the grid is "too" big show only a portion of the grid to decrase aliasing
        // dimension = ...

        Ogre::Vector3 min = position - dimension / 2;
        Ogre::Vector3 max = position + dimension / 2;

        // Create grid
        mGrid->begin(gMajorMaterialName, Ogre::RenderOperation::OT_LINE_LIST);

// TODO        Ogre::Real entityscale = OgreManager::getSingletonPtr()->getScale();
Ogre::Real entityscale = 1;
        Ogre::Vector3 scale = spacing / entityscale;

        // "z" lines
        float x = min.x;
        while (x <= max.x)
        {
            Ogre::ColourValue colour = (x==0) ? Ogre::ColourValue::Blue : Ogre::ColourValue::White;
            colour.a = getAlpha( x, scale );

            mGrid->position(x, mPosition.y, min.z);
            mGrid->colour(colour);
            mGrid->position(x, mPosition.y, max.z);
            mGrid->colour(colour);
            x++;
        }
        
        // "x" lines
        float z = min.z;
        while (z <= max.z)
        {
            Ogre::ColourValue colour = (z==0) ? Ogre::ColourValue::Red : Ogre::ColourValue::White;
            colour.a = getAlpha( z, scale );

            mGrid->position(min.z, mPosition.y, z);
            mGrid->colour(colour);
            mGrid->position(max.z, mPosition.y, z);
            mGrid->colour(colour);
            z++;
        }

        mGrid->end();

        return mGrid;
    }
    //-----------------------------------------------------------------------------
    void Grid::update()
    {
        // TODO If the grid is "too" big show only a portion of the grid to decrase aliasing
        // dimension = ...

        Ogre::Vector3 min = mPosition - mDimension / 2;
        Ogre::Vector3 max = mPosition + mDimension / 2;

        // Update grid
        mGrid->beginUpdate(0);

// TODO        Ogre::Real entityscale = OgreManager::getSingletonPtr()->getScale();
Ogre::Real entityscale = 1;
        Ogre::Vector3 scale = mSpacing * entityscale;

        // "z" lines
        float x = min.x;
        while (x <= max.x)
        {
            Ogre::ColourValue colour = (x==0) ? Ogre::ColourValue::Blue : Ogre::ColourValue::White;
            colour.a = getAlpha( x, scale );

            mGrid->position(x, mPosition.y, min.z);
            mGrid->colour(colour);
            mGrid->position(x, mPosition.y, max.z);
            mGrid->colour(colour);
            x++;
        }
        
        // "x" lines
        float z = min.z;
        while (z <= max.z)
        {
            Ogre::ColourValue colour = (z==0) ? Ogre::ColourValue::Red : Ogre::ColourValue::White;
            colour.a = getAlpha( z, scale );

            mGrid->position(min.z, mPosition.y, z);
            mGrid->colour(colour);
            mGrid->position(max.z, mPosition.y, z);
            mGrid->colour(colour);
            z++;
        }

        mGrid->end();
    }
    //-----------------------------------------------------------------------------
    void Grid::createMaterials()
    {
        Ogre::MaterialManager* mm = Ogre::MaterialManager::getSingletonPtr();
        Ogre::ColourValue cv = Ogre::ColourValue(1,1,1,.75);
        Ogre::SceneBlendType sbt = Ogre::SBT_TRANSPARENT_ALPHA;

        if (!mm->resourceExists(gMajorMaterialName))
        {
            Ogre::MaterialPtr mp = mm->create(gMajorMaterialName, "General");
	        mp->setReceiveShadows(false); 
	        mp->getTechnique(0)->setLightingEnabled(true);
	        mp->getTechnique(0)->getPass(0)->setDiffuse(cv); 
	        mp->getTechnique(0)->getPass(0)->setAmbient(cv); 
	        mp->getTechnique(0)->getPass(0)->setSelfIllumination(cv); 
	        mp->getTechnique(0)->getPass(0)->setSceneBlending(sbt);
	        mp->getTechnique(0)->getPass(0)->setLightingEnabled(false);
	        mp->getTechnique(0)->getPass(0)->setVertexColourTracking(Ogre::TVC_DIFFUSE);
        }
        if (!mm->resourceExists(gMediumMaterialName))
        {
            Ogre::MaterialPtr mp = mm->create(gMediumMaterialName, "General");
	        mp->setReceiveShadows(false); 
	        mp->getTechnique(0)->setLightingEnabled(true);
	        mp->getTechnique(0)->getPass(0)->setDiffuse(cv); 
	        mp->getTechnique(0)->getPass(0)->setAmbient(cv); 
	        mp->getTechnique(0)->getPass(0)->setSelfIllumination(cv); 
	        mp->getTechnique(0)->getPass(0)->setSceneBlending(sbt);
	        mp->getTechnique(0)->getPass(0)->setLightingEnabled(false);
	        mp->getTechnique(0)->getPass(0)->setVertexColourTracking(Ogre::TVC_DIFFUSE);
        }
        if (!mm->resourceExists(gMinorMaterialName))
        {
            Ogre::MaterialPtr mp = mm->create(gMinorMaterialName, "General");
	        mp->setReceiveShadows(false); 
	        mp->getTechnique(0)->setLightingEnabled(true);
	        mp->getTechnique(0)->getPass(0)->setDiffuse(cv); 
	        mp->getTechnique(0)->getPass(0)->setAmbient(cv); 
	        mp->getTechnique(0)->getPass(0)->setSelfIllumination(cv); 
	        mp->getTechnique(0)->getPass(0)->setSceneBlending(sbt);
	        mp->getTechnique(0)->getPass(0)->setLightingEnabled(false);
	        mp->getTechnique(0)->getPass(0)->setVertexColourTracking(Ogre::TVC_DIFFUSE);
        }
    }
    //-----------------------------------------------------------------------------
    Ogre::Real Grid::getAlpha(Ogre::Real x, const Ogre::Vector3& scale)
    {
        return (scale.x >= 1 && (int)x%(int)scale.x) ? 
                   (scale.y >= 1 && (int)x%(int)scale.y) ? 
                       (scale.z >= 1 && (int)x%(int)scale.z) ? 0.0f : 0.2f
                   : 0.5f
               : 1.0f;
    }
    //-----------------------------------------------------------------------------
}
