/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeGraphicsSceneManagerOGRE.h"
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/graphics/OgeGraphicsManagerOGRE.h"
#include "oge/graphics/OgeGraphicsComponentOGRE.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeVector2.h"
#include "oge/object/OgeObject.h"
#include "oge/graphics/OgeCameraComponent.h"
#include "oge/graphics/OgeExtendedCameraComponentOGRE.h"
#include "oge/task/OgeDelegateTask.h"
#include "oge/engine/OgeEngineManager.h"
#include "Ogre.h"
#include "OgrePlane.h"
#include "OgrePCZSceneManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    GraphicsSceneManagerOGRE::GraphicsSceneManagerOGRE(const String& name) :
        GraphicsSceneManager(name),
        mOgreSceneManager(0),
        mCamera(0),
        mOrphanedSceneNode(0),
        mRaySceneQuery(0),
        mRaySceneQuery2(0),
        mRaySceneQuery3(0),
        mRaySceneQuery4(0),
        mDebugLines(0),
        mDebugOn(false)
    {
        initMetaData();
    }
    //-------------------------------------------------------------------------
    GraphicsSceneManagerOGRE::~GraphicsSceneManagerOGRE()
    {
        delete mRaySceneQuery;
        delete mRaySceneQuery2;
        delete mRaySceneQuery3;

        if (mOrphanedSceneNode)
            mOgreSceneManager->destroySceneNode( mOrphanedSceneNode->getName() );

        clearScene();
    }
    //-------------------------------------------------------------------------
    void GraphicsSceneManagerOGRE::init()
    {
        assert( ((GraphicsManagerOGRE*) GraphicsManager::getManager())->getOgreRoot() );
        mOgreSceneManager = ((GraphicsManagerOGRE*) GraphicsManager::getManager())->getOgreRoot()
            ->createSceneManager(mMetaData.typeName, mName);

        // Create an orphaned scenenode that is not attached to the root node
        mOrphanedSceneNode = mOgreSceneManager->createSceneNode("OrphanedSceneNode");

        mRaySceneQuery = mOgreSceneManager->createRayQuery(
            Ogre::Ray(Ogre::Vector3::ZERO, Ogre::Vector3::NEGATIVE_UNIT_Y));

        // Create the debug capabilities
        setupDebugLines();
    }
    //-------------------------------------------------------------------------
    void GraphicsSceneManagerOGRE::initMetaData() const
    {
        mMetaData.type = "Generic SM";
        mMetaData.family = "Graphics";
        mMetaData.description = "Generic graphical scene manager";
        mMetaData.sceneTypeMask = Ogre::ST_GENERIC; // Not used in fact
        mMetaData.typeName = "DefaultSceneManager"; // used to initialise ogre3d
        mMetaData.worldGeometrySupported = false;
    }
	//-------------------------------------------------------------------------
	void GraphicsSceneManagerOGRE::logProfileInfo(double currentTime)
	{
		double realTime = EngineManager::getSingletonPtr()->getCurrentTime(true);

		ComponentMapIter iter;
        for (iter = mComponents.begin(); mComponents.end() != iter; ++iter) {
			if((*iter).second->getType() == "GraphicsComponent") {
				GraphicsComponentOGRE *comp = (GraphicsComponentOGRE*)(*iter).second;
				LOG(String("Graphics, ") + StringUtil::toString((oge::Real)currentTime) + String(", ") + 
					StringUtil::toString((oge::Real)realTime) + String(", ") + 
					comp->getObjectId() + String(", ") + 
					StringUtil::vector3ToString(comp->getPosition()) + String(", 0"));
			}
			else if((*iter).second->getType() == "ExtendedCamera") {
				ExtendedCameraComponentOGRE *comp = (ExtendedCameraComponentOGRE*)(*iter).second;
				String camMode;
				if(comp->getCameraMode() == CameraComponent::FIRST_PERSON) {
					camMode = "FirstPerson";
				}
				else if(comp->getCameraMode() == CameraComponent::THIRD_PERSON_CHASED) {
					camMode = "ThirdPersonChased";
				}
				else {
					camMode = "ThirdPersonFixed";
				}

				LOG(String("Camera") + camMode + String(", ") + StringUtil::toString((oge::Real)currentTime) + String(", ") + 
					StringUtil::toString((oge::Real)realTime) + String(", ") + 
					comp->getObjectId() + String(", ") + 
					StringUtil::vector3ToString(comp->getPosition()) + String(", 0"));
			}
        }
	}
    //-------------------------------------------------------------------------
    void GraphicsSceneManagerOGRE::update(double deltaTime)
    {
        SceneManager::update(deltaTime);

        // TODO: Should be called automatically by the SceneManager class
        mPendingTasks.processAll();

        // TODO bad peformance with terrain shape (too big)
        //      Should we do this each x frames? Aslo we should do it as a task to avoid the if
        if (mDebugOn) 
        {
            mDebugLines->update();
            mDebugNode->needUpdate();
            mDebugLines->clear();
        }
        /*else // TODO This shouldn't be called all the time! Isn't setDebugMode enough? To test
        {
            mDebugLines->clear();
            mDebugLines->update();
            mDebugNode->needUpdate();
        }*/
    }
    //-------------------------------------------------------------------------
    void GraphicsSceneManagerOGRE::clearScene()
    {
        SceneManager::clearScene();

        if (mOgreSceneManager)
            mOgreSceneManager->clearScene();

        if (mDebugLines)
            delete mDebugLines;
    }
    //-------------------------------------------------------------------------
    Ogre::SceneNode* GraphicsSceneManagerOGRE::createSceneNode(const String& name)
    {
        if (name != "")
            return mOgreSceneManager->createSceneNode(name);

        return mOgreSceneManager->createSceneNode();
    }
    //-------------------------------------------------------------------------
    Ogre::Entity* GraphicsSceneManagerOGRE::createEntity(const String& name,
        const String& meshName)
    {
        return mOgreSceneManager->createEntity(name, meshName);
    }
    //-------------------------------------------------------------------------
    void GraphicsSceneManagerOGRE::setActiveCamera(const ObjectId& camera)
    {
        if (camera == mCameraObject)
            return;

        CameraComponent* comp = (CameraComponent*) GraphicsManager::getManager()
            ->getComponent(camera, "Camera");

        if (comp == 0)
            return;

        mCamera = FastAnyCast(Ogre::Camera*, ((CameraComponent*) comp)->getCamera());
        assert( mCamera );
        mCameraObject = camera;
    }
    //-------------------------------------------------------------------------
    void GraphicsSceneManagerOGRE::clampToTerrain(Vector3* position, float offset)
    {
        static Ogre::Ray updateRay;
        Ogre::Vector3 pos(position->val);
        pos.y += offset; 
        updateRay.setOrigin(pos);
        updateRay.setDirection(Ogre::Vector3::NEGATIVE_UNIT_Y);
        mRaySceneQuery->setRay(updateRay);
        Ogre::RaySceneQueryResult& qryResult = mRaySceneQuery->execute();
        Ogre::RaySceneQueryResult::iterator i = qryResult.begin();
        if (i != qryResult.end() && i->worldFragment)
        {
            position->y = i->worldFragment->singleIntersection.y;
        }
    }
    //-------------------------------------------------------------------------
    Vector3* GraphicsSceneManagerOGRE::doCameraRayQuery(Real x, Real y,
        const unsigned int queryMask)
    {
        if (mRaySceneQuery2 == 0)
            mRaySceneQuery2 = getOgreSceneManager()
                ->createRayQuery(Ogre::Ray(Ogre::Vector3::ZERO, Ogre::Vector3::NEGATIVE_UNIT_Y));

        Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(x, y);

        //See http://www.ogre3d.org/wiki/index.php/Intermediate_Tutorial_3
        mRaySceneQuery2->setSortByDistance(false);
        mRaySceneQuery2->setRay(mouseRay);
        mRaySceneQuery2->setQueryMask(queryMask);

        // Execute query
        Ogre::RaySceneQueryResult &result = mRaySceneQuery2->execute();
        Ogre::RaySceneQueryResult::iterator itr = result.begin( );

        if (itr != result.end() && itr->worldFragment)
        {
            Vector3* position = new Vector3();
            position->x = itr->worldFragment->singleIntersection.x;
            position->y = itr->worldFragment->singleIntersection.y;
            position->z = itr->worldFragment->singleIntersection.z;

            return position;
        }
        return 0;
    }
    //-------------------------------------------------------------------------
    String GraphicsSceneManagerOGRE::doObjectIdRayQuery(float x, float y,
        const unsigned int queryMask)
    {
        Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(x, y);

        if (mRaySceneQuery3 == 0)
            mRaySceneQuery3 = getOgreSceneManager()
                ->createRayQuery(Ogre::Ray(Ogre::Vector3::ZERO, Ogre::Vector3::NEGATIVE_UNIT_Y));

        mRaySceneQuery3->setSortByDistance(true); //See http://www.ogre3d.org/wiki/index.php/Intermediate_Tutorial_3
        mRaySceneQuery3->setRay(mouseRay);
        mRaySceneQuery3->setQueryMask(queryMask);

        // Execute query
        Ogre::RaySceneQueryResult &result = mRaySceneQuery3->execute();
        Ogre::RaySceneQueryResult::iterator itr = result.begin( );
        for (; itr != result.end(); itr++)
        {
            if (itr->worldFragment)
            {
                return StringUtil::BLANK;
            }
            if (itr->movable)
            {
                Ogre::MovableObject* object = itr->movable;
                
                // Extract the oge object id from the ogre object :)
                // ??? can't use find_last_of because if nothing found it returns 1 !!!
                String::size_type pos = object->getName().find("Entity");
                if (pos != String::npos)
                {
                    LOGI("Ray queried object :"+ object->getName());
                    String name = object->getName().substr(0, pos); // less "Entity" TODO invert
                    return name;
                }
            }
        }
        return StringUtil::BLANK;
    }
	//-------------------------------------------------------------------------
	bool GraphicsSceneManagerOGRE::getScreenspaceBounds(Ogre::MovableObject* object, Vector2& min, Vector2& max)
	{
		if(!object->isInScene())
			return false;

		Ogre::Camera *camera = mCamera;

		const Ogre::AxisAlignedBox &AABB = object->getWorldBoundingBox(true);
		Ogre::Matrix4 mat = camera->getProjectionMatrix() * camera->getViewMatrix();

		Ogre::FrustumPlane culledBy;
		if(!camera->isVisible(AABB,&culledBy)) {
			// just return the screen co-ords of the center position
			Ogre::Vector3 center = mat * AABB.getCenter();

			min.x = max.x = 0.5f + (0.5f * center.x);
			min.y = max.y = 0.5f + (0.5f * -center.y);
			return false;
		}

		const Ogre::Vector3* corners = AABB.getAllCorners();
		float min_x = 1.0f, max_x = 0.0f, min_y = 1.0f, max_y = 0.0f;

		//The normal vector of the plaine.this points directly infront of the cam
		//Ogre::Vector3 CameraPlaneNormal = camera->getDerivedOrientation().zAxis();

		//the plane that divides the space before and behind the camera
		//Ogre::Plane CameraPlane = Ogre::Plane(CameraPlaneNormal,camera->getDerivedPosition());

		//bool valid = false;
		for (int i=0; i<8; i++) {
			Ogre::Vector3 corner = corners[i];
/*
			if (CameraPlane.getSide(corner) != Ogre::Plane::NEGATIVE_SIDE) {
				continue;
			}
			valid = true; 
*/
			// multiply the AABB corner vertex by the view matrix to 
			// get a camera-space vertex
			corner = mat * corner;

			// make 2D relative/normalized coords from the view-space vertex
			// by dividing out the Z (depth) factor -- this is an approximation
			float x = 0.5f + (0.5f * corner.x);
			float y = 0.5f + (0.5f * -corner.y);

			if (x < min_x) 
				min_x = x;

			if (x > max_x) 
				max_x = x;

			if (y < min_y) 
				min_y = y;

			if (y > max_y) 
				max_y = y;
		}
		

		min.x = min_x;
		min.y = min_y;
		max.x = max_x;
		max.y = max_y;

		return true;
		//return valid;
	}
	//-------------------------------------------------------------------------
	bool GraphicsSceneManagerOGRE::getScreenspaceCoords(Ogre::MovableObject* object, Vector2& result)
	{
	   if(!object->isInScene())
		  return false;

	   Ogre::Camera *camera = mCamera;
	   const Ogre::AxisAlignedBox &AABB = object->getWorldBoundingBox(true);

	   /**
	   * If you need the point above the object instead of the center point:
	   * This snippet derives the average point between the top-most corners of the bounding box
	   * Ogre::Vector3 point = (AABB.getCorner(AxisAlignedBox::FAR_LEFT_TOP)
	   *    + AABB.getCorner(AxisAlignedBox::FAR_RIGHT_TOP)
	   *    + AABB.getCorner(AxisAlignedBox::NEAR_LEFT_TOP)
	   *    + AABB.getCorner(AxisAlignedBox::NEAR_RIGHT_TOP)) / 4;
	   */

	   // Get the center point of the object's bounding box
	   Ogre::Vector3 point = AABB.getCenter();

	   // Is the camera facing that point? If not, return false
	   Ogre::Plane cameraPlane = Ogre::Plane(Ogre::Vector3(camera->getDerivedOrientation().zAxis()), camera->getDerivedPosition());
	   if(cameraPlane.getSide(point) != Ogre::Plane::NEGATIVE_SIDE)
		  return false;
	   
	   // Transform the 3D point into screen space
	   point = camera->getProjectionMatrix() * (camera->getViewMatrix() * point);

	   // Transform from coordinate space [-1, 1] to [0, 1] and update in-value
	   result.x = (point.x / 2) + 0.5f;
	   result.y = 1 - ((point.y / 2) + 0.5f);

	   return true;
	}
    //-------------------------------------------------------------------------
    bool GraphicsSceneManagerOGRE::collidesWithEntity(const Vector3& fromPoint, const Vector3& toPoint, 
        float padding, const unsigned int queryMask)
    {
        Vector3 fromPointAdj(fromPoint.x, fromPoint.y - 1, fromPoint.z);
        Vector3 toPointAdj(toPoint.x, toPoint.y - 1, toPoint.z);	
        Vector3 normal = toPointAdj - fromPointAdj;

        Vector3 result(0, 0, 0);
        Ogre::Entity* entity = 0;
        float distToColl = 0.0f;

        if (raycastFromPoint(fromPointAdj, normal, result, 
                (unsigned long&)entity, distToColl, queryMask))
        {
            distToColl -= padding;
            return (distToColl <= normal.normalise());
        }
        else
        {
            return false;
        }
    }
    //-------------------------------------------------------------------------
    bool GraphicsSceneManagerOGRE::raycastFromPoint(const Vector3& from, 
            const Vector3& normal, Vector3& result, unsigned long& target, 
            float& closestDistance, const unsigned int queryMask)
    {
        target = 0;

        Ogre::Ray ray(Ogre::Vector3(from.x, from.y, from.z),
            Ogre::Vector3(normal.x, normal.y, normal.z));

        // Check we are initialised
        if (mRaySceneQuery4 == 0)
        {
            mRaySceneQuery4 = getOgreSceneManager()->createRayQuery(ray);
        }
        else
            mRaySceneQuery4->setRay(ray);

        //mRaySceneQuery->setSortByDistance(true); No the bounding box != mesh
        mRaySceneQuery4->setQueryMask(queryMask);

        if (mRaySceneQuery4->execute().size() <= 0)
            return false; // didn't hit a bounding box

        // At this point we have raycast to a series of different objects bounding boxes.
        // we need to test these different objects to see which is the first polygon hit.
        // there are some minor optimizations (distance based) that mean we wont have to
        // check all of the objects most of the time, but the worst case scenario is that
        // we need to test every triangle of every object.
        closestDistance = -1.0f;
        Ogre::Vector3 closestResult;
        Ogre::RaySceneQueryResult &queryResult = mRaySceneQuery4->getLastResults();
        for (size_t qr_idx = 0; qr_idx < queryResult.size(); qr_idx++)
        {
            // Stop checking if we have found a raycast hit that is closer
            // than all remaining entities
            if ((closestDistance >= 0.0f) &&
                (closestDistance < queryResult[qr_idx].distance))
            {
                break;
            }

            /*if (query_result[qr_idx].movable != NULL) {
            printf("colcheck: %s, %s \n", query_result[qr_idx].movable->getName().c_str(),query_result[qr_idx].movable->getMovableType().c_str());
            }*/
            // only check this result if its a hit against an entity

            if ((queryResult[qr_idx].movable != NULL)  &&
                (queryResult[qr_idx].movable->getMovableType().compare("Entity") == 0)) 
            {
                // get the entity to check
                Ogre::Entity* entity = static_cast<Ogre::Entity*>(queryResult[qr_idx].movable); 			

                // Mesh data to retrieve         
                size_t vertexCount;
                size_t indexCount;
                Ogre::Vector3* vertices;
                unsigned long* indices;

                // Get the mesh information
                GraphicsManagerOGRE::getSingletonPtr()->getMeshInformation(
                    entity->getMesh(), vertexCount, vertices, indexCount, indices,             
                    entity->getParentNode()->_getDerivedPosition(),
                    entity->getParentNode()->_getDerivedOrientation(),
                    entity->getParentNode()->getScale());

                // Test for hitting individual triangles on the mesh
                bool foundCloser = false;
                for (int i = 0; i < static_cast<int>(indexCount); i += 3)
                {
                    // Check for a hit against this triangle
                    std::pair<bool, Ogre::Real> hit = 
                        Ogre::Math::intersects(ray, vertices[indices[i]],
                    vertices[indices[i+1]], vertices[indices[i+2]], true, false);

                    // If it was a hit check if its the closest
                    if (hit.first)
                    {
                        if ((closestDistance < 0.0f) ||
                            (hit.second < closestDistance))
                        {
                            // this is the closest so far, save it off
                            closestDistance = hit.second;
                            foundCloser = true;
                        }
                    }
                }

                // Free the verticies and indicies memory
                delete[] vertices;
                delete[] indices;

                // If we found a new closest raycast for this object, update the
                // closest_result before moving on to the next object.
                if (foundCloser)
                {
                    target = (unsigned long) entity;
                    closestResult = ray.getPoint(closestDistance);               
                }
            }
        }

        if (closestDistance >= 0.0f)
        {
            result = closestResult;
            return true;
        }
        else
        {
            return false; // raycast failed
        }
    }
    //-------------------------------------------------------------------------
    /*
    TODO  Ogre::Real getTerrainHeight(Ogre::Real x, Ogre::Real z)
     {
         Ogre::Ray* verticalRay = new Ogre::Ray( Ogre::Vector3(x, 5000, z), Ogre::Vector3::NEGATIVE_UNIT_Y );
         mRaySceneQuery->setRay( *verticalRay );
     
           // Execute query
         Ogre::RaySceneQueryResult &result = mRaySceneQuery->execute();
         Ogre::RaySceneQueryResult::iterator itr = result.begin( );
     
          if ( itr != result.end() && itr->worldFragment )
          {
            Ogre::Vector3 intersection = itr->worldFragment->singleIntersection;
            return intersection.y;
         }      
         else
         {
            return 0;
         }   
     } 
    */
    //-------------------------------------------------------------------------
    void GraphicsSceneManagerOGRE::setupDebugLines()
    {
        assert(!mDebugLines);
        mDebugLines = new DynamicLines();

        mDebugNode = mOgreSceneManager->getRootSceneNode()->createChildSceneNode("DebugNode");
        mDebugNode->attachObject(mDebugLines);

   // TODO doesn't work with 1.7.0 why?     Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create("DebugGroup/DebugLines", "DebugGroup");
        Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create("DebugGroup/DebugLines", "General");
		mat->setReceiveShadows(false);
		//mat->setSelfIllumination(1,1,1);
        mat->getTechnique(0)->setLightingEnabled(false);
         /*mat->getTechnique(0)->setLightingEnabled(true); 
         mat->getTechnique(0)->getPass(0)->setDiffuse(0,0,1,0); 
         mat->getTechnique(0)->getPass(0)->setAmbient(0,0,1); 
         mat->getTechnique(0)->getPass(0)->setSelfIllumination(0,0,1); 
        */

        // Set colour tracking to show vertex color.
        // it should be necessary, but colors are shown also without this. why?!
        mat->getTechnique(0)->getPass(0)->setVertexColourTracking(Ogre::TVC_AMBIENT);

		mDebugLines->setMaterial("DebugGroup/DebugLines");
    }
    //-------------------------------------------------------------------------
    void GraphicsSceneManagerOGRE::setDebugLinesMode(bool mode)
    {
        mDebugOn = mode;
        if (!mDebugOn)
        {
            mDebugLines->clear();
            mDebugLines->update();
            mDebugNode->needUpdate();
        }
    }
    //-------------------------------------------------------------------------
    void GraphicsSceneManagerOGRE::drawDebugLine(const Vector3& from, 
        const Vector3& to, const Vector3& color)
    {
        // TODO use color
        mDebugLines->addPoint(from, color);
        mDebugLines->addPoint(to, color);
    }
    //-------------------------------------------------------------------------
    void GraphicsSceneManagerOGRE::drawDebugContactPoint(const Vector3& point, 
        const Vector3& normal, float distance, int lifeTime, const Vector3& color)
    {
        // TODO use color and lifeTime
        mDebugLines->addPoint(point, color);
        Vector3 arrow = point + normal * distance * 20.0f;
        mDebugLines->addPoint(arrow, color);
    }
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    GraphicsSceneManagerFactoryOGRE::GraphicsSceneManagerFactoryOGRE()
    {
        // Temporary sm to get the type name
        GraphicsSceneManagerOGRE sm("temp");
        sm.initMetaData();
        mType = sm.getType();
    }
    //-------------------------------------------------------------------------
    GraphicsSceneManagerFactoryOGRE::~GraphicsSceneManagerFactoryOGRE()
    {
    }
    //-------------------------------------------------------------------------
    SceneManager* GraphicsSceneManagerFactoryOGRE::createInstance(
        const String& name)
    {
        assert( ((GraphicsManagerOGRE*) GraphicsManager::getManager())->getOgreRoot() );

        GraphicsSceneManagerOGRE* sm = new GraphicsSceneManagerOGRE(name);
        sm->init();
        assert(sm->getType() == mType);
        return sm;
    }
    //-------------------------------------------------------------------------
    bool GraphicsSceneManagerFactoryOGRE::checkAvailability()
    { 
        if( !((GraphicsManagerOGRE*) GraphicsManager::getManager())->isPluginAvailable("Octree & Terrain Scene Manager") )
        {
            LOGW("The plugin ogre::OctreeSceneManager is not available - using the ogre default");
        }

        mAvailable = true; // always true either octree or default ogre sm
        return true;
    }
    //-------------------------------------------------------------------------


    //-------------------------------------------------------------------------
    TerrainGraphicsSceneManager::TerrainGraphicsSceneManager(const String& name) :
        GraphicsSceneManagerOGRE(name)
    {
        initMetaData();
    }
    //-------------------------------------------------------------------------
    TerrainGraphicsSceneManager::~TerrainGraphicsSceneManager()
    {
    }
    //-------------------------------------------------------------------------
    void TerrainGraphicsSceneManager::init()
    {
        LOG("Initialise the TerrainGraphicsSceneManager");

        assert( ((GraphicsManagerOGRE*) GraphicsManager::getManager())->getOgreRoot() );

        mOgreSceneManager = ((GraphicsManagerOGRE*) GraphicsManager::getManager())
            ->getOgreRoot()->createSceneManager(mMetaData.typeName, mName);

        // Create an orphaned scenenode that is not attached to the root node
        mOrphanedSceneNode = mOgreSceneManager->createSceneNode("OrphanedSceneNode");

        mRaySceneQuery = mOgreSceneManager->createRayQuery(
            Ogre::Ray(Ogre::Vector3::ZERO, Ogre::Vector3::NEGATIVE_UNIT_Y));

        // Create the debug capabilities
        setupDebugLines();
    }
    //-------------------------------------------------------------------------
    void TerrainGraphicsSceneManager::initMetaData() const
    {
        mMetaData.type = "Terrain Close SM";
        mMetaData.family = "Graphics";
        mMetaData.description = "Terrain graphical Scene Manager";
        mMetaData.sceneTypeMask = Ogre::ST_EXTERIOR_CLOSE; // not used
        mMetaData.typeName = "TerrainSceneManager"; // used to initialise ogre3d
        mMetaData.worldGeometrySupported = true;
    }
    //-------------------------------------------------------------------------
    AsyncBool TerrainGraphicsSceneManager::setWorldGeometry(String filename)
    {
        typedef DelegateTask1<String, bool> DT;
        DT* fn = new DT(filename, DT::Delegate(this, 
            &TerrainGraphicsSceneManager::_setWorldGeometry));

        AsyncBool result = fn->getResult();
        mPendingTasks.addTask(fn);
        return result;
    }
    //-------------------------------------------------------------------------
    bool TerrainGraphicsSceneManager::_setWorldGeometry(String filename)
    {
        mOgreSceneManager->setWorldGeometry( filename );
        return true;
    }
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    TerrainGraphicsSceneManagerFactory::TerrainGraphicsSceneManagerFactory()
    {
        // Temporary sm to get the type name
        TerrainGraphicsSceneManager sm("temp");
        sm.initMetaData();
        mType = sm.getType();
    }
    //-------------------------------------------------------------------------
    SceneManager* TerrainGraphicsSceneManagerFactory::createInstance(
        const String& name)
    {
        TerrainGraphicsSceneManager* sm =
            new TerrainGraphicsSceneManager(name);
        sm->init();
        assert(sm->getType() == mType);
        return sm;
    }
    //-------------------------------------------------------------------------
    bool TerrainGraphicsSceneManagerFactory::checkAvailability()
    {   
        if( !((GraphicsManagerOGRE*) GraphicsManager::getManager())->isPluginAvailable("Octree & Terrain Scene Manager") )
        {
            LOGE("The ogre plugin TerrainSceneManager is not available.");
            mAvailable = false;
            return false;
        }
        mAvailable = true;
        return true;
    }
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    PCZGraphicsSceneManager::PCZGraphicsSceneManager(const String& name) :
        GraphicsSceneManagerOGRE(name)
    {
        initMetaData();
    }
    //-------------------------------------------------------------------------
    PCZGraphicsSceneManager::~PCZGraphicsSceneManager()
    {
    }
    //-------------------------------------------------------------------------
    void PCZGraphicsSceneManager::init()
    {
        LOG("Initialise the PCZGraphicsSceneManager");

        assert( ((GraphicsManagerOGRE*) GraphicsManager::getManager())->getOgreRoot() );

        mOgreSceneManager = ((GraphicsManagerOGRE*) GraphicsManager::getManager())
            ->getOgreRoot()->createSceneManager(mMetaData.typeName, mName);

        // Create an orphaned scenenode that is not attached to the root node
        mOrphanedSceneNode = mOgreSceneManager->createSceneNode("OrphanedSceneNode");

        mRaySceneQuery = mOgreSceneManager->createRayQuery(
            Ogre::Ray(Ogre::Vector3::ZERO, Ogre::Vector3::NEGATIVE_UNIT_Y));

        // Create the debug capabilities
        setupDebugLines();

        // Initialize the scene manager using terrain as default zone
        String zoneTypeName = "ZoneType_Default";
        String zoneFilename = "none";
        ((Ogre::PCZSceneManager*) mOgreSceneManager)->init(zoneTypeName);
    }
    //-------------------------------------------------------------------------
    void PCZGraphicsSceneManager::initMetaData() const
    {
        mMetaData.type = "PCZ SM";
        mMetaData.family = "Graphics";
        mMetaData.description = "Portal Connected Zone Scene Manager";
        mMetaData.sceneTypeMask = 0xFFFF; // sceneTypeMask not used because of this one ! TODO verify that OGRE has a mask now!
        mMetaData.typeName = "PCZSceneManager"; // used to initialise ogre3d
        mMetaData.worldGeometrySupported = false; // CAREFUL this means probably that we can't do raycast!
    }
    //-------------------------------------------------------------------------
    AsyncBool PCZGraphicsSceneManager::setWorldGeometry(String filename)
    {
        typedef DelegateTask1<String, bool> DT;
        DT* fn = new DT(filename, DT::Delegate(this, 
            &PCZGraphicsSceneManager::_setWorldGeometry));

        AsyncBool result = fn->getResult();
        mPendingTasks.addTask(fn);
        return result;
    }
    //-------------------------------------------------------------------------
    bool PCZGraphicsSceneManager::_setWorldGeometry(String filename)
    {
        // TODO Test if this works with PCZ or if a specialised method must be done?
        mOgreSceneManager->setWorldGeometry( filename );
        return true;
    }
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    PCZGraphicsSceneManagerFactory::PCZGraphicsSceneManagerFactory()
    {
        // Temporary sm to get the type name
        PCZGraphicsSceneManager sm("temp");
        sm.initMetaData();
        mType = sm.getType();
    }
    //-------------------------------------------------------------------------
    SceneManager* PCZGraphicsSceneManagerFactory::createInstance(
        const String& name)
    {
        PCZGraphicsSceneManager* sm =
            new PCZGraphicsSceneManager(name);
        sm->init();
        assert(sm->getType() == mType);
        return sm;
    }
    //-------------------------------------------------------------------------
    bool PCZGraphicsSceneManagerFactory::checkAvailability()
    {   
        // See the name in OgrePCZPlugin.cpp
        if( !((GraphicsManagerOGRE*) GraphicsManager::getManager())
            ->isPluginAvailable("Portal Connected Zone Scene Manager") )
        {
            LOGE("The ogre plugin PCZ SceneManager is not available.");
            mAvailable = false;
            return false;
        }
        mAvailable = true;
        return true;
    }
    //-------------------------------------------------------------------------

}
