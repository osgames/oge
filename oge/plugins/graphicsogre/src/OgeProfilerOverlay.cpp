/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics//OgeProfilerOverlay.h"
#include "oge/OgeProfiler.h"
#include "OgreOverlayManager.h"
#include "OgreStringConverter.h"


namespace oge {

	ProfilerOverlay::ProfilerOverlay() 
		: mInitialized(false)
		, mMaxDisplayProfiles(50)
		, mOverlay(0)
		, mProfileGui(0)
		, mBarHeight(10)
		, mGuiHeight(25)
		, mGuiWidth(250)
		, mGuiLeft(0)
        , mGuiTop(0)
		, mBarIndent(250)
		, mGuiBorderWidth(10)
		, mBarLineWidth(2)
		, mBarSpacing(3)
		, mUpdateDisplayFrequency(10)
		, mDisplayMode(DISPLAY_MILLISECONDS)
		, mResetExtents(false)
	{
		initialize();
    }
    //-----------------------------------------------------------------------
    ProfilerOverlay::~ProfilerOverlay() {
        mProfileBars.clear();
    }
	//---------------------------------------------------------------------
	void ProfilerOverlay::setOverlayDimensions(Real width, Real height)
	{
		mGuiWidth = width;
		mGuiHeight = height;
		mBarIndent = mGuiWidth;

		mProfileGui->setDimensions(width, height);

	}
	//---------------------------------------------------------------------
	void ProfilerOverlay::setOverlayPosition(Real left, Real top)
	{
		mGuiLeft = left;
		mGuiTop = top;

		mProfileGui->setPosition(left, top);
	}
	//---------------------------------------------------------------------
	void ProfilerOverlay::setOverlayVisible(bool visible)
	{
		if(mOverlay) {
			if(visible) {
				mOverlay->show();
			}
			else {
				mOverlay->hide();
			}
		}
	}
	//---------------------------------------------------------------------
	void ProfilerOverlay::setUpdateDisplayFrequency(unsigned int freq)
	{
		mUpdateDisplayFrequency = freq;
	}
	//---------------------------------------------------------------------
	Real ProfilerOverlay::getOverlayWidth() const
	{
		return mGuiWidth;
	}
	//---------------------------------------------------------------------
	Real ProfilerOverlay::getOverlayHeight() const
	{
		return mGuiHeight;
	}
	//---------------------------------------------------------------------
	Real ProfilerOverlay::getOverlayLeft() const
	{
		return mGuiLeft;
	}
	//---------------------------------------------------------------------
	Real ProfilerOverlay::getOverlayTop() const
	{
		return mGuiTop;
	}
	//---------------------------------------------------------------------
    void ProfilerOverlay::initialize() 
	{
        // create a new overlay to hold our Profiler display
        mOverlay = Ogre::OverlayManager::getSingleton().create("Profiler");
        mOverlay->setZOrder(500);

        // this panel will be the main container for our profile bars
        mProfileGui = createContainer();

        Ogre::OverlayElement* element;

        // we create an initial pool of 50 profile bars
        for (unsigned int i = 0; i < mMaxDisplayProfiles; ++i) {

            // this is for the profile name and the number of times it was called in a frame
			element = createTextArea("profileText" + Ogre::StringConverter::toString(i), 90, mBarHeight, mGuiBorderWidth + (mBarHeight + mBarSpacing) * i, 0, 14, "", false);
            mProfileGui->addChild(element);
            mProfileBars.push_back(element);

            // this indicates the current frame time
            element = createPanel("currBar" + Ogre::StringConverter::toString(i), 0, mBarHeight, mGuiBorderWidth + (mBarHeight + mBarSpacing) * i, mBarIndent, "Core/ProfilerCurrent", false);
            mProfileGui->addChild(element);
            mProfileBars.push_back(element);

            // this indicates the minimum frame time
            element = createPanel("minBar" + Ogre::StringConverter::toString(i), mBarLineWidth, mBarHeight, mGuiBorderWidth + (mBarHeight + mBarSpacing) * i, 0, "Core/ProfilerMin", false);
            mProfileGui->addChild(element);
            mProfileBars.push_back(element);

            // this indicates the maximum frame time
            element = createPanel("maxBar" + Ogre::StringConverter::toString(i), mBarLineWidth, mBarHeight, mGuiBorderWidth + (mBarHeight + mBarSpacing) * i, 0, "Core/ProfilerMax", false);
            mProfileGui->addChild(element);
            mProfileBars.push_back(element);

            // this indicates the average frame time
            element = createPanel("avgBar" + Ogre::StringConverter::toString(i), mBarLineWidth, mBarHeight, mGuiBorderWidth + (mBarHeight + mBarSpacing) * i, 0, "Core/ProfilerAvg", false);
            mProfileGui->addChild(element);
            mProfileBars.push_back(element);

			// this indicates the text of the frame time
			element = createTextArea("statText" + Ogre::StringConverter::toString(i), 20, mBarHeight, mGuiBorderWidth + (mBarHeight + mBarSpacing) * i, 0, 14, "", false);
			mProfileGui->addChild(element);
			mProfileBars.push_back(element);
        }

        // throw everything all the GUI stuff into the overlay and display it
        mOverlay->add2D(mProfileGui);
        mOverlay->show();

    }
    //-----------------------------------------------------------------------
    void ProfilerOverlay::displayResults() {
		Profiler *profiler = Profiler::getSingletonPtr();
		Profiler::ProfileHistoryList  *profileHistory = profiler->getProfileHistoryList();

        // if its time to update the display
        if (!(mCurrentFrame % mUpdateDisplayFrequency)) {
            Profiler::ProfileHistoryList::iterator iter;
            ProfileBarList::iterator bIter;

            Ogre::OverlayElement* g;

            Real newGuiHeight = mGuiHeight;

            int profileCount = 0; 

			Real maxTimeMillisecs = (Real)profiler->getMaxTotalFrameTime() / 1000.0f;

            // go through each profile and display it
            for (iter = profileHistory->begin(), bIter = mProfileBars.begin(); 
				iter != profileHistory->end() && bIter != mProfileBars.end(); 
				++iter, ++bIter) 
			{

                // display the profile's name and the number of times it was called in a frame
                g = *bIter;
                g->show();
                g->setCaption(String((*iter).name + " (" + Ogre::StringConverter::toString((*iter).numCallsThisFrame) + ")"));
                g->setLeft(10 + (*iter).hierarchicalLvl * 15.0f);

                // display the main bar that show the percentage of the frame time that this
                // profile has taken
                bIter++;
                g = *bIter;
                g->show();
                // most of this junk has been set before, but we do this to get around a weird
                // Ogre gui issue (bug?)
                g->setMetricsMode(Ogre::GMM_PIXELS);
                g->setHeight(mBarHeight);
				if (mDisplayMode == DISPLAY_PERCENTAGE)
					g->setWidth(((*iter).currentTimePercent) * mGuiWidth);
				else
					g->setWidth(((*iter).currentTimeMillisecs / maxTimeMillisecs) * mGuiWidth);
                g->setLeft(mGuiWidth);
                g->setTop(mGuiBorderWidth + profileCount * (mBarHeight + mBarSpacing));

                // display line to indicate the minimum frame time for this profile
                bIter++;
                g = *bIter;
                g->show();
				if (mDisplayMode == DISPLAY_PERCENTAGE)
		            g->setLeft(mBarIndent + (*iter).minTimePercent * mGuiWidth);
				else
					g->setLeft(mBarIndent + ((*iter).minTimeMillisecs / maxTimeMillisecs) * mGuiWidth);

                // display line to indicate the maximum frame time for this profile
                bIter++;
                g = *bIter;
                g->show();
				if (mDisplayMode == DISPLAY_PERCENTAGE)
	                g->setLeft(mBarIndent + (*iter).maxTimePercent * mGuiWidth);
				else
					g->setLeft(mBarIndent + ((*iter).maxTimeMillisecs / maxTimeMillisecs) * mGuiWidth);
                // display line to indicate the average frame time for this profile
                bIter++;
                g = *bIter;
                g->show();
                if ((*iter).totalCalls != 0)
					if (mDisplayMode == DISPLAY_PERCENTAGE)
	                    g->setLeft(mBarIndent + ((*iter).totalTimePercent / (*iter).totalCalls) * mGuiWidth);
					else
						g->setLeft(mBarIndent + (((*iter).totalTimeMillisecs / (*iter).totalCalls) / maxTimeMillisecs) * mGuiWidth);
                else
                    g->setLeft(mBarIndent);

				// display text
				bIter++;
				g = *bIter;
				g->show();
				if (mDisplayMode == DISPLAY_PERCENTAGE)
				{
					g->setLeft(mBarIndent + (*iter).currentTimePercent * mGuiWidth + 2);
					g->setCaption(Ogre::StringConverter::toString((*iter).currentTimePercent * 100.0f, 3, 3) + "%");
				}
				else
				{
					g->setLeft(mBarIndent + ((*iter).currentTimeMillisecs / maxTimeMillisecs) * mGuiWidth + 2);
					g->setCaption(Ogre::StringConverter::toString((*iter).currentTimeMillisecs, 3, 3) + "ms");
				}

				// we set the height of the display with respect to the number of profiles displayed
                newGuiHeight += mBarHeight + mBarSpacing;

                profileCount++;

            }

            // set the main display dimensions
            mProfileGui->setMetricsMode(Ogre::GMM_PIXELS);
            mProfileGui->setHeight(newGuiHeight);
            mProfileGui->setWidth(mGuiWidth * 2 + 15);
            mProfileGui->setTop(5);
            mProfileGui->setLeft(5);

            // we hide all the remaining pre-created bars
            for (; bIter != mProfileBars.end(); ++bIter) {

                (*bIter)->hide();

            }

        }

		mCurrentFrame++;

    }
    //-----------------------------------------------------------------------
    Ogre::OverlayContainer* ProfilerOverlay::createContainer() {

        Ogre::OverlayContainer* container = (Ogre::OverlayContainer*) 
			Ogre::OverlayManager::getSingleton().createOverlayElement(
				"BorderPanel", "profiler");
        container->setMetricsMode(Ogre::GMM_PIXELS);
        container->setMaterialName("Core/StatsBlockCenter");
        container->setHeight(mGuiHeight);
        container->setWidth(mGuiWidth * 2 + 15);
        container->setParameter("border_size", "1 1 1 1");
        container->setParameter("border_material", "Core/StatsBlockBorder");
        container->setParameter("border_topleft_uv", "0.0000 1.0000 0.0039 0.9961");
        container->setParameter("border_top_uv", "0.0039 1.0000 0.9961 0.9961");
        container->setParameter("border_topright_uv", "0.9961 1.0000 1.0000 0.9961");
        container->setParameter("border_left_uv","0.0000 0.9961 0.0039 0.0039");
        container->setParameter("border_right_uv","0.9961 0.9961 1.0000 0.0039");
        container->setParameter("border_bottomleft_uv","0.0000 0.0039 0.0039 0.0000");
        container->setParameter("border_bottom_uv","0.0039 0.0039 0.9961 0.0000");
        container->setParameter("border_bottomright_uv","0.9961 0.0039 1.0000 0.0000");
        container->setLeft(5);
        container->setTop(5);

        return container;

    }
    //-----------------------------------------------------------------------
    Ogre::OverlayElement* ProfilerOverlay::createTextArea(const String& name, Real width, Real height, Real top, Real left, 
                                         unsigned int fontSize, const String& caption, bool show) {


        Ogre::OverlayElement* textArea = 
			Ogre::OverlayManager::getSingleton().createOverlayElement("TextArea", name);
        textArea->setMetricsMode(Ogre::GMM_PIXELS);
        textArea->setWidth(width);
        textArea->setHeight(height);
        textArea->setTop(top);
        textArea->setLeft(left);
        textArea->setParameter("font_name", "BlueHighway");
        textArea->setParameter("char_height", Ogre::StringConverter::toString(fontSize));
        textArea->setCaption(caption);
        textArea->setParameter("colour_top", "1 1 1");
        textArea->setParameter("colour_bottom", "1 1 1");

        if (show) {
            textArea->show();
        }
        else {
            textArea->hide();
        }

        return textArea;

    }
    //-----------------------------------------------------------------------
    Ogre::OverlayElement* ProfilerOverlay::createPanel(const String& name, Real width, Real height, Real top, Real left, 
                                      const String& materialName, bool show) {

        Ogre::OverlayElement* panel = 
			Ogre::OverlayManager::getSingleton().createOverlayElement("Panel", name);
        panel->setMetricsMode(Ogre::GMM_PIXELS);
        panel->setWidth(width);
        panel->setHeight(height);
        panel->setTop(top);
        panel->setLeft(left);
        panel->setMaterialName(materialName);

        if (show) {
            panel->show();
        }
        else {
            panel->hide();
        }

        return panel;
		
    }
    //-----------------------------------------------------------------------

}
