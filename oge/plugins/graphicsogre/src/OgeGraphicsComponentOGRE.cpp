/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeGraphicsComponentOGRE.h"
#include "oge/graphics/OgeGraphicsSceneManagerOGRE.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"
#include "oge/object/OgeObject.h"

#include "OgreEntity.h"
#include "OgreSceneNode.h"
#include "OgreSceneManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    bool GraphicsComponentOGRE::_reset(const MessageList& params, SceneManager* sceneManager)
    {
        const String& objId = getObjectId();
        Ogre::SceneManager *ogreSM = ((GraphicsSceneManagerOGRE*)sceneManager)->getOgreSceneManager();

        const String *meshName;
        if (params.find( ObjectMessage::INIT_GRAPHICS_MESH, meshName ))
        {
            assert(!mEntity && !mNode && !mOffsetNode && !meshName->empty());
            try
            {
                // NOTE See GraphicsSceneManagerOGRE::doObjectIdRayQuery !!!
                mEntity = ogreSM->createEntity(objId + "Entity", *meshName);
            }
            catch (const Ogre::FileNotFoundException &)
            {
                return false;
                /** OR:
                #ifdef _DEBUG
                meshName = "sphere.mesh"; // TODO Put in debug a "big yellow sphere"
                #else
                meshName = "sphere.mesh"; // and in release a "small nonvisible dot"
                #endif*/
            }

            mNode = ogreSM->getRootSceneNode()->createChildSceneNode(objId + "EntityNode");
            mOffsetNode = mNode->createChildSceneNode(objId + "EntityOffsetNode");
            mOffsetNode->attachObject(mEntity);
        }
        else
        {
            LOGW( "INIT_GRAPHICS_MESH message could not be found or its parameters are invalid "
                  "while resetting a '" + getType() + "' component! Owner object Id: " + getObjectId() );
            return false;
        }

        bool enableShadows;
        if (params.find( ObjectMessage::SET_SHADOWS_ENABLED, enableShadows ))
        {
            mEntity->setCastShadows(enableShadows);
        }

        // Careful !!! INIT_GRAPHICS_MESH must be called first!
        for (MessageList::ConstIter it = params.list.begin(); it != params.list.end(); ++it)
        {
            _processMessage(*it);
        }

        return true;
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_destroy(SceneManager* sceneManager)
    {
        mOffsetNode->detachAllObjects();
        ((GraphicsSceneManagerOGRE*)sceneManager)
            ->getOgreSceneManager()->destroySceneNode(mOffsetNode);
        mOffsetNode = 0;

        mNode->detachAllObjects();
        // mSceneManager->getRootSceneNode()->removeChild( gc->_getNode() );
        // TODO This is not recommanded by ogre !!! Why? Performance issues?
        //      Is this the origin of the memory leak above?
        ((GraphicsSceneManagerOGRE*)sceneManager)
            ->getOgreSceneManager()->destroySceneNode(mNode);
        mNode = 0;

        ((GraphicsSceneManagerOGRE*)sceneManager)
            ->getOgreSceneManager()->destroyEntity(mEntity); // the ptr is invalid now
        mEntity = 0;
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_activate(bool activate, SceneManager* sceneManager)
    {
        LOGWC( activate && mEntity->isInScene(),
            "Reactivating a '" + getType() + "' component that was already activated! Address: " +
            StringUtil::toString(this, 0, ' ', std::ios::hex | std::ios::showbase) );
        LOGWC( !activate && !mEntity->isInScene(),
            "Deactivating a '" + getType() + "' component that was already deactivated! Address: " +
            StringUtil::toString(this, 0, ' ', std::ios::hex | std::ios::showbase) );

        // See: Ogre::MovableObject::setVisible()
        if (activate)
        {
            ((GraphicsSceneManagerOGRE*)sceneManager)
                ->getOgreSceneManager()->getRootSceneNode()->addChild(mNode);
        }
        else
        {
            ((GraphicsSceneManagerOGRE*)sceneManager)
                ->getOgreSceneManager()->getRootSceneNode()->removeChild(mNode);
        }
    }
    //-------------------------------------------------------------------------
    Component::UpdateRate GraphicsComponentOGRE::_getUpdateRate() const
    {
		return Component::NO_UPDATE;
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_update(double deltaTime)
    {
        LOGW("Should not be called!");
    }
    //-------------------------------------------------------------------------
	const Quaternion GraphicsComponentOGRE::getOrientation() const
	{
		return Quaternion(mNode->getOrientation().ptr());
	}
    //-------------------------------------------------------------------------
	const Vector3 GraphicsComponentOGRE::getPosition() const
	{
		return Vector3(mNode->getPosition().ptr());
	}
    //-------------------------------------------------------------------------
    Vector3 GraphicsComponentOGRE::getDimensions() const
    {
        assert(mNode && mEntity);
        // Get its AABB dimensions (AxisAlignedBox::getMaximum() - AxisAlignedBox::getMinimum())
        // It remove the extension factor that is applied to all the bounding boxes
        Ogre::AxisAlignedBox box = mEntity->getBoundingBox();
        box.scale(mNode->getScale());
        Vector3 min(box.getMinimum().ptr());
        Vector3 max(box.getMaximum().ptr());
        return Vector3(fabs(max.x-min.x), fabs(max.y-min.y), fabs(max.z-min.z));
    }
    //-------------------------------------------------------------------------
	void GraphicsComponentOGRE::setPosition(const Vector3 position)
	{
		mNode->setPosition( Ogre::Vector3(position.ptr()) );
	}
    //-------------------------------------------------------------------------
	void GraphicsComponentOGRE::setOrientation(const Quaternion orientation)
	{
		mNode->setOrientation( Ogre::Quaternion((Ogre::Real*)orientation.ptr()) );
	}
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::showBoundingBox( bool show )
    {
        mNode->showBoundingBox(show);
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_setEntity(Ogre::Entity* entity)
    {
        mEntity = entity;
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_setNode(Ogre::SceneNode* node)
    {
        mNode = node;
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_setPositionByMessage(const Message& message)
    {
        const Vector3 *position = FastAnyCast(Vector3, &message.getParam_1());
        mNode->setPosition( Ogre::Vector3(position->ptr()) );
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_setScaleByMessage(const Message& message)
    {
        const Vector3 *scale = FastAnyCast(Vector3, &message.getParam_1());
        mNode->setScale( Ogre::Vector3(scale->ptr()) );
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_setOrientationByMessage(const Message& message)
    {
        const Quaternion *orientation = FastAnyCast(Quaternion, &message.getParam_1());
        mNode->setOrientation( Ogre::Quaternion((Ogre::Real*)orientation->ptr()) );
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_setPositionOrientationByMessage(const Message& message)
    {
        const Vector3 *position = FastAnyCast(Vector3, &message.getParam_1());
        const Quaternion *orientation = FastAnyCast(Quaternion, &message.getParam_2());
        assert(position && orientation);
        mNode->setPosition( Ogre::Vector3(position->ptr()) );
        mNode->setOrientation( Ogre::Quaternion((Ogre::Real*)orientation->ptr()) );
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_setOffsetPositionByMessage(const Message& message)
    {
        const Vector3 *position = FastAnyCast(Vector3, &message.getParam_1());
        assert(position && "No position");
        mOffsetNode->setPosition( Ogre::Vector3(position->ptr()) );
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_setOffsetOrientationByMessage(const Message& message)
    {
        const Quaternion *orientation = FastAnyCast(Quaternion, &message.getParam_1());
        assert(orientation && "No orientation");
        mOffsetNode->setOrientation( Ogre::Quaternion((Ogre::Real*)orientation->ptr()) );
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_setOffsetPositionOrientationByMessage(const Message& message)
    {
        const Vector3 *position = FastAnyCast(Vector3, &message.getParam_1());
        const Quaternion *orientation = FastAnyCast(Quaternion, &message.getParam_2());
        assert(position && orientation && "No position or orientation");
        mOffsetNode->setPosition( Ogre::Vector3(position->ptr()) );
        mOffsetNode->setOrientation( Ogre::Quaternion((Ogre::Real*)orientation->ptr()) );
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_setDimensionsByMessage(const Message& message)
    {
        const Vector3* dimensions = FastAnyCast(Vector3, &message.getParam_1());
        Vector3 scale = *dimensions / getDimensions();
        mNode->setScale( scale.x, scale.y, scale.z );
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_setMaterialByMessage(const Message& message)
    {
        const String* material = FastAnyCast(String, &message.getParam_1());
        mEntity->setMaterialName(*material);
    }
    //-------------------------------------------------------------------------
    void GraphicsComponentOGRE::_processMessage(const Message& message)
    {
        switch (message.getType())
        {
            case ObjectMessage::SET_POSITION: _setPositionByMessage( message ); return;
            case ObjectMessage::SET_SCALE: _setScaleByMessage( message ); return;
            case ObjectMessage::SET_DIMENSIONS: _setDimensionsByMessage( message ); return;
            case ObjectMessage::SET_ORIENTATION: _setOrientationByMessage( message ); return;
            case ObjectMessage::SET_POSITION_ORIENTATION: _setPositionOrientationByMessage( message ); return;
            case ObjectMessage::SET_GRAPHICS_MATERIAL: _setMaterialByMessage( message ); return;
            case ObjectMessage::SET_OFFSET_POSITION: _setOffsetPositionByMessage( message ); return;
            case ObjectMessage::SET_OFFSET_ORIENTATION: _setOffsetOrientationByMessage( message ); return;
            case ObjectMessage::SET_OFFSET_POSITION_ORIENTATION: _setOffsetPositionOrientationByMessage( message ); return;
            // NEXT default: ++counterOfUnnecessaryMessageProcessedByAComponent;
        }
    }
    //-------------------------------------------------------------------------
}
