/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeCameraComponentOGRE.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"
#include "oge/object/OgeObject.h"

#include "oge/graphics/OgeGraphicsSceneManager.h"
#include "oge/graphics/OgeGraphicsSceneManagerOGRE.h"

#include "OgreEntity.h"
#include "OgreSceneNode.h"

#include "oge/physics/OgePhysicsManager.h" // TODO remove
#include "oge/physics/OgePhysicsComponent.h" // TODO remove

namespace oge
{
    //-------------------------------------------------------------------------
    CameraComponentOGRE::CameraComponentOGRE(const ComponentType& type, const ComponentType& family)
        : CameraComponent(type, family),
        mCamera(0), mCameraName(""), mSceneManager(0), mNode(0), mCameraNode(0)
    {
    }
    //-------------------------------------------------------------------------
    CameraComponentOGRE::~CameraComponentOGRE()
    {
    }
    //-------------------------------------------------------------------------
    bool CameraComponentOGRE::_reset(const MessageList& params, SceneManager* sceneManager)
    {
        // There are only few camera hence it is worthwile to store the ogre::scenemanager
        // in fact it would be a mess to delete without storing this ptr!
        mSceneManager = ((GraphicsSceneManagerOGRE*)sceneManager)->getOgreSceneManager();

        mCameraName = String("camera") + getObjectId();
        // Test if this camera name already exist!
        if (mSceneManager->hasCamera( mCameraName ))
        {
            LOGW("This camera name already exist... we should abort!");
            return false;
        }

        mNode = mSceneManager->createSceneNode(String("BaseCameraNode")+ getObjectId()); // new
        mCameraNode = mNode->createChildSceneNode(String("CameraNode")+ getObjectId()); // new
        mCamera = mSceneManager->createCamera(mCameraName);
        mCameraNode->attachObject( mCamera );
        mCameraNode->setPosition( mCameraNode->getPosition() + Ogre::Vector3(0,15,0) ); // TODO as parameter

        const Vector3 *position;
        if (params.find( ObjectMessage::SET_POSITION, position ))
        {
            mNode->setPosition( position->x, position->y, position->z );
        }
        else
        {
            LOGW( "SET_POSITION message could not be found or its parameters are invalid "
                  "while resetting a '" + getType() + "' component! Owner object Id: " + getObjectId() );
            return false;
        }

#pragma OGE_WARN("Either SET_ORIENTATION or SET_ORIENTATION_EULER should present, LogW only if none of them exist.")
        const Vector3 *orientation;
        if (params.find( ObjectMessage::SET_ORIENTATION_EULER, orientation ))
        {
            mNode->resetOrientation();
            mNode->pitch(Ogre::Degree(orientation->x));
            mNode->yaw(Ogre::Degree(orientation->y));
            mNode->roll(Ogre::Degree(orientation->z));
        }
        else
        {
            LOGW( "SET_ORIENTATION_EULER message could not be found or its parameters are invalid "
                  "while resetting a '" + getType() + "' component! Owner object Id: " + getObjectId() );
            return false;
        }

        Real clip;
        if (params.find( ObjectMessage::SET_CAMERA_NEARCLIP, clip ))
        {
            assert(0 <= clip);
            mCamera->setNearClipDistance( (0 <= clip) ? clip : 1 );
        }

        // TODO How to do it best? setFarClipDistance(INFINITE) with INFINIT = 0
        // but what is the value for hardware that don't support this capability ?
        //if (mRoot->getRenderSystem()->getCapabilities()->hasCapability(RSC_INFINITE_FAR_PLANE))
        if (params.find( ObjectMessage::SET_CAMERA_FARCLIP, clip ))
        {
            assert(0 <= clip);
            mCamera->setFarClipDistance( (0 <= clip) ? clip : 1000 );
        }

        return true;
    }
    //-------------------------------------------------------------------------
    void CameraComponentOGRE::_destroy(SceneManager* sceneManager)
    {
        mNode->detachAllObjects();
        mCameraNode->detachAllObjects();

        mSceneManager->destroySceneNode( mNode->getName() );
        mSceneManager->destroySceneNode( mCameraNode->getName() );
        mNode = 0;
        mCameraNode = 0;

        ((GraphicsSceneManagerOGRE*)sceneManager)
            ->getOgreSceneManager()->destroyCamera( mCameraName ); // the ptr is invalid now
        mCamera = 0;
        mCameraName = "";
    }
    //-------------------------------------------------------------------------
    void CameraComponentOGRE::_activate(bool activate, SceneManager* sceneManager)
    {
        // TODO
    }
    //-------------------------------------------------------------------------
    void CameraComponentOGRE::_update(double deltaTime)
    {
#pragma OGE_WARN("FIXME? Should we use a LinkComponent ? This works pretty well :/")
        PhysicsComponent* comp = (PhysicsComponent*) PhysicsManager::getManager()
            ->getComponent( mOwner, "Physics" );
        if (comp != 0)
        {
            mNode->setPosition( 
                comp->getPosition().x, comp->getPosition().y, 
                comp->getPosition().z  );

            mNode->setOrientation( 
                comp->getOrientation().w, comp->getOrientation().x, 
                comp->getOrientation().y, comp->getOrientation().z );

            mCameraNode->needUpdate();
        }
    }
    //-------------------------------------------------------------------------
    void CameraComponentOGRE::write(Serialiser* serialisePtr)
    {
        // todo
    }
    //-------------------------------------------------------------------------
    void CameraComponentOGRE::read(Serialiser* serialisePtr)
    {
        // todo
    }
    //-------------------------------------------------------------------------
    bool CameraComponentOGRE::fixup()
    {
        // todo
        return true;
    }
    //-------------------------------------------------------------------------
}
