/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeTextRendererOGRE.h"
#include "oge/logging/OgeLogManager.h"

#include "OgreOverlayManager.h"
#include "OgreRenderTarget.h"
#include "OgreRenderWindow.h"
#include "OgreOverlayElement.h"
#include "OgreString.h"
#include "OgreStringConverter.h"
#include "OgreOverlayContainer.h"

namespace oge
{
    //-------------------------------------------------------------------------
    TextRendererOGRE::TextRendererOGRE()
    {
        mOverlayMgr = Ogre::OverlayManager::getSingletonPtr();
        mOverlay = mOverlayMgr->create("overlay1");
        mPanel = static_cast<Ogre::OverlayContainer*>(
            mOverlayMgr->createOverlayElement("Panel", "container1"));
        mPanel->setDimensions(1, 1);
        mPanel->setPosition(0, 0);
        mOverlay->add2D( mPanel );
        mOverlay->show();
    }
    //-------------------------------------------------------------------------
    TextRendererOGRE::~TextRendererOGRE()
    {
        // todo memory leak? 
        // does the overlaymgr destroy all or must be do it manually
    }
    //-------------------------------------------------------------------------
    void TextRendererOGRE::addTextBox(
        const String& id,
        const String& text,
        Ogre::Real x, Ogre::Real y,
        Ogre::Real width, Ogre::Real height)
//        , const Ogre::ColourValue& color)
    {
        Ogre::OverlayElement* textBox = 
            mOverlayMgr->createOverlayElement("TextArea", id);
        textBox->setDimensions(width, height);
        textBox->setMetricsMode(Ogre::GMM_PIXELS);
        textBox->setPosition(x, y);
        textBox->setWidth(width);
        textBox->setHeight(height);

    try {
        textBox->setParameter("font_name", "OgeDebugFont"); // NEXT
    }
    catch(...)
    {
        LOGW("No font named: OgeDebugFont was found.");
    }
        textBox->setParameter("char_height", "16");
        textBox->setColour(Ogre::ColourValue::Red);

        textBox->setCaption(text);

        mPanel->addChild(textBox);
    }
    //-------------------------------------------------------------------------
    void TextRendererOGRE::removeTextBox(const String& id)
    {
        mPanel->removeChild(id);
        mOverlayMgr->destroyOverlayElement(id);
    }
    //-------------------------------------------------------------------------
    void TextRendererOGRE::setText(const String& id, const String& text)
    {
        Ogre::OverlayElement* textBox = mOverlayMgr->getOverlayElement(id);
        if (textBox)
            textBox->setCaption(text);
    }
    //-------------------------------------------------------------------------
    void TextRendererOGRE::printf(const std::string& id,  const char *fmt, ...)
    {
        char text[256];
        va_list ap;

        if (fmt == 0)
        {
            *text=0;
        }
        else
        {
            va_start(ap, fmt);
            vsprintf(text, fmt, ap);
            va_end(ap);
        }

        Ogre::OverlayElement* textBox = mOverlayMgr->getOverlayElement(id);
        textBox->setCaption(text);
    }
    //-------------------------------------------------------------------------
} // namespace
