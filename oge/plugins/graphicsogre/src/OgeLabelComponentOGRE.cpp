/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeLabelComponentOGRE.h"
#include "oge/graphics/OgeGraphicsSceneManagerOGRE.h"
#include "oge/graphics/OgeGraphicsComponentOGRE.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"
#include "oge/object/OgeObject.h"
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/message/OgeMessageList.h"

#include "OgreEntity.h"
#include "OgreSceneNode.h"
#include "OgreSceneManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    bool LabelComponentOGRE::_reset(const MessageList& params, SceneManager* sceneManager)
    {
// TODO get text !
        createMovableText( getObjectId(), String("Test")+getObjectId() );
        createMovableOverlay( getObjectId(), String("Test")+getObjectId() );

        for (MessageList::ConstIter it = params.list.begin(); it != params.list.end(); ++it)
        {
            _processMessage(*it);
        }
        return true;
    }
    //-------------------------------------------------------------------------
    void LabelComponentOGRE::_destroy(SceneManager* sceneManager)
    {
#pragma OGE_WARN("TODO/FIXME: Shouldn't the mMovableText be dettached first?")

        delete mMovableText;  mMovableText = 0;
        delete mMovableOverlay; mMovableOverlay = 0;
        delete mAttrs; mAttrs = 0;


/* GraphicsComponent code to remove or adapt?
        mNode->detachAllObjects();
        // mSceneManager->getRootSceneNode()->removeChild( gc->_getNode() );
        // TODO This is not recommanded by ogre !!! Why? Performance issues?
        //      Is this the origin of the memory leak above?
        ((GraphicsSceneManagerOGRE*)sceneManager)
            ->getOgreSceneManager()->destroySceneNode(mNode);
        mNode = 0;

        ((GraphicsSceneManagerOGRE*)sceneManager)
            ->getOgreSceneManager()->destroyEntity(mEntity); // the ptr is invalid now
        mEntity = 0;
*/
    }
    //-------------------------------------------------------------------------
    void LabelComponentOGRE::_activate(bool activate, SceneManager* sceneManager)
    {
#pragma OGE_WARN("TODO WIP Make this customisable. Not sure yet how to pass a toggle signal")
//mMovableOverlay->setVisible(activate); very expensive ... :(
mMovableText->setVisible(activate);

/*  GraphicsComponent code to remove or adapt?
        LOGWC( activate && mEntity->isInScene(),
            "Reactivating a '" + getType() + "' component that was already activated! Address: " +
            StringUtil::toString(this, 0, ' ', std::ios::hex | std::ios::showbase) );
        LOGWC( !activate && !mEntity->isInScene(),
            "Deactivating a '" + getType() + "' component that was already deactivated! Address: " +
            StringUtil::toString(this, 0, ' ', std::ios::hex | std::ios::showbase) );

        // See: Ogre::MovableObject::setVisible()
        if (activate)
        {
            ((GraphicsSceneManagerOGRE*)sceneManager)
                ->getOgreSceneManager()->getRootSceneNode()->addChild(mNode);
        }
        else
        {
            ((GraphicsSceneManagerOGRE*)sceneManager)
                ->getOgreSceneManager()->getRootSceneNode()->removeChild(mNode);
        }
*/
    }
    //-------------------------------------------------------------------------
    Component::UpdateRate LabelComponentOGRE::_getUpdateRate() const
    {
        return Component::EVERY_TICK;
    }
    //-------------------------------------------------------------------------
    void LabelComponentOGRE::_update(double deltaTime)
    {
        //LOGW("Should not be called!");
        // MovableOverlay part
        mMovableOverlay->update(1);

        // NEXT Can this be moved in the class considering RectLayoutManager?
        if (mMovableOverlay->isVisible() && mMovableOverlay->isOnScreen()) 
        {
            //  See OgeLabelComponentOGRE.h at the bottom 
            //  (or http://www.ogre3d.org/wiki/index.php/MovableTextOverlay )
            //  RectLayoutManager::Rect r( 
            //      mMovableOverlay->getPixelsLeft(),
            //      mMovableOverlay->getPixelsTop(),
            //      mMovableOverlay->getPixelsRight(),
            //      mMovableOverlay->getPixelsBottom());
            //
            //RectLayoutManager::RectList::iterator it = m.addData(r);
            //if (it != m.getListEnd())
            //{
            //  mMovableOverlay->setPixelsTop((*it).getTop());
              mMovableOverlay->enable(true);
            //}
            //else
            //  mMovableOverlay->enable(false);
        }
        else
            mMovableOverlay->enable(false);
    }
    //-------------------------------------------------------------------------
    void LabelComponentOGRE::createMovableText(const String& id, const String& caption)
    {
        assert(!mMovableText);
        mMovableText = new Ogre::MovableTextOGRE( (const Ogre::String&) id, (const Ogre::String&) caption );

        // Attach to node
        GraphicsComponentOGRE* graphics =
            (GraphicsComponentOGRE*)(GraphicsManager::getManager()->getComponent( getObjectId(), "Graphics" ));

        if (0 == graphics)
        {
            LOGW(String("Problem: either we have an MT problem: the GraphicsComponent was not created yet") +
                " or there is no GraphicsComponent");
            return; // we should perhaps simply assert
        }
        else if (0 == graphics->_getEntity())
        {
            LOGW(String("Problem: either we have an MT problem: the entity was not created yet") +
                " or the GraphicsComponent doesn't have one.");
            return; // we should perhaps simply assert
        }

        mMovableText->setTextAlignment(Ogre::MovableTextOGRE::H_CENTER, Ogre::MovableTextOGRE::V_ABOVE);
        //  mMovableText->setAdditionalHeight( 2.0f ); 
        mMovableText->setAdditionalHeight( graphics->_getEntity()->getBoundingRadius() );
        mMovableText->setColor(Ogre::ColourValue::Red);
      //  mMovableText->setCharacterHeight(1.0f);

        mNode = graphics->_getNode(); // TODO Dangerous... we should "add" it to the graphical as a child...
        mNode->attachObject(mMovableText);
    }
    //-----------------------------------------------------------------------------
    void LabelComponentOGRE::createMovableOverlay(const String &id, 
        const String &caption)
    {
        GraphicsComponentOGRE* graphics =
            (GraphicsComponentOGRE*)(GraphicsManager::getManager()->getComponent( getObjectId(), "Graphics" ));

        if (0 == graphics)
        {
            LOGW(String("Problem: either we have an MT problem: the GraphicsComponent was not created yet") +
                " or there is no GraphicsComponent");
            return; // we should perhaps simply assert
        }

	    // Create the attributes used by MovableTextOverlay
        // NEXT Can we reuse this attribute across identical info component?
        // TODO Use parameters, catch if bug raised and use default font to prevent termination
        try
        {
            mAttrs = new Ogre::MovableTextOverlayAttributes(
                    "Attrs1",0,"OgeBlueHighway",16,Ogre::ColourValue::White,"OgeExamples/RedTransparent");
        } catch (...) // TODO
        {
            LOGE("Missing materials probably.");
            return;
        }
    
        mMovableOverlay = new Ogre::MovableTextOverlayOGRE(id,caption, graphics->_getEntity(), mAttrs);
        
        mMovableOverlay->enable(false); // make it invisible for now

        // set update frequency to 0.01 seconds
        mMovableOverlay->setUpdateFrequency(0.01); // TODO I should duplicate this in other component :)
    }
    //-----------------------------------------------------------------------------
    void LabelComponentOGRE::showInfo(int index)
    {
        if (index==0)
        {
            mMovableText->setVisible(false);
            mMovableOverlay->setVisible(false);
        }
        else if (index==1)
        {
            mMovableText->setVisible(true);
            mMovableOverlay->setVisible(false);
        }
        else
        {
            mMovableText->setVisible(false);
            mMovableOverlay->setVisible(true);
        }
    }
    //-------------------------------------------------------------------------
    void LabelComponentOGRE::_processMessage(const Message& message)
    {
        switch (message.getType())
        {
            case ObjectMessage::SET_POSITION: _setPositionByMessage( message ); return;
            case ObjectMessage::SET_SCALE: _setScaleByMessage( message ); return;
            //_case ObjectMessage::SET_DIMENSIONS: _setDimensionsByMessage( message ); return;
            case ObjectMessage::SET_ORIENTATION: _setOrientationByMessage( message ); return;
            case ObjectMessage::SET_POSITION_ORIENTATION: _setPositionOrientationByMessage( message ); return;
            default:
                // NEXT ++counterOfUnnecessaryMessageProcessedByAComponent;
                return;
        }
    }
    //-----------------------------------------------------------------------------
/* GraphicsComponent code to remove or adapt?
    Vector3 LabelComponentOGRE::getDimensions() const
    {
        assert(mNode && mEntity);
        // Get its AABB dimensions (AxisAlignedBox::getMaximum() - AxisAlignedBox::getMinimum())
        // It remove the extension factor that is applied to all the bounding boxes
        Ogre::AxisAlignedBox box = mEntity->getBoundingBox();
        box.scale(mNode->getScale());
        Vector3 min(box.getMinimum().ptr());
        Vector3 max(box.getMaximum().ptr());
        return Vector3(fabs(max.x-min.x), fabs(max.y-min.y), fabs(max.z-min.z));
    }
*/
    //-------------------------------------------------------------------------
    void LabelComponentOGRE::showBoundingBox( bool show )
    {
        //segfault! mNode->showBoundingBox(show);
    }
    //-------------------------------------------------------------------------
    void LabelComponentOGRE::_setNode(Ogre::SceneNode* node)
    {
        mNode = node;
    }
    //-------------------------------------------------------------------------
    void LabelComponentOGRE::_setPositionByMessage(const Message& message)
    {
        const Vector3 *position = FastAnyCast(Vector3, &message.getParam_1());
        mNode->setPosition( Ogre::Vector3(position->ptr()) );
    }
    //-------------------------------------------------------------------------
    void LabelComponentOGRE::_setScaleByMessage(const Message& message)
    {
        const Vector3 *scale = FastAnyCast(Vector3, &message.getParam_1());
        mNode->setScale( Ogre::Vector3(scale->ptr()) );
    }
    //-------------------------------------------------------------------------
    void LabelComponentOGRE::_setOrientationByMessage(const Message& message)
    {
        const Quaternion *orientation = FastAnyCast(Quaternion, &message.getParam_1());
        mNode->setOrientation( Ogre::Quaternion((Ogre::Real*)orientation->ptr()) );
    }
    //-------------------------------------------------------------------------
    void LabelComponentOGRE::_setPositionOrientationByMessage(const Message& message)
    {
        const Vector3 *position = FastAnyCast(Vector3, &message.getParam_1());
        const Quaternion *orientation = FastAnyCast(Quaternion, &message.getParam_2());
        assert(position && orientation);
        mNode->setPosition( Ogre::Vector3(position->ptr()) );
        mNode->setOrientation( Ogre::Quaternion((Ogre::Real*)orientation->ptr()) );
    }
    //-------------------------------------------------------------------------
/*    void LabelComponentOGRE::_setDimensionsByMessage(const Message& message)
    {
        const Vector3* dimensions = FastAnyCast(Vector3, &message.getParam_1());
        Vector3 scale = *dimensions / getDimensions();
        mNode->setScale( scale.x, scale.y, scale.z );
    }
*/
    //-------------------------------------------------------------------------
}
