/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeExtendedCameraComponentOGRE.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"
#include "oge/object/OgeObject.h"

#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/graphics/OgeGraphicsSceneManager.h"
#include "oge/graphics/OgeGraphicsSceneManagerOGRE.h"
#include "oge/graphics/OgeGraphicsComponent.h"

#include "OgreEntity.h"
#include "OgreSceneNode.h"

#include "oge/physics/OgePhysicsManager.h"
#include "oge/physics/OgePhysicsComponent.h"

namespace oge
{
    //-------------------------------------------------------------------------
    ExtendedCameraComponentOGRE::ExtendedCameraComponentOGRE(
            const ComponentType& type,
            const ComponentType& family) :
            CameraComponent(type, family),
            mCameraMode(FIRST_PERSON),
            mCamera(0),
            mCameraName(""),
            mSceneManager(0),
            mViewerNode(0),
            mSightNode(0),
            mChaseNode(0),
            mFixedPosition(Ogre::Vector3::ZERO),
            mTightness(1.0f),
            mFixedYaw(true),
            mLastTick(0),
            mViewerOffset(Ogre::Vector3::ZERO),
            mViewerSightOffset(Ogre::Vector3::ZERO),
            mSightOffset(Ogre::Vector3::ZERO),
            mChaseOffset(Ogre::Vector3::ZERO)
    {
    }
    //-------------------------------------------------------------------------
    ExtendedCameraComponentOGRE::~ExtendedCameraComponentOGRE()
    {
    }
	Quaternion ExtendedCameraComponentOGRE::getOrientation() const
	{
		return mViewerNode->getOrientation().ptr();
	}
	Vector3 ExtendedCameraComponentOGRE::getPosition() const
	{
		return mViewerNode->getPosition().ptr();
	}

    //-------------------------------------------------------------------------
    bool ExtendedCameraComponentOGRE::_reset(const MessageList& params, SceneManager* sceneManager)
    {
        // There are only few camera hence it is worthwile to store 
        // the ogre::scenemanager. In fact it would be a mess to delete 
        // without storing this ptr!
        mSceneManager = ((GraphicsSceneManagerOGRE*)sceneManager)->getOgreSceneManager();

        mCameraName = String("ExtCamera") + getObjectId();
        // Test if this camera name already exist!
        if (mSceneManager->hasCamera( mCameraName ))
        {
            LOGW("This camera name already exist... we should abort!");
            return false;
        }

        // Note below 15 you will often see "below" the terrain... :(
        mViewerOffset = Ogre::Vector3(0,15,0); // TODO as param
        mViewerSightOffset = Ogre::Vector3(0,15,0); // TODO as param
        mSightOffset = Ogre::Vector3(0,0,0);  // TODO as param && be able to change the nodes at run-time
        mChaseOffset = Ogre::Vector3(0,5,-10);  // TODO as param && be able to change the nodes at run-time

        mViewerNode = mSceneManager->createSceneNode  (mCameraName + "ViewerNode");
        mSightNode  = mViewerNode->createChildSceneNode(mCameraName + "SightNode", mSightOffset);
        mChaseNode  = mSightNode->createChildSceneNode(mCameraName + "ChaseNode", mChaseOffset);

        mCamera = mSceneManager->createCamera(mCameraName);

        Real clip;
        if (params.find( ObjectMessage::SET_CAMERA_NEARCLIP, clip ))
        {
            assert(0 <= clip);
            mCamera->setNearClipDistance( (0 <= clip) ? clip : 1 );
        }

        // TODO How to do it best? setFarClipDistance(INFINITE) with INFINITE == 0
        // but what is the value for hardware that don't support this capability ?
        //if (mRoot->getRenderSystem()->getCapabilities()->hasCapability(RSC_INFINITE_FAR_PLANE))
        if (params.find( ObjectMessage::SET_CAMERA_FARCLIP, clip ))
        {
            assert(0 <= clip);
            mCamera->setFarClipDistance( (0 <= clip) ? clip : 1000 );
        }

        // Set initial position - also used by fixed position
        const Vector3 *position;
        if (params.find( ObjectMessage::SET_POSITION, position ))
        {
            mViewerNode->setPosition( position->x, position->y, position->z );
        }
        else
        {
            LOGW( "SET_POSITION message could not be found or its parameters are invalid "
                  "while resetting a '" + getType() + "' component! Owner object Id: " + getObjectId() );
            return false;
        }

        mFixedPosition = mViewerNode->getPosition(); 

        // TODO Set auto tracking as parameter and add camera movement 
        //      along with a "reset sight" which reset the orientation to the mSightNode
        //mCameraNode->setAutoTracking(true, mSightNode); // the camera will always look at the target

		/*
        const Vector3 *axis;
        const bool *fixed;
        if(params.find(ObjectMessage::SET_CAMERA_FIXED_YAW, fixed, axis)) {
            mFixedYaw = *fixed;
            mCameraNode->setFixedYawAxis(mFixedYaw, Ogre::Vector3(axis->x, axis->y, axis->z)); // TODO as param Needed because of auto tracking
        }
        else {
            mCameraNode->setFixedYawAxis(true);
        }
*/
//		mSightNode->setFixedYawAxis(true);
		mSightNode->setFixedYawAxis(true, mSightNode->_getDerivedOrientation().yAxis());
//		mChaseNode->setFixedYawAxis(true, mSightNode->_getDerivedOrientation().yAxis());
		mChaseNode->attachObject( mCamera );

		// make the chase node look at the sight node
		mChaseNode->lookAt(Ogre::Vector3::ZERO,Ogre::Node::TS_PARENT);
        return true;
    }
    //-------------------------------------------------------------------------
    void ExtendedCameraComponentOGRE::_destroy(SceneManager* sceneManager)
    {
        mChaseNode->detachAllObjects();
        mSightNode->detachAllObjects();
        mViewerNode->detachAllObjects();

        mSceneManager->destroySceneNode( mViewerNode->getName() );
        mSceneManager->destroySceneNode( mSightNode->getName() );
        mSceneManager->destroySceneNode( mChaseNode->getName() );
        mViewerNode = 0;
        mSightNode = 0;
        mChaseNode = 0;

        ((GraphicsSceneManagerOGRE*)sceneManager)
            ->getOgreSceneManager()->destroyCamera( mCameraName );
        mCamera = 0;
        mCameraName = "";
    }
    //-------------------------------------------------------------------------
    void ExtendedCameraComponentOGRE::_activate(bool activate, SceneManager* sceneManager)
    {
        // TODO  setCameraMode( ... );
    }
    //-------------------------------------------------------------------------
	void ExtendedCameraComponentOGRE::_preTick(double currentTime) 
	{
		// this is in _preTick so that we move the camera right before
		// graphics renders instead of in _update() which is only called
		// as often as the ObjectManager ticks.

        // TODO We shouldn't update all the time...
		Ogre::Quaternion ownerOrientation = getOwnerOrientation(mOwner.empty() ? getObjectId() : mOwner);
		Ogre::Vector3 ownerPosition = getOwnerPosition(mOwner.empty() ? getObjectId() : mOwner);
		
		switch ( mCameraMode )
        {
        case CameraComponent::FIRST_PERSON:
        case CameraComponent::THIRD_PERSON_CHASED:
			mViewerNode->setOrientation(ownerOrientation);
            update( ownerPosition );
            break;
        case CameraComponent::THIRD_PERSON_FIXED:
            // NOTE Depending on the distance we could update this orientation or not
            update( mFixedPosition );
            break;
        default:
            // NO_UPDATE ... for example no parent object
            break;
        }

        mViewerNode->needUpdate();
        mSightNode->needUpdate();
        mChaseNode->needUpdate();
    }
    //-------------------------------------------------------------------------
	Ogre::Vector3 ExtendedCameraComponentOGRE::getOwnerPosition(const ObjectId& id)
	{
		// prefer to use the graphics component position over physics position
		// because the physics position can update before the graphics due to
		// how object messaging works
        GraphicsComponent* gfxComp = (GraphicsComponent*)
            GraphicsManager::getManager()->getComponent( id, "Graphics" );

		if(gfxComp) {
			return Ogre::Vector3(gfxComp->getPosition().ptr());
		}

		PhysicsComponent* physicsComp = (PhysicsComponent*)
			PhysicsManager::getManager()->getComponent( id, "Physics" );
		if (physicsComp) {
			return Ogre::Vector3(physicsComp->getPosition().ptr());
		}

		return mViewerNode->getPosition();
	}
    //-------------------------------------------------------------------------
	Ogre::Quaternion ExtendedCameraComponentOGRE::getOwnerOrientation(const ObjectId& id)
	{
		// prefer to use the graphics component position over physics position
		// because the physics position can update before the graphics due to
		// how object messaging works
        GraphicsComponent* gfxComp = (GraphicsComponent*)
            GraphicsManager::getManager()->getComponent( id, "Graphics" );
		if(gfxComp) {
			return Ogre::Quaternion((Ogre::Real*)gfxComp->getOrientation().ptr());
		}

		PhysicsComponent* physicsComp = (PhysicsComponent*)
			PhysicsManager::getManager()->getComponent( id, "Physics" );
		if (physicsComp) {
			return Ogre::Quaternion((Ogre::Real*)physicsComp->getOrientation().ptr());
		}

		return mViewerNode->getOrientation();
	}
    //-------------------------------------------------------------------------
    void ExtendedCameraComponentOGRE::update(const Ogre::Vector3& target)
    {
        // don't use tightness in first person mode
        Real tightness = mCameraMode == CameraComponent::FIRST_PERSON ? 1.0 : mTightness;

        Ogre::Vector3 displacement = (target - mViewerNode->getPosition()) * tightness;
        mViewerNode->translate( displacement );
	}
    //-------------------------------------------------------------------------
    void ExtendedCameraComponentOGRE::_update(double deltaTime)
    {
		// don't use this because _update is only called as often as the ObjectManager
		// ticks - we use _preTick instead so we update as often as the graphics
		// manager ticks
    }
    //-------------------------------------------------------------------------
    void ExtendedCameraComponentOGRE::instantUpdate(const Ogre::Vector3& target)
    {
        mViewerNode->setPosition( target );
    }
    //-------------------------------------------------------------------------
    void ExtendedCameraComponentOGRE::write(Serialiser* serialisePtr)
    {
        // todo
    }
    //-------------------------------------------------------------------------
    void ExtendedCameraComponentOGRE::read(Serialiser* serialisePtr)
    {
        // todo
    }
    //-------------------------------------------------------------------------
    bool ExtendedCameraComponentOGRE::fixup()
    {
        // todo
        return true;
    }
	//-------------------------------------------------------------------------
	void ExtendedCameraComponentOGRE::rotateChaseOffset(float yaw, float pitch)
	{
		// yaw about the parent's unit y
		mSightNode->rotate(Ogre::Vector3::UNIT_Y,Ogre::Radian(Ogre::Degree(yaw)),Ogre::Node::TS_PARENT);

		mSightNode->pitch(Ogre::Radian(Ogre::Degree(pitch)));

		/*
		// always keep the same up vector as the viewer node
		Ogre::Vector3 localY = mViewerNode->getOrientation().yAxis();
        Ogre::Quaternion rotationTo = mSightNode->getOrientation().yAxis().getRotationTo(localY);
        mSightNode->rotate(rotationTo, Ogre::Node::TS_PARENT);
		*/
	}
    //-------------------------------------------------------------------------
    void ExtendedCameraComponentOGRE::setCameraMode( int mode )
    {
		Ogre::Quaternion orientation = getOwnerOrientation(mOwner.empty() ? getObjectId() : mOwner);
		Ogre::Vector3 pos = getOwnerPosition(mOwner.empty() ? getObjectId() : mOwner);

        switch ( mode )
        {
        case CameraComponent::FIRST_PERSON:
            //@TODO hide the first person mesh based on param
            mCameraMode = FIRST_PERSON;

			mChaseNode->setPosition(Ogre::Vector3::ZERO);
			mChaseNode->setOrientation(Ogre::Quaternion::IDENTITY);
			mSightNode->setOrientation(Ogre::Quaternion::IDENTITY);

            update(pos);
            break;
        case CameraComponent::THIRD_PERSON_CHASED:
			//@TODO show the first person mesh based on param
            mCameraMode = THIRD_PERSON_CHASED;
			
			// update the base view node to match the owner
			mViewerNode->setPosition(pos);
			mViewerNode->setOrientation(orientation);

			// update the chase node so it is at the chase offset looking where it should
			mChaseNode->setPosition(mChaseOffset);
			mChaseNode->lookAt(Ogre::Vector3::ZERO,Ogre::Node::TS_PARENT);
            update( pos );
            break;
        case CameraComponent::THIRD_PERSON_FIXED:
			//@TODO show the first person mesh based on param
            mCameraMode = THIRD_PERSON_FIXED;
            update( mFixedPosition );
            break;
        case CameraComponent::TOGGLE_MODE:
            if ( mCameraMode == CameraComponent::FIRST_PERSON )
                setCameraMode( CameraComponent::THIRD_PERSON_CHASED );
            else if ( mCameraMode == CameraComponent::THIRD_PERSON_CHASED )
        	    setCameraMode( CameraComponent::THIRD_PERSON_FIXED );
            else if ( mCameraMode == CameraComponent::THIRD_PERSON_FIXED )
                setCameraMode( CameraComponent::FIRST_PERSON );
            else if ( mCameraMode == CameraComponent::DEFAULT_MODE )
                setCameraMode( CameraComponent::FIRST_PERSON );
            break;
        default:
            // NO_UPDATE ... for example no parent object
            break;      
        }
    }
    //-------------------------------------------------------------------------
}
