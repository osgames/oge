/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeDynamicRenderable.h"
#include "OgreHardwareBufferManager.h"
#include "OgreCamera.h"

namespace oge
{
    //-------------------------------------------------------------------------
    DynamicRenderable::DynamicRenderable()
    {
    }
    //-------------------------------------------------------------------------
    DynamicRenderable::~DynamicRenderable()
    {
        delete mRenderOp.vertexData;
        delete mRenderOp.indexData;
    }
    //-------------------------------------------------------------------------
    void DynamicRenderable::initialize(
        Ogre::RenderOperation::OperationType operationType, bool useIndices)
    {
        // Initialize render operation
        mRenderOp.operationType = operationType;
        mRenderOp.useIndexes = useIndices;
        mRenderOp.vertexData = new Ogre::VertexData;
        if (mRenderOp.useIndexes)
            mRenderOp.indexData = new Ogre::IndexData;

        // Reset buffer capacities
        mVertexBufferCapacity = 0;
        mIndexBufferCapacity = 0;

        // Create vertex declaration
        createVertexDeclaration();
    }
    //-------------------------------------------------------------------------
    void DynamicRenderable::prepareHardwareBuffers(size_t vertexCount, 
        size_t indexCount)
    {
        // Prepare vertex buffer
        size_t newVertCapacity = mVertexBufferCapacity;
        if ((vertexCount > mVertexBufferCapacity) ||
            (!mVertexBufferCapacity))
        {
            // vertexCount exceeds current capacity!
            // It is necessary to reallocate the buffer.

            // Check if this is the first call
            if (!newVertCapacity)
                newVertCapacity = 1;

            // Make capacity the next power of two
            while (newVertCapacity < vertexCount)
                newVertCapacity <<= 1;
        }
        else if (vertexCount < mVertexBufferCapacity>>1)
        {
            // Make capacity the previous power of two
            while (vertexCount < newVertCapacity>>1)
                newVertCapacity >>= 1;
        }
        if (newVertCapacity != mVertexBufferCapacity) 
        {
            mVertexBufferCapacity = newVertCapacity;
            // Create new vertex buffer
            Ogre::HardwareVertexBufferSharedPtr vbuf =
                Ogre::HardwareBufferManager::getSingleton().createVertexBuffer(
                    mRenderOp.vertexData->vertexDeclaration->getVertexSize(0),
                    mVertexBufferCapacity,
                    Ogre::HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY); // TODO: Custom HBU_?

            // Bind buffer
            mRenderOp.vertexData->vertexBufferBinding->setBinding(0, vbuf);
        }
        // Update vertex count in the render operation
        mRenderOp.vertexData->vertexCount = vertexCount;

        if (mRenderOp.useIndexes)
        {
            OgreAssert(indexCount <= std::numeric_limits<unsigned short>::max(), "indexCount exceeds 16 bit");

            size_t newIndexCapacity = mIndexBufferCapacity;
            // Prepare index buffer
            if ((indexCount > newIndexCapacity) ||
                (!newIndexCapacity))
            {
            // indexCount exceeds current capacity!
            // It is necessary to reallocate the buffer.

            // Check if this is the first call
            if (!newIndexCapacity)
                newIndexCapacity = 1;

            // Make capacity the next power of two
            while (newIndexCapacity < indexCount)
            newIndexCapacity <<= 1;
        }
        else if (indexCount < newIndexCapacity>>1) 
        {
            // Make capacity the previous power of two
            while (indexCount < newIndexCapacity>>1)
                newIndexCapacity >>= 1;
        }

        if (newIndexCapacity != mIndexBufferCapacity)
        {
            mIndexBufferCapacity = newIndexCapacity;
            // Create new index buffer
            mRenderOp.indexData->indexBuffer =
            Ogre::HardwareBufferManager::getSingleton().createIndexBuffer(
                Ogre::HardwareIndexBuffer::IT_16BIT,
                mIndexBufferCapacity,
                Ogre::HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY); // TODO: Custom HBU_?
            }

            // Update index count in the render operation
            mRenderOp.indexData->indexCount = indexCount;
        }
    }
    //-------------------------------------------------------------------------
    Real DynamicRenderable::getBoundingRadius() const
    {
        return Ogre::Math::Sqrt(std::max(mBox.getMaximum().squaredLength(),
            mBox.getMinimum().squaredLength()));
    }
    //-------------------------------------------------------------------------
    Real DynamicRenderable::getSquaredViewDepth(const Ogre::Camera* cam) const
    {
        Ogre::Vector3 vMin, vMax, vMid, vDist;
        vMin = mBox.getMinimum();
        vMax = mBox.getMaximum();
        vMid = ((vMax - vMin) * 0.5) + vMin;
        vDist = cam->getDerivedPosition() - vMid;

        return vDist.squaredLength();
    }
    //-------------------------------------------------------------------------
}
