/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeDebugOverlay.h"
#include "oge/logging/OgeLogManager.h"

#include "OgreOverlayManager.h"
#include "OgreRenderTarget.h"
#include "OgreRenderWindow.h"
#include "OgreOverlayElement.h"
#include "OgreString.h"
#include "OgreStringConverter.h"

namespace oge
{
    //-------------------------------------------------------------------------
    DebugOverlay::DebugOverlay(Ogre::Root* ogreRoot, 
        Ogre::RenderWindow* renderWindow) :
        mValid(false), mOgreRoot(ogreRoot), mRenderWindow(renderWindow)
    {
        mOgreOverlayManager = Ogre::OverlayManager::getSingletonPtr();
        
        mDebugOverlay = Ogre::OverlayManager::getSingleton().getByName("Core/DebugOverlay");
        if (mDebugOverlay == 0)
        {
            LOGW("Couldn't create the debug overlay.");
            mValid = false;
        }
        else
            mValid = true;
    }
    //-------------------------------------------------------------------------
    DebugOverlay::~DebugOverlay()
    {
    }
    //-------------------------------------------------------------------------
    void DebugOverlay::updateStatistics()
    {
        if (!mValid)
            return;

        static String currFps = "Current FPS: ";
        static String avgFps = "Average FPS: ";
        static String bestFps = "Best FPS: ";
        static String worstFps = "Worst FPS: ";
        static String tris = "Triangle Count: ";

        try {
            Ogre::OverlayElement* guiAvg =
                Ogre::OverlayManager::getSingleton().getOverlayElement("Core/AverageFps");
            Ogre::OverlayElement* guiCurr =
                Ogre::OverlayManager::getSingleton().getOverlayElement("Core/CurrFps");
            Ogre::OverlayElement* guiBest =
                Ogre::OverlayManager::getSingleton().getOverlayElement("Core/BestFps");
            Ogre::OverlayElement* guiWorst =
                Ogre::OverlayManager::getSingleton().getOverlayElement("Core/WorstFps");

            const Ogre::RenderTarget::FrameStats& statistics = mRenderWindow->getStatistics();

            guiAvg->setCaption(avgFps + Ogre::StringConverter::toString(statistics.avgFPS));
            guiCurr->setCaption(currFps + Ogre::StringConverter::toString(statistics.lastFPS));
            guiBest->setCaption(bestFps + Ogre::StringConverter::toString(statistics.bestFPS)
                +" "+Ogre::StringConverter::toString(statistics.bestFrameTime)+" ms");
            guiWorst->setCaption(worstFps + Ogre::StringConverter::toString(statistics.worstFPS)
                +" "+Ogre::StringConverter::toString(statistics.worstFrameTime)+" ms");

            Ogre::OverlayElement* guiTris =
                Ogre::OverlayManager::getSingleton().getOverlayElement("Core/NumTris");
            guiTris->setCaption(tris + Ogre::StringConverter::toString(statistics.triangleCount));

            Ogre::OverlayElement* guiDbg =
                Ogre::OverlayManager::getSingleton().getOverlayElement("Core/DebugText");
            guiDbg->setCaption(mCaption);
        }
        catch(...)
        {
            // ignore
        }
    }
    //-------------------------------------------------------------------------
    void DebugOverlay::showOverlay(bool show)
    {
        if (!mValid)
            return;

        if (show)
            mDebugOverlay->show();
        else
            mDebugOverlay->hide();
    }
    //-------------------------------------------------------------------------
    bool DebugOverlay::isVisible()
    {
        return mDebugOverlay->isVisible();
    }
    //-------------------------------------------------------------------------
}
