/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeAnimationComponentOGRE.h"
#include "oge/graphics/OgeGraphicsSceneManagerOGRE.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"
#include "oge/object/OgeObject.h"
#include "oge/object/OgeObjectManager.h"

#include "OgreEntity.h"
#include "OgreSceneNode.h"
#include "OgreSceneManager.h"
#include "OgreAnimationState.h"

namespace oge
{
    //-------------------------------------------------------------------------
    Component::UpdateRate AnimationComponentOGRE::_getUpdateRate() const
    {
        return Component::EVERY_TICK;    // NEXT: Should be ~33ms
    }
    //-------------------------------------------------------------------------
    void AnimationComponentOGRE::_update(double deltaTime)
    {
        //if(!mAnimationState) return; // This shouldn't happen
        mAnimationState->addTime(deltaTime * 0.001); // in Sec
    }
    //-------------------------------------------------------------------------
    void AnimationComponentOGRE::setAnimationState(const String& stateName, bool loop, bool enable)
    {
        // TODO: 'enable' is not used...
        // NEXT: Shouldn't we disable previous active state(s)? Otherwise they will be composed!
        //        And this class only holds one/last state.
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_ANIMATION_STATE, stateName, loop, 0, getObjectId() );
    }
    //-----------------------------------------------------------------------------
    void AnimationComponentOGRE::_setAnimationStateByMessage(const Message& message)
    {
        // -------------- Animation specific --------------------
        // Set an idle animation
        // WIP Where did we get �Idle� from? 
        // How did this magic constant slip in there?
        // Every mesh has their own set of Animations defined for them. 
        // In order to see all of the Animations for the particular mesh 
        // you are working on, you need to download the OgreMeshViewer 
        // and view the mesh from there.

        const String *stateName    = FastAnyCast(String, &message.getParam_1());
        const bool *loop        = FastAnyCast(bool, &message.getParam_2());
        assert(stateName && loop);

        Ogre::AnimationState* state;

        try
        {
            state = mEntity->getAnimationState(*stateName);
        }
        catch (Ogre::ItemIdentityException &)
        {
            LOGW("This animation state doesn't exist: " + *stateName);
            return;
        }

        mAnimationState = state;
        mAnimationState->setLoop(*loop);
        mAnimationState->setEnabled(true);    // TODO pass also as parameter
    }
    //-----------------------------------------------------------------------------
    void AnimationComponentOGRE::_processMessage(const Message& message)
    {
        switch (message.getType())
        {
            case ObjectMessage::SET_ANIMATION_STATE: _setAnimationStateByMessage( message ); return;
            default: GraphicsComponentOGRE::_processMessage(message);
        }
    }
    //-------------------------------------------------------------------------
}
