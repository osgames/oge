/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/OgeDynamicLines.h"

#include "OgreRoot.h"

namespace oge
{
    //-------------------------------------------------------------------------
    DynamicLines::DynamicLines(OperationType opType)
    {
        initialize(opType,false);
        setMaterial("BaseWhiteNoLighting");
        mOgreRenderSystem = Ogre::Root::getSingleton().getRenderSystem();
        assert(mOgreRenderSystem && "Ogre RenderSystem not yet created!");
        mDirty = true;
    }
    //-------------------------------------------------------------------------
    DynamicLines::~DynamicLines()
    {
        clear();
    }
    //-------------------------------------------------------------------------
    void DynamicLines::setOperationType(OperationType opType)
    {
        mRenderOp.operationType = opType;
    }
    //-------------------------------------------------------------------------
    Ogre::RenderOperation::OperationType DynamicLines::getOperationType() const
    {
        return mRenderOp.operationType;
    }
    //-------------------------------------------------------------------------
    void DynamicLines::addPoint(const Vector3& point, const Vector3& colour)
    {
        mPoints.push_back(Ogre::Vector3(point.ptr()));
        mColors.push_back(Ogre::ColourValue(colour.x, colour.y, colour.z)); // NEXT set the alpha?
        mDirty = true;
    }
    //-------------------------------------------------------------------------
    void DynamicLines::addPoint(Real x, Real y, Real z,
        Real red, Real green, Real blue)
    {
        mPoints.push_back(Ogre::Vector3(x,y,z));
        mColors.push_back(Ogre::ColourValue(red, green, blue)); // NEXT set the alpha?
        mDirty = true;
    }
    //-------------------------------------------------------------------------
    const Ogre::Vector3& DynamicLines::getPoint(unsigned short index) const
    {
        assert(index < mPoints.size() && "Point index is out of bounds!!");
        return mPoints[index];
    }
    //-------------------------------------------------------------------------
    unsigned short DynamicLines::getNumPoints() const
    {
        return (unsigned short)mPoints.size();
    }
    //-------------------------------------------------------------------------
    void DynamicLines::setPoint(unsigned short index, const Ogre::Vector3& value)
    {
        assert(index < mPoints.size() && "Point index is out of bounds!!");

        mPoints[index] = value;
        mDirty = true;
    }
    //-------------------------------------------------------------------------
    void DynamicLines::clear()
    {
        mPoints.clear();
        mColors.clear();
        mDirty = true;
    }
    //-------------------------------------------------------------------------
    void DynamicLines::update()
    {
        if (mDirty) fillHardwareBuffers();
    }
    //-------------------------------------------------------------------------
    void DynamicLines::createVertexDeclaration()
    {
        Ogre::VertexDeclaration *decl = mRenderOp.vertexData->vertexDeclaration;
        decl->addElement(POSITION_BINDING, 0, Ogre::VET_FLOAT3, Ogre::VES_POSITION);
        decl->addElement(POSITION_BINDING, Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3), Ogre::VET_COLOUR, Ogre::VES_DIFFUSE);
    }
    //-------------------------------------------------------------------------
    void DynamicLines::fillHardwareBuffers()
    {
        size_t size = mPoints.size();

        prepareHardwareBuffers(size,0);

        if (!size)
        { 
            mBox.setExtents(Ogre::Vector3::ZERO,Ogre::Vector3::ZERO);
            mDirty=false;
            return;
        }

        Ogre::Vector3 vaabMin = mPoints[0];
        Ogre::Vector3 vaabMax = mPoints[0];

        Ogre::HardwareVertexBufferSharedPtr vbuf =
        mRenderOp.vertexData->vertexBufferBinding->getBuffer(0);

        Real *prPos = static_cast<Real*>(vbuf->lock(Ogre::HardwareBuffer::HBL_DISCARD));
        {
            for(size_t i = 0; i < size; i++)
            {
                *prPos++ = mPoints[i].x;
                *prPos++ = mPoints[i].y;
                *prPos++ = mPoints[i].z;

                mOgreRenderSystem->convertColourValue( mColors[i], (Ogre::uint32*)prPos++ );

                if(mPoints[i].x < vaabMin.x)
                vaabMin.x = mPoints[i].x;
                if(mPoints[i].y < vaabMin.y)
                vaabMin.y = mPoints[i].y;
                if(mPoints[i].z < vaabMin.z)
                vaabMin.z = mPoints[i].z;

                if(mPoints[i].x > vaabMax.x)
                vaabMax.x = mPoints[i].x;
                if(mPoints[i].y > vaabMax.y)
                vaabMax.y = mPoints[i].y;
                if(mPoints[i].z > vaabMax.z)
                vaabMax.z = mPoints[i].z;
            }
        }
        vbuf->unlock();

        mBox.setExtents(vaabMin, vaabMax);

        mDirty = false;
    }
    //-------------------------------------------------------------------------
    /*
    void DynamicLines::getWorldTransforms(Matrix4 *xform) const
    {
        // return identity matrix to prevent parent transforms
        *xform = Matrix4::IDENTITY;
    }
    */
    //-------------------------------------------------------------------------
    /*
    const Quaternion &DynamicLines::getWorldOrientation(void) const
    {
        return Quaternion::IDENTITY;
    }
    //-------------------------------------------------------------------------
    const Vector3 &DynamicLines::getWorldPosition(void) const
    {
        return Vector3::ZERO;
    }
    //-------------------------------------------------------------------------
    */
}
