/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/resource/OgeArchiveOGRE.h"
#include "oge/graphics/resource/OgeDataStreamOGRE.h"
#include "oge/filesystem/OgeFileSystem.h"
#include "oge/filesystem/OgeFile.h"

namespace oge
{
    //-------------------------------------------------------------------------
    ArchiveOGRE::ArchiveOGRE(const Ogre::String& name) : 
        Ogre::Archive(name, "oge")
    {
        
    }
    //-------------------------------------------------------------------------
    ArchiveOGRE::~ArchiveOGRE()
    {

    }
    //-------------------------------------------------------------------------
    void ArchiveOGRE::load()
    {
        mArchive = FileSystem::getSingleton().getArchive(mName, true);
    }
    //-------------------------------------------------------------------------
    void ArchiveOGRE::unload()
    {
        mArchive = 0;
    }
    //-------------------------------------------------------------------------
    bool ArchiveOGRE::isCaseSensitive(void) const
    {
        return mArchive->isCaseSensitive();
    }
    //-------------------------------------------------------------------------
// tmp
#if (OGRE_VERSION_MINOR < 7)
    Ogre::DataStreamPtr ArchiveOGRE::open(const Ogre::String& filename) const
#else
    Ogre::DataStreamPtr ArchiveOGRE::open(const Ogre::String& filename,
        bool readOnly) const
#endif
    {
        // TODO Use the readOnly param in 0gre 1.7

        FilePtr file = mArchive->getFile(filename);
        if (!file)
            return Ogre::DataStreamPtr();

        DataStreamPtr actualStream = file->openRead();
        if (!actualStream)
            return Ogre::DataStreamPtr();

        return Ogre::DataStreamPtr(new DataStreamOGRE(actualStream));
    }
    //-------------------------------------------------------------------------
    Ogre::StringVectorPtr ArchiveOGRE::list(bool recursive, bool dirs)
    {
        Ogre::StringVectorPtr filenames(new Ogre::StringVector());

        oge::StringVector actualFilenames = mArchive->getFilenames();
        oge::StringVector::iterator iter = actualFilenames.begin();
        for (; iter != actualFilenames.end(); ++iter)
        {
            filenames->push_back(*iter);
        }

        return filenames;
    }
    //-------------------------------------------------------------------------
    Ogre::FileInfoListPtr ArchiveOGRE::listFileInfo(bool recursive, bool dirs)
    {
        Ogre::FileInfoListPtr fileInfoList(new Ogre::FileInfoList());

        FileVector files = mArchive->getFiles();
        FileVector::iterator iter = files.begin();
        for (; iter != files.end(); ++iter)
        {
            Ogre::FileInfo fi;
            fi.archive = this;
            fi.basename = (*iter)->getFilename();
            fi.filename = (*iter)->getFilename();
            fi.uncompressedSize = 0;
            fi.compressedSize = 0;
            fi.path = "";
        }

        return fileInfoList;
    }
    //-------------------------------------------------------------------------
    Ogre::StringVectorPtr ArchiveOGRE::find(const Ogre::String& pattern, 
        bool recursive, bool dirs)
    {
        Ogre::StringVectorPtr filenames(new Ogre::StringVector());

        oge::StringVector actualFilenames = mArchive->findFilenames(pattern);
        oge::StringVector::iterator iter = actualFilenames.begin();
        for (; iter != actualFilenames.end(); ++iter)
        {
            filenames->push_back(*iter);
        }

        return filenames;
    }
    //-------------------------------------------------------------------------
    bool ArchiveOGRE::exists(const Ogre::String& filename)
    {
        return mArchive->containsFile(filename);
    }
    //-------------------------------------------------------------------------
    time_t ArchiveOGRE::getModifiedTime(const Ogre::String& filename)
    {
        return 0;
    }
    //-------------------------------------------------------------------------
#if (OGRE_VERSION_MINOR <= 8)
    Ogre::FileInfoListPtr ArchiveOGRE::findFileInfo(const Ogre::String& pattern, 
        bool recursive, bool dirs)
#else
    Ogre::FileInfoListPtr ArchiveOGRE::findFileInfo(const Ogre::String& pattern, 
        bool recursive, bool dirs) const
#endif
    {
        Ogre::FileInfoListPtr fileInfoList(new Ogre::FileInfoList());

        FileVector files = mArchive->findFiles(pattern);
        FileVector::iterator iter = files.begin();
        for (; iter != files.end(); ++iter)
        {
            Ogre::FileInfo fi;
            fi.archive = this;
            fi.basename = (*iter)->getFilename();
            fi.filename = (*iter)->getFilename();
            fi.uncompressedSize = 0;
            fi.compressedSize = 0;
            fi.path = "";

            fileInfoList->push_back(fi);
        }

        return fileInfoList;
    }
    //-------------------------------------------------------------------------
    ArchiveFactoryOGRE::ArchiveFactoryOGRE() : mType("oge")
    {

    }
    //-------------------------------------------------------------------------
    const Ogre::String& ArchiveFactoryOGRE::getType() const
    {
        return mType;
    }
    //-------------------------------------------------------------------------
    Ogre::Archive* ArchiveFactoryOGRE::createInstance(const Ogre::String& name) 
    {
        return new ArchiveOGRE(name);
    }
    //-------------------------------------------------------------------------
    void ArchiveFactoryOGRE::destroyInstance(Ogre::Archive* archive)
    {
        delete archive;
    }
    //-------------------------------------------------------------------------
}
