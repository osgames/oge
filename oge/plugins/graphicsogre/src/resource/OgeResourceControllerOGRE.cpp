/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/graphics/resource/OgeResourceControllerOGRE.h"
#include "oge/filesystem/OgeFileSystem.h"
#include "OgreResourceGroupManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    ResourceControllerOGRE::ResourceControllerOGRE() : 
        ResourceController("Graphics")
    {
        mFileSystem = FileSystem::getSingletonPtr();

        using namespace Ogre;
        mOgreResourceGroupManager = ResourceGroupManager::getSingletonPtr();
    }
    //-------------------------------------------------------------------------
    ResourceControllerOGRE::~ResourceControllerOGRE()
    {

    }
    //-------------------------------------------------------------------------
    bool ResourceControllerOGRE::processOperation(const ResourceOp& operation)
    {
        return false;
    }
    //-------------------------------------------------------------------------
    void ResourceControllerOGRE::notifyArchiveAdded(const String& name)
    {
        
    }
    //-------------------------------------------------------------------------
    void ResourceControllerOGRE::notifyArchiveRemoved(const String& name)
    {

    }
    //-------------------------------------------------------------------------
    void ResourceControllerOGRE::notifyArchivesRemoved(const String& type)
    {
        
    }
    //-------------------------------------------------------------------------
    void ResourceControllerOGRE::notifyAllArchivesRemoved()
    {

    }
    //-------------------------------------------------------------------------
    void ResourceControllerOGRE::notifyArchiveGroupAdded(const String& name)
    {
        ArchiveGroupPtr group = mFileSystem->getArchiveGroup(name);
        if (mResourceGroups.addEntry(name, group))
        {
			if(!mOgreResourceGroupManager->resourceGroupExists(name)) {
				mOgreResourceGroupManager->createResourceGroup(name);
			}
            mOgreResourceGroupManager->addResourceLocation(name, "oge", name);
        }   
    }
    //-------------------------------------------------------------------------
    void ResourceControllerOGRE::notifyArchiveGroupRemoved(const String& name)
    {
        // Ogre throws an exception if the resource group doesn't exist
        try
        {
            mResourceGroups.removeEntry(name);
            mOgreResourceGroupManager->destroyResourceGroup(name);
        }
        catch(...)
        {
            // TODO: Log the error
        }
    }
    //-------------------------------------------------------------------------
    void ResourceControllerOGRE::notifyAllArchiveGroupsRemoved()
    {
        ArchiveGroupMap::Iterator iter = mResourceGroups.begin();
        for (; iter != mResourceGroups.end(); ++iter)
        {
            try
            {
                mOgreResourceGroupManager->destroyResourceGroup(iter->first);
            }
            catch(...)
            {
                // TODO: Log error
            }
        }

        mResourceGroups.removeAll();
    }
    //-------------------------------------------------------------------------
}
