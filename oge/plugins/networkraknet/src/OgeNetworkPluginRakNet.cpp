/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2008 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/network/OgeNetworkPluginRakNet.h"
#include "oge/network/OgeNetworkManagerRakNet.h"
#include "oge/system/OgeSystemManager.h"
#include "oge/logging/OgeLogManager.h"

/* This define is ugly but we when build statically we need to be 
   in the oge namespace and outside when building dynamically.
   Also the entry point is common to all plugins when dynamic
   (pluginRegistration) and unique when static (or there would be conflicts).
*/
#if defined(OGE_NETWORK_RAKNET_STATIC_LIB)
extern "C"
{
    /** @note I am not sure if I need a different name per plugin or if I can reuse it!
     *        See in PluginFramework the plugin.h
     * @note The name registered is used to retrieve the plugin in PluginManager
     *        We use the shortened dll plugin (without _d & .dll/.os/.dynlib)
     */
    OGE_NETWORK_API bool NetworkRakNetPluginStaticInit(const oge::PluginServices* ps, void* hdll)
    {
        oge::PluginManager::PluginRegisterParams rp;
        rp.versionMajor = 1;
        rp.versionMinor = 0;
        rp.hDll = hdll;
        rp.createFct = oge::NetworkPluginRakNet::createInstance;
        rp.destroyFct = oge::NetworkPluginRakNet::destroyInstance;

        if (!ps->mgr->registerPluginInstance((const unsigned char*) "OgeNetworkRakNet", &rp))
            return false;

        // NEXT Note that several plugin can be registered at once!

        return true;
    }
} // extern "C"

//PluginRegistrar StaticPlugin_registrar(PluginRegistrar);

#else // if not defined OGE_Network_RakNet_STATIC_LIB

namespace oge
{
    extern "C"
    {
        /**
         * @note Several plugin can be registered at once with this method.
         * @note The name registered is used to retrieve the plugin in PluginManager
         *        We use the shortened dll plugin (without _d & .dll/.os/.dynlib)
         */
        OGE_NETWORK_API bool pluginRegistration(const PluginServices * ps, void* hdll)
        {
            PluginManager::PluginRegisterParams rp;
            rp.versionMajor = 1;
            rp.versionMinor = 0;
            rp.hDll = hdll;
            rp.createFct = NetworkPluginRakNet::createInstance;
            rp.destroyFct = NetworkPluginRakNet::destroyInstance;

#if _DEBUG
            if (!ps->mgr->registerPluginInstance((const unsigned char*) "OgeNetworkRakNet_d", &rp))
                return false;
#else
            if (!ps->mgr->registerPluginInstance((const unsigned char*) "OgeNetworkRakNet", &rp))
                return false;
#endif
            return true;
        }
        //---------------------------------------------------------------------
        // Plugin factory function
        OGE_NETWORK_API Plugin* createPlugin(PluginManager& manager)
        {
	        return new NetworkPluginRakNet( manager );
        }
        //---------------------------------------------------------------------
        // Plugin cleanup function
        OGE_NETWORK_API void releasePlugin(Plugin* plugin)
        {
	        delete (NetworkPluginRakNet*) plugin;
        }
    } // extern "C"
}

#endif

namespace oge
{
    //-------------------------------------------------------------------------
    NetworkPluginRakNet::NetworkPluginRakNet(PluginManager& mgr) :
        mPluginManager(mgr), mName("NetworkPluginRakNet"), mVersion("0.0.1"), 
        mNetworkManager(0)
    {
        //ogeInitMemoryCheck();
        LOG("NetworkPluginRakNet created.");
    }
    //-------------------------------------------------------------------------
    NetworkPluginRakNet::~NetworkPluginRakNet()
    {
        shutdown();
        LOG("NetworkPluginRakNet deleted.");
    }
    //-------------------------------------------------------------------------
    const String& NetworkPluginRakNet::getName() const
    {
        return mName;
    }
    //-------------------------------------------------------------------------
    const String& NetworkPluginRakNet::getVersion() const
    {
        return mVersion;
    }
    //-------------------------------------------------------------------------
    bool NetworkPluginRakNet::initialise()
    {
        LOG("Initialise NetworkPluginRakNet...");
        mNetworkManager = NetworkManagerRakNet::createSingleton();
        if (!SystemManager::getSingleton().registerSystem(mNetworkManager))
        {
            LOGE("Couldn't register NetworkPluginRakNet as system!");
            return false;
        }
        return true;
    }
    //-------------------------------------------------------------------------
    bool NetworkPluginRakNet::shutdown()
    {
        LOG("Shuting down NetworkPluginRakNet...");
        if (mNetworkManager)
        {
            SystemManager::getSingleton().unregisterSystem(mNetworkManager);
            NetworkManagerRakNet::destroySingleton();
        }

        return true;
    }
    //-------------------------------------------------------------------------
    Plugin* NetworkPluginRakNet::createInstance(Plugin_Params* params)
    {
        return new NetworkPluginRakNet(*params->mgr);
    }
    //-------------------------------------------------------------------------
    int NetworkPluginRakNet::destroyInstance(Plugin* plugin)
    {
        if (!plugin)
            return -1;

        // make sure p can be cast to NetworkPluginRakNet*
        NetworkPluginRakNet* NetworkPlugin = dynamic_cast<NetworkPluginRakNet*>(plugin);

        if (!NetworkPlugin)
            return -1;

        delete NetworkPlugin;
        return 0;
    }
    //-------------------------------------------------------------------------
}
