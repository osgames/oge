/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2008 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
is free software; you can redistribute it and/or modify it under the terms
of the GNU Lesser General Public License (LGPL) as published by the
Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
a written agreement from the 'OGE Team'. The exact wording of this
license can be obtained from the 'OGE Team'. In essence this
OGE Unrestricted License state that the GNU Lesser General Public License
applies except that the software is distributed with no limitation or
requirements to publish or give back to the OGE Team changes made
to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/network/OgeNetworkManagerRakNet.h"
#include "oge/network/OgeNetworkSceneManagerRakNet.h"
#include "oge/network/OgeNetworkComponentRakNet.h"
#include "oge/resource/OgeResourceRegistry.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/object/OgeObjectManager.h"
#include "oge/object/OgeObjectSceneManager.h"
#include "oge/object/OgeObject.h"
#include "oge/physics/OgePhysicsComponent.h"
#include "oge/physics/OgePhysicsManager.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"
#include "GetTime.h"
#include "RakNetVersion.h"
#include "oge/OgeProfiler.h"

namespace oge
{
	//-------------------------------------------------------------------------
	template<> NetworkManagerRakNet* Singleton<NetworkManagerRakNet>::mSingleton = 0;
	//-------------------------------------------------------------------------
	NetworkManagerRakNet::NetworkManagerRakNet() : 
		NetworkManager("NetworkManager"),
		mRakPeer(0)
	{
	}

	//-------------------------------------------------------------------------
	NetworkManagerRakNet::~NetworkManagerRakNet()
	{
	}
    //-----------------------------------------------------------------------------
    void NetworkManagerRakNet::createDefaultSceneManager()
    {
        LOGI("Creating the default RakNet scene manager...");
        
        // This SM will be destroyed by the default ObjectSceneManager
        // as it will add it to its enumerator
        mSceneManager = static_cast<NetworkSceneManager*>(
            mSceneManagerEnumerator->createSceneManager(
                "OGE_Default_SceneManager", "Generic SM"));

        if (mSceneManager == 0) {
            LOGE("A RakNet SceneManager of type 'Generic SM' couldn't be created.");
            return;
        }
    }
    //-------------------------------------------------------------------------
    const String NetworkManagerRakNet::getLibrariesVersion()
    {
        String version("Network: RakNet ");
		version += RAKNET_VERSION;
        version += " \n";
        return version;
    }
	//-------------------------------------------------------------------------
	double NetworkManagerRakNet::getTickFraction(double time)
	{
		double tick, wrapped;

		if(time == 0.0) {
			tick = (mCurrentTime - mZeroTickTime) / (1000.0 / mNetworkTickRate);
		}
		else {
			tick = (time - mZeroTickTime) / (1000.0 / mNetworkTickRate);
		}

		// wrap to the numeric limit of an unsigned short
		wrapped =(((unsigned long long)floor(tick)) % std::numeric_limits<unsigned short>::max());

		// tick - floor(tick) is the fractional part
		return wrapped + (tick - floor(tick));
	}

	//-------------------------------------------------------------------------
	bool NetworkManagerRakNet::initialise()
	{
        // Register the in-build component templates
        ObjectManager* objectMgr = ObjectManager::getSingletonPtr();
        objectMgr->registerComponentTemplate( "Network", new NetworkComponentTemplateRakNet() );

        // Registering the in-build SceneManagerFactories
        mSceneManagerEnumerator->registerSceneManagerFactory(
            new NetworkSceneManagerFactoryRakNet());

        createDefaultSceneManager();

		if (!System::initialise())
			return false;

		return true;
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::setServerName(const String& name)
	{
		mServerName = name;

		// name has changed so update our offline ping response
		_updateOfflinePingResponse();
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::shutdown()
	{
		System::shutdown();

		if(mRakPeer) {
			// shutdown
			mRakPeer->Shutdown(100);

			// destroy the interface
			RakNet::RakPeerInterface::DestroyInstance(mRakPeer);
		}
	}
	//-------------------------------------------------------------------------
	bool NetworkManagerRakNet::connect(const String& host, unsigned short remotePort, 
		unsigned int numAttempts, unsigned int attemptWaitTime)
	{
		mIsServer = false;
		mConnectionStatus = NetworkManager::NOT_CONNECTED;

		if(!mRakPeer) {
			mRakPeer = RakNet::RakPeerInterface::GetInstance();

			RakNet::SocketDescriptor null_descriptor;
			mRakPeer->Startup(1,  &null_descriptor, 1);

			// we use the timestamp feature to synchronize and we must
			// send occasional pings to utilize this feature
			mRakPeer->SetOccasionalPing(true);
		}

		// if we are already connected to this host, 
		// Raknet::RakPeerInterface:Connect() will return false
		RakNet::ConnectionAttemptResult result = mRakPeer->Connect(host.c_str(), remotePort, 0, 0);

		if(result == RakNet::CONNECTION_ATTEMPT_STARTED) {
			mConnectionStatus = NetworkManager::CONNECTING;
		}
		else if(result == RakNet::ALREADY_CONNECTED_TO_ENDPOINT) {
			mConnectionStatus = NetworkManager::CONNECTED;
		}
		else {
			mConnectionStatus = NetworkManager::NOT_CONNECTED;
		}

		return result == RakNet::CONNECTION_ATTEMPT_STARTED;
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_onClientInfo(RakNet::Packet* packet)
	{
		RakNet::RakString rs;
		RakNet::BitStream bs(packet->data,packet->length,false);

		// ignore the message id  bytes
		bs.IgnoreBytes(sizeof(RakNet::MessageID));

		// read the client network id
		unsigned short networkId;
		bs.Read(networkId);

		if(networkId >= mClients.size()) {
			NetworkClient defaultClient;
			defaultClient.id = 0;
			defaultClient.name = "";

			mClients.resize(networkId + 1, defaultClient);
		}

		if(mClients.size() != mClientControlledObjects.size()) {
			mClientControlledObjects.resize(mClients.size());
		}

		bool thisClient;
		bs.Read(thisClient);
		if(thisClient) {
			mNetworkId = networkId;
			std::cout << "This client's network id is " << networkId << std::endl;
		}
		else {
			std::cout << "Other client's network id is " << networkId << std::endl;
		}

		// read the client name
		bs.Read(rs);

		mClients[networkId].id = packet->guid.g;
		mClients[networkId].name = rs.C_String();

		// read the client controlled object
		bs.Read(rs);
		ObjectId objectId = rs.C_String();

		// add to the list of client controlled objects
		// the client controlled objects set does not allow duplicates
		// REMOVED because the objectId here is the objectId on the server and not
		// necessarily the objectId on this machine.  The client controlled object
		// is set in _onCreateObject() where the objectId for on this machine is known.
		/*
		if(networkId != -1) {
			addClientControlledObject(objectId,networkId);
		}
		*/

		if(mNetworkListener) {
			mNetworkListener->notifyClientInfo(networkId, thisClient);
		}
		else {
			std::cout << "_onClientInfo() no network listener!" << std::endl;
		}
	}
	//-------------------------------------------------------------------------
	bool NetworkManagerRakNet::_onCreateObject(RakNet::Packet* packet)
	{
		RakNet::RakString rs;
		RakNet::BitStream bs(packet->data,packet->length,false);

		// ignore the message id  bytes
		bs.IgnoreBytes(sizeof(RakNet::MessageID));

		// read the object network id
		bs.Read(rs);
		String objectNetworkId = rs.C_String();
		assert(objectNetworkId != "/_");

		std::cout << "Creating object with server object id: " << objectNetworkId << std::endl;

		// if the object already exists abort
		ObjectIdMap::iterator ii = mNetworkObjectIds.find(objectNetworkId);
		if(ii != mNetworkObjectIds.end()) {
			std::cout << "ERROR: Object with server object id: " << objectNetworkId << " already exists " << std::endl;
			return false;
		}

		// read the owner of this object
		int ownerId;
		bs.Read(ownerId);

		// read the object type
		bs.Read(rs);
		String objectType = rs.C_String();

		// build the object template
		ObjectTemplate* objectTemplate = new ObjectTemplate(objectType);

		// read the number of components
		unsigned int numComponents = 0;
		bs.Read(numComponents);

		ObjectManager* mgr = ObjectManager::getSingletonPtr();
		for(int i = 0; i < numComponents; ++i) {
			// read the component
			bs.Read(rs);
			objectTemplate->addComponent(rs.C_String());
		}

		// register the object template
		mgr->registerObjectTemplate(objectTemplate);

		// get the params
		MessageList params;
		unsigned int numParams = 0;
		bs.Read(numParams);

		unsigned int msgType, param1Type, param2Type;

		for(int i = 0; i < numParams; ++i) {
			Any param1, param2;
			param1Type = NetworkManager::NONE;
			param2Type = NetworkManager::NONE;

			// get the message type
			bs.Read(msgType);

			// get the param1 type
			bs.Read(param1Type);

			if(param1Type) {
				param1 = _getParam(param1Type, &bs);
			}

			// get the param2 type
			bs.Read(param2Type);

			if(param2Type) {
				param2 = _getParam(param2Type, &bs);
			}

			params.add(msgType, param1, param2);
		}

		ObjectId id = mgr->createObject(objectType, params);
		if(id == "") {
			std::cout << "ERROR: Failed to create object of type : " << objectType << std::endl;
			return false;
		}

		// add to list of client controlled objects if owned by a player
		if(ownerId >= 0) {
			addClientControlledObject(id, ownerId);
		}

		std::cout << "Created object type: " << objectType <<  " num params: " << numParams << " local id: " << id << " ownerId: " << ownerId << std::endl;

		mNetworkObjectIds[objectNetworkId] = id;

		// send the client connected event
		if(mNetworkListener) {
			mNetworkListener->notifyObjectCreated(id, ownerId);
		}

		return true;
	}

	//-------------------------------------------------------------------------
	NetworkManagerRakNet* NetworkManagerRakNet::createSingleton()
	{
		if (!mSingleton)
			mSingleton = new NetworkManagerRakNet();

		return mSingleton;
	}

	//-------------------------------------------------------------------------
	bool NetworkManagerRakNet::disconnect(const String& host)
	{
		if(!mRakPeer)
		{
			return false;
		}

		// get a list of existing connections
		RakNet::SystemAddress* systems = NULL;
		unsigned short numSystems = 0;
		if(!mRakPeer->GetConnectionList( systems, &numSystems )) 
		{
			return false;
		}

		if(0 == numSystems) 
		{
			return false;
		}

		if(host.empty()) 
		{
			// close the first connection
			mRakPeer->CloseConnection(systems[0], true);
			return true;
		}
		else 
		{
			// look for the specified connection
			for(int i = 0; i < numSystems; ++i) 
			{
				String remoteHost(systems[i].ToString(false));
				if(0 == remoteHost.compare(host)) {
					mRakPeer->CloseConnection(systems[i], true);
					return true;
				}
			}
		}

		return false;
	}

	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::destroySingleton()
	{
		if (mSingleton)
		{
			delete mSingleton;
			mSingleton = 0;
		}
	}
	//-------------------------------------------------------------------------
	NetworkManager::NetworkClientId NetworkManagerRakNet::_getNetworkIdFromPacket(RakNet::Packet *p)
	{
		for(int i = 0; i < mClients.size(); ++i) {
			if(mClients[i].id == p->guid.g) {
				return i;
			}
		}

		return -1;
	}
	//-------------------------------------------------------------------------
	unsigned char NetworkManagerRakNet::_getPacketIdentifier(RakNet::Packet *p)
	{
		if (p==0)
			return 255;

		if ((unsigned char)p->data[0] == ID_TIMESTAMP)
			return (unsigned char) p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
		else
			return (unsigned char) p->data[0];
	}

	//-------------------------------------------------------------------------
	Poco::Any NetworkManagerRakNet::_getParam(unsigned int paramType, RakNet::BitStream* bs)
	{
		switch(paramType)
		{
		case NetworkManagerRakNet::BOOL: 
			{
				bool param;
				bs->Read(param);
				return Poco::Any(param);
			}
			break;
		case NetworkManagerRakNet::FLOAT: 
			{
				float param;
				bs->Read(param);
				return Poco::Any(param);
			}
			break;
		case NetworkManagerRakNet::DOUBLE: 
			{
				double param;
				bs->Read(param);
				return Poco::Any(param);
			}
			break;
		case NetworkManagerRakNet::STRING: 
			{
				RakNet::RakString param;
				bs->Read(param);
				return Poco::Any(String(param.C_String()));
			}
			break;
		case NetworkManagerRakNet::INT: 
			{
				int param;
				bs->Read(param);
				return Poco::Any(param);
			}
			break;
		case NetworkManagerRakNet::UINT: 
			{
				unsigned int param;
				bs->Read(param); 
				return Poco::Any(param);
			}
			break;
		case NetworkManagerRakNet::VECTOR3: 
			{
				Vector3 param;
				bs->Read(param.x);
				bs->Read(param.y);
				bs->Read(param.z);
				return Poco::Any(param);
			}
			break;					
		case NetworkManagerRakNet::VECTOR4: 
			{
				Quaternion param;
				bs->Read(param.x);
				bs->Read(param.y);
				bs->Read(param.z);
				bs->Read(param.w);
				return Poco::Any(param);
			}
			break;
		}

		return Any();
	}

	//-------------------------------------------------------------------------
	bool NetworkManagerRakNet::listen(unsigned short port, unsigned short maxConnections, int serverType)
	{
		double deltaTime = 1000.0 / mNetworkTickRate;

		// calculate the initial tick time on the server when it first starts
		mZeroTickTime = floor(mCurrentTime / deltaTime) * deltaTime;
		mLastNetworkTickTime = mZeroTickTime;
		std::cout << "Setting Server Zero Tick Time: " << mZeroTickTime << std::endl;

		mIsServer = true; 

		RakNet::StartupResult result;

		mMaxConnections = maxConnections;

		NetworkClient defaultClient;
		defaultClient.id = 0;
		defaultClient.name = "";

		mClients.resize(maxConnections, defaultClient);

		if(!mRakPeer) {
			mRakPeer = RakNet::RakPeerInterface::GetInstance();
		}
		else if(0 == mRakPeer->GetMaximumIncomingConnections()) {
			// mRakPeer is in client mode, restart it in server mode
			mRakPeer->Shutdown(0);
		}

		// we use the timestamp feature to synchronize and we must
		// send occasional pings to utilize this feature
		mRakPeer->SetOccasionalPing(true);

		// update our offline ping response
		_updateOfflinePingResponse();

		// multiple calls to startup() while already active are ignored
		RakNet::SocketDescriptor socket_desc(port, 0);
		result = mRakPeer->Startup(mMaxConnections + 2,  &socket_desc, 1);
		
		// create a client if in listen server mode and not already connected.
		// In listen server mode, id 0 is always the server player client id
		if(serverType == NetworkManager::LISTEN_SERVER && !mClients[0].id ) {
			RakNet::Packet p;
			p.guid = mRakPeer->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS);
			_onClientConnect(&p);
		}

		// set max incoming connections
		mRakPeer->SetMaximumIncomingConnections(mMaxConnections + 2);

		mServerType = serverType;

		return (result == RakNet::RAKNET_STARTED || result == RakNet::RAKNET_ALREADY_STARTED);
	}

	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_sendClientCommand(const ObjectId& id)
	{
		// get all the objects that this client controls and send commands for each
		NetworkComponent* component = static_cast<NetworkComponent*>(mSceneManager->getComponent(id,"Network"));

		if(!component) return;

		RakNet::BitStream bs;

		// write the command id
		bs.Write((RakNet::MessageID)ID_CLIENT_CMD);

		// @TODO write the ObjectId of the object this command is for

		// write the current tick value
		bs.Write(component->getClientCommand().mTick);

		// @TODO write the msec since tick

		// write the command bits
		bs.Write(component->getClientCommand().mCmdBits);

		// write the mouseX & mouseY
		bs.Write(component->getClientCommand().mMouseX);
		bs.Write(component->getClientCommand().mMouseY);

		// write the position & orientation
		// write the position
		bs.Write(component->getClientCommand().mPosition.x);
		bs.Write(component->getClientCommand().mPosition.y);
		bs.Write(component->getClientCommand().mPosition.z);

		// write the orientation
		bs.Write(component->getClientCommand().mOrientation.x);
		bs.Write(component->getClientCommand().mOrientation.y);
		bs.Write(component->getClientCommand().mOrientation.z);
		bs.Write(component->getClientCommand().mOrientation.w);

		// send to the server
		mRakPeer->Send(&bs,HIGH_PRIORITY,UNRELIABLE_SEQUENCED,0,mRakPeer->GetGUIDFromIndex(0),false);
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_sendClientConnectedMessage(NetworkClientId clientId, NetworkClientId otherClientId)
	{
		// tell the new client about each existing client and themselves
		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_CLIENT_CONNECTED);

		// send client network id
		bs.Write((unsigned short)otherClientId);

		// send the flag to let the other client know if this is them
		bs.Write((bool)(otherClientId == clientId));

		// send client name
		bs.Write(RakNet::RakString(mClients[otherClientId].name.c_str()));

		// send client controlled object
		const ObjectId& objectId = getClientControlledObject(otherClientId);
		bs.Write(RakNet::RakString(objectId.c_str()));

		// send to the server
		if(!mRakPeer->Send(&bs,HIGH_PRIORITY,RELIABLE_SEQUENCED,0,RakNet::RakNetGUID(mClients[clientId].id),false)) {
			std::cout << "Failed to send client " << clientId << " connected packet to client " << otherClientId << std::endl;
		}
		else {
			std::cout << "Sent client " << clientId << " a client connected packet for client " << otherClientId << std::endl;
		}
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_sendClientDisconnectedMessage(NetworkManager::NetworkClientId clientId, NetworkClientId otherClientId)
	{
		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_CLIENT_DISCONNECTED);

		// send other client's network id
		bs.Write((unsigned short)otherClientId);

		mRakPeer->Send(&bs,HIGH_PRIORITY,RELIABLE_SEQUENCED,0,RakNet::RakNetGUID(mClients[clientId].id),false);
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_sendClientInfoMessage(NetworkManager::NetworkClientId clientId, NetworkClientId otherClientId)
	{
		// send the client with clientId updated info about otherClientId
		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_CLIENT_INFO);

		// send client network id
		bs.Write((unsigned short)otherClientId);

		// send the flag to let the other client know if this is them
		bs.Write((bool)(otherClientId == clientId));

		// send client name
		bs.Write(RakNet::RakString(mClients[otherClientId].name.c_str()));

		// send client controlled object
		const ObjectId& objectId = getClientControlledObject(otherClientId);
		bs.Write(RakNet::RakString(objectId.c_str()));

		// send to the server
		if(!mRakPeer->Send(&bs,HIGH_PRIORITY,RELIABLE_SEQUENCED,0,RakNet::RakNetGUID(mClients[clientId].id),false)) {
			std::cout << "Failed to send client " << clientId << " client info packet to client " << otherClientId << std::endl;
		}
		else {
			std::cout << "Sent client " << clientId << " a client info packet for client " << otherClientId << std::endl;
		}
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_sendCreateObjectMessage(NetworkManager::NetworkClientId clientId, const ObjectId& id)
	{
		// tell the new client about each existing client and themselves
		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_OBJECT_CREATE);

		// get the objectId for this network client
		std::cout << "Sending create object with id: " << id.c_str() << " to client " << clientId << "." << std::endl;
		bs.Write(RakNet::RakString(id.c_str()));

		// write the networkid of the owner (-1 if no owner)
		NetworkClientId ownerId = getObjectNetworkOwner(id);
		bs.Write(ownerId);

		// get the object type and template
		ObjectType objType = ObjectManager::getSingletonPtr()->getObjectSceneManager()->getObjectType(id);
		ObjectTemplate *objTemplate = ObjectManager::getSingletonPtr()->getObjectTemplate(objType);

		// get the object creation params
		const MessageList& params = ((NetworkSceneManager*)mSceneManager)->getObjectCreationParams(id);

		// @TODO really at this point we should update some of the creation params
		// like the position, orientation, animation etc.
		// though those should be sent in the first update too.
		// eventually we should use the save()/load() component functionality
		// write the object template
		_writeObjectTemplate(objTemplate, params, bs);

		if(!mRakPeer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::RakNetGUID(mClients[clientId].id), false)) {
			std::cout << "Failed to send create object packet to client " << clientId << std::endl;
		}
		else {
			std::cout << "Sent create object packet to client " << clientId << std::endl;
		}
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_sendDestroyObjectMessage(NetworkClientId clientId, const ObjectId& id)
	{
		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_OBJECT_DESTROY);
		bs.Write(id.c_str());
		mRakPeer->Send(&bs, HIGH_PRIORITY,RELIABLE_ORDERED, 0,
			RakNet::RakNetGUID(mClients[clientId].id),
			false);
	}	
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_sendServerInfoMessage(NetworkClientId clientId)
	{
		RakNet::BitStream bsServerInfo;
		bsServerInfo.Write((RakNet::MessageID)ID_TIMESTAMP);
		bsServerInfo.Write(RakNet::GetTime());
		bsServerInfo.Write((RakNet::MessageID)ID_SERVER_INFO);
		bsServerInfo.Write(mNetworkTickRate);
		bsServerInfo.Write(mNetworkTick);
		bsServerInfo.Write(mNumClients);
		for(unsigned int i = 0; i < mClients.size(); ++i) {
			if(mClients[i].id) {
				bsServerInfo.Write(i);
				bsServerInfo.Write(RakNet::RakString(mClients[i].name.c_str()));
			}
		}

		if(!mRakPeer->Send(&bsServerInfo,HIGH_PRIORITY,RELIABLE_ORDERED,0,RakNet::RakNetGUID(mClients[clientId].id),false)) {
			std::cout << "Failed to send server info packet to client " << clientId << std::endl;
		}
		else {
			std::cout << "Sent server info to client " << clientId << std::endl;
		}
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_sendServerUpdates()
	{
		// go through each client and check with the scene manager
		// to determine what objects need creating/destroying/updating
		for(int i = 0; i < mClients.size(); i++) {
			if(mClients[i].id == 0 || i == mNetworkId ) continue;
			
			// run through all the client aware objects and send out actual messages
			const NetworkAwareStatus& awareStatus = mSceneManager->getClientAwareObjects(i);
			NetworkAwareStatus::const_iterator statusItr = awareStatus.begin();

			while(statusItr != awareStatus.end()) {
				// we are using a copy iterator because we might have to erase
				// when an object is destroyed
				NetworkAwareStatus::const_iterator copyItr = statusItr;
				++statusItr;

				switch(copyItr->second) {
					case NetworkSceneManager::NETWORK_OBJECT_CREATE:
						// create the object
						_sendCreateObjectMessage(i, copyItr->first);

						mSceneManager->setClientObjectAwareStatus(
							copyItr->first,NetworkSceneManager::NETWORK_OBJECT_PROCESSED,i);
						break;
					case NetworkSceneManager::NETWORK_OBJECT_UPDATE:
						// update the object
						_sendUpdateObjectMessage(i, copyItr->first);

						mSceneManager->setClientObjectAwareStatus(
							copyItr->first,NetworkSceneManager::NETWORK_OBJECT_PROCESSED,i);
						break;
					case NetworkSceneManager::NETWORK_OBJECT_DESTROY:
						// destroy the object
						_sendDestroyObjectMessage(i, copyItr->first);

						// remove the object from the list
						mSceneManager->removeClientAwareObject(copyItr->first, i);
						break;
					case NetworkSceneManager::NETWORK_OBJECT_NOCHANGE:
					case NetworkSceneManager::NETWORK_OBJECT_PROCESSED:
					default:
						// do nothing
						break;
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_sendUpdateObjectMessage(NetworkClientId clientId, const ObjectId& id)
	{
		NetworkComponent* networkComp = (NetworkComponent*)mSceneManager->getComponent(id, "Network");
		NetworkComponent::TickData data = networkComp->getTickData(networkComp->getLastTick());

		RakNet::BitStream bs;

		// write the packet id
		bs.Write((RakNet::MessageID)ID_NETWORK_COMPONENT_UPDATE);

		// write the tick value for this update
		bs.Write(data.tick);

		// write the object id in a RakString
		bs.Write(RakNet::RakString(id.c_str()));

		// write the position
		bs.Write(data.position.x);
		bs.Write(data.position.y);
		bs.Write(data.position.z);

		// write the orientation
		bs.Write(data.orientation.x);
		bs.Write(data.orientation.y);
		bs.Write(data.orientation.z);
		bs.Write(data.orientation.w);

		// write dirty info (non-tick data that has changed)
		unsigned int dirtyFlags = networkComp->getDirtyFlags();
		bs.Write(dirtyFlags);

		if(dirtyFlags) {
			const NetworkComponent::NonTickData& nonTickData = networkComp->getNonTickData();

			// send updated non-tick data (stuff that isn't likely 
			// to change frequently
			if(dirtyFlags & (1 << NetworkComponent::HEALTH)) {
				bs.Write(nonTickData.health);
			}
			if(dirtyFlags & (1 << NetworkComponent::POWER)) {
				bs.Write(nonTickData.power);
			}
			for(int i = 0; i < 8; i++) {
				if(dirtyFlags & (1 << (NetworkComponent::AMMO + i))) {
					bs.Write(nonTickData.ammo[i]);
				}
			}
			for(int i = 0; i < 8; i++) {
				if(dirtyFlags & (1 << (NetworkComponent::ARMOUR + i))) {
					bs.Write(nonTickData.armour[i]);
				}
			}
			for(int i = 0; i < 8; i++) {
				if(dirtyFlags & (1 << (NetworkComponent::DAMAGE + i))) {
					bs.Write(nonTickData.damage[i]);
				}
			}
		}
		networkComp->setDirtyFlags(0);

		// @TODO write the animation and other stuff

		//std::cout << "Sending object update for " << id << " to client " << clientId << std::endl;

		// send to the client
		mRakPeer->Send(&bs, HIGH_PRIORITY, RELIABLE_SEQUENCED, 0, RakNet::RakNetGUID(mClients[clientId].id), false);
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::tick(double currentTime)
	{
		OgeProfileGroup("Network RAKNET", OGEPROF_SYSTEMS);
		if(!mRakPeer) return;

		// set the current time for other functions to grab
		mCurrentTime = currentTime;

		// don't update if we're a client and not connected/connecting
		if(mIsServer || mConnectionStatus == CONNECTED) {
			// @NOTE: currentTime is in milliseconds and mTickRate is number of ticks per second
			// if mNetworkTickRate = 1 then tick every 1000 milliseconds
			// if mNetworkTickRate = 100 then tick every 10 milliseconds

			// deltaTime is the time in between ticks
			double deltaTime = 1000.0 / mNetworkTickRate;

			// don't tick if it isn't time yet
			if(currentTime - mLastNetworkTickTime < deltaTime) return;

			// tick() isn't called every millisecond so we have to do some math
			// to get the last true tick time
			double tick = floor((currentTime - mZeroTickTime) / deltaTime);
			mLastNetworkTickTime = mZeroTickTime + (tick * deltaTime);	

			// this modulo will wrap the tick value if it exceeds the limits of an unsigned short
			mNetworkTick = (unsigned short)(((unsigned long long)tick) % std::numeric_limits<unsigned short>::max());
		}

		if(mIsServer) {
			_processServerPackets();

			// send server updates
			_sendServerUpdates();

			if((mNetworkTick % mNetworkTickRate) == 0) {
				if(mClients.empty()) {
					//std::cout << "Tick " << mNetworkTick << std::endl;
				}
				else {
					//std::cout << "Tick " << mNetworkTick << " Ping " << mRakPeer->GetLastPing(mRakPeer->GetGUIDFromIndex(0)) << std::endl;
				}
			}
		}
		else { //if(mConnectionStatus != NOT_CONNECTED) {
			_processClientPackets();

			if(mConnectionStatus == CONNECTED && mNetworkId != -1) {

				// send the client command for the object the client currently controls
				// for now just the first object in the list
				if(mNetworkId > -1 && mClientControlledObjects.size() >= mNetworkId) {
					std::set<ObjectId>::iterator si = mClientControlledObjects[mNetworkId].begin();
					if(si != mClientControlledObjects[mNetworkId].end()) {
						_sendClientCommand(*si);
					}
				}

				if((mNetworkTick % mNetworkTickRate) == 0 && mRakPeer && mConnectionStatus == NetworkManager::CONNECTED) {
					//std::cout << "Tick " << mNetworkTick << " Ping " << mRakPeer->GetLastPing(mRakPeer->GetGUIDFromIndex(0)) << std::endl;
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_updateOfflinePingResponse()
	{
		if(!mRakPeer) return;

		RakNet::BitStream bs;
		unsigned char length = std::min<int>((int)mServerName.length(),255);
		bs.Write(length);
		bs.WriteAlignedBytes((const unsigned char *)mServerName.substr(0,length).c_str(),length);
		bs.Write((unsigned int)mMaxConnections);
		bs.Write((unsigned int)mNumClients);

		mRakPeer->SetOfflinePingResponse((const char*)bs.GetData(), bs.GetNumberOfBytesUsed());
	}

	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_onClientCommand(RakNet::Packet* packet)
	{
		RakNet::BitStream bs(packet->data,packet->length,false);

		// ignore the message id  bytes
		bs.IgnoreBytes(sizeof(RakNet::MessageID));

		NetworkClientCommand cmd;

		// read the tick value for when this command was sent
		bs.Read(cmd.mTick);

		// read the command bits
		bs.Read(cmd.mCmdBits);

		// read the mouseX & mouseY
		bs.Read(cmd.mMouseX);
		bs.Read(cmd.mMouseY);

		// relay this command to the correct object's network component
		// the network component will convert the command input into 
		// velocity/angular velocity messages on the next tick for that 
		// network component.

		// read the position & orientation
		bs.Read(cmd.mPosition.x);
		bs.Read(cmd.mPosition.y);
		bs.Read(cmd.mPosition.z);

		bs.Read(cmd.mOrientation.x);
		bs.Read(cmd.mOrientation.y);
		bs.Read(cmd.mOrientation.z);
		bs.Read(cmd.mOrientation.w);

		// get the object id this command is for
		NetworkClientId id = -1;
		for(int i = 0; i < mClients.size(); ++i) {
			if(mClients[i].id == packet->guid.g) {
				id = i;
				break;
			}
		}
		// for now just assume this is for the first client controlled object
		ObjectId objectId = *(mClientControlledObjects[id].begin());

		// std::cout << "cmd.bits " << cmd.mCmdBits << std::endl;

		// maybe this message priority should just be HIGH ?
		ObjectManager::getSingleton().postMessage(
			ObjectMessage::CLIENT_COMMAND, cmd, Any(), 0, objectId, Message::IMMEDIATE);
	}

	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_onClientConnect(RakNet::Packet* packet)
	{
		mNumClients++;

		if(mIsServer) {
			// update our offline ping response
			_updateOfflinePingResponse();

			// first check if the server is trying to connect to add a client
			// this could be for the listen server player or bots
			if(packet->guid ==  mRakPeer->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS)) {
				mNetworkId = 0;
				mClients[0].id = packet->guid.g;
				mClients[0].name = "<no name server>";
				std::cout << "Network Client 0 connected with id: " << packet->guid.g << std::endl;
				mSceneManager->removeAllClientAwareObjects(0);
				
				// clean up any old connection
				if(mClientControlledObjects.size() > 0) {
					mClientControlledObjects[0].clear();
				}

				if(mNetworkListener) {
					mNetworkListener->notifyClientConnected(0, true);
				}
			}
			else {
				NetworkClientId clientId = 0;
				for(int i = 0; i < mClients.size(); ++i) {
					if(0 == mClients[i].id) {
						clientId = i;
						mClients[i].id = packet->guid.g;
						mClients[i].name = "<no name client>";
						std::cout << "Network Client " << i << " connected with id: " << packet->guid.g << std::endl;

						// clean up any old connection
						if(mClientControlledObjects.size() > i) {
							mClientControlledObjects[i].clear();
						}
						mSceneManager->removeAllClientAwareObjects(i);

						if(mNetworkListener) {
							mNetworkListener->notifyClientConnected(i, false);
						}
						break;
					}
				}

				// send server info to the new client
				_sendServerInfoMessage(clientId);
				
				// make this client aware of all existing clients
				for(int i = 0; i < mClients.size(); ++i) {
					if(0 != mClients[i].id) {
						_sendClientInfoMessage(clientId, i);

						if(clientId != i) {
							// make this client aware of the new client
							_sendClientConnectedMessage(i, clientId);
						}
					}
				}
			}
		}
		else {
			RakNet::RakString rs;
			RakNet::BitStream bs(packet->data,packet->length,false);

			// ignore the message id  bytes
			bs.IgnoreBytes(sizeof(RakNet::MessageID));

			// read the client network id
			unsigned short networkId;
			bs.Read(networkId);

			if(networkId >= mClients.size()) {
				NetworkClient defaultClient;
				defaultClient.id = 0;
				defaultClient.name = "";

				mClients.resize(networkId + 1, defaultClient);
			}

			if(mClients.size() != mClientControlledObjects.size()) {
				mClientControlledObjects.resize(mClients.size());
			}

			bool thisClient;
			bs.Read(thisClient);
			if(thisClient) {
				mNetworkId = networkId;
				std::cout << "This client's network id is " << networkId << std::endl;
			}
			else {
				std::cout << "Other client's network id is " << networkId << std::endl;
			}

			// read the client name
			bs.Read(rs);

			mClients[networkId].id = packet->guid.g;
			mClients[networkId].name = rs.C_String();

			// read the client controlled object
			bs.Read(rs);
			ObjectId objectId = rs.C_String();
		
			if(mNetworkListener) {
				mNetworkListener->notifyClientConnected(networkId, thisClient);
			}
		}
	}

	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_onClientDisconnect(RakNet::Packet* packet)
	{
		mNumClients--;

		if(mIsServer) {
			NetworkClientId id = 0;
			for(int i = 0; i < mClients.size(); ++i) {
				if(mClients[i].id == packet->guid.g) {
					id = i;
					break;
				}
			}
			
			// remove all the client's objects
			std::set<ObjectId>::iterator si = mClientControlledObjects[id].begin();
			for(; si != mClientControlledObjects[id].end(); ++si) {
				// remove this client object - when an object is removed
				// other clients are told to destroy this object automatically
				// The networkscenemanager catches the DESTROY_OBJECT message
				// and tells all clients to destroy that object
				ObjectManager::getSingletonPtr()->destroyObject(*si);
			}

			mClientControlledObjects[id].clear();

			// remove all client aware objects
			mSceneManager->removeAllClientAwareObjects(id);

			mClients[id].id = 0;
			mClients[id].name = "";

			// let our other clients know about this client disconnecting
			for(int i = 0; i < mClients.size(); ++i) {
				if(mClients[i].id) {
					_sendClientDisconnectedMessage(i, id);
				}
			}

			if(mNetworkListener) {
				mNetworkListener->notifyClientDisconnected(id);
			}

			// update our offline ping response
			_updateOfflinePingResponse();
		}
		else {
			RakNet::RakString rs;
			RakNet::BitStream bs(packet->data,packet->length,false);

			// ignore the message id  bytes
			bs.IgnoreBytes(sizeof(RakNet::MessageID));

			// read the client network id
			unsigned short networkId;
			bs.Read(networkId);

			mClients[networkId].id = 0;
			mClients[networkId].name = "";

			if(mNetworkListener) {
				mNetworkListener->notifyClientDisconnected(networkId);
			}
		}
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_onCustom(RakNet::Packet* packet)
	{
		RakNet::BitStream bs(packet->data,packet->length,false);

		// ignore the message id  bytes
		bs.IgnoreBytes(sizeof(RakNet::MessageID));

		unsigned int messageType = 0;
		bs.Read(messageType);

		unsigned int length = packet->length - sizeof(RakNet::MessageID) - sizeof(unsigned int);
		unsigned char* data = new unsigned char[length];
		bs.ReadAlignedBytes(data,length);

		// get the object id of the client this came from
		NetworkClientId id = _getNetworkIdFromPacket(packet);

		if(mNetworkListener) {
			mNetworkListener->notifyCustomPacket(id,messageType,data,length);
		}

		delete [] data;
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_processClientPackets()
	{
		RakNet::Packet *packet;

		RakNet::ConnectionState connectionState = mRakPeer->GetConnectionState(mRakPeer->GetGUIDFromIndex(0));

		if( connectionState == RakNet::IS_CONNECTING) {
			mConnectionStatus = NetworkManager::CONNECTING;
		}
		else if(connectionState == RakNet::IS_CONNECTED) {
			mConnectionStatus = NetworkManager::CONNECTED;
		}

		for (packet=mRakPeer->Receive(); packet; mRakPeer->DeallocatePacket(packet), packet=mRakPeer->Receive()) 
		{
			String targetName(packet->systemAddress.ToString(true));

			int id = _getPacketIdentifier(packet);

			if( id != ID_NETWORK_COMPONENT_UPDATE) {
				std::cout << "Received packet id " << id << " from " << targetName << std::endl;
			}

			switch (id)
			{
			case ID_ALREADY_CONNECTED:
			case ID_CONNECTION_REQUEST_ACCEPTED:
				LOG("Connection request to " + targetName + " accepted.");
				std::cout << "Connection request to " << targetName << " accepted." << std::endl;
				mConnectionStatus = NetworkManager::CONNECTED;
				break;
			case ID_INVALID_PASSWORD:
				LOG("Invalid password.");
				mConnectionStatus = NetworkManager::CONNECTION_FAILED;
				break;
			case ID_IP_RECENTLY_CONNECTED:
			case ID_INCOMPATIBLE_PROTOCOL_VERSION:
			case ID_NO_FREE_INCOMING_CONNECTIONS:
			case ID_CONNECTION_BANNED:
			case ID_CONNECTION_ATTEMPT_FAILED:
					LOG("Connection attempt to " + targetName + " failed.");
					std::cout << "Connection attempt to " << targetName << " failed." << std::endl;
					mConnectionStatus = NetworkManager::CONNECTION_FAILED;
				break;
			case ID_CLIENT_INFO:
				_onClientInfo(packet);
				break;
			case ID_CLIENT_CONNECTED:
				_onClientConnect(packet);
				break;
			case ID_CONNECTION_LOST:
				LOG("Connection to " + targetName + " lost.");
				std::cout << "Connection to " << targetName << " lost." << std::endl;
				mConnectionStatus = NetworkManager::CONNECTION_FAILED;
				break;
			case ID_DISCONNECTION_NOTIFICATION:
					LOG("Disconnected from " + targetName + ".");
					std::cout << "Disconnected from " << targetName << "." << std::endl;
					mConnectionStatus = NetworkManager::CONNECTION_DISCONNECTED;
				break;
			case ID_NETWORK_COMPONENT_UPDATE:
				_onUpdateNetworkComponent(packet);
				break;
			case ID_OBJECT_CREATE:
				_onCreateObject(packet);
				break;
			case ID_OBJECT_DESTROY:
				_onDestroyObject(packet);
				break;
			case ID_OGE_CUSTOM:
				_onCustom(packet);
				break;
			case ID_SERVER_INFO:
				_onServerInfo(packet);
				break;
			case ID_UNCONNECTED_PONG:
				_onUnconnectedPong(packet);
				break;
			default:
				// do nothing - still waiting
				break;
			}
		}
	}

	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_processServerPackets()
	{
		RakNet::Packet *packet;

		for (packet=mRakPeer->Receive(); packet; mRakPeer->DeallocatePacket(packet), packet=mRakPeer->Receive()) 
		{
			String targetName(packet->systemAddress.ToString(true));

			int id = _getPacketIdentifier(packet);

			if( id != ID_CLIENT_CMD) {
				std::cout << "Received packet id " << id << " from " << targetName << std::endl;
			}

			switch (_getPacketIdentifier(packet))
			{
			case ID_IP_RECENTLY_CONNECTED:
				LOG("This IP address recently connected from " + targetName + ".");
				break;
			case ID_INCOMPATIBLE_PROTOCOL_VERSION:
				LOG("Incompatible protocol version from " + targetName + ".");
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				LOG("Disconnected from " + targetName + ".");
				std::cout << "Disconnected from " << targetName << "." << std::endl;
				_onClientDisconnect(packet);
				break;
			case ID_CONNECTION_LOST:
				LOG("Connection to " + targetName + " lost.");
				std::cout << "Connection to " << targetName << " lost." << std::endl;
				_onClientDisconnect(packet);
				break;
			case ID_NEW_INCOMING_CONNECTION:
			//case ID_CONNECTION_REQUEST_ACCEPTED:
				LOG("New incoming connection from " + targetName + ".");
				std::cout << "New incoming connection from " << targetName << "." << std::endl;
				_onClientConnect(packet);
				break;
			case ID_CLIENT_CMD:
				_onClientCommand(packet);
				break;
			case ID_OGE_CUSTOM:
				_onCustom(packet);
				break;
			case ID_UNCONNECTED_PONG:
				_onUnconnectedPong(packet);
				break;
				// @TODO receive client chat commands
			}
		}
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::send(NetworkClientId recipient, unsigned int messageType, const unsigned char* data, const int length)
	{
		if(!mRakPeer) return;

		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_OGE_CUSTOM);
		bs.Write(messageType);
		bs.WriteAlignedBytes(data,length);

		mRakPeer->Send(&bs, HIGH_PRIORITY, RELIABLE_SEQUENCED, 0, RakNet::RakNetGUID(mClients[recipient].id), false);
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::sendToAll(unsigned int messageType, const unsigned char* data, const int length)
	{
		if(!mRakPeer) return;

		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_OGE_CUSTOM);
		bs.Write(messageType);
		bs.WriteAlignedBytes(data,length);

		for(int i = 0; i < mClients.size(); i++) {
			if(mClients[i].id) {
				mRakPeer->Send(&bs, HIGH_PRIORITY, RELIABLE_SEQUENCED, 0, RakNet::RakNetGUID(mClients[i].id), false);
			}
		}
	}
	//-------------------------------------------------------------------------
	bool NetworkManagerRakNet::_onDestroyObject(RakNet::Packet* packet) 
	{
		RakNet::RakString rs;
		RakNet::BitStream bs(packet->data,packet->length,false);
		
		// ignore the message id
		bs.IgnoreBytes(sizeof(RakNet::MessageID));

		// get the object id of the object to destroy
		bs.Read(rs);
		ObjectId objectNetworkId(rs.C_String());

		// destroy the object
		ObjectIdMap::iterator ii = mNetworkObjectIds.find(objectNetworkId);
		if(ii != mNetworkObjectIds.end()) {
			std::cout << "destroying object " << ii->second << std::endl;
			ObjectManager::getSingleton().destroyObject(ii->second);
			return true;
		}

		return false;
	}

	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_onServerInfo(RakNet::Packet* packet)
	{
		RakNet::RakString rs;
		RakNet::BitStream bs(packet->data,packet->length,false);
		RakNet::Time timestamp = 0;
		unsigned short serverTick = 0;
		
		// ignore the message id  bytes
		bs.IgnoreBytes(sizeof(RakNet::MessageID));
		bs.Read(timestamp);

		// ignore the message id  bytes
		bs.IgnoreBytes(sizeof(RakNet::MessageID));

		// get the server tick rate
		bs.Read(mServerNetworkTickRate);

		// get the server tick
		bs.Read(serverTick);

		// get number of clients
		bs.Read(mNumClients);

		NetworkClient defaultClient;
		defaultClient.id = 0;
		defaultClient.name = "";

		mClients.clear();
		mClients.resize(mNumClients, defaultClient);

		if(mClientControlledObjects.size() != mNumClients) {
			mClientControlledObjects.resize(mNumClients);
		}

		for(int i = 0; i < mNumClients; ++i) {
			unsigned int clientId;
			// get the client id
			bs.Read(clientId);
			
			// get the client name
			bs.Read(rs);
			mClients[clientId].name = rs.C_String();

			// mark this client as active
			mClients[clientId].id = 1;
		}

		// calculate the zero tick time assuming server and client 
		// have same tick rate for now
		double deltaTime = 1000.0 / mServerNetworkTickRate;
		mZeroTickTime = mCurrentTime - (deltaTime * (double)serverTick);

		double ping = RakNet::GetTime()- timestamp;
		
		// account for latency
		mZeroTickTime -= ping;

		std::cout << "Server Tick Rate: " << mServerNetworkTickRate << " Server Tick: " << serverTick << std::endl;
		std::cout << "Client Zero Tick Time: " << mZeroTickTime << " Ping: " << ping << std::endl;
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_onUnconnectedPong(RakNet::Packet* packet)
	{
		RakNet::RakString rs;
		RakNet::BitStream bs(packet->data,packet->length,false);
		RakNet::TimeMS ping = 0;

		// ignore the message id  bytes
		bs.IgnoreBytes(sizeof(RakNet::MessageID));

		// read the ping
		bs.Read(ping);

		// calculate the ping
		//RakNet::Time ping = RakNet::GetTime() - timestamp;

		// read the server name
		unsigned char length = 0;
		bs.Read(length);
		length = std::min<int>(length,255);
		unsigned char* str = new unsigned char[length];
		bs.ReadAlignedBytes(str,length);
		String serverName;
		serverName.assign((const char *)str, length);
		delete str;

		// read the max number of clients
		unsigned int maxClients = 0;
		bs.Read(maxClients);

		// read the number of connected clients
		unsigned int numClients = 0;
		bs.Read(numClients);

		// read if password required
		bool passwordRequired = false;
		bs.Read(passwordRequired);

		if(mNetworkListener) {
			mNetworkListener->notifyPong(
				packet->systemAddress.ToString(false),
				packet->systemAddress.GetPort(), 
				ping,
				serverName, 
				maxClients, 
				numClients, 
				passwordRequired
			);
		}
	}
	//-------------------------------------------------------------------------
	bool NetworkManagerRakNet::_onUpdateNetworkComponent(RakNet::Packet* packet)
	{
		RakNet::RakString rs;
		RakNet::BitStream bs(packet->data,packet->length,false);
		// ignore the message id  bytes
		bs.IgnoreBytes(sizeof(RakNet::MessageID));

		NetworkComponent::TickData data;

		// read the tick
		unsigned short tick;
		bs.Read(tick);
		data.tick = tick;

		// read the object id used by the server
		bs.Read(rs);
		ObjectId objectId = mNetworkObjectIds[ObjectId(rs.C_String())];

		// get the component for this object
		NetworkComponentRakNet* comp = (NetworkComponentRakNet *)mSceneManager->getComponent(objectId, "Network");
		if(!comp) {
			// @TODO assert/log error here
			return false;
		}

		// read the position
		bs.Read(data.position.x);
		bs.Read(data.position.y);
		bs.Read(data.position.z);

		// read the orientation
		bs.Read(data.orientation.x);
		bs.Read(data.orientation.y);
		bs.Read(data.orientation.z);
		bs.Read(data.orientation.w);

		comp->addTickData(data);

		// write dirty info (non-tick data that has changed)
		unsigned int dirtyFlags;
		bs.Read(dirtyFlags);

		if(dirtyFlags) {
			NetworkComponent::NonTickData nonTickData = comp->getNonTickData();

			// send updated non-tick data (stuff that isn't likely 
			// to change frequently
			if(dirtyFlags & (1 << NetworkComponent::HEALTH)) {
				bs.Read(nonTickData.health);
				std::cout << "got health from server: " << nonTickData.health << std::endl;
			}
			if(dirtyFlags & (1 << NetworkComponent::POWER)) {
				bs.Read(nonTickData.power);
			}
			for(int i = 0; i < 8; i++) {
				if(dirtyFlags & (1 << (NetworkComponent::AMMO + i))) {
					bs.Read(nonTickData.ammo[i]);
				}
			}
			for(int i = 0; i < 8; i++) {
				if(dirtyFlags & (1 << (NetworkComponent::ARMOUR + i))) {
					bs.Read(nonTickData.armour[i]);
				}
			}
			for(int i = 0; i < 8; i++) {
				if(dirtyFlags & (1 << (NetworkComponent::DAMAGE + i))) {
					bs.Read(nonTickData.damage[i]);
				}
			}

			comp->setNonTickData(nonTickData);

			// add the dirty flags to the existing ones
			comp->setDirtyFlags(dirtyFlags | comp->getDirtyFlags());
		}
		
		return true;
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::ping(const String& host, unsigned short remotePort)
	{
		if(!mRakPeer) {
			mRakPeer = RakNet::RakPeerInterface::GetInstance();

			RakNet::SocketDescriptor null_descriptor;
			mRakPeer->Startup(1,  &null_descriptor, 1);

			// we use the timestamp feature to synchronize and we must
			// send occasional pings to utilize this feature
			mRakPeer->SetOccasionalPing(true);
		}

		mRakPeer->Ping( host.c_str(), remotePort, false);
	}
	//-------------------------------------------------------------------------
	void NetworkManagerRakNet::_writeObjectTemplate(ObjectTemplate* objTemplate, 
		const MessageList& params, RakNet::BitStream& bs)
	{
		const ObjectTemplate::ComponentTypeList& componentTypeList = objTemplate->getComponents();
		unsigned int numComponentTypes = (unsigned int)componentTypeList.size();
		unsigned int numParams = (unsigned int)params.list.size();

		// write the object type
		bs.Write(RakNet::RakString(objTemplate->getType().c_str()));

		// write the number of components
		bs.Write(numComponentTypes);

		// write each component
		for(int i = 0; i < numComponentTypes; ++i) {
			bs.Write(RakNet::RakString(componentTypeList[i].c_str()));
		}

		// write the number of params
		bs.Write(numParams);

		// write all the params
		for(MessageList::ConstIter ii = params.list.begin(); ii != params.list.end(); ii++) {
			// message type
			bs.Write(ii->getType());

			// write first param
			_writeParam(ii->getParam_1(), bs);

			// write second param
			_writeParam(ii->getParam_2(), bs);
		}
	}

    //-------------------------------------------------------------------------
	void NetworkManagerRakNet::_writeParam(const Any& param, RakNet::BitStream& bs)
	{
		String type = param.type().name();

		if(param.type() == typeid(bool)) {
			bool paramOut = Poco::AnyCast<bool>(param);
			bs.Write(NetworkManagerRakNet::BOOL);
			bs.Write(paramOut);
		}
		else if(param.type() == typeid(float)) {
			float paramOut = Poco::AnyCast<float>(param);
			bs.Write(NetworkManagerRakNet::FLOAT);
			bs.Write(paramOut);
		}
		else if(param.type() == typeid(double)) {
			double paramOut = Poco::AnyCast<double>(param);
			bs.Write(NetworkManagerRakNet::DOUBLE);
			bs.Write(paramOut);
		}
		else if(param.type() == typeid(String)) {
			String paramTmp = Poco::AnyCast<String>(param);
			RakNet::RakString paramOut(paramTmp.c_str());
			bs.Write(NetworkManagerRakNet::STRING);
			bs.Write(paramOut);
		}
		else if(param.type() == typeid(int)) {
			int paramOut = Poco::AnyCast<int>(param);
			bs.Write(NetworkManagerRakNet::INT);
			bs.Write(paramOut);
		}
		else if(param.type() == typeid(unsigned int)) {
			unsigned int paramOut = Poco::AnyCast<unsigned int>(param);
			bs.Write(NetworkManagerRakNet::UINT);
			bs.Write(paramOut);
		}
		else if(param.type() == typeid(Vector3)) {
			Vector3 paramOut = Poco::AnyCast<Vector3>(param);
			bs.Write(NetworkManagerRakNet::VECTOR3);
			bs.Write(paramOut.x);
			bs.Write(paramOut.y);
			bs.Write(paramOut.z);
		}
		else if(param.type() == typeid(Quaternion)) {
			Quaternion paramOut = Poco::AnyCast<Quaternion>(param);
			bs.Write(NetworkManagerRakNet::VECTOR4);
			bs.Write(paramOut.x);
			bs.Write(paramOut.y);
			bs.Write(paramOut.z);
			bs.Write(paramOut.w);
		}
		else {
			// we don't know how to handle this type
			LOG("Unknown type in _writeParam(): " + type);
			bs.Write(NetworkManagerRakNet::NONE);
		}
	}
}
