/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/network/OgeNetworkComponentRakNet.h"
#include "oge/network/OgeNetworkManagerRakNet.h"
#include "oge/network/OgeNetworkClientCommand.h"
#include "oge/object/OgeObjectManager.h"
#include "oge/message/OgeMessageList.h"

namespace oge
{
    //-------------------------------------------------------------------------
    bool NetworkComponentRakNet::_reset(const MessageList& params, SceneManager* sceneManager)
    {
		// process messages on a reset
        for (MessageList::ConstIter it = params.list.begin(); it != params.list.end(); ++it) {
            _processMessage(*it);
        }

        return true;
    }
    //-------------------------------------------------------------------------
    Component::UpdateRate NetworkComponentRakNet::_getUpdateRate() const
    {
        return Component::EVERY_TICK;
    }
	//-------------------------------------------------------------------------
	void NetworkComponentRakNet::_postTick(double currentTime)
	{
		if(!NetworkManagerRakNet::getSingletonPtr()->isServer() &&
			NetworkManager::getManager()->getConnectionStatus() == NetworkManager::CONNECTED) {
			// This only needs to happen on the client

			// broadcast all messages related to non-tick data
			_sendNonTickDataMessages();

			// send tick data messages
			_sendTickDataMessages(currentTime);
		}

		NetworkComponent::_postTick(currentTime);
	}
	//-------------------------------------------------------------------------
	void NetworkComponentRakNet::_sendNonTickDataMessages()
	{
		// do nothing if no data has changed
		if(!mDirtyFlags) {
			return;
		}
		
		ObjectManager* mgr = ObjectManager::getSingletonPtr();

		// post messages for updated non-tick data
		if(mDirtyFlags & (1 << NetworkComponent::HEALTH)) {
			mgr->postMessage(ObjectMessage::SET_HEALTH, (int)mNonTickData.health, Any(), 0, getObjectId());
			std::cout << "Posting SET_HEALTH message in NetworkComponentRakNet - " << mNonTickData.health << std::endl;
		}
		if(mDirtyFlags & (1 << NetworkComponent::POWER)) {
			mgr->postMessage(ObjectMessage::SET_POWER, (int)mNonTickData.power, Any(), 0, getObjectId());
		}
		for(int i = 0; i < 8; i++) {
			if(mDirtyFlags & (1 << (NetworkComponent::AMMO + i))) {
				mgr->postMessage(ObjectMessage::SET_AMMO, (int)mNonTickData.ammo[i], i, 0, getObjectId());
			}
		}
		for(int i = 0; i < 8; i++) {
			if(mDirtyFlags & (1 << (NetworkComponent::ARMOUR + i))) {
				mgr->postMessage(ObjectMessage::SET_ARMOUR, (int)mNonTickData.armour[i], i, 0, getObjectId());
			}
		}
		for(int i = 0; i < 8; i++) {
			if(mDirtyFlags & (1 << (NetworkComponent::DAMAGE + i))) {
				mgr->postMessage(ObjectMessage::SET_DAMAGE, (int)mNonTickData.damage[i], i, 0, getObjectId());
			}
		}

		// we've handled the flags
		mDirtyFlags = 0;
	}
	//-------------------------------------------------------------------------
	void NetworkComponentRakNet::_sendTickDataMessages(double currentTime)
	{
		NetworkManagerRakNet *mgr = NetworkManagerRakNet::getSingletonPtr();

		// calculate the current tick and 
		// use that to get the correct position/orientation to 
		// broadcast to other components
		
		//get the tick fraction 100 ms ago
		double tickFraction = mgr->getTickFraction(currentTime - 100.0);
		double fraction = tickFraction - floor(tickFraction);
		unsigned short tick = (unsigned short)(tickFraction - fraction);

		Vector3 position;
		Quaternion orientation;

		unsigned short startTick = tick, endTick = tick;
		unsigned short tickFutureDelta = 1;
		unsigned short tickPastDelta = 0;
		for(tickPastDelta = 0; tickPastDelta < 3; ++tickPastDelta) {
			unsigned short testTick = tick - tickPastDelta;
			if(tick == 0) {
				tickPastDelta = tickPastDelta;
			}
			if(mTickData[testTick % mNumSlots].tick == testTick) {
				startTick = testTick;
				break;
			}
		}

		for(tickFutureDelta = 1; tickFutureDelta < 4; ++tickFutureDelta) {
			unsigned short testTick = tick + tickFutureDelta;
			if(tick == std::numeric_limits<unsigned short>::max()) {
				tickFutureDelta = tickFutureDelta;
			}
			if(mTickData[testTick % mNumSlots].tick == testTick) {
				endTick = testTick;
				break;
			}
		}

		if(tickPastDelta == 3 || tickFutureDelta == 4) {
			// too many missing packets
			//std::cout << "Too many missing packets to interpolate " << std::endl;
			return;
		}

		// interpolate
		fraction = ((double)tickPastDelta + fraction) / (double)(tickPastDelta + tickFutureDelta);

		// start values
		position = mTickData[startTick % mNumSlots].position;
		orientation = mTickData[startTick % mNumSlots].orientation;

		double distance = position.distance(mTickData[endTick % mNumSlots].position);
		double time = tickPastDelta + tickFutureDelta;
		double speed = distance / time;
		//std::cout << "speed: " << speed << std::endl;

		// interpolate with end values
		position = position.lerp(mTickData[endTick % mNumSlots].position, fraction);
		orientation = Quaternion::Slerp(fraction,orientation,mTickData[endTick % mNumSlots].orientation,true);

		// broadcast the position/orientation
		ObjectManager::getSingleton().postMessage(
			ObjectMessage::SET_POSITION, position, 
			Any(), 0, getObjectId(), Message::IMMEDIATE );
		ObjectManager::getSingleton().postMessage(
			ObjectMessage::SET_ORIENTATION, orientation, 
			Any(), 0, getObjectId(), Message::IMMEDIATE );
	}
    //-------------------------------------------------------------------------
    void NetworkComponentRakNet::_update(double deltaTime)
    {
		// do nothing - we just use the update so we get the _postTick()
    }
}