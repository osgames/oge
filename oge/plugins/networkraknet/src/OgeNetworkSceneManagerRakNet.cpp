/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/network/OgeNetworkSceneManagerRakNet.h"
#include "oge/network/OgeNetworkManagerRakNet.h"
#include "oge/network/OgeNetworkComponent.h"
#include "oge/physics/OgePhysicsManager.h"
#include "oge/physics/OgePhysicsComponent.h"

namespace oge
{
    //-------------------------------------------------------------------------
    NetworkSceneManagerFactoryRakNet::NetworkSceneManagerFactoryRakNet()
    {
        // Temporary sm to get the type name
        NetworkSceneManagerRakNet sm("temp");
        sm.initMetaData();
        mType = sm.getType();
    }
    //-------------------------------------------------------------------------
    NetworkSceneManagerFactoryRakNet::~NetworkSceneManagerFactoryRakNet()
    {
    }
    //-------------------------------------------------------------------------
    SceneManager* NetworkSceneManagerFactoryRakNet::createInstance(const String& name)
    {
        NetworkSceneManagerRakNet* sm = new NetworkSceneManagerRakNet(name);
        sm->init();
        assert(sm->getType() == mType);
        return sm;
    }
    //-------------------------------------------------------------------------
    bool NetworkSceneManagerFactoryRakNet::checkAvailability()
    {
        mAvailable = true;
        return true;
    }
    //-------------------------------------------------------------------------


    //-------------------------------------------------------------------------
    NetworkSceneManagerRakNet::NetworkSceneManagerRakNet(const String& name) :
        NetworkSceneManager(name)
    {
        initMetaData();
    }
     //-------------------------------------------------------------------------
    NetworkSceneManagerRakNet::~NetworkSceneManagerRakNet()
    {
        clearScene();
    }
    //-------------------------------------------------------------------------
    void NetworkSceneManagerRakNet::init()
    {
        // Override this method to set your SM in your plugin
    }
    //-------------------------------------------------------------------------
    void NetworkSceneManagerRakNet::initMetaData() const
    {
        mMetaData.type = "Generic SM";
        mMetaData.family = "Network";
        mMetaData.description = "RakNet network scene manager";
        mMetaData.sceneTypeMask = 0; // Not used
        mMetaData.typeName = "DefaultSceneManager"; // used to initialise ogre3d
        mMetaData.worldGeometrySupported = false;
    }
    //-------------------------------------------------------------------------
    void NetworkSceneManagerRakNet::update(double deltaTime)
    {
		// don't execute this code on the client
		if(!NetworkManager::getManager()->isServer()) {
			return;
		}

		// get all the network client controlled object
		NetworkManagerRakNet* mgr = static_cast<NetworkManagerRakNet*>(NetworkManagerRakNet::getManager());
		double networkTickRate = mgr->getNetworkTickRate();
		double zeroTickTime = mgr->getZeroTickTime();
		// deltaTime is the time in between network ticks
		double networkTickDelta = 1000.0 / networkTickRate;

		// calculate the most recent network tick before the last physics tick
		// this is the most recent tick data we can send to the client
		// @NOTE physics and network ticks don't happen at the same time
		double physicsTickTime = PhysicsManager::getManager()->getLastTickTime();
		double tick = floor((physicsTickTime - zeroTickTime) / networkTickDelta);
		unsigned short wrappedTick = ((unsigned long long)tick) % std::numeric_limits<unsigned short>::max();

		// tick time is the most recent physics update tick time but wrapped by the limits of
		// the number of ticks an unsigned short gives us
		double tickTime = zeroTickTime + ((double)wrappedTick * networkTickDelta);

		// @TODO 
		// here is where we would filter the client aware objects so that
		// clients are only aware of objects they need to be aware of
		// based on audio & visual areas

		// for now just assume each client is controlling one object and that object
		// determines what other objects they should be aware of
		const NetworkManager::NetworkClientObjects& clientObjects = mgr->getClientControlledObjects();

		for(ComponentMap::iterator ci = mComponents.begin(); ci != mComponents.end(); ci++) {
			NetworkComponent* networkComp = (NetworkComponent*)ci->second;
			NetworkComponent::TickData data = networkComp->getTickData(networkComp->getLastTick());
			bool shouldUpdate = false;

			// see if the last tick data we sent to the client is the same or 
			// more recent than the tick data we are about to send
			// if not then update the tick data
			if(data.tick < wrappedTick) {
				// get the latest position/orientation
				PhysicsComponent* physicsComp = (PhysicsComponent*)
					PhysicsManager::getManager()->getComponent( networkComp->getObjectId(), "Physics" );
				if(!physicsComp) {
					continue;
				}
				Quaternion orientation = physicsComp->getOrientation();
				Vector3 position       = physicsComp->getPosition();

				// don't interpolate over 5 ticks
				if(wrappedTick - data.tick > 5) {
					data.orientation = orientation;
					data.position = position;
				}
				else {
					// last tick time is the time of the last tick but wrapped by the limits of
					// the number of ticks an unsigned short gives us
					double lastTickTime = zeroTickTime + ((double)data.tick * networkTickDelta);

					// tickTime should fall between lastTickTime and physicsTickTime
					double fraction = (tickTime - lastTickTime) / (physicsTickTime - lastTickTime);

					// interpolate and save the data
					data.orientation = Quaternion::Slerp(fraction, data.orientation, orientation, true);
					data.position = data.position.lerp(position, fraction);
				}

				data.tick = wrappedTick;

				// update the component with latest data
				networkComp->addTickData(data);
				shouldUpdate = true;
			}
//#define DEBUG_NETWORK
#ifdef DEBUG_NETWORK
			if(ci == mComponents.begin()) {
				data.tick = wrappedTick;
				data.position.z = wrappedTick % 500;
				data.orientation = Quaternion::IDENTITY;
				networkComp->addTickData(data);
				shouldUpdate = true;
			}
#endif

			const NetworkManager::NetworkClients& clients = mgr->getClients();
			for(int i = 0; i < clients.size(); ++i) {
				if(clients[i].id == 0) continue;
				if(i >= mClientAwareObjects.size()) continue;

				NetworkAwareStatus::iterator statusIter = 
					mClientAwareObjects[i].find(ci->first);

				if(statusIter == mClientAwareObjects[i].end()) {
					// this client is not aware of this object so create it
					mClientAwareObjects[i][ci->first] = NETWORK_OBJECT_CREATE;
				}
				else if(shouldUpdate) {
					// @TODO simple distance squared check to set status 
					// to NETWORK_OBJECT_NOUPDATE if the object is far enough
					// away that the client doesn't need to know about it.
					statusIter->second = NETWORK_OBJECT_UPDATE;
				}
			}
		}

        NetworkSceneManager::update(deltaTime);
    }
	//-------------------------------------------------------------------------
	AsyncBool NetworkSceneManagerRakNet::createComponent(const ObjectId& id, const ComponentType& type,
            const MessageList& params)
	{
		// @TODO 
		//ObjectManager::getSingletonPtr()->getObjectTemplate();
		return NetworkSceneManager::createComponent(id, type, params);
	}
    //-------------------------------------------------------------------------
    void NetworkSceneManagerRakNet::clearScene()
    {
        SceneManager::clearScene();
    }
    //-------------------------------------------------------------------------
    bool NetworkSceneManagerRakNet::processMessage(const Message& message)
    {
        switch (message.getType())
        {
			case ObjectMessage::DESTROY_OBJECT:
				{
					// get the object id of the object to destroy
					const ObjectId* id = FastAnyCast(ObjectId, &message.getParam_1());

					// have all clients that are aware of this object destroy it
					for(int i = 0; i < mClientAwareObjects.size(); i++) {
						NetworkAwareStatus::iterator statusItr = mClientAwareObjects[i].find(*id);
						if(statusItr != mClientAwareObjects[i].end()) {
							statusItr->second = NETWORK_OBJECT_DESTROY;
						}
					}
				}
				break;
			default:
				// do nothing
				break;
        }

        return NetworkSceneManager::processMessage(message);
    }
    //-------------------------------------------------------------------------
}
