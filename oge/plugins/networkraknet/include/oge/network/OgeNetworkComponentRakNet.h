/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_NETWORKCOMPONENTRAKNET_H__
#define __OGE_NETWORKCOMPONENTRAKNET_H__

#include "oge/network/OgeNetworkPrerequisitesRakNet.h"
#include "oge/network/OgeNetworkComponent.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"

namespace oge
{
    /**  
     * This component type permits receiving information about this object over
	 * a network.
	 *
     * @author Alex "petrocket" Peterson
     */
    class OGE_NETWORK_API NetworkComponentRakNet : public NetworkComponent
    {
        friend class NetworkComponentTemplateRakNet;

    public:
        virtual ~NetworkComponentRakNet()
        {
        }

        /// Destroy the component but DOESN'T delete
		void _destroy(SceneManager* sceneManager) {};
		void _activate(bool activate, SceneManager* sceneManager) {};

        UpdateRate _getUpdateRate() const;

		/**
		 * This function should be called after the manager for this
		 * component runs tick().  This allows the component to 
		 * update based on the tick timing settings for the manager that
		 * owns this component.
		 * @param currentTime The time that was used for tick()
		 */
		void _postTick(double currentTime);

        /** (Re)set the default values
		 * Message types should be ObjectMessage::SET_CUSTOM
		 * Param1 should be the parameter (String) e.g. "deltas"
		 * Param2 should be the value.
		 *
		 *
		 * Valid Types
		 * ============================
		 * TYPE           VALUE                 DESCRIPTION
		 * "deltas"       &std::vector<string>  List of parameters to update. Valid parameters are:
		 *                                      "position", "orientation", "visible", "animation1", "animation2"
		 * "interpolate"  bool                  TRUE if we should use lag compensation on this component.  
		 *                                      Movable objects like players should use lag compensation, but
		 *                                      static world objects like trees or rocks shouldn't need it.
		 *
		 */
        bool _reset(const MessageList& params, SceneManager* sceneManager);

        void _update(double deltaTime);

    protected:
        inline NetworkComponentRakNet(const ComponentType& type, const ComponentType& family)
            : NetworkComponent(type, family)
        {}

	private:
		/**
		 * Helper function to send non tick data messages
		 */
		void _sendNonTickDataMessages();

		/**
		 * Helper function to send tick data messages
		 */
		void _sendTickDataMessages(double currentTime);
    };

    /**
     * Builder that is used to create the Network Component based on a template.
     *
     * @author Alex Peterson
     */
    class OGE_NETWORK_API NetworkComponentTemplateRakNet : public ComponentTemplate
    {
    public:
        NetworkComponentTemplateRakNet() 
        {
            setFamily("Network");
            setType("NetworkComponent");
        }

        /// Create the component
        virtual Component* createComponent()
        {
            return new NetworkComponentRakNet( getType(), getFamily() );
        }
    };

}
#endif
