/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2008 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_NETWORKMANAGERRAKNET_H__
#define __OGE_NETWORKMANAGERRAKNET_H__

#include "oge/network/OgeNetworkPrerequisitesRakNet.h"
#include "oge/resource/OgeResourceControllerOgeImpl.h"
#include "oge/message/OgeMessageList.h"
#include "oge/network/OgeNetworkManager.h"
#include "oge/OgeSingleton.h"
#include "oge/math/OgeVector3.h"
#include "RakPeerInterface.h"
#include "RakPeer.h"
#include "MessageIdentifiers.h"

namespace oge
{	
	// our custom packet ids
	enum customRakNetPacketIds {
		ID_CLIENT_CMD          = ID_USER_PACKET_ENUM + 0,
		ID_CLIENT_CONNECTED    = ID_USER_PACKET_ENUM + 1, // sent on client connect
		ID_CLIENT_DISCONNECTED = ID_USER_PACKET_ENUM + 2, // sent on client disconnect
		ID_CLIENT_INFO		   = ID_USER_PACKET_ENUM + 3, // sent to update client info
		ID_OBJECT_CREATE	   = ID_USER_PACKET_ENUM + 4, // sent on object created
		ID_OBJECT_DESTROY	   = ID_USER_PACKET_ENUM + 5, // sent on object destroyed
		ID_NETWORK_COMPONENT_UPDATE = ID_USER_PACKET_ENUM + 6, // sent to update an object's frequently updated values like position and orientation
		ID_SERVER_INFO         = ID_USER_PACKET_ENUM + 7, // sent to update server info
		ID_OBJECT_HEALTH	   = ID_USER_PACKET_ENUM + 8, // sent to update an objects health
		ID_OBJECT_ARMOUR	   = ID_USER_PACKET_ENUM + 9, // sent to update an objects armour
		ID_OBJECT_DAMAGE  	   = ID_USER_PACKET_ENUM + 10, // sent to notify an object on damage
		ID_OBJECT_AMMO   	   = ID_USER_PACKET_ENUM + 11, // sent to update an objects ammo
		ID_OBJECT_AMMO2   	   = ID_USER_PACKET_ENUM + 12, // sent to update an objects secondary ammo
		ID_OGE_CUSTOM          = ID_USER_PACKET_ENUM + 13 // used for custom messages
	};

    class OGE_NETWORK_API NetworkManagerRakNet : public NetworkManager, 
        public Singleton<NetworkManagerRakNet>
    {
    public:
        static NetworkManagerRakNet* createSingleton();
        static void destroySingleton();

		enum NetworkParamTypes
		{
			NONE = 0, // no param
			BOOL,
			CHAR,
			UCHAR,
			SHORT,
			USHORT,
			INT,
			UINT,
			LONG,
			ULONG,
			LONGLONG,
			ULONGLONG,
			FLOAT,
			DOUBLE,
			STRING,
			VECTOR2,
			VECTOR3,
			VECTOR4
		};

        /**
         * Get the RakPeer instance
         * @return The RakPeer instance
         */
		RakNet::RakPeerInterface* getRakPeer() { return mRakPeer; }

        static NetworkManagerRakNet* getSingletonPtr() { return mSingleton; }
        static NetworkManagerRakNet& getSingleton()
        {
            assert(mSingleton);
            return *mSingleton;
        }

        /** 
         * Connect to a server or other computer over the network.
         * @note Because we are connecting over a network, we won't know if
         *       the connection has failed until the timeout expires.
         * @param host The ip address or dns name of the host to connect to
         * @param remotePort The port on the remote host to connect to
         * @param numAttempts The number of connection attempts to make
         * @param attemptWaitTime The amount of time to wait between connection
         *                        attempts in milliseconds.
         * @return true on successful connection initiation, false on failure or if
         *         already connected to the specified host
         */
        bool connect(const String& host, unsigned short remotePort, 
            unsigned int numAttempts=12, unsigned int attemptWaitTime=500);

		/**
		 * Create the default scene manager
		 */
		virtual void createDefaultSceneManager();

        /**
         * Disconnect from a server or other computer over the network
         * @param host The ip address or dns name of the host to disconnect to.  
         *             If empty disconnect from the first active connection.
         * @return true on successful disconnect initiation, false on failure or if
         *         not connected.
         */
        bool disconnect(const String& host);

		/**
		 * Get the current (last updated) time
		 * @return the last time this network manager ticked
		 */
		double getCurrentTime() { return mCurrentTime; }

		/**
		 * Get the libraries version
		 * @return the RakNet library version
		 */
		const String getLibrariesVersion();

		/** 
		 * Get the current tick fraction based on the current time
		 * This will be a decimal value between 0 and 1
		 * @param time Optional time to use to calculate the tick fraction
		 * @return the current tick fraction
		 */
		double getTickFraction(double time = 0.0);

        /**
         * Start a server and listen for connections on the given port.
         * @param port The port to listen on
         * @param maxConnections The maximum number or connections allowed
		 * @param serverType The server type - SERVER, LISTEN_SERVER, PEER_TO_PEER
         * @return true on successful initialization
         */
        bool listen(unsigned short port, unsigned short maxConnections = 16, int serverType = SERVER);

		/**
		 * Send a ping to a remote host/port.  The network listener will be
		 * notified when a pong is received.
		 * @param host The host to ping
		 * @param remotePort The remote port
		 */
		void ping(const String& host, unsigned short remotePort);

        /**
         * Send data over the network.
		 * @param recipient The networkclientid of the recipient
		 * @param messageType The type of message
		 * @param data The data to send
		 * @param length The length of the data we're sending
         */
		void send(NetworkClientId recipient, unsigned int messageType, const char* data, const int length);

        /**
         * Send data over the network to all clients
		 * @param messageType The type of message
		 * @param data The data to send
		 * @param length The length of the data we're sending
         */
		void sendToAll(unsigned int messageType, const char* data, const int length);

		/**
		 * Set the server name
		 * @param name The new server name
		 */
		void setServerName(const String& name);

    protected:
        // System methods
        bool initialise();
        void shutdown();
        void tick(double currentTime);

		double mCurrentTime;

    private:
        NetworkManagerRakNet();
        ~NetworkManagerRakNet();

		/**
		 * Utility function to get a client network id from a packet
		 * @param p The packet
		 * @return the network id
		 */
		NetworkClientId _getNetworkIdFromPacket(RakNet::Packet *p);

		/**
		* Utility function to get a packet identifier
		* @param p The packet
		* @return the packet identifier
		*/
		unsigned char _getPacketIdentifier(RakNet::Packet *p);

		/**
		 * Get a parameter of a particular type
		 * @param type The type of the parameter
		 * @param RakNet::BitStream*
		 * @return the Any param
		 */
		Poco::Any _getParam(unsigned int paramType, RakNet::BitStream* bs);

		/**
		 * Helper function for when a client sends a command
		 * @param packet
		 */
		void _onClientCommand(RakNet::Packet* packet);

		/**
		 * Helper function for when a client connects
		 * @param packet
		 */
		void _onClientConnect(RakNet::Packet* packet);

		/**
		 * Helper function for when a client disconnects
		 * @param packet
		 */
		void _onClientDisconnect(RakNet::Packet* packet);

		/**
		 * Helper function for when we get client info
		 * @param packet
		 */
		void _onClientInfo(RakNet::Packet* packet);

		/**
		 * Create an object based on a packet.  This will create the object
		 * template if one doesn't already exist
		 * @param packet The packet
		 * @return true on success, false on failure
		 */
		bool _onCreateObject(RakNet::Packet* packet);

		/**
		 * On receipt of a custom OGE packet
		 * @param The packet
		 */
		void _onCustom(RakNet::Packet* packet);

		/**
		 * Destroy an object based on a packet.  
		 * @param packet The packet
		 * @return true on success, false on failure
		 */
		bool _onDestroyObject(RakNet::Packet* packet);

		/**
		 * Store server information and synchronize ticks
		 * @param packet
		 */
		void _onServerInfo(RakNet::Packet* packet);

		/**
		 * Update a network component for an object based on a packet
		 * @param The packet
		 * @return true on success, false on failure
		 */
		bool _onUpdateNetworkComponent(RakNet::Packet* packet);

		/**
		 * We received an unconnected pong - pass along to client
		 * @param The packet
		 */
		void _onUnconnectedPong(RakNet::Packet* packet);

		/**
		 * Helper function to process client packets
		 */
		void _processClientPackets();

		/**
		 * Helper function to process server packets
		 */
		void _processServerPackets();

        /**
         * Send data over the network.
		 * @param recipient The networkclientid of the recipient
		 * @param messageType The type of message
		 * @param data The data to send
		 * @param length The length of the data we're sending
         */
		void send(NetworkClientId recipient, unsigned int messageType, const unsigned char* data, const int length);

        /**
         * Send data over the network to all clients
		 * @param messageType The type of message
		 * @param data The data to send
		 * @param length The length of the data we're sending
         */
		void sendToAll(unsigned int messageType, const unsigned char* data, const int length);

		/**
		 * Helper function to send the latest client command to the server
		 */
		void _sendClientCommand(const ObjectId& id);

		/**
		 * Helper function to send a ID_CLIENT_CONNECTED message to a client
		 * @param clientId The client to send the message to
		 * @param otherClientId The client who connected
		 */
		void _sendClientConnectedMessage(NetworkClientId clientId, NetworkClientId otherClientId);

		/**
		 * Helper function to send a ID_CLIENT_DISCONNECTED message to a client
		 * @param clientId The client to send the message to
		 * @param otherClientId The client who connected
		 */
		void _sendClientDisconnectedMessage(NetworkClientId clientId, NetworkClientId otherClientId);

		/**
		 * Helper function to send a ID_CLIENT_INFO message to a client
		 * @param clientId The client to send the message to
		 * @param otherClientId The client with the information to send
		 */
		void _sendClientInfoMessage(NetworkClientId clientId, NetworkClientId otherClientId);

		/**
		 * Helper function to send a ID_CREATE_OBJECT message to a client
		 * @param clientId The client to send the message to
		 * @param id The ObjectId of the object to create
		 */
		void _sendCreateObjectMessage(NetworkClientId clientId, const ObjectId& id);

		/**
		 * Helper function to send a ID_DESTROY_OBJECT message to a client
		 * @param clientId The client to send the message to
		 * @param id The ObjectId of the object to destroy
		 */
		void _sendDestroyObjectMessage(NetworkClientId clientId, const ObjectId& id);

		/**
		 * Helper function to send server ID_SERVER_INFO message to a client.
		 * @param clientId The client to send the message to
		 */
		void _sendServerInfoMessage(NetworkClientId clientId);

		/**
		 * Helper function to send server updates to clients
		 */
		void _sendServerUpdates();

		/**
		 * Helper function to send a ID_NETWORK_COMPONENT_UPDATE message to a client
		 * @param clientId The client to send the message to
		 * @param id The ObjectId of the object to update
		 */
		void _sendUpdateObjectMessage(NetworkClientId clientId, const ObjectId& id);

		/**
		 * Helper function to update the offline ping response
		 */
		void _updateOfflinePingResponse();

		/** 
		 * Helper function to write an object template to a bitstream
		 * @param objTemplate the object template
		 * @param params the params list
		 * @param bs the RakNet::BitStream
		 */
		void _writeObjectTemplate(ObjectTemplate* objTemplate, const MessageList& params, RakNet::BitStream& bs);

		/**
		 * Helper function to write a param to a bitstream
		 * @param param The parameter
		 * @param bs the RakNet::BitStream
		 */
		void _writeParam(const Any& param, RakNet::BitStream& bs);

        /// RakPeer interface instance
		RakNet::RakPeerInterface *mRakPeer;
    };
}
#endif // __OGE_NETWORKMANAGERRAKNET_H__
