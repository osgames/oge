/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_INPUTMANAGEROIS_H__
#define __OGE_INPUTMANAGEROIS_H__

#include "oge/input/OgeInputPrerequisites.h"
#include "oge/input/OgeOISIncludes.h"
#include "oge/input/OgeInputManager.h"
#include "oge/input/OgeDefaultInputEventHandler.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/input/OgeInputDevice.h"

namespace oge
{
    /**
     * OIS based InputManager
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_INPUT_API InputManagerOIS : 
        public InputManager, public Singleton<InputManagerOIS>
    {
    private:
        OIS::InputManager* mOIS;
        DefaultInputEventHandler mDefaultIEH;

    public:
        virtual ~InputManagerOIS();

        /// Used to create the manager
        static InputManagerOIS* createSingleton();
        static InputManagerOIS* getSingletonPtr();
        static InputManagerOIS& getSingleton();
        /// Used to destroy the manager
        static void destroySingleton();

        // ----------- System methods -----------
        bool initialise();
        void shutdown();
        void tick(double currentTime);

        // ----------- OIS specific -----------
        /**
         * @note Since OIS::System isn't a singleton anymore the InputDevice
         *       has to know to which OIS:System it has to be bind
         */
        OIS::InputManager* getOIS();
        /// TODO refactor
        const String getLibrariesVersion();

        /**
         * Creates an input device of the specified type (keyboard, mouse, etc).
         *
         * @param Name must be unique.
         * @param DeviceType enumeration and adding it to the device list. 
         */
        InputDevice* createInputDevice(const String& name, DeviceType devType);

        /** Called by RenderTargetManager::windowResized()
         */
        void windowResized(unsigned int width, unsigned int height);

    private:
        InputManagerOIS();
        /// Create the OIS input manager
        bool createOIS();
    };
}
#endif // __OGE_INPUTMANAGEROIS_H__
