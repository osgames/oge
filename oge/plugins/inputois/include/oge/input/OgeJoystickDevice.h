/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_JOYSTICKDEVICE_H__
#define __OGE_JOYSTICKDEVICE_H__

#include "oge/input/OgeInputPrerequisites.h"
#include "oge/input/OgeOISIncludes.h"
#include "oge/input/OgeInputDevice.h"
#include "oge/OgeNonCopyable.h"
#include "oge/logging/OgeLogManager.h" // TODO remove when implementing getState

namespace oge
{
    typedef OIS::Pov JoystickPov;

    /**
     * Joystick device - this version is will return the events directly
     * to the user code via an Any. This class must be used with the
     * appropriate InputEventHandler Any-based methods.
     * 
     * @author Steven 'lazalong' Gay
     * @note Original code from Alkis LEKAKIS
     */
    class OGE_INPUT_API JoystickDeviceAny : 
        public OIS::JoyStickListener, public InputDevice, public NonCopyable
    {
        friend class InputManagerOIS;

    private:
        OIS::JoyStick *mOISJoystick;
        OIS::InputManager* mOIS;

    public:
        /**
         * Updates mouse device data such as button presses, releases and 
         * axis movements.
         */
        void update();
        /**
         * Checks for buffered joystick axis movements.
         */
        bool axisMoved(const OIS::JoyStickEvent &arg, int axis);
        /**
         * Checks for buffered joystick button presses.
         */
        bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
        /**
         * Checks for buffered joystick button releases.
         */
        bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
        /**
         * Checks for buffered joystick pov movements.
         */
        bool povMoved(const OIS::JoyStickEvent &arg, int povID);
        /**
         * Returns the joystick state information.
         */
        const OIS::JoyStickState& getOISState() const;
        /**
         * Get the state information of the device
         */
        virtual const InputState* getState() { return 0; LOGW("Not implemented"); } // TODO

    private:
        JoystickDeviceAny(OIS::InputManager* ois, const String& name);
        virtual ~JoystickDeviceAny();
    };

    /**
     * @author Steven 'lazalong' Gay
     * @note Original code from Alkis LEKAKIS
     */
    class OGE_INPUT_API JoystickDevice : 
        public OIS::JoyStickListener, public InputDevice, public NonCopyable
    {
        friend class InputManagerOIS;

    private:
        OIS::JoyStick *mOISJoystick;
        OIS::InputManager* mOIS;

    public:
        /**
         * Updates mouse device data such as button presses, releases and 
         * axis movements.
         */
        void update();
        /**
         * Checks for buffered joystick axis movements.
         */
        bool axisMoved(const OIS::JoyStickEvent& arg, int axis);
        /**
         * Checks for buffered joystick button presses.
         */
        bool buttonPressed(const OIS::JoyStickEvent& arg, int button);
        /**
         * Checks for buffered joystick button releases.
         */
        bool buttonReleased(const OIS::JoyStickEvent& arg, int button);
        /**
         * Checks for buffered joystick pov movements.
         */
        bool povMoved(const OIS::JoyStickEvent& arg, int povID);
        /**
         * Returns the joystick state information.
         */
        const OIS::JoyStickState& getOISState() const;
        /**
         * Get the state information of the device
         */
        virtual const InputState* getState() { return 0; LOGW("Not implemented"); } // TODO

    private:
        JoystickDevice(OIS::InputManager* ois, const String& name);
        virtual ~JoystickDevice();
    };

}
#endif
