/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_INPUTEVENTSOIS_H__
#define __OGE_INPUTEVENTSOIS_H__

#include "oge/input/OgeInputPrerequisites.h"
#include "oge/input/OgeInputEvent.h"

namespace oge
{
    /**
     * OIS specific keyboard events
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_INPUT_API KeyEventOIS : public KeyEvent
    {
    public:
        /**
         * @todo KeyEvent are simple: do we really need to store the event?
         */
        const OIS::KeyEvent& mEvent;

    public:
        KeyEventOIS( const OIS::KeyEvent& arg ) :
            KeyEvent( (KeyCode) (const int) arg.key ),
            mEvent( arg )
        {
        }
        virtual ~KeyEventOIS() {}

        unsigned int getAsText() const { return mEvent.text; }
    };

    /**
     * OIS specific mouse events
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_INPUT_API MouseEventOIS : public MouseEvent
    {
    public:
        const OIS::MouseEvent& mEvent;
        int mButton;

    public:
        MouseEventOIS( const OIS::MouseEvent& arg ) :
            mEvent(arg), mButton(-1)
        {
        }
        virtual ~MouseEventOIS() {}

        // Comparison to avoid VC "conversion performance warning"
        inline int getAbsX()   const { return mEvent.state.X.abs; }
        inline int getAbsY()   const { return mEvent.state.Y.abs; }
        inline int getAbsZ()   const { return mEvent.state.Z.abs; }
        inline int getRelX()   const { return mEvent.state.X.rel; }
        inline int getRelY()   const { return mEvent.state.Y.rel; }
        inline int getRelZ()   const { return mEvent.state.Z.rel; }
        inline int getWidth()  const { return mEvent.state.width; }
        inline int getHeight() const { return mEvent.state.height; }
        inline int getButton() const { return mButton; }

        inline bool isButtonDown( MouseButtonID button ) const
        {
            return ((mEvent.state.buttons & 
                (1L << (OIS::MouseButtonID) button )) == 0)
                ? false : true;
        }
    };

    /**
     * OIS specific joystick events
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_INPUT_API JoystickEventOIS : public JoystickEvent
    {
    public:
        const OIS::JoyStickEvent& mEvent;

    public:
        JoystickEventOIS( const OIS::JoyStickEvent& arg ) :
            mEvent(arg)
        {
        }
        virtual ~JoystickEventOIS() {}
    };
}
#endif
