/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_MOUSEDEVICE_H__
#define __OGE_MOUSEDEVICE_H__

#include "oge/input/OgeInputPrerequisites.h"
#include "oge/input/OgeOISIncludes.h"
#include "oge/input/OgeInputDevice.h"
#include "oge/OgeNonCopyable.h"
#include "oge/logging/OgeLogManager.h" // TODO remove when implementing getState
#include "oge/input/OgeInputState.h"

namespace oge
{
    typedef OIS::MouseButtonID MouseButton;

    /**
     * Mouse device - this version is will return the events directly
     * to the user code via an Any. This class must be used with the
     * appropriate InputEventHandler Any-based methods.
     * 
     * @author Steven 'lazalong' Gay
     * @note Original code from Alkis LEKAKIS
     */
    class OGE_INPUT_API MouseDeviceAny : 
        public OIS::MouseListener, public InputDevice, public NonCopyable
    {
        friend class InputManagerOIS;

    private:
        OIS::Mouse *mOISMouse;
        OIS::InputManager* mOIS;

    public:
        /**
         * Updates mouse device data such as button presses and releases.
         */
        void update();
        /**
         * Checks for buffered mouse movement.
         */
        bool mouseMoved(const OIS::MouseEvent& arg);
        /**
         * Checks for buffered mouse button presses.
         */
        bool mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id);
        /**
         * Checks for buffered mouse button releases.
         */
        bool mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id);
        /**
         * Returns a string description of the mouse button id.
         */
        const String getAsString(MouseButton button);
        /**
         * Returns the mouse state information.
         */
        const OIS::MouseState& getOISState() const;
        /** 
         * Set the windows sizes !
         */
        void setRenderSizes( unsigned int width, unsigned int height );
        /**
         * Get the state information of the device
         */
        virtual const InputState* getState() { return 0; LOGW("Not implemented"); } // TODO

    private:
        MouseDeviceAny(OIS::InputManager* ois, const String& name);
        virtual ~MouseDeviceAny();
    };

    /**
     * @author Steven 'lazalong' Gay
     * @note Original code from Alkis LEKAKIS
     */
    class OGE_INPUT_API MouseDevice : 
        public OIS::MouseListener, public InputDevice, public NonCopyable
    {
        friend class InputManagerOIS;

    private:
        OIS::Mouse *mOISMouse;
        OIS::InputManager* mOIS;
        /// Used to return the state of the device as an event
        MouseState mState;

    public:
        /**
         * Updates mouse device data such as button presses and releases.
         */
        void update();
        /**
         * Checks for buffered mouse movement.
         */
        bool mouseMoved(const OIS::MouseEvent& arg);
        /**
         * Checks for buffered mouse button presses.
         */
        bool mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id);
        /**
         * Checks for buffered mouse button releases.
         */
        bool mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id);
        /**
         * Returns a string description of the mouse button id.
         */
        const String getAsString(MouseButton button);
        /**
         * Returns the mouse state information.
         */
        const OIS::MouseState& getOISState() const;
        /** 
         * Set the windows sizes !
         */
        void setRenderSizes( unsigned int width, unsigned int height );
        /**
         * Get the state information of the device
         */
        virtual const InputState* getState();

    private:
        MouseDevice(OIS::InputManager* ois, const String& name);
        virtual ~MouseDevice();
    };
}
#endif
