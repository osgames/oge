/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/input/OgeJoystickDevice.h"
#include "oge/input/OgeInputManager.h"
#include "oge/input/OgeInputEventHandler.h"

// -----------  Any-based ---------------------------------------------------
namespace oge
{
    //---------------------------------------------------------------------------
    JoystickDeviceAny::JoystickDeviceAny(OIS::InputManager* ois, const String& name)
        : InputDevice(OGE_INPUT_JOYSTICK_ANY, name), mOIS(ois)
    {
        mOISJoystick = static_cast<OIS::JoyStick*> 
            (mOIS->createInputObject(OIS::OISJoyStick, true));
        mOISJoystick->setEventCallback(this);
        setDeviceUpdate(true);
    }
    //---------------------------------------------------------------------------
    JoystickDeviceAny::~JoystickDeviceAny()
    {
        if (mOISJoystick && mOIS && InputManager::getManager())
        {
            mOIS->destroyInputObject(mOISJoystick);
            mOISJoystick = 0;
        }
    }
    //---------------------------------------------------------------------------
    void JoystickDeviceAny::update()
    {
        assert(mCanUpdate);
        mOISJoystick->capture();
    }
    //---------------------------------------------------------------------------
    bool JoystickDeviceAny::buttonPressed(const OIS::JoyStickEvent &arg, int button)
    {
        mEventHandler->joyButtonPressedAny(&arg, button);
        return true;
    }
    //---------------------------------------------------------------------------
    bool JoystickDeviceAny::buttonReleased(const OIS::JoyStickEvent &arg, int button)
    {
        mEventHandler->joyButtonReleasedAny(&arg, button);
        return true;
    }
    //---------------------------------------------------------------------------
    bool JoystickDeviceAny::axisMoved(const OIS::JoyStickEvent &arg, int axis)
    {
        mEventHandler->joyAxisMovedAny(&arg, axis);
        return true;
    }
    //---------------------------------------------------------------------------
    bool JoystickDeviceAny::povMoved(const OIS::JoyStickEvent &arg, int povID)
    {
        mEventHandler->joyPovMovedAny(&arg, povID);
        return true;
    }
    //---------------------------------------------------------------------------
    const OIS::JoyStickState& JoystickDeviceAny::getOISState() const
    {
        return mOISJoystick->getJoyStickState();
    }
    //---------------------------------------------------------------------------
}

// -----------  Event-based -------------------------------------------------
namespace oge
{
    //---------------------------------------------------------------------------
    JoystickDevice::JoystickDevice(OIS::InputManager* ois, const String& name)
        : InputDevice(OGE_INPUT_JOYSTICK, name), mOIS(ois)
    {
        mOISJoystick = static_cast<OIS::JoyStick*> 
            (mOIS->createInputObject(OIS::OISJoyStick, true));
        mOISJoystick->setEventCallback(this);
        setDeviceUpdate(true);
    }
    //---------------------------------------------------------------------------
    JoystickDevice::~JoystickDevice()
    {
        if (mOISJoystick && mOIS && InputManager::getManager())
        {
            mOIS->destroyInputObject(mOISJoystick);
            mOISJoystick = 0;
        }
    }
    //---------------------------------------------------------------------------
    void JoystickDevice::update()
    {
        assert(mCanUpdate);
        mOISJoystick->capture();
    }
    //---------------------------------------------------------------------------
    bool JoystickDevice::buttonPressed(const OIS::JoyStickEvent& arg, int button)
    {
        mEventHandler->joyButtonPressedAny(&arg, button);
        return true;
    }
    //---------------------------------------------------------------------------
    bool JoystickDevice::buttonReleased(const OIS::JoyStickEvent& arg, int button)
    {
        mEventHandler->joyButtonReleasedAny(&arg, button);
        return true;
    }
    //---------------------------------------------------------------------------
    bool JoystickDevice::axisMoved(const OIS::JoyStickEvent& arg, int axis)
    {
        mEventHandler->joyAxisMovedAny(&arg, axis);
        return true;
    }
    //---------------------------------------------------------------------------
    bool JoystickDevice::povMoved(const OIS::JoyStickEvent& arg, int povID)
    {
        mEventHandler->joyPovMovedAny(&arg, povID);
        return true;
    }
    //---------------------------------------------------------------------------
    const OIS::JoyStickState& JoystickDevice::getOISState() const
    {
        return mOISJoystick->getJoyStickState();
    }
    //---------------------------------------------------------------------------
} // namespace
