/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/input/OgeKeyboardDevice.h"
#include "oge/input/OgeInputManager.h"
#include "oge/input/OgeInputManagerOIS.h"
#include "oge/input/OgeInputEventHandler.h"
#include "oge/input/OgeInputEventOIS.h"

// -----------  Any-based -------------------------------------------------
namespace oge
{
    //-------------------------------------------------------------------------
    KeyboardDeviceAny::KeyboardDeviceAny(OIS::InputManager* ois, const String& name) :
        InputDevice(OGE_INPUT_KEYBOARD_ANY, name), mOISKeyboard(0), mOIS(ois)
    {
        mOISKeyboard = static_cast<OIS::Keyboard*> 
            (mOIS->createInputObject(OIS::OISKeyboard, true));
        mOISKeyboard->setEventCallback(this);
        setDeviceUpdate(true);
    }
    //-------------------------------------------------------------------------
    KeyboardDeviceAny::~KeyboardDeviceAny()
    {
        if (mOISKeyboard && mOIS && InputManager::getManager())
        {
            mOIS->destroyInputObject(mOISKeyboard);
            mOISKeyboard = 0;
        }
    }
    //-------------------------------------------------------------------------
    void KeyboardDeviceAny::update()
    {
        assert(mCanUpdate);
        mOISKeyboard->capture();
    }
    //-------------------------------------------------------------------------
    bool KeyboardDeviceAny::keyPressed(const OIS::KeyEvent& arg)
    {
        mEventHandler->keyPressedAny(&arg);
        return true;
    }
    //-------------------------------------------------------------------------
    bool KeyboardDeviceAny::keyReleased(const OIS::KeyEvent& arg)
    {
        mEventHandler->keyReleasedAny(arg);
        return true;
    }
    //-------------------------------------------------------------------------
    bool KeyboardDeviceAny::isKeyDown(OIS::KeyCode kc)
    {
        return mOISKeyboard->isKeyDown(kc);
    }
    //-------------------------------------------------------------------------
    bool KeyboardDeviceAny::isKeyDown(KeyModifier modifier)
    {
        return mOISKeyboard->isModifierDown((OIS::Keyboard::Modifier)modifier);
    }
    //-------------------------------------------------------------------------
    const String& KeyboardDeviceAny::getAsString(OIS::KeyCode key)
    {
        return mOISKeyboard->getAsString(key);
    }
    //-------------------------------------------------------------------------
    const String KeyboardDeviceAny::getAsString(KeyModifier modifier)
    {
        if ((OIS::Keyboard::Modifier)modifier == OIS::Keyboard::Shift)
            return "Modifier Shift";
        else if ((OIS::Keyboard::Modifier)modifier == OIS::Keyboard::Alt)
            return "Modifier Alt";
        else if ((OIS::Keyboard::Modifier)modifier == OIS::Keyboard::Ctrl)
            return "Modifier Ctrl";
        return "";
    }
    //-------------------------------------------------------------------------
} // namespace

// -----------  Event-based -------------------------------------------------
namespace oge
{
    //-------------------------------------------------------------------------
    KeyboardDevice::KeyboardDevice(OIS::InputManager* ois, const String& name) :
        InputDevice(OGE_INPUT_KEYBOARD, name), mOISKeyboard(0), mOIS(ois)
    {
        mOISKeyboard = static_cast<OIS::Keyboard*> 
            (mOIS->createInputObject(OIS::OISKeyboard, true));
        mOISKeyboard->setEventCallback(this);
        setDeviceUpdate(true);
    }
    //-------------------------------------------------------------------------
    KeyboardDevice::~KeyboardDevice()
    {
        if (mOISKeyboard && mOIS && InputManager::getManager())
        {
            mOIS->destroyInputObject(mOISKeyboard);
            mOISKeyboard = 0;
        }
    }
    //-------------------------------------------------------------------------
    void KeyboardDevice::update()
    {
        assert(mCanUpdate);
        mOISKeyboard->capture();
    }
    //-------------------------------------------------------------------------
    bool KeyboardDevice::keyPressed(const OIS::KeyEvent& arg)
    {
        KeyEventOIS evt(arg);
        mEventHandler->keyPressed(evt);
        return true;
    }
    //-------------------------------------------------------------------------
    bool KeyboardDevice::keyReleased(const OIS::KeyEvent& arg)
    {
        KeyEventOIS evt(arg);
        mEventHandler->keyReleased(evt);
        return true;
    }
    //-------------------------------------------------------------------------
    bool KeyboardDevice::isKeyDown(OIS::KeyCode kc)
    {
        return mOISKeyboard->isKeyDown(kc);
    }
    //-------------------------------------------------------------------------
    bool KeyboardDevice::isKeyDown(KeyModifier modifier)
    {
        return mOISKeyboard->isModifierDown((OIS::Keyboard::Modifier)modifier);
    }
    //-------------------------------------------------------------------------
    const String& KeyboardDevice::getAsString(OIS::KeyCode key)
    {
        return mOISKeyboard->getAsString(key);
    }
    //-------------------------------------------------------------------------
    const String KeyboardDevice::getAsString(KeyModifier modifier)
    {
        if ((OIS::Keyboard::Modifier)modifier == OIS::Keyboard::Shift)
            return "Modifier Shift";
        else if ((OIS::Keyboard::Modifier)modifier == OIS::Keyboard::Alt)
            return "Modifier Alt";
        else if ((OIS::Keyboard::Modifier)modifier == OIS::Keyboard::Ctrl)
            return "Modifier Ctrl";
        return "";
    }
    //-------------------------------------------------------------------------
} // namespace
