/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/input/OgeInputManagerOIS.h"
#include "oge/input/OgeInputDevice.h"
#include "oge/input/OgeKeyboardDevice.h"
#include "oge/input/OgeJoystickDevice.h"
#include "oge/input/OgeMouseDevice.h"
#include "oge/engine/OgeEngineManager.h"
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/config/OgeOptionManager.h"
#include "oge/math/OgeVector3.h"
#include "oge/OgeProfiler.h"

namespace oge
{
    //------------------------------------------------------------------------
    template<> InputManagerOIS* Singleton<InputManagerOIS>::mSingleton = 0;
    InputManagerOIS* InputManagerOIS::getSingletonPtr() 
    { 
        return mSingleton; 
    }
    InputManagerOIS& InputManagerOIS::getSingleton()
    {
        assert(mSingleton);
        return *mSingleton;
    }    
    //------------------------------------------------------------------------
    InputManagerOIS* InputManagerOIS::createSingleton()
    {
        if (mSingleton == 0)
            mSingleton = new InputManagerOIS();
        return mSingleton;
    }
    //------------------------------------------------------------------------
    void InputManagerOIS::destroySingleton()
    {
        if (mSingleton != 0)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //-------------------------------------------------------------------------
    InputManagerOIS::InputManagerOIS() : 
        InputManager("InputManager"), // Not InputManagerOIS as we -for now- want only one manager of each type
        mOIS(0)
    {
        LOG("InputManagerOIS created");
    }
    //-------------------------------------------------------------------------
    InputManagerOIS::~InputManagerOIS()
    {
        LOG("InputManagerOIS destroying...");
        shutdown();
        LOG("InputManagerOIS destroyed");
    }
    //-------------------------------------------------------------------------
    bool InputManagerOIS::initialise()
    {
        LOG("InputManagerOIS initialising...");

        // The window handle MUST be set because the ois::inputmanager
        // needs it in its constructor!
        if (!mWindowHandle)
        {
			LOGE("The OIS::InputManager has no window handle in initialise().  Aborting.");
            //EngineManager::getSingletonPtr()->controlSystem("InputManager", System::INITIALISE);
            return false;
        }

        if (!createOIS())
        {
            LOGE("The OIS::InputManager couldn't be created! We should abort!");
            // TODO abort or try again?
            return false;
        }

        // Create default keyboard, mouse & InputEventHandler
        // Careful: this MUST of course be after createOIS()
        // NEXT Should we create them always or must they be set though config.
        //      but don't forget that DefaultInputEventHandler needs them actually
        InputDevice* id1 = createInputDevice("Default Keyboard", OGE_INPUT_KEYBOARD);
        InputDevice* id2 = createInputDevice("Default Mouse", OGE_INPUT_MOUSE);

        LOGWC( id1 == 0, "No default keyboard device created: if none other was created oge can segfault!");
        LOGWC( id2 == 0, "No default mouse device created: if none other was created oge can segfault!");

        // NEXT Should we create them always or must they be set though config.
        // BTW we could create an ActionMap instead of an InputEventHandler!
        if (id1)
            id1->setInputEventHandler(&mDefaultIEH);
        if (id2)
            id2->setInputEventHandler(&mDefaultIEH);

        // TODO: Remove these and find a better approach that doesn't hard code this!
        requestSetTickInterval(10);
        requestSchedule();
        //EngineManager::getSingletonPtr()->controlSystem("InputManager", System::SCHEDULE);

        if (!System::initialise())
            return false;

        return true;
    }
    //-------------------------------------------------------------------------
    void InputManagerOIS::shutdown()
    {
        System::shutdown();

        LOG("InputManagerOIS shutting down...");
        if (mOIS)
        {
            // Destroy all devices
            mInputDevices.deleteAll();

            // Destroy the input system
            OIS::InputManager::destroyInputSystem(mOIS);
            mOIS = 0;
        }
        LOG("InputManagerOIS shutdown done.");
    }
    //-------------------------------------------------------------------------
    void InputManagerOIS::tick(double currentTime)
    {
		OgeProfileGroup("Input OIS", OGEPROF_SYSTEMS);
        DeviceMap::Iterator iter;
        for (iter = mInputDevices.begin() ; iter != mInputDevices.end(); ++iter)
        {
            iter->second->update();
        }
    }
    //-------------------------------------------------------------------------
    bool InputManagerOIS::createOIS()
    {
        LOG("Create OIS::InputManager...");
        
        OIS::ParamList pl;
        std::ostringstream wnd;

        if ( mWindowHandle == 0 )
        {
            LOGE( "The window handle was not set: Stopping or we would segfault!" );
            return false;
        }

        OptionPtr options = OptionManager::getSingleton().getOption("oge.inputmanager");

        // TODO: what are we supposed to be doing with this? [RL]
        bool exclusive = false;

        if(options)
        {
            if( options->getOption("mouse") &&
                options->getOption("mouse")->getOption("exclusive")) 
            {
                exclusive = StringUtil::toBool(options->getOption("mouse")->getOption("exclusive")->getValue());
            }
        }

        // Careful: the wndhdl is set by the graphics manager
        wnd << (size_t)mWindowHandle;
        pl.insert(std::make_pair( String("WINDOW"), wnd.str() ));

        #ifdef OIS_WIN32_PLATFORM
        if (!mIsFullscreen)
        {
            if(!exclusive) {
                pl.insert(std::make_pair( String("w32_mouse"), String("DISCL_NONEXCLUSIVE")));
                pl.insert(std::make_pair( String("w32_mouse"), String("DISCL_FOREGROUND" )));
            }
        }
        else
        {
            if(!exclusive) {
                // TODO To use exclusive we need first to have a gui & gui mouse
                // The main reason is that the ois hasn't the correct screen resolution
                // pl.insert(std::make_pair( String("w32_mouse"), String("DISCL_EXCLUSIVE")));
                pl.insert(std::make_pair( String("w32_mouse"), String("DISCL_NONEXCLUSIVE")));
                pl.insert(std::make_pair( String("w32_mouse"), String("DISCL_FOREGROUND" )));
            }
        }
        #else
            // TODO LINUX and MAC support !
            pl.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
            pl.insert(std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
            pl.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
            pl.insert(std::make_pair(std::string("XAutoRepeatOn"), std::string("true")));
        #endif

        mOIS = OIS::InputManager::createInputSystem(pl);

        LOG("OIS::InputManager created.");
        return true;
    }
    //-------------------------------------------------------------------------
    OIS::InputManager* InputManagerOIS::getOIS()
    {
        return mOIS;
    }
    //-------------------------------------------------------------------------
    const String InputManagerOIS::getLibrariesVersion()
    {
        String version("Input: OIS ");
        version += mOIS->getVersionName();
        version += " - ";
        // Bits: 1-8 Patch number, 9-16 Minor version, 17-32 Major version
        // ((OIS_VERSION_MAJOR << 16) | (OIS_VERSION_MINOR << 8) | OIS_VERSION_PATCH)
        unsigned int number = mOIS->getVersionNumber();
        unsigned int major = number/65536;
        unsigned int minor = (number-major*65536) / 256;
        unsigned int patch = number-major*65536-minor*256;
        
        version += StringUtil::toString(major);
        version += ".";
        version += StringUtil::toString(minor);
        version += ".";
        version += StringUtil::toString(patch);
        version += "\n";
        return version;
    }
    //-------------------------------------------------------------------------
    InputDevice* InputManagerOIS::createInputDevice(const String& name, 
        oge::DeviceType devType)
    {
        InputDevice *inpDevice = 0;

        bool nameExists = false;
        //check if name exists. If it does, return the device found.
        for (DeviceMap::Iterator i = mInputDevices.begin(); i != mInputDevices.end(); ++i)
        {
            if (i->first == name)
            {
                inpDevice = i->second;
                nameExists = true;
                break;
            }
        }
        if (nameExists)
            return inpDevice;

        switch (devType)
        {
        case OGE_INPUT_KEYBOARD:
            if (mOIS->getNumberOfDevices(OIS::OISKeyboard) <= 0)
                return 0;
            inpDevice = new KeyboardDevice(mOIS, name);
            break;
        case OGE_INPUT_MOUSE:
            if (mOIS->getNumberOfDevices(OIS::OISMouse) <= 0)
                return 0;
            inpDevice = new MouseDevice(mOIS, name);
            ((MouseDevice*)inpDevice)->setRenderSizes(
                (unsigned int) GraphicsManager::getManager()->getRenderWindowSize().x,
                (unsigned int) GraphicsManager::getManager()->getRenderWindowSize().y );
            break;
        case OGE_INPUT_JOYSTICK:
            if (mOIS->getNumberOfDevices(OIS::OISJoyStick) <= 0)
                return 0;
            inpDevice = new JoystickDevice(mOIS, name);
            // NEXT Does the joystick also need to setWidth as the mouse?
            break;
/* NEXT       case OGE_INPUT_TABLET:
            if (mOIS->getNumberOfDevices(OIS::OISTablet) <= 0)
                return 0;
            inpDevice = new TabletDevice(mOIS, name);
            break;
*/
        case OGE_INPUT_KEYBOARD_ANY:
            if (mOIS->getNumberOfDevices(OIS::OISKeyboard) <= 0)
                return 0;
            inpDevice = new KeyboardDeviceAny(mOIS, name);
            break;
        case OGE_INPUT_MOUSE_ANY:
            if (mOIS->getNumberOfDevices(OIS::OISMouse) <= 0)
                return 0;
            inpDevice = new MouseDeviceAny(mOIS, name);
            ((MouseDevice*)inpDevice)->setRenderSizes(
                (unsigned int) GraphicsManager::getManager()->getRenderWindowSize().x,
                (unsigned int) GraphicsManager::getManager()->getRenderWindowSize().y );
            break;
        case OGE_INPUT_JOYSTICK_ANY:
            if (mOIS->getNumberOfDevices(OIS::OISJoyStick) <= 0)
                return 0;
            inpDevice = new JoystickDeviceAny(mOIS, name);
            // NEXT Does the joystick also need to setWidth as the mouse?
            break;
/* NEXT       case OGE_INPUT_TABLET_ANY:
            if (mOIS->getNumberOfDevices(OIS::OISTablet) <= 0)
                return 0;
            inpDevice = new TabletDeviceAny(mOIS, name);
            break;
*/
        default:
            return 0;
        }

        return addInputDevice(inpDevice);
    }
    //-------------------------------------------------------------------------
    void InputManagerOIS::windowResized(unsigned int width, unsigned int height)
    {
        if (width == 0 || height == 0)
            return;

        DeviceMap::Iterator iter = mInputDevices.begin();
        for (; iter != mInputDevices.end(); ++iter)
        {
            // TODO also the others types? How to do with MouseDeviceAny?
            if (iter->second->getDeviceType() == OGE_INPUT_MOUSE)
            {
                ((MouseDevice*) (iter->second))->setRenderSizes(width, height);
            }
        }
    }
    //-------------------------------------------------------------------------
}
