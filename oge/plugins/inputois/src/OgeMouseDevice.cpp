/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/input/OgeMouseDevice.h"
#include "oge/input/OgeInputManager.h"
#include "oge/input/OgeInputEvent.h"
#include "oge/input/OgeInputEventOIS.h"
#include "oge/input/OgeInputEventHandler.h"
#include "oge/logging/OgeLogManager.h"

namespace oge
{
    //---------------------------------------------------------------------------
    // Mouse device based on passing the library event as an Any
    //---------------------------------------------------------------------------
    MouseDeviceAny::MouseDeviceAny(OIS::InputManager* ois, const String& name)
        : InputDevice(OGE_INPUT_MOUSE_ANY, name), mOIS(ois)
    {
        mOISMouse = static_cast<OIS::Mouse*> 
            (mOIS->createInputObject(OIS::OISMouse, true));
        mOISMouse->setEventCallback(this);
        setDeviceUpdate(true);
    }
    //---------------------------------------------------------------------------
    MouseDeviceAny::~MouseDeviceAny()
    {
        if (mOISMouse && mOIS && InputManager::getManager())
        {
            mOIS->destroyInputObject(mOISMouse);
            mOISMouse = 0;
        }
    }
    //---------------------------------------------------------------------------
    void MouseDeviceAny::update()
    {
        assert(mCanUpdate);
        mOISMouse->capture();
    }
    //---------------------------------------------------------------------------
    bool MouseDeviceAny::mousePressed(const OIS::MouseEvent& arg, 
        OIS::MouseButtonID id)
    {
        mEventHandler->mousePressedAny(arg, id);
        return true;
    }
    //---------------------------------------------------------------------------
    bool MouseDeviceAny::mouseReleased(const OIS::MouseEvent& arg, 
        OIS::MouseButtonID id)
    {
        mEventHandler->mouseReleasedAny(arg, id);
        return true;
    }
    //---------------------------------------------------------------------------
    bool MouseDeviceAny::mouseMoved(const OIS::MouseEvent& arg)
    {
        mEventHandler->mouseMovedAny(arg);
        return true;
    }
    //---------------------------------------------------------------------------
    const String MouseDeviceAny::getAsString(MouseButton button)
    {
        if (button == OIS::MB_Right)
            return "Right Button";
        else if (button == OIS::MB_Left)
            return "Left Button";
        else if (button == OIS::MB_Middle)
            return "Middle Button";
        else if (button == OIS::MB_Button3)
            return "Button 3";
        else if (button == OIS::MB_Button4)
            return "Button 4";
        else if (button == OIS::MB_Button5)
            return "Button 5";
        else if (button == OIS::MB_Button6)
            return "Button 6";
        else if (button == OIS::MB_Button7)
            return "Button 7";

        LOGW("Unsupported button! #" + StringUtil::toString((int)button));
        return "";
    }
    //---------------------------------------------------------------------------
    const OIS::MouseState& MouseDeviceAny::getOISState() const
    {
        return mOISMouse->getMouseState();
    }
    //---------------------------------------------------------------------------
    void MouseDeviceAny::setRenderSizes( unsigned int width, unsigned int height )
    {
        mOISMouse->getMouseState().width = width;
        mOISMouse->getMouseState().height = height;
    }
    //---------------------------------------------------------------------------
} // namespace

namespace oge
{
    //---------------------------------------------------------------------------
    // Mouse device based on oge event
    //---------------------------------------------------------------------------
    MouseDevice::MouseDevice(OIS::InputManager* ois, const String& name)
        : InputDevice(OGE_INPUT_MOUSE, name), mOIS(ois)
    {
        mOISMouse = static_cast<OIS::Mouse*> 
            (mOIS->createInputObject(OIS::OISMouse, true));
        mOISMouse->setEventCallback(this);
        setDeviceUpdate(true);
    }
    //---------------------------------------------------------------------------
    MouseDevice::~MouseDevice()
    {
        if (mOISMouse && mOIS && InputManager::getManager())
        {
            mOIS->destroyInputObject(mOISMouse);
            mOISMouse = 0;
        }
    }
    //---------------------------------------------------------------------------
    void MouseDevice::update()
    {
        assert(mCanUpdate);
        mOISMouse->capture();
    }
    //---------------------------------------------------------------------------
    bool MouseDevice::mousePressed(const OIS::MouseEvent& arg, 
        OIS::MouseButtonID id)
    {
        MouseEventOIS evt(arg);
        evt.mButton = id;
        mEventHandler->mousePressed(evt);
        return true;
    }
    //---------------------------------------------------------------------------
    bool MouseDevice::mouseReleased(const OIS::MouseEvent& arg, 
        OIS::MouseButtonID id)
    {
        MouseEventOIS evt(arg);
        evt.mButton = id;
        mEventHandler->mouseReleased(evt);
        return true;
    }
    //---------------------------------------------------------------------------
    bool MouseDevice::mouseMoved(const OIS::MouseEvent& arg)
    {
        MouseEventOIS evt(arg);
        mEventHandler->mouseMoved(evt);
        return true;
    }
    //---------------------------------------------------------------------------
    const String MouseDevice::getAsString(MouseButton button)
    {
        if (button == OIS::MB_Right)
            return "Right Button";
        else if (button == OIS::MB_Left)
            return "Left Button";
        else if (button == OIS::MB_Middle)
            return "Middle Button";
        else if (button == OIS::MB_Button3)
            return "Button 3";
        else if (button == OIS::MB_Button4)
            return "Button 4";
        else if (button == OIS::MB_Button5)
            return "Button 5";
        else if (button == OIS::MB_Button6)
            return "Button 6";
        else if (button == OIS::MB_Button7)
            return "Button 7";

        LOGW("Unsupported button! #" + StringUtil::toString((int)button));
        return "";
    }
    //---------------------------------------------------------------------------
    const OIS::MouseState& MouseDevice::getOISState() const
    {
        return mOISMouse->getMouseState();
    }
    //---------------------------------------------------------------------------
    void MouseDevice::setRenderSizes( unsigned int width, unsigned int height )
    {
        mOISMouse->getMouseState().width = width;
        mOISMouse->getMouseState().height = height;
    }
    //---------------------------------------------------------------------------
    const InputState* MouseDevice::getState()
    {
        // Not really helpful because OIS::MouseEvent is not a copy
        // and the mouseState does change to fast 
        // MouseEventOIS* evt = new MouseEventOIS( 
        //    OIS::MouseEvent(0, mOISMouse->getMouseState()) );
        const OIS::MouseState& state = mOISMouse->getMouseState();
        mState.absX = state.X.abs;
        mState.absY = state.Y.abs;
        mState.absZ = state.Z.abs;
        mState.relX = state.X.rel;
        mState.relY = state.Y.rel;
        mState.relZ = state.Z.rel;
        mState.buttons = state.buttons;
        mState.width = state.width;
        mState.height = state.height;

        return &mState;
    }
    //---------------------------------------------------------------------------
} // namespace
