/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/input/OgeDefaultInputEventHandler.h"
#include "oge/input/OgeOISIncludes.h"
#include "oge/engine/OgeEngineManager.h"
#include "oge/input/OgeInputEventOIS.h"
#include "oge/graphics/OgeGraphicsManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    DefaultInputEventHandler::DefaultInputEventHandler()
    {
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::keyPressed (const KeyEvent& arg)
    {
    // We could also do this
    //   const OIS::KeyEvent& evt = static_cast<const KeyEventOIS&>(arg).mEvent;
    //   if (evt.key == OIS::KC_ESCAPE)
        InputEventHandler::keyPressed( arg );

        if (arg.mKeyCode == KC_ESCAPE)
        {
            EngineManager::getSingletonPtr()->stop();
        }
        else if (arg.mKeyCode == KC_SYSRQ)
        {
            if (GraphicsManager::getManager())
                GraphicsManager::getManager()->saveScreenshot("OGE_Screenshot.jpg");
        }
        else if (arg.mKeyCode == KC_F1)
        {
            if (GraphicsManager::getManager())
                GraphicsManager::getManager()->toggleDebugOverlay();
        }
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::keyReleased (const KeyEvent& arg)
    {
        InputEventHandler::keyReleased( arg );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::mousePressed (const MouseEvent& arg)
    {
        InputEventHandler::mousePressed( arg );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::mouseReleased (const MouseEvent& arg)
    {
        InputEventHandler::mouseReleased( arg );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::mouseMoved (const MouseEvent& arg)
    {
        InputEventHandler::mouseMoved( arg );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::joyButtonPressed (const JoystickEvent& arg)
    {
        InputEventHandler::joyButtonPressed( arg );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::joyButtonReleased (const JoystickEvent& arg)
    {
        InputEventHandler::joyButtonReleased( arg );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::joyAxisMoved (const JoystickEvent& arg)
    {
        InputEventHandler::joyAxisMoved( arg );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::joyPovMoved (const JoystickEvent& arg)
    {
        InputEventHandler::joyPovMoved( arg );
    }
    //-------------------------------------------------------------------------

    //------------- Any-based input event handler -----------------------------

    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::keyPressedAny(Any arg)
    {
        InputEventHandler::keyPressedAny( arg );

        const OIS::KeyEvent* evt = FastAnyCast(const OIS::KeyEvent*, arg);

        if (evt->key == OIS::KC_ESCAPE)
        {
            EngineManager::getSingletonPtr()->stop();
        }
        else if (evt->key == OIS::KC_SYSRQ)
        {
            if (GraphicsManager::getManager())
                GraphicsManager::getManager()->saveScreenshot("OGE_Screenshot.jpg");
        }
        else if (evt->key == OIS::KC_F1)
        {
            if (GraphicsManager::getManager())
                GraphicsManager::getManager()->toggleDebugOverlay();
        }
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::keyReleasedAny(Any arg)
    {
        InputEventHandler::keyReleasedAny( arg );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::mousePressedAny(Any arg, const int mousebutton)
    {
        InputEventHandler::mousePressedAny( arg, mousebutton );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::mouseReleasedAny(Any arg, const int mousebutton)
    {
        InputEventHandler::mouseReleasedAny( arg, mousebutton );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::mouseMovedAny(Any arg)
    {
        InputEventHandler::mouseMovedAny( arg );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::joyButtonPressedAny(Any arg, int button)
    {
        InputEventHandler::joyButtonPressedAny( arg, button );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::joyButtonReleasedAny(Any arg, int button)
    {
        InputEventHandler::joyButtonReleasedAny( arg, button );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::joyAxisMovedAny(Any arg, int axis)
    {
        InputEventHandler::joyAxisMovedAny( arg, axis );
    }
    //-------------------------------------------------------------------------
    void DefaultInputEventHandler::joyPovMovedAny(Any arg, int pov)
    {
        InputEventHandler::joyPovMovedAny( arg, pov );
    }
    //-------------------------------------------------------------------------
} // namespace
