﻿/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/

#ifndef __OGE_GUICONSOLE_H__
#define __OGE_GUICONSOLE_H__
#include "oge/gui/OgeGuiPrerequisitesMyGUI.h"
#include "MyGUI.h"
#include "BaseLayout/BaseLayout.h"
#include "oge/datastreams/readers/OgeTextReader.h"
#include <limits>

#ifdef max
#    undef max
#    undef min
#endif

namespace oge
{
#if OGE_PLATFORM == OGE_PLATFORM_WIN32
#	pragma warning(push)
#	pragma warning(disable:4275)
#endif
	typedef MyGUI::delegates::CDelegate2<const MyGUI::UString&, const MyGUI::UString&> CommandDelegate;

	namespace formates
	{
		template<typename T> inline std::string format()
		{
			return MyGUI::utility::toString("[ ", (std::numeric_limits<T>::min)(), " | ", (std::numeric_limits<T>::max)(), " ]");
		}
		template<> inline std::string format<bool>()
		{
			return "[ true | false ]";
		}
		template<> inline std::string format<float>()
		{
			return MyGUI::utility::toString("[ ", -(std::numeric_limits<float>::max)(), " | ", (std::numeric_limits<float>::max)(), " ]");
		}
		template<> inline std::string format<double>()
		{
			return MyGUI::utility::toString("[ ", -(std::numeric_limits<double>::max)(), " | ", (std::numeric_limits<double>::max)(), " ]");
		}
	}

	class OGE_GUI_API GuiConsole :
		public MyGUI::Singleton<GuiConsole>,
		public wraps::BaseLayout
	{
	public:
		GuiConsole();
		virtual ~GuiConsole();

		void addToConsole(const MyGUI::UString& _line);
		void addToConsole(const MyGUI::UString& _reason, const MyGUI::UString& _key, const MyGUI::UString& _value);
		void updateLogs();
		void clearConsole();

		/** Method : add command.\n
			@example Add_console_command
			@code
				registerConsoleDelegate("your_command_1", MyGUI::newDelegate(your_func));
				registerConsoleDelegate("your_command_2", MyGUI::newDelegate(your_static_method));
				registerConsoleDelegate("your_command_3", MyGUI::newDelegate(your_class_ptr, &your_class_name::your_method_name));
			@endcode

			signature your method : void method(const MyGUI::UString & _key, const MyGUI::UString & _value)
		*/
		void registerConsoleDelegate(const MyGUI::UString& _command, CommandDelegate::IDelegate* _delegate);

		/** Remove command. */
		void unregisterConsoleDelegate(const MyGUI::UString& _command);

		/** Event : Unknown command.\n
			signature : void method(const MyGUI::UString & _key, const MyGUI::UString & _value)
		*/
		CommandDelegate eventConsoleUnknowCommand;

		const MyGUI::UString& getConsoleStringCurrent() const;
		const MyGUI::UString& getConsoleStringError() const;
		const MyGUI::UString& getConsoleStringSuccess() const;
		const MyGUI::UString& getConsoleStringUnknow() const;
		const MyGUI::UString& getConsoleStringFormat() const;

		bool getVisible();
		void setVisible(bool _visible);
		
		bool showingLogs;

		template <typename T>
		bool isAction(T& _result, const MyGUI::UString& _key, const MyGUI::UString& _value, const MyGUI::UString& _format = "")
		{
			if (_value.empty())
			{
				addToConsole(getConsoleStringCurrent(), _key, MyGUI::utility::toString(_result));
			}
			else
			{
				if (!MyGUI::utility::parseComplex(_value, _result))
				{
					addToConsole(getConsoleStringError(), _key, _value);
					addToConsole(getConsoleStringFormat(), _key, _format.empty() ? formates::format<T>() : _format);
				}
				else
				{
					addToConsole(getConsoleStringSuccess(), _key, _value);
					return true;
				}
			}
			return false;
		}

	private:
		void notifyWindowButtonPressed(MyGUI::Window* _sender, const std::string& _button);

		void notifyMouseButtonClick(MyGUI::Widget* _sender);
		void notifyComboAccept(MyGUI::ComboBox* _sender, size_t _index);
		void notifyButtonPressed(MyGUI::Widget* _sender, MyGUI::KeyCode _key, MyGUI::Char _char);

		void internalCommand(MyGUI::Widget* _sender, const MyGUI::UString& _key, const MyGUI::UString& _value);

	private:
		MyGUI::EditBox* mListHistory;
		MyGUI::ComboBox* mComboCommand;
		MyGUI::Button* mButtonSubmit;

		typedef std::map<MyGUI::UString, CommandDelegate> MapDelegate;
		MapDelegate mDelegates;

		MyGUI::UString mStringCurrent;
		MyGUI::UString mStringError;
		MyGUI::UString mStringSuccess;
		MyGUI::UString mStringUnknow;
		MyGUI::UString mStringFormat;

		// если текущий текст автодополнен
		bool mAutocomleted;

		String mLogFilename;
		TextReader *mLogReader;

		static GuiConsole* m_instance;
	};
}
#if OGE_PLATFORM == OGE_PLATFORM_WIN32
#	pragma warning(pop)
#endif
#endif // __CONSOLE_H__
