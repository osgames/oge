/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_GUIMANAGERMYGUI_H__
#define __OGE_GUIMANAGERMYGUI_H__

#include "oge/gui/OgeGuiPrerequisitesMyGUI.h"
#include "oge/resource/OgeResourceControllerOgeImpl.h"
#include "oge/gui/OgeGuiManager.h"
#include "oge/OgeSingleton.h"

#include "oge/input/OgeInputEvent.h"
#include "MyGUI.h"
#include "MyGUI_OgrePlatform.h"
#include "oge/gui/OgeGuiConsole.h"

namespace oge
{
    class OGE_GUI_API GuiManagerMyGUI : public GuiManager, 
        public Singleton<GuiManagerMyGUI>
    {
    private:
        MyGUI::Gui* mGUI;
        MyGUI::OgrePlatform* mPlatform;
        MyGUI::WidgetPtr mMainWidget;
		GuiConsole *mConsole;
        MyGUI::VectorWidgetPtr mListWindowRoot;

    public:

        static GuiManagerMyGUI* createSingleton();
        static void destroySingleton();
        static GuiManagerMyGUI* getSingletonPtr() { return mSingleton; }
        static GuiManagerMyGUI& getSingleton()
        {
            assert(mSingleton);
            return *mSingleton;
        }
    protected:
        // System methods
        bool initialise();
        void shutdown();
        void tick(double currentTime);
    private:
        GuiManagerMyGUI();
        ~GuiManagerMyGUI();

        /**
         * @note This method works because the key event uses
         *       the same enum values than the OIS one
         *       (a simple cast (MyGUI::KeyCode) is used).
         */
        void keyPressed (const KeyEvent& arg);
        /**
         * @note This method works because the key event uses
         *       the same enum values than the OIS one
         *       (a simple cast (MyGUI::KeyCode) is used).
         */
        void keyReleased (const KeyEvent& arg);
        /**
         * @note This method works because the mouse button uses
         *       the same enum values than the OIS one
         *       (a simple cast (MyGUI::MouseButton) is used).
         */
        void mousePressed (const MouseEvent& arg);
        /**
         * @note This method works because the mouse button uses
         *       the same enum values than the OIS one
         *       (a simple cast (MyGUI::MouseButton) is used).
         */
        void mouseReleased (const MouseEvent& arg);
        void mouseMoved (const MouseEvent& arg);
        void joyButtonPressed (const JoystickEvent& arg);
        void joyButtonReleased (const JoystickEvent& arg);
        void joyAxisMoved (const JoystickEvent& arg);
        void joyPovMoved (const JoystickEvent& arg);

    public:
        // ------------- MyGui Specific methods ---------------
        MyGUI::Widget* getWidget(const String& layout);
        MyGUI::Widget* getWidgetT(const String& layout);

        // TEMP how to do a "general" solution :(const String& widget, enum type) and a switch?
        String getSelected(const String& widget, const String& widgetType);
        /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        const String getLibrariesVersion();

		/**
		 * Get the console
		 *@return the console
		 */
		GuiConsole *getConsole() { return mConsole; }

		/**
		 * Get the MyGUI singleton
		 *@return the MyGUI singleton pointer
		 */
		MyGUI::Gui* getMyGUI() { return mGUI; }

		/**
		 * Return the height of the current viewport
		 *@return the height of the current viewport
		 */
		const int getViewHeight();

		/**
		 * Return the width of the current viewport
		 *@return the width of the current viewport
		 */
		const int getViewWidth();

        /**
         * Load a gui layout from file.
         * In the case of MyGui those are .layout files.
         */
        void loadLayout(const String& filename, const String& layout);

		/**
		 * Show or hide the cursor
		 * @param visible Whether the cursor should be visible or not
		 */
		void setCursorVisible(bool visible);
    };
}
#endif // __OGE_GUIMANAGERMYGUI_H__
