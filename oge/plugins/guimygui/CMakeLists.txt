# CMakeLists.txt  -  Version usable for OGE 0.3.64 or later.
#
# This file is part of OGE (Open Game Engine)
# For the latest info, see http://sourceforge.net/projects/oge
# Copyright (c) 2008 The OGE Team
# Also see acknowledgements in Readme.txt
#

SET(Current_Lib
OgeGuiMyGUI
)

SET(Current_Sources
src/OgeGuiConsole.cpp
src/OgeGuiManagerMyGUI.cpp
src/OgeGuiPluginMyGUI.cpp
)


SET(Current_Headers
include/oge/gui/OgeGuiConsole.h
include/oge/gui/OgeGuiManagerMyGUI.h
include/oge/gui/OgeGuiPluginMyGUI.h
include/oge/gui/OgeGuiPrerequisitesMyGUI.h
)

SET(Current_Include_Dirs
include
${CMAKE_SOURCE_DIR}/oge/utilities/include
${CMAKE_SOURCE_DIR}/oge/core/include
${CMAKE_SOURCE_DIR}/oge/plugins/graphicsogre/include
)

INCLUDE(DefineFlags)
FIND_PACKAGE(OGRE)
FIND_PACKAGE(MyGUI)
FIND_PACKAGE(Poco)
FIND_PACKAGE(TBB)
FIND_PACKAGE(FastDelegates)
FIND_PACKAGE(ZLib)
FIND_PACKAGE(ZZip)

IF(OGRE_FOUND AND MyGUI_FOUND AND Poco_FOUND AND TBB_FOUND AND FastDelegates_FOUND AND ZLib_FOUND AND ZZip_FOUND)
   INCLUDE_DIRECTORIES(${Current_Include_Dirs} ${Poco_Foundation_INCLUDE_DIR} ${Poco_Util_INCLUDE_DIR}  ${TBB_INCLUDE_DIR} ${FastDelegates_INCLUDE_DIR} ${TBB_INCLUDE_DIR} ${ZLib_INCLUDE_DIR} ${ZZip_INCLUDE_DIR} ${OGRE_OgreMain_INCLUDE_DIR} ${OGRE_BUILD_INCLUDE_DIR} ${MyGUIEngine_INCLUDE_DIR} ${MyGUIOGREPlatform_INCLUDE_DIR} ${MyGUI_COMMON_INCLUDE_DIR})

  SOURCE_GROUP("Header Files" FILES ${Current_Headers})
  SOURCE_GROUP("Source Files" FILES ${Current_Sources})

  ADD_LIBRARY(${Current_Lib} SHARED ${Current_Sources} ${Current_Headers})

IF(OGE_DOUBLE)
  ADD_DEFINITIONS(/DOGE_DOUBLE)
ENDIF()

  SET_TARGET_PROPERTIES(${Current_Lib} PROPERTIES DEFINE_SYMBOL OGE_GUIMYGUI_EXPORTS)

  TARGET_LINK_LIBRARIES(${Current_Lib} OgeCore)
  TARGET_LINK_LIBRARIES(${Current_Lib} OgeUtilities)
  TARGET_LINK_LIBRARIES(${Current_Lib} OgeGraphicsOGRE)
  TARGET_LINK_LIBRARIES(${Current_Lib} debug ${OGRE_OgreMain_LIBRARY_DBG} optimized ${OGRE_OgreMain_LIBRARY})
  TARGET_LINK_LIBRARIES(${Current_Lib} debug ${MyGUIEngine_LIBRARY_DBG} optimized ${MyGUIEngine_LIBRARY})
  TARGET_LINK_LIBRARIES(${Current_Lib} debug ${MyGUIOGREPlatform_LIBRARY_DBG} optimized ${MyGUIOGREPlatform_LIBRARY})

  MESSAGE("Dependency for ${Current_Lib}: OK\n")
ELSE()
  MESSAGE("Dependency for ${Current_Lib}: FAIL\n")
ENDIF(OGRE_FOUND AND MyGUI_FOUND AND Poco_FOUND AND TBB_FOUND AND FastDelegates_FOUND AND ZLib_FOUND AND ZZip_FOUND)
