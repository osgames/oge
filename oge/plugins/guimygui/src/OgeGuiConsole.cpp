﻿/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
is free software; you can redistribute it and/or modify it under the terms
of the GNU Lesser General Public License (LGPL) as published by the
Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
a written agreement from the 'OGE Team'. The exact wording of this
license can be obtained from the 'OGE Team'. In essence this
OGE Unrestricted License state that the GNU Lesser General Public License
applies except that the software is distributed with no limitation or
requirements to publish or give back to the OGE Team changes made
to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/gui/OgeGuiConsole.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/datastreams/OgeFileSystemStream.h"
#include "oge/datastreams/OgeDataStream.h"
#include "MyGUI_InputManager.h"

// внутри неймспейса demo почему то не линкуется, даже если указать абсолютные пути
template <> oge::GuiConsole* MyGUI::Singleton<oge::GuiConsole>::msInstance = nullptr;
template <> const char* MyGUI::Singleton<oge::GuiConsole>::mClassTypeName("GuiConsole");

namespace oge
{
	GuiConsole::GuiConsole() : BaseLayout("console.layout")
	{
		assignWidget(mListHistory, "list_History");
		assignWidget(mComboCommand, "combo_Command");
		assignWidget(mButtonSubmit, "button_Submit");

		MyGUI::Window* window = mMainWidget->castType<MyGUI::Window>(false);
		if (window != nullptr) window->eventWindowButtonPressed += newDelegate(this, &GuiConsole::notifyWindowButtonPressed);

		mStringCurrent = mMainWidget->getUserString("Current");
		mStringError = mMainWidget->getUserString("Error");
		mStringSuccess = mMainWidget->getUserString("Success");
		mStringUnknow = mMainWidget->getUserString("Unknown");
		mStringFormat = mMainWidget->getUserString("Format");

		mAutocomleted = false;

		mComboCommand->eventComboAccept += newDelegate(this, &GuiConsole::notifyComboAccept);
		mComboCommand->eventKeyButtonPressed += newDelegate(this, &GuiConsole::notifyButtonPressed);
		mButtonSubmit->eventMouseButtonClick += newDelegate(this, &GuiConsole::notifyMouseButtonClick);
		mListHistory->setOverflowToTheLeft(true);
		mListHistory->setMaxTextLength(4096 * 4);

		mMainWidget->setVisible(false);
		mMainWidget->setRealSize(1.0,0.5);

		mLogReader = 0;
	}

	GuiConsole::~GuiConsole()
	{
		if(mLogReader) {
			delete mLogReader;
		}
	}

	void GuiConsole::notifyWindowButtonPressed(MyGUI::Window* _sender, const std::string& _button)
	{
		if (_button == "close")
		{
			mMainWidget->setVisible(false);
		}
	}

	void GuiConsole::notifyMouseButtonClick(MyGUI::Widget* _sender)
	{
		notifyComboAccept(mComboCommand, MyGUI::ITEM_NONE);
	}

	void GuiConsole::notifyComboAccept(MyGUI::ComboBox* _sender, size_t _index)
	{
		const MyGUI::UString& command = _sender->getOnlyText();
		if (command == "") return;

		MyGUI::UString key = command;
		MyGUI::UString value;

		size_t pos = command.find(' ');
		if (pos != MyGUI::UString::npos)
		{
			key = command.substr(0, pos);
			value = command.substr(pos + 1);
		}

		MapDelegate::iterator iter = mDelegates.find(key);
		if (iter != mDelegates.end())
		{
			iter->second(key, value);
		}
		else
		{
			if (eventConsoleUnknowCommand.empty())
			{
				addToConsole(mStringUnknow + "'" + key + "'");
			}
			else
			{
				eventConsoleUnknowCommand(key, value);
			}
		}

		_sender->setCaption("");
	}


	void GuiConsole::notifyButtonPressed(MyGUI::Widget* _sender, MyGUI::KeyCode _key, MyGUI::Char _char)
	{
		MyGUI::EditBox* edit = _sender->castType<MyGUI::EditBox>();
		size_t len = edit->getCaption().length();
		if ((_key == MyGUI::KeyCode::Backspace) && (len > 0) && (mAutocomleted))
		{
			edit->deleteTextSelection();
			len = edit->getCaption().length();
			edit->eraseText(len - 1);
		}

		MyGUI::UString command = edit->getCaption();
		if (command.length() == 0)
			return;

		for (MapDelegate::iterator iter = mDelegates.begin(); iter != mDelegates.end(); ++iter)
		{
			if (iter->first.find(command) == 0)
			{
				if (command == iter->first) break;
				edit->setCaption(iter->first);
				edit->setTextSelection(command.length(), iter->first.length());
				mAutocomleted = true;
				return;
			}
		}
		mAutocomleted = false;
	}

	void GuiConsole::updateLogs()
	{
		// update the console with the latest lines from the log manager
		Logger *l = LogManager::getSingleton().getLogger(0);
		if(l) {

			const String& filename = l->getFilename();
			if(mLogFilename.empty() || mLogFilename != filename) {
				mLogFilename = String(filename);

				if(mLogReader) {
					delete mLogReader;
				}

				FileSystemStream* stream = new FileSystemStream();
				stream->openRead(filename);
				mLogReader = new TextReader(stream);
			}

			DataStreamPtr ds = l->getDataStream();

			if(!ds.isNull()) {
				while(true) {
					String line = mLogReader->readLine();
					if(line.empty()) {
						break;
					}

					addToConsole(line);
				}
			}
		}
	}

	void GuiConsole::addToConsole(const MyGUI::UString& _line)
	{
		if (mListHistory->getCaption().empty())
			mListHistory->addText(_line);
		else
			mListHistory->addText("\n" + _line);

		//mListHistory->setTextCursor(0);
		mListHistory->setTextSelection(mListHistory->getTextLength(), mListHistory->getTextLength());
	}

	void GuiConsole::clearConsole()
	{
		mListHistory->setCaption("");
	}

	void GuiConsole::registerConsoleDelegate(const MyGUI::UString& _command, CommandDelegate::IDelegate* _delegate)
	{
		mComboCommand->addItem(_command);
		MapDelegate::iterator iter = mDelegates.find(_command);
		if (iter != mDelegates.end())
		{
			MYGUI_LOG(Warning, "console - command '" << _command << "' already exist");
		}
		mDelegates[_command] = _delegate;
	}

	void GuiConsole::unregisterConsoleDelegate(const MyGUI::UString& _command)
	{
		MapDelegate::iterator iter = mDelegates.find(_command);
		if (iter != mDelegates.end())
		{
			mDelegates.erase(iter);
			for (size_t i = 0; i < mComboCommand->getItemCount(); ++i)
			{
				if (mComboCommand->getItemNameAt(i) == _command)
				{
					mComboCommand->removeItemAt(i);
					break;
				}
			}
		}
		else
			MYGUI_LOG(Warning, "console - command '" << _command << "' doesn't exist");
	}

	void GuiConsole::internalCommand(MyGUI::Widget* _sender, const MyGUI::UString& _key, const MyGUI::UString& _value)
	{
		if (_key == "clear")
		{
			clearConsole();
		}
	}

	void GuiConsole::addToConsole(const MyGUI::UString& _reason, const MyGUI::UString& _key, const MyGUI::UString& _value)
	{
		addToConsole(MyGUI::utility::toString(_reason, "'", _key, " ", _value, "'"));
	}

	const MyGUI::UString& GuiConsole::getConsoleStringCurrent() const
	{
		return mStringCurrent;
	}

	const MyGUI::UString& GuiConsole::getConsoleStringError() const
	{
		return mStringError;
	}

	const MyGUI::UString& GuiConsole::getConsoleStringSuccess() const
	{
		return mStringSuccess;
	}

	const MyGUI::UString& GuiConsole::getConsoleStringUnknow() const
	{
		return mStringUnknow;
	}

	const MyGUI::UString& GuiConsole::getConsoleStringFormat() const
	{
		return mStringFormat;
	}

	bool GuiConsole::getVisible()
	{
		return mMainWidget->getVisible();
	}

	void GuiConsole::setVisible(bool _visible)
	{
		mMainWidget->setVisible(_visible);
		if (_visible)
		{
			MyGUI::InputManager::getInstance().setKeyFocusWidget(mComboCommand);
			mComboCommand->eraseText(0, mComboCommand->getTextCursor());
		}
		else
		{
			MyGUI::InputManager::getInstance().setKeyFocusWidget(nullptr);
		}
	}

} // namespace demo
