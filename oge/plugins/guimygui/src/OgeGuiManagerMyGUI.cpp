/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
is free software; you can redistribute it and/or modify it under the terms
of the GNU Lesser General Public License (LGPL) as published by the
Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
a written agreement from the 'OGE Team'. The exact wording of this
license can be obtained from the 'OGE Team'. In essence this
OGE Unrestricted License state that the GNU Lesser General Public License
applies except that the software is distributed with no limitation or
requirements to publish or give back to the OGE Team changes made
to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/gui/OgeGuiManagerMyGUI.h"
#include "oge/resource/OgeResourceRegistry.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/graphics/OgeGraphicsSceneManagerOGRE.h"
#include "oge/graphics/OgeRenderTargetManager.h"
#include "oge/graphics/OgeGraphicsManagerOGRE.h"
#include "oge/OgeProfiler.h"
#include "oge/config/OgeOption.h"
#include "oge/config/OgeOptionManager.h"
#include "oge/filesystem/OgeFileSystem.h"

// WIP
#include "OgreResourceManager.h"
#include "OgreRenderWindow.h"

namespace oge
{
    //-------------------------------------------------------------------------
    template<> GuiManagerMyGUI* Singleton<GuiManagerMyGUI>::mSingleton = 0;
    //-------------------------------------------------------------------------
    GuiManagerMyGUI::GuiManagerMyGUI() :
            GuiManager("GuiManager"), // Not GuiManagerMyGUI as we -for now- want only one mgr for each type
            mGUI(0),
            mPlatform(0),
            mMainWidget(0),
            mConsole(0)
    {

    }
    //-------------------------------------------------------------------------
    GuiManagerMyGUI::~GuiManagerMyGUI()
    {

    }
    ////-------------------------------------------------------------------------
    bool GuiManagerMyGUI::initialise()
    {
        LOG("Gui Initialise...");

        // The render window MUST be set because the mygui::initialise needs it
        if (!mRenderWindow)
        {
			// Attempt to get it from Ogre
			GraphicsManagerOGRE* mgr = (GraphicsManagerOGRE*)GraphicsManager::getManager();
			if(mgr) {
				mRenderWindow = mgr->getRenderWindow();
			}
		}

		if(!mRenderWindow) {
            //EngineManager::getSingletonPtr()->controlSystem("GuiManager", System::INITIALISE);
            LOGE("The render window is not set!");
            return false;
        }

        LOG("Got a render window: initialising the MyGUI based gui manager");

        // Temporary solution for the above problem
        OptionPtr option = OptionManager::getSingleton().getOption("oge.filesystem");
        OptionPtr option2 = option->getOption("group");
        Option::Iterator groupIter = option2->getOptionIterator();

        for (groupIter.begin(); groupIter.hasNext(); groupIter.next())
        {
            if (groupIter.getName() != "MyGuiGroup")
                continue;

            OptionPtr groupArchivesOption = 
                groupIter.getOption()->getOption("archive");

            Option::ValueIterator valueIter2 = 
                groupArchivesOption->getValueIterator();
            for(valueIter2.begin(); valueIter2.hasNext(); valueIter2.next())
            {
                ArchivePtr archive = FileSystem::getSingletonPtr()->getArchive(valueIter2.getValue(), false);
                std::cout << archive->getName() << std::endl;
                Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archive->getName(), "FileSystem");
            }
        }

#if MYGUI_VERSION_MAJOR < 3
        mGUI = new MyGUI::Gui();
        mGUI->initialise( (Ogre::RenderWindow*) mRenderWindow, "core.xml" ); // TODO get from oge.xml
#else
        mPlatform = new MyGUI::OgrePlatform();

        GraphicsSceneManagerOGRE* mgr = (GraphicsSceneManagerOGRE*)GraphicsManager::getManager()->getActiveSceneManager();
        mPlatform->initialise((Ogre::RenderWindow*) mRenderWindow, mgr->getOgreSceneManager());

        mGUI = new MyGUI::Gui();
        mGUI->initialise(); // TODO get from oge.xml

		// load the console
		mConsole = new GuiConsole();
#endif

        if (!System::initialise())
            return false;

        return true;
    }
    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::shutdown()
    {
        LOG("Gui Shutdown...");
        System::shutdown();

		if(mConsole) {
			delete mConsole;
			mConsole = 0;
		}

        if (mGUI)
        {
            MyGUI::LayoutManager::getInstance().unloadLayout(mListWindowRoot);
            mListWindowRoot.clear();

            mGUI->shutdown();
            delete mGUI;
            mGUI = 0;
#if MYGUI_VERSION_MAJOR > 2
            mPlatform->shutdown();
            delete mPlatform;
            mPlatform = 0;
#endif
        }
    }

    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::tick(double currentTime)
    {
		OgeProfileGroup("GUI MyGUI", OGEPROF_SYSTEMS);
#if MYGUI_VERSION_MAJOR <= 3
        mGUI->injectFrameEntered(1); // todo evt.timeSinceLastFrame
#endif
		if(mConsole && mConsole->getVisible() && mConsole->showingLogs) {
			mConsole->updateLogs();
		}
    }
    //-------------------------------------------------------------------------
    GuiManagerMyGUI* GuiManagerMyGUI::createSingleton()
    {
        if (!mSingleton)
            mSingleton = new GuiManagerMyGUI();

        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::destroySingleton()
    {
        if (mSingleton)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::keyPressed (const KeyEvent& arg)
    {
#if MYGUI_VERSION_MAJOR <= 2 && MYGUI_VERSION_MINOR <= 2 && MYGUI_VERSION_PATCH <= 0
        mGUI->injectKeyPress((MyGUI::KeyCode)(arg.mKeyCode));
#elif MYGUI_VERSION_MAJOR < 3
        mGUI->injectKeyPress(MyGUI::KeyCode((MyGUI::KeyCode::Enum)arg.mKeyCode));
#else
		mGUI->injectKeyPress(MyGUI::KeyCode::Enum(arg.mKeyCode),(MyGUI::Char)arg.getAsText());
#endif

    }
    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::keyReleased (const KeyEvent& arg)
    {
#if MYGUI_VERSION_MAJOR <= 2 && MYGUI_VERSION_MINOR <= 2 && MYGUI_VERSION_PATCH <= 0
        mGUI->injectKeyRelease((MyGUI::KeyCode)arg.mKeyCode);
#elif MYGUI_VERSION_MAJOR < 3
        mGUI->injectKeyRelease( MyGUI::KeyCode((MyGUI::KeyCode::Enum)arg.mKeyCode) );
#else
        mGUI->injectKeyRelease( MyGUI::KeyCode::Enum(arg.mKeyCode));
#endif
    }
    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::mousePressed (const MouseEvent& arg)
    {
#if MYGUI_VERSION_MAJOR <= 2 && MYGUI_VERSION_MINOR <= 2 && MYGUI_VERSION_PATCH <= 0
        mGUI->injectMousePress(arg.getAbsX(), arg.getAbsY(), (MyGUI::MouseButton)arg.getButton());
#elif MYGUI_VERSION_MAJOR < 3
        mGUI->injectMousePress(arg.getAbsX(), arg.getAbsY(), MyGUI::MouseButton((MyGUI::MouseButton::Enum)arg.getButton()) );
#else
        mGUI->injectMousePress(arg.getAbsX(), arg.getAbsY(), MyGUI::MouseButton::Enum(arg.getButton()) );
#endif
    }
    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::mouseReleased (const MouseEvent& arg)
    {
#if MYGUI_VERSION_MAJOR <= 2 && MYGUI_VERSION_MINOR <= 2 && MYGUI_VERSION_PATCH <= 0
        mGUI->injectMouseRelease(arg.getAbsX(), arg.getAbsY(), (MyGUI::MouseButton)arg.getButton());
#elif MYGUI_VERSION_MAJOR < 3
        mGUI->injectMouseRelease(arg.getAbsX(), arg.getAbsY(), MyGUI::MouseButton((MyGUI::MouseButton::Enum)arg.getButton()) );
#else
        mGUI->injectMouseRelease(arg.getAbsX(), arg.getAbsY(), MyGUI::MouseButton::Enum(arg.getButton()) );
#endif
    }
    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::mouseMoved(const MouseEvent& arg)
    {
        mGUI->injectMouseMove(arg.getAbsX(), arg.getAbsY(), arg.getAbsZ());
    }
    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::joyButtonPressed (const JoystickEvent& arg)
    {
        LOGD("joy button pressed todo");
    }
    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::joyButtonReleased (const JoystickEvent& arg)
    {
        LOGD("joy button released todo");
    }
    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::joyAxisMoved (const JoystickEvent& arg)
    {
        LOGD("joyAxisMoved todo");
    }
    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::joyPovMoved (const JoystickEvent& arg)
    {
        LOGD("joyPovMoved todo");
    }
    //-------------------------------------------------------------------------
    void GuiManagerMyGUI::loadLayout(const String& filename, const String& layout)
    {
        // TODO See _parent in BaseLayout::loadLayout(MyGUI::WidgetPtr _parent)
        mListWindowRoot = MyGUI::LayoutManager::getInstance().load(filename); // TODO use the Ogre::ResourceGroupManager::Group parameter

        for (MyGUI::VectorWidgetPtr::iterator iter=mListWindowRoot.begin(); iter!=mListWindowRoot.end(); ++iter) {
            if ((*iter)->getName() == layout) {
                mMainWidget = (*iter);
                return;
            }
        }

        LOGW("Root widget named: '"+layout+"' was not found.");
    }
    //-------------------------------------------------------------------------
    MyGUI::Widget* GuiManagerMyGUI::getWidget(const String& layout)
    {
        for (MyGUI::VectorWidgetPtr::iterator iter=mListWindowRoot.begin(); iter!=mListWindowRoot.end(); ++iter) {
            if ((*iter)->getName() == layout) {
                return (*iter);
            }
        }
        return 0;
    }
    //-------------------------------------------------------------------------
    MyGUI::Widget* GuiManagerMyGUI::getWidgetT(const String& layout)
    {
        return mGUI->findWidgetT(layout,false);
    }
    //-------------------------------------------------------------------------
    String GuiManagerMyGUI::getSelected(const String& widget, const String& widgetType)
    {
        String selection;
        // TODO catch the error because findWidget can throw an error
        //      but be sure to log correctly
        // try
        // todo switch for each type fo widget type!
        if (widgetType == "Edit")
        {
            MyGUI::EditPtr edit = mGUI->findWidget<MyGUI::Edit>(widget);
            if (edit)
                return edit->getCaption();
        }
        else if (widgetType == "List")
        {
            MyGUI::ListPtr list = mGUI->findWidget<MyGUI::List>(widget);
            if (list)
            {
                // TODO replace by return list->getItemIndexSelected();
                // because next line is obsolete
                size_t index = list->getItemSelect();
                if (index != MyGUI::ITEM_NONE)
                {
                    // TODO replace by return list->getItemNameAt( index );
                    // because next line is obsolete
                    return list->getItem( index );
                }
            }
        }
        // catch
        return selection;
    }
    //-------------------------------------------------------------------------
    const String GuiManagerMyGUI::getLibrariesVersion()
    {
        String version("Gui: MYGUI ");
        version += StringUtil::toString((int)MYGUI_VERSION_MAJOR);
        version += ".";
        version += StringUtil::toString((int)MYGUI_VERSION_MINOR);
        version += ".";
        version += StringUtil::toString((int)MYGUI_VERSION_PATCH);
        version += " ";
        version += StringUtil::toString((int)MYGUI_VERSION);
        version += " \n";
        return version;
    }
    //-----------------------------------------------------------------------------
	const int GuiManagerMyGUI::getViewHeight()
	{
		return mGUI->getViewHeight();
	}
    //-----------------------------------------------------------------------------
	const int GuiManagerMyGUI::getViewWidth()
	{
		return mGUI->getViewWidth();
	}
	//-----------------------------------------------------------------------------
	void GuiManagerMyGUI::setCursorVisible(bool visible)
	{
		mGUI->setVisiblePointer(visible);
	}
	//-----------------------------------------------------------------------------
}
