/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_PHYSICSDEBUGDRAWERBULLET_H__
#define __OGE_PHYSICSDEBUGDRAWERBULLET_H__

#include "oge/physics/OgePhysicsPrerequisitesBullet.h"

namespace oge
{
    /**
     * Display debug shapes for the existing physical objects
     * (contact points, static & dynamic objects, ...)
     *
     * @author Steven 'lazalong' Gay, Vincenzo 'firecool' Greco
     */
    class OGE_PHYSICS_API DebugDrawer : public btIDebugDraw
    {
    public:
        enum DebugMode
        {
            OGE_NoDebug          = 0,
            OGE_Wireframe        = 1,
            OGE_Aabb             = 2,
            OGE_ContactPoints    = 4,
            OGE_Text             = 8,
            OGE_Constraints      = 16,
            OGE_ConstraintLimits = 32,
            OGE_ExcludeTerrainShape = 64
        } mDebugMode;

    private:
        btDynamicsWorld* mWorld;

    public:
        DebugDrawer(btDynamicsWorld* world);
        virtual ~DebugDrawer();

        void step();

        // ---------- btIDebugDraw abstract methods -------
        void drawLine(const btVector3& from, const btVector3& to,
            const btVector3& color);
        void drawContactPoint(const btVector3& PointOnB, 
            const btVector3& normalOnB, btScalar distance,
            int lifeTime, const btVector3& color);
        void reportErrorWarning(const char* warningString);
        void draw3dText(const btVector3& location, const char* textString);
        /// 0 == off, 0 != on
        void setDebugMode(int mode);
        /// 0 == off, 0 != on
        int getDebugMode() const { return (int)mDebugMode; }

        void drawDebugSphere(btScalar radius, const btTransform& transform, const btVector3& color);
        void drawObject(const btTransform& worldTransform, const btCollisionShape* shape, const btVector3& color);
        void drawConstraint(btTypedConstraint* constraint);

    };

    /**
     *
     * @author Vincenzo 'firecool' Greco
     */
    class ProcessTriangleCallback : public btTriangleCallback, public btInternalTriangleIndexCallback
    {
        DebugDrawer*	m_debugDrawer;
        btVector3	m_color;
        btTransform	m_worldTrans;

    public:

        ProcessTriangleCallback(DebugDrawer* debugDrawer, const btTransform& worldTrans,
            const btVector3& color)
            : m_debugDrawer(debugDrawer),
            m_color(color),
            m_worldTrans(worldTrans)
        {
        }

        virtual void internalProcessTriangleIndex(btVector3* triangle, int partId, int triangleIndex)
        {
            processTriangle(triangle,partId,triangleIndex);
        }

        virtual void processTriangle(btVector3* triangle, int partId, int triangleIndex)
        {
            OGE_UNUSED(partId)
            OGE_UNUSED(triangleIndex)

            btVector3 wv0,wv1,wv2;
            wv0 = m_worldTrans*triangle[0];
            wv1 = m_worldTrans*triangle[1];
            wv2 = m_worldTrans*triangle[2];
            m_debugDrawer->drawLine(wv0,wv1,m_color);
            m_debugDrawer->drawLine(wv1,wv2,m_color);
            m_debugDrawer->drawLine(wv2,wv0,m_color);
        }
    };

}
#endif
