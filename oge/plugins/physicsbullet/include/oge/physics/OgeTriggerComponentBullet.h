/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_TRIGGERCOMPONENTBULLET_H__
#define __OGE_TRIGGERCOMPONENTBULLET_H__

#include "oge/physics/OgePhysicsPrerequisitesBullet.h"
#include "oge/object/OgeComponent.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"

#include "oge/physics/OgeTriggerComponent.h"
#include "oge/object/OgeObjectManager.h"
#include "oge/physics/OgePhysicsComponent.h"

/* DOC TEMP
http://www.bulletphysics.com/mediawiki-1.5.8/index.php?title=Collision_Callbacks_and_Triggers
http://www.bulletphysics.com/Bullet/phpBB3/viewtopic.php?f=9&t=3026
http://www.bulletphysics.com/Bullet/phpBB3/viewtopic.php?f=9&t=3293
http://www.bulletphysics.com/Bullet/phpBB3/viewtopic.php?f=9&t=2943 !!!!
http://www.bulletphysics.com/Bullet/phpBB3/viewtopic.php?f=9&t=1943
http://www.bulletphysics.com/Bullet/phpBB3/viewtopic.php?f=9&t=3162 A sample demo for Contact callbacks
http://www.bulletphysics.com/Bullet/phpBB3/viewtopic.php?f=9&t=3359

*/
#include "BulletCollision/CollisionDispatch/btGhostObject.h"

namespace oge
{
    /**  
     * The Bullet 'TriggerComponent' type component
     *
     * @author Steven 'lazalong' GAY
     */
    class OGE_PHYSICS_API TriggerComponentBullet : public TriggerComponent
    {
        friend class TriggerComponentTemplateBullet;

    private:
        Vector3    mPosition;
        Quaternion mOrientation;
        Vector3    mDimensions;
        Vector3    mScale;

        btGhostObject* mBody; // ! same name as in PhysicsComponent to keep code similar
        btCollisionShape* mShape; // remove if managed by the PhysicsMgr

        CollisionData mCollisionData;

		short mCollisionGroup;
        short mCollisionMask;

    public:
        virtual ~TriggerComponentBullet()
        {}

        /// (Re)set the default values
        virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
        virtual void _destroy(SceneManager* sceneManager);
        virtual void _activate(bool activate, SceneManager* sceneManager);
        virtual UpdateRate _getUpdateRate() const;
        virtual void _update(double deltaTime);

        /** Implement Serialisable methods
         * @note Actually the serialisation methods read/write the whole object.
         * @todo Only the differences from the object template should be serialised.
         */
        virtual void write(Serialiser* serialiser);
        virtual void read(Serialiser* serialiser);
        virtual bool fixup();

        // --- Specific methods to the Trigger Family ---

        void setPosition(const Vector3& position);
        void setScale(const Vector3& scale);
        /**
         * @note Must be called on components that their owner objects has either
         *        Graphics or Physics components.
         */
        void setDimensions(const Vector3& dimensions);
        void setOrientation(const Quaternion& orientation);
        /**
         * @note The object is first reset to an upright(?) position
         *        and then yaw (around y axis), pitch (x) and roll (z)
         *        are applied in this order.
         */
        void setOrientationEuler(const Vector3& orientation);
        void setPositionOrientation(const Vector3& position, const Quaternion& orientation);

        /** TODO
        void moveRelative(Real x, Real y, Real z);
        void yaw(const Radian& angle);
        void pitch(const Radian& angle);
        void roll(const Radian& angle);
        void resetOrientation();
        ... see: Ogre::Camera */

        const Vector3& getPosition() const; // { return mPosition; }
        inline const Vector3& getScale() const { return mScale; };
        /**
         * @note Must be called on components that their owner objects has either
         *        Graphics or Physics components.
         */
        const Vector3& getDimensions() const;
        const Quaternion& getOrientation() const; // { return mOrientation; };
        Vector3 getEulerOrientation() const;

        virtual void _processMessage(const Message& message);

    protected:
        inline TriggerComponentBullet(const ComponentType& type, const ComponentType& family)
            : TriggerComponent(type, family),
			mCollisionGroup(0), mCollisionMask(0)
        {}

        void _setPositionByMessage(const Message& message);
        void _setScaleByMessage(const Message& message);
        void _setDimensionsByMessage(const Message& message);
        void _setOrientationByMessage(const Message& message);
        void _setEulerOrientationByMessage(const Message& message);
        void _setPositionOrientationByMessage(const Message& message);

    private:
    };

    /**
     * Builder that is used to create Bullet based Trigger Components based on a template.
     *
     * @author Steven 'lazalong' GAY
     */
    class OGE_PHYSICS_API TriggerComponentTemplateBullet : public ComponentTemplate
    {
    public:
        TriggerComponentTemplateBullet()
        {
            setFamily("Trigger"); // NOTE: only in the base component of a family!
            setType("TriggerComponent");
        }
        /// Create the component
        virtual Component* createComponent()
        {
            return new TriggerComponentBullet( getType(), getFamily() );  
        }
    };
} // namespace
#endif
