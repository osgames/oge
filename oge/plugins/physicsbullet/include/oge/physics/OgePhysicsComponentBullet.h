/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
is free software; you can redistribute it and/or modify it under the terms
of the GNU Lesser General Public License (LGPL) as published by the
Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
a written agreement from the 'OGE Team'. The exact wording of this
license can be obtained from the 'OGE Team'. In essence this
OGE Unrestricted License state that the GNU Lesser General Public License
applies except that the software is distributed with no limitation or
requirements to publish or give back to the OGE Team changes made
to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_PHYSICSCOMPONENTBULLET_H__
#define __OGE_PHYSICSCOMPONENTBULLET_H__

#include "oge/physics/OgePhysicsPrerequisitesBullet.h"
#include "oge/engine/OgeEngineManager.h"
#include "oge/object/OgeComponent.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"

#include "oge/physics/OgePhysicsManagerBullet.h"
#include "oge/physics/OgePhysicsComponent.h"
#include "oge/object/OgeObjectManager.h"
#include "oge/network/OgeNetworkManager.h"

namespace oge
{
	/**  
	* The Bullet 'PhysicsComponent' type component
	*
	* @author Steven 'lazalong' GAY
	*/
	class OGE_PHYSICS_API PhysicsComponentBullet : public PhysicsComponentDefault
	{
		friend class PhysicsComponentTemplateBullet;

		// TODO clean
		// based on http://www.bulletphysics.com/mediawiki-1.5.8/index.php?title=MotionStates
		// and bullet/linearmath/btDefaultMotionState.h
		class OgeMotionState : public btMotionState
		{
		public:
			OgeMotionState(ObjectId id, void* ptr, const btTransform& startTrans = btTransform::getIdentity()
				,const btTransform& centerOfMassOffset = btTransform::getIdentity())
				: m_graphicsWorldTrans(startTrans),
				m_centerOfMassOffset(centerOfMassOffset),
				m_startWorldTrans(startTrans)
			{
				mId = id;
				mPtr = ptr;
			}
			virtual ~OgeMotionState() {}

			virtual void getWorldTransform(btTransform& centerOfMassWorldTrans ) const
			{
				centerOfMassWorldTrans = m_centerOfMassOffset.inverse()
					* m_graphicsWorldTrans ;
			}
			virtual void setWorldTransform(const btTransform& centerOfMassWorldTrans)
			{
				if (m_graphicsWorldTrans == centerOfMassWorldTrans * m_centerOfMassOffset) 
					return;

				m_graphicsWorldTrans = centerOfMassWorldTrans * m_centerOfMassOffset;
				// moved all the message sending to _postTick() so we just send one message
				// per tick instead of from each internal bullet tick.

				//btQuaternion rot = m_graphicsWorldTrans.getRotation();
				//btVector3 pos = m_graphicsWorldTrans.getOrigin();
			}
		protected:
			btTransform m_graphicsWorldTrans;
			btTransform	m_centerOfMassOffset;
			btTransform m_startWorldTrans;

			// object id for the physics component
			ObjectId mId;

			// pointer to the physics component
			void* mPtr;
		};


	private:
		btRigidBody* mBody;
		btCollisionShape* mShape; // remove if managed by the PhyscsMgr
		
		double mLastPreTickTime;

		/// constant force per second
		Vector3 mConstantForce;

		// constant angular force per second
		Vector3 mConstantAngularForce;

		// TODO Make a specific class for the terrain OgePhysicsTerrainComponentBullet.h ?
		unsigned int* mRawHeightfieldData;

		CollisionData mCollisionData;

	public:
		virtual ~PhysicsComponentBullet()
		{}

		/// (Re)set the default values
		virtual bool _reset(const MessageList& params, SceneManager* sceneManager);
		virtual void _destroy(SceneManager* sceneManager);
		virtual void _activate(bool activate, SceneManager* sceneManager);
		virtual UpdateRate _getUpdateRate() const;
		virtual void _preTick(double currentTime);
		virtual void _postTick(double currentTime);
		virtual void _update(double deltaTime);

		virtual bool fixup();

        const Vector3& getAngularVelocity() const;
		const Vector3& getDimensions() const;
		const Vector3& getPosition() const;
		const Quaternion& getOrientation() const;
		Vector3 getEulerOrientation() const;
        const Vector3& getVelocity() const;

		btRigidBody* getRigidBody() { return mBody; }

		void preTickCallBack(double deltaTime);

		virtual void _processMessage(const Message& message);

		void tickCallBack(double deltaTime);

	protected:
        inline PhysicsComponentBullet(const ComponentType& type, const ComponentType& family) :
                PhysicsComponentDefault(type, family),
                mBody(0),
                mLastPreTickTime(0.0),
                mConstantForce(Vector3::ZERO),
                mConstantAngularForce(Vector3::ZERO)
        {
        }

		void _applyForceByMessage(const Message& message);
		void _applyTorqueByMessage(const Message& message);
        void _setAngularVelocityByMessage(const Message& message);
        void _setVelocityByMessage(const Message& message);
		void _setPositionByMessage(const Message& message);
		void _setScaleByMessage(const Message& message);
		void _setDragByMessage(const Message& message);
		void _setDimensionsByMessage(const Message& message);
		void _setOrientationByMessage(const Message& message);
		void _setEulerOrientationByMessage(const Message& message);
		void _setPositionOrientationByMessage(const Message& message);

	private:
		/**
		* @returns a btHeightfieldTerrainShape in reality
		*
		* @param filename
		* @param dimensions used to scale the image to the desired dimensions (default 1000,100,1000)
		* @param upAxis (default 1 == Y-axis as "up")
		* @param flipQuadEdges (default false)
		*
		* @note Don't forget to delete the shape.
		* @note For now only square terrain are supported
		*
		* @note (Bullet) The local origin of the heightfield is assumed 
		*  to be the exact center (as determined by width and length and height,
		*  with each axis multiplied by the localScaling)!
		*
		* @see Bullet TerrainDemo.cpp TerrainDemo::resetPhysics()
		*/
		btCollisionShape* createTerrainShape(const String& filename, 
			Vector3 dimensions = Vector3(1000,100,1000), int upAxis = 1, bool flipQuadEdges = false);

		/// TODO WIP Code taken from my IGE test code
		btCollisionShape* createMeshShape();
	};

	/**
	* Builder that is used to create Bullet based Physics Components based on a template.
	*
	* @author Steven 'lazalong' GAY
	*/
	class OGE_PHYSICS_API PhysicsComponentTemplateBullet : public ComponentTemplate
	{
	public:
		PhysicsComponentTemplateBullet()
		{
			setFamily("Physics"); // NOTE: only in the base component of a family!
			setType("Location");
		}
		/// Create the component
		virtual Component* createComponent()
		{
			return new PhysicsComponentBullet( getType(), getFamily() );  
		}
	};
} // namespace
#endif
