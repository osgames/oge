/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_PHYSICSMANAGERBULLET_H__
#define __OGE_PHYSICSMANAGERBULLET_H__

#include "oge/physics/OgePhysicsPrerequisitesBullet.h"
#include "oge/resource/OgeResourceControllerOgeImpl.h"
#include "oge/physics/OgePhysicsManager.h"
#include "oge/OgeSingleton.h"
#include "oge/physics/OgePhysicsDebugDrawerBullet.h"

namespace oge
{
    class OGE_PHYSICS_API PhysicsManagerBullet : public PhysicsManager, 
        public Singleton<PhysicsManagerBullet>
    {
    private:
        /**
         * The world simulation, provide a high level interface that manages the
         * physics objects and constraints. It also implements the update of all objects each frame.
         * (either a btDiscreteDynamicsWorld or btSoftRigidDynamicsWorld)
         */
        btDynamicsWorld* mDynamicsWorld;

        btBroadphaseInterface* mBroadphase;
        btCollisionDispatcher* mDispatcher;
        btConstraintSolver* mSolver;
        btDefaultCollisionConfiguration* mCollisionConfiguration;

        bool mIsSimulating; // TODO Config

		// The value of currentTime the last time tick() was called
        double mLastTime;
		double mDeltaTime;

		// The last physics step time based on fixedTimeStep
		double mLastPhysicsStepTime;

        DebugDrawer* mDebugDrawer;
		float mBulletLocalTime;
		int mMaxBulletSubSteps;
		int mNumBulletSubSteps;
		float mBulletFixedTimeStep;

    public:

        static PhysicsManagerBullet* createSingleton();
        static void destroySingleton();
        static PhysicsManagerBullet* getSingletonPtr() { return mSingleton; }
        static PhysicsManagerBullet& getSingleton()
        {
            assert(mSingleton);
            return *mSingleton;
        }

		/**
		 * Get the last physics step time based on fixedTimeStep
		 * @return the last physics step time
		 */
		double getLastPhysicsStepTime() { return mLastPhysicsStepTime; }

		/**
		 * Get the last currentTime when tick() was run
		 * @return the most recent currentTime value
		 */
		double getLastTickTime() { return mLastTime; }

        /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        const String getLibrariesVersion();

		/**
		 * Return the delta time for tick
		 * @return the delta time for tick
		 */
		double getDeltaTime() { return mDeltaTime; }

		/**
		 * Return the number of substeps bullet physics will perform this tick
		 * @return the number of substeps bullet physics will run this tick
		 */
		int getNumBulletSubSteps() { return mNumBulletSubSteps; }

        void addRigidBody(btRigidBody* body, const short collisionGroup = 0, const short collisonMask = 0 );
        void addCollisionObject(btCollisionObject* body, const short collisionGroup = 0, const short collisonMask = 0 );
        void removeCollisionObject(btCollisionObject* body);
		void removeRigidBody(btRigidBody* body);

        void setDebugDrawerMode(DebugDrawer::DebugMode mode);

		DebugDrawer::DebugMode getDebugDrawerMode();

        // --------- Bullet specific methods --------------
        inline btDynamicsWorld* const getWorld() { return mDynamicsWorld; }

		/**
		 * This is called before every bullet internal tick.
		 * We use this to apply constant force when damping is present
		 * @param world The dynamics world
		 * @param timeStep The elapsed time
		 */
		void preTickCallback(btDynamicsWorld *world, btScalar timeStep);

		/**
		 * This is called after every bullet internal tick.
		 * We use this to constrain velocities
		 * @param world The dynamics world
		 * @param timeStep The elapsed time
		 */
		void tickCallback(btDynamicsWorld *world, btScalar timeStep);

    protected:
        // System methods
        bool initialise();
        void shutdown();
		void preTick(double currentTime);
        void tick(double currentTime);

        // Note that the PhysicsManager::createDefaultSceneManager() must NOT be called
        void createDefaultSceneManager();

    private:
        PhysicsManagerBullet();
        ~PhysicsManagerBullet();
    };
}
#endif // __OGE_PHYSICSMANAGERBULLET_H__
