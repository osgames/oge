/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_PHYSICSSCENEMANAGERBULLET_H__
#define __OGE_PHYSICSSCENEMANAGERBULLET_H__

#include "oge/physics/OgePhysicsPrerequisitesBullet.h"
#include "oge/containers/OgeMap.h"
#include "oge/scene/OgeSceneManager.h"
#include "oge/physics/OgePhysicsSceneManager.h"
#include "oge/OgeAsyncValue.h"

namespace oge
{
    /**
     * Default Bullet Physics SceneManager
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_PHYSICS_API PhysicsSceneManagerBullet : public PhysicsSceneManager
    {
        friend class ObjectManager;

    public:
        PhysicsSceneManagerBullet(const String& name);
        virtual ~PhysicsSceneManagerBullet();

        /** Internal method used once to initialise the metadata, must be implemented
         *  @note Also used by factories to know which type of sm it will create.
         */
        virtual void initMetaData() const;
        virtual void init();

        virtual void clearScene();
        virtual void update(double deltaTime);

		/**
		 * This tick callback is apply constant force
		 * @param deltaTime the elapsed time
		 */
		void preTickCallBack(double deltaTime);

		/**
		 * This tick callback is used to cap max velocities
		 * for rigid bodies
		 * @param deltaTime the elapsed time
		 */
		void tickCallBack(double deltaTime);

		void logProfileInfo(double currentTime);

    protected:
        bool processMessage(const Message& message);
    };

    //-------------------------------------------------------------------------

    /**
     * Factory instanciating the scene manager
     * @author Steven 'lazalong' Gay
     */
    class OGE_PHYSICS_API PhysicsSceneManagerFactoryBullet : public SceneManagerFactory
    {
    public:
        PhysicsSceneManagerFactoryBullet();
        virtual ~PhysicsSceneManagerFactoryBullet();

        virtual SceneManager* createInstance(const String& name);
        virtual bool checkAvailability();
    };

}
#endif
