 Physics Bullet Plugin
=======================

Documentation & Forum
---------------------

- http://www.bulletphysics.com/mediawiki-1.5.8/index.php?title=Documentation
- http://www.bulletphysics.com/Bullet/phpBB3


NOTE:
-----

- The 4 base statics libs are: 
  libbulletmath, libbulletcollision, libbulletdynamics and libbulletsoftbody

- Bullet is always linked as static. The "dll" version are just using the 
  "Multi-threaded DLL Code Generation" settings.
  On Windows we are using the "ReleaseDll" and "DebugDll".
  
- See http://www.bulletphysics.com/mediawiki-1.5.8/index.php?title=Installation
  The Step 5 speak about integrating with ODE: What is the benefit over the bullet collision code?
  

TODO:
-----

- Add timing & profiling measure: see p. 9 of Bullet_User_Manual.pdf (BT_PROFILE)
- Enable to choose to use a single precision or double precision version of the library
- Integrate with our memory allocator, hook up own performance profiler or debug drawer
- Enable to use the collision detection component without the rigid body dynamics component
- Enable to use the rigid body dynamics component without soft body dynamics component

- Study the libbulletmultithreaded (which is optional and not 64-bits safe yet).
  See discussion http://www.bulletphysics.com/Bullet/phpBB3/viewtopic.php?f=9&t=2557
  "Just tried to reproduce your issue with the Demos/ConcaveDemo/ConcavePhysicsDemo, with following modifications:

    * enable define #USE_PARALLEL_DISPATCHER
    * add btCompoundShape
    * add libbulletmultithreaded as link dependency
  and all works fine, using SpuGatheringCollisionDispatcher and Win32ThreadSupport."
  
- Study the "convex decomposition" sub library : libconvexdecomposition.lib
  See http://www.bulletphysics.com/Bullet/phpBB3/viewtopic.php?f=4&t=2655
      http://www.bulletphysics.com/Bullet/phpBB3/viewtopic.php?f=9&t=2462
      
- Change mass during during simulation the body must be removed 
  from the simulation, change mass then added again.

-  A kinematic object is something thats velocity is modified without using forces. A good example is say, an animated character in a game. You wouldn't move it using forces, but by modifying the velocities yourself, based on inputs from the player.
  Having a rigid body as a kinematic object is a nice shortcut (as far as I can tell, only just started using Bullet myself) to hooking into the dynamics and collision world, so that everything responds to the objects movement in a mostly proper manner.

- The collision filter has three methods: using masks, using a Broadphase Filter callback 
  and using a cutom NearCallback.  See the bullet pfd doc.
  The problem being that masks use a... short for bitwise. This limit the number of available
  collisions groups.
  We must enable an easy way to use the callbacks.
   
- Either follow this feature request:  [Feature request] Exclude an object from btIDebugDraw
  http://www.bulletphysics.com/Bullet/phpBB3/viewtopic.php?f=9&t=3540
  Or implement our own debug draw class.
  
  
