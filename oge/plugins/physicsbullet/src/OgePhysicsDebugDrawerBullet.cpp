/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/physics/OgePhysicsDebugDrawerBullet.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/math/OgeVector3.h"

namespace oge
{
    //-------------------------------------------------------------------------
    DebugDrawer::DebugDrawer(btDynamicsWorld* world)
        : mDebugMode(OGE_NoDebug), mWorld(world)
    {
    }
    //-------------------------------------------------------------------------
    DebugDrawer::~DebugDrawer()
    {
    }
    //-------------------------------------------------------------------------
    void DebugDrawer::step()
    {
        if (mDebugMode)
        {
            // -------- contact points ------------
            if (mDebugMode & OGE_ContactPoints)
            {
                int numManifolds = mWorld->getDispatcher()->getNumManifolds();
                btVector3 color(0,0,0);
                for (int i=0;i<numManifolds;i++)
                {
                    btPersistentManifold* contactManifold = mWorld->getDispatcher()->getManifoldByIndexInternal(i);

                    int numContacts = contactManifold->getNumContacts();
                    for (int j=0;j<numContacts;j++)
                    {
                        btManifoldPoint& cp = contactManifold->getContactPoint(j);
                        drawContactPoint(cp.m_positionWorldOnB,cp.m_normalWorldOnB,cp.getDistance(),cp.getLifeTime(),color);
                    }
                }
            }
            // ----------  aabb -----------
            if (mDebugMode & (OGE_Wireframe | OGE_Aabb))
            {
                btCollisionObjectArray m_collisionObjects = mWorld->getCollisionObjectArray();
                for (int i=0;i<m_collisionObjects.size();i++)
                {
                    btCollisionObject* colObj = m_collisionObjects[i];
                    if (mDebugMode & OGE_Wireframe)
                    {
                        btVector3 color(btScalar(1),btScalar(1),btScalar(1));
                        switch(colObj->getActivationState())
                        {
                        case  ACTIVE_TAG:
                            color = btVector3(btScalar(1),btScalar(1),btScalar(1)); break;
                        case ISLAND_SLEEPING:
                            color =  btVector3(btScalar(0),btScalar(1),btScalar(0));break;
                        case WANTS_DEACTIVATION:
                            color = btVector3(btScalar(0),btScalar(1),btScalar(1));break;
                        case DISABLE_DEACTIVATION:
                            color = btVector3(btScalar(1),btScalar(0),btScalar(0));break;
                        case DISABLE_SIMULATION:
                            color = btVector3(btScalar(1),btScalar(1),btScalar(0));break;
                        default:
                        {
                            color = btVector3(btScalar(1),btScalar(0),btScalar(0));
                        }
                    };

                    drawObject(colObj->getWorldTransform(),colObj->getCollisionShape(),color);
                    }

                    if (mDebugMode & OGE_Aabb)
                    {
                        btVector3 minAabb,maxAabb;
                        btVector3 colorvec(1,0,0);
                        colObj->getCollisionShape()->getAabb(colObj->getWorldTransform(), minAabb,maxAabb);
                        drawAabb(minAabb,maxAabb,colorvec);
                    }
                }
            }
            // ------ constraints -----------
            if(mDebugMode & OGE_Constraints)
            {
                for(int i = mWorld->getNumConstraints()-1; i>=0 ;i--)
                {
                    btTypedConstraint* constraint = mWorld->getConstraint(i);
                    drawConstraint(constraint);
                }
            }
        }
    }
    //-------------------------------------------------------------------------
    void DebugDrawer::drawLine(const btVector3& from, const btVector3& to,
        const btVector3& color)
    {
        GraphicsManager::getManager()->drawDebugLine(
            Vector3(from.x(), from.y(), from.z()),
            Vector3(to.x(), to.y(), to.z()),
            Vector3(color.x(), color.y(), color.z()));
    }
    //-------------------------------------------------------------------------
    void DebugDrawer::drawContactPoint(const btVector3& pointOnB, 
        const btVector3& normalOnB, btScalar distance,
        int lifeTime, const btVector3& color)
    {
        GraphicsManager::getManager()->drawDebugContactPoint(
            Vector3(pointOnB.x(), pointOnB.y(), pointOnB.z()),
            Vector3(normalOnB.x(), normalOnB.y(), normalOnB.z()),
            distance, lifeTime,
            Vector3(color.x(), color.y(), color.z()));
    }
    //-------------------------------------------------------------------------
    void DebugDrawer::reportErrorWarning(const char* warningString)
    {
        LOGE(String("DebugDrawer: ") + warningString);
    }
    //-------------------------------------------------------------------------
    void DebugDrawer::draw3dText(const btVector3& location, const char* textString)
    {
        // TODO
    }
    //-------------------------------------------------------------------------
    void DebugDrawer::setDebugMode(int mode)
    {
        mDebugMode = static_cast<DebugMode>(mode); 
        GraphicsManager::getManager()->setDebugLinesMode((mode == 0) ? false : true);
    }
    //-------------------------------------------------------------------------
    void DebugDrawer::drawDebugSphere(btScalar radius, const btTransform& transform, const btVector3& color)
    {
        btVector3 start = transform.getOrigin();

        const btVector3 xoffs = transform.getBasis() * btVector3(radius,0,0);
        const btVector3 yoffs = transform.getBasis() * btVector3(0,radius,0);
        const btVector3 zoffs = transform.getBasis() * btVector3(0,0,radius);

        // XY 
        drawLine(start-xoffs, start+yoffs, color);
        drawLine(start+yoffs, start+xoffs, color);
        drawLine(start+xoffs, start-yoffs, color);
        drawLine(start-yoffs, start-xoffs, color);

        // XZ
        drawLine(start-xoffs, start+zoffs, color);
        drawLine(start+zoffs, start+xoffs, color);
        drawLine(start+xoffs, start-zoffs, color);
        drawLine(start-zoffs, start-xoffs, color);

        // YZ
        drawLine(start-yoffs, start+zoffs, color);
        drawLine(start+zoffs, start+yoffs, color);
        drawLine(start+yoffs, start-zoffs, color);
        drawLine(start-zoffs, start-yoffs, color);
    }
    //-------------------------------------------------------------------------
    void DebugDrawer::drawObject(const btTransform& worldTransform, const btCollisionShape* shape, const btVector3& color)
    {
        // if OGE_ExcludeTerrainShape, don't draw terrain shape
        if ((shape->getShapeType() == TERRAIN_SHAPE_PROXYTYPE) && 
            (mDebugMode & OGE_ExcludeTerrainShape))
        {
            return;
        }

        // Draw a small simplex at the center of the object
        {
            btVector3 start = worldTransform.getOrigin();
            drawLine(start, start+worldTransform.getBasis() * btVector3(1,0,0), btVector3(1,0,0));
            drawLine(start, start+worldTransform.getBasis() * btVector3(0,1,0), btVector3(0,1,0));
            drawLine(start, start+worldTransform.getBasis() * btVector3(0,0,1), btVector3(0,0,1));
        }

        if (shape->getShapeType() == COMPOUND_SHAPE_PROXYTYPE)
        {
            const btCompoundShape* compoundShape = 
                static_cast<const btCompoundShape*>(shape);
            for (int i=compoundShape->getNumChildShapes()-1;i>=0;i--)
            {
                btTransform childTrans = compoundShape->getChildTransform(i);
                const btCollisionShape* colShape = compoundShape->getChildShape(i);
                drawObject(worldTransform*childTrans,colShape,color);
            }
            return;
        } 
        else
        {
            switch (shape->getShapeType())
            {
                case SPHERE_SHAPE_PROXYTYPE:
                {
                    const btSphereShape* sphereShape = static_cast<const btSphereShape*>(shape);
                    btScalar radius = sphereShape->getMargin();//radius doesn't include the margin, so draw with margin

                    drawDebugSphere(radius, worldTransform, color);
                    break;
                }
                case MULTI_SPHERE_SHAPE_PROXYTYPE:
                {
                    const btMultiSphereShape* multiSphereShape = static_cast<const btMultiSphereShape*>(shape);

                    for (int i = multiSphereShape->getSphereCount()-1; i>=0;i--)
                    {
                        btTransform childTransform = worldTransform;
                        childTransform.getOrigin() += multiSphereShape->getSpherePosition(i);
                        drawDebugSphere(multiSphereShape->getSphereRadius(i), childTransform, color);
                    }
                    break;
                }
                case CAPSULE_SHAPE_PROXYTYPE:
                {
                    const btCapsuleShape* capsuleShape = static_cast<const btCapsuleShape*>(shape);

                    btScalar radius = capsuleShape->getRadius();
                    btScalar halfHeight = capsuleShape->getHalfHeight();

                    int upAxis = capsuleShape->getUpAxis();


                    btVector3 capStart(0.f,0.f,0.f);
                    capStart[upAxis] = -halfHeight;

                    btVector3 capEnd(0.f,0.f,0.f);
                    capEnd[upAxis] = halfHeight;

                    // Draw the ends
                    {
                        btTransform childTransform = worldTransform;
                        childTransform.getOrigin() = worldTransform * capStart;
                        drawDebugSphere(radius, childTransform, color);
                    }

                    {
                        btTransform childTransform = worldTransform;
                        childTransform.getOrigin() = worldTransform * capEnd;
                        drawDebugSphere(radius, childTransform, color);
                    }

                    // Draw some additional lines
                    btVector3 start = worldTransform.getOrigin();


                    capStart[(upAxis+1)%3] = radius;
                    capEnd[(upAxis+1)%3] = radius;
                    drawLine(start+worldTransform.getBasis() * capStart,start+worldTransform.getBasis() * capEnd, color);
                    capStart[(upAxis+1)%3] = -radius;
                    capEnd[(upAxis+1)%3] = -radius;
                    drawLine(start+worldTransform.getBasis() * capStart,start+worldTransform.getBasis() * capEnd, color);

                    capStart[(upAxis+1)%3] = 0.f;
                    capEnd[(upAxis+1)%3] = 0.f;

                    capStart[(upAxis+2)%3] = radius;
                    capEnd[(upAxis+2)%3] = radius;
                    drawLine(start+worldTransform.getBasis() * capStart,start+worldTransform.getBasis() * capEnd, color);
                    capStart[(upAxis+2)%3] = -radius;
                    capEnd[(upAxis+2)%3] = -radius;
                    drawLine(start+worldTransform.getBasis() * capStart,start+worldTransform.getBasis() * capEnd, color);

                    break;
                }
                case CONE_SHAPE_PROXYTYPE:
                {
                    const btConeShape* coneShape = static_cast<const btConeShape*>(shape);
                    btScalar radius = coneShape->getRadius();//+coneShape->getMargin();
                    btScalar height = coneShape->getHeight();//+coneShape->getMargin();
                    btVector3 start = worldTransform.getOrigin();

                    int upAxis= coneShape->getConeUpIndex();


                    btVector3	offsetHeight(0,0,0);
                    offsetHeight[upAxis] = height * btScalar(0.5);
                    btVector3	offsetRadius(0,0,0);
                    offsetRadius[(upAxis+1)%3] = radius;
                    btVector3	offset2Radius(0,0,0);
                    offset2Radius[(upAxis+2)%3] = radius;

                    drawLine(start+worldTransform.getBasis() * (offsetHeight),start+worldTransform.getBasis() * (-offsetHeight+offsetRadius),color);
                    drawLine(start+worldTransform.getBasis() * (offsetHeight),start+worldTransform.getBasis() * (-offsetHeight-offsetRadius),color);
                    drawLine(start+worldTransform.getBasis() * (offsetHeight),start+worldTransform.getBasis() * (-offsetHeight+offset2Radius),color);
                    drawLine(start+worldTransform.getBasis() * (offsetHeight),start+worldTransform.getBasis() * (-offsetHeight-offset2Radius),color);

                    break;
                }
                case CYLINDER_SHAPE_PROXYTYPE:
                {
                    const btCylinderShape* cylinder = static_cast<const btCylinderShape*>(shape);
                    int upAxis = cylinder->getUpAxis();
                    btScalar radius = cylinder->getRadius();
                    btScalar halfHeight = cylinder->getHalfExtentsWithMargin()[upAxis];
                    btVector3 start = worldTransform.getOrigin();
                    btVector3	offsetHeight(0,0,0);
                    offsetHeight[upAxis] = halfHeight;
                    btVector3	offsetRadius(0,0,0);
                    offsetRadius[(upAxis+1)%3] = radius;
                    drawLine(start+worldTransform.getBasis() * (offsetHeight+offsetRadius),
                    start+worldTransform.getBasis() * (-offsetHeight+offsetRadius),color);
                    drawLine(start+worldTransform.getBasis() * (offsetHeight-offsetRadius),
                    start+worldTransform.getBasis() * (-offsetHeight-offsetRadius),color);
                    break;
                }
                case STATIC_PLANE_PROXYTYPE:
                {
                    const btStaticPlaneShape* staticPlaneShape = static_cast<const btStaticPlaneShape*>(shape);
                    btScalar planeConst = staticPlaneShape->getPlaneConstant();
                    const btVector3& planeNormal = staticPlaneShape->getPlaneNormal();
                    btVector3 planeOrigin = planeNormal * planeConst;
                    btVector3 vec0,vec1;
                    btPlaneSpace1(planeNormal,vec0,vec1);
                    btScalar vecLen = 100.f;
                    btVector3 pt0 = planeOrigin + vec0*vecLen;
                    btVector3 pt1 = planeOrigin - vec0*vecLen;
                    btVector3 pt2 = planeOrigin + vec1*vecLen;
                    btVector3 pt3 = planeOrigin - vec1*vecLen;
                    drawLine(worldTransform*pt0,worldTransform*pt1,color);
                    drawLine(worldTransform*pt2,worldTransform*pt3,color);
                    break;
                }
                default:
                {
                    if (shape->isConcave())
                    {
                        btConcaveShape* concaveMesh = (btConcaveShape*) shape;

                        ///@todo pass camera, for some culling? no -> we are not a graphics lib
                        btVector3 aabbMax(btScalar(1e30),btScalar(1e30),btScalar(1e30));
                        btVector3 aabbMin(btScalar(-1e30),btScalar(-1e30),btScalar(-1e30));

                        ProcessTriangleCallback drawCallback(this,worldTransform,color);
                        concaveMesh->processAllTriangles(&drawCallback,aabbMin,aabbMax);
                    }

                    if (shape->getShapeType() == CONVEX_TRIANGLEMESH_SHAPE_PROXYTYPE)
                    {
                        btConvexTriangleMeshShape* convexMesh = (btConvexTriangleMeshShape*) shape;
                        //todo: pass camera for some culling			
                        btVector3 aabbMax(btScalar(1e30),btScalar(1e30),btScalar(1e30));
                        btVector3 aabbMin(btScalar(-1e30),btScalar(-1e30),btScalar(-1e30));
                        //DebugDrawcallback drawCallback;
                        ProcessTriangleCallback drawCallback(this,worldTransform,color);
                        convexMesh->getMeshInterface()->InternalProcessAllTriangles(&drawCallback,aabbMin,aabbMax);
                    }

                    /// for polyhedral shapes
                    if (shape->isPolyhedral())
                    {
                        btPolyhedralConvexShape* polyshape = (btPolyhedralConvexShape*) shape;

                        int i;
                        for (i=0;i<polyshape->getNumEdges();i++)
                        {
                            btVector3 a,b;
                            polyshape->getEdge(i,a,b);
                            btVector3 wa = worldTransform * a;
                            btVector3 wb = worldTransform * b;
                            drawLine(wa,wb,color);
                        }
                    }
                }
            } // end of switch
        }
    }
    //-------------------------------------------------------------------------
    void DebugDrawer::drawConstraint(btTypedConstraint* constraint)
    {
        bool drawFrames = (mDebugMode & OGE_Constraints) != 0;
        bool drawLimits = (mDebugMode & OGE_ConstraintLimits) != 0;
        btScalar dbgDrawSize = constraint->getDbgDrawSize();
        if (dbgDrawSize <= btScalar(0.f))
        {
            return;
        }

        switch (constraint->getConstraintType())
        {
            case POINT2POINT_CONSTRAINT_TYPE:
            {
                btPoint2PointConstraint* p2pC = (btPoint2PointConstraint*)constraint;
                btTransform tr;
                tr.setIdentity();
                btVector3 pivot = p2pC->getPivotInA();
                pivot = p2pC->getRigidBodyA().getCenterOfMassTransform() * pivot;
                tr.setOrigin(pivot);
                drawTransform(tr, dbgDrawSize);
                // that ideally should draw the same frame
                pivot = p2pC->getPivotInB();
                pivot = p2pC->getRigidBodyB().getCenterOfMassTransform() * pivot;
                tr.setOrigin(pivot);
                if (drawFrames)
                    drawTransform(tr, dbgDrawSize);
                break;
            }
            case HINGE_CONSTRAINT_TYPE:
            {
                btHingeConstraint* pHinge = (btHingeConstraint*)constraint;
                btTransform tr = pHinge->getRigidBodyA().getCenterOfMassTransform() * pHinge->getAFrame();
                if (drawFrames) drawTransform(tr, dbgDrawSize);
                tr = pHinge->getRigidBodyB().getCenterOfMassTransform() * pHinge->getBFrame();
                if (drawFrames) drawTransform(tr, dbgDrawSize);
                btScalar minAng = pHinge->getLowerLimit();
                btScalar maxAng = pHinge->getUpperLimit();
                if (minAng == maxAng)
                {
                    break;
                }
                bool drawSect = true;
                if (minAng > maxAng)
                {
                    minAng = btScalar(0.f);
                    maxAng = SIMD_2_PI;
                    drawSect = false;
                }
                if (drawLimits)
                {
                    btVector3& center = tr.getOrigin();
                    btVector3 normal = tr.getBasis().getColumn(2);
                    btVector3 axis = tr.getBasis().getColumn(0);
                    drawArc(center, normal, axis, dbgDrawSize, dbgDrawSize, minAng, maxAng, btVector3(0,0,0), drawSect);
                }
                break;
            }
            case CONETWIST_CONSTRAINT_TYPE:
            {
                btConeTwistConstraint* pCT = (btConeTwistConstraint*)constraint;
                btTransform tr = pCT->getRigidBodyA().getCenterOfMassTransform() * pCT->getAFrame();
                if (drawFrames) drawTransform(tr, dbgDrawSize);
                tr = pCT->getRigidBodyB().getCenterOfMassTransform() * pCT->getBFrame();
                if (drawFrames) drawTransform(tr, dbgDrawSize);
                if (drawLimits)
                {
                    //const btScalar length = btScalar(5);
                    const btScalar length = dbgDrawSize;
                    static int nSegments = 8*4;
                    btScalar fAngleInRadians = btScalar(2.*3.1415926) * (btScalar)(nSegments-1)/btScalar(nSegments);
                    btVector3 pPrev = pCT->GetPointForAngle(fAngleInRadians, length);
                    pPrev = tr * pPrev;
                    for (int i=0; i<nSegments; i++)
                    {
                        fAngleInRadians = btScalar(2.*3.1415926) * (btScalar)i/btScalar(nSegments);
                        btVector3 pCur = pCT->GetPointForAngle(fAngleInRadians, length);
                        pCur = tr * pCur;
                        drawLine(pPrev, pCur, btVector3(0,0,0));

                        if (i%(nSegments/8) == 0)
                        drawLine(tr.getOrigin(), pCur, btVector3(0,0,0));

                        pPrev = pCur;
                    }
                    btScalar tws = pCT->getTwistSpan();
                    btScalar twa = pCT->getTwistAngle();
                    bool useFrameB = (pCT->getRigidBodyB().getInvMass() > btScalar(0.f));
                    if (useFrameB)
                    {
                        tr = pCT->getRigidBodyB().getCenterOfMassTransform() * pCT->getBFrame();
                    }
                    else
                    {
                        tr = pCT->getRigidBodyA().getCenterOfMassTransform() * pCT->getAFrame();
                    }
                    btVector3 pivot = tr.getOrigin();
                    btVector3 normal = tr.getBasis().getColumn(0);
                    btVector3 axis1 = tr.getBasis().getColumn(1);
                    drawArc(pivot, normal, axis1, dbgDrawSize, dbgDrawSize, -twa-tws, -twa+tws, btVector3(0,0,0), true);
                }
                break;
            }
            case D6_CONSTRAINT_TYPE:
            {
                btGeneric6DofConstraint* p6DOF = (btGeneric6DofConstraint*)constraint;
                btTransform tr = p6DOF->getCalculatedTransformA();
                if (drawFrames) drawTransform(tr, dbgDrawSize);
                tr = p6DOF->getCalculatedTransformB();
                if (drawFrames) drawTransform(tr, dbgDrawSize);
                if (drawLimits)
                {
                    tr = p6DOF->getCalculatedTransformA();
                    const btVector3& center = p6DOF->getCalculatedTransformB().getOrigin();
                    btVector3 up = tr.getBasis().getColumn(2);
                    btVector3 axis = tr.getBasis().getColumn(0);
                    btScalar minTh = p6DOF->getRotationalLimitMotor(1)->m_loLimit;
                    btScalar maxTh = p6DOF->getRotationalLimitMotor(1)->m_hiLimit;
                    btScalar minPs = p6DOF->getRotationalLimitMotor(2)->m_loLimit;
                    btScalar maxPs = p6DOF->getRotationalLimitMotor(2)->m_hiLimit;
                    drawSpherePatch(center, up, axis, dbgDrawSize * btScalar(.9f), minTh, maxTh, minPs, maxPs, btVector3(0,0,0));
                    axis = tr.getBasis().getColumn(1);
                    btScalar ay = p6DOF->getAngle(1);
                    btScalar az = p6DOF->getAngle(2);
                    btScalar cy = btCos(ay);
                    btScalar sy = btSin(ay);
                    btScalar cz = btCos(az);
                    btScalar sz = btSin(az);
                    btVector3 ref;
                    ref[0] = cy*cz*axis[0] + cy*sz*axis[1] - sy*axis[2];
                    ref[1] = -sz*axis[0] + cz*axis[1];
                    ref[2] = cz*sy*axis[0] + sz*sy*axis[1] + cy*axis[2];
                    tr = p6DOF->getCalculatedTransformB();
                    btVector3 normal = -tr.getBasis().getColumn(0);
                    btScalar minFi = p6DOF->getRotationalLimitMotor(0)->m_loLimit;
                    btScalar maxFi = p6DOF->getRotationalLimitMotor(0)->m_hiLimit;
                    if (minFi > maxFi)
                    {
                        drawArc(center, normal, ref, dbgDrawSize, dbgDrawSize, -SIMD_PI, SIMD_PI, btVector3(0,0,0), false);
                    }
                    else if (minFi < maxFi)
                    {
                        drawArc(center, normal, ref, dbgDrawSize, dbgDrawSize, minFi, maxFi, btVector3(0,0,0), true);
                    }
                    tr = p6DOF->getCalculatedTransformA();
                    btVector3 bbMin = p6DOF->getTranslationalLimitMotor()->m_lowerLimit;
                    btVector3 bbMax = p6DOF->getTranslationalLimitMotor()->m_upperLimit;
                    drawBox(bbMin, bbMax, tr, btVector3(0,0,0));
                }
                break;
            }
            case SLIDER_CONSTRAINT_TYPE:
            {
                btSliderConstraint* pSlider = (btSliderConstraint*)constraint;
                btTransform tr = pSlider->getCalculatedTransformA();
                if (drawFrames) drawTransform(tr, dbgDrawSize);
                tr = pSlider->getCalculatedTransformB();
                if (drawFrames) drawTransform(tr, dbgDrawSize);
                if (drawLimits)
                {
                    btTransform tr = pSlider->getCalculatedTransformA();
                    btVector3 li_min = tr * btVector3(pSlider->getLowerLinLimit(), 0.f, 0.f);
                    btVector3 li_max = tr * btVector3(pSlider->getUpperLinLimit(), 0.f, 0.f);
                    drawLine(li_min, li_max, btVector3(0, 0, 0));
                    btVector3 normal = tr.getBasis().getColumn(0);
                    btVector3 axis = tr.getBasis().getColumn(1);
                    btScalar a_min = pSlider->getLowerAngLimit();
                    btScalar a_max = pSlider->getUpperAngLimit();
                    const btVector3& center = pSlider->getCalculatedTransformB().getOrigin();
                    drawArc(center, normal, axis, dbgDrawSize, dbgDrawSize, a_min, a_max, btVector3(0,0,0), true);
                }
                break;
            }
            default :
                break;
        }
    }
    //-------------------------------------------------------------------------
}
