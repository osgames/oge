/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/physics/OgePhysicsComponentBullet.h"
#include "oge/physics/OgePhysicsManagerBullet.h"
#include "oge/serialisation/OgeSerialiser.h"
#include "oge/object/OgeObject.h"
#include "oge/object/OgeObjectManager.h"
#include "oge/math/OgeMath.h"
#include "oge/graphics/OgeGraphicsComponent.h"
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/message/OgeMessageList.h"
#include "oge/game/OgeGameClock.h"

#include "BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h"

#include "oge/graphics/OgeGraphicsManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    bool PhysicsComponentBullet::_reset(const MessageList& params, SceneManager* sceneManager)
    {
        mRawHeightfieldData = 0;
		mBody = 0;

        // -------- INIT_PHYSICS_SHAPE -----------------
        const String *type;
        if (params.find( ObjectMessage::INIT_PHYSICS_SHAPE, type ))
        {
            // TODO Use only the enum (of course this means either a numerical value in the .xml or we must convert it when we load it
            if (*type == "Box")
                _setShapeType( PhysicsComponent::OGE_BOX );
            else if(*type == "Sphere")
                _setShapeType( PhysicsComponent::OGE_SPHERE );
            else if(*type == "Convex")
                _setShapeType( PhysicsComponent::OGE_CONVEX );
            else if(*type == "Cylinder")
                _setShapeType( PhysicsComponent::OGE_CYLINDER );
            else if(*type == "Cone")
                _setShapeType( PhysicsComponent::OGE_CONE );
            else if(*type == "Capsule")
                _setShapeType( PhysicsComponent::OGE_CAPSULE );
            else if(*type == "Compound")
                _setShapeType( PhysicsComponent::OGE_COMPOUND );
            else if(*type == "Mesh")
                _setShapeType( PhysicsComponent::OGE_MESH );
            else if(*type == "Terrain") // TODO review
                _setShapeType( PhysicsComponent::OGE_TERRAIN_HEIGHTFIELD );
            else
            {   assert(0);  }   // TODO
        }
        else
        {
            LOGW( "INIT_PHYSICS_SHAPE message could not be found or its parameters are invalid. "
                  "while resetting a '" + getType() + "' component! Owner object Id: " + getObjectId() );

            _setShapeType( PhysicsComponent::OGE_BOX ); // NEXT: Or return false?
        }

        // ------ Create the body and add it to the world ----
        const Vector3 *dimensions;
        if (!params.find( ObjectMessage::SET_DIMENSIONS, dimensions ))
        {
            LOGW( "SET_DIMENSIONS message could not be found or its parameters are invalid "
                  "while resetting a '" + getType() + "' component! Owner object Id: " + getObjectId() );
            return false;
        }
        // Create the collision shape
        // TODO move this in the PhysiscManager and find a way to reuse shapes
        switch (mShapeType)
        {
            case PhysicsComponent::OGE_SPHERE:
                mShape = new btSphereShape(dimensions->x); // radius
                break;
            case PhysicsComponent::OGE_CYLINDER:
                // Aligned to the Y-axis TODO see btCylinderShapeX and Z
                mShape = new btCylinderShape(btVector3(dimensions->x, dimensions->y, dimensions->z)); // half extends
                break;
            case PhysicsComponent::OGE_CONE:
                // Aligned to the Y-axis TODO see btConeShapeX and Z
                mShape = new btConeShape( dimensions->x, dimensions->y ); // radius, height
                break;
            case PhysicsComponent::OGE_CAPSULE:
                // Aligned to the Y-axis TODO see btCapsuleShapeX and Z
                mShape = new btCapsuleShape( dimensions->x, dimensions->y ); // radius, height 
                break;
            case PhysicsComponent::OGE_MESH:
        		mShape = createMeshShape();
                break;
//            case PhysicsComponent::OGE_PLANE:
// TODO        		mShape = new btPlaneShape( ... );  or btStaticPlaneShape() ?
//                break;
            case PhysicsComponent::OGE_CONVEX:
// TODO        		mShape = new btConvexHullShape( ... );
                break;
            case PhysicsComponent::OGE_COMPOUND:
// TODO        		mShape = new btCompoundShape( 
                break;
            case PhysicsComponent::OGE_TERRAIN_HEIGHTFIELD:
                const String *terrain;
#pragma OGE_WARN("Add a special INIT msg for terrains.")
                if (!params.find( ObjectMessage::INIT_GRAPHICS_MESH, terrain ))
                {
                    LOGW( "INIT_GRAPHICS_MESH message could not be found or its parameters are invalid "
                          "while resetting a '" + getType() + "' component! Owner object Id: " + getObjectId() );
                    return false;
                }

                mShape = createTerrainShape(*terrain, *dimensions);
                break;

            default:
                LOGW("No shape was defined creating a default box shape!");
            case PhysicsComponent::OGE_BOX:
                mShape = new btBoxShape(btVector3(dimensions->x, dimensions->y, dimensions->z)); // 5,10,5)); // TODO boxHalfExtents
                break;
        }

        // -----------------  SET_PHYSICS_MASS ------------------
        // See note about mass below
        float mass = 0.0f; // == Static object by default !
        params.find( ObjectMessage::SET_PHYSICS_MASS, mass );

        // ------------------ SET_COLLISION_GROUP & MASK --------
        short collisionGroup = 0;
        short collisionMask = 0;
        params.find( ObjectMessage::SET_COLLISION_GROUP, collisionGroup );
        params.find( ObjectMessage::SET_COLLISION_MASK, collisionMask );

        // TODO test if static object mass must be zero !!

        // NOTE !!! If you want to change the mass during simulation
        //   the body must be removed from the simulation, change mass
        //   then added again.
        btVector3 localInertia(0,0,0);
        if (mass != 0.0f) // if 0 then non-dynamic !
            mShape->calculateLocalInertia( btScalar(mass), localInertia );

        btTransform initTransform;
        initTransform.setIdentity();

        Vector3 pos;
		Quaternion orientation;
		if (params.find( ObjectMessage::SET_ORIENTATION, orientation )) {
			initTransform.setRotation( btQuaternion(orientation.x, orientation.y, orientation.z, orientation.w) );
		}

        if (params.find( ObjectMessage::SET_POSITION, pos ))
            initTransform.setOrigin( btVector3(pos.x, pos.y, pos.z) );
        else
            initTransform.setOrigin( btVector3(0,0,0) );

        // using motionstate is recommended, it provides interpolation capabilities,
        // and only synchronizes 'active' objects
        // btDefaultMotionState* motionState = new btDefaultMotionState(groundTransform);
        btMotionState* motionState = new OgeMotionState(getObjectId(), this, initTransform );

        // At last create the physical body
        mBody = new btRigidBody(btScalar(mass), motionState, mShape, localInertia);

        mCollisionData.mObjectId = getObjectId();
        mCollisionData.mType = "Test"; // @TODO
        mBody->setUserPointer(&mCollisionData);

		bool triggerOnly = false;
		if (params.find( ObjectMessage::SET_PHYSICS_TRIGGER_ONLY, triggerOnly )) {
			if(triggerOnly) {
				mBody->setCollisionFlags(mBody->getCollisionFlags() |
					btCollisionObject::CF_NO_CONTACT_RESPONSE);
			}
		}

		// disable deactivation for dynamic objects - like players
		// bullet has this feature where it will put objects to "sleep" if no
		// forces are acting on them and they aren't collided with anything after
		// a specific period of time to save CPU cycles.  if a player object goes 
		// to sleep then it will only wake on a collision and otherwise the 
		// player will be frozen.
		bool dynamic = true;
		params.find( ObjectMessage::SET_PHYSICS_DYNAMIC, dynamic );
		if(dynamic) {
			mBody->setActivationState(DISABLE_DEACTIVATION);
		}

        // add it to the world
        PhysicsManagerBullet::getSingletonPtr()->addRigidBody( mBody, collisionGroup, collisionMask );

        // ------------ All others messages types --------------------
        for (MessageList::ConstIter it = params.list.begin(); it != params.list.end(); ++it) {
            _processMessage( (*it) );
        }

        return true;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_destroy(SceneManager* sceneManager)
    {
        if (mBody && mBody->getMotionState())
            delete mBody->getMotionState();

        PhysicsManagerBullet::getSingletonPtr()->removeCollisionObject(mBody);

        delete mBody; mBody = 0;
        delete mShape; mShape = 0;

        delete mRawHeightfieldData; mRawHeightfieldData = 0;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_activate(bool activate, SceneManager* sceneManager)
    {
		// remove it just in case it has already been added somehow
		PhysicsManagerBullet::getSingletonPtr()->removeRigidBody(mBody);
		
		if(activate) {
			// add it to the collision/dynamics world
			PhysicsManagerBullet::getSingletonPtr()->addRigidBody( mBody, mCollisionGroup, mCollisionMask );
			mBody->setActivationState(DISABLE_DEACTIVATION);
		}
		else {
			mBody->setActivationState(DISABLE_SIMULATION);
		}
    }
    //-------------------------------------------------------------------------
    Component::UpdateRate PhysicsComponentBullet::_getUpdateRate() const
    {
		return Component::EVERY_TICK;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_update(double deltaTime)
    {
    }
    //-------------------------------------------------------------------------
    bool PhysicsComponentBullet::fixup()
    {
        // no fixup to do
        return true;
    }
    //-------------------------------------------------------------------------
    const Vector3& PhysicsComponentBullet::getDimensions() const
    {
        return mDimensions;
    }
    //-------------------------------------------------------------------------
    Vector3 PhysicsComponentBullet::getEulerOrientation() const
    {
#pragma OGE_WARN("TODO: This code perhaps works in Ogre. Functions should be added to OGE")
    //    Matrix3 rot;
    //    mOrientation.ToRotationMatrix(rot);
    //    Radian yAngle, pAngle, rAngle;
    //    rot.ToEulerAnglesYXZ(yAngle, pAngle, rAngle);
    //    return Vector3(yAngle.valueDegrees(), pAngle.valueDegrees(), rAngle.valueDegrees());
          return Vector3();
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_applyForceByMessage(const Message& message)
    {
        const Vector3 *force = FastAnyCast(Vector3, &message.getParam_1());
        const bool *isConstant = FastAnyCast(bool, &message.getParam_2());
        assert(force);

		if(*isConstant) {
			mConstantForce = *force;
			if(mConstantForce != Vector3::ZERO) {
				mConstantForce = mConstantForce;
			}
		}
		else {
			mBody->applyCentralImpulse(btVector3((*force).x, (*force).y,(*force).z));
		}
    }
	//-------------------------------------------------------------------------
	void PhysicsComponentBullet::_applyTorqueByMessage(const Message& message)
	{
        const Vector3 *force = FastAnyCast(Vector3, &message.getParam_1());
        const bool *isConstant = FastAnyCast(bool, &message.getParam_2());
        assert(force);

		if(*isConstant) {
			mConstantAngularForce = *force;
		}
		else {
			mBody->applyTorqueImpulse(btVector3((*force).x, (*force).y,(*force).z));
		}
	}
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_setAngularVelocityByMessage(const Message& message)
    {
        const Vector3 *angularVelocity = FastAnyCast(Vector3, &message.getParam_1());
        assert(angularVelocity);

		// apply limits after orienting them
		Vector3 tmp = *angularVelocity;
		tmp.x = std::min(mMaxAngularVelocity,tmp.x);
		tmp.x = std::max(-mMaxAngularVelocity,tmp.x);
		tmp.y = std::min(mMaxAngularVelocity,tmp.y);
		tmp.y = std::max(-mMaxAngularVelocity,tmp.y);
		tmp.z = std::min(mMaxAngularVelocity,tmp.z);
		tmp.z = std::max(-mMaxAngularVelocity,tmp.z);

		mAngularVelocity = tmp;
		mBody->setAngularVelocity(btVector3(mAngularVelocity.x, mAngularVelocity.y,mAngularVelocity.z));
		//mBody->applyTorqueImpulse(btVector3(mAngularVelocity.x, mAngularVelocity.y,mAngularVelocity.z));
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_setVelocityByMessage(const Message& message)
    {
        const Vector3 *velocity = FastAnyCast(Vector3, &message.getParam_1());
        assert(velocity);

		// apply limits after orienting them
		mVelocity = *velocity;

		Real speed = mVelocity.length();
		if(speed > mMaxVelocity) {
			mVelocity *= mMaxVelocity/speed;
		}
		mBody->setLinearVelocity(btVector3(mVelocity.x, mVelocity.y,mVelocity.z));
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_setPositionByMessage(const Message& message)
    {
        const Vector3 *pos = FastAnyCast(Vector3, &message.getParam_1());
        assert(pos);
		mPosition = *pos;

        btTransform trans;
		
		mBody->getMotionState()->getWorldTransform(trans);
		trans.setOrigin( btVector3( pos->x, pos->y, pos->z ) );
		mBody->setWorldTransform(trans);
		mBody->getMotionState()->setWorldTransform(trans);
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_setScaleByMessage(const Message& message)
    {
        const Vector3 *scale = FastAnyCast(Vector3, &message.getParam_1());
        assert(scale);
        mScale = *scale;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_setDimensionsByMessage(const Message& message)
    {
        const Vector3 *dim = FastAnyCast(Vector3, &message.getParam_1());
        assert(dim);
        mDimensions = *dim;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_setDragByMessage(const Message& message)
    {
        const Real *drag = FastAnyCast(Real, &message.getParam_1());
        assert(drag);
        mDrag = *drag;
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_setOrientationByMessage(const Message& message)
    {
        const Quaternion *orient = FastAnyCast(Quaternion, &message.getParam_1());
		mOrientation = *orient;
        assert(orient);
		
		btTransform trans;
		mBody->getMotionState()->getWorldTransform(trans);
		trans.setRotation( btQuaternion( orient->x, orient->y, orient->z, orient->w));
		mBody->setWorldTransform(trans);
		mBody->getMotionState()->setWorldTransform(trans);
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_setEulerOrientationByMessage(const Message& message)
    {
        const Vector3 *angles = FastAnyCast(Vector3, &message.getParam_1());
        assert(angles);
        setOrientationEuler(*angles); // NOTE: Will send another message.
    }
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_setPositionOrientationByMessage(const Message& message)
    {
		if(message.getSender() == this) {
			return;
		}

        const Vector3 *pos = FastAnyCast(Vector3, &message.getParam_1());
        const Quaternion *orient = FastAnyCast(Quaternion, &message.getParam_2());
        assert(pos && orient);
		mPosition = *pos;
		mOrientation = *orient;

		btTransform trans;

		// get the current motion stat transform
		mBody->getMotionState()->getWorldTransform(trans);

		// update position / orientation in the transform
		trans.setOrigin( btVector3( pos->x, pos->y, pos->z ) );
		trans.setRotation( btQuaternion( orient->x, orient->y, orient->z, orient->w));

		// set the physics world transform for this body
		mBody->setWorldTransform(trans);
    }
	//-------------------------------------------------------------------------
	void PhysicsComponentBullet::_preTick(double currentTime)
	{
		/*
		int subSteps = ((PhysicsManagerBullet*)PhysicsManager::getManager())->getNumBulletSubSteps();

		std::cout << "_preTick substeps: " << subSteps << std::endl;

		// don't apply constant force if we've exceeded our max speed
		if(subSteps > 0 && mBody->getLinearVelocity().length2() < (mMaxVelocity * mMaxVelocity)) {
			double deltaTime = ((PhysicsManagerBullet*)PhysicsManager::getManager())->getDeltaTime();
			double timeStep = 1.0 / 60.0;
			double dampening = 0.5;
			double c = pow(1.0 - dampening, timeStep);
			double appliedDampening = pow(c,subSteps);

			std::cout << "_preTick appliedDampening: " << appliedDampening << std::endl;

			Vector3 velocity = Vector3(mBody->getLinearVelocity().x(),
				mBody->getLinearVelocity().y(),mBody->getLinearVelocity().z());


			// apply instant velocity with constant force per second
			// F = ((v + (f * t)) / c^n) - v
			Vector3 centralImpulse = ((velocity + (mConstantForce * deltaTime)) / appliedDampening) - velocity;

			std::cout << "_preTick centralImpulse.z: " << centralImpulse.z << std::endl;
			std::cout << "_preTick velocity.z: " << mBody->getLinearVelocity().z() << std::endl;
			mBody->applyCentralImpulse(btVector3(centralImpulse.x, centralImpulse.y, centralImpulse.z));

			// apply constant angular force
			Vector3 torqueImpulse = mConstantAngularForce * deltaTime;
			mBody->applyTorqueImpulse(btVector3(torqueImpulse.x, torqueImpulse.y, torqueImpulse.z));
		}
		*/
	}
	//-------------------------------------------------------------------------
	void PhysicsComponentBullet::preTickCallBack(double deltaTime)
	{
		if(mConstantForce != Vector3::ZERO) {
			btVector3 velocity = btVector3(mConstantForce.x, mConstantForce.y, mConstantForce.z);
			mBody->applyCentralImpulse(velocity * deltaTime);
		}

		if(mConstantAngularForce != Vector3::ZERO) {
			btVector3 angular = btVector3(mConstantAngularForce.x, mConstantAngularForce.y, mConstantAngularForce.z);
			mBody->applyTorqueImpulse(angular * deltaTime);
		}
	}
	//-------------------------------------------------------------------------
	void PhysicsComponentBullet::_postTick(double currentTime)
	{
		Vector3 position = getPosition();
		Quaternion orientation = getOrientation();

		// skip the message system
		/*
		GraphicsComponent* gfxComp = (GraphicsComponent*)
			GraphicsManager::getManager()->getComponent( getObjectId(), "Graphics" );
		if (gfxComp) {
			gfxComp->setPosition(position);
			gfxComp->setOrientation(orientation);
		}
		*/

		// just send one message per physics tick
		ObjectManager::getSingleton().postMessage(
			ObjectMessage::SET_POSITION_ORIENTATION, 
			position, 
			orientation,
			this, getObjectId(), Message::IMMEDIATE );

		mLastPreTickTime = currentTime;
	}
    //-------------------------------------------------------------------------
    void PhysicsComponentBullet::_processMessage(const Message& message)
    {
        switch (message.getType())
        {
            case ObjectMessage::APPLY_FORCE: _applyForceByMessage( message ); return;
			case ObjectMessage::APPLY_TORQUE: _applyTorqueByMessage( message ); return;
            case ObjectMessage::SET_ANGULAR_VELOCITY: _setAngularVelocityByMessage( message ); return;
            case ObjectMessage::SET_DIMENSIONS: _setDimensionsByMessage( message ); return;
			case ObjectMessage::SET_DRAG: _setDragByMessage( message ); return;
            case ObjectMessage::SET_ORIENTATION: _setOrientationByMessage( message ); return;
            case ObjectMessage::SET_ORIENTATION_EULER: _setEulerOrientationByMessage( message ); return;
            case ObjectMessage::SET_POSITION: _setPositionByMessage( message ); return;
            case ObjectMessage::SET_POSITION_ORIENTATION: _setPositionOrientationByMessage( message ); return;
            case ObjectMessage::SET_SCALE: _setScaleByMessage( message ); return;
            case ObjectMessage::SET_VELOCITY: _setVelocityByMessage( message ); return;
        }

		PhysicsComponentDefault::_processMessage(message);
    }
   //-------------------------------------------------------------------------
    const Vector3& PhysicsComponentBullet::getAngularVelocity() const
    {
		Vector3& angularVelocity = const_cast<Vector3&>(mAngularVelocity);
		btVector3 tmp = mBody->getAngularVelocity();
		angularVelocity.x = tmp.x();
		angularVelocity.y = tmp.y();
		angularVelocity.z = tmp.z();
        return mAngularVelocity;
    }
    //-------------------------------------------------------------------------
    const Vector3& PhysicsComponentBullet::getPosition() const
    {
        Vector3& pos = const_cast<Vector3&>(mPosition);
        btTransform trans;
        mBody->getMotionState()->getWorldTransform(trans);
        pos.x = trans.getOrigin().x();
        pos.y = trans.getOrigin().y();
        pos.z = trans.getOrigin().z();
        return mPosition;
    }
    //-------------------------------------------------------------------------
    const Quaternion& PhysicsComponentBullet::getOrientation() const
    {
        Quaternion& ori = const_cast<Quaternion&>(mOrientation);
        btTransform trans;
        mBody->getMotionState()->getWorldTransform(trans);
        ori.w = trans.getRotation().w();
        ori.x = trans.getRotation().x();
        ori.y = trans.getRotation().y();
        ori.z = trans.getRotation().z();
        return mOrientation;
    }
   //-------------------------------------------------------------------------
    const Vector3& PhysicsComponentBullet::getVelocity() const
    {
		Vector3& velocity = const_cast<Vector3&>(mVelocity);
		btVector3 tmp = mBody->getLinearVelocity();
		velocity.x = tmp.x();
		velocity.y = tmp.y();
		velocity.z = tmp.z();
        return mVelocity;
    }
    //-------------------------------------------------------------------------
    btCollisionShape* PhysicsComponentBullet::createTerrainShape(
        const String& filename, Vector3 dimensions, int upAxis, bool flipQuadEdges)
    {
        // Code adapted from the Bullet TerrainDemo.cpp sample

        // These values are used to determine the heightfield's
        // axis-aligned bounding box, multiplied by localScaling.
        // NOTE If I understood correctly those values are INDEPENDENT 
        //      of the heightfield values so the y values will be truncated 
        //      if below/above those min/max!!!
        float ymin, ymax;

        // Grid size taken from the image
        // Must be (2^N) + 1 such as 513 for "../../media/graphics/materials/textures/terrain.png"
        unsigned int gridSize;

        float* hf = GraphicsManager::getManager()
            ->getHeightfield( filename, ymin, ymax, gridSize );

        // TODO test if gridSize is (2^n)+1

        // The local origin of the heightfield is assumed to be the exact
        // center (as determined by width and length and height, with each
        // axis multiplied by the localScaling) !!!
        btHeightfieldTerrainShape* heightfieldShape =
            new btHeightfieldTerrainShape(
                gridSize, gridSize,
                (unsigned char*) hf, // mRawHeightfieldData,
                1, // s_gridHeightScale, IGNORED WHEN USING PHY_FLOAT !!!!
                ymin, // m_minHeight,
                ymax, //  m_maxHeight,  which is 24 for the terrain.png
                upAxis,
                PHY_FLOAT, // m_type,
                flipQuadEdges);

        // For "../../media/graphics/materials/textures/terrain.png"
        // which has a grid size of 513 we need to scale to:
        //  2.92969 * 512 ~= 1500 which is the desired terrain size
        // The y on the other hand
        //  24 * 4.166666  = 100 because 24 (=3*8) is the max value of the Terrain.png file !
        heightfieldShape->setLocalScaling(btVector3(
            dimensions.x / gridSize,
            dimensions.y / (ymax - ymin),
            dimensions.z / gridSize));

        return heightfieldShape;
    }
    //-------------------------------------------------------------------------
    btCollisionShape* PhysicsComponentBullet::createMeshShape()
    {
        /* WIP  (Steven) Taken from my old IGE test code

        String name = boost::any_cast<String>( params.find(Object::SET_MESH_NAME)->second);
        GraphicsComponent* comp = (GraphicsComponent*)(this->getOwnerObject()->getComponent("Graphics"));
        Ogre::Entity* entity = comp->_getEntity();

        MeshData meshData;
        Vector3 meshPosition;
        Quaternion meshOrientation;
        Vector3 meshScale(100,10,100);
        Ogre::Vector3* vertices;
        unsigned long *indices;
        GraphicsManager::getSingletonPtr()->getMeshInformation( entity->getMesh(), 
            meshData.vertexCount, 
            vertices, // (Ogre::Vector3*)(meshData.vertices),
            meshData.triangleCount,
            indices, // meshData.triangles,
            meshPosition, meshOrientation, meshScale);

        meshData.vertices = new Vector3[ meshData.vertexCount ];
        for (unsigned int i=0; i<meshData.vertexCount;i++)
        {
        meshData.vertices[i] = vertices[i];
        //std::cout << i << " " << vertices[i].x << " " << vertices[i].y << " " << vertices[i].z << std::endl;
        //std::cout << i << " " << meshData.vertices[i].x << " " << meshData.vertices[i].y << " " << meshData.vertices[i].z << std::endl;

        }

        meshData.triangles = new Vector3[ meshData.triangleCount/3 ];
        for (unsigned int i=0; i<meshData.triangleCount/3;i++)
        {
            meshData.triangles[i].x = indices[i*3];
            meshData.triangles[i].y = indices[i*3+1];
            meshData.triangles[i].z = indices[i*3+2];
        //std::cout << i << " " << indices[i*3] << " " << indices[i*3+1] << " " << indices[i*3+2] << std::endl;
        }

        mBody->setMeshShape( meshData.vertexCount, meshData.triangleCount/3,
            meshData.vertices, meshData.triangles, Vector3(100,10,100) );

        delete[] vertices;
        delete[] indices;
        */
        return 0;
    }
	//-------------------------------------------------------------------------
	void PhysicsComponentBullet::tickCallBack(double deltaTime)
	{
		btVector3 velocity = mBody->getLinearVelocity();
		Real speed = velocity.length();

		//std::cout << "tickCallBack velocity.z: " << velocity.z() << std::endl;

		// cap speed
		if(speed > mMaxVelocity) {
			std::cout << "tickCallBack() capping velocity " << std::endl;
			velocity *= mMaxVelocity / speed;
			mVelocity.x = velocity.x();
			mVelocity.y = velocity.y();
			mVelocity.z = velocity.z();
			mBody->setLinearVelocity(velocity);
		}
	}
}
