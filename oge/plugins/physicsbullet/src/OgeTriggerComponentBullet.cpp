/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/physics/OgeTriggerComponentBullet.h"
#include "oge/physics/OgePhysicsManagerBullet.h"
#include "oge/serialisation/OgeSerialiser.h"
#include "oge/object/OgeObject.h"
#include "oge/object/OgeObjectManager.h"
#include "oge/math/OgeMath.h"
#include "oge/graphics/OgeGraphicsComponent.h"
#include "oge/message/OgeMessageList.h"

#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/physics/OgePhysicsComponent.h" // enum ShapeType

// MUST READ DOC ON GhostObject: http://www.bulletphysics.com/Bullet/phpBB3/viewtopic.php?f=9&t=3026


//http://www.ogre3d.org/forums/viewtopic.php?f=5&t=46856&start=25
//http://github.com/nikki93/btogre/blob/1e7a11d5943b17dae4ddcea7dbbb4bd29b0833d0/include/BtOgrePG.h

/*
http://www.bulletphysics.com/Bullet/phpBB3/viewtopic.php?f=9&t=3359&p=12969&hilit=trigger#p12969

enum ObType
{
    OT_STATIC = 1,
    OT_DYNAMIC = 2
};

btDiscreteDynamicsWorld* m_collisionWorld = //  some initialization

float actorMass = 1;
btRigidBody* actor = new btRigidBody(actorMass , motionState, new btSphereShape(1), btVector3(0, 0, 0));
actor->setCollisionFlags(0);
collisionWorld->addRigidBody(actor, OT_DYNAMIC, OT_STATIC | OT_DYNAMIC);

btGhostObject* powerup = new btGhostObject();
powerup->setCollisionShape(new btSphereShape(1));
powerup->setCollisionFlags(
       btCollisionObject::CF_KINEMATIC_OBJECT |
       btCollisionObject::CF_NO_CONTACT_RESPONSE);
collisionWorld->addCollisionObject(powerup, OT_STATIC, OT_DYNAMIC);
*/
namespace oge
{
    //-------------------------------------------------------------------------
    bool TriggerComponentBullet::_reset(const MessageList& params, SceneManager* sceneManager)
    {
        // -------- INIT_PHYSICS_SHAPE -----------------
        String type;
        if (params.find( ObjectMessage::INIT_PHYSICS_SHAPE, type ))
        {
            // TODO Use only the enum (of course this means either a numerical value in the .xml or we must convert it when we load it
            if (type == "Box")
                _setShapeType( TriggerComponent::OGE_BOX );
            else if(type == "Sphere")
                _setShapeType( TriggerComponent::OGE_SPHERE );
            else if(type == "Convex")
                _setShapeType( TriggerComponent::OGE_CONVEX );
            else if(type == "Cylinder")
                _setShapeType( TriggerComponent::OGE_CYLINDER );
            else if(type == "Cone")
                _setShapeType( TriggerComponent::OGE_CONE );
            else if(type == "Capsule")
                _setShapeType( TriggerComponent::OGE_CAPSULE );
            else if(type == "Compound")
                _setShapeType( TriggerComponent::OGE_COMPOUND );
            else if(type == "Mesh")
                _setShapeType( TriggerComponent::OGE_MESH );
            else if(type == "Terrain") // TODO review
                _setShapeType( TriggerComponent::OGE_TERRAIN_HEIGHTFIELD );
        }
        else
        {
            LOGW( "INIT_PHYSICS_SHAPE message could not be found or its parameters are invalid. "
                  "while resetting a '" + getType() + "' component! Owner object Id: " + getObjectId() );

            _setShapeType( TriggerComponent::OGE_BOX ); // NEXT: Or return false?
        }

        // -----------------  SET_PHYSICS_MASS ------------------
        // See note about mass below
        float mass = 0.0f; // == Static object by default !
        params.find( ObjectMessage::SET_PHYSICS_MASS, mass );


        // ------ Create the body and add it to the world ----
        const Vector3 *dimensions;
        if (!params.find( ObjectMessage::SET_DIMENSIONS, dimensions ))
        {
            LOGW( "SET_DIMENSIONS message could not be found or its parameters are invalid "
                  "while resetting a '" + getType() + "' component! Owner object Id: " + getObjectId() );
            return false;
        }
        // Create the collision shape
        // TODO move this in the PhysiscManager and find a way to reuse shapes
        switch (mShapeType)
        {
            case PhysicsComponent::OGE_SPHERE:
                mShape = new btSphereShape(dimensions->x); // radius
                break;
            case PhysicsComponent::OGE_CYLINDER:
                // Aligned to the Y-axis TODO see btCylinderShapeX and Z
                mShape = new btCylinderShape(btVector3(dimensions->x, dimensions->y, dimensions->z)); // half extends
                break;
            case PhysicsComponent::OGE_CONE:
                // Aligned to the Y-axis TODO see btConeShapeX and Z
                mShape = new btConeShape( dimensions->x, dimensions->y ); // radius, height
                break;
            case PhysicsComponent::OGE_CAPSULE:
                // Aligned to the Y-axis TODO see btCapsuleShapeX and Z
                mShape = new btCapsuleShape( dimensions->x, dimensions->y ); // radius, height 
                break;
            case PhysicsComponent::OGE_MESH:
        		// NEXT: mShape = createMeshShape();
                break;
//            case PhysicsComponent::OGE_PLANE:
// TODO        		mShape = new btPlaneShape( ... );  or btStaticPlaneShape() ?
//                break;
            case PhysicsComponent::OGE_CONVEX:
// TODO        		mShape = new btConvexHullShape( ... );
                break;
            case PhysicsComponent::OGE_COMPOUND:
// TODO        		mShape = new btCompoundShape( 
                break;
            case TriggerComponent::OGE_TERRAIN_HEIGHTFIELD:
                // NEXT See PhysicsComponent for an implementation
                // but this seems overkill for a trigger... 
                // The bullet doc seems to imply that you should use 
                // ContactPoints to do things like friction (mud, etc).

            default:
                LOGW("No shape was defined creating a default box shape!");
            case PhysicsComponent::OGE_BOX:
                mShape = new btBoxShape(btVector3(dimensions->x, dimensions->y, dimensions->z)); // 5,10,5)); // TODO boxHalfExtents
                break;
        }

        // ------------------ SET_COLLISION_GROUP & MASK --------
        params.find( ObjectMessage::SET_COLLISION_GROUP, mCollisionGroup );
        params.find( ObjectMessage::SET_COLLISION_MASK, mCollisionMask );

        // NOTE !!! If you want to change the mass during simulation
        //   the body must be removed from the simulation, change mass
        //   then added again.
        btVector3 localInertia(0,0,0);
        if (mass != 0.0f) // if 0 then non-dynamic !
            mShape->calculateLocalInertia( btScalar(mass), localInertia );

        btTransform initTransform;
        initTransform.setIdentity();

        Vector3 pos;
        if (params.find( ObjectMessage::SET_POSITION, pos ))
            initTransform.setOrigin( btVector3(pos.x, pos.y, pos.z) ); // TODO !!!
        else
            initTransform.setOrigin( btVector3(0,0,0) );

        // using motionstate is recommended, it provides interpolation capabilities,
        // and only synchronizes 'active' objects
        //        btDefaultMotionState* motionState = new btDefaultMotionState(groundTransform);
// code from PhysicsCommponent btMotionState* motionState = new OgeMotionState(getObjectId(), this, initTransform );

        // At last create the physical object
        mBody = new btGhostObject();
        mBody->setWorldTransform( initTransform );
        mBody->setCollisionShape( mShape );
        mBody->setCollisionFlags(
            btCollisionObject::CF_KINEMATIC_OBJECT |  // TODO explication
            btCollisionObject::CF_NO_CONTACT_RESPONSE ); // IMPORTANT This prevents object colliding to react on collision i.e. bounce

        mBody->setActivationState(DISABLE_DEACTIVATION);

        mCollisionData.mObjectId = getObjectId();
        mCollisionData.mType = "Trigger"; // TODO see PhysicsComponentBullet
        mBody->setUserPointer(&mCollisionData);


        // ------------ All others messages types --------------------
        // Careful !!! INIT_PHYSICS_SHAPE must be called first!
        for (MessageList::ConstIter it = params.list.begin(); it != params.list.end(); ++it) {
            _processMessage( (*it) );
        }

        return true;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::_destroy(SceneManager* sceneManager)
    {
        PhysicsManagerBullet::getSingletonPtr()->removeCollisionObject(mBody);

        delete mBody; mBody = 0;
        delete mShape; mShape = 0;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::_activate(bool activate, SceneManager* sceneManager)
    {
		// remove it just in case it has already been added somehow
		PhysicsManagerBullet::getSingletonPtr()->removeCollisionObject(mBody);
		
		if(activate) {
			// add it to the collision/dynamics world
			PhysicsManagerBullet::getSingletonPtr()->addCollisionObject( mBody, mCollisionGroup, mCollisionMask );
			mBody->setActivationState(DISABLE_DEACTIVATION);
		}
		else {
			mBody->setActivationState(DISABLE_SIMULATION);
		}
    }
    //-------------------------------------------------------------------------
    Component::UpdateRate TriggerComponentBullet::_getUpdateRate() const
    {
        return Component::EVERY_TICK;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::_update(double deltaTime)
    {
        for (int i=0; i<mBody->getNumOverlappingObjects(); ++i) {
            btCollisionObject* object = mBody->getOverlappingObject(i);
            if(object) {
                CollisionData* data = (CollisionData*)object->getUserPointer();
				// @TODO how do we avoid our ghost object colliding with our physics rigid body?
				if(data->mObjectId != getObjectId()) {
					rememberObject( data->mObjectId );
				}
            }
        }

        // Triggers the callbacks
        triggerCallbacks();
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::write(Serialiser* serialisePtr)
    {
//        serialisePtr->writeVector3(mPosition);
//        serialisePtr->writeVector3(mScale);
//        serialisePtr->writeQuaternion(mOrientation);
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::read(Serialiser* serialisePtr)
    {
//        setPosition(serialisePtr->readVector3());
//        setScale(serialisePtr->readVector3());
//        setOrientation(serialisePtr->readQuaternion());
    }
    //-------------------------------------------------------------------------
    bool TriggerComponentBullet::fixup()
    {
        // no fixup to do
        return true;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::setPosition(const Vector3& position)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_POSITION, position, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::setScale(const Vector3& scale)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_SCALE, scale, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::setDimensions(const Vector3& dimensions)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_DIMENSIONS, dimensions, Any(), 0, getObjectId() );
/*_ old code
        Vector3 scale = dimensions / getDimensions();
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_SCALE, scale, Any(), 0, getObjectId() );
*/
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::setOrientation(const Quaternion& orientation)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_ORIENTATION, orientation, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::setOrientationEuler(const Vector3& angles)
    {
#pragma OGE_WARN("FIXME/TODO: This code perhaps works in Ogre. Similar functions should be added to OGE.")
    //    Matrix3 rot;
    //    rot.FromEulerAnglesYXZ(Degree(angles.y), Degree(angles.x), Degree(angles.z));
    //    Quaternion qrot;
    //    qrot.FromRotationMatrix(rot);
    // 
    //    ObjectManager::getSingleton().postMessage(
    //        ObjectMessage::SET_ORIENTATION, qrot, Any(), 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::setPositionOrientation(const Vector3& position, const Quaternion& orientation)
    {
        ObjectManager::getSingleton().postMessage(
            ObjectMessage::SET_POSITION_ORIENTATION, position, orientation, 0, getObjectId() );
    }
    //-------------------------------------------------------------------------
    const Vector3& TriggerComponentBullet::getDimensions() const
    {
        return mDimensions;
    }
    //-------------------------------------------------------------------------
    Vector3 TriggerComponentBullet::getEulerOrientation() const
    {
#pragma OGE_WARN("TODO: This code perhaps works in Ogre. Functions should be added to OGE")
    //    Matrix3 rot;
    //    mOrientation.ToRotationMatrix(rot);
    //    Radian yAngle, pAngle, rAngle;
    //    rot.ToEulerAnglesYXZ(yAngle, pAngle, rAngle);
    //    return Vector3(yAngle.valueDegrees(), pAngle.valueDegrees(), rAngle.valueDegrees());
          return Vector3();
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::_setPositionByMessage(const Message& message)
    {
// TODO really useful this assert() ? Would we not anyway segfault hence unncessary assert?
        const Vector3 *pos = FastAnyCast(Vector3, &message.getParam_1());
        assert(pos);

        btTransform& trans = mBody->getWorldTransform();
        trans.setOrigin( btVector3( pos->x, pos->y, pos->z ) );
        mBody->setWorldTransform( trans ); // TODO necessary?

     	// TODO ? mBody->setCenterOfMassTransform (trans); see DynamicCharacterController.cpp
//std::cout << "setPosition: " << pos->x << " " << pos->y << " " << pos->z << std::endl;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::_setScaleByMessage(const Message& message)
    {
        const Vector3 *scale = FastAnyCast(Vector3, &message.getParam_1());
        assert(scale);
        mScale = *scale;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::_setDimensionsByMessage(const Message& message)
    {
        const Vector3 *dim = FastAnyCast(Vector3, &message.getParam_1());
        assert(dim);
//_        // Do what setDimensions() does, but request to process the message immediately.
//_        // NOTE: mScale will be set again.
//_        mScale = *dim / getDimensions();
//_        ObjectManager::getSingleton().postMessage( 
//_            ObjectMessage::SET_SCALE, mScale, Any(), 0, getObjectId(), Message::IMMEDIATE );
        mDimensions = *dim;
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::_setOrientationByMessage(const Message& message)
    {
        const Quaternion *orient = FastAnyCast(Quaternion, &message.getParam_1());
        assert(orient);

        btTransform& trans = mBody->getWorldTransform();
        trans.setRotation( btQuaternion( orient->x, orient->y, orient->z, orient->w) );
        mBody->setWorldTransform( trans ); // TODO necessary?

     	// TODO ? mBody->setCenterOfMassTransform (trans); see DynamicCharacterController.cpp
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::_setEulerOrientationByMessage(const Message& message)
    {
        const Vector3 *angles = FastAnyCast(Vector3, &message.getParam_1());
        assert(angles);
        setOrientationEuler(*angles); // NOTE: Will send another message.
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::_setPositionOrientationByMessage(const Message& message)
    {
        const Vector3 *pos = FastAnyCast(Vector3, &message.getParam_1());
        const Quaternion *orient = FastAnyCast(Quaternion, &message.getParam_2());
        assert(pos && orient);
        //mPosition = *pos;
        btTransform trans;
        trans.setIdentity();
        trans.setOrigin( btVector3( pos->x, pos->y, pos->z ) );

        trans.setRotation( btQuaternion( orient->x, orient->y, orient->z, orient->w));

        mBody->setWorldTransform( trans ); // TODO necessary?
     	// TODO ? mBody->setCenterOfMassTransform (trans); see DynamicCharacterController.cpp
    }
    //-------------------------------------------------------------------------
    void TriggerComponentBullet::_processMessage(const Message& message)
    {
        switch (message.getType())
        {
            case ObjectMessage::SET_POSITION: _setPositionByMessage( message ); return;
            case ObjectMessage::SET_SCALE: _setScaleByMessage( message ); return;
            case ObjectMessage::SET_DIMENSIONS: _setDimensionsByMessage( message ); return;
            case ObjectMessage::SET_ORIENTATION: _setOrientationByMessage( message ); return;
            case ObjectMessage::SET_ORIENTATION_EULER: _setEulerOrientationByMessage( message ); return;
            case ObjectMessage::SET_POSITION_ORIENTATION: _setPositionOrientationByMessage( message ); return;
            // NEXT default: ++counterOfUnnecessaryMessageProcessedByAComponent;
        }
    }
    //-------------------------------------------------------------------------
    const Vector3& TriggerComponentBullet::getPosition() const
    {
        Vector3& pos = const_cast<Vector3&>(mPosition);
        const btTransform& trans = mBody->getWorldTransform();
        pos.x = trans.getOrigin().x();
        pos.y = trans.getOrigin().y();
        pos.z = trans.getOrigin().z();
        return mPosition;
    }
    //-------------------------------------------------------------------------
    const Quaternion& TriggerComponentBullet::getOrientation() const
    {
        Quaternion& ori = const_cast<Quaternion&>(mOrientation);
        const btTransform& trans = mBody->getWorldTransform();
        ori.w = trans.getRotation().w();
        ori.x = trans.getRotation().x();
        ori.y = trans.getRotation().y();
        ori.z = trans.getRotation().z();
        return mOrientation;
    }
    //-------------------------------------------------------------------------
}

