/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/physics/OgePhysicsManagerBullet.h"
#include "oge/engine/OgeEngineManager.h"
#include "oge/resource/OgeResourceRegistry.h"
#include "oge/logging/OgeLogManager.h"

#include "oge/physics/OgePhysicsComponentBullet.h"
#include "oge/physics/OgePhysicsSceneManagerBullet.h"
#include "oge/physics/OgePhysicsDebugDrawerBullet.h"
#include "oge/physics/OgeTriggerComponentBullet.h"
#include "oge/OgeProfiler.h"

namespace oge
{
	//-------------------------------------------------------------------------
	static void physicsPreTickCallback(btDynamicsWorld *world, btScalar timeStep)
	{
		((PhysicsManagerBullet*)PhysicsManagerBullet::getManager())->preTickCallback(world, timeStep);
	}

	//-------------------------------------------------------------------------
	static void physicsTickCallback(btDynamicsWorld *world, btScalar timeStep)
	{
		((PhysicsManagerBullet*)PhysicsManagerBullet::getManager())->tickCallback(world, timeStep);
	}

    //-------------------------------------------------------------------------
    template<> PhysicsManagerBullet* Singleton<PhysicsManagerBullet>::mSingleton = 0;
    //-------------------------------------------------------------------------
    PhysicsManagerBullet::PhysicsManagerBullet() : 
        PhysicsManager("PhysicsManager"), // Not PhysicsManagerBullet as we -for now- want only one mgr for each type
		mBulletLocalTime(1.0/60.0),
		mBulletFixedTimeStep(1.0/60.0),
		mDeltaTime(0),
        mDynamicsWorld(0), 
		mIsSimulating(true), 
		mLastPhysicsStepTime(0),
		mLastTime(0), 
		mMaxBulletSubSteps(60),
		mNumBulletSubSteps(0),
		mDebugDrawer(0)
    {

    }
    //-------------------------------------------------------------------------
    PhysicsManagerBullet::~PhysicsManagerBullet()
    {

    }
    //-------------------------------------------------------------------------
    void PhysicsManagerBullet::addRigidBody(btRigidBody* body,
        const short collisionGroup, const short collisionMask)
    {
        assert(body);

        if (collisionGroup == 0 && collisionMask == 0)
        {
            // use default collision group/mask values (dynamic/kinematic/static)
            static_cast <btDiscreteDynamicsWorld *> (mDynamicsWorld)
                ->addRigidBody( body );
        }
        else
        {
            static_cast <btDiscreteDynamicsWorld *> (mDynamicsWorld)
                ->addRigidBody( body, collisionGroup, collisionMask );
        }
    }
    //-------------------------------------------------------------------------
    void PhysicsManagerBullet::addCollisionObject(btCollisionObject* body,
        const short collisionGroup, const short collisionMask)
    {
        assert(body);
        // TODO param (see bullet CharacterDemo sample)
        // void addCollisionObject(btCollisionObject* collisionObject,
        //     short int collisionFilterGroup=btBroadphaseProxy::DefaultFilter,
        //     short int collisionFilterMask=btBroadphaseProxy::AllFilter);
        // such as 	only collide with static for now (no interaction with dynamic objects)
	    // mDynamicsWorld->addCollisionObject(m_ghostObject,
        //     btBroadphaseProxy::CharacterFilter,
        //     btBroadphaseProxy::StaticFilter|btBroadphaseProxy::DefaultFilter);

        if (collisionGroup == 0 && collisionMask == 0)
        {
            // use default collision group/mask values (dynamic/kinematic/static)
            mDynamicsWorld->addCollisionObject( body );
        }
        else
        {
            mDynamicsWorld->addCollisionObject( body, collisionGroup, collisionMask );
        }
    }
    //-------------------------------------------------------------------------
    void PhysicsManagerBullet::removeCollisionObject(btCollisionObject* body)
    {
        assert(body);
        mDynamicsWorld->removeCollisionObject( body );
    }
    //-------------------------------------------------------------------------
    void PhysicsManagerBullet::removeRigidBody(btRigidBody* body)
    {
        assert(body);
		mDynamicsWorld->removeRigidBody( body );
    }
    //-------------------------------------------------------------------------
    bool PhysicsManagerBullet::initialise()
    {
        // Register the in-build component templates
        ObjectManager* objectMgr = ObjectManager::getSingletonPtr();
		objectMgr->registerComponentTemplate( "Physics", new PhysicsComponentTemplateBullet() );
        objectMgr->registerComponentTemplate( "Physics", new TriggerComponentTemplateBullet() );

        // Registering the in-build SceneManagerFactories
        mSceneManagerEnumerator->registerSceneManagerFactory(
            new PhysicsSceneManagerFactoryBullet());

        //if (mCreateDefaultSceneManager) // NEXT can be set by config?
        //createDefaultSceneManager();
        createDefaultSceneManager();

        // Bullet specific
        // TODO Get param from config file see GraphicsManagerOGRE::initialise()

        // collision configuration contains default setup for memory, collision setup
        mCollisionConfiguration = new btDefaultCollisionConfiguration();

        // use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
        mDispatcher = new btCollisionDispatcher(mCollisionConfiguration);

        // We need to specify what Broadphase algorithm we want to use. 
        // Choosing the broadphase is important if the world will have 
        // a lot of rigid bodies in it, since it has to somehow check 
        // every pair which when implemented naively is an O(n^2) problem.
        mBroadphase = new btDbvtBroadphase();

        // Without this the GhostObjects wouldn't have any overlapping shapes
        // hence no trigger could work!!!!!!!!!!!! The bullet doc is s***
        mBroadphase->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());

        // the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
        btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver();
        mSolver = solver;


        // TODO or a btSoftRigidDynamicsWorld
        mDynamicsWorld = new btDiscreteDynamicsWorld(
            mDispatcher,
            mBroadphase,
            mSolver,
            mCollisionConfiguration);

        if (mDynamicsWorld == 0)
        {
            LOGE("The bullet dynamics world couldn't be created!");
            return false;
        }

		mDynamicsWorld->setInternalTickCallback(physicsTickCallback);
		mDynamicsWorld->setInternalTickCallback(physicsPreTickCallback,0,true);

        //mDynamicsWorld->setGravity(btVector3(0,-10,0)); // TODO Tmp
		mDynamicsWorld->setGravity(btVector3(0.,0.,0.)); // TODO Tmp

        mDebugDrawer = new DebugDrawer(mDynamicsWorld);

        if (!System::initialise()) 
            return false;
    
        return true;
    }
	//-------------------------------------------------------------------------
	void PhysicsManagerBullet::preTickCallback(btDynamicsWorld *world, btScalar timeStep)
	{
		// let physics components do stuff before each internal bullet tick
		PhysicsSceneManagerBullet* mgr = static_cast<PhysicsSceneManagerBullet*>(getActiveSceneManager());
		mgr->preTickCallBack(timeStep);
	}
	//-------------------------------------------------------------------------
	void PhysicsManagerBullet::tickCallback(btDynamicsWorld *world, btScalar timeStep)
	{
		// let physics components do stuff after each internal bullet tick
		PhysicsSceneManagerBullet* mgr = static_cast<PhysicsSceneManagerBullet*>(getActiveSceneManager());
		mgr->tickCallBack(timeStep);
	}
    //-------------------------------------------------------------------------
    void PhysicsManagerBullet::shutdown()
    {
        System::shutdown();

        // Remove rigidbodies and delete them
        // Delete collision shapes

        delete mDebugDrawer;
        delete mDynamicsWorld;
        delete mSolver;
        delete mBroadphase;
        delete mDispatcher;
        delete mCollisionConfiguration;
    }
	//-------------------------------------------------------------------------
	void PhysicsManagerBullet::preTick(double currentTime)
	{
		if (mLastTime == 0) {
            mLastTime = currentTime;
		}
		
		// update the delta time
        mDeltaTime = (currentTime - mLastTime) * 0.001;
        mLastTime = currentTime;

		// update the bullet local time and numsubsteps - 
		// copied from btDynamicWorld::stepSimulation()
		// we try to keep in sync here so we can accurately predict
		// the number of bullet substeps which is needed for maintaining
		// a constant force when dampening is involved
		mBulletLocalTime += (btScalar)mDeltaTime;
		if (mBulletLocalTime >= mBulletFixedTimeStep)
		{
			mNumBulletSubSteps = int( mBulletLocalTime / mBulletFixedTimeStep);
			mBulletLocalTime -= mNumBulletSubSteps * mBulletFixedTimeStep;
		}
		else {
			mNumBulletSubSteps = 0;
		}

		if(mNumBulletSubSteps > mMaxBulletSubSteps) {
			mNumBulletSubSteps = mMaxBulletSubSteps;
		}

		PhysicsManager::preTick(currentTime);
	}
    //-------------------------------------------------------------------------
    void PhysicsManagerBullet::tick(double currentTime)
    {
		OgeProfileGroup("Physics BULLET", OGEPROF_SYSTEMS);
        if (mDynamicsWorld && mIsSimulating)
        {
            // TODO Should we use fixed time stepping?
            //      The documenation & sample imply that we should but why?
		    //stepSimulation proceeds the simulation over 'timeStep', units in preferably in seconds.
		    //By default, Bullet will subdivide the timestep in constant substeps of each 'fixedTimeStep'.
		    //in order to keep the simulation real-time, the maximum number of substeps can be clamped to 'maxSubSteps'.
		    //You can disable subdividing the timestep/substepping by passing maxSubSteps=0 as second argument to stepSimulation, 
			// but in that case you have to keep the timeStep constant.
		    // virtual int stepSimulation( btScalar timeStep,int maxSubSteps=1, btScalar fixedTimeStep=btScalar(1.)/btScalar(60.))=0;

			// let the physics world do interpolation for us by passing maxSubSteps > 1
			// from bulletphysics.org wiki:
			// When you pass Bullet maxSubSteps > 1, it will interpolate movement for you. 
			// This means that if your fixedTimeStep is 3 units, and you pass a timeStep of 4, 
			// then it will do exactly one tick, and estimate the remaining movement by 1/3. 
			// This saves you having to do interpolation yourself, but keep in mind that maxSubSteps needs to be greater than 1.
		
			// IMPORTANT make sure that fixedTimeStep is less than 1000 / tickInterval for this  system!
			// stepSimulation will not really do anything if the deltaTime is less than fixedTimeStep
			//btScalar fixedTimeStep = 1./60.;
			
			//if(NetworkManager::getManager()->isServer()) {
				int subSteps = mDynamicsWorld->stepSimulation((btScalar)(mDeltaTime), mMaxBulletSubSteps,(btScalar)mBulletFixedTimeStep);
				//int subSteps = mDynamicsWorld->stepSimulation((btScalar)(mDeltaTime), 0,(btScalar)mBulletFixedTimeStep);

				//std::cout << "_tick subSteps: " << subSteps << std::endl;
			//}

			/*
			int numManifolds = mDynamicsWorld->getDispatcher()->getNumManifolds();
			for (int i=0;i<numManifolds;i++)
			{
				btPersistentManifold* contactManifold = mDynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
				btCollisionObject* obA = static_cast<btCollisionObject*>(contactManifold->getBody0());
				btCollisionObject* obB = static_cast<btCollisionObject*>(contactManifold->getBody1());
			
				int numContacts = contactManifold->getNumContacts();
				for (int j=0;j<numContacts;j++)
				{
					btManifoldPoint& pt = contactManifold->getContactPoint(j);
					btVector3 ptA = pt.getPositionWorldOnA();
					btVector3 ptB = pt.getPositionWorldOnB();
				}
			}
			*/

				

			int numSteps = (int)(currentTime / mBulletFixedTimeStep);
			mLastPhysicsStepTime = numSteps * mBulletFixedTimeStep;
        }

        mDebugDrawer->step();

		// go over every entity we have and report the position and velocity
		//((PhysicsSceneManagerBullet*)mSceneManager)->logProfileInfo(currentTime);
    }
    //-------------------------------------------------------------------------
    PhysicsManagerBullet* PhysicsManagerBullet::createSingleton()
    {
        if (!mSingleton)
            mSingleton = new PhysicsManagerBullet();
        
        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void PhysicsManagerBullet::destroySingleton()
    {
        if (mSingleton)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
	//-------------------------------------------------------------------------
	DebugDrawer::DebugMode PhysicsManagerBullet::getDebugDrawerMode()
	{
		if(mDebugDrawer) {
            return (DebugDrawer::DebugMode)mDebugDrawer->getDebugMode();
		}

		return DebugDrawer::OGE_NoDebug;
	}
    //-------------------------------------------------------------------------
    const String PhysicsManagerBullet::getLibrariesVersion()
    {
        String version(String("Physics: Bullet ") + StringUtil::toString(BT_BULLET_VERSION) + "\n"); // btScalar.h
        return version;
    }
    //-----------------------------------------------------------------------------
    void PhysicsManagerBullet::createDefaultSceneManager()
    {
        LOGI("Creating the default Bullet scene manager...");
        
        // This SM will be destroyed by the default ObjectSceneManager
        // as it will add it to its enumerator
        mSceneManager = static_cast<PhysicsSceneManager*>(
            mSceneManagerEnumerator->createSceneManager(
                "OGE_Default_SceneManager", "Generic SM"));

        if (mSceneManager == 0)
        {
            LOGE("A Bullet SceneManager of type 'Generic SM' couldn't be created.");
            return;
        }
    }
    //-------------------------------------------------------------------------
    void PhysicsManagerBullet::setDebugDrawerMode(DebugDrawer::DebugMode mode)
    {
        if (mDebugDrawer)
            mDebugDrawer->setDebugMode(mode);
    }
    //-------------------------------------------------------------------------
}
