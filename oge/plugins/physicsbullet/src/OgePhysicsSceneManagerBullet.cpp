/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/physics/OgePhysicsSceneManagerBullet.h"
#include "oge/physics/OgePhysicsComponentBullet.h"

namespace oge
{
    //-------------------------------------------------------------------------
    PhysicsSceneManagerFactoryBullet::PhysicsSceneManagerFactoryBullet()
    {
        // Temporary sm to get the type name
        PhysicsSceneManagerBullet sm("temp");
        sm.initMetaData();
        mType = sm.getType();
    }
    //-------------------------------------------------------------------------
    PhysicsSceneManagerFactoryBullet::~PhysicsSceneManagerFactoryBullet()
    {
    }
    //-------------------------------------------------------------------------
    SceneManager* PhysicsSceneManagerFactoryBullet::createInstance(const String& name)
    {
        PhysicsSceneManagerBullet* sm = new PhysicsSceneManagerBullet(name);
        sm->init();
        assert(sm->getType() == mType);
        return sm;
    }
    //-------------------------------------------------------------------------
    bool PhysicsSceneManagerFactoryBullet::checkAvailability()
    {
        mAvailable = true;
        return true;
    }
    //-------------------------------------------------------------------------


    //-------------------------------------------------------------------------
    PhysicsSceneManagerBullet::PhysicsSceneManagerBullet(const String& name) :
        PhysicsSceneManager(name)
    {
        initMetaData();
    }
     //-------------------------------------------------------------------------
    PhysicsSceneManagerBullet::~PhysicsSceneManagerBullet()
    {
        clearScene();
    }
    //-------------------------------------------------------------------------
    void PhysicsSceneManagerBullet::init()
    {
        // Override this method to set your SM in your plugin
    }
    //-------------------------------------------------------------------------
    void PhysicsSceneManagerBullet::initMetaData() const
    {
        mMetaData.type = "Generic SM";
        mMetaData.family = "Physics";
        mMetaData.description = "Generic physics scene manager Bullet";
        mMetaData.sceneTypeMask = 0; // OGRE setting - Not used in fact
        mMetaData.typeName = "DefaultSceneManager"; // used to initialise ogre3d
        mMetaData.worldGeometrySupported = false;
    }
	//-------------------------------------------------------------------------
	void PhysicsSceneManagerBullet::logProfileInfo(double currentTime)
	{
		double realTime = EngineManager::getSingletonPtr()->getCurrentTime(true);

		ComponentMapIter iter;
        for (iter = mComponents.begin(); mComponents.end() != iter; ++iter) {
			if(((*iter).second)->getType() == "Location") {
				PhysicsComponentBullet *comp = (PhysicsComponentBullet*)(*iter).second;
				LOG(String("Physics, ") + StringUtil::toString((oge::Real)currentTime) + String(", ") + StringUtil::toString((oge::Real)realTime) + String(", ") + comp->getObjectId() + String(", ") + StringUtil::vector3ToString(comp->getPosition()) + String(", ") + StringUtil::vector3ToString(comp->getVelocity()));
			}
        }
	}
    //-------------------------------------------------------------------------
    void PhysicsSceneManagerBullet::update(double deltaTime)
    {
        PhysicsSceneManager::update(deltaTime);
    }
    //-------------------------------------------------------------------------
    void PhysicsSceneManagerBullet::clearScene()
    {
        PhysicsSceneManager::clearScene();
    }
    //-------------------------------------------------------------------------
    bool PhysicsSceneManagerBullet::processMessage(const Message& message)
    {
        return PhysicsSceneManager::processMessage(message);
    }
    //-------------------------------------------------------------------------
	void PhysicsSceneManagerBullet::preTickCallBack(double deltaTime)
	{
       for (int i = 0; Component::NUM_UPDATE_RATES > i; ++i)
        {
            ComponentSet &compList = mUpdatableComponents[i];
			ComponentIter iter;
            for (iter = compList.begin(); compList.end() != iter; ++iter) {
				if((*iter)->getType() == "Location") {
					// update will check for max velocity and cap if necessary
					((PhysicsComponentBullet*)(*iter))->preTickCallBack(deltaTime);
				}
            }
        }
	}
    //-------------------------------------------------------------------------
	void PhysicsSceneManagerBullet::tickCallBack(double deltaTime)
	{
       for (int i = 0; Component::NUM_UPDATE_RATES > i; ++i)
        {
            ComponentSet &compList = mUpdatableComponents[i];
			ComponentIter iter;
            for (iter = compList.begin(); compList.end() != iter; ++iter) {
				if((*iter)->getType() == "Location") {
					// update will check for max velocity and cap if necessary
					((PhysicsComponentBullet*)(*iter))->tickCallBack(deltaTime);
				}
            }
        }
	}
}