/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/script/OgeScriptManagerLua.h"
#include "oge/resource/OgeResourceRegistry.h"
#include "oge/logging/OgeLogManager.h"

//------- class to bind ---------------
#include "oge/message/OgeMessage.h"
#include "lua.h"

/** 
 * Error method to by-pass the exit that is called by the standard
 * lua_atpanic.
 * @todo Not sure this works. Should we also throw?
 *       http://www.gamedev.net/community/forums/topic.asp?topic_id=284645
 */
extern "C"
{
    int OgeLuaError(lua_State* state)
    {
        if (state)
        {
            std::string str( lua_tostring(state, -1) );
            lua_pop(state, 1); // remove error mesage
            lua_getglobal(state, "ERROR");
            // TODO is throw  LUAI_THROW also needed to avoid the exit?
        }
        return 0;
    }
}


namespace oge
{
    //-------------------------------------------------------------------------
    template<> ScriptManagerLua* Singleton<ScriptManagerLua>::mSingleton = 0;
    //-------------------------------------------------------------------------
    ScriptManagerLua::ScriptManagerLua() : 
        ScriptManager("ScriptManager"), // Not ScriptManagerLua as we -for now- want only one mgr for each type
        mLuaState(0)
    {

    }
    //-------------------------------------------------------------------------
    ScriptManagerLua::~ScriptManagerLua()
    {
        closeState();
    }
    //-------------------------------------------------------------------------
    bool ScriptManagerLua::initialise()
    {
    //    if (!setupResourceManagement())
    //        return false;

        // Create a new lua state
        openState();

        if (!System::initialise())
            return false;
    
        return true;
    }
    //-------------------------------------------------------------------------
    void ScriptManagerLua::shutdown()
    {
        System::shutdown();

        closeState();

    //    shutdownResourceManagement();
    }
    //-------------------------------------------------------------------------
    void ScriptManagerLua::tick(double currentTime)
    {
        LOGE("yes it works");
    }
    //-------------------------------------------------------------------------
    ScriptManagerLua* ScriptManagerLua::createSingleton()
    {
        if (!mSingleton)
            mSingleton = new ScriptManagerLua();
        
        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void ScriptManagerLua::destroySingleton()
    {
        if (mSingleton)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //-------------------------------------------------------------------------
    void ScriptManagerLua::openState()
    {
        // Create a new lua state (was lua_open() )
        // TODO use lua_newstate to create a state with a standard allocation
        //      function (based on realloc). 
        mLuaState = luaL_newstate(); 

        // luaL_newstate creates a standard panic function that uses exit()
        // which kills the application (see lua_atpanic )
        // If you want that lua exit with fatal error then comment out this method
        lua_register( mLuaState, "_ALERT", OgeLuaError);

        if (!mLuaState)
        {
            LOGE("A lua state couldn't be created.");
            return;
        }

        // Opens all standard Lua libraries into the given state.
        luaL_openlibs(mLuaState); 
    
        // The above method is similar to:
        // luaopen_base(mLuaState);
        // luaopen_package(mLuaState);  // was luaopen_loadlib()
        // luaopen_string(mLuaState);
        // luaopen_table(mLuaState);
        // luaopen_math(mLuaState);
        // luaopen_io(mLuaState);
        // luaopen_os(mLuaState);
        // luaopen_debug(mLuaState);

        luabind::open(mLuaState); // is this performed by luaL_openlibs() ?

        // Connect LuaBind to the lua state
        luabind::open(mLuaState);

        // Bind the oge class to lua
        bindClasses(mLuaState);
    }
    //-------------------------------------------------------------------------
    void ScriptManagerLua::closeState()
    {
        if (mLuaState)
        {
            lua_close(mLuaState);
            mLuaState = 0;
        }
    }
    //-------------------------------------------------------------------------
    void ScriptManagerLua::resetState()
    {
        LOGI("Reset Lua state");
        closeState();
        openState();
    }
    //-------------------------------------------------------------------------
    bool ScriptManagerLua::doFile( const String& script )
    {
        if (script.empty())
            return false;

        return (0 == luaL_dofile( mLuaState, script.c_str()) ); // was lua_dofile
    }
    //-------------------------------------------------------------------------
    bool ScriptManagerLua::doStdin()
    {
        return (0 == luaL_dofile( mLuaState, 0 ));
    }
    //-------------------------------------------------------------------------
    bool ScriptManagerLua::doString(const String& script)
    {
        if (script.empty())
            return false;
        return (0 == luaL_dostring(mLuaState, script.c_str()) );  // was lua_dostring
    }
    //-------------------------------------------------------------------------
    String ScriptManagerLua::getStackDump(lua_State* state)
    {
        if (state == 0)
            state = mLuaState;

        int top = lua_gettop(state);
        String dump;

        for (int i = 1; i <= top; i++)
        {
            int t = lua_type(state, i);

            switch (t)
            {
                case LUA_TSTRING:
                    dump += lua_tostring(state, i);
                    break;
                case LUA_TBOOLEAN:
                    dump += (lua_toboolean(state, i) ? "true" : "false");
                    break;
                case LUA_TNUMBER:
                    dump += StringUtil::toString((oge::Real)lua_tonumber(state, i));
                    break;
                default:
                    dump += lua_typename(state, t);
            }
            dump += "  ";
        }
        dump += "\n";
        return dump;
    }
    //-------------------------------------------------------------------------
    void ScriptManagerLua::bindClasses( lua_State* luaState )
    {
        luabind::module( luaState )
        [  // Note: '[' and not '{'
            // Bind our String type (LuaBind already knows about std::string, so no need for more)
            luabind::class_<String>("String"),

            // The template paramater is the C++ class, the "Message" is the class name for Lua
            luabind::class_<Message>("Message")
    //        luabind::.def( "setMessage", &MessageBoy::setMessage ) // bind the setMessage method
    //        luabind::.def( "getMessage", &MessageBoy::getMessage ) // bind the getMessage method
        ];
    }
    //-------------------------------------------------------------------------
    const String ScriptManagerLua::getLibrariesVersion()
    {
		String version("Script: ");
		version += String(LUA_RELEASE) + String("\n");
        return version;
    }
    //-----------------------------------------------------------------------------
}
