# CMakeLists.txt  -  Version usable for OGE 0.3.64 or later.
#
# This file is part of OGE (Open Game Engine)
# For the latest info, see http://sourceforge.net/projects/oge
# Copyright (c) 2008 The OGE Team
# Also see acknowledgements in Readme.txt
#

SET(Current_Lib
OgeScriptLua
)

SET(Current_Sources
src/OgeScriptManagerLua.cpp
src/OgeScriptPluginLua.cpp
)

SET(Current_Headers
include/oge/script/OgeScriptManagerLua.h
include/oge/script/OgeScriptPluginLua.h
include/oge/script/OgeScriptPrerequisitesLua.h
)

SET(Current_Include_Dirs
include
${CMAKE_SOURCE_DIR}/oge/utilities/include
${CMAKE_SOURCE_DIR}/oge/core/include
)

INCLUDE(DefineFlags)
FIND_PACKAGE(Lua)
FIND_PACKAGE(LuaBind)
FIND_PACKAGE(Poco)
FIND_PACKAGE(TBB)
FIND_PACKAGE(FastDelegates)
FIND_PACKAGE(ZLib)
FIND_PACKAGE(ZZip)

IF(LUA_FOUND AND Poco_FOUND AND TBB_FOUND AND FastDelegates_FOUND AND ZLib_FOUND AND ZZip_FOUND)

    INCLUDE_DIRECTORIES(${Current_Include_Dirs}  ${Poco_Foundation_INCLUDE_DIR} ${Poco_Util_INCLUDE_DIR}  ${TBB_INCLUDE_DIR} ${FastDelegates_INCLUDE_DIR} 
 ${TBB_INCLUDE_DIR} ${ZLib_INCLUDE_DIR} ${ZZip_INCLUDE_DIR} ${LUA_INCLUDE_DIR} ${LUA_BIND_INCLUDE_DIR} ${BOOST_INCLUDE_DIR})

  SOURCE_GROUP("Header Files" FILES ${Current_Headers})
  SOURCE_GROUP("Source Files" FILES ${Current_Sources})

  ADD_LIBRARY(${Current_Lib} SHARED ${Current_Sources} ${Current_Headers})

IF(OGE_LUA_STATIC)
  ADD_DEFINITIONS(/DOGE_SCRIPT_LUA_STATIC_LIB)
ENDIF()

IF(OGE_DOUBLE)
  ADD_DEFINITIONS(/DOGE_DOUBLE)
ENDIF()

  SET_TARGET_PROPERTIES(${Current_Lib} PROPERTIES DEFINE_SYMBOL OGE_SCRIPTLUA_EXPORTS)


  TARGET_LINK_LIBRARIES(${Current_Lib} OgeCore)
  TARGET_LINK_LIBRARIES(${Current_Lib} OgeUtilities)
  TARGET_LINK_LIBRARIES(${Current_Lib} debug ${LUA_LIBRARY_DBG} optimized ${LUA_LIBRARY})
  TARGET_LINK_LIBRARIES(${Current_Lib} debug ${LUA_BIND_LIBRARY_DBG} optimized ${LUA_BIND_LIBRARY})

  MESSAGE("Dependency for ${Current_Lib}: OK\n")
ELSE()
  MESSAGE("Dependency for ${Current_Lib}: FAIL\n")
ENDIF(LUA_FOUND AND Poco_FOUND AND TBB_FOUND AND FastDelegates_FOUND AND ZLib_FOUND AND ZZip_FOUND)
