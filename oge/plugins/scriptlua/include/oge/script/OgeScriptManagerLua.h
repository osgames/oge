/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_SCRIPTMANAGERLUA_H__
#define __OGE_SCRIPTMANAGERLUA_H__

#include "oge/script/OgeScriptPrerequisitesLua.h"
#include "oge/resource/OgeResourceControllerOgeImpl.h"
#include "oge/script/OgeScriptManager.h"
#include "oge/OgeSingleton.h"

// Lua
extern "C"
{
    #include "lua.h"
    #include "lualib.h"
    #include "lauxlib.h"
}

// luabind
#include "luabind/luabind.hpp"
#include "luabind/adopt_policy.hpp"
#include "luabind/out_value_policy.hpp"
#include "luabind/return_reference_to_policy.hpp"

namespace oge
{


    /**
     * @brief Manages and execute the lua scripts
     *
     * @author Steven GAY
     */
    class OGE_SCRIPT_API ScriptManagerLua : public ScriptManager, 
        public Singleton<ScriptManagerLua>
    {
    private:
        lua_State* mLuaState;

    public:

        static ScriptManagerLua* createSingleton();
        static void destroySingleton();
        static ScriptManagerLua* getSingletonPtr() { return mSingleton; }
        static ScriptManagerLua& getSingleton()
        {
            assert(mSingleton);
            return *mSingleton;
        }

        /// Create a new lua state and load additional libraries
        void openState();
        void closeState();
        /// Close the current lua state and calls openState()
        void resetState();

        /**
         * Execute a script file.
         * Able to execute pre-compiled chunks. It detects whether
         * the file is text or not, and loads it accordingly (see luac).
         */
        bool doFile( const String& script );
        /// Execute a script string.
        bool doString( const String& script );
        /// Execute a script from STDIN
        bool doStdin();
        /// Returns a dump of lua as a string
        String getStackDump(lua_State* state = 0);

        lua_State* getLuaState() { return mLuaState; }

         /**
         *  Return the dependancies libraries version
         *  This method is principally used for debugging on remote pc (testers)
         *  and determine if the appropriate libraries are present.
         */
        virtual const String getLibrariesVersion();

    protected:
        // System methods
        bool initialise();
        void shutdown();
        void tick(double currentTime);

        /**
         * Bind classes to lua.
         * It will defines lua functions that we can call
         * @note The classes are hard-coded in this method.
         */
        void bindClasses( lua_State* luaState );

/* Experimental code
        void call( const char* const function )
        {
            luabind::call_function<void>(mState, function);
        }

        template<typename T0>
        void call( const char * const function,
            const T0& parameter0 )
        {
            luabind::call_function<void>(mState, function, parameter0);
        }

        template<typename T0, typename T1>
        void call( const char* const function, const T0& parameter0,
            const T1& parameter1 )
        {
            luabind::call_function<void>( mState, function, parameter0,
                parameter1);
        }

        template<typename T0, typename T1, typename T2>
        void call(const char* const function, const T0& parameter0,
         const T1& parameter1, const T2& parameter2 )
        {
            luabind::call_function<void>( mState, function, parameter0,
                parameter1, parameter2);
        }

        template< typename T0, typename T1, typename T2, typename T3>
        void call(const char* const function, const T0& parameter0,
            const T1& parameter1, const T2& parameter2, const T3& parameter3 )
        {
            luabind::call_function<void>( mState, function, parameter0,
                parameter1, parameter2, parameter3);
        }
*/

    private:
        ScriptManagerLua();
        ~ScriptManagerLua();
    };
}
#endif // __OGE_SCRIPTMANAGERLUA_H__

/* Prospective code
*/