OGE Authors and Contributors
============================

This file is based on the similar file from the OGRE (Object-Oriented Graphics Rendering Engine) project.


OGE Team 
---------
 Steven GAY (lazaruslong aka steven on oge/ogre forums)             
 Christopher JONES (c_j (sourceforge) / Chris Jones (ogre/oge forums))
 Mehdi Toghianifar
 Alex Peterson (petrocket)

Ex-OGE Team
-----------
 Matt (Mibocote)
 Alkis (alkis_clio)
 Stephen (Muppet)


Feature Contributors
--------------------
The following list includes developers who have contributed features to OGE
and signed the Contributor Agreement:


Contributor                             Subject Areas 
-----------                             --------------------------------------
 Vincenzo Greco (Firecool)              DynamicLines


Others
------
The following people have been notable in their support of OGE through the
fixing of bugs, testing patches, and various other miscellaneous contributions:

 Forname Name (pseudo)
 
 Roger Zoellner (fraggleonlinux)  - linux fix, upgrade to ois 1.0
  


If we've forgotten you in the above list, please contact us!

See Readme.html in the Docs folder for more details, 
in particular informations on the media supplied with the demos.


-----------------------------------------------------------------------------
This file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2008 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------