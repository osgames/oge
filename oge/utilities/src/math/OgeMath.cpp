/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/math/OgeMath.h"
#include "oge/math/OgeAngle.h"

namespace oge
{
    const Real Math::PI = (Real) 3.14159265;
    const Real Math::TWO_PI = (Real) 6.2831853;
    const Real Math::HALF_PI = (Real) 1.570796325;
    const Real Math::SQRT_05 = (Real) 0.70710678118654752440084436210485;
    const Real Math::DEGREE2RADIAN = Math::PI / Real(180.0);
    const Real Math::RADIAN2DEGREE = Real(180.0) / Math::PI;

    //-------------------------------------------------------------------------
    bool Math::areEqual( Real a, Real b, Real tolerance )
    {
        if (fabs(b-a) <= tolerance)
            return true;
        else
            return false;
    }
    //-------------------------------------------------------------------------
    Radian Math::ACos(Real angle)
    {
        if ( angle > -1.0 )
        {
            if ( angle < 1.0 )
                return Radian(acos(angle));
            else
                return Radian(0.0);
        }
        else
        {
            return Radian(PI);
        }
    }
    //-------------------------------------------------------------------------
    Radian Math::ASin(Real angle)
    {
        if ( angle > -1.0 )
        {
            if ( angle < 1.0 )
                return Radian(asin(angle));
            else
                return Radian(HALF_PI);
        }
        else
        {
            return Radian(-HALF_PI);
        }
    }
    //-------------------------------------------------------------------------
    Radian Math::ATan(Real angle)
    {
        return Radian(atan(angle));
    }
    //-------------------------------------------------------------------------
    Radian Math::ATan2(Real angle1, Real angle2)
    {
        return Radian(atan2(angle1,angle2));
    }
    //-------------------------------------------------------------------------
    Real Math::invSqrt( Real x )
    {
        #ifdef OGRE_DOUBLE
        union { double f; unsigned long long ul; } y;
        y.f = x;
        y.ul = ( 0xBFCDD6A18F6A6F54 - y.ul ) >> 1;
        y.f = ((double) 0.5f) * y.f * ( ((double) 3.0f) - x * y.f * y.f );
        return y.f;
        #else
        union { float f; unsigned long ul; } y;
        y.f = x;
        y.ul = ( 0xBE6EB50C - y.ul ) >> 1;
        y.f = 0.5f * y.f * ( 3.0f - x * y.f * y.f );
        return y.f;
        #endif
    }
    //----------------------------------------------------------------------
    Real Math::round(Real value, unsigned int digits)
    {
        Real f = 1.0;

        while (digits--)
            f = f * (Real) 10.0;

        value *= f;

        if (value >= 0.0)
            value = (Real) std::floor(value + 0.5);
        else
            value = (Real) std::ceil(value - 0.5);

        value /= f;
        return value;
    }
    //----------------------------------------------------------------------
}
