/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/config/OgeXmlOptionReader.h"
#include "Poco/DOM/NodeFilter.h"
#include "Poco/DOM/NamedNodeMap.h"

namespace oge
{
    //-------------------------------------------------------------------------
    XmlOptionReader::XmlOptionReader(OptionReaderFactory* creator) : 
        OptionReader(creator), mXmlReader(0)
    {

    }
    //-------------------------------------------------------------------------
    XmlOptionReader::~XmlOptionReader()
    {
        cleanUp();
    }
    //-------------------------------------------------------------------------
    bool XmlOptionReader::parse(DataStreamPtr dataStream, OptionPtr rootOption)
    {
        cleanUp();

        mXmlReader = new XmlReader(dataStream, false);

        if (!rootOption || !mXmlReader->parse())
            return false;

        using namespace Poco::XML;

        Document* document = mXmlReader->getDocument();
        parseRoots(rootOption, document->firstChild(), false, true);

        return true;
    }
    //-------------------------------------------------------------------------
    void XmlOptionReader::parseRoots(OptionPtr option, 
        Poco::XML::Node* currentNode, bool initialOverrideStrict, 
        bool initialSetToModified)
    {
        using namespace Poco::XML;

        while(currentNode)
        {
            if (currentNode->nodeType() == Node::ELEMENT_NODE)
            {
                String name = currentNode->nodeName();
                StringUtil::toLowerCase(name);
                if (name == "config")
                {
                    parseConfig(option, currentNode, initialOverrideStrict, 
                        initialSetToModified);
                }
                else if (name == "configs")
                {
                    parseRoots(option, currentNode->firstChild(), 
                        initialOverrideStrict, initialSetToModified);
                }
            }

            currentNode = currentNode->nextSibling();
        }
    }
    //-------------------------------------------------------------------------
    void XmlOptionReader::parseConfig(OptionPtr option, 
        Poco::XML::Node* currentNode, bool overrideStrict, bool setToModified)
    {
        using namespace Poco::XML;

        String rootName = "";
        bool clearOptions = false;
        bool clearValues = false;

        NamedNodeMap* attributes = currentNode->attributes();
        unsigned long size = attributes->length();

        for (unsigned long index = 0; index < size; ++index)
        {
            Node* attribute = attributes->item(index);
            if (attribute)
            {
                String attrName = attribute->nodeName();
                String attrValue = attribute->nodeValue();

                if (attrValue != "")
                {
                    if (attrName == "root")
                    {
                        rootName = attrValue;
                    }
                    else if(attrName == "clearOptions")
                    {
                        clearOptions = StringUtil::toBool(attrValue);
                    }
                    else if (attrName == "clearValues")
                    {
                        clearValues = StringUtil::toBool(attrValue);
                    }
                    else if (attrName == "overrideStrict")
                    {
                        overrideStrict = StringUtil::toBool(attrValue);
                    }
                    else if (attrName == "setToModified")
                    {
                        setToModified = StringUtil::toBool(attrValue);
                    }
                }
            }
        }

        if (rootName != "")
            option = option->getOption(rootName, overrideStrict, setToModified);

        if (option)
        {
            if (clearOptions)
                option->removeAllOptions();

            if (clearValues)
                option->recursiveRemoveAllValues();

            parseNode(option, currentNode->firstChild(), overrideStrict, 
                setToModified);
        }

        attributes->release();
    }
    //-------------------------------------------------------------------------
    void XmlOptionReader::parseNode(OptionPtr option, 
        Poco::XML::Node* currentNode, bool overrideStrict, bool setToModified)
    {
        using namespace Poco::XML;

        if (!option)
            return;

        while(currentNode)
        {
            switch(currentNode->nodeType())
            {
            case Node::ELEMENT_NODE:
                {
                    OptionPtr newOption = option->getOption(
                        currentNode->nodeName(), overrideStrict, setToModified,
                        true);

                    newOption = parseAttributes(newOption, currentNode, 
                        overrideStrict, setToModified);

                    if (newOption)
                    {
                        parseNode(newOption, currentNode->firstChild(), 
                            overrideStrict, setToModified);
                    }
                }
                break;
            case Node::TEXT_NODE:
                {
                    String value = currentNode->nodeValue();
                    StringUtil::trimWhitespace(value);
                    if (value != "")
                        option->addValue(value, setToModified);
                }
                break;
            }

            currentNode = currentNode->nextSibling();
        }
    }
    //-------------------------------------------------------------------------
    OptionPtr XmlOptionReader::parseAttributes(OptionPtr option, 
        Poco::XML::Node* currentNode, bool overrideStrict, bool setToModified)
    {
        using namespace Poco::XML;
        NamedNodeMap* attributes = currentNode->attributes();

        Node* nameAttr = attributes->getNamedItem("name");
        if (nameAttr)
        {
            String nameValue = nameAttr->nodeValue();
            if (nameValue != "")
            {
                option = option->getOption(nameValue, overrideStrict, 
                    setToModified, true);
            }
        }

        if (option)
        {
            unsigned long size = attributes->length();
            for (unsigned long index = 0; index < size; ++index)
            {
                Node* attribute = attributes->item(index);
                if (attribute)
                {
                    String attrName = attribute->nodeName();
                    String attrValue = attribute->nodeValue();
                    
                    if (attrName != "name" && attrValue != "")
                    {
                        OptionPtr opt = option->getOption(attrName, 
                            overrideStrict, setToModified);

                        if (opt)
                            opt->addValue(attrValue, setToModified);
                    }
                }
            }
        }

        attributes->release();

        return option;
    }
    //-------------------------------------------------------------------------
    void XmlOptionReader::cleanUp()
    {
        if (mXmlReader)
        {
            delete mXmlReader;
            mXmlReader = 0;
        }
    }
    //-------------------------------------------------------------------------
    bool XmlOptionReaderFactory::canParse(DataStreamPtr dataStream)
    {
        size_t oldPos = dataStream->getPosition();
        dataStream->setPosition(0);

        TextReader reader(dataStream);
        String line;

        while(!reader.isAtEndOfStream())
        {
            line = reader.readLine();
            StringUtil::trimWhitespace(line);

            if (!line.empty())
                break;
        }

        bool found = (  line.find("<!--") != std::string::npos || 
                        line.find("<configs>") != std::string::npos ||
                        line.find("<config>") != std::string::npos);

        dataStream->setPosition(oldPos);

        return found;
    }
    //-------------------------------------------------------------------------
    OptionReader* XmlOptionReaderFactory::createReader()
    {
        return new XmlOptionReader(this);
    }
    //-------------------------------------------------------------------------
    void XmlOptionReaderFactory::destroyReader(OptionReader* reader)
    {
        if (reader)
            delete reader;
    }
    //-------------------------------------------------------------------------
}
