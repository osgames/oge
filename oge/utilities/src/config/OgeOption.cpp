/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/config/OgeOption.h"

namespace oge
{
    //-------------------------------------------------------------------------
    Option::Option(const String& name, bool strict) : mName(name),
        mStrict(strict)
    {

    }
    //-------------------------------------------------------------------------
    Option::~Option()
    {

    }
    //-------------------------------------------------------------------------
    size_t Option::getNumberOfValues() const
    {
        RWMutex::ScopedLock lock(mOptionMutex, false);
        return mValues.size();
    }
    //-------------------------------------------------------------------------
    void Option::addValue(const String& value, bool alterModified)
    {
        RWMutex::ScopedLock lock(mOptionMutex, true);
        mValues.push_back(value);

        if (alterModified)
            mValuesModified = true;
    }
    //-------------------------------------------------------------------------
    bool Option::setValue(const String& value, size_t index, bool alterModified)
    {
        RWMutex::ScopedLock lock(mOptionMutex, true);
        size_t size = mValues.size();

        if (size == index)
        {
            mValues.push_back(value);
        }
        else if (index < size)
        {
            mValues[index] = value;
        }
        else
        {
            return false;
        }

        if (alterModified)
            mValuesModified = true;

        return true;
    }
    //-------------------------------------------------------------------------
    String Option::getValue(size_t index) const
    {
        RWMutex::ScopedLock lock(mOptionMutex, false);

        if (index < mValues.size())
            return mValues[index];

        return StringUtil::BLANK;
    }
    //-------------------------------------------------------------------------
    void Option::removeValue(size_t index, bool alterModified)
    {
        RWMutex::ScopedLock lock(mOptionMutex, false);

        if (index < mValues.size())
        {
            ValueVector::iterator iter = mValues.begin();
            iter += index;
            mValues.erase(iter);

            if (alterModified)
                mValuesModified = true;
        }
    }
    //-------------------------------------------------------------------------
    void Option::removeAllValues(bool setModified)
    {
        RWMutex::ScopedLock lock(mOptionMutex, false);
        
        mValues.clear();
        mValuesModified = setModified;
    }
    //-------------------------------------------------------------------------
    void Option::recursiveRemoveAllValues(bool setModified)
    {
        RWMutex::ScopedLock lock(mOptionMutex, true);
        mValues.clear();

        OptionMap::iterator iter = mOptions.begin();
        for (; iter != mOptions.end(); ++iter)
        {
            iter->second->recursiveRemoveAllValues();
        }

        mValuesModified = setModified;
    }
    //-------------------------------------------------------------------------
    Option::ValueIterator Option::getValueIterator()
    {
        RWMutex::ScopedLock lock(mOptionMutex, false);
        return ValueIterator(mValues);
    }
    //-------------------------------------------------------------------------
    Option::ValueConstIterator Option::getValueIterator() const
    {
        RWMutex::ScopedLock lock(mOptionMutex, false);
        return ValueConstIterator(mValues);
    }
    //-------------------------------------------------------------------------
    Option::ValueVector Option::getValueVector() const
    {
        RWMutex::ScopedLock lock(mOptionMutex, false);
        return mValues;
    }
    //-------------------------------------------------------------------------
    OptionPtr Option::addOption(const String& name, bool strict, 
        bool alterModified)
    {
        RWMutex::ScopedLock lock(mOptionMutex, true);

        OptionMap::iterator iter = mOptions.find(name);
        if (iter != mOptions.end())
            return OptionPtr();

        OptionPtr option = new Option(name, strict);
        mOptions[name] = option;

        if (alterModified)
            mOptionsModified = true;

        return option;
    }
    //-------------------------------------------------------------------------
    bool Option::removeOption(const String& name, bool alterModified)
    {
        RWMutex::ScopedLock lock(mOptionMutex, true);

        OptionMap::iterator iter = mOptions.find(name);
        if (iter == mOptions.end())
            return false;

        mOptions.erase(iter);

        if (alterModified)
            mOptionsModified = true;

        return true;
    }
    //-------------------------------------------------------------------------
    void Option::removeAllOptions(bool setModified)
    {
        RWMutex::ScopedLock lock(mOptionMutex, true);

        mOptions.clear();
        mOptionsModified = setModified;
    }
    //-------------------------------------------------------------------------
    OptionPtr Option::getOption(const String& name, bool overrideStrict,
        bool alterModifiedIfCreated, bool ignoreSeperator)
    {
        {
            // change to SpinMutex?
            RWMutex::ScopedLock lock(mOptionMutex, false);

            OptionMap::iterator iter = mOptions.find(name);
            if (iter != mOptions.end())
            {
                return iter->second;
            }
        }

        if (!ignoreSeperator)
        {
            String::size_type sepPos = name.find('.');
            if (sepPos != String::npos)
            {
                String actualName = name.substr(0, sepPos);
                String subOptionName = 
                    name.substr(sepPos + 1, name.size() - sepPos - 1);

                OptionPtr nextOption = getOption(actualName, overrideStrict,
                    alterModifiedIfCreated);

                if (nextOption)
                {
                    return nextOption->getOption(subOptionName, overrideStrict, 
                        alterModifiedIfCreated);
                }

                return OptionPtr();
            }
        }
        
        if (!mStrict || overrideStrict)
        {
            OptionPtr newOption = addOption(name, mStrict, 
                alterModifiedIfCreated);

            if (newOption && alterModifiedIfCreated)
                mOptionsModified = true;

            return newOption;
        }

        return OptionPtr();
        
        //#########################################



        /*
        String::size_type sepPos = name.find('.');
                if (sepPos == String::npos)
                {
                    {
                        RWMutex::ScopedLock lock(mOptionMutex, false);
        
                        OptionMap::iterator iter = mOptions.find(name);
                        if (iter != mOptions.end())
                        {
                            return iter->second;
                        }
                    }
        
                    if (!mStrict || overrideStrict)
                    {
                        OptionPtr newOption = addOption(name, mStrict, 
                            alterModifiedIfCreated);
        
                        if (newOption && alterModifiedIfCreated)
                            mOptionsModified = true;
        
                        return newOption;
                    }
                }
                else
                {
                    String actualName = name.substr(0, sepPos);
                    String subOptionName = 
                        name.substr(sepPos + 1, name.size() - sepPos - 1);
        
                    OptionPtr nextOption = getOption(actualName, overrideStrict,
                        alterModifiedIfCreated);
        
                    if (nextOption)
                    {
                        return nextOption->getOption(subOptionName, overrideStrict, 
                            alterModifiedIfCreated);
                    }
                }
        
                return OptionPtr();*/
        
    }
    //-------------------------------------------------------------------------
    const OptionPtr Option::getOption(const String& name) const
    {
        String::size_type sepPos = name.find('.');
        if (sepPos == String::npos)
        {
            {
                RWMutex::ScopedLock lock(mOptionMutex, false);

                OptionMap::const_iterator iter = mOptions.find(name);
                if (iter != mOptions.end())
                {
                    return iter->second;
                }
            }
        }
        else
        {
            String actualName = name.substr(0, sepPos);
            String subOptionName = 
                name.substr(sepPos + 1, name.size() - sepPos - 1);

            OptionPtr nextOption = getOption(actualName);
            if (nextOption)
            {
                return nextOption->getOption(subOptionName);
            }
        }

        return OptionPtr();
    }
    //-------------------------------------------------------------------------
    const OptionPtr Option::getFirstOption() const
    {
        if (mOptions.empty())
            return OptionPtr();
        RWMutex::ScopedLock lock(mOptionMutex, false);
        Option::ConstIterator iter = this->getOptionIterator();
        return iter.getOption();
    }
    //-------------------------------------------------------------------------
    Option::Iterator Option::getOptionIterator()
    {
        RWMutex::ScopedLock lock(mOptionMutex, false);
        return Iterator(mOptions);
    }
    //-------------------------------------------------------------------------
    Option::ConstIterator Option::getOptionIterator() const
    {
        RWMutex::ScopedLock lock(mOptionMutex, false);
        return ConstIterator(mOptions);
    }
    //-------------------------------------------------------------------------
    Option::OptionMap Option::getOptionMap()
    {
        RWMutex::ScopedLock lock(mOptionMutex, false);
        return mOptions;
    }
    //-------------------------------------------------------------------------
    String Option::getOptionValue(const String& name, size_t index,
        bool overrideStrict, bool alterModifiedIfCreated, bool ignoreSeparator)
    {
        OptionPtr option = getOption(name, overrideStrict, 
            alterModifiedIfCreated, ignoreSeparator);

        if (!option)
            return "";

        return option->getValue(index);
    }
    //-------------------------------------------------------------------------
    String Option::getOptionValue(const String& name, size_t index) const
    {
        OptionPtr option = getOption(name);

        if (!option)
            return "";

        return option->getValue(index);
    }
    //-------------------------------------------------------------------------
    bool Option::isOptionsModified() const
    {
        RWMutex::ScopedLock lock(mOptionMutex, false);
        return mOptionsModified;
    }
    //-------------------------------------------------------------------------
    bool Option::isValuesModified() const
    {
        RWMutex::ScopedLock lock(mOptionMutex, false);
        return mValuesModified;
    }
    //-------------------------------------------------------------------------
    void Option::setOptionsModified(bool modified)
    {
        RWMutex::ScopedLock lock(mOptionMutex, true);
        mOptionsModified = modified;
    }
    //-------------------------------------------------------------------------
    void Option::setValuesModified(bool modified)
    {
        RWMutex::ScopedLock lock(mOptionMutex, true);
        mValuesModified = modified;
    }
    //-------------------------------------------------------------------------
    Option::Iterator::Iterator(OptionMap& options) : mOptions(options) 
    {
        mIter = mOptions.begin();
    }
    //-------------------------------------------------------------------------
    Option::Iterator::Iterator(OptionMap& options, 
        OptionMap::iterator iter) : mOptions(options), mIter(iter) { }
    //-------------------------------------------------------------------------
    void Option::Iterator::begin() { mIter = mOptions.begin(); }
    //-------------------------------------------------------------------------
    bool Option::Iterator::hasNext() { return mIter != mOptions.end(); }
    //-------------------------------------------------------------------------
    void Option::Iterator::next() { ++mIter; }
    //-------------------------------------------------------------------------
    OptionPtr Option::Iterator::getOption() { return mIter->second; }
    //-------------------------------------------------------------------------
    String Option::Iterator::getName() { return mIter->first; }
    //-------------------------------------------------------------------------
    Option::ConstIterator::ConstIterator(const OptionMap& options) : 
        mOptions(options) 
    {
        mIter = mOptions.begin();
    }
    //-------------------------------------------------------------------------
    Option::ConstIterator::ConstIterator(const OptionMap& options, 
        OptionMap::const_iterator iter) : mOptions(options), mIter(iter) { }
    //-------------------------------------------------------------------------
    void Option::ConstIterator::begin() { mIter = mOptions.begin(); }
    //-------------------------------------------------------------------------
    bool Option::ConstIterator::hasNext() { return mIter != mOptions.end(); }
    //-------------------------------------------------------------------------
    void Option::ConstIterator::next() { ++mIter; }
    //-------------------------------------------------------------------------
    OptionPtr Option::ConstIterator::getOption() { return mIter->second; }
    //-------------------------------------------------------------------------
    String Option::ConstIterator::getName() { return mIter->first; }
    //-------------------------------------------------------------------------
    Option::ValueIterator::ValueIterator(ValueVector& values) : mValues(values)
    {
        mIter = mValues.begin();
    }
    //-------------------------------------------------------------------------
    Option::ValueIterator::ValueIterator(ValueVector& values, 
        ValueVector::iterator iter) : mValues(values), mIter(iter) { }
    //-------------------------------------------------------------------------
    void Option::ValueIterator::begin() { mIter = mValues.begin(); }
    //-------------------------------------------------------------------------
    bool Option::ValueIterator::hasNext() { return mIter != mValues.end(); }
    //-------------------------------------------------------------------------
    void Option::ValueIterator::next() { ++mIter; }
    //-------------------------------------------------------------------------
    String Option::ValueIterator::getValue() { return *mIter; }
    //-------------------------------------------------------------------------
    Option::ValueConstIterator::ValueConstIterator(const ValueVector& values) : 
        mValues(values)
    {
        mIter = mValues.begin();
    }
    //-------------------------------------------------------------------------
    Option::ValueConstIterator::ValueConstIterator(const ValueVector& values, 
        ValueVector::const_iterator iter) : mValues(values), mIter(iter) { }
    //-------------------------------------------------------------------------
    void Option::ValueConstIterator::begin() { mIter = mValues.begin(); }
    //-------------------------------------------------------------------------
    bool Option::ValueConstIterator::hasNext() { return mIter != mValues.end(); }
    //-------------------------------------------------------------------------
    void Option::ValueConstIterator::next() { ++mIter; }
    //-------------------------------------------------------------------------
    String Option::ValueConstIterator::getValue() { return *mIter; }
    //-------------------------------------------------------------------------
}

