/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/config/OgeOptionManager.h"
#include "oge/config/OgeXmlOptionReader.h"
#include "oge/datastreams/OgeFileSystemStream.h"

namespace oge
{
    //-------------------------------------------------------------------------
    template<> OptionManager* Singleton<OptionManager>::mSingleton = 0;
    OptionManager* OptionManager::getSingletonPtr()
    {
        return mSingleton;
    }
    OptionManager& OptionManager::getSingleton()
    {
        assert(mSingleton);
        return *mSingleton;
    }
    
    //-------------------------------------------------------------------------
    OptionManager::OptionManager()
    {
        mRootOption = new Option("OptionManager", true);
    }
    //-------------------------------------------------------------------------
    OptionManager::~OptionManager()
    {
        
    }
    //-------------------------------------------------------------------------
    void OptionManager::initialise()
    {
        OptionReaderFactory* factory = new XmlOptionReaderFactory();
        if (!registerOptionReaderFactory(factory))
        {
            delete factory;
        }

        // TODO: Remove all hard coded extensions
        registerFileExtension("xml", "Xml");
    }
    //-------------------------------------------------------------------------
    void OptionManager::shutdown()
    {
        // delete any remaining factories
        OptionReaderFactoryMap::iterator facIter = mOptionReaderFactories.begin();
        for(; facIter != mOptionReaderFactories.end(); ++facIter)
        {
            delete facIter->second;
        }

        mOptionReaderFactories.clear();
    }
    //-------------------------------------------------------------------------
    OptionPtr OptionManager::addOption(const String& name, bool strict, 
        bool alterModified)
    {
        return mRootOption->addOption(name, strict, alterModified);
    }
    //-------------------------------------------------------------------------
    bool OptionManager::removeOption(const String& name, bool alterModified)
    {
        return mRootOption->removeOption(name, alterModified);
    }
    //-------------------------------------------------------------------------
    void OptionManager::removeAllOptions(bool setModified)
    {
        mRootOption->removeAllOptions(setModified);
    }
    //-------------------------------------------------------------------------
    OptionPtr OptionManager::getOption(const String& name, bool overrideStrict,
        bool alterModifiedIfCreated)
    {
        return mRootOption->getOption(name, overrideStrict, 
            alterModifiedIfCreated);
    }
    //-------------------------------------------------------------------------
    const OptionPtr OptionManager::getOption(const String& name) const
    {
        return mRootOption->getOption(name);
    }
    //-------------------------------------------------------------------------
    Option::Iterator OptionManager::getOptionIterator()
    {
        return mRootOption->getOptionIterator();
    }
    //-------------------------------------------------------------------------
    Option::ConstIterator OptionManager::getOptionIterator() const
    {
        return mRootOption->getOptionIterator();
    }
    //-------------------------------------------------------------------------
    Option::OptionMap OptionManager::getOptionMap()
    {
        return mRootOption->getOptionMap();
    }
    //-------------------------------------------------------------------------
    bool OptionManager::registerOptionReaderFactory(
        OptionReaderFactory* factory)
    {
        Mutex::ScopedLock lock(mOptionFactoriesMutex);

        const String& type = factory->getType();
        if (mOptionReaderFactories.find(type) != 
            mOptionReaderFactories.end())
            return false;

        mOptionReaderFactories[type] = factory;

        return true;
    }
    //-------------------------------------------------------------------------
    void OptionManager::unregisterOptionReaderFactory(const String& type)
    {
        Mutex::ScopedLock lock(mOptionFactoriesMutex);

        OptionReaderFactoryMap::iterator iter = 
            mOptionReaderFactories.find(type);

        if (iter != mOptionReaderFactories.end())
            mOptionReaderFactories.erase(iter);
    }
    //-------------------------------------------------------------------------
    void OptionManager::unregisterOptionReaderFactory(
        OptionReaderFactory* factory)
    {
        Mutex::ScopedLock lock(mOptionFactoriesMutex);

        OptionReaderFactoryMap::iterator facIter = mOptionReaderFactories.begin();
        for(; facIter != mOptionReaderFactories.end(); ++facIter)
        {
            if (facIter->second == factory)
            {
                mOptionReaderFactories.erase(facIter++);
                
                OptionReaderFactoryMap::iterator extIter = 
                    mReaderFileExtensions.begin();
                while(extIter != mReaderFileExtensions.end())
                {
                    if (extIter->second == factory)
                    {
                        mReaderFileExtensions.erase(extIter++);
                    }
                    else
                    {
                        ++extIter;
                    }
                }

                return;
            }
        }
    }
    //-------------------------------------------------------------------------
    bool OptionManager::registerFileExtension(const String& extension, 
        const String& type)
    {
        Mutex::ScopedLock lock(mOptionFactoriesMutex);

        if (mReaderFileExtensions.find(extension) != 
            mReaderFileExtensions.end())
        {
            return false;
        }

        OptionReaderFactoryMap::iterator iter = 
            mOptionReaderFactories.find(type);

        if (iter == mOptionReaderFactories.end())
            return false;

        mReaderFileExtensions[extension] = iter->second;
        return true;
    }
    //-------------------------------------------------------------------------
    void OptionManager::unregisterFileExtension(const String& extension)
    {
        Mutex::ScopedLock lock(mOptionFactoriesMutex);

        OptionReaderFactoryMap::iterator iter = 
            mReaderFileExtensions.find(extension);

        if (iter != mReaderFileExtensions.end())
            mReaderFileExtensions.erase(iter);
    }
    //-------------------------------------------------------------------------
    bool OptionManager::loadFromFilename(const String& filename, 
        const String& type)
    {
        FileSystemStream* stream = new FileSystemStream();
        if (!stream->openRead(filename))
        {
            stream->close();
            delete stream;
            return false;
        }

        return loadFromDataStream(DataStreamPtr(stream), type);
    }
    //-------------------------------------------------------------------------
    bool OptionManager::loadFromDataStream(DataStreamPtr dataStream, 
        const String& type)
    {
        OptionReader* reader = createReaderFromType(type);
        if (!reader)
            return false;

        bool success = reader->parse(dataStream, mRootOption);
        reader->getCreator()->destroyReader(reader);

        return success;
    }
    //-------------------------------------------------------------------------
    bool OptionManager::loadFromFilename(const String& filename)
    {
        FileSystemStream* stream = new FileSystemStream();
        if (!stream->openRead(filename))
        {
            stream->close();
            delete stream;
            return false;
        }

        // transfer of ownership
        DataStreamPtr dataStream(stream);

        Path path = filename;
        OptionReader* reader = createReaderFromExtension(path.getExtension());
        
        // no factory registered for the given extension
        if (!reader)
        {
            reader = createReaderFromContent(dataStream);
        }

        if (!reader)
            return false;

        bool success = reader->parse(dataStream, mRootOption);
        reader->getCreator()->destroyReader(reader);

        return success;
    }
    //-------------------------------------------------------------------------
    OptionReader* OptionManager::createReaderFromType(const String& type)
    {
        Mutex::ScopedLock lock(mOptionFactoriesMutex);

        OptionReaderFactoryMap::iterator iter = 
            mOptionReaderFactories.find(type);

        if (iter == mOptionReaderFactories.end())
            return 0;

        return iter->second->createReader();
    }
    //-------------------------------------------------------------------------
    OptionReader* OptionManager::createReaderFromExtension(
        const String& extension)
    {
        Mutex::ScopedLock lock(mOptionFactoriesMutex);

        OptionReaderFactoryMap::iterator iter = 
            mReaderFileExtensions.find(extension);

        if (iter == mReaderFileExtensions.end())
            return 0;

        return iter->second->createReader();
    }
    //-------------------------------------------------------------------------
    OptionReader* OptionManager::createReaderFromContent(
        DataStreamPtr dataStream)
    {
        Mutex::ScopedLock lock(mOptionFactoriesMutex);

        OptionReaderFactoryMap::iterator iter = mOptionReaderFactories.begin();

        for (; iter != mOptionReaderFactories.end(); ++iter)
        {
            if (iter->second->canParse(dataStream))
            {
                return iter->second->createReader();
            }
        }

        return 0;
    }
    //-------------------------------------------------------------------------
    OptionManager* OptionManager::createSingleton()
    {
        if (!mSingleton)
            new OptionManager();

        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void OptionManager::destroySingleton()
    {
        if (mSingleton)
            delete mSingleton;
    }
    //-------------------------------------------------------------------------
}
