/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/hardware/OgeCPUDetect.h"

namespace oge
{
    //-------------------------------------------------------------------------
    bool CPUDetect::isCPUIDSupported()
    {
        static bool supported = _isCPUIDSupported();
        return supported;
    }
    //-------------------------------------------------------------------------
    bool CPUDetect::_isCPUIDSupported()
    {
#if OGE_ARCHITECTURE == OGE_ARCHITECTURE_64
        return true; // All x86/64, 64bit CPU's support CPUID
#else        
        oge::UInt32 originalEFLAGS = 0;
        oge::UInt32 modifiedEFLAGS = 0;

#   if OGE_COMPILER == OGE_COMPILER_MSVC
        // If bit 21 of the EFLAGS register can be written to, 
        // CPUID is supported
        __asm
        {
            // Make a copy of the EFLAGS register onto the stack
            pushfd
            // Pop EFLAGS from the stack into EAX
            pop     eax
            // EAX is to be modified, ECX is to compare EAX against
            mov     ecx, eax
            // Invert bit 21
            xor     eax, 0x200000
            // Push EAX back onto the stack
            push    eax
            // Write to the EFLAGS register with the value popped from the stack
            popfd
            // Make another copy of EFLAGS register to compare against ECX
            pushfd
            // Copy EFLAGS from the stack into EAX
            pop     eax
            // Copy the original and modified values for EFLAGS
            mov     originalEFLAGS, ecx
            mov     modifiedEFLAGS, eax
            // Push the original EFLAGS value onto the stack
            push    ecx
            // Write to EFLAGS with the value from the stack
            popfd
        }
#elif OGE_COMPILER == OGE_COMPILER_GNUC
        // Same as the MSVC version but for GNUC
        __asm__
        (
            "pushfl         \n\t"
            "pop    %0      \n\t"
            "mov    %0, %1  \n\t"
            "xor    %2, %0  \n\t"
            "push   %0      \n\t"
            "popfl          \n\t"
            "pushfl         \n\t"
            "pop    %0      \n\t"
            "push   %1      \n\t"
            "popfl          \n\t"
            : "=r" (originalEFLAGS), "=r" (modifiedEFLAGS)
            : "n" (0x200000)
        );
#   endif // Compiler Dependent Code
        // If the two are different, CPUID is supported
        return originalEFLAGS != modifiedEFLAGS;
#endif // 32 bit architecture
    }
    //-------------------------------------------------------------------------
    CPUIDResult CPUDetect::cpuid(int func, bool useECX, int ecxInput)
    {
#if OGE_COMPILER == OGE_COMPILER_MSVC
        // VC8 and above has __cpuid, VC9 SP1 has __cpuidex
    #if OGE_COMP_VER_FULL >= OGE_COMPILER_MSVC_9_SP1
        int reg[4] = {0};
        if (useECX)
        {
            //__cpuidex(reg, func, ecxInput);
            __cpuid(reg, func);
        }
        else
        {
            __cpuid(reg, func);
        }

        return CPUIDResult(reg);
    #elif OGE_COMP_VER >= OGE_COMPILER_MSVC_8 && OGE_ARCHITECTURE == OGE_ARCHITECTURE_64
        // 64 bit VC8 only supports basic __cpuid intrinsic
        if (useECX)
            return CPUIDResult(false);

        int reg[4] = {0};
        __cpuid(reg, func);
        return CPUIDResult(reg);
    #elif OGE_ARCHITECTURE == OGE_ARCHITECTURE_32
        int tempA, tempB, tempC, tempD;
        __asm
        {
            mov	    eax, func
            mov     ebx, 0
            mov     ecx, ecxInput
            mov     edx, 0
            cpuid
            mov     tempA, eax
            mov     tempB, ebx
            mov     tempC, ecx
            mov     tempD, edx
        }

        int reg[4] = {tempA, tempB, tempC, tempD};
        return CPUIDResult(reg);
    #else
        #define LINE_NUM_STR2(x) #x
        #define LINE_NUM_STR(x) LINE_NUM_STR2(x)
        #pragma message(__FILE__ "(" LINE_NUM_STR(__LINE__) ") CPUID cannot be used with the current compiler and settings")
        // No corresponding cpuid operation available
        return CPUIDResult(false);
    #endif
#elif OGE_COMPILER == OGE_COMPILER_GNUC
        int reg[4] = {0};
        __asm__ __volatile__ 
        (
            "cpuid            \n\t"
            : "=a"(reg[0]), "=b"(reg[1]), "=c"(reg[2]), "=d"(reg[3])
            : "a" (func), "c" (ecxInput)
            : "cc"
        );

        return CPUIDResult(reg);
#else
        // Compiler unsupported
        return CPUIDResult(false);
#endif
    }
    //-------------------------------------------------------------------------
    int CPUDetect::getMaxCPUIDFunc()
    {
        static int max = _getMaxCPUIDFunc();
        return max;
    }
    //-------------------------------------------------------------------------
    int CPUDetect::_getMaxCPUIDFunc()
    {
        CPUIDResult result = cpuid(0);
        if (!result.valid)
            return -1;

        return result.eax.get();
    }
    //-------------------------------------------------------------------------
    const String& CPUDetect::getVendorID()
    {
        static String id = _getVendorID();
        return id;
    }
    //-------------------------------------------------------------------------
    String CPUDetect::_getVendorID()
    {
        if (!isCPUIDSupported())
            return "";

        CPUIDResult result = CPUDetect::cpuid(0);
        return  String((char*)&result.ebx, 4) + 
            String((char*)&result.edx, 4) + 
            String((char*)&result.ecx, 4);
    }
    //-------------------------------------------------------------------------
    bool CPUDetect::isMultithreaded()
    {
        static bool mt = _isMultithreaded();
        return mt;
    }
    //-------------------------------------------------------------------------
    bool CPUDetect::_isMultithreaded()
    {
        if (getVendorID() != "GenuineIntel")
            return false;

        CPUIDResult result = cpuid(1);
        if (!result.valid)
            return false;

        return result.edx[CPUID_HT] != 0;
    }
    //-------------------------------------------------------------------------
    const CPUInfo& CPUDetect::getCPUInfo()
    {
        static CPUInfo info = _getCPUInfo();
        return info;
    }
    //-------------------------------------------------------------------------
    CPUInfo CPUDetect::_getCPUInfo()
    {
        CPUInfo info;
        info.vendor = getVendorID();
        if (!_fillProcCountsPrimary(info) && !_fillProcCountsSecondary(info))
        {
            info.packageCount = info.coreCount = info.logicalCount = 1;
            info.cpuDetectMethod = CPUInfo::DM_FAIL;
        }
        
        info.HT = info.logicalCount > info.coreCount;
        
        return info;
    }
    //-------------------------------------------------------------------------
    bool CPUDetect::_fillProcCountsPrimary(CPUInfo& info)
    {
#if OGE_PLATFORM == OGE_PLATFORM_WIN32 && OGE_COMP_VER >= OGE_COMPILER_MSVC_9
        typedef BOOL (WINAPI *GLPIFn)(
            PSYSTEM_LOGICAL_PROCESSOR_INFORMATION, PDWORD);

        GLPIFn func = (GLPIFn)GetProcAddress(GetModuleHandle(TEXT("kernel32")), 
            "GetLogicalProcessorInformation");

        if (func == 0)
            return false;

        bool done = false;
        PSYSTEM_LOGICAL_PROCESSOR_INFORMATION buffer = NULL;
        PSYSTEM_LOGICAL_PROCESSOR_INFORMATION ptr = NULL;
        DWORD returnLength = 0;
        DWORD byteOffset = 0;

        while(true)
        {
            if (func(buffer, &returnLength) == TRUE) 
                break;

            if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) 
            {
                if (buffer)
                    free(buffer);

                buffer = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)malloc(
                        returnLength);

                if (buffer == NULL) 
                    return false;
            } 
            else
            {
                return false;
            }
        }

        ptr = buffer;
        info.packageCount = info.coreCount = info.logicalCount = 0;
            
        size_t sizeOfStruct = sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
        while (byteOffset + sizeOfStruct <= returnLength) 
        {
            switch (ptr->Relationship) 
            {
            case RelationProcessorCore:
                info.coreCount++;
                info.logicalCount += _utilCountSetBits(ptr->ProcessorMask);
                break;
            case RelationProcessorPackage:
                info.packageCount++;
                break;
            }

            byteOffset += sizeOfStruct;
            ptr++;
        }

        free(buffer);
#elif OGE_PLATFORM == OGE_PLATFORM_LINUX
        typedef std::map<String, String> StringMap;
        typedef std::map<int, StringMap> IntStringMap;
        IntStringMap logicalProcessors;

        typedef std::map<int, int> IntMap;
        typedef std::map<int, IntMap> IntMapMap;
        IntMapMap coreIDs;
        
        std::ifstream file("/proc/cpuinfo");

        if (!file.is_open())
            return false;

        const int bufSize = 512;
        char buffer[bufSize] = {0};

        int currentLogical = 0;

        while(!file.eof())
        {
            file.getline(buffer, bufSize - 1, 10);
            String line = buffer;

            String::size_type split = line.find("\t");

            if (split != String::npos)
            {
                String name = line.substr(0, split);

                split = line.find(": ");
                String value = line.substr(split + 2, line.length());

                if (name == "processor")
                {
                    currentLogical = atoi(value.c_str());
                    if (logicalProcessors.find(currentLogical) == logicalProcessors.end())
                    {
                        logicalProcessors[currentLogical] = StringMap();
                    }
                }
                else
                {
                    logicalProcessors[currentLogical][name] = value;
                }
            }
        }

        file.close();

        size_t packageCount = 0;
        size_t coreCount = 0;
        size_t logicalCount = logicalProcessors.size();

        if (logicalCount > 1)
        {
            for (size_t curLogical = 0; curLogical < logicalCount; ++curLogical)
            {
                String coreIDStr = logicalProcessors[curLogical]["core id"];
                String packageIDStr = logicalProcessors[curLogical]["physical id"];

                if (coreIDStr != "" && packageIDStr != "")
                {
                    int coreID = atoi(coreIDStr.c_str());
                    int packageID = atoi(packageIDStr.c_str());

                    IntMap& pID = coreIDs[packageID];

                    if (pID.find(coreID) == pID.end())
                    {
                        pID[coreID] = 1;
                    }
                    else
                    {
                        pID[coreID]++;
                    }
                }
            }

            packageCount = coreIDs.size();

            for (size_t index = 0; index < packageCount; ++index)
            {
                coreCount += coreIDs[index].size();
            }
        }
        else
        {
            packageCount = coreCount = 1;
        }

        info.packageCount = packageCount;
        info.coreCount = coreCount;
        info.logicalCount = logicalCount;
#else
        return false;
#endif

        info.cpuDetectMethod = CPUInfo::DM_PRIMARY;
        return true;
    }
    //-------------------------------------------------------------------------
    bool CPUDetect::_fillProcCountsSecondary(CPUInfo& info)
    {
        info.logicalCount = _detectLogicalProcessorCount();
        size_t logicalPerCore = _detectLogicalPerCore();

        // Couldn't detect the number of logical processors
        if (info.logicalCount == 0 || logicalPerCore == 0)
            return false;

        info.coreCount = info.logicalCount / logicalPerCore;
        info.packageCount = 1; // TODO: How to detect this?

        info.cpuDetectMethod = CPUInfo::DM_SECONDARY;
        return true;
    }
    //-------------------------------------------------------------------------
    size_t CPUDetect::_detectLogicalProcessorCount()
    {
#if OGE_PLATFORM == OGE_PLATFORM_WIN32
        SYSTEM_INFO systemInfo;
        GetSystemInfo(&systemInfo);
        return systemInfo.dwNumberOfProcessors;
#elif OGE_PLATFORM == OGE_PLATFORM_LINUX
        return sysconf(_SC_NPROCESSORS_CONF);
#else
        return 1;
#endif
    }
    //-------------------------------------------------------------------------
    size_t CPUDetect::_detectLogicalPerCore()
    {
        if (getVendorID() == "GenuineIntel")
        {
            UInt32 MASK_CORE_BITS       = 0xFC000000;
            UInt32 MASK_LOGICAL_BITS    = 0x00FF0000;

            // The CPU doesn't not support multiple hardware threads
            if (!isMultithreaded())
                return 1;

            // CPUID function of 4 is required to retrieve the necessary 
            // information. If less than 4, Hyper-Threading is enabled
            if (getMaxCPUIDFunc() < 4)
                return 2;

            // Query the CPU for the max number of processor cores
            // cpuid with EAX == 4, ECX == 0
            CPUIDResult coreResult = cpuid(4, true, 0);
            // Query the CPU for the max number of logical processors
            CPUIDResult logicalResult = cpuid(1);
            // Make sure each results are valid, if not guess that
            // Hyper-Threading is enabled as the CPU supports multiple
            // hardware threads
            if (!coreResult.valid || !logicalResult.valid)
                return 2;

            // Calculate the maximum number of cores and logical processors
            // from the results of the queries
            size_t maxCores = ((coreResult.eax.get() & MASK_CORE_BITS) >> 26) + 1;
            size_t maxLogical = ((logicalResult.ebx.get() & MASK_LOGICAL_BITS) >> 16);

            // Calculate the number of logical processors per core
            return maxLogical / maxCores;
        }

        // Not an Intel CPU, doesn't support Hyper-Threading
        return 1;
    }
    //-------------------------------------------------------------------------
    UInt32 CPUDetect::_utilCountSetBits(UInt64 value)
    {
        UInt32 shift = sizeof(UInt64) * 8 - 1;
        UInt32 count = 0;
        UInt64 test = (UInt64)1 << shift;    
        
        for (UInt32 index = 0; index <= shift; ++index)
        {
            count += (value & test) ? 1 : 0;
            test /= 2;
        }

        return count;
    }
    //-------------------------------------------------------------------------
}
