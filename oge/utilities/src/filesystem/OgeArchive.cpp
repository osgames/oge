/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/filesystem/OgeArchive.h"

namespace oge
{
    Archive::Archive(const String& name, const String& type, bool allowRead, 
        bool allowWrite) : mName(name), mType(type), mCanRead(allowRead), 
        mCanWrite(allowWrite)
    {
    }
    //-------------------------------------------------------------------------
    Archive::~Archive()
    {
    }
    //-------------------------------------------------------------------------
    FilePtr Archive::getFile(const String& filename)
    {
        RWMutex::ScopedLock lock(mFilesMutex, false);

        FileMap::iterator iter = mFiles.find(filename);
        if (iter != mFiles.end())
            return iter->second;

        return FilePtr();
    }
    //-------------------------------------------------------------------------
    bool Archive::containsFile(const String& filename)
    {
        RWMutex::ScopedLock lock(mFilesMutex, false);

        FileMap::iterator iter = mFiles.find(filename);
        if (iter != mFiles.end())
            return true;

        return false;
    }
    //-------------------------------------------------------------------------
    StringVector Archive::getFilenames()
    {
        RWMutex::ScopedLock lock(mFilesMutex, false);
        StringVector list(mFiles.size());

        FileMap::iterator iter = mFiles.begin();
        for (int index = 0; iter != mFiles.end(); ++iter, ++index)
        {
            list[index] = iter->first;
        }

        return list;
    }
    //-------------------------------------------------------------------------
    FileVector Archive::getFiles()
    {
        RWMutex::ScopedLock lock(mFilesMutex, false);
        FileVector list(mFiles.size());

        FileMap::iterator iter = mFiles.begin();
        for (int index = 0; iter != mFiles.end(); ++iter, ++index)
        {
            list[index] = iter->second;
        }

        return list;
    }
    //-------------------------------------------------------------------------
    StringVector Archive::findFilenames(const String& pattern)
    {
        StringVector list;
        Glob glob(pattern);
        
        RWMutex::ScopedLock lock(mFilesMutex, false);

        FileMap::iterator iter = mFiles.begin();
        for (; iter != mFiles.end(); ++iter)
        {
            const String& filename = iter->first;
            if (glob.match(filename))
            {
                list.push_back(filename);
            }
        }

        return list;
    }
    //-------------------------------------------------------------------------
    FileVector Archive::findFiles(const String& pattern)
    {
        FileVector list;
        Glob glob(pattern);
        
        RWMutex::ScopedLock lock(mFilesMutex, false);

        FileMap::iterator iter = mFiles.begin();
        for (; iter != mFiles.end(); ++iter)
        {
            const String& filename = iter->first;
            if (glob.match(filename))
            {
                list.push_back(iter->second);
            }
        }

        return list;
    }
    //-------------------------------------------------------------------------
}
