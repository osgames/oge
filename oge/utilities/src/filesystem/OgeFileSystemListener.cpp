/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/

#include "oge/filesystem/OgeFileSystemListener.h"

namespace oge
{
    //-------------------------------------------------------------------------
    void FileSystemListenerGroup::addListener(FileSystemListener* listener)
    {
        Mutex::ScopedLock lock(mMutex);
        mListeners.push_back(listener);
    }
    //-------------------------------------------------------------------------
    void FileSystemListenerGroup::removeListener(FileSystemListener* listener)
    {
        Mutex::ScopedLock lock(mMutex);

        ListenerVector::iterator iter = mListeners.begin();
        for(; iter != mListeners.end(); ++iter)
        {
            if (*iter == listener)
            {
                mListeners.erase(iter);
                return;
            }
        }
    }
    //-------------------------------------------------------------------------
    void FileSystemListenerGroup::notifyArchiveAdded(const String& name)
    {
        Mutex::ScopedLock lock(mMutex);

        size_t count = mListeners.size();
        for(size_t index = 0; index < count; ++index)
        {
            mListeners[index]->notifyArchiveAdded(name);
        }
    }
    //-------------------------------------------------------------------------
    void FileSystemListenerGroup::notifyArchiveRemoved(const String& name)
    {
        Mutex::ScopedLock lock(mMutex);

        size_t count = mListeners.size();
        for(size_t index = 0; index < count; ++index)
        {
            mListeners[index]->notifyArchiveRemoved(name);
        }
    }
    //-------------------------------------------------------------------------
    void FileSystemListenerGroup::notifyArchiveGroupAdded(const String& name)
    {
        Mutex::ScopedLock lock(mMutex);

        size_t count = mListeners.size();
        for(size_t index = 0; index < count; ++index)
        {
            mListeners[index]->notifyArchiveGroupAdded(name);
        }
    }
    //-------------------------------------------------------------------------
    void FileSystemListenerGroup::notifyArchiveGroupRemoved(
        const String& name)
    {
        Mutex::ScopedLock lock(mMutex);

        size_t count = mListeners.size();
        for(size_t index = 0; index < count; ++index)
        {
            mListeners[index]->notifyArchiveGroupRemoved(name);
        }
    }
    //-------------------------------------------------------------------------
    void FileSystemListenerGroup::notifyArchivesRemoved(const String& type)
    {
        Mutex::ScopedLock lock(mMutex);

        size_t count = mListeners.size();
        for(size_t index = 0; index < count; ++index)
        {
            mListeners[index]->notifyArchivesRemoved(type);
        }
    }
    //-------------------------------------------------------------------------
    void FileSystemListenerGroup::notifyAllArchivesRemoved()
    {
        Mutex::ScopedLock lock(mMutex);

        size_t count = mListeners.size();
        for(size_t index = 0; index < count; ++index)
        {
            mListeners[index]->notifyAllArchivesRemoved();
        }
    }
    //------------------------------------------------------------------------- 
    void FileSystemListenerGroup::notifyAllArchiveGroupsRemoved()
    {
        Mutex::ScopedLock lock(mMutex);

        size_t count = mListeners.size();
        for(size_t index = 0; index < count; ++index)
        {
            mListeners[index]->notifyAllArchiveGroupsRemoved();
        }
    }
    //-------------------------------------------------------------------------
}
