/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/filesystem/OgeArchiveGroup.h"
#include "oge/logging/OgeLogManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    ArchiveGroup::ArchiveGroup(const String& name) : 
        Archive(name, "Group", true, true), mAnyArchiveCaseSensitive(false)
    {
    }
    //-------------------------------------------------------------------------
    ArchiveGroup::~ArchiveGroup()
    {
    }
    //-------------------------------------------------------------------------
    bool ArchiveGroup::addArchive(ArchivePtr archive)
    {
        RWMutex::ScopedLock lock(mArchivesMutex, true);

        const String& name = archive->getName();
        // An archive with the name already exists
        if (mArchives.find(name) != mArchives.end())
        {
            LOGD(String("addArchive error for ") + name);
            return false;
        }
        else
        {
            LOGD(String("addArchive success for ") + name);
        }

        mArchives[name] = archive;
        mAnyArchiveCaseSensitive = 
            mAnyArchiveCaseSensitive || archive->isCaseSensitive();

        return true;
    }
    //-------------------------------------------------------------------------
    void ArchiveGroup::removeArchive(ArchivePtr archive)
    {
        RWMutex::ScopedLock lock(mArchivesMutex, true);

        ArchiveMap::iterator iter = mArchives.find(archive->getName());
        if (iter != mArchives.end())
        {
            mArchives.erase(iter);
            mAnyArchiveCaseSensitive = false;
            for (iter = mArchives.begin(); iter != mArchives.end(); ++iter)
            {
                mAnyArchiveCaseSensitive = 
                    mAnyArchiveCaseSensitive || iter->second->isCaseSensitive();

                if (mAnyArchiveCaseSensitive)
                    break;
            }
        }
    }
    //-------------------------------------------------------------------------
    void ArchiveGroup::removeArchive(const String& archiveName)
    {
        RWMutex::ScopedLock lock(mArchivesMutex, true);

        ArchiveMap::iterator iter = mArchives.find(archiveName);
        if (iter != mArchives.end())
        {
            mArchives.erase(iter);
            recalculateCaseSensitivity();

            mAnyArchiveCaseSensitive = false;
            for (iter = mArchives.begin(); iter != mArchives.end(); ++iter)
            {
                mAnyArchiveCaseSensitive = 
                    mAnyArchiveCaseSensitive || iter->second->isCaseSensitive();

                if (mAnyArchiveCaseSensitive)
                    break;
            }
        }
    }
    //-------------------------------------------------------------------------
    void ArchiveGroup::removeAllArchives()
    {
        RWMutex::ScopedLock lock(mArchivesMutex, true);
        mArchives.clear();
        mAnyArchiveCaseSensitive = false;
    }
    //-------------------------------------------------------------------------
    void ArchiveGroup::removeArchivesOfType(const String& type)
    {
        RWMutex::ScopedLock archiveLock(mArchivesMutex, true);

        ArchiveMap::iterator iter = mArchives.begin();
        while(iter != mArchives.end())
        {
            if (iter->second->getType() == type)
            {
                mArchives.erase(iter++);
            }
            else
            {
                ++iter;
            }
        }
        recalculateCaseSensitivity();
    }
    //-------------------------------------------------------------------------
    ArchivePtr ArchiveGroup::getArchive(const String& archiveName) const
    {
        RWMutex::ScopedLock archiveLock(mArchivesMutex, false);

        ArchiveMap::const_iterator iter = mArchives.find(archiveName);
        return iter != mArchives.end() ? iter->second : ArchivePtr();
    }
    //-------------------------------------------------------------------------
    bool ArchiveGroup::archiveExists(const String& archiveName) const
    {
        RWMutex::ScopedLock archiveLock(mArchivesMutex, false);

        ArchiveMap::const_iterator iter = mArchives.find(archiveName);
        return iter != mArchives.end() ? true : false;
    }
    //-------------------------------------------------------------------------
    StringVector ArchiveGroup::getArchiveNames() const
    {
        RWMutex::ScopedLock lock(mArchivesMutex, false);

        StringVector names;
        ArchiveMap::const_iterator iter = mArchives.begin();
        for (; iter != mArchives.end(); ++iter)
        {
            names.push_back(iter->first);
        }

        return names;
    }
    //-------------------------------------------------------------------------
    ArchivePtr ArchiveGroup::getDefaultArchive() const
    {
        RWMutex::ScopedLock lock(mArchivesMutex, false);
        return mDefaultArchive;
    }
    //-------------------------------------------------------------------------
    void ArchiveGroup::setDefaultArchive(ArchivePtr archive)
    {
        RWMutex::ScopedLock lock(mArchivesMutex, true);
        mDefaultArchive = archive;
    }
    //-------------------------------------------------------------------------
    bool ArchiveGroup::isCaseSensitive() const
    {
        RWMutex::ScopedLock lock(mArchivesMutex, false);
        return mAnyArchiveCaseSensitive;
    }
    //-------------------------------------------------------------------------
    FilePtr ArchiveGroup::createFile(const String& filename)
    {
        RWMutex::ScopedLock lock(mArchivesMutex, false);
        
        if (!mDefaultArchive)
            return FilePtr();

        return mDefaultArchive->createFile(filename);
    }
    //-------------------------------------------------------------------------
    bool ArchiveGroup::deleteFile(const String& filename)
    {
        RWMutex::ScopedLock lock(mArchivesMutex, false);

        if (!mDefaultArchive)
            return false;

        return mDefaultArchive->deleteFile(filename);
    }
    //-------------------------------------------------------------------------
    FilePtr ArchiveGroup::getFile(const String& filename)
    {
        RWMutex::ScopedLock lock(mArchivesMutex, false);

        ArchiveMap::iterator iter = mArchives.begin();
        for (; iter != mArchives.end(); ++iter)
        {
            FilePtr file = iter->second->getFile(filename);
            if (file.get())
                return file;
        }

        return FilePtr();
    }
    //-------------------------------------------------------------------------
    bool ArchiveGroup::containsFile(const String& filename)
    {
        RWMutex::ScopedLock lock(mArchivesMutex, false);

        ArchiveMap::iterator iter = mArchives.begin();
        for (; iter != mArchives.end(); ++iter)
        {
            if (iter->second->getFile(filename).get())
                return true;
        }

        return false;
    }
    //-------------------------------------------------------------------------
    void ArchiveGroup::refreshFileList()
    {
        RWMutex::ScopedLock lock(mArchivesMutex, false);

        ArchiveMap::iterator iter = mArchives.begin();
        for (; iter != mArchives.end(); ++iter)
        {
            iter->second->refreshFileList();
        }
    }
    //-------------------------------------------------------------------------
    StringVector ArchiveGroup::getFilenames()
    {
        RWMutex::ScopedLock lock(mArchivesMutex, false);

        StringVector filenames;
        
        ArchiveMap::iterator archiveIter = mArchives.begin();
        for (; archiveIter != mArchives.end(); ++archiveIter)
        {
            StringVector list = archiveIter->second->getFilenames();
            StringVector::iterator listIter = list.begin();
            for (; listIter != list.end(); ++listIter)
            {
                filenames.push_back(*listIter);
            }
        }

        return filenames;
    }
    //-------------------------------------------------------------------------
    FileVector ArchiveGroup::getFiles()
    {
        RWMutex::ScopedLock lock(mArchivesMutex, false);

        FileVector files;
        
        ArchiveMap::iterator archiveIter = mArchives.begin();
        for (; archiveIter != mArchives.end(); ++archiveIter)
        {
            FileVector list = archiveIter->second->getFiles();
            FileVector::iterator listIter = list.begin();
            for (; listIter != list.end(); ++listIter)
            {
                files.push_back(*listIter);
            }
        }

        return files;
    }
    //-------------------------------------------------------------------------
    StringVector ArchiveGroup::findFilenames(const String& pattern)
    {
        RWMutex::ScopedLock lock(mArchivesMutex, false);

        StringVector filenames;
        
        ArchiveMap::iterator archiveIter = mArchives.begin();
        for (; archiveIter != mArchives.end(); ++archiveIter)
        {
            StringVector list = archiveIter->second->findFilenames(pattern);
            StringVector::iterator listIter = list.begin();
            for (; listIter != list.end(); ++listIter)
            {
                filenames.push_back(*listIter);
            }
        }

        return filenames;
    }
    //-------------------------------------------------------------------------
    FileVector ArchiveGroup::findFiles(const String& pattern)
    {
        RWMutex::ScopedLock lock(mArchivesMutex, false);

        FileVector files;
        
        ArchiveMap::iterator archiveIter = mArchives.begin();
        for (; archiveIter != mArchives.end(); ++archiveIter)
        {
            FileVector list = archiveIter->second->findFiles(pattern);
            FileVector::iterator listIter = list.begin();
            for (; listIter != list.end(); ++listIter)
            {
                files.push_back(*listIter);
            }
        }

        return files;
    }
    //-------------------------------------------------------------------------
    void ArchiveGroup::recalculateCaseSensitivity()
    {
        ArchiveMap::iterator iter;
        mAnyArchiveCaseSensitive = false;
        for (iter = mArchives.begin(); iter != mArchives.end(); ++iter)
        {
            mAnyArchiveCaseSensitive = 
                mAnyArchiveCaseSensitive || iter->second->isCaseSensitive();

            if (mAnyArchiveCaseSensitive)
                break;
        }
    }
    //-------------------------------------------------------------------------
}
