/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/filesystem/OgeFileSystem.h"
#include "oge/filesystem/OgeArchiveFactory.h"
#include "oge/filesystem/filesystem/OgeFileSystemArchive.h"
#include "oge/filesystem/zip/OgeZipArchive.h"
#include "oge/logging/OgeLogManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    template<> FileSystem* Singleton<FileSystem>::mSingleton = 0;
    FileSystem* FileSystem::getSingletonPtr()
    {
        return mSingleton;
    }
    FileSystem& FileSystem::getSingleton()
    {
        assert(mSingleton);
        return *mSingleton;        
    }
    //-------------------------------------------------------------------------
    FileSystem::FileSystem()
    {
       
    }
    //-------------------------------------------------------------------------
    FileSystem::~FileSystem()
    {
    }
    //-------------------------------------------------------------------------
    void FileSystem::initialise()
    {
        registerArchiveFactory(new FileSystemArchiveFactory());
        registerArchiveFactory(new ZipArchiveFactory());

        mArchives = new ArchiveGroup("FileSystem Archives");
        mArchiveGroups = new ArchiveGroup("FileSystem Archive Groups");
    }
    //-------------------------------------------------------------------------
    void FileSystem::shutdown()
    {
        mArchives = 0;
        mArchiveGroups = 0;
        deleteAllFactories();
    }
    //-------------------------------------------------------------------------
    bool FileSystem::registerArchiveFactory(ArchiveFactory* archiveFactory)
    {
        RWMutex::ScopedLock factoryLock(mArchiveFactoriesMutex, true);

        const String& factoryType = archiveFactory->getType();

        if (mArchiveFactories.find(factoryType) != mArchiveFactories.end())
            return false;

        mArchiveFactories[factoryType] = archiveFactory;
        return true;
    }
    //-------------------------------------------------------------------------
    void FileSystem::unregisterArchiveFactory(const String& archiveType)
    {
        RWMutex::ScopedLock factoryLock(mArchiveFactoriesMutex, true);

        ArchiveFactoryMap::iterator iter = mArchiveFactories.find(archiveType);
        
        if (iter != mArchiveFactories.end())
        {
            removeArchivesOfType(archiveType);
            mArchiveFactories.erase(iter);
        }
    }
    //-------------------------------------------------------------------------
    ArchivePtr FileSystem::addArchive(const String& path, bool returnExisting,
        bool allowRead, bool allowWrite)
    {
        RWMutex::ScopedLock factoryLock(mArchiveFactoriesMutex, true);
        Mutex::ScopedLock archiveLock(mArchivesMutex);

        ArchivePtr archive = mArchives->getArchive(path);
        if (archive)
            return returnExisting ? archive : ArchivePtr();
               
        // Archive doesn't exist, attempt to create it   
        ArchiveFactoryMap::iterator factoryIter = mArchiveFactories.begin();
        for (; factoryIter != mArchiveFactories.end(); ++factoryIter)
        {
            archive = factoryIter->second->createArchive(path, allowRead, 
                allowWrite);

            if (archive)
            {
                // The path has been handled successfully                
                mArchives->addArchive(archive);
                mListeners.notifyArchiveAdded(path);
                break;
            }
        }

        return archive;
    }
    //-------------------------------------------------------------------------
    void FileSystem::removeArchive(const String& path)
    {
        mArchives->removeArchive(path);
        mListeners.notifyArchiveRemoved(path);
    }
    //-------------------------------------------------------------------------
    void FileSystem::removeArchivesOfType(const String& type)
    {
        mArchives->removeArchivesOfType(type);       
        mListeners.notifyArchivesRemoved(type);
    }
    //-------------------------------------------------------------------------
    void FileSystem::removeAllArchives()
    {
        mArchives->removeAllArchives();
        mListeners.notifyAllArchivesRemoved();
    }
    //-------------------------------------------------------------------------
    ArchivePtr FileSystem::getArchive(const String& path, bool searchGroups)
    {
        ArchivePtr archive = mArchives->getArchive(path);
        if (archive || !searchGroups)
            return archive;

        return mArchiveGroups->getArchive(path);
    }
    //-------------------------------------------------------------------------
    bool FileSystem::archiveExists(const String& path, bool searchGroups)
    {
        bool exists = mArchives->archiveExists(path);
        if (exists || !searchGroups)
            return exists;

        return mArchiveGroups->archiveExists(path);
    }
    //-------------------------------------------------------------------------
    ArchiveGroupPtr FileSystem::addArchiveGroup(const String& name, 
        bool returnExisting)
    {
        Mutex::ScopedLock archiveLock(mArchivesMutex);

        ArchiveGroupPtr group = mArchiveGroups->getArchive(name).cast<ArchiveGroup>();
        if (group)
            return returnExisting ? group : ArchiveGroupPtr();

        group = new ArchiveGroup(name);

        mArchiveGroups->addArchive(group);
        mListeners.notifyArchiveGroupAdded(name);

        return group;
    }
    //-------------------------------------------------------------------------
    void FileSystem::removeArchiveGroup(const String& name)
    {
        mArchiveGroups->removeArchive(name);
        mListeners.notifyArchiveGroupRemoved(name);
    }
    //-------------------------------------------------------------------------
    ArchiveGroupPtr FileSystem::getArchiveGroup(const String& name)
    {
        return mArchiveGroups->getArchive(name).cast<ArchiveGroup>();
    }
    //-------------------------------------------------------------------------
    bool FileSystem::archiveGroupExists(const String& name)
    {
        return mArchiveGroups->archiveExists(name);
    }
    //-------------------------------------------------------------------------
    void FileSystem::removeAllArchiveGroups()
    {
        return mArchiveGroups->removeAllArchives();
    }
    //-------------------------------------------------------------------------
    FilePtr FileSystem::getFile(const String& filename)
    {
        return mArchives->getFile(filename);
    }
    //-------------------------------------------------------------------------
    bool FileSystem::containsFile(const String& filename)
    {
        return mArchives->containsFile(filename);
    }
    //-------------------------------------------------------------------------
    void FileSystem::refreshFileList()
    {
        mArchives->refreshFileList();
    }
    //-------------------------------------------------------------------------
    StringVector FileSystem::getFilenames()
    {
        return mArchives->getFilenames();
    }
    //-------------------------------------------------------------------------
    FileVector FileSystem::getFiles()
    {
        return mArchives->getFiles();
    }
    //-------------------------------------------------------------------------
    StringVector FileSystem::findFilenames(const String& pattern)
    {
        return mArchives->findFilenames(pattern);
    }
    //-------------------------------------------------------------------------
    FileVector FileSystem::findFiles(const String& pattern)
    {
        return mArchives->findFiles(pattern);
    }
    //-------------------------------------------------------------------------
    void FileSystem::addListener(FileSystemListener* listener)
    {
        mListeners.addListener(listener);
    }
    //-------------------------------------------------------------------------
    void FileSystem::removeListener(FileSystemListener* listener)
    {
        mListeners.removeListener(listener);
    }
    //-------------------------------------------------------------------------
    void FileSystem::deleteAllFactories()
    {
        ArchiveFactoryMap::iterator iter = mArchiveFactories.begin();
        for(; iter != mArchiveFactories.end(); ++iter)
        {
            delete iter->second;
        }
    }
    //-------------------------------------------------------------------------
    bool FileSystem::setupFromOptions(OptionPtr rootOption)
    {
        if (!rootOption)
            return false;

        setupArchivesFromOptions(rootOption->getOption("archive"));
        setupGroupsFromOptions(rootOption->getOption("group"));

        return true;
    }
    //-------------------------------------------------------------------------
    void FileSystem::setupArchivesFromOptions(OptionPtr rootOption)
    {
        if (!rootOption)
            return;

        Option::ValueIterator valueIter = rootOption->getValueIterator();

        for(valueIter.begin(); valueIter.hasNext(); valueIter.next())
        {
            if (!addArchive(valueIter.getValue(), false))
            {
                LOGW(String("The archive '") + valueIter.getValue() +
                     "' couldn't be added and was not created!");
            }
        }
    }
    //-------------------------------------------------------------------------
    void FileSystem::setupGroupsFromOptions(OptionPtr rootOption)
    {
        if (!rootOption)
            return;

        Option::Iterator groupIter = rootOption->getOptionIterator();

        for (groupIter.begin(); groupIter.hasNext(); groupIter.next())
        {
            ArchiveGroupPtr archiveGroup = 
                addArchiveGroup(groupIter.getName(), false);

            if (archiveGroup)
            {
                OptionPtr groupArchivesOption = 
                    groupIter.getOption()->getOption("archive");

                if (!groupArchivesOption)
                    break;

                Option::ValueIterator valueIter = 
                    groupArchivesOption->getValueIterator();

                for(valueIter.begin(); valueIter.hasNext(); valueIter.next())
                {
                    ArchivePtr archive = getArchive(valueIter.getValue(), true);
                    if (!archive)
                        archive = addArchive(valueIter.getValue(), false);

                    if (archive)
                    {
                        archiveGroup->addArchive(archive);
                    }
                    else
                    {
                        LOGW(String("The archive '") + valueIter.getValue() +
                            "' does not exist and could not be created!");
                    }
                }
            }
            else
            {
                LOGW(String("The archive group'") + groupIter.getName() +
                    "' couldn't be added either because it " +
                    " already exist or it couldn't be created!");
            }
        }
    }
    //-------------------------------------------------------------------------
    FileSystem* FileSystem::createSingleton()
    {
        if (!mSingleton)
            mSingleton = new FileSystem();

        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void FileSystem::destroySingleton()
    {
        if (mSingleton)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //-------------------------------------------------------------------------
}
