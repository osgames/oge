/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/filesystem/zip/OgeZipArchive.h"
#include "oge/filesystem/zip/OgeZipFile.h"

namespace oge
{
    //-------------------------------------------------------------------------
    ZipArchive::ZipArchive(const String& pathname, ZZIP_DIR* zipHandle) :
        Archive(pathname, "zip", true, false), mZipHandle(zipHandle)
    {
        if (zipHandle)
        {
            _refreshFileList();
        }
        else
        {
            refreshFileList();
        }
    }
    //-------------------------------------------------------------------------
    ZipArchive::~ZipArchive()
    {
        cleanUp();
    }
    //-------------------------------------------------------------------------
    bool ZipArchive::isCaseSensitive() const
    {
        return true;
    }
    //-------------------------------------------------------------------------
    FilePtr ZipArchive::createFile(const String& filename)
    {
        return FilePtr();
    }
    //-------------------------------------------------------------------------
    bool ZipArchive::deleteFile(const String& filename)
    {
        return false;
    }
    //-------------------------------------------------------------------------
    void ZipArchive::refreshFileList()
    {
        cleanUp();
        if (openZipArchive())
        {
            _refreshFileList();
        }
    }
    //-------------------------------------------------------------------------
    bool ZipArchive::openZipArchive()
    {
        if (mZipHandle)
            return true;

        mZipHandle = zzip_dir_open(mName.c_str(), 0);
        return mZipHandle != 0;
    }
    //-------------------------------------------------------------------------
    void ZipArchive::_refreshFileList()
    {
        ZZIP_DIRENT dirent;
        while (zzip_dir_read(mZipHandle,&dirent))
        {
            mFiles[dirent.d_name] = new ZipFile(dirent.d_name, mZipHandle);
        }
    }
    //-------------------------------------------------------------------------
    void ZipArchive::cleanUp()
    {
        if (mZipHandle)
        {
            zzip_dir_close(mZipHandle);    
            mZipHandle = 0;
        }
    }
    //-------------------------------------------------------------------------
    ZipArchiveFactory::ZipArchiveFactory() : ArchiveFactory("Zip")
    {

    }
    //-------------------------------------------------------------------------
    ZipArchiveFactory::~ZipArchiveFactory()
    {

    }
    //-------------------------------------------------------------------------
    ArchivePtr ZipArchiveFactory::createArchive(const String& path, 
        bool allowRead, bool allowWrite)
    {
        ZZIP_DIR* zipHandle = zzip_dir_open(path.c_str(), 0);
        if (!zipHandle)
            return ArchivePtr();

        return ArchivePtr(new ZipArchive(path, zipHandle));
    }
    //-------------------------------------------------------------------------
}
