/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/filesystem/zip/OgeZipStream.h"

namespace oge
{
    //-------------------------------------------------------------------------
    ZipStream::ZipStream(DataStreamOwner* owner) : DataStream(owner), 
        mZipHandle(0), mFileHandle(0), mFilename(""), mSize(0), mIsOpen(false)
    {
    }
    //-------------------------------------------------------------------------
    ZipStream::~ZipStream()
    {
        close();
    }
    //-------------------------------------------------------------------------
    bool ZipStream::openRead(ZZIP_DIR* zipHandle, const String& filename)
    {
        if (mIsOpen || !zipHandle || filename == "")
            return false;

        mZipHandle = zipHandle;
        mFileHandle = zzip_file_open(mZipHandle, filename.c_str(), 
            ZZIP_ONLYZIP | ZZIP_CASELESS);

        if (!mFileHandle)
            return false;
    
        mFilename = filename;
        mAccess = ACCESS_READ;
        mSize = _getSize();
        mIsOpen = true;

        return true;
    }
    //-------------------------------------------------------------------------
    void ZipStream::close()
    {
        DataStream::close();

        if (mFileHandle)
        {
            zzip_file_close(mFileHandle);
            mFileHandle = 0;
        }

        mSize = 0;
        mFilename = "";
        mIsOpen = false;
    }
    //-------------------------------------------------------------------------
    bool ZipStream::isValid() const
    {
        return mIsOpen;
    }
    //-------------------------------------------------------------------------
    size_t ZipStream::getSize() const
    {
        return mSize;
    }
    //-------------------------------------------------------------------------
    const String& ZipStream::getName() const
    {
        return mFilename;
    }
    //-------------------------------------------------------------------------
    bool ZipStream::isAtEndOfStream() const
    {
        return zzip_tell(mFileHandle) == static_cast<zzip_off_t>(mSize);
    }
    //-------------------------------------------------------------------------
    size_t ZipStream::read(char* buffer, size_t size)
    {
        if (!isValid())
            return 0;

        return zzip_file_read(mFileHandle, buffer, size);
    }
    //-------------------------------------------------------------------------
    bool ZipStream::write(const char* buffer, size_t size)
    {
        return false;
    }
    //-------------------------------------------------------------------------
    bool ZipStream::setPosition(size_t position)
    {
        if (!isValid())
            return false;

        return position ==
            (size_t)(zzip_seek(mFileHandle, (zzip_off_t) position, SEEK_SET));
    }
    //-------------------------------------------------------------------------
    size_t ZipStream::getPosition() const
    {
        if (!isValid())
            return 0;

        return zzip_tell(mFileHandle);
    }
    //-------------------------------------------------------------------------
    DataStream::Access ZipStream::getAccess() const
    {
        return mAccess;
    }
    //-------------------------------------------------------------------------
    size_t ZipStream::_getSize()
    {
        ZZIP_STAT stat;
        zzip_file_stat(mFileHandle, &stat);
        size_t size = static_cast<size_t>(stat.st_size);

        return size;
    }
    //-------------------------------------------------------------------------
}
