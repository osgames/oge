/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/      
#include "oge/filesystem/filesystem/OgeFileSystemFile.h"
#include "oge/datastreams/OgeFileSystemStream.h"
#include "oge/datastreams/OgeBufferMemoryStream.h"
#include "oge/containers/OgeBuffer.h"

namespace oge
{
    //-------------------------------------------------------------------------
    FileSystemFile::FileSystemFile(const Path& filename, bool allowRead,
        bool allowWrite) : mFilename(filename), mCurrentStreamType(ST_NONE), 
        mReadStreamCount(0)
    {
        FileUtil fu(mFilename);
        mReadable = allowRead && fu.canRead();
        mWritable = allowWrite && fu.canWrite();
    }
    //-------------------------------------------------------------------------
    FileSystemFile::~FileSystemFile()
    {
        mCache = 0;
    }
    //-------------------------------------------------------------------------
    String FileSystemFile::getFilename() const
    {
        String extension = "." + mFilename.getExtension();
        String basename = mFilename.getBaseName();

        if (extension == ".")
            return basename;
         
        return basename + extension;
    }
    //-------------------------------------------------------------------------
    DataStreamPtr FileSystemFile::openRead()
    {
        if (!mReadable)
            return DataStreamPtr();

        Mutex::ScopedLock lock(mMutex);

        // Already open for write, can't read
        if (mCurrentStreamType == ST_WRITER)
            return DataStreamPtr();

        mCurrentStreamType = ST_READER;
        ++mReadStreamCount;

        if (mCache)
        {
            BufferMemoryStream* stream = new BufferMemoryStream(this);
            if (stream->openRead(mFilename.getFileName(), mCache))
                return DataStreamPtr(stream);

            delete stream;
        }
        else
        {
            FileSystemStream* stream = new FileSystemStream(this);
            if (stream->openRead(mFilename))
                return DataStreamPtr(stream);

            delete stream;
        }

        return DataStreamPtr();
    }
    //-------------------------------------------------------------------------
    DataStreamPtr FileSystemFile::openWrite(bool append, bool truncate)
    {
        if (!mWritable)
            return DataStreamPtr();
        
        Mutex::ScopedLock lock(mMutex);

        // Can only open for write if there are no current
        // read or write streams
        if (mCurrentStreamType != ST_NONE)
            return DataStreamPtr();

        mCurrentStreamType = ST_WRITER;
        // Clear the cache
        mCache = 0;

        FileSystemStream* stream = new FileSystemStream();

        if (!stream->openWrite(mFilename, append, true, truncate, false))
        {
            delete stream;
            return DataStreamPtr();
        }
        
        return DataStreamPtr(stream);
    }
    //-------------------------------------------------------------------------
    bool FileSystemFile::canRead()
    {
        return mReadable;
    }
    //-------------------------------------------------------------------------
    bool FileSystemFile::canWrite()
    {
        return mWritable;
    }
    //-------------------------------------------------------------------------
    bool FileSystemFile::exists()
    {
        FileUtil file(mFilename);
        return file.exists();
    }
    //-------------------------------------------------------------------------
    bool FileSystemFile::fillCache()
    {
        // Can't fill the cache if the file cannot be read
        if (!mReadable)
            return false;

        Mutex::ScopedLock lock(mMutex);

        if (mCache)
            return true;

        DataStreamPtr fs = openRead();
        if (!fs)
            return false;

        size_t size = fs->getSize();
        mCache = new Buffer(size, false);
        fs->read(mCache->get(), size);

        return true;
    }
    //-------------------------------------------------------------------------
    bool FileSystemFile::clearCache()
    {
        Mutex::ScopedLock lock(mMutex);
        if (mCurrentStreamType != ST_NONE)
            return false;

        mCache = 0;
        return true;
    }
    //-------------------------------------------------------------------------
    void FileSystemFile::_dataStreamReadReleased()
    {
        Mutex::ScopedLock lock(mMutex);
        // return if there isn't a record of the read stream
        // this shouldn't happen
        if (mCurrentStreamType != ST_READER)
            return;

        --mReadStreamCount;

        if (mReadStreamCount == 0)
        {
            mCurrentStreamType = ST_NONE;
        }
    }
    //-------------------------------------------------------------------------
    void FileSystemFile::_dataStreamWriteReleased()
    {
        Mutex::ScopedLock lock(mMutex);
        // return if there isn't a record of the write stream
        // this shouldn't happen
        if (mCurrentStreamType != ST_WRITER)
            return;

        mCurrentStreamType = ST_NONE;
    }
    //-------------------------------------------------------------------------
}
