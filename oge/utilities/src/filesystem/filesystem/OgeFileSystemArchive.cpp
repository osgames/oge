/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/filesystem/filesystem/OgeFileSystemArchive.h"
#include "oge/filesystem/filesystem/OgeFileSystemFile.h"
#include "Poco/DirectoryIterator.h"
#include "oge/logging/OgeLogManager.h"

// TODO Important catch all exceptions that can thrown by poco
namespace oge
{
    //-------------------------------------------------------------------------
    FileSystemArchive::FileSystemArchive(const String& path, bool allowRead, 
        bool allowWrite) : Archive(path, "FileSystem", allowRead, allowWrite), 
        mPath(path)
    {
        // use path and not mPath or you might trigger a file not found assert - petrocket
        FileUtil fu(path);
        // Check the file system to see if the allowed settings can be used
        mCanRead = mCanRead && fu.canRead();
        mCanWrite = mCanWrite && fu.canWrite();

        refreshFileList();
    }
    //-------------------------------------------------------------------------
    FileSystemArchive::~FileSystemArchive()
    {
    }
    //-------------------------------------------------------------------------
    bool FileSystemArchive::isCaseSensitive() const
    {
        return true;
    }
    //-------------------------------------------------------------------------
    FilePtr FileSystemArchive::createFile(const String& filename)
    {
        if (!mCanWrite)
            return FilePtr();

        RWMutex::ScopedLock lock(mFilesMutex, true);

        if (mFiles.find(filename) != mFiles.end())
            return FilePtr();

        Path filepath(mPath);
        filepath.setFileName(filename);
        FileUtil fu(filepath);

        // The file already exists, it cannot be created
        if (!fu.createFile())
            return FilePtr();

        FilePtr file(new FileSystemFile(filepath, mCanRead, mCanWrite));
        mFiles[filename] = file;

        return file;
    }
    //-------------------------------------------------------------------------
    bool FileSystemArchive::deleteFile(const String& filename)
    {
        if (!mCanWrite)
            return false;

        RWMutex::ScopedLock lock(mFilesMutex, true);

        FileMap::iterator iter = mFiles.find(filename);

        // TODO: choose between true/false
        if (iter == mFiles.end())
        {
            LOGW("File '"+filename+"' couldnt' be deleted: not found.");
            return true;
        }

        mFiles.erase(iter);

        Path filepath(mPath);
        filepath.setFileName(filename);

        try
        {
            FileUtil fu(filepath);
            fu.remove();
        }
        catch (...)
        {
            // TODO Log the exception
            LOGE("Error while deleting a file.");
            return false;
        }
        return true;
    }
    //-------------------------------------------------------------------------
    void FileSystemArchive::refreshFileList()
    {
        RWMutex::ScopedLock lock(mFilesMutex, true);

        mFiles.clear();

        Poco::DirectoryIterator dirIter(mPath);
        Poco::DirectoryIterator endIter;

        while(dirIter != endIter)
        {
            if (dirIter->isFile())
            {
                Poco::File& file = *dirIter;
                Poco::Path filePath(file.path());
                const std::string filename = filePath.getFileName();

                mFiles[filename] = FilePtr(new FileSystemFile(file.path(), 
                    mCanRead, mCanWrite));
            }

            ++dirIter;
        }
    }
    //-------------------------------------------------------------------------
    FileSystemArchiveFactory::FileSystemArchiveFactory() : 
        ArchiveFactory("FileSystem")
    {
    }
    //-------------------------------------------------------------------------
    FileSystemArchiveFactory::~FileSystemArchiveFactory()
    {
    }
    //-------------------------------------------------------------------------
    ArchivePtr FileSystemArchiveFactory::createArchive(const String& path,
        bool allowRead, bool allowWrite)
    {
        FileUtil file(path);
        if (file.exists() && file.isDirectory())
        {
            LOGD(String("FileSystemArchiveFactory::createArchive sucess for "));
            return ArchivePtr(new FileSystemArchive(path, allowRead, 
                allowWrite));
        }
        else
        {
            LOGI(String("Directory ") + path + " not exists");
            return ArchivePtr();
        }
    }
    //-------------------------------------------------------------------------
}
