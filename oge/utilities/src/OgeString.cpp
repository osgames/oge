/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/OgeString.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"

namespace oge 
{
    //-------------------------------------------------------------------------
    #if OGE_WCHAR_T_STRINGS
        const String StringUtil::BLANK = String(L"");
    #else
        const String StringUtil::BLANK = String("");
    #endif
    //-------------------------------------------------------------------------
    #if (OGE_PLATFORM == OGE_PLATFORM_WIN32)
        const String StringUtil::EOL = String("\r\n");
    #else
        const String StringUtil::EOL = String("\n");
    #endif
    //-------------------------------------------------------------------------
    String StringUtil::toString(Real value, unsigned short precision,
        unsigned short width, char fill, std::ios::fmtflags flags)
    {
        StringUtil::StringStreamType stream;
        stream.precision(precision);
        stream.width(width);
        stream.fill(fill);
        if (flags)
            stream.setf(flags);
        stream << value;
        return stream.str();
    }
    //-------------------------------------------------------------------------
    String StringUtil::toString(int value, unsigned short width, char fill,
        std::ios::fmtflags flags)
    {
        StringUtil::StringStreamType stream;
        stream.width(width);
        stream.fill(fill);
        if (flags)
            stream.setf(flags);
        stream << value;
        return stream.str();
    }
    //-------------------------------------------------------------------------
    String StringUtil::toString(unsigned int value, unsigned short width, 
        char fill, std::ios::fmtflags flags)
    {
        StringUtil::StringStreamType stream;
        stream.width(width);
        stream.fill(fill);
        if (flags)
            stream.setf(flags);
        stream << value;
        return stream.str();
    }
    //-------------------------------------------------------------------------
    String StringUtil::toString(void* value, unsigned short width,
        char fill, std::ios::fmtflags flags)
    {
        StringUtil::StringStreamType stream;
        stream.width(width);
        stream.fill(fill);
        if (flags)
            stream.setf(flags);
        stream << value;
        return stream.str();
    }
    //-------------------------------------------------------------------------
    String StringUtil::toString(long value, unsigned short width, char fill,
        std::ios::fmtflags flags)
    {
        StringUtil::StringStreamType stream;
        stream.width(width);
        stream.fill(fill);
        if (flags)
            stream.setf(flags);
        stream << value;
        return stream.str();
    }
    //-------------------------------------------------------------------------
    String StringUtil::toString(const Vector3& vec, unsigned short precision,
        unsigned short width, char fill, std::ios::fmtflags flags)
    {
        StringUtil::StringStreamType stream;
        stream.precision(precision);
        stream.width(width);
        stream.fill(fill);
        if (flags)
            stream.setf(flags);
        stream << "(";
        stream << vec.x;
        stream << ", ";
        stream << vec.y;
        stream << ", ";
        stream << vec.z;
        stream << ")";
        return stream.str();
    }
    //-------------------------------------------------------------------------
    String StringUtil::toString(const Quaternion& qua, unsigned short precision,
        unsigned short width, char fill, std::ios::fmtflags flags)
    {
        StringUtil::StringStreamType stream;
        stream.precision(precision);
        stream.width(width);
        stream.fill(fill);
        if (flags)
            stream.setf(flags);
        stream << "(";
        stream << qua.w;
        stream << ", ";
        stream << qua.x;
        stream << ", ";
        stream << qua.y;
        stream << ", ";
        stream << qua.z;
        stream << ")";
        return stream.str();
    }
    //-------------------------------------------------------------------------
    Real StringUtil::toReal(const String& value)
    {
        #if OGE_WCHAR_T_STRINGS
            TODO return (Real) 0.0;
        #else
            return (Real) atof(value.c_str());
        #endif
    }
    //-------------------------------------------------------------------------
    int StringUtil::toInt(const String& value)
    {
        #if OGE_WCHAR_T_STRINGS
            TODO return 0;
        #else
            return atoi(value.c_str());
        #endif
    }
    //-------------------------------------------------------------------------
    unsigned int StringUtil::toUnsignedInt(const String& value)
    {
        #if OGE_WCHAR_T_STRINGS
            TODO return 0L;
        #else
            return static_cast<unsigned int>(strtoul(value.c_str(), 0, 10));
        #endif
    }
    //-------------------------------------------------------------------------
    long StringUtil::toLong(const String& value)
    {
        #if OGE_WCHAR_T_STRINGS
            TODO return 0;
        #else
            return strtol(value.c_str(), 0, 10);
        #endif
    }
    //-------------------------------------------------------------------------
    unsigned long StringUtil::toUnsignedLong(const String& value)
    {
        #if OGE_WCHAR_T_STRINGS
            TODO return 0;
        #else
            return strtoul(value.c_str(), 0, 10);
        #endif
    }
    //-------------------------------------------------------------------------
    bool StringUtil::toBool(String value)
    {
        toLowerCase(value);
        return value == "1" || value == "true" || value == "yes" ? true : false;
    }
    //-------------------------------------------------------------------------
    void StringUtil::trim(String& string)
    {
        String::size_type left = string.find_first_not_of(' ');
        String::size_type right = string.find_last_not_of(' ');

        string = string.substr(left == String::npos ? 0 : left,
            right == String::npos ? string.length() - 1 : right - left + 1);
    }
    //-------------------------------------------------------------------------
    void StringUtil::trimWhitespace(String& string)
    {
        bool foundFirst = false;
        size_t firstChar = 0;
        size_t lastChar = 0;
        size_t size = string.size();

        for (size_t index = 0; index < size; ++index)
        {
            char c = string[index];
            if (c != ' ' && c != '\n' && c != '\r' && c != '\t')   
            {    
                if (!foundFirst)
                {
                    firstChar = index;
                    foundFirst = true;
                }

                lastChar = index;
            }
        }

        if(foundFirst)
        {
            string = string.substr(firstChar, lastChar - firstChar + 1);
        }
        else
        {
            string = "";
        }
    }
    //-------------------------------------------------------------------------
    void StringUtil::toLowerCase(String& string)
    {
        std::transform(string.begin(), string.end(), string.begin(), tolower);
    }
    //-------------------------------------------------------------------------
}
