/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt .
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/OgeClock.h"

namespace oge 
{
#if OGE_PLATFORM == OGE_PLATFORM_WIN32
    //-------------------------------------------------------------------------
    Clock::Clock() : mStartTicks(0), mStartTime(0), mSecsPerTick(0), mNewTicks(0)
    {
        LARGE_INTEGER startTime;
        assert( QueryPerformanceCounter(&startTime) );// If assert use PreciseTimer

        LARGE_INTEGER frequency;
        QueryPerformanceFrequency(&frequency);
        mSecsPerTick = 1.0 / frequency.QuadPart; // If 0 we are anyway in troubles

        QueryPerformanceCounter(&startTime);
        mStartTime = (double) startTime.QuadPart;
        mStartTicks = (double) GetTickCount();
    }
    //-------------------------------------------------------------------------
    void Clock::update()
    {
        QueryPerformanceCounter(&mCurrentTime);

        // Scale by 1000 for milliseconds
        mNewTicks = (1000.0 *
          ((double)mCurrentTime.QuadPart - mStartTime) * mSecsPerTick);

        // Detect and compensate for performance counter leaps
        // (surprisingly common, see Microsoft KB: Q274323)
        double check = (double) GetTickCount() - mStartTicks;
        double errorMs = mNewTicks - check;
        if (errorMs < -100 || errorMs > 100)
        {
            mStartTime += errorMs * mSecsPerTick * 0.001;
            mNewTicks = check;
        }
    }

#else // ------------------ Linux + Mac ---------------------------------------

    //-------------------------------------------------------------------------
    Clock::Clock() : mNewTicks(0)
    {
        gettimeofday(&mStartTime, 0);
        mStartTicks = (double)(mStartTime.tv_sec * 1000 + mStartTime.tv_usec / 1000.0);
    }
    //-------------------------------------------------------------------------
    void Clock::update()
    {
        gettimeofday(&mCurrentTime, 0);
        double ticks = (double)(mCurrentTime.tv_sec * 1000 + mCurrentTime.tv_usec / 1000.0);
        mNewTicks = ticks - mStartTicks;
    }
    //-------------------------------------------------------------------------
#endif // Linux + Mac

}
