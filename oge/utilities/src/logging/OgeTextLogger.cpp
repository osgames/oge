/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/logging/OgeLogManager.h"
#include "oge/logging/OgeTextLogger.h"

namespace oge
{
    //-------------------------------------------------------------------------
    void TextLogger::logMessage(int level, const String& msg, 
        const char* file, const unsigned long line)
    {
        if (mLines > mMaxLines)
            return;

        RWMutex::ScopedLock lock(mMutex, false);

        String str( getDebugLine() );

        if (mLines == mMaxLines)
        {
            str += "======== Logging stopped : Too much lines: > ";
            str += StringUtil::toString( mLines );
            str += " ========";
            str += StringUtil::EOL;
            mTextWriter->writeLine( str );
            return;
        }
        
        // Indent based on the stack level
        for(unsigned int i = 0; i < mPreviousStackLevel; i++)
            str += "|  ";

        str += " ";
        str += msg;
        str += " ";
//         str += logCallStack();

        #ifdef LINE_FILE
        str += " at line ";
        str += StringUtil::toString((unsigned int)line);
        str += " from file ";
        str += file;
        #endif
        str += StringUtil::EOL;

        mTextWriter->writeLine( str );    
    }
    //-------------------------------------------------------------------------
    void TextLogger::update(float time, int frame)
    {
        RWMutex::ScopedLock lock(mMutex, false);

        String str( getDebugLine() );

        if (mLines == mMaxLines)
        {
            str += "======== Logging stopped : Too much lines: > ";
            str += StringUtil::toString( mLines );
            str += " ========";
            str += StringUtil::EOL;
            mTextWriter->writeLine( str );
            return;
        }

        str += "--------  Time since last update: ";
        str += StringUtil::toString( (Real) time );
        str += " ms";
        if (frame >= -1)
        {
            str += " - Frame: ";
            str += StringUtil::toString( frame );
        }
        str += " --------";
        str += StringUtil::EOL;
        mTextWriter->writeLine( str );
    }
    //-------------------------------------------------------------------------
    void TextLogger::addLines(unsigned int lines)
    {
        RWMutex::ScopedLock lock(mMutex, false);
        mMaxLines += lines;

        String str( getDebugLine() );
        str += "--------  Adding ";
        str += StringUtil::toString(lines);
        str += " lines --------";
        str += StringUtil::EOL;
        mTextWriter->writeLine( str );
    }
    //-------------------------------------------------------------------------
    void TextLogger::printHeader(const String& header)
    {
        String str = header;
        str += StringUtil::EOL;
        str += "Logged the ";
        str += getDate();
        str += " at ";
        str += getTime();
        str += StringUtil::EOL;
        str += "--------------------------------------------------";
        str += StringUtil::EOL;
        
        mTextWriter->writeLine( str );
    }
    //-------------------------------------------------------------------------
    void TextLogger::printFooter(const String& footer)
    {
        String str( getDebugLine() );
        str += "--------------- Log file closed ------------------------";
        str += StringUtil::EOL;
        mTextWriter->writeLine( str );
    }
    //-------------------------------------------------------------------------
    const String TextLogger::getDebugLine()
    {
        mLines = LogManager::getSingletonPtr()->getCount();
        String str(StringUtil::toString(mLines, 4, '0'));
        str += " ";
        str += getTime();
        str += " ";
        int threadId = (Poco::Thread::current() == 0)?0:Poco::Thread::current()->id();
        str += StringUtil::toString(threadId, 2, '0'); // < 99 threads!
        str += " ";
        return str;
    }
    //------------------------------------------------------------------------
}
