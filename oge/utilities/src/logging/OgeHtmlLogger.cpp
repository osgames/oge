/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/logging/OgeHtmlLogger.h"
#include "oge/logging/OgeLogManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    void HtmlLogger::logMessage(int level, 
        const String& msg, const char* file, const unsigned long line)
    {
        if (mLines > mMaxLines)
            return;

        RWMutex::ScopedLock lock(mMutex, false);

        String str( getDebugLine() );

        if (mLines == mMaxLines)
        {
            str += " ======== Logging stopped : Too much lines: > ";
            str += StringUtil::toString( mLines );
            str += " ========<br>";
            str += StringUtil::EOL;
            mTextWriter->writeLine( str );
            return;
        }
        
        str += "</font> ";

        // Indent based on the stack level
        for(unsigned int i = 0; i < mPreviousStackLevel; i++)
            str += "|  ";

        str += " ";
        str += getHtmlFont(level);
        str += msg;
        str += "</font> ";
        str += StringUtil::EOL;

//         str += logCallStack();

    #ifdef LINE_FILE
    #ifdef LOG_WITHOUT_TOOLTIP
        str += " at line ";
        str += StringUtil::toString((unsigned int)line);
        str += " from file ";
        str += file;
        str += </br>;
        str += StringUtil::EOL;
    #else
        //mFileLog << "(<a href=\"file://" << file << "\" title=\"" << "line "
        //    << line << " in " << file << "\">line " << line
        //    << "</a>)</br>"<< std::endl;
        str += "(<a href=\"file://";
        str += file;
        str += "\" title=\"line ";
        str += StringUtil::toString((unsigned int)line);
        str += " in ";
        str += file;
        str += "\">line ";
        str += StringUtil::toString((unsigned int)line);
        str += "</a>)</br>";
        str += StringUtil::EOL;
    #endif
    #endif

        mTextWriter->writeLine( str );    
    }
    //-------------------------------------------------------------------------
    void HtmlLogger::update(float time, int frame)
    {
        RWMutex::ScopedLock lock(mMutex, false);

        String str( getDebugLine() );

        if (mLines == mMaxLines)
        {
            str += " ======== Logging stopped : Too much lines: > ";
            str += StringUtil::toString( mLines );
            str += " ========<br>";
            str += StringUtil::EOL;
            mTextWriter->writeLine( str );
            return;
        }
        
        str += " --------  Time since last update: ";
        str += StringUtil::toString( (Real) time );
        str += " ms";
        if (frame >= -1)
        {
            str += " - Frame: ";
            str += StringUtil::toString( frame );
        }
        str += " --------</br>";
        str += StringUtil::EOL;
        mTextWriter->writeLine( str );
    }
    //-------------------------------------------------------------------------
    void HtmlLogger::addLines(unsigned int lines)
    {
        RWMutex::ScopedLock lock(mMutex, false);
        mMaxLines += lines;

        String str( getDebugLine() );
       
        str += " --------  Adding ";
        str += StringUtil::toString(lines);
        str += " lines --------<br>";
        str += StringUtil::EOL;
        mTextWriter->writeLine( str );
    }
    //-------------------------------------------------------------------------
    void HtmlLogger::printHeader(const String& header)
    {
        String str("<header></header><body>");
        str += StringUtil::EOL;
        str += "<font style=\"FONT-FAMILY: \'Courier New\'\" size=2></br>";
        str += StringUtil::EOL;
        str += header;
        str += "</br>";
        str += StringUtil::EOL;

        str += "</br>Legend: ";
        str += getHtmlFont(LogManager::LOGERROR)   + "Error"      + "</font>, ";
        str += getHtmlFont(LogManager::LOGWARNING) + "Warning"    + "</font>, ";
        str += getHtmlFont(LogManager::LOGNORMAL)  + "Normal"     + "</font>, ";
        str += getHtmlFont(LogManager::LOGINFO)    + "Information"+ "</font>, ";
        str += getHtmlFont(LogManager::LOGDEBUG)   + "Debug"      + "</font>, ";
        str += getHtmlFont(LogManager::LOGVERBOSE) + "Verbose"    + "</font>.</br>" + StringUtil::EOL;

        str += "</br>Logged the ";
        str += getDate();
        str += " at ";
        str += getTime();
        str += StringUtil::EOL;

        str += "</br>--------------------------------------------------</br>";
        str += StringUtil::EOL;
        
        mTextWriter->writeLine( str );
    }
    //-------------------------------------------------------------------------
    void HtmlLogger::printFooter(const String& footer)
    {
        String str( getDebugLine() );
        str += " --------------- Log file closed --------------------------</br></br>";
        str += StringUtil::EOL;
        mTextWriter->writeLine( str );
    }
    //-------------------------------------------------------------------------
    const String HtmlLogger::getHtmlFont(int level) const
    {
        String str;
        switch (level)
        {
            case LogManager::LOGERROR:
                str = "<font color=\"white\" style=\"FONT-FAMILY: \'Courier New\';BACKGROUND-COLOR:red\" size=2>";
                break;
            case LogManager::LOGWARNING:
                str = "<font color=\"white\" style=\"FONT-FAMILY: \'Courier New\';BACKGROUND-COLOR:orange\" size=2>";
                break;
            case LogManager::LOGNORMAL:
                str = "<font color=\"green\" style=\"FONT-FAMILY: \'Courier New\';BACKGROUND-COLOR:white\" size=2>";
                break;
            case LogManager::LOGINFO:
                str = "<font color=\"blue\" style=\"FONT-FAMILY: \'Courier New\';BACKGROUND-COLOR:white\" size=2>";
                break;
            case LogManager::LOGDEBUG:
                str = "<font color=\"violet\" style=\"FONT-FAMILY: \'Courier New\';BACKGROUND-COLOR:white\" size=2>";
                break;
            case LogManager::LOGVERBOSE:
                str = "<font color=\"gray\" style=\"FONT-FAMILY: \'Courier New\';BACKGROUND-COLOR:white\" size=2>";
                break;
            default:
                str = "<font color=\"black\" style=\"FONT-FAMILY: \'Courier New\';BACKGROUND-COLOR:white\" size=2>";
        }

        return str;
    }
    //------------------------------------------------------------------------
    const String HtmlLogger::getDebugLine()
    {
        mLines = LogManager::getSingletonPtr()->getCount();
        String str;
        str += getHtmlFont(0); // default font
        str += StringUtil::toString(mLines, 4, '0');
        str += " ";
        str += getTime();
        str += " ";
        int threadId = (Poco::Thread::current() == 0)?0:Poco::Thread::current()->id();
        str += StringUtil::toString(threadId, 2, '0');
        str += " ";
        return str;
    }
    //------------------------------------------------------------------------
}
