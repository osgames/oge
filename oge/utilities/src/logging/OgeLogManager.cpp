/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/logging/OgeLogManager.h"
#include "oge/datastreams/OgeFileSystemStream.h"
#include "oge/datastreams/OgeDataStream.h"
#include "oge/logging/OgeHtmlLogger.h"
#include "oge/logging/OgeTextLogger.h"
#include "oge/logging/OgeTabLogger.h"
#include "oge/logging/OgeConsoleLogger.h"
#include "oge/logging/OgeGuiConsoleLogger.h"

namespace oge
{
    //------------------------------------------------------------------------
    template<> LogManager* Singleton<LogManager>::mSingleton = 0;
    
    LogManager* LogManager::getSingletonPtr()
    {
        return mSingleton;
    }
    LogManager& LogManager::getSingleton()
    {
        assert(mSingleton);
        return *mSingleton;
    }
    
    LogManager::EventLogFN::EventLogFN(const char* function)
    {
        mSingleton->pushFunction(function);
    }
    
    LogManager::EventLogFN::~EventLogFN()
    {
        mSingleton->popFunction();
    }    
    
    //------------------------------------------------------------------------
    Atomic<unsigned int> LogManager::mLogCount = 0;
    //------------------------------------------------------------------------
    LogManager::LogManager(LogType type) :
        mPreviousStackLevel(0), maxLogCount(1000), mLogType(type), 
        mHeaderPresent(true), mStreams(0)
    {
        //ogeInitMemoryCheck();
        mLogCount = 0;
        mCallStack.reserve(32);
    }
    //------------------------------------------------------------------------
    LogManager::~LogManager()
    {
        closeLogs();
    }
    //------------------------------------------------------------------------
    LogManager* LogManager::createSingleton(LogType type)
    {
        if (mSingleton == 0)
            mSingleton = new LogManager(type);
        return mSingleton;
    }
    //------------------------------------------------------------------------
    void LogManager::destroySingleton()
    {
        if (mSingleton != 0)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //------------------------------------------------------------------------
    Logger* LogManager::getLogger( int threadId )
    {
        if (threadId < 0)
            return 0;

        // If one thread only it returns the first created
        if (mOneThreadStream)
        {
            if (mLoggers.size() > 0)
                return mLoggers.begin()->second;
        }

        // One logger per stream
        std::map<int, Poco::SharedPtr<Logger> >::iterator iter = mLoggers.find(threadId);
        if (iter == mLoggers.end())
            return 0;
        return iter->second;
    }
    //------------------------------------------------------------------------
    Logger* LogManager::createLogger()
    {
        // Get the thread id and use it to find if a logger already exist.
        int threadId = (Poco::Thread::current() == 0) ? 0 : Poco::Thread::current()->id();
        Logger* logger = getLogger( threadId );
        if (0 != logger)
        {
            return logger;
        }

        // Don't exist hence create it
        switch (mLogType)
        {
            case LogManager::LOGTYPE_CONSOLE:
            {
                if (mStreams == 0)
                {
                    logger = new ConsoleLogger();
                    mLoggers.insert( std::make_pair( threadId, logger ));
                }
                else
                {
                    // WRONG Need to be done
                    for (unsigned int i=0; i<mStreams->size(); i++)
                    {
                        logger = new TextLogger( (*mStreams)[i], StringUtil::toString( i, 3, '0') );
                        mLoggers.insert( std::make_pair( threadId, logger ));
                    }
                }
                break;
            }
            case LogManager::LOGTYPE_GUICONSOLE:
            {
                if (mStreams == 0)
                {
                    logger = new GuiConsoleLogger();
                    mLoggers.insert( std::make_pair( threadId, logger ));
                }
                else
                {
                    // WRONG Need to be done
                    for (unsigned int i=0; i<mStreams->size(); i++)
                    {
                        logger = new TextLogger( (*mStreams)[i], StringUtil::toString( i, 3, '0') );
                        mLoggers.insert( std::make_pair( threadId, logger ));
                    }
                }
                break;
            }
            case LogManager::LOGTYPE_TEXT:
            {
                if (mStreams == 0)
                {
                    String filename(mFilename);
                    filename += StringUtil::toString(threadId);
                    filename += ".txt";

                    DataStreamPtr stream = createDataStream( filename );
                    if (0 == stream)
                        return 0;

                    logger = new TextLogger( stream, "000" );
                    mLoggers.insert( std::make_pair( threadId, logger ));
                }
                else
                {
                    // WRONG Need to be done
                    for (unsigned int i=0; i<mStreams->size(); i++)
                    {
                        logger = new TextLogger( (*mStreams)[i], StringUtil::toString( i, 3, '0') );
                        mLoggers.insert( std::make_pair( threadId, logger ));
                    }
                }
                break;
            }
            case LogManager::LOGTYPE_TAB:
            {
                if (mStreams == 0)
                {
                    String filename(mFilename);
                    filename += StringUtil::toString(threadId);
                    filename += ".csv";

                    DataStreamPtr stream = createDataStream( filename );
                    if (0 == stream)
                        return 0;
                    logger = new TabLogger( stream, "000" );
                    mLoggers.insert( std::make_pair( threadId, logger ));
                }
                else
                {
                    // WRONG Need to be done
                    for (unsigned int i=0; i<mStreams->size(); i++)
                    {
                        logger = new TabLogger( (*mStreams)[i], StringUtil::toString( i, 3, '0') );
                        mLoggers.insert( std::make_pair( threadId, logger ));
                    }
                }
                break;
            }
            case LogManager::LOGTYPE_HTML:
            default:
            {
                if (mStreams == 0)
                {
                    String filename(mFilename);
                    filename += StringUtil::toString(threadId);
                    filename += ".htm";

                    DataStreamPtr stream = createDataStream( filename );
                    if (0 == stream)
                        return 0;
                    logger = new HtmlLogger( stream, "000" );
                    mLoggers.insert( std::make_pair( threadId, logger ));
                }
                else
                {
                    // WRONG Need to be done
                    for (unsigned int i=0; i<mStreams->size(); i++)
                    {
                        logger = new HtmlLogger( (*mStreams)[i], StringUtil::toString( i, 3, '0') );
                        mLoggers.insert( std::make_pair( threadId, logger ));
                    }
                }
            }
        }

        logger->printHeader( mHeader );
        return logger;
    }
    //------------------------------------------------------------------------
    void LogManager::initLogs(bool header, bool oneThreadStream, 
        const String& filename, std::vector<DataStreamPtr>* streams)
    {
        mHeaderPresent = header;
        mOneThreadStream = oneThreadStream;
        mFilename = filename;
        mStreams = streams;
    }
    //------------------------------------------------------------------------
    void LogManager::closeLogs()
    {
        printFooters();
        mLoggers.clear();
    }
    //------------------------------------------------------------------------
    DataStreamPtr LogManager::createDataStream( const String& streamname )
    {
        FileSystemStream* stream = new FileSystemStream();

        // We flush to ensure all is written until a crash
        if (!stream->openWrite( streamname, false, true, true, false ))
        {
            // NEXT Should we abort and throw an exception as all LOG will fail?
            delete stream;
            return DataStreamPtr();
        }
        return DataStreamPtr(stream);
    }
    //------------------------------------------------------------------------
    void LogManager::printHeaders()
    {
        for(unsigned int i = 0; i < mLoggers.size(); ++i )
        {
            mLoggers[i]->printHeader( mHeader );
        }
    }
    //------------------------------------------------------------------------
    void LogManager::printFooters()
    {
        for(unsigned int i = 0; i < mLoggers.size(); ++i )
        {
            mLoggers[i]->printFooter();
        }
    }
    //------------------------------------------------------------------------
    void LogManager::setHeader(const String& header, bool detailed,
        const String& versionName, const String& versionNb, 
        const String& versionRevision)
    {
        if (!detailed)
            mHeader = header;
        else
        {
            #if defined(_DEBUG)
            #    define TEMP_BUILD "debug"
            #elif defined(NDEBUG)
            #    define TEMP_BUILD "release"
            #else
            #    define TEMP_BUILD "Error: neither _DEBUG nor NDEBUG defined!"
            #endif

            // NEXT Put this code in the loggers, create a temporary instance
            //      and use a method   string createHeader(); to set mHeader
            switch (mLogType)
            {
                case LogManager::LOGTYPE_HTML:
                {
                    String str("</br>");
                    str += header;
                    str += "</br>";
                    str += StringUtil::EOL;
                    str += "</br>Version name &nbsp;&nbsp;&nbsp;&nbsp;: ";
                    str += versionName;
                    str += StringUtil::EOL;
                    str += "</br>Version number &nbsp;&nbsp;: ";
                    str += versionNb;
                    str += StringUtil::EOL;
                    str += "</br>Version revision : ";
                    str += versionRevision;
                    str += StringUtil::EOL;
                    str += "</br>Version build &nbsp;&nbsp;&nbsp;: ";
                    str += TEMP_BUILD;
                    str += "</br>";
                    str += StringUtil::EOL;

                    str += "</br>Compiler &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp: ";
                    str += StringUtil::toString( OGE_COMPILER );
                    str += " - See OgeUtilitiesPrerequisites.h";
                    str += StringUtil::EOL;
                    str += "</br>Platform &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp: ";
                    str += StringUtil::toString( OGE_PLATFORM );
                    str += " - See OgeUtilitiesPrerequisites.h";
                    str += StringUtil::EOL;
                #ifdef OGE_DOUBLE
                    str += "</br>Precision &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: double</br>";
                    str += StringUtil::EOL;
                #else
                    str += "</br>Precision &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: float</br>";
                    str += StringUtil::EOL;
                #endif
                    mHeader = str;
                    break;
                }
                case LogManager::LOGTYPE_TAB:
                {
                    String str;
                    str += header;
                    str += StringUtil::EOL;
                    str += StringUtil::EOL;
                    str += "Version name\t";
                    str += versionName;
                    str += StringUtil::EOL;
                    str += "Version number\t";
                    str += versionNb;
                    str += StringUtil::EOL;
                    str += "Version revision\t";
                    str += versionRevision;
                    str += StringUtil::EOL;
                    str += "Version build\t";
                    str += TEMP_BUILD;
                    str += StringUtil::EOL;

                    str += "Compiler\t";
                    str += StringUtil::toString( OGE_COMPILER );
                    str += StringUtil::EOL;
                    str += "Platform\t";
                    str += StringUtil::toString( OGE_PLATFORM );
                    str += StringUtil::EOL;
                #ifdef OGE_DOUBLE
                    str += "Precision\tdouble";
                    str += StringUtil::EOL;
                #else
                    str += "Precision\tfloat";
                    str += StringUtil::EOL;
                #endif
                    mHeader = str;
                    break;
                }
                case LogManager::LOGTYPE_TEXT:
                default:
                {
                    String str;
                    str += header;
                    str += StringUtil::EOL;
                    str += StringUtil::EOL;
                    str += "Version name     : ";
                    str += versionName;
                    str += StringUtil::EOL;
                    str += "Version number   : ";
                    str += versionNb;
                    str += StringUtil::EOL;
                    str += "Version revision : ";
                    str += versionRevision;
                    str += StringUtil::EOL;
                    str += "Version build    : ";
                    str += TEMP_BUILD;
                    str += StringUtil::EOL;

                    str += "Compiler         : ";
                    str += StringUtil::toString( OGE_COMPILER );
                    str += " - See OgeUtilitiesPrerequisites.h";
                    str += StringUtil::EOL;
                    str += "Platform         : ";
                    str += StringUtil::toString( OGE_PLATFORM );
                    str += " - See OgeUtilitiesPrerequisites.h";
                    str += StringUtil::EOL;
                #ifdef OGE_DOUBLE
                    str += "Precision        : double";
                    str += StringUtil::EOL;
                #else
                    str += "Precision        : float";
                    str += StringUtil::EOL;
                #endif
                    mHeader = str;
                    break;
                }
            }

            #undef TEMP_BUILD
        }
    }
    //------------------------------------------------------------------------
    void LogManager::addLines(unsigned int lines)
    {
        for(unsigned int i = 0; i < mLoggers.size(); ++i )
        {
            mLoggers[i]->addLines(lines);
        }
    }
    //------------------------------------------------------------------------
    void LogManager::update(float timeDelta, 
        unsigned int frame)
    {
        for(unsigned int i = 0; i < mLoggers.size(); ++i )
        {
            mLoggers[i]->update(timeDelta, frame);
        }
    }
    //------------------------------------------------------------------------
    void LogManager::pushFunction(const char* function)
    {
//        RWMutex::ScopedLock lock(mMutex, false);
//        mCallStack.push_back(function);
    }
    //------------------------------------------------------------------------
    void LogManager::popFunction()
    {
//        RWMutex::ScopedLock lock(mMutex, false);
//        if(mCallStack.empty())
//            return;
//        mCallStack.pop_back();
//        if(mPreviousStackLevel >= mCallStack.size())
//            logCallStack();
    }
    //------------------------------------------------------------------------
    //void LogManager::logCallStack()
    //{
    //    unsigned int currentStackLevel = (unsigned int) mCallStack.size(); // NEXT can be unsigned long

    //    while(mPreviousStackLevel < currentStackLevel)
    //    {
    //        htmlFunctionStart(mCallStack[mPreviousStackLevel]); // NEXT only html
    //        mPreviousStackLevel++;
    //    }

    //    while(mPreviousStackLevel > currentStackLevel)
    //    {
    //        htmlFunctionEnd(); // NEXT only html
    //        mPreviousStackLevel--;
    //    }
    //}
    ////------------------------------------------------------------------------
    //void LogManager::writeIndent()
    //{
    //    RWMutex::ScopedLock lock(mMutex, false);
    //    if (mLogType == LOGTYPE_HTML)
    //    {
    //        for(unsigned int i = 0; i < mPreviousStackLevel; i++)
    //            mFileLog << "|&nbsp;&nbsp;";
    //    }
    //    else if (mLogType == LOGTYPE_XML)
    //    {
    //        // NEXT
    //    }
    //    else
    //    {
    //        for(unsigned int i = 0; i < mPreviousStackLevel; i++)
    //            mFileLog << "|  ";
    //    }
    //}
    ////------------------------------------------------------------------------
    //void LogManager::htmlFunctionStart(const char* function)
    //{
    //    // Replace "#0002 14:21:24 :" = 17 non blanck space
    //    mFileLog << "<font style=\"FONT-FAMILY: \'Courier New\'\" size=2>" << std::endl;
    //    mFileLog << "</br>";
    //    mFileLog << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    //    writeIndent();
    //    mFileLog << "in " << function ;
    //    mFileLog << "</font>" << std::endl;
    //    mFileLog.flush();
    //}
    ////------------------------------------------------------------------------
    //void LogManager::htmlFunctionEnd()
    //{
    // //   writeIndent();
    //}

    //-------------------------------------------------------------------------
} // namespace
