/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/logging/OgeConsoleLogger.h"
#include "oge/logging/OgeLogManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    void ConsoleLogger::logMessage(int level, const String& msg, 
        const char* file, const unsigned long line)
    {
        if (mLines > mMaxLines)
            return;

        RWMutex::ScopedLock lock(mMutex, false);

        std::cout << getDebugLine();
        if (mLines == mMaxLines)
        {
            std::cout << "======== Logging stopped : Too much lines: > "
                << mLines << " ========" << std::endl;
            return;
        }
        for (unsigned int i = 0; i < mPreviousStackLevel; i++)
            std::cout << "|  ";
        std::cout << " " << msg << " ";
        
        #ifdef LINE_FILE
        std::cout << " at line " << line << " from file " << file;
        #endif
        std::cout << std::endl;
    }
    //-------------------------------------------------------------------------
    void ConsoleLogger::update(float time, int frame)
    {
        RWMutex::ScopedLock lock(mMutex, false);

        std::cout << getDebugLine();
        if (mLines == mMaxLines)
        {
            std::cout << "======== Logging stopped : Too much lines: > "
                << mLines << " ========" << std::endl;
            return;
        }
        std::cout << "--------  Time since last update: " << time << " ms";
        if (frame >= -1)
        {
            std::cout << " - Frame: " << frame;
        }
        std::cout << std::endl;
    }
    //-------------------------------------------------------------------------
    void ConsoleLogger::addLines(unsigned int lines)
    {
        RWMutex::ScopedLock lock(mMutex, false);
        mMaxLines += lines;
        std::cout << getDebugLine() << "--------  Adding " << lines
            << " lines --------" << std::endl;
    }
    //-------------------------------------------------------------------------
    void ConsoleLogger::printHeader(const String& header)
    {
        std::cout << "Logged the " << getDate() << " at " << getTime() << std::endl;
        std::cout << "--------------------------------------------------" << std::endl;
    }
    //-------------------------------------------------------------------------
    void ConsoleLogger::printFooter(const String& footer)
    {
        std::cout << getDebugLine() 
            << "--------------- Log file closed ------------------------" 
            << std::endl;
    }
    //-------------------------------------------------------------------------
    const String ConsoleLogger::getDebugLine()
    {
        mLines = LogManager::getCount();
        String str(StringUtil::toString(mLines, 4, '0'));
        str += " ";
        str += getTime();
        str += " ";
        int threadId = (Poco::Thread::current() == 0)?0:Poco::Thread::current()->id();
        str += StringUtil::toString(threadId, 2, '0'); // < 99 threads!
        str += " ";
        return str;
    }
    //------------------------------------------------------------------------
}
