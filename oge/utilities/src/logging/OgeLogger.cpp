/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/logging/OgeLogger.h"
#include "oge/logging/OgeLogManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    Logger::Logger() :
        mLines(0), mMaxLines(5000), mPreviousStackLevel(0)
    {
    }
    //-------------------------------------------------------------------------
    Logger::Logger(DataStreamPtr stream, String id) :
        mStream(stream), mId(id), mLines(0), mMaxLines(5000), mPreviousStackLevel(0)
    {
        mTextWriter = new TextWriter(stream);
    }
    //-------------------------------------------------------------------------
    void Logger::setLogger(DataStreamPtr stream, String id)
    {
        // close any existing streams
        if(mStream) {
            mStream->close();
        }

        // remove any existing text writers
        if(mTextWriter) {
            delete mTextWriter;
        }

        mStream = stream;
        mId = id;
        mTextWriter = new TextWriter(stream);
    }
    //-------------------------------------------------------------------------
    Logger* Logger::getInstance()
    {
        // The heart of the log manager! 
        // The thread local is local to each thread hence it will test if a logger
        // was set if not it ask the log manager for a new instance OR the same
        // instance in case we have asked "one stream only".
        Poco::ThreadLocal<LocalLogger> mLocalLogger;

        if (0 == mLocalLogger->logger)
        {
            mLocalLogger->logger = oge::LogManager::getSingletonPtr()->createLogger();
        }
        return mLocalLogger->logger;
    }

    //-------------------------------------------------------------------------
    Logger::~Logger()
    {
        if (mStream)
            mStream->close();

        delete mTextWriter;
    }
	//-------------------------------------------------------------------------
	const String Logger::getFilename() const
	{
		if(mStream) {
			return mStream->getName();
		}

		return "";
	}
	//-------------------------------------------------------------------------
	DataStreamPtr Logger::getDataStream()
	{
		return mStream;
	}
    //-------------------------------------------------------------------------
    const String Logger::getDate() const
    {
        struct tm newtime;
        time_t ctTime;
        time(&ctTime);

    #if OGE_COMPILER == OGE_COMPILER_MSVC && OGE_COMP_VER >= 1400
        localtime_s(&newtime, &ctTime);
    #else
        newtime = *localtime(&ctTime);
    #endif

        String str;
        str += StringUtil::toString(newtime.tm_mday, 2, '0');
        str += ".";
        str += StringUtil::toString(newtime.tm_mon, 2, '0');
        str += ".";
        str += StringUtil::toString(newtime.tm_year+1900);

        return str;
    }
    //-------------------------------------------------------------------------
    const String Logger::getTime() const
    {
        struct tm newtime;
        time_t ctTime;
        time(&ctTime);

    #if OGE_COMPILER == OGE_COMPILER_MSVC && OGE_COMP_VER >= 1400
        localtime_s(&newtime, &ctTime);
    #else
        newtime = *localtime(&ctTime);
    #endif

        String str;
        str += StringUtil::toString(newtime.tm_hour, 2, '0');
        str += ":";
        str += StringUtil::toString(newtime.tm_min, 2, '0');
        str += ":";
        str += StringUtil::toString(newtime.tm_sec, 2, '0');

        return str;
    }
    //-------------------------------------------------------------------------
    void Logger::logMessage( int level, bool test, const String& msg, 
        const char* file, const unsigned long line)
    {
        if (test)
            logMessage(level, msg, file, line);
    }
    //-------------------------------------------------------------------------
    void Logger::_update(float time, int frame)
    {
        if (0 == LogManager::getSingletonPtr())
            this->update(time, frame);
        else
            LogManager::getSingletonPtr()->update(time, frame);
    }
    //-------------------------------------------------------------------------
    void Logger::_addLines(unsigned int lines)
    {
        if (0 == LogManager::getSingletonPtr())
            this->addLines(lines);
        else
            LogManager::getSingletonPtr()->addLines(lines);
    }
    //-------------------------------------------------------------------------
}
