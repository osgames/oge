/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/memory/OgeMemoryTracker.h"
#include <sstream>

#if OGE_USE_MEMORY_TRACKER

namespace oge
{
    //-------------------------------------------------------------------------
    MemoryTracker MemoryTracker::mSingleton;
    //-------------------------------------------------------------------------
    MemoryTracker::MemoryTracker() : mFilename("oge_memory_leaks.txt"),
        mUseStandardOutput(false), mUseFileOutput(false)
    {

    }
    //-------------------------------------------------------------------------
    MemoryTracker::~MemoryTracker()
    {
        reportMemoryLeaks();
    }
    //-------------------------------------------------------------------------
    void MemoryTracker::addAllocation(void* pointer)
    {
        AllocationMap::accessor entry;

        bool success = mAllocations.insert(entry, pointer);
        assert(success && "Two allocations have been made with the same address");

        Allocation& alloc = entry->second;
        alloc.file = "";
        alloc.func = "";
        alloc.line = 0;
    }
    //-------------------------------------------------------------------------
    void MemoryTracker::addAllocation(void* pointer, const char* file, 
        const char* func, size_t line)
    {
        AllocationMap::accessor entry;

        bool success = mAllocations.insert(entry, pointer);
        assert(success && "Two allocations have been made with the same address");

        Allocation& alloc = entry->second;
        alloc.file = file;
        alloc.func = func;
        alloc.line = line;
    }
    //-------------------------------------------------------------------------
    void MemoryTracker::removeAllocation(void* pointer)
    {
        bool success = mAllocations.erase(pointer);
        assert(success && "Attempted to delete memory that has not been previously allocated");
    }
    //-------------------------------------------------------------------------
    bool MemoryTracker::reportMemoryLeaks()
    {
        using namespace std;
        ostringstream stream;

        bool leaks = !mAllocations.empty();

        if(!leaks)
        {
            stream << "No memory leaks detected";
        }
        else
        {
            stream << "The following memory leaks were detected:" << endl << endl;
            AllocationMap::iterator iter = mAllocations.begin();
            for (; iter != mAllocations.end(); ++iter)
            {
                void* pointer = iter->first;
                const Allocation& alloc = iter->second;

                stream << "Address: " << std::hex << pointer;
                if (*(alloc.file))
                {
                    stream <<   " in \"" << alloc.file << 
                                "\", (function \"" << alloc.func << 
                                "\", line " << alloc.line << ")";
                }

                stream << endl;
            }
        }

        if(mUseFileOutput)
        {
            ofstream file;
            file.open(mFilename.c_str());
            file << stream.str();
            file.close();
        }

        if (mUseStandardOutput)
        {
            cout << stream.str() << endl << "Press enter to continue...";
            cin.get();
        }

        return leaks;
    }
    //-------------------------------------------------------------------------
    void MemoryTracker::setFilename(const std::string& filename)
    {
        mFilename = filename;
    }
    //-------------------------------------------------------------------------
    void MemoryTracker::setUseStandardOutput(bool use)
    {
        mUseStandardOutput = use;
    }
    //-------------------------------------------------------------------------
    void MemoryTracker::setUseFileOutput(bool use)
    {
        mUseFileOutput = use;
    }
    //-------------------------------------------------------------------------
}

#endif // if OGE_USE_MEMORY_TRACKER
