/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/resource/OgeResourceControllerOgeImpl.h"
#include "oge/resource/OgeResourceManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    ResourceControllerOgeImpl::ResourceControllerOgeImpl(const String& name) : 
        ResourceController(name)
    {

    }
    //-------------------------------------------------------------------------
    ResourceControllerOgeImpl::~ResourceControllerOgeImpl()
    {

    }
    //-------------------------------------------------------------------------
    bool ResourceControllerOgeImpl::registerManager(const String& name, 
        ResourceManager* manager)
    {
        RWMutex::ScopedLock lock(mMutex, true);

        ResourceManagerMap::iterator iter = mResourceManagers.find(name);
        if (iter != mResourceManagers.end())
            return false;

        mResourceManagers[name] = manager;
        return true;
    }
    //-------------------------------------------------------------------------
    void ResourceControllerOgeImpl::unregisterManager(const String& name)
    {
        RWMutex::ScopedLock lock(mMutex, true);

        ResourceManagerMap::iterator iter = mResourceManagers.find(name);
        if (iter != mResourceManagers.end())
        {
            mResourceManagers.erase(iter);
        }
    }
    //-------------------------------------------------------------------------
    bool ResourceControllerOgeImpl::processOperation(
        const ResourceOp& operation)
    {
        const String& managerName = operation.getManagerName();
        ResourceManager* manager = getResourceManager(managerName);

        if (!manager)
            return false;

        const String& resourceName = operation.getResourceName();

        switch(operation.getOperation())
        {
        case ResourceOp::OP_CREATE:
            {
                return ( manager->createResource(resourceName).get() != 0);
            }
            break;
        case ResourceOp::OP_LOAD:
            {
                return manager->loadResource(resourceName);
            }
            break;
        case ResourceOp::OP_RELOAD:
            {
                return manager->reloadResource(resourceName);
            }
            break;
        case ResourceOp::OP_UNLOAD:
            {
                manager->unloadResource(resourceName);
            }
            break;
        default:
            return false;
        }

        return true;
    }
    //-------------------------------------------------------------------------
    ResourceManager* ResourceControllerOgeImpl::getResourceManager(
        const String& name)
    {
        RWMutex::ScopedLock lock(mMutex, false);

        ResourceManagerMap::const_iterator iter = mResourceManagers.find(name);

        return iter != mResourceManagers.end() ? iter->second : 0;
    }
    //-------------------------------------------------------------------------
}
