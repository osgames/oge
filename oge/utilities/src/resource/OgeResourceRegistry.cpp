/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/resource/OgeResourceRegistry.h"
#include "oge/resource/OgeResourceController.h"

namespace oge
{
    //-------------------------------------------------------------------------
    template<> ResourceRegistry* Singleton<ResourceRegistry>::mSingleton = 0;
    ResourceRegistry* ResourceRegistry::getSingletonPtr() 
    { 
        return mSingleton; 
    }
    
    ResourceRegistry& ResourceRegistry::getSingleton()
    {
        assert(mSingleton);
        return *mSingleton;
    }
    //-------------------------------------------------------------------------
    ResourceRegistry::ResourceRegistry()
    {

    }
    //-------------------------------------------------------------------------
    ResourceRegistry::~ResourceRegistry()
    {

    }
    //-------------------------------------------------------------------------
    bool ResourceRegistry::registerController(ResourceController* controller)
    {
        const String& name = controller->getName();
        RWMutex::ScopedLock lock(mMutex, true);

        ResourceControllerMap::iterator iter = mResourceControllers.find(name);
        if (iter != mResourceControllers.end())
            return false;

        mResourceControllers[name] = controller;
        return true;
    }
    //-------------------------------------------------------------------------
    void ResourceRegistry::unregisterController(const String& name)
    {
        RWMutex::ScopedLock lock(mMutex, true);
        
        ResourceControllerMap::iterator iter = mResourceControllers.find(name);
        if (iter != mResourceControllers.end())
        {
            mResourceControllers.erase(iter);
        }
    }
    //-------------------------------------------------------------------------
    bool ResourceRegistry::processOperation(const String& controllerName,
        const ResourceOp& operation)
    {
        RWMutex::ScopedLock lock(mMutex, false);

        ResourceController* controller = getController(controllerName);
        if (!controller)
            return false;

        return controller->processOperation(operation);
    }  
    //-------------------------------------------------------------------------
    bool ResourceRegistry::processOperation(ResourceOp::OP op, 
        const String& controllerName, const String& managerName, 
        const String& resourceName)
    {
        RWMutex::ScopedLock lock(mMutex, false);
        
        ResourceController* controller = getController(controllerName);
        if (!controller)
            return false;

        ResourceOp operation(op, managerName, resourceName);
        return controller->processOperation(operation);
    }
    //-------------------------------------------------------------------------
    void ResourceRegistry::processGroup(const Group& group)
    {
        RWMutex::ScopedLock lock(mMutex, false);

        const Group::OpMap& ops = group.getGroups();
        Group::OpMap::const_iterator iter = ops.begin();
        for(; iter != ops.end(); ++iter)
        {
            const String& controllerName = iter->first;
            const ResourceOp::Group& resOpGroup = iter->second;

            ResourceController* controller = getController(controllerName);
            if (controller)
            {
                controller->processOperations(resOpGroup);
            }
        }
    }
    //-------------------------------------------------------------------------
    ResourceController* ResourceRegistry::getController(const String& name)
    {
        ResourceControllerMap::const_iterator iter = 
            mResourceControllers.find(name);

        return iter != mResourceControllers.end() ? iter->second : 0;
    }
    //-------------------------------------------------------------------------
    void ResourceRegistry::Group::addResourceOpGroup(const String& groupName, 
        const ResourceOp::Group& group)
    {
        OpMap::iterator iter = mGroups.find(groupName);
        if (iter == mGroups.end())
        {
            mGroups[groupName] = group;
        }
        else
        {
            iter->second.addOperations(group);
        }
    }
    //-------------------------------------------------------------------------
    ResourceRegistry* ResourceRegistry::createSingleton()
    {
        if (!mSingleton)
            mSingleton = new ResourceRegistry();

        return mSingleton;
    }
    //-------------------------------------------------------------------------
    void ResourceRegistry::destroySingleton()
    {
        if (mSingleton)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    }
    //-------------------------------------------------------------------------
}
