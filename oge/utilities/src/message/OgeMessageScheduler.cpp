/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/message/OgeMessageScheduler.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/OgeString.h"

namespace oge
{

    //-------------------------------------------------------------------------
    MessageScheduler::MessageScheduler()
        : mTimeLimit(10.0), mAvgMsgProcessingTime(0.0)
    {
        addMessageHook( Message::EMPTY,
            MessageHook(this, &MessageScheduler::emptyMessageHook) );
    }
    //-------------------------------------------------------------------------
    MessageScheduler::~MessageScheduler()
    {
    }
    //-------------------------------------------------------------------------
    void MessageScheduler::setSchedulerTimeLimit(double timeLimit)
    {
        mTimeLimit = 1.0 < timeLimit ? timeLimit : 1.0;
    }
    //-------------------------------------------------------------------------
    void MessageScheduler::affectSchedulerTimeLimit(double suggestedTimeLimit)
    {
        if (suggestedTimeLimit < 1.0)
            suggestedTimeLimit = 1.0;
        mTimeLimit = 0.75 * mTimeLimit + 0.25 * suggestedTimeLimit;    // NEXT: evaluate and make better
    }
    //-------------------------------------------------------------------------
    bool MessageScheduler::processMessages(double tickTime)
    {
        RWMutex::ScopedLock lock(mSchedulerMutex, true);

        // Sort messages into a descending order so front element of the queue has
        // the most priority.
        std::sort(mMessageQueue_TimeLimited.rbegin(), mMessageQueue_TimeLimited.rend());

        // The size of the queue could be changed while processing the messages (e.g.
        // their handler methods can post other messages). So before any processing,
        // we store 'current' number of the messages in this cycle and only try to
        // respond to these requests.
        size_t numTasks = mMessageQueue_TimeLimited.size();
        double approxProcessingTime = 0;

        // Process until time limit is reached or all done. All messages with
        // immediate priority are processed, anyhow.
        // NOTE: The scheduler might be reset as a result of a message. This
        //        conditions should be checked in every iteration.
        while ( 0 < numTasks--  && !mMessageQueue_TimeLimited.empty() &&
            (Message::IMMEDIATE <= mMessageQueue_TimeLimited.front().priority
            || approxProcessingTime < mTimeLimit) )
        {
            approxProcessingTime += mAvgMsgProcessingTime;

            Task &task = mMessageQueue_TimeLimited.front();

            //--------------- Process the message --------------
            // Note that while processing, other messages could be posted and pushed to the end of the queue.
            bool allowed = true;

            // Find and invode registerd hooks for message types that has no specific receiver.
            // TODO: DOC!
            //if (task.message.getReceiver().type() != typeid(String))  // TODO: ...==0) ie: String --> size_t
            //{
                MessageHookMap::const_iterator it = mHooksMap.find(task.message.getType());
				if (mHooksMap.end() != it) {
					MessageHookVector::const_iterator vit = it->second.begin();
					for(;vit != it->second.end(); vit++) {
						allowed = (*vit)(task.message);  // NOTE: Not considered in processing time

						if(!allowed) break;
					}
				}
            //}

            if (allowed)
                _dispatchMessage(task.message);
            //--------------------------------------------------

            if (!mMessageQueue_TimeLimited.empty())
                // If MT, perhaps msgs should be poped a few ticks later, so that dispatcher sends their ptr
                mMessageQueue_TimeLimited.pop_front();
        }
        ++numTasks;    // decreased from its real value if time limit is reached.

        // Enhance the priority of unprocessed tasks (but NOT lately added messages).
        for (MessageQueue::iterator it = mMessageQueue_TimeLimited.begin();   // begins from front
            0 < numTasks-- && mMessageQueue_TimeLimited.end() != it; ++it)    // ends to back of the queue
        {
            (*it).increasePriority();
        }

        // Only one object of this class should exist per app, so static is ok
		// disabled - Alex
		/*
        static double logPeriod = tickTime;
        if (logPeriod <= tickTime)
        {
            logPeriod = tickTime + 10000;    // Log a random status every 10 sec
            LOGI( "==== Message processing stats -- Tick time: " + StringUtil::toString(Real(tickTime * 0.001))
                + "sec -- Last Avg. processing time: " + StringUtil::toString((Real)mAvgMsgProcessingTime)
                + "ms -- Current time limit: " + StringUtil::toString((Real)mTimeLimit)
                + "ms -- Remained msgs to process: " + StringUtil::toString((unsigned int) mMessageQueue_TimeLimited.size()) );
        }
		*/

        return mMessageQueue_TimeLimited.empty() ? false : true;
    }
    //-------------------------------------------------------------------------
    bool MessageScheduler::processMessageImmediate(const Message& message)
	{

        MessageHookMap::const_iterator it = mHooksMap.find(message.getType());
		if (mHooksMap.end() != it) {
			MessageHookVector::const_iterator vit = it->second.begin();
			for(;vit != it->second.end(); vit++) {
				if(!((*vit)(message))) return false;
			}
		}

		_dispatchMessage(message, true);

		return true;
	}
    //-------------------------------------------------------------------------
    void MessageScheduler::resetScheduler()
    {
        LOGI("Resetting message scheduler...");
        RWMutex::ScopedLock lock(mSchedulerMutex, true);

        mMessageQueue_TimeLimited.clear();

        LOGI("Message scheduler resetting done.");
    }
    //-------------------------------------------------------------------------
    void MessageScheduler::discardMessages(const Any& receiver)
    {
#pragma OGE_WARN("This lock should work but it seems the recursivity doesn't work properly!")
        RWMutex::ScopedLock lock(mSchedulerMutex, true);
        assert(!receiver.empty() && receiver.type() == typeid(String));

        unsigned int num = 0;
        int msgType = 0;
        for (MessageQueue::iterator it = mMessageQueue_TimeLimited.begin();
            mMessageQueue_TimeLimited.end() != it; ++it)
        {
            // TODO This part has the same problem... if ptr 0 then exception...
            if (FastAnyCast(String, (*it).message.getReceiver()) == FastAnyCast(String, receiver))
            {
                // TODO Efficient?
                (*it).message.setReceiver( Any() );
                msgType = (*it).message.getType();
                (*it).message.setType(Message::EMPTY); // Do not change the priority and so queue's order!
                ++num;
            }
        }

        LOGIC( 0 < num, StringUtil::toString(num) + " messages were discarded by their receiver. "
            "Last message type: #" + StringUtil::toString(msgType) );
    }
    //-------------------------------------------------------------------------
    bool MessageScheduler::emptyMessageHook(const Message& message)
    {
        return false;
    }
    //-------------------------------------------------------------------------
}
