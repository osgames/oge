/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/plugin/OgePluginManager.h"
#include "oge/plugin/OgePlugin.h"
#include "oge/logging/OgeLogManager.h"

#if OGE_PLATFORM == OGE_PLATFORM_APPLE
#    define OGE_PLUGIN_EXTENSION ".dylib"
#include <CoreFoundation/CoreFoundation.h>
std::string macBundlePath()
{
    char path[1024];
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    assert(mainBundle);
    
    CFURLRef mainBundleURL = CFBundleCopyBundleURL(mainBundle);
    assert(mainBundleURL);
    
    CFStringRef cfStringRef = CFURLCopyFileSystemPath( mainBundleURL, kCFURLPOSIXPathStyle);
    assert(cfStringRef);
    
    CFStringGetCString(cfStringRef, path, 1024, kCFStringEncodingASCII);
    
    CFRelease(mainBundleURL);
    CFRelease(cfStringRef);
    
    return std::string(path);
}
std::string macPluginPath()
{
    return macBundlePath() + "/Contents/Plugins/";
}
#elif OGE_PLATFORM == OGE_PLATFORM_LINUX
#    define OGE_PLUGIN_EXTENSION ".so"
#elif OGE_PLATFORM == OGE_PLATFORM_WIN32
#    define OGE_PLUGIN_EXTENSION ".dll"
#endif

namespace oge
{
    /**
     * The registration params may be received from an external plugin 
     * so it is crucial to validate it, because it was never subjected to our tests.
     *
     * @next LATER Test in all situations (linux, mac, static & dynamic)
     *       if we can make this a method of PluginManager. The reference
     *       documentation implied we can't (because of the static nature
     *       of registerPluginInstance? )
     */
    static bool isPluginValid(const unsigned char* pluginType, 
        const PluginManager::PluginRegisterParams* params)
    {
      if (!pluginType || !(*pluginType))
         return false;
      if (!params ||!params->createFct || !params->destroyFct)
        return false;
      
      return true;
    }

    //-------------------------------------------------------------------------
    template<> PluginManager* Singleton<PluginManager>::mSingleton = 0;

    PluginManager* PluginManager::createSingleton()
    {
        if (mSingleton == 0)
            mSingleton = new PluginManager();
        return mSingleton;
    };
    /**
     * Used to delete the instance.
     */
    void PluginManager::destroySingleton()
    {
        if (mSingleton != 0)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    };
    //-------------------------------------------------------------------------
    PluginManager::PluginManager()
    {
      mPlatformServices.versionMajor = 1; // TODO Use major of Utilities
      mPlatformServices.versionMinor = 0;
      mPlatformServices.mgr = this;
      //mPlatformServices.invokeService = 0; // can be populated during loadAll()
      //mPlatformServices.registerObject = registerObject;
    }
    //-------------------------------------------------------------------------
    PluginManager::~PluginManager()
    {
        unloadAll();
    }
    //-------------------------------------------------------------------------
    bool PluginManager::loadPlugin( const String& filename )
    {
        LOG("PluginManager::loadPlugin()");

        std::string plugin = filename;
        plugin += OGE_PLUGIN_EXTENSION;

        #ifdef WIN32
            HMODULE hDll = ::LoadLibraryA(plugin.c_str());
            if (hDll == 0)
            {
                DWORD error = ::GetLastError();
                String str("PluginManager::loadLibrary(");
                str += plugin;
                str += ") failed with error code: ";
                str += StringUtil::toString( (long) error );
                LOGE( str );
                return false;
            }
        #else
            #if (OGE_PLATFORM == OGE_PLATFORM_APPLE)
        
            // for macs we need the path inside the bundle
            std::string fullPath=plugin.c_str();
            fullPath = macPluginPath()+"/"+fullPath;
            void* hDll = ::dlopen(fullPath.c_str(), RTLD_NOW);
        
            #else
        
            void* hDll = ::dlopen(plugin.c_str(), RTLD_NOW);
        
            #endif
            if (!hDll) 
            {
                std::string errorString;
                std::string error;
                const char *errorCode = ::dlerror(); // !!
                if (errorCode)
                    error = errorCode;
                errorString += "PluginManager::loadLibrary("
                    + plugin + '"';
                if(error.size())
                    errorString += " with error code: " + error;
                LOGE( errorString );
                return false;
            }
        #endif

        #ifdef WIN32
            PluginInitFct initFct = (PluginInitFct) ::GetProcAddress (hDll, "pluginRegistration");
        #else
            PluginInitFct initFct = (PluginInitFct) ::dlsym (hDll, "pluginRegistration");
        #endif
        
        if (!initFct)
        {
            LOGE("Error no entry point 'PluginInitFct'?");
            return false;
        }

        if (!initFct( &mPlatformServices, hDll ))
        {
            LOGE("Error while initialising plugin");
            #ifdef WIN32
            ::FreeLibrary((HMODULE)hDll);
            #else
            ::dlclose(hDll);
            #endif
            return false;
        }

        // We could add the plugin to an application using the dll
        //    AnApplication * pAnApplication = static_cast<AnApplication*>( ptr );
        //    pAnApplication->AddExporter(pPlugin->GetExportName());

        return true;
    }
    //-------------------------------------------------------------------------
    bool PluginManager::loadStaticPlugin(PluginInitFct initFunc)
    {
        PluginManager* pm = PluginManager::createSingleton();
        if (!initFunc( &pm->mPlatformServices, 0 ))
            return false;
        return true;
    }
    //-------------------------------------------------------------------------
    bool PluginManager::registerPluginInstance(const unsigned char* pluginType, 
        const PluginRegisterParams* params)
    {
        // Check parameters
        if (!isPluginValid(pluginType, params))
            return false;

        PluginManager* pm = PluginManager::createSingleton();

        // Verify that versions match
        if (pm->mPlatformServices.versionMajor != params->versionMajor)
            return false;

        // NEXT We could create the plugin later! We do it here for... conveniance
        //      and because later you need to know the name of the plugin to call it!
        //      We could get the name from the plugin dll pluginRegistration method.
        Plugin_Params pp;
        pp.mgr = pm;
        pp.pluginType = pluginType;

        Plugin* plugin = params->createFct( &pp ); // THE PLUGIN IS CREATED HERE!
        if (!plugin)
        {
            LOGE("ERROR Couldn't create the plugin.");
            return false;
        }

        (const_cast<PluginRegisterParams*>(params))->plugin = plugin; 

        // We need to store the params (as it contains the methods
        // to create the plugin)
        String type((const char *)pluginType);
        pm->mPlugins[type] = *params;

        return true; 
    }
    //-------------------------------------------------------------------------
    void PluginManager::unloadAll()
    {
        LOG("PluginManager::unloadAll()");

        OgeHashMap<String, PluginRegisterParams>::iterator iter;

        for (iter=mPlugins.begin(); iter != mPlugins.end(); iter++)
        {
            PluginRegisterParams* params = &iter->second;
            try
            {
                if (params->destroyFct &&  params->plugin)
                    params->destroyFct(params->plugin); // TODO mh...
                else
                {
                    LOGE(String("No destroy fct or plugin instance in ")
                        + (*iter).first);
                }

                // Very important!
                #ifdef WIN32
                    if (params->hDll)
                        ::FreeLibrary((HMODULE)params->hDll);
                #else
                    if (params->hDll)
                        ::dlclose(params->hDll);
                #endif
            }
            catch(...)
            {
                LOGE(String("Unloading plugin: ") + (*iter).first );
                // Very important!
                #ifdef WIN32
                    if (params->hDll)
                        ::FreeLibrary((HMODULE)params->hDll);
                #else
                    if (params->hDll)
                        ::dlclose(params->hDll);
                #endif
            }
        }

        mPlugins.clear();
    }
    //-------------------------------------------------------------------------
    size_t PluginManager::getNumPlugins() const
    {
        return mPlugins.size();
    }
    //-------------------------------------------------------------------------
    Plugin* PluginManager::getPlugin( const String& name )
    {
        OgeHashMap<String, PluginRegisterParams>::iterator iter = mPlugins.find(name);
        if (iter == mPlugins.end())
            return 0;

        return iter->second.plugin;
    }
    //-------------------------------------------------------------------------
    bool PluginManager::initialisePlugin( const String& name )
    {
        Plugin* plugin = getPlugin(name);

        if (0 == plugin)
        {
            LOGE(String("Couldn't find a plugin named '")+name+"'");
            return false;
        }

        if (!plugin->initialise())
        {
            LOGE(String("Couldn't initialise the plugin '")+name+"'");
            return false;
        }
        return true;
    }
    //-------------------------------------------------------------------------
}
