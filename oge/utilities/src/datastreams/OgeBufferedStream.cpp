/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/datastreams/OgeBufferedStream.h"
#include "oge/containers/OgeBuffer.h"

namespace oge
{
    //-------------------------------------------------------------------------
    BufferedStream::BufferedStream() : mBuffer(0), mIsOpen(false), 
        mOwnsBuffer(false), mSize(0), mBytesBuffered(0)
    {
    }
    //-------------------------------------------------------------------------
    BufferedStream::~BufferedStream()
    {
    }
    //-------------------------------------------------------------------------
    bool BufferedStream::openRead(DataStreamPtr dataStream, Buffer* buffer)
    {
        if (mIsOpen)
            close();

        if (dataStream.get() == 0 || !dataStream->isValid() || 
            dataStream->getAccess() != ACCESS_READ)
            return false;

        mOwnsBuffer = false;

        if (buffer == 0)
        {
            buffer = new Buffer(dataStream->getSize());
            mOwnsBuffer = true;
        }
        else if (buffer->getSize() < dataStream->getSize())
        {
            return false;
        }

        mBuffer = buffer;
        mDataStream = dataStream;
        mSize = mDataStream->getSize();
        mBytesBuffered = 0;
        mAccess = ACCESS_READ;
        mIsOpen = true;

        return true;
    }
    //-------------------------------------------------------------------------
    void BufferedStream::close()
    {
        DataStream::close();

        mDataStream = 0;
        if (mOwnsBuffer && mBuffer)
        {
            delete mBuffer;
        }

        mBuffer = 0;
        mBytesBuffered = 0;
        mSize = 0;
        mOwnsBuffer = false;
        mIsOpen = false;
    }
    //-------------------------------------------------------------------------
    bool BufferedStream::isValid() const
    {
        return (mIsOpen && mDataStream.get() && mDataStream->isValid());
    }
    //-------------------------------------------------------------------------
    size_t BufferedStream::getSize() const
    {
        return mSize;
    }
    //-------------------------------------------------------------------------
    const String& BufferedStream::getName() const
    {
        return mDataStream->getName();
    }
    //-------------------------------------------------------------------------
    bool BufferedStream::isAtEndOfStream() const
    {
        if (!isValid())
            return true;

        return mDataStream->isAtEndOfStream();
    }
    //-------------------------------------------------------------------------
    size_t BufferedStream::read(char* buffer, size_t size)
    {
        if (!isValid() || mAccess != ACCESS_READ)
            return 0;

        size_t currentPosition = mDataStream->getPosition();

        // Data needed is already in the buffer
        if (currentPosition + size <= mBytesBuffered)
        {
            memcpy(buffer, mBuffer->get() + currentPosition, size);
            mDataStream->setPosition(currentPosition + size);
            return size;
        }

        // copy already buffered bytes
        size_t totalBytes = 0;
        size_t bytesAlreadyBuffered = mBytesBuffered - currentPosition;
        memcpy(buffer, mBuffer->get() + currentPosition, bytesAlreadyBuffered);
        buffer += bytesAlreadyBuffered;
        size -= bytesAlreadyBuffered;
        currentPosition += bytesAlreadyBuffered;
        totalBytes += bytesAlreadyBuffered;
        mDataStream->setPosition(currentPosition);

        // Buffer remaining bytes
        size_t bytesLeft = mSize - currentPosition;
        size_t bytesToBuffer = bytesLeft < size ? bytesLeft : size;

        size_t bytesActuallyRead = mDataStream->read(
            mBuffer->get() + currentPosition, bytesToBuffer);

        memcpy(buffer, mBuffer->get() + currentPosition, bytesActuallyRead);

        currentPosition += bytesActuallyRead;
        totalBytes += bytesActuallyRead;
        mBytesBuffered += bytesActuallyRead;

        return totalBytes;
    }
    //-------------------------------------------------------------------------
    bool BufferedStream::write(const char* buffer, size_t size)
    {
        return false;
    }
    //-------------------------------------------------------------------------
    bool BufferedStream::setPosition(size_t position)
    {
        if (position > mSize)
            return false;

        if (position <= mBytesBuffered)
            return mDataStream->setPosition(position);

        size_t bytesToBuffer = position - mBytesBuffered;
        size_t oldPosition = mDataStream->getPosition();
        if (!mDataStream->setPosition(mBytesBuffered))
        {
            mDataStream->setPosition(oldPosition);
            return false;
        }

        mDataStream->read(mBuffer->get() +  mBytesBuffered, bytesToBuffer);
        mBytesBuffered = position;

        return true;
    }
    //-------------------------------------------------------------------------
    size_t BufferedStream::getPosition() const
    {
        return mDataStream->getPosition();
    }
    //-------------------------------------------------------------------------
    DataStream::Access BufferedStream::getAccess() const
    {
        return mAccess;
    }
    //-------------------------------------------------------------------------
}
