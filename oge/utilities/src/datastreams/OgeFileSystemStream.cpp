/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/datastreams/OgeFileSystemStream.h"
#include "oge/logging/OgeLogManager.h"
#include "Poco/Exception.h"

namespace oge
{
    //-------------------------------------------------------------------------
    FileSystemStream::FileSystemStream(DataStreamOwner* owner) : 
        DataStream(owner), mSize(0), mIsOpen(false), mFlush(false)
    {
    }
    //-------------------------------------------------------------------------
    FileSystemStream::~FileSystemStream()
    {
        close();
    }
    //-------------------------------------------------------------------------
    bool FileSystemStream::openRead(const Path& filename)
    {
        if (mIsOpen)
            return false;

        mFilename = filename;
        mAccess = ACCESS_READ;

        if (!mFilename.isFile())
            return false;

        FileUtil file(mFilename);
        if (!file.exists() || !file.canRead())
            return false;

        mInputStream.open(mFilename.toString(), std::ios::in);
        if (mInputStream.fail())
            return false;

        mSize = _getSize();
        mIsOpen = true;
        return true;
    }
    //-------------------------------------------------------------------------
    bool FileSystemStream::openWrite(const Path& filename, bool append, 
        bool flush, bool truncate, bool failIfExists)
    {
        if (mIsOpen)
            return false;

        mFilename = filename;
        mAccess = ACCESS_WRITE;
        mFlush = flush;

        if (!mFilename.isFile())
            return false;

        FileUtil file(mFilename);
        bool exists = file.exists();
        if ((failIfExists && exists) || (exists && !file.canWrite()))
            return false;

        std::ios::openmode mode = std::ios::in | std::ios::out | std::ios::ate;
        if (truncate)
            mode |= std::ios::trunc;
        
		try {
			mStream.open(mFilename.toString(), mode);
		}
		catch(...) {
			return false;
		}
        if (mStream.fail())
            return false;

        mSize = _getSize();
        mIsOpen = true;
        
        setPosition(append ? mSize : 0);
       
        return true;
    }
    //-------------------------------------------------------------------------
    void FileSystemStream::close()
    {
        DataStream::close();
        mStream.close();
		mInputStream.close();
        mSize = 0;
        mFilename.clear();
        mIsOpen = false;
        mFlush = false;
    }
    //-------------------------------------------------------------------------
    bool FileSystemStream::isValid() const
    {
        return (mIsOpen);
    }
    //-------------------------------------------------------------------------
    size_t FileSystemStream::getSize() const
    {
        return mSize;
    }
    //-------------------------------------------------------------------------
    const String& FileSystemStream::getName() const
    {
        return mFilename.getFileName();
    }
    //-------------------------------------------------------------------------
    bool FileSystemStream::isAtEndOfStream() const
    {
		return mAccess == ACCESS_READ ? mInputStream.eof() : mStream.eof();
    }
    //-------------------------------------------------------------------------
    size_t FileSystemStream::read(char* buffer, size_t size)
    {
        if (!isValid())// || mAccess != ACCESS_READ)
            return 0;

        LOGEC( (((int) size) > std::numeric_limits<int>::max()), 
            "Bad cast: A value is trunked to the max value of an int.");

		if(mAccess == ACCESS_READ) {
			mInputStream.clear();
			mInputStream.read(buffer, (int)size);
			return mInputStream.gcount();
		}
		else {
			mStream.clear();
			mStream.read(buffer, (int)size);
			return mStream.gcount();
		}
    }
    //-------------------------------------------------------------------------
    bool FileSystemStream::write(const char* buffer, size_t size)
    {
        if (!isValid() || mAccess != ACCESS_WRITE)
            return false;

        LOGEC( (((int) size) > std::numeric_limits<int>::max()), 
            "Bad cast: A value is trunked to the max value of an int.");

        mStream.write(buffer, (int)size);
        if (mFlush)
            mStream.flush();

        mSize = _getSize();

        return true;
    }
    //-------------------------------------------------------------------------
    bool FileSystemStream::setPosition(size_t position)
    {
        //We can't because LOG use this method to create a stream...
        //LOGEC( (position > std::numeric_limits<long>::max()), 
        //    "Bad cast: A value is trunked to the max value of a long.");

        if (mAccess == ACCESS_READ)
        {
            if (position <= mSize)
            {
                mInputStream.clear();
                mInputStream.seekg((long)position, std::ios::beg);
                return true;
            }
        }
        else if (mAccess == ACCESS_WRITE)
        {
            if (position <= mSize)
            {
                mStream.clear();
                mStream.seekp((long)position, std::ios::beg);
                return true;
            }
        }

        return false;
    }
    //-------------------------------------------------------------------------
    size_t FileSystemStream::getPosition() const
    {
        if (mAccess == ACCESS_READ)
        {
            mInputStream.clear();
            return mInputStream.tellg();
        }
        else if (mAccess == ACCESS_WRITE)
        {
            mStream.clear();
            return mStream.tellp();
        }
        
        return 0;
    }
    //-------------------------------------------------------------------------
    DataStream::Access FileSystemStream::getAccess() const
    {
        return mAccess;
    }
    //-------------------------------------------------------------------------
    size_t FileSystemStream::_getSize()
    {
        size_t size = 0;
        long currentPosition;

        if (mAccess == ACCESS_READ)
        {
            mInputStream.clear();
            currentPosition = mInputStream.tellg();
            mInputStream.seekg(0, std::ios::end);
            size = mInputStream.tellg();
            mInputStream.seekg(currentPosition, std::ios::beg);
        }
        else if (mAccess == ACCESS_WRITE)
        {
            mStream.clear();
            currentPosition = mStream.tellp();
            mStream.seekp(0, std::ios::end);
            size = mStream.tellp();
            mStream.seekp(currentPosition, std::ios::beg);
        }

        return size;
    }
    //-------------------------------------------------------------------------
}
