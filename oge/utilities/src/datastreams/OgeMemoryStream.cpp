/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/datastreams/OgeMemoryStream.h"

namespace oge
{
    //-------------------------------------------------------------------------
    MemoryStream::MemoryStream(DataStreamOwner* owner) : DataStream(owner),
        mOwnsData(false), mDataStart(0), mDataCurrent(0), mDataEnd(0),
        mSize(0), mIsOpen(false), mName("")
    {

    }
    //-------------------------------------------------------------------------
    MemoryStream::~MemoryStream()
    {
        close();
    }
    //-------------------------------------------------------------------------
    bool MemoryStream::openRead(const String& name, char* buffer, 
        size_t size)
    {
        if (mIsOpen)
            close();

        if (buffer == 0 || size == 0)
            return false;

        mDataStart = buffer;
        mDataCurrent = mDataStart;
        mDataEnd = mDataStart + size;
        mOwnsData = false;
        mSize = size;
        mAccess = ACCESS_READ;
        mName = name;
        mIsOpen = true;

        return true;
    }
    //-------------------------------------------------------------------------
    bool MemoryStream::openWrite(const String& name, size_t size, 
        char* buffer)
    {
        if (mIsOpen)
            close();

        if (size == 0)
            return false;

        mSize = size;
        mAccess = ACCESS_WRITE;
        mName = name;
        mOwnsData = false;

        mDataStart = buffer;
        if (mDataStart == 0)
        {
            mDataStart = new char[mSize];
            mOwnsData = true;
        }
        
        mDataCurrent = mDataStart;
        mDataEnd = mDataStart + size;

        mIsOpen = true;

        return true;
    }
    //-------------------------------------------------------------------------
    void MemoryStream::close()
    {
        DataStream::close();

        mIsOpen = false;

        if (mOwnsData)
            delete[] mDataStart;

        mDataStart = 0;
        mDataCurrent = 0;
        mDataEnd = 0;
        mOwnsData = false;
        mSize = 0;
        mName = "";
    }
    //-------------------------------------------------------------------------
    bool MemoryStream::isValid() const
    {
        return mIsOpen && mSize > 0 && mDataStart != 0 && mDataCurrent != 0;
    }
    //-------------------------------------------------------------------------
    bool MemoryStream::isAtEndOfStream() const
    {
        return mDataCurrent > mDataEnd;
    }
    //-------------------------------------------------------------------------
    size_t MemoryStream::read(char* buffer, size_t size)
    {
        if (!isValid() || mAccess != ACCESS_READ)
            return 0;

        size_t remaining = mDataEnd - mDataCurrent;
        if (remaining <= 0)
            return 0;

        size_t bytesCopied = size <= remaining ? size : remaining;
        memcpy(buffer, mDataCurrent, bytesCopied);
        mDataCurrent += bytesCopied;
        return bytesCopied;
    }
    //-------------------------------------------------------------------------
    bool MemoryStream::write(const char* buffer, size_t size)
    {
        if (!isValid() || mAccess != ACCESS_WRITE)
            return false;

        size_t remaining = mDataEnd - mDataCurrent;
        if (remaining < size)
            return false;

        memcpy(mDataCurrent, buffer, size);
        mDataCurrent += size;

        return true;
    }
    //-------------------------------------------------------------------------
    bool MemoryStream::setPosition(size_t position)
    {
        if (!isValid())
            return false;

        char* newPosition = mDataStart + position;
        if (newPosition > mDataEnd + 1)
            return false;

        mDataCurrent = newPosition;

        return true;
    }
    //-------------------------------------------------------------------------
    size_t MemoryStream::getPosition() const
    {
        if (!isValid())
            return 0;
        
        return (mDataCurrent - mDataStart);
    }
    //-------------------------------------------------------------------------
    DataStream::Access MemoryStream::getAccess() const
    {
        return mAccess;
    }
    //-------------------------------------------------------------------------
}
