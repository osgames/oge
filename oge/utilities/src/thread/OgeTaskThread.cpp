/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/thread/OgeTaskThread.h"
#include "oge/task/OgeTask.h"
#include "oge/task/OgeDelegateTask.h"
#include "oge/logging/OgeLogManager.h"

namespace oge
{
    //-------------------------------------------------------------------------
    TaskThread::TaskThread() : Thread(), mWaitEvent(true), mContinue(false)
    {
    }
    //-------------------------------------------------------------------------
    TaskThread::TaskThread(const String& name) : Thread(name), mWaitEvent(true),
        mContinue(false)
    {
    }
    //-------------------------------------------------------------------------
    TaskThread::TaskThread(OSThread* thread) : Thread(thread), mWaitEvent(true),
        mContinue(false)
    {

    }
    //-------------------------------------------------------------------------
    TaskThread::~TaskThread()
    {

    }        
    //-------------------------------------------------------------------------
    void TaskThread::run()
    {
        LOGV("TaskThread started");
        mContinue = true;
        Task* task = 0;

        notifyThreadStarted();

		bool firstTask = true;

        while(true)
        {
            {
                while(true)
                {   
                    if (!mContinue)
                    {
                        notifyThreadStopped();
                        return;
                    }

                    if (mTasks.pop(task))
                        break;

                    LOGV("TaskThread waiting");
                    mWaitEvent.wait();
                    LOGV("TaskThread woken");
                }
            }

            task->run();

			// hack to not double delete the first task which results sometimes
			// in a block deleted error
			if(!firstTask) {
				delete task;
			}
			else {
				firstTask = false;
			}	
        }
    }
    //-------------------------------------------------------------------------
    void TaskThread::stop()
    {
        typedef DelegateTask0<void> DT;
        DT* fn = new DT(DT::Delegate(this, &TaskThread::_stopThread));
        addTask(fn);
    }
    //-------------------------------------------------------------------------
    void TaskThread::stopNow()
    {
        _stopThread();
        mWaitEvent.set();
    }
    //-------------------------------------------------------------------------
    void TaskThread::_stopThread()
    {
        mContinue = false;
    }
    //-------------------------------------------------------------------------
    void TaskThread::addTask(Task* task)
    {
        mTasks.push(task);
        mWaitEvent.set();
    }
    //-------------------------------------------------------------------------
    void TaskThread::addListener(Listener* listener)
    {
        Mutex::ScopedLock lock(mListenerMutex);
        mListeners.push_back(listener);
    }
    //-------------------------------------------------------------------------
    void TaskThread::removeListener(Listener* listener)
    {
        Mutex::ScopedLock lock(mListenerMutex);

        ListenerVector::iterator iter = mListeners.begin();
        for (; iter != mListeners.end(); ++iter)
        {
            if (*iter == listener)
            {
                mListeners.erase(iter);
                return;
            }
        }
    }
    //-------------------------------------------------------------------------
    void TaskThread::notifyThreadStarted()
    {
        Mutex::ScopedLock lock(mListenerMutex);

        size_t size = mListeners.size();
        for (size_t index = 0; index < size; ++index)
        {
            mListeners[index]->notifyThreadStarted();
        }
    }
    //-------------------------------------------------------------------------
    void TaskThread::notifyThreadStopped()
    {
        LOG("Stopping thread id: "+StringUtil::toString(getID())
            + " name: " + getName());
        // TODO: Do we need this first?
        while(!mTasks.empty())
        {
            Task* fn;
            if (mTasks.pop(fn))
            {
                delete fn;
            }
        }
        
        Mutex::ScopedLock lock(mListenerMutex);

        size_t size = mListeners.size();
        for (size_t index = 0; index < size; ++index)
        {
            mListeners[index]->notifyThreadStopped();
            mListeners[index]->notifyDettachedFromThread();
        }

        mListeners.clear();

        while(!mTasks.empty())
        {
            Task* fn;
            if (mTasks.pop(fn))
            {
                delete fn;
            }
        }
    }
    //-------------------------------------------------------------------------
    TaskThread::Listener::Listener() : mThread(0)
    {

    }
    //-------------------------------------------------------------------------
    TaskThread::Listener::~Listener()
    {

    }
    //-------------------------------------------------------------------------
    AsyncBool TaskThread::Listener::attachToThread(TaskThread* thread)
    {
        typedef DelegateTask1<TaskThread*, bool> DT;
        DT* fn = new DT(thread, DT::Delegate(this, &Listener::attachedToThread));

        AsyncBool result = fn->getResult();
		
        thread->addTask(fn);
        return result;
    }
    //-------------------------------------------------------------------------
    AsyncBool TaskThread::Listener::dettachFromThread()
    {
        if (!mThread)
            return AsyncBool(true, true);

        typedef DelegateTask0<bool> DT;
        DT* fn = new DT(DT::Delegate(this, &Listener::dettachedFromThread));
        
        AsyncBool result = fn->getResult();
        mThread->addTask(fn);
        return result;
    }
    //-------------------------------------------------------------------------
    bool TaskThread::Listener::attachedToThread(TaskThread* thread)
    {
        mThread = thread;
        mThread->addListener(this);
        notifyAttachedToThread();
        LOG("Attached to thread id: "+StringUtil::toString(thread->getID())
            + " name: " + thread->getName());

        return true;
    }
    //-------------------------------------------------------------------------
    bool TaskThread::Listener::dettachedFromThread()
    {
        LOG("Dettached from thread id: "+StringUtil::toString(mThread->getID())
            + " name: " + mThread->getName());
        if (mThread)
        {
            notifyDettachedFromThread();
            mThread->removeListener(this);
            mThread = 0;
        }

        return true;
    }
    //-------------------------------------------------------------------------
}
