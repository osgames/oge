/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt .
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/task/OgeTaskHandler.h"
#include "oge/task/OgeTask.h"

namespace oge
{
    //-------------------------------------------------------------------------
    void TaskHandler::processAll()
    {
        Task* task;
        while(mTasks.pop(task))
        {
            task->run();
            delete task;
        }
    }
    //-------------------------------------------------------------------------
    void TaskHandler::processCount(size_t max)
    {
        size_t count = 0;
        Task* task;
        while(count < max && mTasks.pop(task))
        {
            ++count;
            task->run();
            delete task;
        }
    }
    //-------------------------------------------------------------------------
    void TaskHandler::clearAll()
    {
        Task* task;
        while(mTasks.pop(task))
        {
            delete task;
        }
    }
    //-------------------------------------------------------------------------
    void PriorityTaskHandler::processAll()
    {
        Task* task;
        for(size_t priority = PR_HIGH; priority <= PR_LOW; ++priority)
        {
            while(mTasks[priority].pop(task))
            {
                task->run();
                delete task;
            }
        }
    }
    //-------------------------------------------------------------------------
    void PriorityTaskHandler::processPriority(Priority priority)
    {
        Task* task;
        while(mTasks[priority].pop(task))
        {
            task->run();
            delete task;
        }
    }
    //-------------------------------------------------------------------------
    void PriorityTaskHandler::processCount(size_t max)
    {
        Task* task;
        size_t count = 0;

        if (max == 0)
            return;

        for(size_t priority = PR_HIGH; priority <= PR_LOW; ++priority)
        {
            while(mTasks[priority].pop(task))
            {
                task->run();
                delete task;

                if(++count == max)
                    return;
            }
        }
    }
    //-------------------------------------------------------------------------
    void PriorityTaskHandler::clearAll()
    {
        Task* task;
        for(size_t priority = PR_HIGH; priority <= PR_LOW; ++priority)
        {
            while(mTasks[priority].pop(task))
            {
                delete task;
            }
        }
    }
    //-------------------------------------------------------------------------
}
