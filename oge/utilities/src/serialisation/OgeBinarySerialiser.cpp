/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#include "oge/serialisation/OgeBinarySerialiser.h"
#include "oge/math/OgeVector3.h"
#include "oge/math/OgeQuaternion.h"
#include "oge/datastreams/OgeDataStream.h"

namespace oge
{
    //-------------------------------------------------------------------------
    int BinarySerialiser::readInt()
    {
        int n = 0;
        mDataStream->read((char*)&n, sizeof(int));
        return n;
    }
    //-------------------------------------------------------------------------
    float BinarySerialiser::readFloat()
    {
        float f = 0;
        mDataStream->read((char*)&f, sizeof(float));
        return f;
    }
    //-------------------------------------------------------------------------
    std::string BinarySerialiser::readString()
    {
        int nLength = readInt();
        char txt[512]; // TODO see writeString()
        mDataStream->read(txt, nLength);
        return std::string(txt);
    }
    //-------------------------------------------------------------------------
    Vector3 BinarySerialiser::readVector3()
    {
        Vector3 v;
        v.x = readFloat();
        v.y = readFloat();
        v.z = readFloat();
        return v;
    }
    //-------------------------------------------------------------------------
    Quaternion BinarySerialiser::readQuaternion()
    {
        Quaternion q;
        q.w = readFloat();
        q.x = readFloat();
        q.y = readFloat();
        q.z = readFloat();
        return q;
    }
    //-------------------------------------------------------------------------
    void* BinarySerialiser::readPointer()
    {
        void* p;
        mDataStream->read((char*)&p, sizeof(void*));
        return p;
    }
    //-------------------------------------------------------------------------
    void BinarySerialiser::writeInt(int n)
    {
        mDataStream->write((char*)&n, sizeof(int));
    }
    //-------------------------------------------------------------------------
    void BinarySerialiser::writeFloat(float f)
    {
        mDataStream->write((char*)&f, sizeof(float));
    }
    //-------------------------------------------------------------------------
    void BinarySerialiser::writeString(const std::string & s)
    {
        int nLength = (int) (s.length()+1);

        // TODO See readString() to avoid new/deleting char array all the time 
        // we need to parse a long text into chunks. My quick try needed more testing.
        if (nLength > 512)
            nLength = 512; // for now I trunk it to 512 chars

        writeInt (nLength);
        mDataStream->write((const char*) (s.c_str()), nLength);
    }
    //-------------------------------------------------------------------------
    void BinarySerialiser::writeVector3(const Vector3& v)
    {
        writeFloat( v.x );
        writeFloat( v.y );
        writeFloat( v.z );
    }
    //-------------------------------------------------------------------------
    void BinarySerialiser::writeQuaternion(const Quaternion& q)
    {
        writeFloat( q.w );
        writeFloat( q.x );
        writeFloat( q.y );
        writeFloat( q.z );
    }
    //-------------------------------------------------------------------------
    void BinarySerialiser::writePointer(void *p)
    {
        mDataStream->write((char*)&p, sizeof(void*));
    }
    //-------------------------------------------------------------------------
}
