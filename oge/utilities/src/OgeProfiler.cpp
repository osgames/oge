/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/

#include "oge/OgeProfiler.h"
#include "oge/OgeClock.h"

namespace oge {
    //-----------------------------------------------------------------------
    // PROFILE DEFINITIONS
    //-----------------------------------------------------------------------
    template<> Profiler* Singleton<Profiler>::mSingleton = 0;
    Profiler* Profiler::createSingleton()
    {
        if (mSingleton == 0)
            mSingleton = new Profiler();
        return mSingleton;
    };
    /**
     * Used to delete the instance.
     */
    void Profiler::destroySingleton()
    {
        if (mSingleton != 0)
        {
            delete mSingleton;
            mSingleton = 0;
        }
    };
    Profiler* Profiler::getSingletonPtr(void)
    {
        return mSingleton;
    }
    Profiler& Profiler::getSingleton(void)
    {  
        assert( mSingleton );  return ( *mSingleton );  
    }
    //-----------------------------------------------------------------------
    Profile::Profile(const String& profileName, unsigned int groupID) 
		: mName(profileName)
		, mGroupID(groupID)
	{

        Profiler::getSingleton().beginProfile(profileName, groupID);

    }
    //-----------------------------------------------------------------------
    Profile::~Profile() {

        Profiler::getSingleton().endProfile(mName, mGroupID);

    }
    //-----------------------------------------------------------------------


    //-----------------------------------------------------------------------
    // PROFILER DEFINITIONS
    //-----------------------------------------------------------------------
    Profiler::Profiler() 
		: mInitialized(false)
		, mMaxDisplayProfiles(50)
		, mUpdateDisplayFrequency(10)
		, mCurrentFrame(0)
		, mClock(0)
		, mTotalFrameTime(0)
		, mEnabled(false)
		, mEnableStateChangePending(false)
		, mNewEnableState(false)
		, mProfileMask(0xFFFFFFFF)
		, mMaxTotalFrameTime(0)
		, mAverageFrameTime(0)
		, mResetExtents(false)
	{
		
    }
    //-----------------------------------------------------------------------
    Profiler::~Profiler() {

        if (!mProfileHistory.empty()) {
            // log the results of our profiling before we quit
            logResults();
        }

        // clear all our lists
        mProfiles.clear();
        mProfileFrame.clear();
        mProfileHistoryMap.clear();
        mProfileHistory.clear();
        mDisabledProfiles.clear();
    }
	//---------------------------------------------------------------------
    void Profiler::initialize() 
	{

    }
    //-----------------------------------------------------------------------
    void Profiler::setClock(Clock* t) {

        mClock = t;

    }
    //-----------------------------------------------------------------------
    Clock* Profiler::getClock() {

        assert(mClock && "Clock not set!");
        return mClock;

    }
    //-----------------------------------------------------------------------
    void Profiler::setEnabled(bool enabled) {

        if (!mInitialized && enabled) {

            // the user wants to enable the Profiler for the first time
            // so we initialize the GUI stuff
            initialize();
            mInitialized = true;
            mEnabled = true;

			if(!mClock) {

			}
        }
        else {
            // We store this enable/disable request until the frame ends
            // (don't want to screw up any open profiles!)
            mEnableStateChangePending = true;
            mNewEnableState = enabled;
        }

    }
    //-----------------------------------------------------------------------
    bool Profiler::getEnabled() const {

        return mEnabled;

    }
    //-----------------------------------------------------------------------
    void Profiler::disableProfile(const String& profileName) {

        // make sure the profile isn't already disabled
        DisabledProfileMap::iterator iter;
        iter = mDisabledProfiles.find(profileName);

        // make sure you don't disable a profile in the middle of that profile
        ProfileStack::iterator pIter;
        for (pIter = mProfiles.begin(); pIter != mProfiles.end(); ++pIter) {

            if (profileName == (*pIter).name)
                break;

        }

        // if those two conditions are met, disable the profile
        if ( (iter == mDisabledProfiles.end()) && (pIter == mProfiles.end()) ) {

            mDisabledProfiles.insert(std::pair<String, bool>(profileName, true));

        }

    }
    //-----------------------------------------------------------------------
    void Profiler::enableProfile(const String& profileName) {

        // make sure the profile is actually disabled
        DisabledProfileMap::iterator iter;
        iter = mDisabledProfiles.find(profileName);

        // make sure you don't enable a profile in the middle of that profile
        ProfileStack::iterator pIter;
        for (pIter = mProfiles.begin(); pIter != mProfiles.end(); ++pIter) {

            if (profileName == (*pIter).name)
                break;

        }

        // if those two conditions are met, enable the profile by removing it from
        // the disabled list
        if ( (iter != mDisabledProfiles.end()) && (pIter == mProfiles.end()) ) {

            mDisabledProfiles.erase(iter);

        }

    }
    //-----------------------------------------------------------------------
    void Profiler::beginProfile(const String& profileName, unsigned int groupID) 
	{
        // if the profiler is enabled
        if (!mEnabled) {
            return;
        }

		// mask groups
		if ((groupID & mProfileMask) == 0) {
			return;
		}


		// we only process this profile if isn't disabled
		DisabledProfileMap::iterator dIter;
		dIter = mDisabledProfiles.find(profileName);
		if ( dIter != mDisabledProfiles.end() ) {
			return;
		}

        // empty string is reserved for the root
        assert ((profileName != "") && ("Profile name can't be an empty string"));

        ProfileStack::iterator iter;
        for (iter = mProfiles.begin(); iter != mProfiles.end(); ++iter) {
            if ((*iter).name == profileName) {
                break;
            }
        }

        // make sure this profile isn't being used more than once
        assert ((iter == mProfiles.end()) && ("This profile name is already being used"));


        ProfileInstance p;
		p.hierarchicalLvl = static_cast<unsigned int>(mProfiles.size());

        // this is the root, it has no parent
        if (mProfiles.empty()) {
            p.parent = "";
        }
        // otherwise peek at the stack and use the top as the parent
        else {
            ProfileInstance parent = mProfiles.back();
            p.parent = parent.name;

        }

        // need a clock to profile!
        assert (mClock && "Clock not set!");

        ProfileFrameList::iterator fIter;
        ProfileHistoryList::iterator hIter;

        // we check to see if this profile has been called in the frame before
        for (fIter = mProfileFrame.begin(); fIter != mProfileFrame.end(); ++fIter) {

            if ((*fIter).name == profileName)
                break;

        }
        // if it hasn't been called before, set its position in the stack
        if (fIter == mProfileFrame.end()) {

            ProfileFrame f;
            f.name = profileName;
            f.frameTime = 0;
            f.calls = 0;
            f.hierarchicalLvl = (unsigned int) mProfiles.size();
            mProfileFrame.push_back(f);

        }

        // we check to see if this profile has been called in the app before
        ProfileHistoryMap::iterator histMapIter;
        histMapIter = mProfileHistoryMap.find(profileName);

        // if not we add a profile with just the name into the history
        if (histMapIter == mProfileHistoryMap.end()) {

            ProfileHistory h;
            h.name = profileName;
            h.numCallsThisFrame = 0;
            h.totalTimePercent = 0;
			h.totalTimeMillisecs = 0;
            h.totalCalls = 0;
            h.maxTimePercent = 0;
			h.maxTimeMillisecs = 0;
            h.minTimePercent = 1;
			h.minTimeMillisecs = 100000;
            h.hierarchicalLvl = p.hierarchicalLvl;
            h.currentTimePercent = 0;
			h.currentTimeMillisecs = 0;

            // we add this to the history
            hIter = mProfileHistory.insert(mProfileHistory.end(), h);

            // for quick look-ups, we'll add it to the history map as well
            mProfileHistoryMap.insert(std::pair<String, ProfileHistoryList::iterator>(profileName, hIter));

        }

        // add the stats to this profile and push it on the stack
        // we do this at the very end of the function to get the most
        // accurate timing results
        p.name = profileName;
        p.currTime = mClock->updateAndGetTime();
        p.accum = 0;
        mProfiles.push_back(p);

    }
    //-----------------------------------------------------------------------
    void Profiler::endProfile(const String& profileName, unsigned int groupID) {

		// if the profiler received a request to be enabled or disabled
		// we reached the end of the frame so we can safely do this
		if (mEnableStateChangePending) {
			changeEnableState();
		}

		// if the profiler is enabled
        if(!mEnabled) {
            return;
        }

		// mask groups
		if ((groupID & mProfileMask) == 0) {
			return;
		}

		// we only process this profile if isn't disabled
		DisabledProfileMap::iterator dIter;
		dIter = mDisabledProfiles.find(profileName);
		if ( dIter != mDisabledProfiles.end() ) {
			return;
		}

        // need a timer to profile!
        assert (mClock && "Clock not set!");

        // get the end time of this profile
        // we do this as close the beginning of this function as possible
        // to get more accurate timing results
        unsigned long endTime = mClock->updateAndGetTime();

        // empty string is reserved for designating an empty parent
        assert ((profileName != "") && ("Profile name can't be an empty string"));
      
        // stack shouldn't be empty
		if(mProfiles.empty()) {
			// looks like we got enabled at an odd time
			return;
		}
        //assert (!mProfiles.empty());

        // get the start of this profile
        ProfileInstance bProfile;
        bProfile = mProfiles.back();
        mProfiles.pop_back();

        // calculate the elapsed time of this profile
        unsigned long timeElapsed = endTime - bProfile.currTime;

		if(timeElapsed == 0) {
			return;
		}

        // update parent's accumulator if it isn't the root
        if (bProfile.parent != "") {

            // find the parent
            ProfileStack::iterator iter;
            for(iter = mProfiles.begin(); iter != mProfiles.end(); ++iter) {

                if ((*iter).name == bProfile.parent)
                    break;

            }

            // the parent should be found 
            assert(iter != mProfiles.end());

            // add this profile's time to the parent's accumlator
            (*iter).accum += timeElapsed;

        }

        // we find the profile in this frame
        ProfileFrameList::iterator iter;
        for (iter = mProfileFrame.begin(); iter != mProfileFrame.end(); ++iter) {

            if ((*iter).name == bProfile.name)
                break;

        }

		// nested profiles are cumulative
        (*iter).frameTime += timeElapsed;
        (*iter).calls++;

        // the stack is empty and all the profiles have been completed
        // we have reached the end of the frame so process the frame statistics
        if (mProfiles.empty()) {

            // we know that the time elapsed of the main loop is the total time the frame took
            mTotalFrameTime = timeElapsed;

			if (timeElapsed > mMaxTotalFrameTime)
				mMaxTotalFrameTime = timeElapsed;

            // we got all the information we need, so process the profiles
            // for this frame
            processFrameStats();

            // clear the frame stats for next frame
            mProfileFrame.clear();

            // we display everything to the screen
            displayResults();

        }

    }
    //-----------------------------------------------------------------------
    void Profiler::processFrameStats() {

        ProfileFrameList::iterator frameIter;
        ProfileHistoryList::iterator historyIter;

        // we set the number of times each profile was called per frame to 0
        // because not all profiles are called every frame
        for (historyIter = mProfileHistory.begin(); historyIter != mProfileHistory.end(); ++historyIter) {
            (*historyIter).numCallsThisFrame = 0;
        }

		Real maxFrameTime = 0;

        // iterate through each of the profiles processed during this frame
        for (frameIter = mProfileFrame.begin(); frameIter != mProfileFrame.end(); ++frameIter) {

            String s = (*frameIter).name;

            // use our map to find the appropriate profile in the history
            historyIter = (*mProfileHistoryMap.find(s)).second;

            // extract the frame stats
            unsigned long frameTime = (*frameIter).frameTime;
            unsigned int calls = (*frameIter).calls;
            unsigned int lvl = (*frameIter).hierarchicalLvl;

            // calculate what percentage of frame time this profile took
            Real framePercentage = (Real) frameTime / (Real) mTotalFrameTime;

			Real frameTimeMillisecs = (Real)frameTime / 1000.0f;

            // update the profile stats
			(*historyIter).currentTimePercent = framePercentage;
			(*historyIter).currentTimeMillisecs = frameTimeMillisecs;
			if (mResetExtents)
			{
				(*historyIter).totalTimePercent = framePercentage;
				(*historyIter).totalTimeMillisecs = frameTimeMillisecs;
				(*historyIter).totalCalls = 1;
			}
			else
			{
				(*historyIter).totalTimePercent += framePercentage;
				(*historyIter).totalTimeMillisecs += frameTimeMillisecs;
				(*historyIter).totalCalls++;
			}
            (*historyIter).numCallsThisFrame = calls;
            (*historyIter).hierarchicalLvl = lvl;

            // if we find a new minimum for this profile, update it
            if (frameTimeMillisecs < ((*historyIter).minTimeMillisecs)
				|| mResetExtents)
			{
                (*historyIter).minTimePercent = framePercentage;
				(*historyIter).minTimeMillisecs = frameTimeMillisecs;
            }

            // if we find a new maximum for this profile, update it
            if (frameTimeMillisecs > ((*historyIter).maxTimeMillisecs) 
				|| mResetExtents)
			{
                (*historyIter).maxTimePercent = framePercentage;
				(*historyIter).maxTimeMillisecs = frameTimeMillisecs;
            }

			if (frameTime > maxFrameTime)
				maxFrameTime = (Real)frameTime;

        }

		// Calculate whether the extents are now so out of date they need regenerating
		if (mCurrentFrame == 0)
			mAverageFrameTime = maxFrameTime;
		else
			mAverageFrameTime = (mAverageFrameTime + maxFrameTime) * 0.5f;

		if ((Real)mMaxTotalFrameTime > mAverageFrameTime * 4)
		{
			mResetExtents = true;
			mMaxTotalFrameTime = (unsigned long)mAverageFrameTime;
		}
		else
			mResetExtents = false;

    }
    //-----------------------------------------------------------------------
    void Profiler::displayResults() {

        if (!mEnabled) {
            return;
        }


		mCurrentFrame++;

    }
    //-----------------------------------------------------------------------
    bool Profiler::watchForMax(const String& profileName) {

        ProfileHistoryMap::iterator mapIter;
        ProfileHistoryList::iterator iter;

        mapIter = mProfileHistoryMap.find(profileName);

        // if we don't find the profile, return false
        if (mapIter == mProfileHistoryMap.end())
            return false;

        iter = (*mapIter).second;

        return ((*iter).currentTimePercent == (*iter).maxTimePercent);

    }
    //-----------------------------------------------------------------------
    bool Profiler::watchForMin(const String& profileName) {

        ProfileHistoryMap::iterator mapIter;
        ProfileHistoryList::iterator iter;

        mapIter = mProfileHistoryMap.find(profileName);

        // if we don't find the profile, return false
        if (mapIter == mProfileHistoryMap.end())
            return false;

        iter = (*mapIter).second;

        return ((*iter).currentTimePercent == (*iter).minTimePercent);

    }
    //-----------------------------------------------------------------------
    bool Profiler::watchForLimit(const String& profileName, Real limit, bool greaterThan) {

        ProfileHistoryMap::iterator mapIter;
        ProfileHistoryList::iterator iter;

        mapIter = mProfileHistoryMap.find(profileName);

        // if we don't find the profile, return false
        if (mapIter == mProfileHistoryMap.end())
            return false;

        iter = (*mapIter).second;

        if (greaterThan)
            return ((*iter).currentTimePercent > limit);
        else
            return ((*iter).currentTimePercent < limit);

    }
    //-----------------------------------------------------------------------
    void Profiler::logResults() {

		// @TODO output to default log
		/*
        ProfileHistoryList::iterator iter;
        LogManager::getSingleton().logMessage("----------------------Profiler Results----------------------");

        for (iter = mProfileHistory.begin(); iter != mProfileHistory.end(); ++iter) {

            // create an indent that represents the hierarchical order of the profile
            String indent = "";
            for (unsigned int i = 0; i < (*iter).hierarchicalLvl; ++i) {

                indent = indent + "   ";

            }

            LogManager::getSingleton().logMessage(indent + "Name " + (*iter).name + 
				" | Min " + StringConverter::toString((*iter).minTimePercent) + 
				" | Max " + StringConverter::toString((*iter).maxTimePercent) + 
				" | Avg "+ StringConverter::toString((*iter).totalTimePercent / (*iter).totalCalls));

        }

        LogManager::getSingleton().logMessage("------------------------------------------------------------");
		*/
    }
    //-----------------------------------------------------------------------
    void Profiler::reset() {

        ProfileHistoryList::iterator iter;
        for (iter = mProfileHistory.begin(); iter != mProfileHistory.end(); ++iter) {
        
            (*iter).currentTimePercent = (*iter).maxTimePercent = (*iter).totalTimePercent = 0;
			(*iter).currentTimeMillisecs = (*iter).maxTimeMillisecs = (*iter).totalTimeMillisecs = 0;
            (*iter).numCallsThisFrame = (*iter).totalCalls = 0;

            (*iter).minTimePercent = 1;
			(*iter).minTimeMillisecs = 100000;

        }
		mMaxTotalFrameTime = 0;

    }
    //-----------------------------------------------------------------------
    void Profiler::changeEnableState() {
        mEnabled = mNewEnableState;
        mEnableStateChangePending = false;
    }
}
