/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt .
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_ASYNCFUNCTION_H__
#define __OGE_ASYNCFUNCTION_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeReferenceCounter.h"
#include "oge/task/OgeTask.h"

namespace oge
{
    /**
     * Class that holds a value and a valid flag, allowing to check whether
     * the value has been explicitly set or not
     */
    template<typename T, typename RC = oge::ReferenceCounter> class AsyncValue
    {
    protected:
        struct Value
        {
        public:
            T value;
            bool set;
        public:
            Value(const T& v, bool s) : value(v), set(s) {}
        private:
            Value() {}
        };
        typedef SharedPtr<Value, RC> SharedT;
    protected:
        SharedT mValue;
    public:
        inline AsyncValue() : mValue(new Value(T(), false)) {}
        inline AsyncValue(T value, bool set = false) : 
            mValue(new Value(value, set))
        {
        }
        inline AsyncValue(const AsyncValue& value) : mValue(value) { }
        inline bool isValid() const { return mValue->set; }
        inline void unset() { mValue->set = false; }
        inline void set(const T& value, bool set)
        {
            mValue->value = value;
            mValue->set = set;
        }
        inline T get() { return mValue->value; }
        inline AsyncValue& operator=(const AsyncValue& v)
        {
            mValue = v;
            return *this;
        }
//        template <typename T>
        inline AsyncValue& operator=(const T& v)
        {
            mValue->value = v;
            mValue->set = true;
            return *this;
        }
    protected:
        inline operator SharedT() const { return mValue; }
    };

    // Common value types
    typedef AsyncValue<void>    AsyncVoid;
    typedef AsyncValue<bool>    AsyncBool;
    typedef AsyncValue<int>     AsyncInt;
    typedef AsyncValue<float>   AsyncFloat;
    typedef AsyncValue<double>  AsyncDouble;
}

#endif // __OGE_ASYNCFUNCTION_H__
