/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt .
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_CLOCK_H__
#define __OGE_CLOCK_H__

#include "oge/OgeUtilitiesPrerequisites.h"

namespace oge 
{

#if OGE_PLATFORM == OGE_PLATFORM_WIN32

    /* Documentation
    Line 2240 of winnt.h:
        struct {
           DWORD   LowPart
           LONG   HighPart
        }     u
        LONGLONG     QuadPart
    */

    /** 
     * Clock class
     *
     * @todo If the MS Performance Counter is not present this clock must be
     * modified with the code from PreciseTimer from CodeProject:
     * http://www.codeproject.com/cpp/precisetimer.asp
     * (Mingw will need '-Wno-long-long' to compile "long long" type).
     *
     * @author GAY Steven
     */
    class OGE_UTIL_API Clock
    {
    public:
        /**
         * Clock
         */
        Clock();
        ~Clock(){};
        /**
         * Update the clock : this should be called ONCE by frame cycle.
         * This permits to have a stable frame of reference to update all objects.
         * This avoids the problem of getting stable time measures in the same frame
         * if the update method was called multiple time during a frame cycle.
         * @note This should be called only ONCE by frame cycle.
         * @see Clock::updateAndGetTime() and the class Timer
         */
        void update();
        /**
         * Get time in milliseconds : time is counted in ms from the clock start().
         */
        inline double getTime()
        {
            return mNewTicks;
        };
        /**
         * Update and Get time in milliseconds : time is counted in ms from
         * the clock start().
         * @note This should be called only ONCE by frame cycle.
         *       You should use a Timer instead.
         * @see Clock::update() and the class Timer
         */
        inline double updateAndGetTime()
        {
            update();
            return mNewTicks;
        };

    private:
        double  mStartTicks;
        double  mStartTime;
        double  mSecsPerTick;

        double mNewTicks;
        LARGE_INTEGER mCurrentTime;
    };

#else // --------------------- Linux + Mac ----------------------------------

    class OGE_UTIL_API Clock
    {
    public:
        Clock();
        ~Clock() {};

        /**
         * Update the clock : this should be called ONCE by frame cycle.
         * This permits to have a stable frame of reference to update all objects.
         * This avoids the problem of getting stable time measures in the same frame
         * if the update method was called multiple time during a frame cycle.
         */
        void update();
        /**
         * Get time in milliseconds : time is counted in ms from the clock start().
         */
        inline double getTime()
        {
            return mNewTicks;
        };

        /**
         * Update and Get time in milliseconds : time is counted in ms from
         * the clock start().
         */
        inline double updateAndGetTime()
        {
            update();
            return mNewTicks;
        };

    private:
        bool    mInitialised;
        double  mStartTicks;
        timeval mStartTime;
        // static ?? mFrequency; TODO Needed with Linux ?

        double mNewTicks;
        timeval mCurrentTime;
    };

#endif // Linux + Mac
}

#endif // __OGE_CLOCK_H__
