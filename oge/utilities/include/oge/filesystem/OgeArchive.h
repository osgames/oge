/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_ARCHIVE_H__
#define __OGE_ARCHIVE_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeString.h"
#include "oge/filesystem/OgeFile.h"

namespace oge
{
    /**
     * Represents an archive, such as a file system folder, compressed file
     * (e.g. .zip).
     *
     * Subclass to create specific implementations to handle specific types of
     * archive. This allows all archives to be interacted with in the same way.
     * 
     * @author Christopher Jones
     */
    class OGE_UTIL_API Archive
    {
    public:
    protected:
        typedef std::map<String, FilePtr> FileMap;
        RWMutex mFilesMutex;
        FileMap mFiles;
        String mName;
        String mType;
        bool mCanRead;
        bool mCanWrite;
    public:
        /**
         * Constructor. Creates the archive with given restrictions
         *
         * @param   name        The name of the archive, this is usually the
         *                      path to the location of the real archive
         * @param   type        The name of the type of the ArchiveFactory that
         *                      created this archive.
         * @param   allowRead   false stops the archive from allowing the files
         *                      it contains to be read from.
         * @param   allowWrite  false stops the archive from allowing the files
         *                      it contains to be written to.
         */
        Archive(const String& name, const String& type, bool allowRead, 
            bool allowWrite);
        virtual ~Archive();
        /**
         * Gets the name (usually the path) of the archive
         */
        virtual const String& getName() const { return mName; }
        /**
         * Gets the name of the type of the subclass of this archive
         */
        virtual const String& getType() const { return mType; }
        /**
         * Gets whether the file names in this archive are case sensitive
         */
        virtual bool isCaseSensitive() const = 0;
        /**
         * Creates a file in the archive
         *
         * This archive must be allowed write access and a file must
         * not already exist with the same name.
         *
         * @param   filename    The name of the file to create
         * @return  Instance of the file that was created, if it was created.
         *          Returns a null pointer if it couldn't be created
         * @see     canWrite()
         */
        virtual FilePtr createFile(const String& filename) = 0;
        /**
         * Deletes a file from the archive
         *
         * This archive must be allowed write access and a file must
         * already exist with the given name.
         *
         * @param   filename    The name of the file to delete
         * @return  true if the file was deleted
         * @see     canWrite()
         */
        virtual bool deleteFile(const String& filename) = 0;
        /**
         * Gets a file from the archive
         *
         * @return The file with the given filename, if it exists in the archive
         */
        virtual FilePtr getFile(const String& filename);
        /**
         * Is a file contained with the given filename?
         */
        virtual bool containsFile(const String& filename);
        /**
         * Causes the archive to clear its file list and rescan the 
         * actual archive for its contents.
         *
         * This is useful if the archives contents has changed, e.g.
         * in editors when new content is added, or if a game was to download
         * new content.
         * This function must be implemented in the subclass
         */
        virtual void refreshFileList() = 0;
        /**
         * Gets a list of the contained files names
         */
        virtual StringVector getFilenames();
        /**
         * Gets a list of all the contained files
         */
        virtual FileVector getFiles();
        /**
         * Gets a list of the contained files that matches the specified
         * pattern
         *
         * The pattern can contain * and ? wild cards
         */
        virtual StringVector findFilenames(const String& pattern);
        /**
         * Gets a list of the files whose names match the specified pattern
         *
         * The pattern can contain * and ? wild cards
         */
        virtual FileVector findFiles(const String& pattern);
        /**
         * Does this archive allow reading?
         *
         * This is a combination of the restriction given in the constructor
         * and the actual restriction of the archive (such as a folder not
         * having read permission).
         */
        bool canRead() { return mCanRead; }
        /**
         * Does this archive allow writing?
         *
         * This is a combination of the restriction given in the constructor
         * and the actual restriction of the archive (such as a folder not
         * having write permission).
         */
        bool canWrite() { return mCanWrite; }
    };

    typedef Poco::SharedPtr<Archive> ArchivePtr;
}

#endif // __OGE_ARCHIVE_H__
