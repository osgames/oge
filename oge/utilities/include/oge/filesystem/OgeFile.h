/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_FILE_H__
#define __OGE_FILE_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/datastreams/OgeDataStreamOwner.h"
#include "oge/datastreams/OgeDataStream.h"

namespace oge
{
    /**
     * Provides a simple and consistent interface for accessing files
     * in the FileSystem.
     *
     * A file can cache its contents for faster access. This is especially
     * useful if the file is cached in a background thread and processed
     * in a main thread.
     * If the cache has been filled, opening a file for read access will
     * get a data stream to the cache. Opening for write access will
     * invalidate the cache.
     *
     * @author Christopher Jones
     */
    class OGE_UTIL_API File : public DataStreamOwner
    {
    public:
        File();
        virtual ~File();
        /**
         * Gets the filename (doesn't include the path)
         */
        virtual String getFilename() const = 0;
        /**
         * Attempts to open the file for reading
         *
         * This file must have read permission, query using canRead().
         * The file can't be opened for writing, but can be opened
         * multiple times for reading.
         *
         * @return  A data stream instance for reading the files contents.
         *          null if the file can't be opened for read access
         */
        virtual DataStreamPtr openRead() = 0;
        /**
         * Attempts to open the file for writing
         *
         * This file must have write permission, query using canWrite().
         * The file can't be opened for reading and only one write stream
         * can exist for the file at any time.
         *
         * @param   append      Puts the data streams position at the end
         *                      of the file if true. useless if truncate = true
         * @param   truncate    Deletes the files contents when opening
         * @return  The data stream for the files contents
         */
        virtual DataStreamPtr openWrite(bool append, bool truncate) = 0;
        /**
         * Can this file be opened for read access?
         *
         * This does not take into account it already being opened for write
         * access.
         */
        virtual bool canRead() = 0;
        /**
         * Can this file be opened for write access?
         *
         * This does not take into account it already being opened for read
         * access.
         */
        virtual bool canWrite() = 0;
        /**
         * Does this file actually exist?
         */
        virtual bool exists() = 0;
        /**
         * Reads the file into memory.
         *
         * This is only done if the file is readable and can currently be
         * opened for read access.
         * @see openRead()
         */
        virtual bool fillCache() = 0;
        /**
         * Removes the cached contents.
         *
         * This can only be done if the file is not currently open.
         */
        virtual bool clearCache() = 0;
        /**
         * Only to be called from data streams that are being closed and
         * have read access to this file
         */
        virtual void _dataStreamReadReleased() = 0;
        /**
         * Only to be called from data streams that are being closed and
         * have write access to this file
         */
        virtual void _dataStreamWriteReleased() = 0;
    };

    typedef Poco::SharedPtr<oge::File> FilePtr;
    typedef std::vector<FilePtr> FileVector;
}

#endif // __OGE_FILE_H__
