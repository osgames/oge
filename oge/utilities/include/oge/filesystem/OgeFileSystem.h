/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_FILESYSTEM_H__
#define __OGE_FILESYSTEM_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeSingleton.h"
#include "oge/filesystem/OgeArchive.h"
#include "oge/filesystem/OgeArchiveGroup.h"
#include "oge/filesystem/OgeFileSystemListener.h"
#include "oge/OgeString.h"
#include "oge/config/OgeOption.h"

namespace oge
{
    /**
     * Manages the virtual file system.
     *
     * Implementations of types of archives can be registered to allow
     * new paths/files to be used, such as zip files, remote locations etc.
     *
     * ArchiveFactory and Archive maps must be locked in the order:
     * mArchiveFactoriesMutex, mGroupsMutex to avoid possible deadlock
     *
     * @see ArchiveFactory
     * @see Archive
     * @see ArchiveGroup
     * @author Christopher Jones
     */
    class OGE_UTIL_API FileSystem : public Singleton<FileSystem>
    {
    private:
        typedef std::map<String, ArchiveFactory*> ArchiveFactoryMap;
        /// Mutex for archive factories
        RWMutex mArchiveFactoriesMutex;
        ArchiveFactoryMap mArchiveFactories;
        /// Mutex for archives and archive groups
        Mutex mArchivesMutex;
        ArchiveGroupPtr mArchives;
        ArchiveGroupPtr mArchiveGroups;
        FileSystemListenerGroup mListeners;
    public:
        static FileSystem* createSingleton();
        static void destroySingleton();
        /**
         * Sets up the FileSystem with the inbuilt archive factories
         */
        void initialise();
        /**
         * Clears all existing archives and *deletes* all remaining archive
         * factories.
         */
        void shutdown();
        /**
         * Adds an ArchiveFactory to the list of available factories.
         *
         * @return  true if the factory was successfully registered
         */
        bool registerArchiveFactory(ArchiveFactory* archiveFactory);
        /**
         * Removes an ArchiveFactory from the list.
         */
        void unregisterArchiveFactory(const String& archiveType);
        /**
         * Adds an Archive given a path.
         *
         * Each factory is queried to see if it can handle the path. When
         * a valid path is found, the archive is added.
         * 
         * @param   path            The path to the location/archive
         * @param   returnExisting  If the archive already exists then using
         *                          true will return that archive, false
         *                          will return null
         * @param   allowRead       false stops the archive from allowing
         *                          read access to its files
         * @param   allowWrite      false stops the archive from allowing
         *                          write access to its files
         * @return  The archive if it could be added, null if it couldn't
         */
        ArchivePtr addArchive(const String& path, bool returnExisting = false,
            bool allowRead = true, bool allowWrite = true);
        /**
         * Remove an archive that handles the given path
         */
        void removeArchive(const String& path);
        /**
         * Removes all archive of the given type
         *
         * @see ArchiveFactory
         */
        void removeArchivesOfType(const String& type);
        /**
         * Removes all archives
         */
        void removeAllArchives();
        /**
         * Gets an archive by its name
         *
         * @param   path            The name/path of the archive
         * @param   searchGroups    true will search the archive groups' names
         * @return  The archive if it exists                         
         */
        ArchivePtr getArchive(const String& path, bool searchGroups = true);
        /**
         * Does an archive exist that handles the given path
         */
        bool archiveExists(const String& path, bool searchGroups = true);
        /**
         * Creates an new ArchiveGroup with the given name.
         *
         * @param   name            A unique name for the archive
         * @param   returnExisting  If an archive group already exists
         *                          return that.
         * @return  The archive group if it could be added
         */
        ArchiveGroupPtr addArchiveGroup(const String& name, 
            bool returnExisting = false);
        /**
         * Does an ArchiveGroup exist with the given name?
         */
        bool archiveGroupExists(const String& name);
        /**
         * Remove an ArchiveGroup with the given name
         */
        void removeArchiveGroup(const String& name);
        /**
         * Gets an archive group with the given name if it exists
         */
        ArchiveGroupPtr getArchiveGroup(const String& name);
        /**
         * Removes all groups
         */
        void removeAllArchiveGroups();
        /**
         * Searches every archive for a file with the given filename
         */
        FilePtr getFile(const String& filename);
        /**
         * Searches every archive to see if a file exists with the filename
         */
        bool containsFile(const String& filename);
        /**
         * Causes every archive to refresh their file list
         *
         * @see Archive::refreshFileList()
         */
        void refreshFileList();
        /**
         * Gets a list of every filename from every Archive
         */
        StringVector getFilenames();
        /**
         * Gets a list of files using every Archive
         */
        FileVector getFiles();
        /**
         * Gets a list of filenames from every Archive that matches a pattern
         */
        StringVector findFilenames(const String& pattern);
        /**
         * Gets a list of files from every Archive that their filenames matches
         * a pattern
         */
        FileVector findFiles(const String& pattern);
        /**
         * Gets an Archive containing every Archive added to the FileSystem
         */
        ArchivePtr getArchives() const { return mArchives; }
        /**
         * Gets an Archive containing every ArchiveGroup added to the FileSystem
         */
        ArchivePtr getArchiveGroups() const { return mArchiveGroups; }
        /**
         * Adds a FileSystemListener to be able to receive notifications
         */
        void addListener(FileSystemListener* listener);
        /**
         * Removes the first matching listener found from the list
         */
        void removeListener(FileSystemListener* listener);
        /**
         * Configures the file system from an option
         */
        bool setupFromOptions(OptionPtr rootOption);
        static FileSystem* getSingletonPtr();
        static FileSystem& getSingleton();
    private:
        FileSystem();
        ~FileSystem();
        void deleteAllFactories();
        void setupArchivesFromOptions(OptionPtr rootOption);
        void setupGroupsFromOptions(OptionPtr rootOption);
    };
}

#endif // __OGE_FILESYSTEM_H__
