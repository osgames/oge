/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_ARCHIVEFACTORY_H__
#define __OGE_ARCHIVEFACTORY_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/filesystem/OgeArchive.h"
#include "oge/OgeString.h"

namespace oge
{
    /**
     * Used to create an Archive of a specific type
     *
     * Subclass for different types of Archive, e.g. compressed files.
     *
     * @author Christopher Jones
     */
    class OGE_UTIL_API ArchiveFactory
    {
    public:
        /**
         * Constructor. Pass in the name of the type of Archive the subclass
         * handles.
         */
        ArchiveFactory(const String& type);
        virtual ~ArchiveFactory();
        /**
         * Creates an Archive for the given path.
         *
         * This should only pass back a valid Archive if this factory
         * can create an Archive that can handle the given path. It must
         * never return an Archive that is invalid (e.g. doesn't exist).
         *
         * @param   path        The path used to find the archive to be used
         * @param   allowRead   false to stop the contained files from being
         *                      opened for read access. true doesn't apply a
         *                      restriction
         * @param   allowWrite  false to stop the contained files from being
         *                      opened for write access. true to allow writing.
         * @return  A shared pointer to an instance of an Archive if a valid
         *          Archive could be created/handled. a Null pointer if
         *          it could not be handled.
         */
        virtual ArchivePtr createArchive(const String& path, bool allowRead, 
            bool allowWrite) = 0;
        /**
         * Gets the type of Archive this creates.
         *
         * @see ArchiveFactory::ArchiveFactory()
         */
        const String& getType() const { return mType; }
    protected:
        String mType;
    };
}

#endif // __OGE_ARCHIVEFACTORY_H__
