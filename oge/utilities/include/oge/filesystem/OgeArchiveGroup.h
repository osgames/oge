/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_ARCHIVEGROUP_H__
#define __OGE_ARCHIVEGROUP_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/filesystem/OgeArchive.h"
#include "oge/OgeString.h"

namespace oge
{
    /**
     * Group one or more Archive's together under a single name
     *
     * The group can have a default Archive which is used when creating
     * or deleting files, it is not searched when finding/accessing files
     * though. The default Archive can also be part of the group.
     *
     * @author Christopher Jones
     */
    class OGE_UTIL_API ArchiveGroup : public Archive
    {
    private:
        typedef std::map<String, ArchivePtr> ArchiveMap;
        mutable RWMutex mArchivesMutex;
        ArchiveMap mArchives;
        ArchivePtr mDefaultArchive;
        String mName;
        bool mAnyArchiveCaseSensitive;
    public:
        /**
         * Constructor. Creates the group with a name
         */
        ArchiveGroup(const String& name);
        ~ArchiveGroup();
        /**
         * Adds an archive to the group
         *
         * @return  true if the archive was added.
         */
        bool addArchive(ArchivePtr archive);
        /**
         * Removes an archive from the group
         */
        void removeArchive(ArchivePtr archive);
        /**
         * Removes an archive from the group by its name
         */
        void removeArchive(const String& archiveName);
        /**
         * Removes all of the archives contained.
         *
         * This does not affect the default archive, this must be removed
         * separately.
         */
        void removeAllArchives();
        /**
         * Removes all Archives that have been created by a given factory
         */
        void removeArchivesOfType(const String& type);
        /**
         * Retrieves an Archive contained in the group
         */
        ArchivePtr getArchive(const String& archiveName) const;
        /**
         * Does the archive exist in this group?
         */
        bool archiveExists(const String& archiveName) const;
        /**
         * Retrieves a vector of the names of the contained archives
         */
        StringVector getArchiveNames() const;
        /**
         * Returns the default Archive
         *
         * The default archive is used when creating or deleting files.
         * It is not used in the other functions, such as containsFile(),
         * getFile() etc.
         * The same archive can be part of the group as well.
         */
        ArchivePtr getDefaultArchive() const;
        /**
         * Sets the default archive.
         *
         * If a archive is already set, it is removed.
         *
         * @param   archive     The archive to use, this can be a null pointer
         */
        void setDefaultArchive(ArchivePtr archive);
        /**
         * Is any of the contained archives case sensitive?
         *
         * @return  true if any one of the contained archives are case sensitive
         */
        bool isCaseSensitive() const;
        /**
         * Creates a file in the groups default archive
         *
         * A default archive must be set, which must be writable.
         * The filename must not already exist.
         *
         * @param   filename    The name of the file to create
         * @return  A shared pointer to the file, null if it couldn't be
         *          created
         */
        FilePtr createFile(const String& filename);
        /**
         * Deletes a file from the groups default archive.
         *
         * The default archive must be set which must be writable.
         * The file must exist.
         */
        bool deleteFile(const String& filename);
        /**
         * Searches each contained archive for the file.
         *
         * The first file with the given filename that is found will be
         * returned. So multiple files with the same name in different archives
         * won't always return the desired file.
         */
        FilePtr getFile(const String& filename);
        /**
         * Is a file with the given name contained in any of the archives?
         */
        bool containsFile(const String& filename);
        /**
         * Causes all contained archives to refresh their file list
         */
        void refreshFileList();
        /**
         * Gets a list of all filenames in all of the contained archives
         */
        StringVector getFilenames();
        /**
         * Gets a list of all the files contained by the archives
         */
        FileVector getFiles();
        /**
         * Gets a list of all the files with filenames that match the pattern
         */
        StringVector findFilenames(const String& pattern);
        /**
         * Gets a list of all the files with filenames matching the pattern
         */
        FileVector findFiles(const String& pattern);
    protected:
        void recalculateCaseSensitivity();
    };

    typedef Poco::SharedPtr<ArchiveGroup> ArchiveGroupPtr;
}

#endif // __OGE_ARCHIVEGROUP_H__
