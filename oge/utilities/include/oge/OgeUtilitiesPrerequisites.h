/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_UTILITIESPREREQUISITES_H__
#define __OGE_UTILITIESPREREQUISITES_H__

// Initial platform/compiler-related define to set.
#define OGE_PLATFORM_APPLE 1
#define OGE_PLATFORM_LINUX 2
#define OGE_PLATFORM_WIN32 3

#define OGE_COMPILER_BORL 1
#define OGE_COMPILER_GNUC 2
#define OGE_COMPILER_MSVC 3

#define OGE_COMPILER_MSVC_6     1200 // Not supported
#define OGE_COMPILER_MSVC_7     1310 // Not supported
#define OGE_COMPILER_MSVC_71    1300
#define OGE_COMPILER_MSVC_8     1400
#define OGE_COMPILER_MSVC_9     1500

// Specific version number to test for compiler/version features
#define OGE_COMPILER_MSVC_9_SP1 150030729

#define OGE_ENDIAN_BIG 1
#define OGE_ENDIAN_LITTLE 2

#define OGE_ARCHITECTURE_32 1
#define OGE_ARCHITECTURE_64 2

#define OGE_DEBUG_MODE   1
#define OGE_RELEASE_MODE 2

// Debug/Release modes
#if defined(NDEBUG)
#    define OGE_MODE OGE_RELEASE_MODE
#else
#    define OGE_MODE OGE_DEBUG_MODE
#endif

// Finds the compiler type and version.
#if defined( _MSC_VER )
#   define OGE_COMPILER OGE_COMPILER_MSVC
#   define OGE_COMP_VER _MSC_VER
#   if defined ( _MSC_FULL_VER )
#       define OGE_COMP_VER_FULL _MSC_FULL_VER
#   else
#       define OGE_COMP_VER_FULL OGE_COMP_VER
#   endif

#elif defined( __GNUC__ )
#   define OGE_COMPILER OGE_COMPILER_GNUC
#   define OGE_COMP_VER (((__GNUC__)*100) + (__GNUC_MINOR__*10) + \
        __GNUC_PATCHLEVEL__)
#   define OGE_COMP_VER_FULL OGE_COMP_VER

#elif defined( __BORLANDC__ )
#   define OGE_COMPILER OGE_COMPILER_BORL
#   define OGE_COMP_VER __BCPLUSPLUS__
#   define OGE_COMP_VER_FULL OGE_COMP_VER

#else
#   error "Sorry, compiler not supported -> Aborting!"
#endif

// 32 or 64 architecture
#if defined(_M_X64) || defined(__x86_64__)
#   define OGE_ARCHITECTURE OGE_ARCHITECTURE_64
#else
#   define OGE_ARCHITECTURE OGE_ARCHITECTURE_32
#endif

#ifdef _MSC_VER
#   pragma warning (disable : 4251) // dll interface to be used by clients...

#   pragma warning (disable : 4275) // non dll-interface class used as base 
                                    // for dll-interface class

#   pragma warning (disable : 4661) // no suitable definition provided for 
                                    // explicit template instantiation request

#   pragma warning (disable : 4311) // type cast' : pointer truncation from 'xxx*' 
                                    // to 'unsigned int'

#endif

// Determine the current platform
#if defined( WIN32 ) || defined( __WIN32__ ) || defined( _WIN32 )
#   define OGE_PLATFORM OGE_PLATFORM_WIN32

#elif defined( __APPLE_CC__)
#   define OGE_PLATFORM OGE_PLATFORM_APPLE

#else
#   define OGE_PLATFORM OGE_PLATFORM_LINUX
#endif

// Dynamic library import/export macro
// TODO: Finish off all platforms
#ifndef OGE_UTIL_API
#    if OGE_PLATFORM == OGE_PLATFORM_WIN32
#        ifdef OGE_UTILITY_EXPORTS
#            define OGE_UTIL_API __declspec(dllexport)
#        else
#            if !defined(__MINGW32__) && !defined(__CYGWIN__)
#                define OGE_UTIL_API __declspec(dllimport)
#            else
#                define OGE_UTIL_API
#            endif
#        endif
#    elif OGE_PLATFORM == OGE_PLATFORM_LINUX
#      define OGE_UTIL_API __attribute__ ((visibility("default")))
#    else
#        define OGE_UTIL_API
#    endif
#endif

// required to stop windows.h screwing up std::min, std::numeric_limits<T>::max() and such definition
#undef NOMINMAX
#define NOMINMAX

// TODO: Remove
// NO_OGE_MEMORY_MANAGER need for OGEd
//  #ifndef NO_OGE_MEMORY_MANAGER
//  #   include "oge/OgeMemoryManager.h"
//  #endif

#include "oge/OgeUtilitiesSystemHeaders.h"
#include "oge/OgeUtilitiesConfig.h"

// TODO: Remove
// This CANT be before the system headers (see OgeMemoryManager.h)
//  #if OGE_MEMORY_MODE == 1
//  #    define new oge_new
//  #endif

/// NEXT Gcc 4.1.2 & 4.2 seems to have trouble with the hash_map
///      until we find a solution we will replace it by a std::map
#if OGE_COMPILER == OGE_COMPILER_GNUC && OGE_COMP_VER >= 310 && !defined(STLPORT)
#   if OGE_COMP_VER >= 400
#       define OgeHashMap ::std::tr1::unordered_map
#    else
#       define OgeHashMap ::__gnu_cxx::hash_map
#    endif
#else
#   if OGE_COMPILER == OGE_COMPILER_MSVC
#       if OGE_COMP_VER > 1300 && !defined(_STLP_MSVC)
#           define OgeHashMap ::stdext::hash_map
#       else
#           define OgeHashMap ::std::hash_map
#       endif
#   else
#       define OgeHashMap ::std::hash_map
#   endif
#endif

#if OGE_COMPILER == OGE_COMPILER_MSVC
#   define OGE_NAKED_FN __declspec(naked)
#else
#   define OGE_NAKED_FN
#endif

namespace oge
{

    /** oge::Real typedefs float or double.
     * @brief To compile OGE with double precision, set OGE_DOUBLE in the
     *        compiler define settings (or add a #define OGE_DOUBLE).
     */
    #ifdef OGE_DOUBLE
        typedef double Real;
    #else
        typedef float Real;
    #endif  

    // Forward declaration of OGE Utility classes
    class AddressTranslator;
    class Archive;
    class ArchiveFactory;
    class ArchiveGroup;
    class BinarySerialiser;
    class Buffer;
    class BufferedStream;
    class BufferMemoryStream;
    template<typename T> class ConcurrentQueue;
    class Clock;
    class ConsoleLogger;
    class DataStream;
    class DataStreamOwner;
    class Degree;
    class DeltaQueue;
    class File;
    class FileFilter;
    class FileSystem;
    class FileSystemListener;
    class FileSystemStream;
    class HtmlLogger;
    class Logger;
    class LogManager;
    class Math;
    class MemoryStream;
    class Message;
    class MessageDispatcher;
    class MessageScheduler;
    class Option;
    class OptionManager;
    class OptionReader;
    class OptionReaderFactory;
    class Quaternion;
    class OptionReader;
    class OptionReaderFactory;
    class Plugin;
    class PluginManager;
    class Radian;
    class Resource;
    class ResourceController;
    class ResourceGroup;
    class ResourceManager;
    class ResourceOp;
    class ResourceRegistry;
    class Serialisable;
    class Serialiser;
    template<typename T> class StandardQueue;
    class TabLogger;
    class Task;
    class TaskThread;
    class TextLogger;
    class TextReader;
    class TextWriter;
    class Thread;
	class Vector2;
    class Vector3;
    class XmlOptionReader;
    class XmlOptionReaderFactory;
    class XmlReader;

    // Declaring custom types
    typedef Poco::Thread        OSThread;

    using   Poco::SharedPtr;
    using   Poco::AutoPtr;

    typedef Poco::File          FileUtil;
    using   Poco::Path;
    using   Poco::Glob;

    using   Poco::Int8;
    using   Poco::UInt8;
    using   Poco::Int16;
    using   Poco::UInt16;
    using   Poco::Int32;
    using   Poco::UInt32;
    using   Poco::Int64;
    using   Poco::UInt64;
    using   Poco::IntPtr;
    using   Poco::UIntPtr;
    using   Poco::Void;
    using   Poco::Any;
    using   Poco::AnyCast;
    using   Poco::RuntimeException;
    using   Poco::NullPointerException;

    using namespace fastdelegate;
    
}

// Useful macros

// Generates compiler warnings - should work on any compiler
#define OGE_QUOTE_INPLACE(x) # x
#define OGE_QUOTE(x) OGE_QUOTE_INPLACE(x)
#if (OGE_COMPILER == OGE_COMPILER_MSVC)
// If the message is started with 'Warning: ', the MSVC IDE actually does catch a warning :)
#   define OGE_WARN(x)  message( __FILE__ "(" OGE_QUOTE(__LINE__) ") : warning:  " x )
#else
// WIP: #define OGE_WARN(x)  #warningmessage "Waaaduuuup?!"    <<< might work on gcc
#    define OGE_WARN(x) LOGE(x); // TODO try the code 3 lines above!
#endif

/** 
 * The implementation of Poco::AnyCast(Any*) is more efficient 
 * than AnyCast(Any&) and should be preferred.
 *
 * However AnyCast makes a comparison that shouldn't be necessary
 * in release. So the more direct cast UnsafeAnyCast should be 
 * used.
 * @usage For example FastAnyCast(OIS::KeyEvent*, any)
 * 
 * @note Thanks, Mehdi.
 */
#ifdef RELEASE_CANDIDATES
#   define FastAnyCast(x, y) Poco::UnsafeAnyCast<x>(y)
#else
#   define FastAnyCast(x, y) Poco::AnyCast<x>(y)
#endif

// To avoid gcc "unused variable" warnings (we could use -Wno-unused-variable
#define OGE_UNUSED(x) (void)x;

#endif // __OGE_UTILITIESPREREQUISITES_H__
