/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_PLUGINMANAGER_H__
#define __OGE_PLUGINMANAGER_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeSingleton.h"
#include "oge/OgeString.h"

namespace oge
{
    /// Abstract struct that is passed to created plugin objects.
    typedef struct Plugin_Params
    {
        const unsigned char* pluginType;
        PluginManager* mgr; //TODO const struct PluginServices * platformServices;
    } Plugin_Params;

    /** 
     * The functions pointer definitions PluginCreate and PluginDestroy 
     * (implemented by the plugin) allow the plugin manager 
     * to create and destroy plugin objects (each plugin registers such functions
     * with the plugin manager)
     */
    typedef Plugin* (*PluginCreate)(Plugin_Params *); 
    typedef int (*PluginDestroy)(Plugin*);

    typedef struct PluginServices
    {
        int versionMajor;
        int versionMinor;
        PluginManager* mgr; // NEXT see in reference: Plugin_RegisterFct registerObject; 
        //NEXT Plugin_InvokeServiceFct invokeService; 
    } PluginServices;

#ifdef OGE_UTILITY_EXPORTS
    #ifdef  __cplusplus
    /// @note Note that several plugins could be registered at once in this method
    extern "C" bool OGE_UTIL_API pluginRegistration(const PluginServices* mgr, void* hdll);
    #else
    /// @note Note that several plugins could be registered at once in this method
    extern OGE_UTIL_API bool pluginRegistration(const PluginServices* mgr, void* hdll);
    #endif
#endif

} // namespace

//--------------------------------------------------------------------
// Global namespace!
typedef bool (*PluginInitFct)(const oge::PluginServices*, void* hdll);
//--------------------------------------------------------------------

namespace oge
{
    /**
     * Plugin manager used to load dynamic (dll) or static (lib) plugins
     *
     * @usage This manager is usually created and used by an EngineManager
     *        But not necessarly you can create it directly like this:
     *
     *    oge::PluginManager* pluginManager = oge::PluginManager::createSingleton();
     *    std::vector<std::string> files = getAllDllFilenames("plugins_dir");
     *    for (unsigned int i = 0; i<files.size(); ++i)
     *        pluginManager->loadPlugin( files[i] );
     *    oge::Plugin* plugin1 = pluginManager->getPlugin(0);
     *    plugin1->process( 0 );  // do something
     *    pluginManager->unloadAll();
     *    pluginManager->destroySingleton();
     *
     *  To load a static plugin you need:
     *
     *    1 - To define xxx_STATIC_LIB as preprocessor definition
     *        where xxx is the name of the plugin usually.
     *    2 - To implement the entry point of the plugin
     *        which must be unique to each plugin type
     *
     *    pluginManager->loadStaticPlugin(OpalPlugin_Static_Init);
     *
     *  This static entry point h is simply defined like this 
     *  for an Opal plugin in OgeOpalPhysics.h:
     *
     *    extern "C" OgeOpalPluginExport bool OpalPlugin_Static_Init(const oge::PluginServices* mgr);
     *
     * @note for later: Perhaps a registrar will be needed on linux:
     *    struct PluginRegistrar
     *    {
     *        PluginRegistrar(PluginInitFct init)
     *        {
     *            oge::PluginManager::loadStaticPlugin(init);
     *        }
     *    };
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_UTIL_API PluginManager  : public Singleton<PluginManager>
    {
    public:
        friend class Singleton<class PluginManager>;

        /** 
         * Contains all the information that a plugin must provide 
         * to the plugin manager upon initialization 
         * (version, create/destroy functions, and programming language).
         */
        typedef struct PluginRegisterParams
        {
            int versionMajor;
            int versionMinor;
            void* hDll; // HMODULE on Win32
            PluginCreate createFct;
            PluginDestroy destroyFct;
            Plugin* plugin; // TODO We could/should put this in a separate map! As this is used in registerPluginInstance()!!!
        } PluginRegisterParams;

    private:
        /// Contains the plugins
        OgeHashMap<String, PluginRegisterParams> mPlugins;

    public:
        bool loadPlugin( const String& filename );
        void unloadAll();

        size_t getNumPlugins() const;
        Plugin* getPlugin( const String& name );
        
        static PluginManager* createSingleton();
        /**
         * Used to delete the instance.
         */
        static void destroySingleton();

        static bool registerPluginInstance(const unsigned char* pluginType, 
            const PluginRegisterParams* params);

        static bool loadStaticPlugin(PluginInitFct initFunc);

        /**
         * Initialise a plugin name
         * @param name Plugin registration name
         * @returns true if initialise correct.
         */
        bool initialisePlugin(const String& name);

    private:
        PluginManager();
        virtual ~PluginManager();

        PluginServices mPlatformServices;
    };
}
#endif
