/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_SERIALISER_H__
#define __OGE_SERIALISER_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeString.h"

namespace oge
{
    /** 
     * The interface Serialiser must be implemented by all classes that want
     * to be able to serialise instances (whose classes must implement 
     * the Serialisable interface).
     *
     * @author Steven 'lazalong' Gay
     */
    class Serialiser
    {
    protected:
        bool mReadMode;
        bool mWriteMode;
        DataStream* mDataStream;

    public:
        Serialiser(DataStream* dataStream) :
            mReadMode(false), mWriteMode(false), mDataStream(dataStream) {};
        virtual ~Serialiser() {};

        inline bool getReadMode() const { return mReadMode; };
        inline bool getWriteMode() const { return mWriteMode; };

        // Read methods (must have a write equivalent)
        virtual int readInt() = 0;
        virtual float readFloat() = 0;
        virtual String readString() = 0;
        virtual Vector3 readVector3() = 0;
        virtual Quaternion readQuaternion() = 0;
        virtual void* readPointer() = 0;

         // Write methods (must have a read equivalent)
        virtual void writeInt        (int i) = 0;
        virtual void writeFloat      (float f) = 0;
        virtual void writeString     (const String & s) = 0;
        virtual void writeVector3    (const Vector3& v) = 0;
        virtual void writeQuaternion (const Quaternion& q) = 0;
        virtual void writePointer    (void* p) = 0;
    };
}

#endif // __OGE_SERIALISER_H__
