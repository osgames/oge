/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_ADDRESSTRANSLATOR_H__
#define __OGE_ADDRESSTRANSLATOR_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/datastreams/OgeDataStream.h"

namespace oge
{
    /**
     * This class saves in memory the addresses of newly created objects
     * to be able to translate the "old" pointers to the new ones.
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_UTIL_API AddressTranslator
    {
    private:
        typedef OgeHashMap<void*, void*> AddressTable;
        static AddressTable sAddressTable;

    public:
        /**
         * Save the memory address of a newly created object against its old one.
         */
        static void addAddress(void* oldAddress, void* newAddress);
        /** 
         * Retrieve the address of a new instantiation corresponding
         * to an old instantiation (using its old address).
         */
        static void* translateAddress(void* oldAddress);
        static void resetAddresses();
    };
}

#endif // __OGE_ADDRESSTRANSLATOR_H__
