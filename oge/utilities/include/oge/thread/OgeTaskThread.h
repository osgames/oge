/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_TASKPROCESSOR_H__
#define __OGE_TASKPROCESSOR_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/thread/OgeThread.h"
#include "oge/containers/OgeQueue.h"
#include "oge/OgeAsyncValue.h"
#include "Poco/Event.h"

namespace oge
{
    /**
     * Simple implementation of thread that waits for a task to be added to
     * its queue and processes it.
     *
     * One use of this class is for basic background processing.
     *
     * @author Christopher Jones
     */
    class OGE_UTIL_API TaskThread : public Thread
    {
    public:
        class OGE_UTIL_API Listener
        {
            friend class TaskThread;
        protected:
            TaskThread* mThread;
        public:
            Listener();
            virtual ~Listener();
            virtual AsyncBool attachToThread(TaskThread* thread);
            virtual AsyncBool dettachFromThread();
            virtual bool isAttachedToThread() const { return mThread != 0; }
        protected:
            bool attachedToThread(TaskThread* thread);
            bool dettachedFromThread();
            virtual void notifyAttachedToThread() {}
            virtual void notifyDettachedFromThread() {}
            virtual void notifyThreadStarted() {}
            virtual void notifyThreadStopped() {}
        };
    public:
        typedef ConcurrentQueue<Task*> TaskQueue;
        //typedef StandardQueue<AsyncFunction*> TaskQueue;
        typedef std::vector<Listener*> ListenerVector;
    protected:
        //FastMutex mWaitMutex;
        //Condition mWaitCondition;
        Mutex mListenerMutex;
        Poco::Event mWaitEvent;
        TaskQueue mTasks;
        volatile bool mContinue;
        ListenerVector mListeners;
    public:
        /**
         * Constructor creates the basic task processor
         */
        TaskThread();
        /**
         * Constructor creating the task processor and giving the thread a name
         */
        TaskThread(const String& name);
        /**
         * Constructor creating the task thread using an existing thread
         */
        TaskThread(OSThread* thread);
        /**
         * Destructor
         */
        virtual ~TaskThread();
        /**
         * Stops the thread using a task.
         *
         * This thread will stop once it processes the task, so tasks added
         * before the shutdown task will still be processed.
         */
        void stop();
        /**
         * Stops the thread as soon as possible.
         */
        void stopNow();
        /**
         * Adds a task to the queue to be processed
         */
        void addTask(Task* task);
        /**
         * Adds a listener
         */
        void addListener(Listener* listener);
        /**
         * Removes a listener
         */
        void removeListener(Listener* listener);
    protected:
        /**
         * The threads entry point
         */
        void run();
        /**
         * Stops the main loop of the thread.
         */
        void _stopThread();
        /**
         * Called when the thread enters its entry point before processing
         * any tasks
         */
        virtual void notifyThreadStarted();
        /**
         * Called just before the thread is about to exit its entry point
         */
        virtual void notifyThreadStopped();
    };
}

#endif // __OGE_TASKPROCESSOR_H__
