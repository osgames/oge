/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2008 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_MUTEXESTBB_H__
#define __OGE_MUTEXESTBB_H__

#include "oge/OgeUtilitiesPrerequisites.h"

#if !defined(OGE_USE_TBB) || OGE_USE_TBB == 0 // disabled
#   error Inclusion of OgeTBBMutexes.h when use of TBB is disabled.
#endif

#include "tbb/mutex.h"
#include "tbb/spin_mutex.h"
#include "tbb/spin_rw_mutex.h"

namespace oge
{
    /**
     * Consistent (non-recursive) Mutex interface using TBB mutex
     *
     * @author Christopher Jones
     */
    class MutexTBB
    {
    public:
        class ScopedUnlock;
        class ScopedLock
        {
        public:
            friend class MutexTBB::ScopedUnlock;
        protected:
            tbb::mutex::scoped_lock mLock;
        public:
            inline ScopedLock(MutexTBB& mutex) : mLock(mutex._getInternalMutex()) {}
            inline ~ScopedLock() {}
        protected:
            inline tbb::mutex::scoped_lock& _getInternalLock() { return mLock; }
        };
        class ScopedUnlock
        {
        protected:
            tbb::mutex& mMutex;
            tbb::mutex::scoped_lock& mLock;
        public:
            inline ScopedUnlock(MutexTBB& mutex, MutexTBB::ScopedLock& lock, 
                bool now = true) :  mMutex(mutex._getInternalMutex()), 
                                    mLock(lock._getInternalLock())
            {
                if (now)
                    unlock();
            }
            inline ~ScopedUnlock()
            {
                mLock.acquire(mMutex); 
            }
            inline void unlock()
            {
                mLock.release();
            }
        };
    protected:
        tbb::mutex mMutex;
    public:
        inline MutexTBB() { }
        inline ~MutexTBB() { }
    protected:
        inline tbb::mutex& _getInternalMutex() { return mMutex; }
    };

    /**
     * Consistent (spin/busy) Mutex interface using TBB spin_mutex
     *
     * @author Christopher Jones
     */
    class SpinMutexTBB
    {
    public:
        class ScopedUnlock;
        class ScopedLock
        {
        public:
            friend class SpinMutexTBB::ScopedUnlock;
        protected:
            tbb::spin_mutex::scoped_lock mLock;
        public:
            inline ScopedLock(SpinMutexTBB& mutex) : mLock(mutex._getInternalMutex()) {}
            inline ~ScopedLock() {}
        protected:
            inline tbb::spin_mutex::scoped_lock& _getInternalLock() { return mLock; }
        };
        class ScopedUnlock
        {
        protected:
            tbb::spin_mutex& mMutex;
            tbb::spin_mutex::scoped_lock& mLock;
        public:
            inline ScopedUnlock(SpinMutexTBB& mutex, SpinMutexTBB::ScopedLock& lock) : 
                mMutex(mutex._getInternalMutex()),
                mLock(lock._getInternalLock())
            {
                mLock.release();
            }
            inline ~ScopedUnlock() { mLock.acquire(mMutex); }
        };
    protected:
        tbb::spin_mutex mMutex;
    public:
        inline SpinMutexTBB() { }
        inline ~SpinMutexTBB() { }
    protected:
        inline tbb::spin_mutex& _getInternalMutex() { return mMutex; }
    };

    /**
     * Consistent (spin/busy wait) Read/Write Mutex interface using
     * TBB spin_rw_mutex
     *
     * @author Christopher Jones
     */
    class SpinRWMutexTBB
    {
    public:
        class ScopedLock
        {
        protected:
            tbb::spin_rw_mutex::scoped_lock mLock;
        public:
            inline ScopedLock(SpinRWMutexTBB& mutex, bool write)
            {
                mLock.acquire(mutex._getInternalMutex(), write);
            }
            inline ~ScopedLock() {}
        };
    protected:
        tbb::spin_rw_mutex mMutex;
    public:
        inline SpinRWMutexTBB() { }
        inline ~SpinRWMutexTBB() { }
    protected:
        inline tbb::spin_rw_mutex& _getInternalMutex() { return mMutex; }
    };
}

#endif // __OGE_MUTEXESTBB_H__
