/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2008 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_MUTEXESPOCO_H__
#define __OGE_MUTEXESPOCO_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "Poco/Mutex.h"
#include "Poco/RWLock.h"

namespace oge
{
    /**
     * Consistent (non-recursive) Mutex interface using POCO FastMutex
     *
     * @author Christopher Jones
     */
    class MutexPOCO
    {
    public:
        class ScopedLock
        {
        protected:
            Poco::FastMutex& mMutex;
        public:
            inline ScopedLock(MutexPOCO& mutex) : 
                mMutex(mutex._getInternalMutex()) 
            { 
                mMutex.lock(); 
            }
            inline ~ScopedLock()
            { 
                mMutex.unlock();
            }
        };
        class ScopedUnlock
        {
        protected:
            Poco::FastMutex& mMutex;
        public:
            inline ScopedUnlock(MutexPOCO& mutex, MutexPOCO::ScopedLock& lock,
                bool now = true) : mMutex(mutex._getInternalMutex())
            {
                if (now)
                    mMutex.unlock(); 
            }
            inline ~ScopedUnlock()
            {
                mMutex.lock(); 
            }
            inline void unlock()
            {
                mMutex.unlock();
            }
        };
    protected:
        Poco::FastMutex mMutex;
    public:
        inline MutexPOCO() {}
        inline ~MutexPOCO() {}
    protected:
        inline Poco::FastMutex& _getInternalMutex() { return mMutex; }
    };

    /**
     * Consistent Recursive Mutex interface using POCO Mutex
     *
     * @author Christopher Jones
     */
    class RecMutexPOCO
    {
    public:
        class ScopedLock
        {
        protected:
            Poco::Mutex& mMutex;
        public:
            inline ScopedLock(RecMutexPOCO& mutex) : 
                mMutex(mutex._getInternalMutex()) 
            { 
                mMutex.lock(); 
            }
            inline ~ScopedLock()
            { 
                mMutex.unlock(); 
            }
        };
    protected:
        Poco::Mutex mMutex;
    public:
        inline RecMutexPOCO() {}
        inline ~RecMutexPOCO() {}
    protected:
        inline Poco::Mutex& _getInternalMutex() { return mMutex; }
    };

    /**
     * Consistent Read/Write Mutex interface using POCO RWMutex
     *
     * @author Christopher Jones
     */
    class RWMutexPOCO
    {
    public:
        class ScopedLock
        {
        protected:
            Poco::RWLock& mMutex;
        public:
            inline ScopedLock(RWMutexPOCO& mutex, bool write) : 
                mMutex(mutex._getInternalMutex()) 
            {
                if (!write) 
                    { mMutex.readLock(); } 
                else 
                    { mMutex.writeLock(); }
            }
            inline ~ScopedLock()
            {
                mMutex.unlock();
            }
        };
    protected:
        Poco::RWLock mMutex;
    public:
        inline RWMutexPOCO() {}
        inline ~RWMutexPOCO() {}
    protected:
        inline Poco::RWLock& _getInternalMutex() { return mMutex; }
    };
}

#endif // __OGE_MUTEXESPOCO_H__
