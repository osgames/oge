/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_THREAD_H__
#define __OGE_THREAD_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeNonCopyable.h"
#include "oge/OgeString.h"
#include "Poco/Runnable.h"

namespace oge
{
    /**
     * Common class for all types of threads in OGE.
     *
     * Contains all of the basic functions for interacting with threads.
     * Different types of threads can be made from inheriting this class and
     * implementing the run() and stop() thread methods. It's also
     * possible to override some other methods for more flexibility.
     *
     * @author Christopher Jones
     */
    class OGE_UTIL_API Thread : public Poco::Runnable, public NonCopyable
    {
    protected:
        OSThread*   mThread;
        bool        mOwnsThread;
    public:
        /**
         * Constructor that creates a basic thread
         */
        Thread();
        /**
         * Constructor that creates a thread with the given name
         */
        Thread(const String& name);
        /**
         * Constructor that accepts an existing thread to use. 
         *
         * This thread should not already be running as this class must handle
         * starting and stopping the thread.
         */
        Thread(OSThread* thread);
        /**
         * Destructor that deletes the contained thread only if this class
         * created the thread when it was constructed.
         */
        virtual ~Thread();
        /**
         * The entry point for the thread when it is started.
         *
         * This shouldn't be called except from the start function. To
         * actually start the thread call start().
         * Override this function when creating a new type of thread.
         */
        virtual void run() = 0;
        /**
         * Starts the thread if it hasn't yet been started.
         *
         * The thread actually starts in the run() method.
         */
        virtual void start();
        /**
         * Request the thread to stop.
         *
         * This should stop in order, so any tasks or activities that were
         * requested before this will be performed first.
         * Override this method when creating new types of thread as stopping
         * the thread is specific to the thread itself.
         */
        virtual void stop() = 0;
        /**
         * Tells the thread to stop as soon as it can.
         *
         * This should stop the thread at the first opportunity. This won't
         * maintain order, so activities requested before this wont't be
         * done.
         * Override this method when creating new types of thread.
         */
        virtual void stopNow() = 0;
        /**
         * Blocks the current thread whilst waiting for this thread to stop.
         */
        virtual void join();
        /**
         * Returns the threads ID that is assigned to it when it is created
         */
        int getID() const;
        /**
         * Returns the threads name which is assigned when it is created
         */
        String getName() const;
        /**
         * Get the threads priority level
         */
        OSThread::Priority getPriority() const;
        /**
         * Sets the threads priority level
         */
        void setPriority(OSThread::Priority priority);
        /**
         * Is this thread running?
         */
        bool isRunning() const;
    };
}

#endif // __OGE_THREAD_H__
