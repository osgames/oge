/*
-----------------------------------------------------------------------------
This source file is part of OGRE
    (Object-oriented Graphics Rendering Engine)
For the latest info, see http://www.ogre3d.org/

Copyright (c) 2000-2012 Torus Knot Software Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
/*

    Although the code is original, many of the ideas for the profiler were borrowed from 
"Real-Time In-Game Profiling" by Steve Rabin which can be found in Game Programming
Gems 1.

    This code can easily be adapted to your own non-Oge project. The only code that is 
Oge-dependent is in the visualization/logging routines and the use of the Timer class.

    Enjoy!

*/

#ifndef __OGE_PROFILER__
#define __OGE_PROFILER__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeSingleton.h"
#include "oge/OgeString.h"

#if OGE_PROFILING == 1
#	define OgeProfile( a ) oge::Profile _OgeProfileInstance( (a) )
#	define OgeProfileBegin( a ) oge::Profiler::getSingleton().beginProfile( (a) )
#	define OgeProfileEnd( a ) oge::Profiler::getSingleton().endProfile( (a) )
#	define OgeProfileGroup( a, g ) oge::Profile _OgeProfileInstance( (a), (g) )
#	define OgeProfileBeginGroup( a, g ) oge::Profiler::getSingleton().beginProfile( (a), (g) )
#	define OgeProfileEndGroup( a, g ) oge::Profiler::getSingleton().endProfile( (a), (g) )
#else
#   define OgeProfile( a )
#   define OgeProfileBegin( a )
#   define OgeProfileEnd( a )
#	define OgeProfileGroup( a, g ) 
#	define OgeProfileBeginGroup( a, g ) 
#	define OgeProfileEndGroup( a, g ) 
#endif

namespace oge {

	/** List of reserved profiling masks
	*/
	enum ProfileGroupMask
	{
		/// User default profile
		OGEPROF_USER_DEFAULT = 0x00000001,
		/// All in-built Oge profiling will match this mask
		OGEPROF_ALL = 0xFFF00000,
		/// General processing
		OGEPROF_GENERAL = 0x80000000,
		/// Reserved
		OGEPROF_RESERVED = 0x40000000,
		/// Systems
		OGEPROF_SYSTEMS = 0x20000000
	};

    /** An individual profile that will be processed by the Profiler
        @remarks
            Use the macro OgeProfile(name) instead of instantiating this profile directly
        @remarks
            We use this Profile to allow scoping rules to signify the beginning and end of
            the profile. Use the Profiler singleton (through the macro OgeProfileBegin(name)
            and OgeProfileEnd(name)) directly if you want a profile to last
            outside of a scope (i.e. the main game loop).
        @author Amit Mathew (amitmathew (at) yahoo (dot) com) & Alex Peterson (petrocket)
    */
	class OGE_UTIL_API Profile  {
        public:
            Profile(const String& profileName, unsigned int groupID = (unsigned int)OGEPROF_USER_DEFAULT);
            ~Profile();

        protected:

            /// The name of this profile
            String mName;
			/// The group ID
			unsigned int mGroupID;
    };

    /** The profiler allows you to measure the performance of your code
        @remarks
            Do not create profiles directly from this unless you want a profile to last
            outside of its scope (i.e. the main game loop). For most cases, use the macro
            OgeProfile(name) and braces to limit the scope. You must enable the Profile
            before you can used it with setEnabled(true). If you want to disable profiling
            in Oge, simply set the macro OGRE_PROFILING to 0.
        @author Amit Mathew (amitmathew (at) yahoo (dot) com) & Alex Peterson (petrocket)
        @todo resolve artificial cap on number of profiles displayed
        @todo fix display ordering of profiles not called every frame
    */
    class OGE_UTIL_API Profiler : public Singleton<Profiler> {

        public:
            Profiler();
            ~Profiler();

			static Profiler* createSingleton();
			static void destroySingleton();

            /// Represents an individual profile call
            struct ProfileInstance {

                /// The name of the profile
                String		name;

                /// The name of the parent, empty string if root
                String		parent;

                /// The time this profile was started
                unsigned long		currTime;

                /// Represents the total time of all child profiles to subtract
                /// from this profile
                unsigned long		accum;

                /// The hierarchical level of this profile, 0 being the root profile
                unsigned int		hierarchicalLvl;
            };

            /// Represents the total timing information of a profile
            /// since profiles can be called more than once each frame
            struct ProfileFrame {
				
                /// The name of the profile
                String	name;

                /// The total time this profile has taken this frame
                unsigned long	frameTime;

                /// The number of times this profile was called this frame
                unsigned int	calls;

                /// The hierarchical level of this profile, 0 being the main loop
                unsigned int	hierarchicalLvl;

            };
			
            /// Represents a history of each profile during the duration of the app
            struct ProfileHistory {

                /// The name of the profile
                String	name;

                /// The current percentage of frame time this profile has taken
                Real	currentTimePercent; 
				/// The current frame time this profile has taken in milliseconds
				Real	currentTimeMillisecs;

                /// The maximum percentage of frame time this profile has taken
                Real	maxTimePercent; 
				/// The maximum frame time this profile has taken in milliseconds
				Real	maxTimeMillisecs; 

                /// The minimum percentage of frame time this profile has taken
                Real	minTimePercent; 
				/// The minimum frame time this profile has taken in milliseconds
				Real	minTimeMillisecs; 

                /// The number of times this profile has been called each frame
                unsigned int	numCallsThisFrame;

                /// The total percentage of frame time this profile has taken
                Real	totalTimePercent;
				/// The total frame time this profile has taken in milliseconds
				Real	totalTimeMillisecs;

                /// The total number of times this profile was called
                /// (used to calculate average)
                unsigned long	totalCalls; 

                /// The hierarchical level of this profile, 0 being the root profile
                unsigned int	hierarchicalLvl;
			};

            typedef std::list<ProfileInstance> ProfileStack;
            typedef std::list<ProfileFrame> ProfileFrameList;
            typedef std::list<ProfileHistory> ProfileHistoryList;
            typedef std::map<String, ProfileHistoryList::iterator> ProfileHistoryMap;
            typedef std::map<String, bool> DisabledProfileMap;

            /** Sets the clock for the profiler */
            void setClock(Clock* t);

            /** Retrieves the clock for the profiler */
            Clock* getClock();

            /** Begins a profile
            @remarks 
                Use the macro OgeProfileBegin(name) instead of calling this directly 
                so that profiling can be ignored in the release version of your app. 
            @remarks 
                You only use the macro (or this) if you want a profile to last outside
                of its scope (i.e. the main game loop). If you use this function, make sure you 
                use a corresponding OgeProfileEnd(name). Usually you would use the macro 
                OgeProfile(name). This function will be ignored for a profile that has been 
                disabled or if the profiler is disabled.
            @param profileName Must be unique and must not be an empty string
			@param groupID A profile group identifier, which can allow you to mask profiles
            */
            void beginProfile(const String& profileName, unsigned int groupID = (unsigned int)OGEPROF_USER_DEFAULT);

            /** Ends a profile
            @remarks 
                Use the macro OgeProfileEnd(name) instead of calling this directly so that
                profiling can be ignored in the release version of your app.
            @remarks
                This function is usually not called directly unless you want a profile to
                last outside of its scope. In most cases, using the macro OgeProfile(name) 
                which will call this function automatically when it goes out of scope. Make 
                sure the name of this profile matches its corresponding beginProfile name. 
                This function will be ignored for a profile that has been disabled or if the
                profiler is disabled.
			@param profileName Must be unique and must not be an empty string
			@param groupID A profile group identifier, which can allow you to mask profiles
            */
            void endProfile(const String& profileName, unsigned int groupID = (unsigned int)OGEPROF_USER_DEFAULT);

            /** Sets whether this profiler is enabled. Only takes effect after the
                the frame has ended.
                @remarks When this is called the first time with the parameter true,
                it initializes the GUI for the Profiler
            */
            void setEnabled(bool enabled);

            /** Gets whether this profiler is enabled */
            bool getEnabled() const;

            /** Enables a previously disabled profile 
            @remarks Only enables the profile if this function is not 
            called during the profile it is trying to enable.
            */
            void enableProfile(const String& profileName);

            /** Disables a profile
            @remarks Only disables the profile if this function is not called during
            the profile it is trying to disable.
            */
            void disableProfile(const String& profileName);

			/** Set the mask which all profiles must pass to be enabled. 
			*/
			void setProfileGroupMask(unsigned int mask) { mProfileMask = mask; }
			/** Get the mask which all profiles must pass to be enabled. 
			*/
			unsigned int getProfileGroupMask() const { return mProfileMask; }

			unsigned long getMaxTotalFrameTime() { return mMaxTotalFrameTime; }

			ProfileHistoryList *getProfileHistoryList() { return &mProfileHistory; }

            /** Returns true if the specified profile reaches a new frame time maximum
            @remarks If this is called during a frame, it will be reading the results
            from the previous frame. Therefore, it is best to use this after the frame
            has ended.
            */
            bool watchForMax(const String& profileName);

            /** Returns true if the specified profile reaches a new frame time minimum
            @remarks If this is called during a frame, it will be reading the results
            from the previous frame. Therefore, it is best to use this after the frame
            has ended.
            */
            bool watchForMin(const String& profileName);

            /** Returns true if the specified profile goes over or under the given limit
                frame time
            @remarks If this is called during a frame, it will be reading the results
            from the previous frame. Therefore, it is best to use this after the frame
            has ended.
            @param limit A number between 0 and 1 representing the percentage of frame time
            @param greaterThan If true, this will return whether the limit is exceeded. Otherwise,
            it will return if the frame time has gone under this limit.
            */
            bool watchForLimit(const String& profileName, Real limit, bool greaterThan = true);

            /** Outputs current profile statistics to the log */
            void logResults();

            /** Clears the profiler statistics */
            void reset();

            /** Override standard Singleton retrieval.*/
            static Profiler& getSingleton(void);

            /** Override standard Singleton retrieval.*/
            static Profiler* getSingletonPtr(void);

        protected:

            /** Initializes the profiler's GUI elements */
            void initialize();

            /** Prints the profiling results of each frame/tick */
            void displayResults();

            /** Processes the profiler data after each frame/tick */
            void processFrameStats();

            /** Handles a change of the profiler's enabled state*/
            void changeEnableState();

            /// A stack for each individual profile per frame
            ProfileStack mProfiles;

            /// Accumulates the results of each profile per frame (since a profile can be called
            /// more than once a frame)
            ProfileFrameList mProfileFrame;

            /// Keeps track of the statistics of each profile
            ProfileHistoryList mProfileHistory;

            /// We use this for quick look-ups of profiles in the history list
            ProfileHistoryMap mProfileHistoryMap;

            /// Holds the names of disabled profiles
            DisabledProfileMap mDisabledProfiles;

            /// Whether the GUI elements have been initialized
            bool mInitialized;

            /// The max number of profiles we can display
            unsigned int mMaxDisplayProfiles;

            /// The number of frames that must elapse before the current
            /// frame display is updated
            unsigned int mUpdateDisplayFrequency;

            /// The number of elapsed frame, used with mUpdateDisplayFrequency
            unsigned int mCurrentFrame;

            /// The clock used for profiling
            Clock* mClock;

            /// The total time each frame takes
            unsigned long mTotalFrameTime;

            /// Whether this profiler is enabled
            bool mEnabled;

            /// Keeps track of whether this profiler has
            /// received a request to be enabled/disabled
            bool mEnableStateChangePending;

            /// Keeps track of the new enabled/disabled state that the user has requested
            /// which will be applied after the frame ends
            bool mNewEnableState;

			/// Mask to decide whether a type of profile is enabled or not
			unsigned int mProfileMask;

			/// The max frame time recorded
			unsigned long mMaxTotalFrameTime;

			/// Rolling average of millisecs
			Real mAverageFrameTime;
			bool mResetExtents;


    }; // end class

} // end namespace

#endif
