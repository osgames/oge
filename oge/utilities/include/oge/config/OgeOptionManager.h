/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_OPTIONMANAGER_H__
#define __OGE_OPTIONMANAGER_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/config/OgeOptionReader.h"
#include "oge/config/OgeOption.h"
#include "oge/OgeSingleton.h"
#include "oge/OgeString.h"

namespace oge
{
    class OGE_UTIL_API OptionManager : public Singleton<OptionManager>
    {
    private:
        typedef std::map<String, OptionReaderFactory*> OptionReaderFactoryMap;
        typedef std::map<String, String> StringMap;
    private:
        OptionPtr mRootOption;
        OptionReaderFactoryMap mOptionReaderFactories;
        OptionReaderFactoryMap mReaderFileExtensions;
        Mutex mOptionFactoriesMutex;
    public:
        void initialise();
        void shutdown();
        /**
         * Adds an Option using a name.
         *
         * @param   name            The name for the option. This must be unique
         *                          for this parent option.
         * @param   strict          The mode for the new option to be in.
         * @param   alterModified   if true the options modified flag will be
         *                          set to true, if false, it wont be changed
         *
         * @return  The new option, null if it could not be created.
         */
        OptionPtr addOption(const String& name, bool strict = false, 
            bool alterModified = true);
        /**
         * Remove a sub-option by its name
         *
         * @param   name            the name of the option to remove
         * @param   alterModified   if true the options modified flag will be
         *                          set to true, if false, it wont be changed
         *
         * @return  true if the option exist and was removed
         */
        bool removeOption(const String& name, bool alterModified = true);
        /**
         * Removes all sub-options
         *
         * @param   setModified   sets the options modified flag
         */
        void removeAllOptions(bool setModified = true);
        /**
         * Retrieves a sub-option given its name.
         *
         * @param   name                        The name of the option
         * @param   overrideStrict              Allows the option to be created
         *                                      even in strict mode
         * @param   alterModifiedIfCreated      If the option was created due
         *                                      to the option being non strict
         *                                      or overrideStrict == true then
         *                                      the options modified flag will
         *                                      be set to true if this param
         *                                      is true
         *
         * @return  The option with the given name. null if it doesn't exist.
         */
        OptionPtr getOption(const String& name, bool overrideStrict = false,
            bool alterModifiedIfCreated = true);
        /**
         * Retrieves a read-only sub-option given its name.
         *
         * This will not create an option if it doesn't exist even in non
         * strict mode.
         *
         * @return  The option with the given name. null if it doesn't exist.
         */
        const OptionPtr getOption(const String& name) const;
        /**
         * Retrieve an iterator to the contained options
         *
         * This is not thread-safe, care must be taken when accessing this
         * iterator that the map is not modified.
         */
        Option::Iterator getOptionIterator();
        /**
         * Retrieve a const iterator to the contained options
         *
         * This is not thread-safe, care must be taken when accessing this
         * iterator that the map is not modified.
         */
        Option::ConstIterator getOptionIterator() const;
        /**
         * UNSAFE: Retrieves a copy of the map of options.
         *
         * A copy is created to keep this class thread-safe. The copy is
         * only a shallow copy of the map. The options contained still point
         * to the same options stored in this option.
         *
         * This is unsafe across DLL boundaries for (at least)
         * Microsoft's implementation of STL.
         */
        Option::OptionMap getOptionMap();
        /**
         * Retrieve the Option that contains all options
         */
        OptionPtr getRootOption() const { return mRootOption; }

        bool registerOptionReaderFactory(OptionReaderFactory* factory);
        void unregisterOptionReaderFactory(const String& type);
        void unregisterOptionReaderFactory(OptionReaderFactory* factory);

        bool registerFileExtension(const String& extension, const String& type);
        void unregisterFileExtension(const String& extension);

        // TODO: remove type once automatic type detection has been written
        bool loadFromFilename(const String& filename, const String& type);
        bool loadFromDataStream(DataStreamPtr dataStream, const String& type);
        bool loadFromFilename(const String& filename);

        static OptionManager* createSingleton();
        static void destroySingleton();
        static OptionManager* getSingletonPtr();
        static OptionManager& getSingleton();
    protected:
        OptionManager();
        ~OptionManager();

        OptionReader* createReaderFromType(const String& type);
        OptionReader* createReaderFromExtension(const String& extension);
        OptionReader* createReaderFromContent(DataStreamPtr dataStream);
    };
}

#endif // __OGE_OPTIONMANAGER_H__
