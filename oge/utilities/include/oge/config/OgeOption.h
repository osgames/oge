/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_OPTION_H__
#define __OGE_OPTION_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeReferenceCounter.h"
#include "oge/OgeString.h"
#include "oge/OgeNonCopyable.h"

namespace oge
{
    typedef SharedPtr<Option, oge::ReferenceCounter> OptionPtr;

    /**
     * Holds a list of values and a map of sub-options.
     *
     * The map of sub-options allows creating a hierarchy of options.
     * The option can be in strict mode or non-strict mode. In strict
     * mode sub-options are only added when you call the appropriate
     * add option functions. If you try to retrieve an option which
     * doesn't exist, null is returned.
     * In non-strict mode, retrieving an option which doesn't exist
     * will create the option first. The created option will be non-strict
     * as well.
     *
     * @note    This class is thread-safe.
     *
     * @author  Christopher Jones
     */
    class OGE_UTIL_API Option : public NonCopyable
    {
        friend class OptionManager;
    private:
        typedef std::map<String, OptionPtr> OptionMap;
        typedef std::vector<String> ValueVector;
    public:
        class OGE_UTIL_API Iterator
        {
            friend class Option;
        private:
            OptionMap& mOptions;
            OptionMap::iterator mIter;
        protected:
            Iterator(OptionMap& options);
            Iterator(OptionMap& options, OptionMap::iterator iter);
        public:
            void begin();
            bool hasNext();
            void next();
            OptionPtr getOption();
            String getName();
        };
        class OGE_UTIL_API ConstIterator
        {
            friend class Option;
        private:
            const OptionMap& mOptions;
            OptionMap::const_iterator mIter;
        protected:
            ConstIterator(const OptionMap& options);
            ConstIterator(const OptionMap& options, 
                OptionMap::const_iterator iter);
        public:
            void begin();
            bool hasNext();
            void next();
            OptionPtr getOption();
            String getName();
        };
        class OGE_UTIL_API ValueIterator
        {
            friend class Option;
        private:
            ValueVector& mValues;
            ValueVector::iterator mIter;
        protected:
            ValueIterator(ValueVector& values);
            ValueIterator(ValueVector& values, ValueVector::iterator iter);
        public:
            void begin();
            bool hasNext();
            void next();
            String getValue();
        };
        class OGE_UTIL_API ValueConstIterator
        {
            friend class Option;
        private:
            const ValueVector& mValues;
            ValueVector::const_iterator mIter;
        protected:
            ValueConstIterator(const ValueVector& values);
            ValueConstIterator(const ValueVector& values, 
                ValueVector::const_iterator iter);
        public:
            void begin();
            bool hasNext();
            void next();
            String getValue();
        };
    private:
        const String mName;
        mutable RWMutex mOptionMutex;
        OptionMap mOptions;
        bool mOptionsModified;
        ValueVector mValues;
        bool mValuesModified;
        const bool mStrict;
    public:
        /**
         * Creates an Option with a name and if it is strict or not
         */
        Option(const String& name, bool strict = false);
        ~Option();
        /**
         * Retrieves the number of values that the option has.
         *
         * This does not include the number of sub options.
         */
        size_t getNumberOfValues() const;
        /**
         * Adds a value to the end of the list of values for this option.
         *
         * @param   value           The value to add
         * @param   alterModified   if true the values modified flag will be
         *                          set to true, if false, the flag won't be
         *                          changed
         */
        void addValue(const String& value, bool alterModified = true);
        /**
         * Sets a value at a given index in the list of values.
         *
         * The index must be a valid index in the list. This includes
         * values added and the end of the list, which does the same
         * thing as addValue()
         *
         * @param   value           The value to set
         * @param   index           The position in the list to set the value
         * @param   alterModified   if true the values modified flag will be
         *                          set to true, if false, the flag won't be
         *                          changed
         */
        bool setValue(const String& value, size_t index = 0,
            bool alterModified = true);
        /**
         * Retrieve a value at a given index.
         *
         * The index must a valid index in the list. StringUtil::Blank
         * is returned if its invalid.
         */
        String getValue(size_t index = 0) const;
        /**
         * Removes a value entry from the list.
         *
         * @param   index           the position in the list to remove
         * @param   alterModified   if true the values modified flag will be
         *                          set to true, if false, the flag won't be
         *                          changed
         */
        void removeValue(size_t index = 0, bool alterModified = true);
        /**
         * Removes all values from the list
         *
         * @param   setModified     Sets the values modified flag to true/false
         */
        void removeAllValues(bool setModified = true);
        /**
         * Removes all values from this option and all sub-options recursively
         *
         * @param   setModified     Sets the values modified flag to true/false
         */
        void recursiveRemoveAllValues(bool setModified = true);
        /**
         * Retrieve an iterator for the values contained in this option.
         *
         * The iterator is not thread-safe and care should be taken when
         * iterating through the vector of values, not to have any other
         * threads modifying the values.
         */
        ValueIterator getValueIterator();
        /**
         * Retrieve an iterator for the values contained in this option.
         *
         * The iterator is not thread-safe and care should be taken when
         * iterating through the vector of values, not to have any other
         * threads modifying the values.
         */
        ValueConstIterator getValueIterator() const;
        /**
         * UNSAFE: Gets a copy of all the values inside the option.
         *
         * A copy is created to keep the Option thread-safe.
         * This is not safe across DLL boundaries with the Microsoft's
         * implementation of STL.
         */
        ValueVector getValueVector() const;
        /**
         * Adds a sub-option using a name.
         *
         * @param   name            The name for the option. This must be unique
         *                          for this parent option.
         * @param   strict          The mode for the new option to be in.
         * @param   alterModified   if true the options modified flag will be
         *                          set to true, if false, it wont be changed
         *
         * @return  The new option, null if it could not be created.
         */
        OptionPtr addOption(const String& name, bool strict = false, 
            bool alterModified = true);
        /**
         * Remove a sub-option by its name
         *
         * @param   name            the name of the option to remove
         * @param   alterModified   if true the options modified flag will be
         *                          set to true, if false, it wont be changed
         *
         * @return  true if the option exist and was removed
         */
        bool removeOption(const String& name, bool alterModified = true);
        /**
         * Removes all sub-options
         *
         * @param   setModified   sets the options modified flag
         */
        void removeAllOptions(bool setModified = true);
        /**
         * Retrieves a sub-option given its name.
         *
         * @param   name                        The name of the option
         * @param   overrideStrict              Allows the option to be created
         *                                      even in strict mode
         * @param   alterModifiedIfCreated      If the option was created due
         *                                      to the option being non strict
         *                                      or overrideStrict == true then
         *                                      the options modified flag will
         *                                      be set to true if this param
         *                                      is true
         *
         * @return  The option with the given name. null if it doesn't exist.
         */
        OptionPtr getOption(const String& name, bool overrideStrict = false,
            bool alterModifiedIfCreated = true, bool ignoreSeperator = false);
        /**
         * Retrieves a read-only sub-option given its name.
         *
         * This will not create an option if it doesn't exist even in non
         * strict mode.
         *
         * @return  The option with the given name. null if it doesn't exist.
         */
        const OptionPtr getOption(const String& name) const;
        /**
         * Returns the first option. Useful when "name" created a suboption.
         * @return The first option. Null if there is none.
         */
        const OptionPtr getFirstOption() const;
        /**
         * Retrieves an iterator to the contained options.
         *
         * The iterator is not thread-safe. Care must be taken not to modify
         * the option whilst using the iterator as it bypasses the lock.
         */
        Iterator getOptionIterator();
        /**
         * Retrieves a const iterator to the contained options.
         *
         * The iterator is not thread-safe. Care must be taken not to modify
         * the option whilst using the iterator as it bypasses the lock.
         */
        ConstIterator getOptionIterator() const;
        /**
         * UNSAFE: Retrieves a copy of the internal map of options.
         *
         * This operation is unsafe across DLL boundaries for (at least)
         * Microsoft's implementation of STL.
         */
        OptionMap getOptionMap();
        /**
         * Retrieves a value from a sub option
         */
        String getOptionValue(const String& name, size_t index = 0, 
            bool overrideStrict = false, bool alterModifiedIfCreated = true,
            bool ignoreSeperator = false);
        /**
         * Retrieves a value from a sub option
         */
        String getOptionValue(const String& name, size_t index = 0) const;
        /**
         * Retrieves the name of this option.
         */
        const String& getName() const { return mName; }
        /**
         * Is this option in strict-mode or non-strict mode?
         */
        bool isStrict() const { return mStrict; }
        bool isOptionsModified() const;
        bool isValuesModified() const;
        void setOptionsModified(bool modified);
        void setValuesModified(bool modified);
    };
}

#endif // __OGE_BUFFER_H__
