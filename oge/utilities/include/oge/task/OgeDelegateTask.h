/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt .
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_DELEGATETASK_H__
#define __OGE_DELEGATETASK_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/task/OgeTask.h"
#include "oge/OgeAsyncValue.h"

namespace oge
{
    /**
     * Task that calls a function that doesn't take any parameters
     */
    template<typename RetType> class DelegateTask0 : public Task
    {
    public:
        typedef FastDelegate0<RetType> Delegate;
        typedef AsyncValue<RetType> Result;
    protected:
        Delegate    mDelegate;
        Result      mResult;
    public:
        DelegateTask0(Delegate fn) : mDelegate(fn) {}
        Result getResult() { return mResult; }
        void run() { mResult = mDelegate(); }
    };

    /**
     * Task that calls a function without a return value or parameters
     */
    template<> class DelegateTask0<void> : public Task
    {
    public:
        typedef FastDelegate0<void> Delegate;
    protected:
        Delegate mDelegate;
    public:
        DelegateTask0(Delegate fn) : mDelegate(fn) { }
        void run() { mDelegate(); }
    };

    /**
     * Task that calls a function with 1 parameter
     */
    template<typename Param1, typename RetType> 
    class DelegateTask1 : public Task
    {
    public:
        typedef FastDelegate1<Param1, RetType> Delegate;
        typedef AsyncValue<RetType> Result;
    protected:
        Delegate    mDelegate;
        Param1      mParam1;
        Result      mResult;
    public:
        DelegateTask1(Param1 p1, Delegate fn) : mDelegate(fn), mParam1(p1) { }
        Result getResult() { return mResult; }
        void run() { mResult = mDelegate(mParam1); }
    };

    /**
     * Task that calls a function with 1 parameter and without a return value
     */
    template<typename Param1> 
    class DelegateTask1<Param1, void> : public Task
    {
    public:
        typedef FastDelegate1<Param1, void> Delegate;
    protected:
        Delegate    mDelegate;
        Param1      mParam1;
    public:
        DelegateTask1(Param1 p1, Delegate fn) : mDelegate(fn), mParam1(p1) { }
        void run() { mDelegate(mParam1); }
    };

    /**
     * Task that calls a function with 2 parameters
     */
    template<typename Param1, typename Param2, typename RetType> 
    class DelegateTask2 : public Task
    {
    public:
        typedef FastDelegate2<Param1, Param2, RetType> Delegate;
        typedef AsyncValue<RetType> Result;
    protected:
        Delegate    mDelegate;
        Param1      mParam1;
        Param2      mParam2;
        Result      mResult;
    public:
        DelegateTask2(Param1 p1, Param2 p2, Delegate fn) : mDelegate(fn), 
            mParam1(p1), mParam2(p2) { }
        Result getResult() { return mResult; }
        void run() { mResult = mDelegate(mParam1, mParam2); }
    };

    /**
     * Task that calls a function with 2 parameters and without a return value
     */
    template<typename Param1, typename Param2> 
    class DelegateTask2<Param1, Param2, void> : public Task
    {
    public:
        typedef FastDelegate2<Param1, Param2, void> Delegate;
    protected:
        Delegate    mDelegate;
        Param1      mParam1;
        Param2      mParam2;
    public:
        DelegateTask2(Param1 p1, Param2 p2, Delegate fn) : mDelegate(fn), 
            mParam1(p1), mParam2(p2) { }
        void run() { mDelegate(mParam1, mParam2); }
    };

    /**
     * Task that calls a function with 3 parameters
     */
    template<typename Param1, typename Param2, typename Param3,
        typename RetType> 
    class DelegateTask3 : public Task
    {
    public:
        typedef FastDelegate3<Param1, Param2, Param3,
            RetType> Delegate;
        typedef AsyncValue<RetType> Result;
    protected:
        Delegate    mDelegate;
        Param1      mParam1;
        Param2      mParam2;
        Param3      mParam3;
        Result      mResult;
    public:
        DelegateTask3(Param1 p1, Param2 p2, Param3 p3, Delegate fn) : 
            mDelegate(fn), mParam1(p1), mParam2(p2), mParam3(p3) { }
        Result getResult() { return mResult; }
        void run() { mResult = mDelegate(mParam1, mParam2, mParam3); }
    };

    /**
     * Task that calls a function with 3 parameters and without a return value
     */
    template<typename Param1, typename Param2, typename Param3> 
    class DelegateTask3<Param1, Param2, Param3, void> : public Task
    {
    public:
        typedef FastDelegate3<Param1, Param2, Param3, void> Delegate;
    protected:
        Delegate    mDelegate;
        Param1      mParam1;
        Param2      mParam2;
        Param3      mParam3;
    public:
        DelegateTask3(Param1 p1, Param2 p2, Param3 p3, Delegate fn) : 
          mDelegate(fn), mParam1(p1), mParam2(p2), mParam3(p3) { }
        void run() { mDelegate(mParam1, mParam2, mParam3); }
    };
}

#endif // __OGE_DELEGATETASK_H__
