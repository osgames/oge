/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_MEMORYMANAGER_H__
#define __OGE_MEMORYMANAGER_H__

// If you DONT want to check for memory leak set the value to 0
#if defined(_DEBUG) && OGE_COMPILER == OGE_COMPILER_MSVC
#    define OGE_MEMORY_MODE 0
#else
#    define OGE_MEMORY_MODE 0
#endif

// This must be added before ANY other system header file
#if OGE_MEMORY_MODE == 1
#    define _CRTDBG_MAP_ALLOC // Maps the base version of the heap functions to their debug versions.
#    include <crtdbg.h>
#endif

////>>> Now macros... should placed somewhere like in prerequisites.h
//-----------------------------------------------------------------------------
// If debug memory allocation turned on and your program has a memory leak,
// it will dump all leaked memory blocks when you close the program:
//
//        Dumping objects ->
//        {5641} normal block at 0x00BB30A0, 100 bytes long.
//        Data: <           > CD CD CD CD CD CD CD CD CD CD CD CD CD
//
// To find the exact location where this leak occurs, you can either call
// _CrtSetBreakAlloc(5641), where 5641 is the allocation index shown in
// brackets of the allocation dump.
// The easier way that particularly helpful with the memory blocks that are
// allocated in Your code is simply redefining new operator, in order for
// IDE to point to the lines of code which allocate the memory that causes
// the leak.
//-----------------------------------------------------------------------------
#if OGE_MEMORY_MODE == 1
#    if defined(__FILE__) && defined(__LINE__)
#        define OGE_DEBUG_NEW    new(_NORMAL_BLOCK, __FILE__, __LINE__)
// NOTE: A BIG problem occurs when you include anything after this code
//        that overloads operator new (global or per class). You are basically
//        left removing the following #define and adding it to each .cpp file
//        (exactly what MFC does). But 1)you might forget some cpp files,
//        2)classes that overload operator new, must provide the specific
//        format of this operator that includes filename and linenumber
//        parameters. You must also remove #define for 3rd party libs.
//#        define new                OGE_DEBUG_NEW    // So... THIS IS ONLY HERE TO BOOST YOUR KNOWLEDGE!
#    else
#        define OGE_DEBUG_NEW    new
#    endif
#    define oge_new                OGE_DEBUG_NEW
#    define oge_delete            delete

// Set the allocation behavior of the debug heap manager. Call this function
// at the beginning of your program and in ANY DLL that (re)allocates memory.
//
//   _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG); Get current flags.
//   _CRTDBG_ALLOC_MEM_DF                 Turn on debug memory allocation.
//   _CRTDBG_CHECK_ALWAYS_DF              Reports inconsistency in the base heap or debug header info,
//                                        or overwrite buffers, by inspecting each memory block at every
//                                        allocation and deallocation request. Although slows down
//                                        execution, it is useful for catching errors quickly.
//                                        Don't actually free memory blocks, assign them the _FREE_BLOCK
//                                        type, and fill them with the byte value '0xDD'.
//                                        It can be used to simulate low-memory conditions.
//   _CRTDBG_CHECK_CRT_DF                 Include internal CRT memory blocks in dump/difference operations.
//   _CRTDBG_LEAK_CHECK_DF;               Turn on automatic leak-checking at program exit.
//   _CrtSetDbgFlag(flag)                 Set flag to the new value.
//
#    define ogeInitMemoryCheck()                         \
        int flag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG); \
        flag |= _CRTDBG_ALLOC_MEM_DF                    \
        /*    |    _CRTDBG_CHECK_ALWAYS_DF   */            \
        /*    |    _CRTDBG_DELAY_FREE_MEM_DF */            \
            |    _CRTDBG_CHECK_CRT_DF                    \
            |    _CRTDBG_LEAK_CHECK_DF;                  \
        _CrtSetDbgFlag(flag);
#else
#    define oge_new                new
#    define oge_delete            delete
#    define ogeInitMemoryCheck()
#endif

#endif // __OGE_MEMORYMANAGER_H__
