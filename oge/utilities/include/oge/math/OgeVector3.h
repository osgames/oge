/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_VECTOR3_H__
#define __OGE_VECTOR3_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/math/OgeMath.h"
#include "oge/math/OgeAngle.h"
#include "oge/math/OgeQuaternion.h"

namespace oge 
{
    /** 
     * Vector3
     *
     * Vector3 tmp;
     * Vector3 tmp2;
     * tmp.x = 2.0f;
     * tmp.y = 4.0f;
     * tmp.z = 6.0f;
     *
     * memcpy( &tmp2.val, &tmp.val, 12 ); // Ok on win32
     * memcpy( &tmp2, &tmp, 12 );  // Ok
     * tmp2.x = tmp.val[0];  // Ok
     * tmp2.y = tmp.val[1];
     * tmp2.z = tmp.val[2];
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_UTIL_API Vector3
    {
    public:
        // http://www.ddj.com/cpp/184403821
        union {
            struct {
                Real x, y, z;
            }; // 12 bytes on windows
            Real val[3];
        };

        static const Vector3 ZERO;
        static const Vector3 UNIT_SCALE;
        static const Vector3 UNIT_X;
        static const Vector3 UNIT_Y;
        static const Vector3 UNIT_Z;
        static const Vector3 NEGATIVE_UNIT_X;
        static const Vector3 NEGATIVE_UNIT_Y;
        static const Vector3 NEGATIVE_UNIT_Z;

    public:
        // No default value to be as fast as possible
        inline Vector3()
        {
        }

        inline Vector3( Real X, Real Y, Real Z )
            : x ( X ), y( Y ), z( Z )
        {
        }

        inline Vector3(Real coord[3])
            : x( coord[0] ),
              y( coord[1] ),
              z( coord[2] )
        {
        }

        inline Vector3(int coord[3])
        {
            this->x = (Real) coord[0];
            this->y = (Real) coord[1];
            this->z = (Real) coord[2];
        }

        inline Vector3( const Real* const f )
            : x( f[0] ), y( f[1] ), z( f[2] )
        {
        }

        inline Vector3( const Vector3& vector)
            : x( vector.x ), y( vector.y ), z( vector.z )
        {
        }

        /// Array accessor operator
        inline Real operator[] (int i ) const
        {
            assert(i < 3);
            return *(&x+i);
        }

        /// Array accessor operator
        inline Real operator[] (size_t i ) const
        {
            assert(i < 3);
            return *(&x+i);
        }

        /// Array accessor operator
        inline Real& operator[] (int i )
        {
            assert(i < 3);
            return *(&x+i);
        }

        /// Pointer accessor
        inline Real* ptr()
        {
            return &x;
        }

        /// Pointer accessor
        inline const Real* ptr() const
        {
            return &x;
        }

        inline Vector3& operator = ( const Vector3& vector )
        {
            this->x = vector.x;
            this->y = vector.y;
            this->z = vector.z;
            return *this;
        }

        /** 
         * Used to convert from a Vector3 of another lib to oge::Vector3
         *
         * @note The vector type must have the public members x, y and z
         * @note The template can examine the parameter we give and work out the type automatically.
         * @see Template toVector<>
         */
        template <typename T>
        inline Vector3& operator = (const T &v)
        {
            this->x = v.x;
            this->y = v.y;
            this->z = v.z;
            return *this;
        }

        inline bool operator == ( const Vector3& vector ) const
        {
            return ( this->x == vector.x && this->y == vector.y && this->z == vector.z );
        }

        inline bool operator != ( const Vector3& vector ) const
        {
            return ( this->x != vector.x || this->y != vector.y || this->z != vector.z );
        }

        inline Vector3 operator + (const Vector3& vector ) const
        {
            Vector3 sum;
            sum.x = x + vector.x;
            sum.y = y + vector.y;
            sum.z = z + vector.z;
            return sum;
        }

        inline Vector3 operator - ( const Vector3& vector ) const
        {
            Vector3 diff;
            diff.x = x - vector.x;
            diff.y = y - vector.y;
            diff.z = z - vector.z;
            return diff;
        }

        inline Vector3 operator * ( Real scalar ) const
        {
            Vector3 prod;
            prod.x = scalar*this->x;
            prod.y = scalar*this->y;
            prod.z = scalar*this->z;
            return prod;
        }

        inline Vector3 operator * ( const Vector3& rhs) const
        {
            Vector3 prod;
            prod.x = rhs.x * this->x;
            prod.y = rhs.y * this->y;
            prod.z = rhs.z * this->z;
            return prod;
        }

        inline Vector3 operator / (Real scalar ) const
        {
            assert( scalar != 0.0 );
            Real inv = (Real) 1.0 / scalar;
            return Vector3( x * inv, y * inv, z * inv);
        }

        inline Vector3 operator / (const Vector3& vector) const
        {
            return Vector3( x/vector.x, y/vector.y, z/vector.z);
        }

        inline Vector3 operator - () const
        {
            return Vector3( -x, -y, -z);
        }

        inline friend Vector3 operator * (Real scalar, const Vector3& vector)
        {
            return Vector3(scalar * vector.x, scalar * vector.y, scalar * vector.z);
        }

        inline friend Vector3 operator / (Real scalar, const Vector3& vector)
        {
            return Vector3(scalar / vector.x, scalar / vector.y, scalar / vector.z);
        }

        inline friend Vector3 operator + (Real scalar, const Vector3& vector)
        {
            return Vector3(scalar + vector.x, scalar + vector.y, scalar + vector.z);
        }

        inline friend Vector3 operator + (const Vector3& vector, Real scalar)
        {
            return Vector3(scalar + vector.x, scalar + vector.y, scalar + vector.z);
        }

        inline friend Vector3 operator - (Real scalar, const Vector3& vector)
        {
            return Vector3(scalar - vector.x, scalar - vector.y, scalar - vector.z);
        }

        inline friend Vector3 operator - (const Vector3& vector, Real scalar)
        {
            return Vector3(vector.x - scalar, vector.y - scalar, vector.z - scalar);
        }

        inline Vector3& operator += ( const Vector3& vector )
        {
            this->x += vector.x;
            this->y += vector.y;
            this->z += vector.z;
            return *this;
        }

        inline Vector3& operator += ( const Real scalar )
        {
            this->x += scalar;
            this->y += scalar;
            this->z += scalar;
            return *this;
        }

        inline Vector3& operator -= ( const Vector3& vector )
        {
            this->x -= vector.x;
            this->y -= vector.y;
            this->z -= vector.z;
            return *this;
        }

        inline Vector3& operator -= ( const Real scalar )
        {
            this->x -= scalar;
            this->y -= scalar;
            this->z -= scalar;
            return *this;
        }

        inline Vector3& operator *= ( const Vector3& vector )
        {
            this->x *= vector.x;
            this->y *= vector.y;
            this->z *= vector.z;
            return *this;
        }

        inline Vector3& operator *= ( Real scalar )
        {
            this->x *= scalar;
            this->y *= scalar;
            this->z *= scalar;
            return *this;
        }

        inline Vector3& operator /= ( const Vector3& vector )
        {
            this->x /= vector.x;
            this->y /= vector.y;
            this->z /= vector.z;
            return *this;
        }

        inline Vector3& operator /= ( Real scalar )
        {
            assert( scalar != 0.0 );
            // NEXT useful? if (scalar == 0.0) scalar = Math::MIN_FLOAT;
            Real inv = (Real) 1.0 / scalar;
            this->x *= inv;
            this->y *= inv;
            this->z *= inv;
            return *this;
        }

        /** Return the square of the length (magnitude) of the vector
         */
        inline Real squaredLength() const
        {
            return x * x + y * y + z * z ;
        }

        /** 
         * Returns the length (magnitude) of the vector
         * @warning Expensive use fastLength()
         */
        inline Real length() const
        {
            return Math::Sqrt( x * x + y * y + z * z );
        }

		/**
		 * Returns a point between two vectors based on the given amt
		 */
		inline Vector3 lerp(const Vector3& vector, Real amt) {
			return *this + (vector - *this) * amt;
		}

        /** 
         * Returns the length (magnitude) of the vector
         * @note Based on oge::fastSqrt()
         */
        inline Real fastLength() const
        {
            return Math::fastSqrt( x * x + y * y + z * z );
        }

        /** Returns the distance to another vector
         * @warning Expensive as this requries a square root.
         *          consider using a fast method (squaredDistance for example)
         */
        inline Real distance( const Vector3& vector) const
        {
            return (*this - vector).length();
        }

        /** Returns the sqaure of the distance to another vector
         */
        inline Real squaredDistance( const Vector3& vector) const
        {
            return (*this - vector).squaredLength();
        }

        /** Returns the dot (scalar) product of this vector with another vector
         *
         * @detail Usually used to calculate the angle between 2 vectors.
                   If both are unit vectors, the dot product is the cosine
                   of the angle; otherwise the dot product must be divided
                   by the product of the lengths of both vectors to get
                   the cosine of the angle. This result can further be used
                   to calculate the distance of a point from a plane.
         */
        inline Real dotProduct( const Vector3& vector) const
        {
            return this->x * vector.x + this->y * vector.y + this->z * vector.z;
        }

        /** Normalises the vector so that the vector length (magnitude) is 1
         * @remarks The result is called an unit vector
         */
        inline Real normalise()
        {
            Real length = Math::Sqrt( x * x + y * y + z * z );

            if (length > 1e-08)
            {
                Real invLen = (Real) 1.0 / length;
                this->x *= invLen;
                this->y *= invLen;
                this->z *= invLen;
            }
            return length;
        }

        inline Vector3 normalisedCopy() const
        {
            Vector3 ret = *this;
            ret.normalise();
            return ret;
        }

        /** Returns the cross-product of 2 vectors, i.e. the vector
         *  that lies perpendicular to them both.
         */
        inline Vector3 crossProduct( const Vector3& vector ) const
        {
            return Vector3( y * vector.z - z * vector.y,
                            z * vector.x - x * vector.z,
                            x * vector.y - y * vector.x);
        }

        /** Returns the mid point of the vector
         */
        inline Vector3 midPoint( const Vector3& vector ) const
        {
            return Vector3( ( x + vector.x ) * (Real) 0.5,
                            ( y + vector.y ) * (Real) 0.5,
                            ( z + vector.z ) * (Real) 0.5 );
        }

        /** Returns true if the vector's scalar components are 
         *  all greater than the ones of the vector it is compared against.
         */
        inline bool operator < ( const Vector3& vector ) const
        {
            if ( x < vector.x && y < vector.y && z < vector.z )
                return true;
            return false;
        }

        /** Returns true if the vector's scalar components are 
         *  all smaller than the ones of the vector it is compared against.
         */
        inline bool operator > ( const Vector3& vector ) const
        {
            if ( x > vector.x && y > vector.y && z > vector.z )
                return true;
            return false;
        }

        inline void makeFloor( const Vector3& vector )
        {
            if( vector.x < x ) this->x = vector.x;
            if( vector.y < y ) this->y = vector.y;
            if( vector.z < z ) this->z = vector.z;
        }

        inline void makeCeil( const Vector3& vector )
        {
            if( vector.x > x ) this->x = vector.x;
            if( vector.y > y ) this->y = vector.y;
            if( vector.z > z ) this->z = vector.z;
        }

        inline Vector3 perpendicular() const
        {
            static const Real fSquareZero = (Real) (1e-06 * 1e-06);
            Vector3 perp = this->crossProduct( Vector3::UNIT_X );

            if( perp.squaredLength() < fSquareZero )
            {
                perp = this->crossProduct( Vector3::UNIT_Y );
            }

            return perp;
        }

        inline bool isZeroLength() const
        {
            Real sqlen = (x * x) + (y * y) + (z * z);
            return (sqlen < (1e-06 * 1e-06)); // NEXT Use const Real?
        }

        inline Vector3 reflect(const Vector3& vector)
        {
            return Vector3( *this - ( 2 * this->dotProduct(vector) * vector ) );
        }

        /** Returns whether this vector is within a tolerance of another vector.
         */
        inline bool isPositionEqualTo(const Vector3& vector, Real tolerance = 1e-04) const
        {
            return Math::areEqual(x, vector.x, tolerance) &&
                   Math::areEqual(y, vector.y, tolerance) &&
                   Math::areEqual(z, vector.z, tolerance);
        }

        /**
         * Returns whether this vector is within a tolerance of another vector,
         * take also scale of the vectors into account.
         */
        inline bool isPositionCloseTo(const Vector3& vector, Real tolerance = 1e-04) const
        {
            return squaredDistance(vector) <= 
                   (squaredLength() + vector.squaredLength()) * tolerance;
        }

        /**
         * Returns whether this vector direction is within a tolerance of another vector.
         * @note Both vectors should be normalised.
         */
        inline bool isDirectionEqualTo(const Vector3& vector, const Radian& tolerance) const
        {
            Real dot = dotProduct(vector);
            Radian angle = Math::ACos(dot);
            return Math::Abs(angle.inRadians()) <= tolerance.inRadians();
        }

        /** 
         * Gets the shortest arc quaternion to rotate this vector 
         * to the destination vector.
         *
         * @note Based on Stan Melax's article in Game Programming Gems
         *
         * @note If you call this with a destination vector that is 
         *       close to the inverse of this vector, we will rotate 
         *       180 degrees around the 'fallbackAxis' (if specified,
         *       or a generated axis if not) since in this case 
         *       ANY axis of rotation is valid.
         */
        Quaternion getRotationTo(const Vector3& dest,
            const Vector3& fallbackAxis = Vector3::ZERO) const
        {
            Quaternion q;
            // Copy, since cannot modify local
            Vector3 v0 = *this;
            Vector3 v1 = dest;
            v0.normalise();
            v1.normalise();

            Real d = v0.dotProduct(v1);
            // If dot == 1, vectors are the same
            if (d >= 1.0f)
            {
                return Quaternion::IDENTITY;
            }
            if (d < (1e-6f - 1.0f))
            {
                if (fallbackAxis != Vector3::ZERO)
                {
                    // rotate 180 degrees about the fallback axis
                    q.fromAngleAxis(Radian(Math::PI), fallbackAxis);
                }
                else
                {
                    // Generate an axis
                    Vector3 axis = Vector3::UNIT_X.crossProduct(*this);
                    if (axis.isZeroLength()) // pick another if colinear
                    axis = Vector3::UNIT_Y.crossProduct(*this);
                    axis.normalise();
                    q.fromAngleAxis(Radian(Math::PI), axis);
                }
            }
            else
            {
                Real s = Math::Sqrt( (1+d)*2 );
                Real invs = 1 / s;

                Vector3 c = v0.crossProduct(v1);

                q.x = c.x * invs;
                q.y = c.y * invs;
                q.z = c.z * invs;
                q.w = s * (Real) 0.5;
                q.norm();
            }
            return q;
        }

        /** Writing to a stream.
        */
        inline friend std::ostream& operator << ( std::ostream& o, const Vector3& vector )
        {
            o << "(" << vector.x << ", " << vector.y << ", " << vector.z << ")";
            return o;
        }
    };

    /** 
     * Converter between two vector i.e. Ogre::Vector3 to Oge::Vector3
     *
     * This template is used to convert from oge::Vector3 to Vector3
     * of another library
     *
     * @note The vector type must have the public members x, y and z
     * @usage NxVec3 nv; Ogre::Vector3 ov = convertVector3<Ogre::Vector3>( nv ); 
     * @link Thanks to Kojack http://www.ogre3d.org/phpBB2/viewtopic.php?t=38999
     */
    template <typename ToType, typename FromType>
    ToType toVector3(const FromType& v)
    {
        return ToType( v.x, v.y, v.z );
    }

}
#endif // __OGE_VECTOR3_H__
