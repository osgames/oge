/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_RADIAN_H__
#define __OGE_RADIAN_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/math/OgeMath.h"

/* Note that both Degree and Radian are defined here
 * this permits us to include only one file and make it easier to develop.
 * @next Should those methods be inlined?
 */
namespace oge 
{
    /** 
     * Class indicating an angle in Radians.
     *
     * @note Conversions between Radian and Degree are done automatically.
     *
     * @note that some are implemented in the OgeMath.h file
     *      because of circular dependencies
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_UTIL_API Radian
    {
        Real mRadian;

    public:
        explicit Radian( Real radian = 0 ) : mRadian(radian) {}
        Radian( const Degree& degree ); // see below
    
        Radian& operator = ( const Real& real ) { mRadian = real; return *this; }
        Radian& operator = ( const Radian& radian ) { mRadian = radian.mRadian; return *this; }
        Radian& operator = ( const Degree& degree ); // see below

        Real inDegrees() const; // see below
        Real inRadians() const { return mRadian; }
        
        const Radian& operator + () const { return *this; }
        Radian operator - () const { return Radian( -mRadian ); }

        Radian operator + ( const Radian& radian ) const { return Radian( mRadian + radian.mRadian ); }
        Radian operator + ( const Degree& degree ) const; // see below

        Radian operator - ( const Radian& radian ) const { return Radian( mRadian - radian.mRadian ); }
        Radian operator - ( const Degree& degree ) const; // see below

        Radian& operator += ( const Radian& radian ) { mRadian += radian.mRadian; return *this; }
        Radian& operator += ( const Degree& degree ); // see below

        Radian& operator -= ( const Radian& radian ) { mRadian -= radian.mRadian; return *this; }
        Radian& operator -= ( const Degree& degree ); // see below

        Radian operator * ( Real real ) const { return Radian ( mRadian * real ); }
        Radian operator * ( const Radian& radian ) const { return Radian ( mRadian * radian.mRadian ); }

        Radian& operator *= ( Real real ) { mRadian *= real; return *this; }
        Radian operator / ( Real real ) const { return Radian ( mRadian / real ); }
        Radian& operator /= ( Real real ) { mRadian /= real; return *this; }

        bool operator <  ( const Radian& radian ) const { return mRadian <  radian.mRadian; }
        bool operator <= ( const Radian& radian ) const { return mRadian <= radian.mRadian; }
        bool operator == ( const Radian& radian ) const { return mRadian == radian.mRadian; }
        bool operator != ( const Radian& radian ) const { return mRadian != radian.mRadian; }
        bool operator >= ( const Radian& radian ) const { return mRadian >= radian.mRadian; }
        bool operator >  ( const Radian& radian ) const { return mRadian >  radian.mRadian; }
        

    };

    /** 
     * Class indicating an angle in Degrees.
     *
     * @note Conversions between Radian and Degree are done automatically.
     *
     * @note that some are implemented in the OgeMath.h file
     *      because of circular dependencies
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_UTIL_API Degree
    {
        Real mDegree;

    public:
        explicit Degree( Real degree = 0 ) : mDegree(degree) {}
        Degree( const Radian& radian ) : mDegree(radian.inDegrees()) {}
    
        Degree& operator = ( const Real& real ) { mDegree = real; return *this; }
        Degree& operator = ( const Degree& degree ) { mDegree = degree.mDegree; return *this; }
        Degree& operator = ( const Radian& radian ) { mDegree = radian.inDegrees(); return *this; }

        Real inDegrees() const { return mDegree; }
        Real inRadians() const; // see below

        const Degree& operator + () const { return *this; }
        Degree operator - () const { return Degree(-mDegree); }

        Degree operator + ( const Degree& degree ) const { return Degree ( mDegree + degree.mDegree ); }
        Degree operator + ( const Radian& radian ) const { return Degree ( mDegree + radian.inDegrees() ); }

        Degree operator - ( const Degree& degree ) const { return Degree ( mDegree - degree.mDegree ); }
        Degree operator - ( const Radian& radian ) const { return Degree ( mDegree - radian.inDegrees() ); }

        Degree& operator += ( const Degree& degree ) { mDegree += degree.mDegree; return *this; }
        Degree& operator += ( const Radian& radian ) { mDegree += radian.inDegrees(); return *this; }

        Degree& operator -= ( const Degree& degree ) { mDegree -= degree.mDegree; return *this; }
        Degree& operator -= ( const Radian& radian ) { mDegree -= radian.inDegrees(); return *this; }

        Degree operator * ( Real real ) const { return Degree ( mDegree * real ); }
        Degree operator * ( const Degree& degree ) const { return Degree ( mDegree * degree.mDegree ); }
        Degree& operator *= ( Real real ) { mDegree *= real; return *this; }
        Degree operator / ( Real real ) const { return Degree ( mDegree / real ); }
        Degree& operator /= ( Real real ) { mDegree /= real; return *this; }

        bool operator <  ( const Degree& degree ) const { return mDegree <  degree.mDegree; }
        bool operator <= ( const Degree& degree ) const { return mDegree <= degree.mDegree; }
        bool operator == ( const Degree& degree ) const { return mDegree == degree.mDegree; }
        bool operator != ( const Degree& degree ) const { return mDegree != degree.mDegree; }
        bool operator >= ( const Degree& degree ) const { return mDegree >= degree.mDegree; }
        bool operator >  ( const Degree& degree ) const { return mDegree >  degree.mDegree; }
    };
}
#endif // __OGE_RADIAN_H__
