/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_MATH_H__
#define __OGE_MATH_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/math/OgeAngle.h"
#include <math.h>

namespace oge
{
    class OGE_UTIL_API Math
    {
    public:
        static const Real PI;
        static const Real TWO_PI;
        static const Real HALF_PI;

        /// Square of 1/2
        static const Real SQRT_05;
        /// Used by OgeAngle.h
        static const Real DEGREE2RADIAN;
        static const Real RADIAN2DEGREE;
        
        /** Compare two reals within a tolerance
        */
        static bool areEqual(Real a, Real b,
            Real tolerance = std::numeric_limits<Real>::epsilon());

		static inline Real Sqr (Real value) { return value*value; }

        // Note: The upper case seems to be needed with gcc/mingw
        static inline Real Sqrt(Real value) { return Real(std::sqrt(value));}
        /** 
         * Fast Square Root (aka sqrt())
         * See invSqrt for references
         */
        static inline Real fastSqrt( Real x ) { return ((Real) 1.0f) / invSqrt( x ); }
        /**
         * Fast Inverse Square Root (aka 1.0/sqrt(r))
         * @link http://www.mceniry.net/papers/Fast%20Inverse%20Square%20Root.pdf
         * @link http://www.ogre3d.org/phpBB2/viewtopic.php?t=37343
         */
        static Real invSqrt( Real x );

        static inline Real Rand() { return Real (std::rand()); }

		static inline Real Clamp(Real value, Real min, Real max) { return std::min(max, std::max(min, value)); }

        // NEXT Use tables see Ogre::Math::Sin
        static inline Real Sin( Real angle ) { return Real (std::sin(angle)); }
        static inline Real Cos( Real angle ) { return Real (std::cos(angle)); }

        static inline Real Abs( Real angle ) { return Real (std::fabs(angle)); }

        static inline Real Sin (const Radian& angle ) { return Real(std::sin(angle.inRadians())); }
        static inline Real Cos (const Radian& angle ) { return Real(std::cos(angle.inRadians())); }

        static inline Real DegreesToRadians(Real degrees) { return degrees * DEGREE2RADIAN; }
        static inline Real RadiansToDegrees(Real radians) { return radians * RADIAN2DEGREE; }

        static Radian ACos( Real angle );
        static Radian ASin( Real angle );
        static Radian ATan(Real angle);
        static Radian ATan2(Real angle1, Real angle2);

        /// @see round(Real, unsigned int)
        static inline Real round(Real value)
            { return  (value >= 0.0) ? (Real) std::floor(value + 0.5): (Real) std::ceil(value - 0.5); }

        /** 
         * Round a real value to the specified number of digits, default 0(whole number)
         * @note Use round(Real) if you are not interested in rounding digits
         */
        static Real round(Real value, unsigned int digits);
    };

    // Some Radian implementations
    // Note: We need to implement them here because they dependent on some Math stuff
    //       or we couldn't inline Radian::inDegrees()
    inline Radian::Radian ( const Degree& degree ) : mRadian(degree.inRadians())
    { 
    }

    inline Radian& Radian::operator = ( const Degree& degree )
    {
        mRadian = degree.inRadians(); return *this;
    }

    inline Real Radian::inDegrees() const
    {
        return Math::RadiansToDegrees( mRadian );
    }

    inline Radian Radian::operator + ( const Degree& degree ) const
    {
        return Radian ( mRadian + degree.inRadians() );
    }

    inline Radian Radian::operator - ( const Degree& degree ) const
    {
        return Radian ( mRadian - degree.inRadians() );
    }

    inline Radian& Radian::operator += ( const Degree& degree )
    {
        mRadian += degree.inRadians();
        return *this;
    }

    inline Radian& Radian::operator -= ( const Degree& degree )
    {
        mRadian -= degree.inRadians();
        return *this;
    }

    inline Radian operator * ( Real real, const Radian& radian )
    {
        return Radian ( real * radian.inRadians() );
    }

    inline Radian operator / ( Real real, const Radian& radian )
    {
        return Radian ( real / radian.inRadians() );
    }

    // Some Degree implementations
    inline Real Degree::inRadians() const
    {
        return Math::DegreesToRadians( mDegree );
    }

    inline Degree operator * ( Real real, const Degree& degree )
    {
        return Degree ( real * degree.inDegrees() );
    }

    inline Degree operator / ( Real real, const Degree& degree )
    {
        return Degree ( real / degree.inDegrees() );
    }
}

#endif // __OGE_MATH_H__
