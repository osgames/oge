/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_QUATERNION_H__
#define __OGE_QUATERNION_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/math/OgeMath.h"

namespace oge 
{
    /** 
     * Quaternion
     *
     * TODO Format correctly
     * w 	 x 	 y 	 z 	 Description
     * 1 	0 	0 	0 	Identity quaternion, no rotation
     * 0 	1 	0 	0 	180' turn around X axis
     * 0 	0 	1 	0 	180' turn around Y axis
     * 0 	0 	0 	1 	180' turn around Z axis
     * sqrt(0.5) 	sqrt(0.5) 	0 	0 	90' rotation around X axis
     * sqrt(0.5) 	0 	sqrt(0.5) 	0 	90' rotation around Y axis
     * sqrt(0.5) 	0 	0 	sqrt(0.5) 	90' rotation around Z axis
     * sqrt(0.5) 	-sqrt(0.5) 	0 	0 	-90' rotation around X axis
     * sqrt(0.5) 	0 	-sqrt(0.5) 	0 	-90' rotation around Y axis
     * sqrt(0.5) 	0 	0 	-sqrt(0.5) 	-90' rotation around Z axis 

     * @author Steven 'lazalong' Gay
     */
    class OGE_UTIL_API Quaternion
    {
    public:
        union {
            struct {
                Real w, x, y, z;
            };   // 16 bytes on windows
            Real val[4];
        };

        // cutoff for sine near zero
        static const Real ms_fEpsilon;
        static const Quaternion ZERO;
        static const Quaternion IDENTITY;

        inline Quaternion( Real W = 1.0, Real X = 0.0, Real Y = 0.0, Real Z = 0.0 )
            : w ( W ), x ( X ), y ( Y ), z ( Z )
        {
        }

        inline Quaternion( int coord[4] )
        {
            this->w = (Real) coord[0];
            this->x = (Real) coord[1];
            this->y = (Real) coord[2];
            this->z = (Real) coord[3];
        }

        inline Quaternion( const Real* const f )
            : w( f[0] ), x( f[1] ), y( f[2] ), z( f[3] )
        {
        }

        inline Quaternion( const Quaternion& q )
            : w( q.w ), x( q.x ), y( q.y ), z( q.z )
        {
        }

        /* Construct a quaternion from 4 values
         * @note This is similar to Quaternion( Real values[4] )
         */
        inline Quaternion(Real* values)
        {
            memcpy(&w, values, sizeof(Real)*4);
        }

        /// Array accessor operator
        inline Real operator [] ( const size_t i ) const
        {
            assert( i < 4 );
            return *(&w+i);
        }

        /// Array accessor operator
        inline Real& operator [] ( const size_t i )
        {
            assert( i < 4 );
            return *(&w+i);
        }

        /// Pointer accessor
        inline Real* ptr()
        {
            return &w;
        }

        /// Pointer accessor
        inline const Real* ptr() const
        {
            return &w;
        }

        /** 
         * Used to convert from a Quaternion of another lib to oge::Quaternion
         *
         * @note The quaternion type must have the public members w, x, y and z
         * @note The template can examine the parameter we give and work out the type automatically.
         * @see Template toQuaternion<>
         */
        template <typename T>
        inline Quaternion& operator = (const T &q)
        {
            this->w = q.w;
            this->x = q.x;
            this->y = q.y;
            this->z = q.z;
            return *this;
        }

        inline Quaternion& operator = (const Quaternion& q)
        {
            this->w = q.w;
            this->x = q.x;
            this->y = q.y;
            this->z = q.z;
            return *this;
        }

        inline bool operator == (const Quaternion& q) const
        {
            return (q.w == w) && (q.x == x) && (q.y == y) && (q.z == z);
        }

        inline bool operator != (const Quaternion& q) const
        {
            return !operator==(q);
        }

        inline friend Quaternion operator * (Real scalar, const Quaternion& q )
        {
            return Quaternion( scalar * q.w,
                               scalar * q.x,
                               scalar * q.y,
                               scalar * q.z);
        }

        inline Quaternion operator - () const
        {
            return Quaternion(-w, -x, -y, -z);
        }

        inline Real dot(const Quaternion& q) const
        {
            return w*q.w + x*q.x + y*q.y + z*q.z;
        }

        inline Real norm() const
        {
            return w*w + x*x + y*y + z*z;
        }

        Quaternion inverse() const
        {
            Real norm = w*w + x*x + y*y + z*z;
            if ( norm > 0.0 )
            {
                Real invNorm = (Real)1.0/norm;
                return Quaternion(w*invNorm,-x*invNorm,-y*invNorm,-z*invNorm);
            }
            else
            {
                // this is an error flag!
                return ZERO;
            }
        }

        inline Quaternion unitInverse() const
        {
            return Quaternion(w,-x,-y,-z);
        }

        /** Function for writing to a stream.
        */
        inline friend std::ostream& operator << ( std::ostream& o, const Quaternion& q )
        {
            o << "(" << q.w << ", " << q.x << ", " << q.y << ", " << q.z << ")";
            return o;
        }
        
        Quaternion operator+ (const Quaternion& q) const;
        
        Quaternion operator- (const Quaternion& q) const;

        /**
         * @note Not commutative (p*q != q*p)
         */
        Quaternion operator* (const Quaternion& q) const;

        /**
         * Rotation of a vector by a quaternion
         * @note nVidia SDK implementation
         */
        Vector3 operator* (const Vector3& v) const;

        /**
         * Normalises this quaternion, and returns the previous length
         */
        Real normalise(void); 

        /**
         * Quaternion constructed from an axis and a rotation around
         *
         * @note The axis[] must be unit length
         */
        void fromAngleAxis(const Radian& angle, const Vector3& axis);

	    // spherical linear interpolation
        static Quaternion Slerp (Real fT, const Quaternion& rkP,
            const Quaternion& rkQ, bool shortestPath = false);

        /// Get the local x-axis
        Vector3 xAxis(void) const;
        /// Get the local y-axis
        Vector3 yAxis(void) const;
        /// Get the local z-axis
        Vector3 zAxis(void) const;
    };

    /** 
     * Converter between two quaternions i.e. Ogre::Quaternion to Oge::Quaternion
     *
     * This template is used to convert from oge::Quaternion to Quaternion
     * of another library
     *
     * @note The quaternion type must have the public members w, x, y and z
     * @usage NxVec3 nv; Ogre::Quaternion ov = convertVector3<Ogre::Quaternion>( nv ); 
     * @note Thanks to Kojack http://www.ogre3d.org/phpBB2/viewtopic.php?t=38999
     */
    template <typename ToType, typename FromType>
    ToType toQuaternion(const FromType& v)
    {
        return ToType( v.w, v.x, v.y, v.z );
    }

}
#endif // __OGE_QUATERNION_H__
