/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_MAP_H__
#define __OGE_MAP_H__

#include "oge/OgeUtilitiesPrerequisites.h"

namespace oge
{
    /**
     * MapBase takes a map, key and value types to form a class
     * that can be used by other classes providing functionality.
     *
     * @author Christopher Jones
     */
    template<class T, typename U, typename V>
    class MapBase
    {

    };
    /**
     * Partial specialisation for using std::map with non pointer types
     * @author Christopher Jones
     */
    template<typename U, typename V>
    class MapBase<std::map<U, V>, U, V>
    {
    public:
        typedef U KeyType;
        typedef V ValueType;
        typedef std::map<U, V> MapType;
        typedef typename MapType::iterator Iterator;
        typedef typename MapType::const_iterator ConstIterator;
        MapBase(){}
        MapBase(int initialize) : mMap(initialize){}
    protected:
        MapType mMap;
    };
    /**
     * Partial specialisation for using std::map with values as pointers.
     * @author Christopher Jones
     */
    template<typename U, typename V>
    class MapBase<std::map<U, V*>, U, V*>
    {
    public:
        typedef U KeyType;
        typedef V ValueType;
        typedef std::map<U, V*> MapType;
        typedef typename MapType::iterator Iterator;
        typedef typename MapType::const_iterator ConstIterator;
        MapBase(){}
        MapBase(int initialize) : mMap(initialize){}
    protected:
        MapType mMap;
    };
    /**
     * Partial specialisation for using OgeHashMap with
     * non pointer values.
     * @author Christopher Jones
     */
    template<typename U, typename V>
    class MapBase<OgeHashMap<U, V>, U, V>
    {
    public:
        typedef U KeyType;
        typedef V ValueType;
        typedef OgeHashMap<U, V> MapType;
        typedef typename MapType::iterator Iterator;
        typedef typename MapType::const_iterator ConstIterator;
        MapBase(){}
        MapBase(int initialize) : mMap(initialize){}
    protected:
        MapType mMap;
    };
    /**
     * Partial specialisation for using OgeHashMap with
     * pointer values.
     * @author Christopher Jones
     */
    template<typename U, typename V>
    class MapBase<OgeHashMap<U, V*>, U, V*>
    {
    public:
        typedef U KeyType;
        typedef V ValueType;
        typedef OgeHashMap<U, V*> MapType;
        typedef typename MapType::iterator Iterator;
        typedef typename MapType::const_iterator ConstIterator;
        MapBase(){}
        MapBase(int initialize) : mMap(initialize){}
    protected:
        MapType mMap;
    };

    /**
     * Provides map functions independant of the type of map being used.
     * Used with values that arent pointers.
     * @author Christopher Jones
     */
    template<class T, typename U, typename V>
    class Map : public MapBase<T, U, V>
    {
    public:
        typedef typename MapBase<T, U, V>::KeyType       KeyType;
        typedef typename MapBase<T, U, V>::ValueType     ValueType;
        typedef typename MapBase<T, U, V>::Iterator      Iterator;
        typedef typename MapBase<T, U, V>::ConstIterator ConstIterator;
        Map() {}
        Map(int initialSize) : MapBase<T, U, V>(initialSize) {}

        ValueType& operator [] (const KeyType& key) { return this->mMap[key]; }
        const ValueType& operator [] (const KeyType& key) const { return this->mMap[key]; }
        Map& operator = (const Map& rhs)
        {
            if (this == &rhs)
                return *this;

            this->mMap.clear();
            this->mMap = rhs.mMap;

            return *this;
        }
        bool operator == (const Map& rhs) { return (this->mMap == rhs.mMap); }

        bool addEntry(const KeyType& key, ValueType& value)
        {
            if (this->mMap.find(key) != this->mMap.end())
                return false;

            this->mMap[key] = value;
            return true;
        }
        void setEntry(const KeyType& key, ValueType& value)
        {
            this->mMap[key] = value;
        }
        bool replaceEntry(const KeyType& key, ValueType& value)
        {
            Iterator iter = this->mMap.find(key);
            if (iter != this->mMap.end())
                return false;

            iter->second = value;
            return true;
        }
        ValueType getEntry(const KeyType& key)
        {
            Iterator iter = this->mMap.find(key);
            if (iter == this->mMap.end())
                return ValueType();

            return iter->second;
        }
        const ValueType getEntry(const KeyType& key) const
        {
            ConstIterator iter = this->mMap.find(key);
            if (iter == this->mMap.end())
                return ValueType();

            return iter->second;
        }
        bool getEntry(const KeyType& key, ValueType& retrievedValue)
        {
            Iterator iter = this->mMap.find(key);
            if (iter == this->mMap.end())
            {
                retrievedValue = ValueType();
                return false;
            }

            retrievedValue = iter->second;
            return true;
        }
        bool entryExists(const KeyType& key)
        {
            return (this->mMap.find(key) != this->mMap.end());
        }
        void removeEntry(const KeyType& key)
        {
            Iterator iter = this->mMap.find(key);
            if (iter != this->mMap.end())
                this->mMap.erase(iter);
        }
        void removeEntry(Iterator iter)
        {
            this->mMap.erase(iter);
        }
        void deleteEntry(const KeyType& key)
        {
            Iterator iter = this->mMap.find(key);
            if (iter != this->mMap.end())
                this->mMap.erase(iter);
        }
        void deleteEntry(Iterator iter)
        {
            this->mMap.erase(iter);
        }
        void removeAll() { this->mMap.clear(); }
        void deleteAll() { this->mMap.clear(); }
        Iterator begin() { return this->mMap.begin(); }
        Iterator end() { return this->mMap.end(); }
        Iterator find(const KeyType& key)
        {
            return this->mMap.find(key);
        }
    };

    /**
     * Provides map functions independant of the type of map being used.
     * Used with values that are pointers.
     * @author Christopher Jones
     */
    template<class T, typename U, typename V>
    class Map<T, U, V*> : public MapBase<T, U, V*>
    {
    public:
        typedef typename MapBase<T, U, V*>::KeyType       KeyType;
        typedef typename MapBase<T, U, V*>::ValueType     ValueType;
        typedef typename MapBase<T, U, V*>::Iterator      Iterator;
        typedef typename MapBase<T, U, V*>::ConstIterator ConstIterator;
        Map() {}
        Map(int initialSize) : MapBase<T, U, V*>(initialSize) {}
        ValueType* operator [] (const KeyType& key) { return this->mMap[key]; }
        const ValueType* operator [] (const KeyType& key) const { return this->mMap[key]; }
        Map& operator = (const Map<T, U, V*>& rhs)
        {
            if (this == &rhs)
                return *this;

            this->mMap.clear();
            this->mMap = rhs.mMap;

            return *this;
        }
        bool operator == (const Map<T, U, V*>& rhs) { return (this->mMap == rhs.mMap); }

        bool addEntry(const KeyType& key, ValueType* value)
        {
            if (this->mMap.find(key) != this->mMap.end())
                return false;

            this->mMap[key] = value;
            return true;
        }
        void setEntry(const KeyType& key, ValueType* value)
        {
            this->mMap[key] = value;
        }
        bool replaceEntry(const KeyType& key, ValueType* value)
        {
            Iterator iter = this->mMap.find(key);
            if (iter != this->mMap.end())
                return false;

            iter->second = value;
            return true;
        }
        ValueType* getEntry(const KeyType& key)
        {
            Iterator iter = this->mMap.find(key);
            if (iter == this->mMap.end())
                return 0;

            return iter->second;
        }
        const ValueType* getEntry(const KeyType& key) const
        {
            ConstIterator iter = this->mMap.find(key);
            if (iter == this->mMap.end())
                return 0;

            return iter->second;
        }
        bool getEntry(const KeyType& key, ValueType*& retrievedValue)
        {
            Iterator iter = this->mMap.find(key);
            if (iter == this->mMap.end())
            {
                retrievedValue = 0;
                return false;
            }

            retrievedValue = iter->second;
            return true;
        }
        bool entryExists(const KeyType& key)
        {
            return (this->mMap.find(key) != this->mMap.end());
        }
        void removeEntry(const KeyType& key)
        {
            Iterator iter = this->mMap.find(key);
            if (iter != this->mMap.end())
                this->mMap.erase(iter);
        }
        void removeEntry(Iterator iter)
        {
            this->mMap.erase(iter);
        }
        void deleteEntry(const KeyType& key)
        {
            Iterator iter = this->mMap.find(key);
            if (iter != this->mMap.end())
            {
                delete iter->second;
                this->mMap.erase(iter);
            }
        }
        void deleteEntry(Iterator iter)
        {
            delete iter->second;
            this->mMap.erase(iter);
        }
        void removeAll() { this->mMap.clear(); }
        void deleteAll() 
        { 
            for (Iterator iter = this->mMap.begin(); iter != this->mMap.end(); ++iter)
            {
                ValueType* value = iter->second;
                if (value)
                    delete value;
            }

            this->mMap.clear(); 
        }
        Iterator begin() { return this->mMap.begin(); }
        Iterator end() { return this->mMap.end(); }
        Iterator find(const KeyType& key)
        {
            return this->mMap.find(key);
        }
    };

    /**
     * Provides thread safe map functions independant of the type of 
     * map being used. Used with values that arent pointers.
     * @author Christopher Jones
     */
    template<class T, typename U, typename V>
    class MapMT : public MapBase<T, U, V>
    {
    protected:
        Mutex mMutex;
    public:
        typedef typename MapBase<T, U, V>::KeyType   KeyType;
        typedef typename MapBase<T, U, V>::ValueType ValueType;
        typedef typename MapBase<T, U, V>::Iterator  Iterator;

        MapMT() {}
        MapMT(int initialSize) : MapBase<T, U, V>(initialSize) {}

        MapMT& operator = (const MapMT& rhs)
        {
            if (this == &rhs)
                return *this;

            {
                Mutex::ScopedLock lock(mMutex);
                this->mMap.clear();

                Mutex::ScopedLock rhsLock(rhs.mMutex);
                this->mMap = rhs.mMap;
            }

            return *this;
        }
        bool operator == (const MapMT& rhs) 
        { 
            Mutex::ScopedLock lock(mMutex);
            Mutex::ScopedLock rhsLock(rhs.mMutex);
            return (this->mMap == rhs.mMap); 
        }

        bool addEntry(const KeyType& key, ValueType& value)
        {
            Mutex::ScopedLock lock(mMutex);

            if (this->mMap.find(key) != this->mMap.end())
                return false;

            this->mMap[key] = value;
            return true;
        }
        void setEntry(const KeyType& key, ValueType& value)
        {
            Mutex::ScopedLock lock(mMutex);
            this->mMap[key] = value;
        }
        bool replaceEntry(const KeyType& key, ValueType& value)
        {
            Mutex::ScopedLock lock(mMutex);

            Iterator iter = this->mMap.find(key);
            if (iter != this->mMap.end())
                return false;

            iter->second = value;
            return true;
        }
        ValueType getEntry(const KeyType& key)
        {
            Mutex::ScopedLock lock(mMutex);
            Iterator iter = this->mMap.find(key);
            if (iter == this->mMap.end())
                return ValueType();

            return iter->second;
        }
        bool getEntry(const KeyType& key, ValueType& retrievedValue)
        {
            Mutex::ScopedLock lock(mMutex);

            Iterator iter = this->mMap.find(key);
            if (iter == this->mMap.end())
            {
                retrievedValue = ValueType();
                return false;
            }

            retrievedValue = iter->second;
            return true;
        }
        bool entryExists(const KeyType& key)
        {
            Mutex::ScopedLock lock(mMutex);
            return (this->mMap.find(key) != this->mMap.end());
        }
        void removeEntry(const KeyType& key)
        {
            Mutex::ScopedLock lock(mMutex);
            Iterator iter = this->mMap.find(key);
            if (iter != this->mMap.end())
                this->mMap.erase(iter);
        }
        void removeEntry(Iterator iter)
        {
            Mutex::ScopedLock lock(mMutex);
            this->mMap.erase(iter);
        }
        void deleteEntry(const KeyType& key)
        {
            Mutex::ScopedLock lock(mMutex);
            Iterator iter = this->mMap.find(key);
            if (iter != this->mMap.end())
                this->mMap.erase(iter);
        }
        void deleteEntry(Iterator iter)
        {
            Mutex::ScopedLock lock(mMutex);
            this->mMap.erase(iter);
        }
        void removeAll() { Mutex::ScopedLock lock(mMutex); this->mMap.clear(); }
        void deleteAll() { Mutex::ScopedLock lock(mMutex); this->mMap.clear(); }
        Iterator begin() 
        {
            Mutex::ScopedLock lock(mMutex); 
            return this->mMap.begin(); 
        }
        Iterator end() 
        {
            Mutex::ScopedLock lock(mMutex);
            return this->mMap.end(); 
        }
        Iterator find(const KeyType& key)
        {
            Mutex::ScopedLock lock(mMutex);
            return this->mMap.find(key);
        }
    };

    /**
     * Provides thread safe map functions independant of the type 
     * of map being used. Used with values that are pointers.
     * @author Christopher Jones
     */
    template<class T, typename U, typename V>
    class MapMT<T, U, V*> : public MapBase<T, U, V*>
    {
    protected:
        Mutex mMutex;
    public:
        typedef typename MapBase<T, U, V*>::KeyType   KeyType;
        typedef typename MapBase<T, U, V*>::ValueType ValueType;
        typedef typename MapBase<T, U, V*>::Iterator  Iterator;
        MapMT() {}
        MapMT(int initialSize) : MapBase<T, U, V>(initialSize) {}

        MapMT& operator = (const MapMT& rhs)
        {
            if (this == &rhs)
                return *this;

            Mutex::ScopedLock lock(mMutex);

            this->mMap.clear();
            Mutex::ScopedLock rhsLock(rhs.mMutex);
            this->mMap = rhs.mMap;

            return *this;
        }
        bool operator == (const Map<T, U, V*>& rhs) 
        {
            Mutex::ScopedLock lock(mMutex);
            return (this->mMap == rhs.mMap);
        }

        bool addEntry(const KeyType& key, ValueType* value)
        {
            Mutex::ScopedLock lock(mMutex);

            if (this->mMap.find(key) != this->mMap.end())
                return false;

            this->mMap[key] = value;
            return true;
        }
        void setEntry(const KeyType& key, ValueType* value)
        {
            Mutex::ScopedLock lock(mMutex);
            this->mMap[key] = value;
        }
        bool replaceEntry(const KeyType& key, ValueType* value)
        {
            Iterator iter = this->mMap.find(key);
            if (iter != this->mMap.end())
                return false;

            iter->second = value;
            return true;
        }
        ValueType* getEntry(const KeyType& key)
        {
            Mutex::ScopedLock lock(mMutex);
            Iterator iter = this->mMap.find(key);
            if (iter == this->mMap.end())
                return 0;

            return iter->second;
        }
        bool getEntry(const KeyType& key, ValueType*& retrievedValue)
        {
            Mutex::ScopedLock lock(mMutex);

            Iterator iter = this->mMap.find(key);
            if (iter == this->mMap.end())
            {
                retrievedValue = 0;
                return false;
            }

            retrievedValue = iter->second;
            return true;
        }
        bool entryExists(const KeyType& key)
        {
            Mutex::ScopedLock lock(mMutex);
            return (this->mMap.find(key) != this->mMap.end());
        }
        void removeEntry(const KeyType& key)
        {
            Mutex::ScopedLock lock(mMutex);

            Iterator iter = this->mMap.find(key);
            if (iter != this->mMap.end())
                this->mMap.erase(iter);
        }
        void removeEntry(Iterator iter)
        {
            Mutex::ScopedLock lock(mMutex);

            this->mMap.erase(iter);
        }
        void deleteEntry(const KeyType& key)
        {
            Mutex::ScopedLock lock(mMutex);

            Iterator iter = this->mMap.find(key);
            if (iter != this->mMap.end())
            {
                delete iter->second;
                this->mMap.erase(iter);
            }
        }
        void deleteEntry(Iterator iter)
        {
            Mutex::ScopedLock lock(mMutex);
            delete iter->second;
            this->mMap.erase(iter);
        }
        void removeAll() { Mutex::ScopedLock lock(mMutex); this->mMap.clear(); }
        void deleteAll() 
        { 
            Mutex::ScopedLock lock(mMutex);

            Iterator iter = this->mMap.begin();
            for (Iterator iter = this->mMap.begin(); iter != this->mMap.end(); ++iter)
            {
                ValueType* value = iter->second;
                if (value)
                    delete value;
            }

            this->mMap.clear(); 
        }
        Iterator begin() 
        {
            Mutex::ScopedLock lock(mMutex);
            return this->mMap.begin(); 
        }
        Iterator end() 
        {
            Mutex::ScopedLock lock(mMutex);
            return this->mMap.end(); 
        }
        Iterator find(const KeyType& key)
        {
            Mutex::ScopedLock lock(mMutex);
            return this->mMap.find(key);
        }
    };
}

#endif // __OGE_MAP_H__
