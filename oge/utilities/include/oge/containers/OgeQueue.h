/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_QUEUE_H__
#define __OGE_QUEUE_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeNonCopyable.h"

#if OGE_USE_TBB
#   include "tbb/concurrent_queue.h"
#endif

namespace oge
{
    template<typename T> class StandardQueue
    {
    public:
        typedef T Type;
    private:
        std::deque<T> mQueue;
    public:
        void push(const Type& data)
        {
            mQueue.push_back(data);
        }
        bool pop(Type& data)
        {
            if (mQueue.empty())
                return false;

            data = mQueue.front();
            mQueue.pop_front();

            return true;
        }
        bool empty()
        {
            return mQueue.empty();
        }
        size_t size()
        {
            return (size_t)mQueue.size();
        }
    };

#if OGE_USE_TBB
    template<typename T> class ConcurrentQueue
    {
    public:
        typedef T Type;
    private:
        tbb::concurrent_bounded_queue<T> mQueue;
    public:
        inline void push(const Type& data)
        {
            mQueue.push(data);
        }
        inline bool pop(Type& data)
        {
            return mQueue.try_pop(data);
        }
        inline bool empty()
        {
            return mQueue.empty();
        }
        inline size_t size()
        {
            return (size_t)mQueue.size();
        }
    };
#else
    template<typename T> class ConcurrentQueue
    {
    public:
        typedef T Type;
        Mutex mMutex;
    private:
        std::deque<T> mQueue;
    public:
        void push(const Type& data)
        {
            Mutex::ScopedLock lock(mMutex);
            mQueue.push_back(data);
        }
        bool pop(Type& data)
        {
            Mutex::ScopedLock lock(mMutex);

            if (mQueue.empty())
                return false;

            data = mQueue.front();
            mQueue.pop_front();

            return true;
        }
        bool empty()
        {
            Mutex::ScopedLock lock(mMutex);
            return mQueue.empty();
        }
        size_t size()
        {
            Mutex::ScopedLock lock(mMutex);
            return (size_t)mQueue.size();
        }
    };
#endif 

    template<class QueueType, int PriorityCount> class PriorityQueue : 
        public NonCopyable
    {
    public:
        typedef typename QueueType::Type Type;
    private:
        QueueType mQueues[PriorityCount];
    public:
        inline void push(const Type& data, size_t priority)
        {
            if (priority >= PriorityCount) 
                priority = PriorityCount - 1;

            mQueues[priority].push(data);
        }
        inline bool pop(Type& data)
        {
            for (int index = 0; index < PriorityCount; ++index)
            {
                if (mQueues[index].pop(data))
                    return true;
            }

            return false;
        }
        inline bool pop(const Type& data, size_t priority)
        {
            if (priority >= PriorityCount)
                return false;

            return mQueues[priority].pop(data);
        }
        inline bool empty()
        {
            for (int index = 0; index < PriorityCount; ++index)
            {
                if (!mQueues[index].empty())
                    return false;
            }

            return true;
        }
        inline bool empty(size_t priority)
        {
            if (priority >= PriorityCount)
                return true;

            return mQueues[priority].empty();
        }
        inline size_t size()
        {
            int size = 0;
            for (int index = 0; index < PriorityCount; ++index)
            {
                size += mQueues[index].size();
            }

            return size;
        }
        inline size_t size(size_t priority)
        {
            if (priority >= PriorityCount)
                return 0;

            return mQueues[priority].size();
        }
    };
}

#endif // __OGE_QUEUE_H__
