/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_BUFFER_H__
#define __OGE_BUFFER_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/datastreams/OgeMemoryStream.h"

namespace oge
{
    /**
     * Class to handle a character array
     *
     * Allows for easier creation, including zeroing of the array.
     * @author  Christopher Jones
     */
    class OGE_UTIL_API Buffer
    {
    protected:
        char* mBuffer;
        size_t mSize;
    public:
        /**
         * Creates a buffer of set size, allows setting all elements to 0
         *
         * @param   size        The number of elements in the array to be created
         * @param   setToZero   If true, sets every element to 0
         */
        Buffer(size_t size, bool setToZero = false);
        /**
         * Creates a new buffer from another buffer
         *
         * If the other buffer is smaller than the other buffer,
         * the elements that arent copied can be set to zero
         *          
         * @param   size    size of this buffer, must be greater or equal to count
         * @param   from    Where to copy from
         * @param   count   how many elements to copy from the buffer given in 
         *                  the "from" parameter, must be less than or equal to
         *                  the size of that array.
         * @param   setTrailingToZero   if any elements on this buffer haven't been
         *                              written to, set them to 0 if true
         */
        Buffer(size_t size, const char* from, size_t count, 
            bool setTrailingToZero);
        /// Destructor, deletes the buffer
        ~Buffer(void);
        /**
         * @brief   allows assignment of the element of the buffer given by index
         *
         * @param   index   The index in the array/buffer to use
         * @return  Reference to the character to alter
         */
        char& operator [] (size_t index) { return mBuffer[index]; }
        /**
         * @brief   allows retrieval of the element of the buffer given by index
         *
         * @param   index   The index in the array/buffer to get
         * @return  Const reference to the character to get.
         */
        const char& operator [] (size_t index) const;
        /// Returns the character array
        char* get(void) { return mBuffer; }
        /**
         * @brief   Copies "count" elements from the array "from" over this buffer
         *
         *          Copying always starts from the begging on each buffer
         *
         * @param   from    The array to copy from
         * @param   count   The number of elements to copy
         * @return  true if copying was possible, if count is > the size of this
         *          buffer, then false is returned
         */
        bool copy(const char* from, size_t count);
        /// Gets the size of this buffer
        inline size_t getSize() { return mSize; }
    };

    typedef Poco::SharedPtr<Buffer> BufferPtr;

}

#endif // __OGE_BUFFER_H__
