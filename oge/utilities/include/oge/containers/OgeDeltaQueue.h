/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_DELTAQUEUE_H__
#define __OGE_DELTAQUEUE_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeNonCopyable.h"

namespace oge 
{
    /**
     * @brief   Stores each element in order, relative the previous element.
     *
     * Elements are pushed onto the queue with a Delta value. The element is 
     * placed in the queue, ordered by the Delta value. The elements actual 
     * delta that is stored is relative to the previous element, and not 
     * the original Delta value. All elements' delta values from the beginning
     * of the queue up to and including the new element, will add up to the 
     * new elements original delta. When popping from the queue, a delta value
     * is given. As each element is relative to the previous element, only the
     * first element must be checked. If that elements delta is <= to the given
     * delta, the next element is checked. A list of elements that match, are 
     * popped and returned.
     * Importantly, each time an element is to be popped, the given delta is 
     * reduced by that elements delta. this is because elements are relative to
     * each other. Also, if an element is not to be popped, its delta will be 
     * reduced by the remaining delta. otherwise it is possible that it will 
     * never be popped. The overall reason for this queue, is speed/efficiency 
     * in popping elements. Main uses are timing, where delta is time.
     * Delta should be a number (int, double) and not a class or String etc
     */
    template<typename Delta, class Type> class DeltaQueue : public NonCopyable
    {
    public:
        struct DeltaNode
        {
            DeltaNode* nextNode;
            Delta delta;
            Type data;
            bool periodic;
            Delta originalDelta;
        };
    public:
        typedef std::vector<Type> Vector;
        DeltaQueue(void) : mFirstNode(0)
        {

        }
        /**
         * @brief   Push an element onto the queue
         *
         * @param   delta   The delta value to order into the queue.
         * @param   data    The data to add to the queue
         * @see             DeltaQueue
         */
        void push(Delta delta, Type data, bool periodic)
        {
            DeltaNode* node = new DeltaNode;
            node->data = data;
            node->originalDelta = delta;
            node->periodic = periodic;

            if (!mFirstNode)
            {
                node->nextNode = 0;
                node->delta = delta;
                mFirstNode = node;
                return;
            }

            if (delta < mFirstNode->delta)
            {
                node->delta = delta;
                node->nextNode = mFirstNode;
                mFirstNode->delta -= node->delta;
                mFirstNode = node;
                return;
            }

            DeltaNode* curNode = mFirstNode;
            delta -= curNode->delta;

            while(curNode->nextNode)
            {
                if(delta < curNode->nextNode->delta)
                {
                    node->delta = delta;
                    node->nextNode = curNode->nextNode;
                    curNode->nextNode = node;
                    node->nextNode->delta -= delta;
                    return;
                }

                curNode = curNode->nextNode;
                delta -= curNode->delta;
            }

            // insert at end of queue
            node->delta = delta;
            node->nextNode = 0;
            curNode->nextNode = node;
        }
        /**
         * @brief   Get a list of elements that fit into the given delta
         *
         * @param   delta   The delta to take away from the queue
         * @return          A vector containing the popped elements
         * @see             DeltaQueue
         */
        Vector pop(Delta delta)
        {
            Vector elements;
            
            while(mFirstNode)
            {
                if (delta < mFirstNode->delta)
                {
                    mFirstNode->delta -= delta;
                    return elements;    
                }
                else
                {
                    delta -= mFirstNode->delta;
                    elements.push_back(mFirstNode->data);
                                  
                    if (mFirstNode->periodic)
                    {
                        Delta originalDelta = mFirstNode->originalDelta;
                        Type data = mFirstNode->data;
                        
                        DeltaNode* nextNode = mFirstNode->nextNode;
                        delete mFirstNode;
                        mFirstNode = nextNode;
                        
                        push(originalDelta, data, true);
                    }
                    else
                    {
                        DeltaNode* nextNode = mFirstNode->nextNode;
                        delete mFirstNode;
                        mFirstNode = nextNode;
                    }
                    
                }
            }
            
            return elements;
        }
        /**
         * @brief   Gets the delta value of the first element
         *
         * @return  The delta value of the first element, 0 if there is no
         *          first element.
         */
        Delta getNextDelta(void)
        {
            return mFirstNode ? mFirstNode->delta : 0;
        }
        /**
         * Removes all entries containing the same data as given.
         */
        void removeByValue(Type data)
        {
            if (!mFirstNode)
                return;
            
            while(mFirstNode->data == data)
            {
                DeltaNode* node = mFirstNode->nextNode;
                delete mFirstNode;
                mFirstNode = node;
            }
            
            DeltaNode* prevNode = mFirstNode;
            DeltaNode* curNode = prevNode->nextNode;
            
            while(curNode)
            {
                if (curNode->data == data)
                {
                    if (curNode->nextNode)
                        curNode->nextNode->delta += curNode->delta;
                    
                    prevNode->nextNode = curNode->nextNode;
                    delete curNode;
                    
                    curNode = prevNode;
                }
                
                prevNode = curNode;
                curNode = curNode->nextNode;
            }
        }
        /**
         * Alter the original delta of all entries containing the given data
         * 
         * This only removes the original delta and does not change any current
         * delta values (i.e. what delta is remaining), this is only usefull
         * for periodic (data that is placed onto the queue when it's popped) 
         * data.
         */
        void changeOriginalDelta(Type data, Delta newDelta)
        {
            DeltaNode* curNode = mFirstNode;
            
            while(curNode)
            {
                if (curNode->data == data)
                    curNode->originalDelta = newDelta;
                
                curNode = curNode->nextNode;
            }
            
        }
    protected:
        DeltaNode* mFirstNode;
    };
}
#endif
