/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_CPUID_H__
#define __OGE_CPUID_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/hardware/OgeCPUInfo.h"
#include "oge/OgeString.h"

// TODO: Add Mac Support (may be the same as Linux)

#if OGE_COMPILER == OGE_COMPILER_MSVC
#   include <intrin.h>
#endif

#if OGE_PLATFORM == OGE_PLATFORM_LINUX
#   include <sched.h>
#endif

namespace oge
{
    struct CPURegister
    {
    private:
        int reg;
    public:
        inline CPURegister(int value = 0) : reg(value) {}
        inline int operator[](int bits) const { return (reg & (1 << bits)); }
        inline void set(int value) { reg = value; }
        inline int get() const { return reg; }
    };

    struct CPUIDResult 
    {
        bool valid;
        CPURegister eax, ebx, ecx, edx; 
        inline CPUIDResult(bool v = true) : valid(v) {}
        inline CPUIDResult(int registers[4], bool v = true) : valid(v),
            eax(registers[0]), ebx(registers[1]), ecx(registers[2]), 
            edx(registers[3]) { }
    };

    class OGE_UTIL_API CPUDetect
    {
    private:
        enum CPUID { CPUID_HT = (1 << 28) };
    public:
        static bool isCPUIDSupported();
        static CPUIDResult cpuid(int func, bool useECX = false, int ecx = 0);
        static int getMaxCPUIDFunc();
        static const String& getVendorID();
        static bool isMultithreaded();
        static const CPUInfo& getCPUInfo();
    private:
        static bool _isCPUIDSupported();
        static String _getVendorID();
        static bool _isMultithreaded();
        static CPUInfo _getCPUInfo();
        static bool _fillProcCountsPrimary(CPUInfo& info);
        static bool _fillProcCountsSecondary(CPUInfo& info);
        static size_t _detectLogicalPerCore();
        static size_t _detectLogicalProcessorCount();
        static UInt32 _utilCountSetBits(UInt64 value);
        static int _getMaxCPUIDFunc();
    };
}

#endif
