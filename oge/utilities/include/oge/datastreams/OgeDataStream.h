/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_DATASTREAM_H__
#define __OGE_DATASTREAM_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeReferenceCounter.h"
#include "oge/OgeString.h"

namespace oge
{
    /**
     * Interface for reading from/writing to any location
     *
     * Subclass to provide access to any types of location, such as a file,
     * or a remote location
     *
     * @author Christopher Jones
     */
    class OGE_UTIL_API DataStream
    {
    public:
        ///Represents the access rights the stream has
        enum Access
        {
            ACCESS_NONE = 0,
            ACCESS_READ,
            ACCESS_WRITE
        };
    protected:
        Access mAccess;
    private:
        DataStreamOwner* mOwner;
    public:
        /**
         * Constructor. Creates a data stream with an optional owner
         *
         * @param   owner   The owner will be notified when the data stream
         *                  gets closed
         */
        DataStream(DataStreamOwner* owner = 0);
        /**
         * Destructor. Also closes the stream if its open.
         */
        virtual ~DataStream();
        /**
         * Close the stream and cleanup used resources
         */
        virtual void close();
        /**
         * Is the stream open without errors?
         */
        virtual bool isValid() const = 0;
        /**
         * Gets the number of bytes of the stream
         */
        virtual size_t getSize() const = 0;
        /**
         * Gets the name of the stream
         */
        virtual const String& getName() const = 0;
        /**
         * Is the position in the stream at the end?
         */
        virtual bool isAtEndOfStream() const = 0;
        /**
         * Reads bytes into an array
         *
         * The stream must be opened for read access
         *
         * @param   buffer  The byte array that the data will be read into
         * @param   size    The number of bytes to read
         * @return  The number of bytes actually read, this can be less than
         *          the number of bytes that was specified in size, this can
         *          occur if the end of the stream is reached first.
         */
        virtual size_t read(char* buffer, size_t size) = 0;
        /**
         * Writes bytes from an array into the stream
         *
         * The stream must be opened for write access.
         *
         * @param   buffer  The byte array to copy data from into the stream
         * @param   size    The number of bytes to copy
         * @return  true if all of the bytes could be copied.
         */
        virtual bool write(const char* buffer, size_t size) = 0;
        /**
         * Sets the position in the stream
         *
         * @param   position    The position to move the stream pointer to.
         *                      This must be inside the stream.
         * @return  true if the position exists and no errors occurred.
         */
        virtual bool setPosition(size_t position) = 0;
        /**
         * Gets the current position of the stream
         */
        virtual size_t getPosition() const = 0;
        /**
         * Gets the access that this stream is opened for
         */
        virtual Access getAccess() const = 0;
    };

    typedef SharedPtr<DataStream, oge::ReferenceCounter> DataStreamPtr;
}

#endif // __OGE_DATASTREAM_H__
