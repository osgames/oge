/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_UTILITIESCONFIG_H__
#define __OGE_UTILITIESCONFIG_H__

//-----------------------------------------------------------------------------
// Flags
//-----------------------------------------------------------------------------

// Enables/Disables the use of Intel's Threading Building Blocks
#define OGE_USE_TBB 1 // 1 to enable, 0 to disable

// Note: This shouldn't be disabled yet, currently used for applying/testing
// null mutexes
#define OGE_MULTITHREADED 1 // 1 to enable, 0 to disable

// Enable/Disable the use of memory allocators
#define OGE_USE_ALLOCATORS 1 // 1 to enable, 0 to disable

// If allocators are enabled then the memory tracker can also be enabled
#if OGE_USE_ALLOCATORS
    // Enable/Disable the memory tracker separately for debug and release modes
#   if OGE_MODE == OGE_DEBUG_MODE
#       define OGE_USE_MEMORY_TRACKER 1 // 1 to enable, 0 to disable
#   else
#       define OGE_USE_MEMORY_TRACKER 1 // 1 to enable, 0 to disable
#   endif
#else // Allocators are disabled
#   define OGE_USE_MEMORY_TRACKER 0
#endif


//-----------------------------------------------------------------------------
// Threading - Mutexes
//-----------------------------------------------------------------------------
#if OGE_MULTITHREADED
#   include "oge/thread/mutex/OgeMutexesPOCO.h"
#   if OGE_USE_TBB
#       include "oge/thread/mutex/OgeMutexesTBB.h"
#   endif
#else
#   include "oge/thread/mutex/OgeMutexesNull.h"
#endif

namespace oge
{

#if OGE_MULTITHREADED // Multi-threading enabled
        typedef MutexPOCO       Mutex;
        typedef RecMutexPOCO    RecMutex;
        typedef RWMutexPOCO     RWMutex;
#   if OGE_USE_TBB // Use of busy wait mutexes
        typedef SpinMutexTBB    SpinMutex;
        typedef SpinRWMutexTBB  SpinRWMutex;
#   else // No spin mutexes available, default to full OS versions
        typedef MutexPOCO       SpinMutex;
        typedef RWMutexPOCO     SpinRWMutex;
#   endif // OGE_USE_TBB

#else // Multi-threading disabled, use Null versions of mutexes
        typedef MutexNull       Mutex;
        typedef RecMutexNull    RecMutex;
        typedef RWMutexNull     RWMutex;
        typedef MutexNull       SpinMutex;
        typedef RWMutexNull     SpinRWMutex;
#endif

}

//-----------------------------------------------------------------------------
// Threading - Atomic
//-----------------------------------------------------------------------------
// Note:    The Atomic implementations must be named "Atomic" as they are templates
//          and cannot be used with typedef.
#if OGE_USE_TBB
#   include "oge/thread/atomic/OgeAtomicTBB.h"
#else
#   include "oge/thread/atomic/OgeAtomicFallback.h"
#endif

//-----------------------------------------------------------------------------
// Threading - Event
//-----------------------------------------------------------------------------
// Note:    The Event implementations must be named "Event" instead of
//          "EventPoco" for example as only one implementation should be
//          available rather than selecting between many (see mutexes above).
#if OGE_MULTITHREADED
#   include "oge/thread/event/OgeEventPOCO.h"
#else
#   include "oge/thread/event/OgeEventNull.h"
#endif

//-----------------------------------------------------------------------------
// Threading - Condition
//-----------------------------------------------------------------------------
// Note:    The Condition implementations must be named "Condition" instead of
//          "ConditionPoco" for example as only one implementation should be
//          available rather than selecting between many (see mutexes above).
#if OGE_MULTITHREADED
#   include "oge/thread/condition/OgeCondition.h"
#else
#   include "oge/thread/condition/OgeConditionNull.h"
#endif


//-----------------------------------------------------------------------------
// Memory Allocation
//-----------------------------------------------------------------------------

#include "oge/memory/OgeAllocatedObject.h"

#if OGE_USE_ALLOCATORS
#   include "oge/memory/OgeStandardAllocator.h"
#   if OGE_USE_TBB
#       include "oge/memory/OgeTBBAllocator.h"
#   endif
#endif

// Macros used for AllocatedObject (if enabled)
#if OGE_USE_ALLOCATORS
#   define oge_new             new (__FILE__, __FUNCTION__, __LINE__)
#   define oge_delete          delete
#   define oge_new_array       new (__FILE__, __FUNCTION__, __LINE__)
#   define oge_delete_array    delete[]
#else // Allocators are disabled, default to standard new/delete
#   define oge_new             new
#   define oge_delete          delete
#   define oge_new_array       new
#   define oge_delete_array    delete[]
#endif

namespace oge
{
    //-------------------------------------------------------------------------
#if OGE_USE_ALLOCATORS
    // Basic implementations of AllocatedObject with different allocators
    class OGE_UTIL_API StandardAllocatedObject : public AllocatedObject<StandardAllocator> {};

#   if OGE_USE_TBB // Use TBB scalable allocator
        class OGE_UTIL_API ScalableAllocatedObject : public AllocatedObject<TBBAllocator> {};
#   else // TBB Scalable allocator isn't available, default to the standard allocator
        class OGE_UTIL_API ScalableAllocatedObject : public AllocatedObject<StandardAllocator> {};
#   endif
#else // if OGE_USE_ALLOCATORS
    class OGE_UTIL_API StandardAllocatedObject {};
    class OGE_UTIL_API ScalableAllocatedObject {};
#endif
    //-------------------------------------------------------------------------
    
    // General categories of AllocatedObject allowing users to change
    // how different types of classes in OGE allocate memory
    typedef StandardAllocatedObject ManagerAllocatedObject;

    typedef StandardAllocatedObject ArchiveAllocatedObject;
    typedef StandardAllocatedObject FileAllocatedObject;
    typedef ScalableAllocatedObject DataStreamAllocatedObject;

    typedef ScalableAllocatedObject TaskAllocatedObject;

    //-------------------------------------------------------------------------
}

#endif // __OGE_UTILITIESCONFIG_H__
