/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_ALLOCATEDOBJECT_H__
#define __OGE_ALLOCATEDOBJECT_H__

#include "oge/OgeUtilitiesPrerequisites.h"

// Remove any macros for new and delete that could cause conflicts with
// override new/delete operators in AllocatedObject
#ifdef new
#undef new
#endif

#ifdef delete
#undef delete
#endif

namespace oge
{
    /**
     * Allows subclasses to specify a custom allocator that will be used to
     * allocate/deallocate memory when new/delete is called on them.
     *
     * This also overloads the new/delete operator to provide a version
     * where the file, function and line number can be specified. This can be
     * used in turn with a memory tracker.
     *
     * @see MemoryTracker
     * @see StandardAllocator
     * 
     * @author Christopher Jones
     */
    template<class Allocator>
    class AllocatedObject
    {
    public:
#if OGE_USE_ALLOCATORS
        // These first four override standard new/delete operators
        inline static void* operator new(size_t size)
        {
            return Allocator::allocate(size);
        }
        inline static void operator delete(void* pointer)
        {
            Allocator::deallocate(pointer);
        }
        inline static void* operator new[](size_t size)
        {
            return Allocator::allocate(size);
        }
        inline static void operator delete[](void* pointer)
        {
            Allocator::deallocate(pointer);
        }

        // These four override and overload new/delete operators specifying
        // the file name, function name and line number
        inline static void* operator new(size_t size, const char* file, 
            const char* func, size_t line)
        {
            return Allocator::allocate(size, file, func, line);
        }
        inline static void operator delete(void* pointer, const char* file,
            const char* func, size_t line)
        {
            Allocator::deallocate(pointer);
        }

        inline static void* operator new[](size_t size, const char* file, 
            const char* func, size_t line)
        {
            return Allocator::allocate(size, file, func, line);
        }
        inline static void operator delete[](void* pointer, const char* file,
            const char* func, size_t line)
        {
            Allocator::deallocate(pointer);
        }
#endif
    };
}

#endif // __OGE_ALLOCATEDOBJECT_H__
