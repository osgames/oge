/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_MEMORYTRACKER_H__
#define __OGE_MEMORYTRACKER_H__

#include "oge/OgeUtilitiesPrerequisites.h"

#if OGE_USE_MEMORY_TRACKER

#include "oge/OgeNonCopyable.h"

#if OGE_USE_TBB == 0
#   pragma OGE_WARN("Use of TBB is disabled, the memory tracker currently uses TBB.")
#endif

#include "tbb/concurrent_hash_map.h"

namespace oge
{
    /**
     * Detects and reports memory leaks by tracking memory allocations
     *
     * Each allocation stored can also have a file name, function name and a 
     * line number associated with it.
     * 
     * Memory leaks (allocations that are not deallocated) can be reported to
     * the standard output and/or to a file. This should be set during program
     * execution. The memory leaks are reported automatically when the program
     * exits.
     * 
     * The memory tracker defaults to writing to a file called 
     * "oge_memory_leaks.txt".
     *
     * @author Christopher Jones
     */
    class OGE_UTIL_API MemoryTracker : public NonCopyable
    {
    private:
        /**
         * Compares two void pointers which is used in the hash map storing
         * the allocations.
         */
        class AddressCompare
        {
        public:
            inline bool equal(const void* const lhs, const void* const rhs) const
            {
                return lhs == rhs;
            }
            inline size_t hash(const void* const address) const
            {
                return (size_t)(address);
            }
        };

        /**
         * The data stored for each allocation added to the maps
         */
        struct Allocation
        {
            const char* file;
            const char* func;
            size_t line;
        };
        
        typedef tbb::concurrent_hash_map<void*, Allocation, AddressCompare> 
            AllocationMap;
    private:
        static MemoryTracker mSingleton;
        AllocationMap mAllocations;
        std::string mFilename;
        bool mUseStandardOutput;
        bool mUseFileOutput;
    public:
        MemoryTracker();
        ~MemoryTracker();
        inline static MemoryTracker& get() { return mSingleton; }
        /**
         * Adds an allocation to the map.
         *
         * This files the extra allocation information in with null values
         */
        void addAllocation(void* pointer);
        /**
         * Adds an allocation to the map with extra information
         */
        void addAllocation(void* pointer, const char* file, 
            const char* func, size_t line);
        /**
         * Removes an allocation from the map
         */
        void removeAllocation(void* pointer);

        /**
         * Sets the filename to use when writing the memory leaks to file
         */
        void setFilename(const std::string& filename);
        /**
         * Enable/Disable writing memory leaks to the standard output (std::cout).
         */
        void setUseStandardOutput(bool use);
        /**
         * Enable/Disable writing memory leaks to a file
         */
        void setUseFileOutput(bool use);
    protected:
        /**
         * Writes the information of all remaining allocations to the selected
         * outputs
         *
         * @see setUseStandardOutput()
         * @see setUseFileOutput()
         */
        bool reportMemoryLeaks();
    };
}

#endif // if OGE_USE_MEMORY_TRACKER
#endif // __OGE_MEMORYTRACKER_H__
