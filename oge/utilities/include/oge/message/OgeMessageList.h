/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_MESSAGELIST_H__
#define __OGE_MESSAGELIST_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/message/OgeMessage.h"

namespace oge
{
    /** 
    * List of messages
    *
    * @author Mehdi Toghianifar, Steven 'lazalong' Gay
    */
    class MessageList
    {
        /// Used by std::list to order the message by type
        struct lessType : public std::binary_function<Message, Message, bool>
        {
            /// functor for operator<
            bool operator()(const Message& _left, const Message& _right) const
            {
                return _left.getType() < _right.getType();
            }
        };

    public:
        typedef std::set<Message, lessType> Set;
        typedef Set::iterator Iter;
        typedef Set::const_iterator ConstIter;

    private:
        mutable Message temp;    /// Enhances the speed of the find() methods
        mutable ConstIter iter;  /// Enhances the speed of the find() methods

    public:
        Set list;

    public:
        virtual ~MessageList()
        {
            clear();
        }

        inline void clear()
        {
            list.clear();
            iter = list.end();
        }

        /**
         * Add a message to the list
         * @param messageType for example an ObjectMessage::SET_POSITION
         * @param param_1 first message parameter (default Any() which indicate an empty param)
         * @param param_2 second message parameter (default Any() which indicate an empty param)
         */
        inline void add(int messageType, const Any& param_1 = Any(), const Any& param_2 = Any())
        {
            add( Message(messageType, param_1, param_2) );
        }
        /**
         * Add a message to the list
         */
        inline void add(Message message)
        {
            assert( list.empty() || list.end() == list.find(message) );
            list.insert(message);
        }
        /**
         * Return true if the message type is found in the list.
         *
         * @param messageType for example an ObjectMessage::SET_POSITION
         */
        inline bool find(int messageType) const
        {
            temp.setType(messageType);
            return list.find(temp) != list.end();
        }
        /**
         * Get the message param in the list. Return true if found.
         *
         * @note The other overloaded method that accepts reference to the
         *       pointer of the template type, is faster than this method.
         * @note The parameter is cast so if the message param
         *       is not of the right type a segfault can occur.
         * @param messageType for example an ObjectMessage::SET_POSITION
         * @param param_1 the first parameter of the message. 
         */
        template<typename T1> inline
            bool find(int messageType, T1& param_1) const
        {
            temp.setType(messageType);
            if ( (iter = list.find(temp)) == list.end() )
                return false;
            param_1 = FastAnyCast(T1, iter->getParam_1());
            return true;
        }
        /**
         * Get the message param in the list. Return true if found.
         *
         * @note The other overloaded method that accepts reference to the
         *       pointer of the template type, is faster than this method.
         * @note The parameters is cast so if the message param
         *       is not of the right type a segfault can occur.
         * @param messageType for example an ObjectMessage::SET_POSITION
         * @param param_1 the first parameter of the message. 
         * @param param_2 the first parameter of the message. 
         */
        template<typename T1, typename T2> inline
            bool find(int messageType, T1& param_1, T2& param_2) const
        {
            temp.setType(messageType);
            if ( (iter = list.find(temp)) == list.end() )
                return false;
            param_1 = FastAnyCast(T1, iter->getParam_1());
            param_2 = FastAnyCast(T2, iter->getParam_2());
            return true;
        }
        /**
         * Get the message param in the list. Return true if found.
         *
         * @note The parameter is cast so if the message param
         *       is not of the right type null will be returned.
         * @param messageType for example an ObjectMessage::SET_POSITION
         * @param param_1 the first parameter of the message. 
         */
        template<typename T1> inline
            bool find(int messageType, const T1* & param_1) const
        {
            temp.setType(messageType);
            if ( (iter = list.find(temp)) == list.end() )
                return false;
            param_1 = FastAnyCast(T1, &iter->getParam_1());
            assert(param_1);
            return (0 != param_1);
        }
        /**
         * Get the message param in the list. Return true if found.
         *
         * @note The parameter is cast so if the message param
         *       is not of the right type null will be returned.
         * @param messageType for example an ObjectMessage::SET_POSITION
         * @param param_1 the first parameter of the message. 
         * @param param_2 the first parameter of the message. 
         */
        template<typename T1, typename T2> inline
            bool find(int messageType, const T1* & param_1, const T2* & param_2) const
        {
            temp.setType(messageType);
            if ( (iter = list.find(temp)) == list.end() )
                return false;
            param_1 = FastAnyCast(T1, &iter->getParam_1());
            param_2 = FastAnyCast(T2, &iter->getParam_2());
            assert(param_1);
            assert(param_2);
            return (0 != param_1 && 0 != param_2);
        }
    };
}
#endif
