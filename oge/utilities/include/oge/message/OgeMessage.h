/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_MESSAGE_H__
#define __OGE_MESSAGE_H__

#include "oge/OgeUtilitiesPrerequisites.h"

namespace oge
{
    /** 
    * Contains information that is to be added to a MessageQueue for other
    * code to handle.
    *
    * @author Steven Gay, Christopher Jones, Mehdi Toghianifar
    */
    class OGE_UTIL_API Message
    {
    public:
        /**
        * @enum Message type used by the receiver to determine if he can handle
        *       the message. If message type are created at run-time use
        *       Type::LAST+x for a new type id.
        *
        * @brief Message types are associated to predefined oge methods.
        *        The user can use additional int value to create custom message.
        */
        enum Type
        {
            //Common
            EMPTY = 0, // Those msg will be ignored
            LAST       // Used as marker for the highest predefined type value
        };
        /**
         * @enum Priority Used by the scheduler to determine urgency of message
         *
         * @var HIGH   = Message must be processed at once
         * @var MEDIUM = Message should be processed in the actual cycle
         * @var LOW    = Message processing can be postponed
         */
        enum Priority
        {
            LOW = 0,
            MEDIUM,
            HIGH,
            IMMEDIATE,
            DEFAULT = MEDIUM
        };
    protected:
        /**
         * Message type used to define what the message does
         */
        int mType;
        /** 
         * First generic data. It must be type cast to the correct type
         */
        Any mParam_1;
        /** 
         * Second generic data. It must be type cast to the correct type
         */
        Any mParam_2;
        /**
         * Sender can be either a pointer or a string id
         * @note Also used to avoid infinite loops by detecting
         *       if a message is send to its creator
         */
        void* mSender;
        /**
         * Receiver can be either a pointer or a string id
         * TODO Should be a void* or size_t for efficiency
         */
        Any mReceiver;

    public:
        /**
         * Default Constructor. Should not be used.
         * @note NEXT: Public because concurrent queue needs it!
         */
        Message() : mType(Message::EMPTY), mSender(0) {}
        /**
         * Message constructor
         *
         * @param[in] type      Message type (usually the Game Time not the local time)
         * @param[in] param_1   First optional data to send (default Any())
         * @param[in] param_2   Second optional data to send (default Any())
         * @param[in] sender    Message sender (default Any())
         * @param[in] receiver  Message receiver (default Any())
         */
        inline Message( int type, 
                        const Any& param_1  = Any(),
                        const Any& param_2  = Any(),
                        void* sender   = 0,
                        const Any& receiver = Any())
            : mType(type), mParam_1(param_1), mParam_2(param_2), mSender(sender), 
              mReceiver(receiver)
        {}
        /// Operator == defined for the std::find algorithm, hence ONLY compares the message type
        inline bool operator==(const Message& rhs) const
        {
            return rhs.mType == mType;
        }
        /// Destructor
        virtual ~Message()
        {}

        /// Operator used in the MessageList
        inline bool operator < ( const Message& msg ) const
        {
            //TODO implement message priorities
            //if ( mPriority > msg.priority )
            if (msg.mType < mType)
                return false;
            return true;
        }

        inline int getType() const { return mType; }
        inline void setType(int type) { mType = type; }

        inline Any& getParam_1() { return mParam_1; };
        inline const Any& getParam_1() const { return mParam_1; };
        inline void setParam_1(const Any& param) { mParam_1 = param; };

        inline Any& getParam_2() { return mParam_2; };
        inline const Any& getParam_2() const { return mParam_2; };
        inline void setParam_2(const Any& param) { mParam_2 = param; };

        inline const void* getSender() const { return mSender; }
        inline void setSender(void* sender) { mSender = sender; }

        inline const Any& getReceiver() const { return mReceiver; }
        inline void setReceiver(const Any& receiver) { mReceiver = receiver; }
    };

    /**
     * MessageMethod : Used by receivers to send the message
     * to the appropriate method
     */
    typedef fastdelegate::FastDelegate1<const Message&, void> MessageDelegate;

    /**
     * MessageHook : Used by receivers to catch the message
     * before its appropriate handlers
     *
     * @return False if the message should not be processed by the handlers
     */
    typedef fastdelegate::FastDelegate1<const Message&, bool> MessageHook;
}
#endif // __OGE_MESSAGE_H__
