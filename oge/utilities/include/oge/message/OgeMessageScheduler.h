/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_MESSAGESCHEDULER_H__
#define __OGE_MESSAGESCHEDULER_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/message/OgeMessage.h"
#include "oge/OgeClock.h"
#include "oge/OgeString.h"

namespace oge
{
// TODO STEVEN Use C:\Work\Oge\WORK\oge\src\oge\utilities\OgeScheduler.cpp

    /** 
     * @todo Make multi-thread:
     *      - Four separate-rotating-concurrent queues should be used for different
     *        priorities.
     *      - Add MT locks.
     * @todo Clean up so that it could be used as singleton (StateManager).
     *
     * @author Mehdi Toghianifar
     */
    class OGE_UTIL_API MessageScheduler
    {
    public:
        /**
         * Map<int type, std::vector<MessageMethod> methods> used to associate
         * a message type with the methods able to treat it.
         */
        // todo try to use a multimap: is it faster? and safer see below
    //    typedef std::map<int, std::vector<MessageDelegate> > MessageDelegatesMap;    TO REMOVE?

		typedef std::vector<MessageHook> MessageHookVector;
        typedef std::map<int, MessageHookVector> MessageHookMap;

    private:
        struct Task        // TODO: Rename!
        {
            Message message;
            /// Message priority, priority increase with value
//_ TODO Put priority in Message
            int priority;
            /// 
//_            MessageDispatcher* receiver;
            /**
             * Elapsed Time: delta time used by the receiving method. This can be
             * used differently depending on the policy needed
             */
            double elapsedTime;

            /// Used to send general/system messages (like Shutdown, ...)
            inline Task(const Message& _message, int _priority)
                : message(_message), priority(_priority), elapsedTime(0)
            {}
            /// Operator < is used by the std::sort algorithm.
            inline bool operator<(const Task& rhs) const { return priority < rhs.priority; }
            inline void increasePriority() { ++priority; assert(Message::IMMEDIATE >= priority); }
            inline void decreasePriority() { --priority; assert(Message::LOW <= priority); }

        private:
            Task(); //: receiver(0)    {}
        };

    private:
        mutable RWMutex mSchedulerMutex;
        /// Time limit to process batchs of messages. Default: 10ms
        double mTimeLimit;

        /// WIP
        double mAvgMsgProcessingTime;

        typedef std::deque<Task> MessageQueue;    // WIP
        MessageQueue mMessageQueue_TimeLimited;

        // Maps all the message handlers
        MessageHookMap mHooksMap;     // NOTE/TODO: Should be OPTIMISED

    public:
        /**
         * @note Messages hooks are called by the scheduler.
         * @note Do not use messages hooks where possible. They decrease
         *  performance of the scheduler.
         */
        inline void addMessageHook(int messageType, const MessageHook& hook)
        {
            LOGIC(isMessageHookPresent(messageType, hook),
                "Message hook already present: " + StringUtil::toString(messageType));
            RWMutex::ScopedLock lock(mSchedulerMutex, true);
			mHooksMap[messageType].push_back(hook);

        }
        inline void removeMessageHook(int messageType, const MessageHook& hook)
        {
            RWMutex::ScopedLock lock(mSchedulerMutex, true);
            MessageHookMap::iterator it = mHooksMap.find(messageType);
			if (mHooksMap.end() != it) {
				MessageHookVector::iterator vit = it->second.begin();
				for(; vit != it->second.end(); vit++) {
					if(*vit == hook) {
						it->second.erase(vit);
						return;
					}
				}
				LOGW("Message hook not be found: " + StringUtil::toString(messageType));
			}
            else
                LOGW("Message hook not be found: " + StringUtil::toString(messageType));
        }
        inline void removeAllMessageHooks()
        {
            RWMutex::ScopedLock lock(mSchedulerMutex, true);
            mHooksMap.clear();

        }
        inline bool isMessageHookPresent(int messageType, const MessageHook& hook)
        {
            RWMutex::ScopedLock lock(mSchedulerMutex, false);

			MessageHookMap::iterator it = mHooksMap.find(messageType);
			if(mHooksMap.end() == it) {
				return false;
			}

			MessageHookVector::iterator vit = it->second.begin();
			for(; vit != it->second.end(); vit++) {
				if(*vit == hook) {
					return true;
				}
			}

			return false;
        }

        /// @note Do not use the returned container other than the scheduler thread.
        inline const MessageHookMap& _getMessageHookMap() { return mHooksMap; }

        /**
         * 
         * @param sender Pointer to the sender: used to avoid infinite loops by
         *        detecting if a message is send to its creator.
         */
        inline void postMessage(int type,
                                const Any& param_1 = Any(),
                                const Any& param_2 = Any(),
                                void* sender = 0,
                                const Any& receiver = Any(),
                                Message::Priority priority = Message::DEFAULT)
        {
            postMessage( Message(type, param_1, param_2, sender, receiver), priority );
        }
        /**
         * 
         */
        inline void postMessage(const Message& message,
                                Message::Priority priority = Message::DEFAULT)
        {// TODO: Make threadsafe!
			if(priority == Message::IMMEDIATE) {
				processMessageImmediate(message);
			}
			else {
	            RWMutex::ScopedLock lock(mSchedulerMutex, true);
				mMessageQueue_TimeLimited.push_back( Task(message, priority) );
			}
        }
        /**
         * Timed message with:
         * - a start time at which the message must be processed
         * - an end time after which is should no
         * - a duration time which determine how long this message should be processed.
         */
    //TODO    inline void postMessage_Timed(const Message& message,
    //                                      double start = 0, double end = 0, double duration = 0,
    //                                      MessageDispatcher* receiver = 0);
        /**
         * Messages are processed based on their sequence: 1,2,3,... (& a priority for each sequence?)
         */
    //TODO    inline void postMessage_Sequenced(const Message& message,
    //                                          int sequenceIndex,
    //                                          MessageDispatcher* receiver = 0,
    //                                          Message::Priority priority = Message::DEFUALT);
        /**
         * Message are processed based on their sequence: 1,2,3,... and time constraints
         */
    //TODO    inline void postMessage_TimedSequenced(const Message& message, ..., MessageDispatcher* receiver = 0); ???

        //============ Hmm... semi-internal methods << NEXT: From config file?

        /**
         * @note WIP
         */
        inline void setAvgMessageProcessingTime(double avgTime) { mAvgMsgProcessingTime = avgTime; }
        /**
         * Get the time limit that scheduler uses to process the messages.
         * @return Time limit in miliseconds.
         */
        double getSchedulerTimeLimit() const { return mTimeLimit; }
        /**
         * Set a time limit that scheduler uses to process messages.
         * @param timeLimit Time limit in miliseconds. 1ms will be used if the
         *        value was less than 1ms.
         */
        void setSchedulerTimeLimit(double timeLimit);
        /**
         * Smoothly change the time limit of the scheduler, by interpolating
         * between the new and old values.
         * @param suggestedTimeLimit Time limit in miliseconds. 1ms will be used
         *        if value was less than 1ms.
         */
        void affectSchedulerTimeLimit(double suggestedTimeLimit);
        /**
         * Force immediate processing of messages of all queues. Used during object creation.
         * @note Should not be used because interferes with the concurrent follow of the engine!
         */
    //TODO???    void _forceProcessMessages(const MessageDispatcher* receiver);
    //TODO???    void _forceProcessMessages(int messageType);
        /**
         * Removes messages from the queue. One usage ot this method is when
         * the receiver object was deleted.
         */
        void discardMessages(const Any& receiver);
    // TODO     void discardMessages(int messageType, const ObjectId& receiver);
    // TODO?    void discardMessages(int messageType);
        /*
         * Dispatch depending on their type to the registered MessageDelegate
         * @param message The Message
		 * @param immediate If immediate then the message should be processed immediately
         * @note In OGE engine this method is implemened in ObjectManager   
         */
        virtual void _dispatchMessage(const Message& message, bool immediate = false) = 0;

    protected:
        MessageScheduler();
        ~MessageScheduler();

        /**
         * Process tasks of the message queue.
         * @return false if no message was left in the queue
         */
        bool processMessages(double tickTime);
		bool processMessageImmediate(const Message& message);
        void resetScheduler();

    private:
        bool emptyMessageHook(const Message& message);
    };
}
#endif // __OGE_MESSAGESCHEDULER_H__
