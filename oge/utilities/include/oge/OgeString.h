/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_STRING_H__
#define __OGE_STRING_H__

#include "oge/OgeUtilitiesPrerequisites.h"

#ifdef _UNICODE
#   define OGE_WCHAR_T_STRINGS 1
#   define _GLIBCXX_USE_WCHAR_T 1
#endif

namespace oge
{
    // Define the Char type as either char or wchar_t
    // wchar_t should be the default !
    #if OGE_WCHAR_T_STRINGS == 1
    #    define OgeChar wchar_t
    #    define _TO_CHAR( x ) L##x
    #else
    #    define OgeChar char
    #    define _TO_CHAR( x ) x
    #endif

    #if OGE_WCHAR_T_STRINGS
        typedef std::wstring String;
    #else
        typedef std::string String;
    #endif

    /**
     * Utility functions for working with strings.
     */
    class OGE_UTIL_API StringUtil
    {
    public:
        #if OGE_WCHAR_T_STRINGS
            typedef std::wostringstream StringStreamType;
        #else
            typedef std::ostringstream StringStreamType;
        #endif

        static const String BLANK;
        static const String EOL;

        /**
         * @note If you have a link error "unresolved symbol" check if you
         *       have a mismatched Real (OGE_DOUBLE).
         */
        static String toString(Real value, unsigned short precision = 6,
            unsigned short width = 0, char fill = ' ',
            std::ios::fmtflags flags = std::ios::fmtflags(0));

        static String toString(int value, unsigned short width = 0,
            char fill = ' ', std::ios::fmtflags Flags = std::ios::fmtflags(0));

        static String toString(unsigned int value, unsigned short width = 0,
            char fill = ' ', std::ios::fmtflags Flags = std::ios::fmtflags(0));

        static String toString(void* value, unsigned short width = 0,
            char fill = ' ', std::ios::fmtflags Flags = std::ios::fmtflags(0));

        static String toString(long value, unsigned short width = 0,
            char fill = ' ', std::ios::fmtflags flags = std::ios::fmtflags(0));

        static String toString(const Vector3& vec, unsigned short precision = 6,
            unsigned short width = 0, char fill = ' ',
            std::ios::fmtflags flags = std::ios::fmtflags(0));

        static String toString(const Quaternion& vec,  unsigned short precision = 6,
            unsigned short width = 0, char fill = ' ',
            std::ios::fmtflags flags = std::ios::fmtflags(0));

        /** Create a string with a quaternion from any library
         *  @note The quaternion needs the public members: w, x, y and z.
         *  @note The template can examine the parameter we give and work out the type automatically.
         */
        template <typename T>
        static inline String quaternionToString(const T &q, unsigned short precision = 6,
            unsigned short width = 0, char fill = ' ',
            std::ios::fmtflags flags = std::ios::fmtflags(0))
        {
            StringUtil::StringStreamType stream;
            stream.precision(precision);
            stream.width(width);
            stream.fill(fill);
            if (flags)
                stream.setf(flags);
            stream << "(";
            stream << q.w;
            stream << ", ";
            stream << q.x;
            stream << ", ";
            stream << q.y;
            stream << ", ";
            stream << q.z;
            stream << ")";
            return stream.str();
        }

        /** Create a string with a vector3 from any library
         *  @note The vector3 needs the public members: x, y and z.
         *  @note The template can examine the parameter we give and work out the type automatically.
         */
        template <typename T>
        static inline String vector3ToString(const T &v, unsigned short precision = 6,
            unsigned short width = 0, char fill = ' ',
            std::ios::fmtflags flags = std::ios::fmtflags(0))
        {
            StringUtil::StringStreamType stream;
            stream.precision(precision);
            stream.width(width);
            stream.fill(fill);
            if (flags)
                stream.setf(flags);
            stream << "(";
            stream << v.x;
            stream << ", ";
            stream << v.y;
            stream << ", ";
            stream << v.z;
            stream << ")";
            return stream.str();
        }

        static Real toReal(const String& Val);
        static int toInt(const String& Val);
        static unsigned int toUnsignedInt(const String& Val);
        static long toLong(const String& Val);
        static unsigned long toUnsignedLong(const String& Val);
        static bool toBool(String value);
        static void trim(String& string);
        static void trimWhitespace(String& string);
        static void toLowerCase(String& string);

    }; // StringUtil

    typedef std::vector<String> StringVector;
    typedef std::list<String> StringList;

} // namespace oge

#endif // __OGE_STRING_H__
