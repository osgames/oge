/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_LOGMANAGER_H__
#define __OGE_LOGMANAGER_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeSingleton.h"
#include "oge/OgeString.h"
#include "oge/logging/OgeLogger.h"
#include "oge/datastreams/OgeDataStream.h"

namespace oge
{
    // __FILE__ and __LINE__ preprocessor directives not supported
    // by old compiler... the next lines should be unnecessary.
    #undef LINE_FILE
    #ifdef __LINE__
    #   ifdef __FILE__
    #       define LINE_FILE
    #   endif
    #endif

    #ifdef LINE_FILE
    #   define LOGE(e)     Logger::getInstance()->logMessage(LogManager::LOGERROR, e, __FILE__, __LINE__);
    #   define LOGEC(t, e) Logger::getInstance()->logMessage(LogManager::LOGERROR, t, e, __FILE__, __LINE__);
    #   define LOGW(e)     Logger::getInstance()->logMessage(LogManager::LOGWARNING, e , __FILE__, __LINE__);
    #   define LOGWC(t, e) Logger::getInstance()->logMessage(LogManager::LOGWARNING, t, e, __FILE__, __LINE__);
    #   define LOG(e)      Logger::getInstance()->logMessage(LogManager::LOGNORMAL, e , __FILE__, __LINE__);
    #   define LOGC(t, e)  Logger::getInstance()->logMessage(LogManager::LOGNORMAL, t, e, __FILE__, __LINE__);
    #   define LOGI(e)     Logger::getInstance()->logMessage(LogManager::LOGINFO, e , __FILE__, __LINE__);
    #   define LOGIC(t, e) Logger::getInstance()->logMessage(LogManager::LOGINFO, t, e, __FILE__, __LINE__);
    #   define LOGD(e)     Logger::getInstance()->logMessage(LogManager::LOGDEBUG, e , __FILE__, __LINE__);
    #   define LOGDC(t, e) Logger::getInstance()->logMessage(LogManager::LOGDEBUG, t, e, __FILE__, __LINE__);
    #   define LOGV(e)     Logger::getInstance()->logMessage(LogManager::LOGVERBOSE, e, __FILE__, __LINE__);
    #   define LOGVC(t, e) Logger::getInstance()->logMessage(LogManager::LOGVERBOSE, t, e, __FILE__, __LINE__);
    #else
    #   define LOGE(e)     Logger::getInstance()->logMessage(LogManager::LOGERROR, e );
    #   define LOGEC(t, e) Logger::getInstance()->logMessage(LogManager::LOGERROR, t, e );
    #   define LOGW(e)     Logger::getInstance()->logMessage(LogManager::LOGWARNING, e );
    #   define LOGWC(t, e) Logger::getInstance()->logMessage(LogManager::LOGWARNING, t, e );
    #   define LOG(e)      Logger::getInstance()->logMessage(LogManager::LOGNORMAL, e );
    #   define LOGC(t, e)  Logger::getInstance()->logMessage(LogManager::LOGNORMAL, t, e );
    #   define LOGI(e)     Logger::getInstance()->logMessage(LogManager::LOGINFO, e );
    #   define LOGIC(t, e) Logger::getInstance()->logMessage(LogManager::LOGINFO, t, e );
    #   define LOGD(e)     Logger::getInstance()->logMessage(LogManager::LOGDEBUG, e );
    #   define LOGDC(t, e) Logger::getInstance()->logMessage(LogManager::LOGDEBUG, t, e );
    #   define LOGV(e)     Logger::getInstance()->logMessage(LogManager::LOGVERBOSE, e );
    #   define LOGVC(t, e) Logger::getInstance()->logMessage(LogManager::LOGVERBOSE, t, e );
    #endif

    #define LOG_UPDATE(e, f) Logger::getInstance()->_update(e, f);
    #define LOG_ADDLINES(e)  Logger::getInstance()->_addLines(e);

    #ifndef OGE_LOG_VERBOSE
    #   undef LOGV
    #   undef LOGVC
    #   define LOGV(e) {}
    #   define LOGVC(t, e) {}

    #   ifndef OGE_LOG_DEBUG
    #       undef LOGD
    #       undef LOGDC
    #       define LOGD(e) {}
    #       define LOGDC(t, e) {}

    #       ifndef OGE_LOG_INFO
    #           undef LOGI
    #           undef LOGIC
    #           define LOGI(e) {}
    #           define LOGIC(t, e) {}

    #           ifndef OGE_LOG_NORMAL
    #               undef LOG
    #               undef LOGC
    #               define LOG(e) {}
    #               define LOGC(t, e) {}

    #               undef FN
    #               define FN(e) {}

    #               undef LOG_UPDATE
    #               define LOG_UPDATE(e, f) {}

    #               ifndef OGE_LOG_CRITICAL
                        // Then we are in OGE_LOG_NONE !
    #                   undef LOGW
    #                   undef LOGWC
    #                   define LOGW(e) {}
    #                   define LOGWC(t, e) {}

    #                   undef LOGE
    #                   undef LOGEC
    #                   undef LOG_ADDLINES
    #                   define LOGE(e) {}
    #                   define LOGEC(t, e) {}
    #                   define LOG_ADDLINES(e) {}

    #               endif
    #           endif
    #       endif
    #   endif
    #endif

    /**
     * Log manager
     *
     * There is two types of logging :
     *    1 - LOG(e) which log an expression (must be a String).
     *    2 - LOG(t, e) which log an expression if the test (t) is true.
     *
     * For example,
     *
     *    LOGI( mass < 0, "The mass of " + object.getName() + " is less than zero !");
     *    LOG( "ObjectManager created." );
     *
     *    LOGD( "The position is " + StringUtil::toString( position )); // Vector3
     *
     * The next table show the different logging level available.
     *
     *  level      | Error | Warning | Normal | Info  | Debug  | Verbose |                 </br>
     *             | LOGE  | LOGW    | LOG    | LOGI  | LOGD   | LOGV    |                 </br>
     *       Color | Red   | Orange  | Green  | Blue  | Violet | Gray    |  # define       </br>
     * =================================================================================== </br>
     *  NONE       |   -   |   -     |   -    |   -   |  -     |   -     |     ---         </br>
     *  LOGERROR   |   +   |   +     |   -    |   -   |  -     |   -     | OGE_LOG_CRITICAL</br>
     *  LOGWARNING |   +   |   +     |   -    |   -   |  -     |   -     | OGE_LOG_CRITICAL</br>
     *  LOGNORMAL  |   +   |   +     |   +    |   -   |  -     |   -     | OGE_LOG_NORMAL  </br>
     *  LOGINFO    |   +   |   +     |   +    |   +   |  +     |   -     | OGE_LOG_INFO    </br>
     *  LOGDEBUG   |   +   |   +     |   +    |   +   |  -     |   -     | OGE_LOG_DEBUG   </br>
     *  LOGVERBOSE |   +   |   +     |   +    |   +   |  +     |   +     | OGE_LOG_VERBOSE </br>
     *
     * The LOGDEBUG level should be understood as SPECIAL_DEBUG or FULL_DEBUG.
     * When a code is fairly stable we can change a LOG to LOGD,
     * and by doing this, decrease the quantity of debug messages.
     * We don't need to log all classes, all the time.
     *
     * FIXME: Convert to a standard doc! :
     * @todo

-- LOGE: [usable for both non-/programmers]
-- LOGW: [usable for both non-/programmers]
-- LOG:  [usually usable for non-programmers, eg server admins] Major changes/steps of the engine/systems.
-- LOGI: [for programmers] Important infos like... more details about changes/steps of the engine/systems/plugins; game object creation/destruction; state changes of components/object/etc; setps of scene loading; etc. AND frequent(/random) stats in long (at least 10sec, not every tick!) periods (see OgeMessageScheduler.cpp, line 129). THE POINT of the this log level is that it provides detailed infos of internal operations BUT should be still human-readable, ie not 10 thousands of line for few minutes of running! IMO we should have ~1000 lines in log file after the engine is run for ~5min, while a scene with normal complexity is loaded.
-- LOGD: [for programmers] more-detailed/less-important and periodic (every tick, at MOST, but should be less if possible) infos. Like sent/received updates through the network.
-- LOGV: [for miserable programmers!] fully-detailed and periodic (eg multiple times in each tick) infos. Like updates of individual components.

     * @todo LOG_SPECIAL( id, xxx ) or LOGS( id, xxx) which logs in additonal streams.
     * @todo The streams can't be set for now: not implemented. See initLogs()
     * @todo If not enough streams we should either cycle, assert, ignore or use the default stream.
     * @todo The method callstack is not implemented now (use __FUNCTION__  or __func__ ?)
     *       #define __func__ __FUNCTION__
     *       See http://msdn.microsoft.com/en-us/library/b0084kay(VS.71).aspx
     *       See http://beta.devworld.apple.com/documentation/DeveloperTools/gcc-3.3/gcc/Function-Names.html
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_UTIL_API LogManager : public Singleton<LogManager>
    {
    public:
        /**
         * Helper class designed to push and pop the method call stack
         */
        class OGE_UTIL_API EventLogFN
        {
        private:
            const char* m_class;
            const char* m_function;
        public:
            EventLogFN(const char* function);
            ~EventLogFN();
        };

        /**
         * The log levels
         */
        enum LogLevel
        {
            LOGERROR = 1,
            LOGWARNING,
            LOGNORMAL,
            LOGINFO,
            LOGDEBUG,
            LOGVERBOSE
        };
        /**
         * The type of logging : text, html, xml or tab-delineated file.
         */
        enum LogType
        {
            LOGTYPE_CONSOLE = 1,
            LOGTYPE_TEXT,
            LOGTYPE_HTML,
            LOGTYPE_TAB,
			LOGTYPE_GUICONSOLE
        };

    private:
        /** 
         * typedef used to store the call stack
         */
        typedef std::vector<const char*> CharPtrVec;
        
        std::map<int, Poco::SharedPtr<Logger> > mLoggers;

        CharPtrVec mCallStack;
        unsigned int mPreviousStackLevel;

        static Atomic<unsigned int> mLogCount;
        unsigned int maxLogCount;
        LogLevel mLogLevel;
        LogType mLogType;

        /// Common header for all logs (must be set before the logger is created)
        String mHeader;
        String mFilename;

        bool mHeaderPresent;
        bool mOneThreadStream;
        std::vector<DataStreamPtr>* mStreams;

        mutable RWMutex mMutex;

    public:
        /**
         * Destructor
         */
        virtual ~LogManager();
        /**
         * Used to create the log manager
         */
        static LogManager* createSingleton(LogType type);
        static LogManager* getSingletonPtr();
        static LogManager& getSingleton();
        /**
         * Used to destroy the log manager
         */
        static void destroySingleton();
        /**
         * Create the logger
         */
        Logger* createLogger();
        Logger* getLogger( int threadId );
        /**
         * Create the log streams 
         *
         * @todo Passing streams is not implemented now.
         */
        void initLogs( bool header = true, bool oneThreadStream = true,
            const String& filename = "Oge_log_", 
            std::vector<DataStreamPtr>* streams = 0);
        /**
         * Close the log file.
         */
        void closeLogs();
        /**
         * Create a datastream
         */
        static DataStreamPtr createDataStream( const String& streamname );
        /**
         * Print the header in each streams
         */
        void printHeaders();
        /**
         * Print the footer in each streams
         */
        void printFooters();
        /**
         * Set the common header for each loggers
         * @todo Move this method in the loggers!
         */
        void setHeader(const String& header, bool detailed = true,
            const String& versionName = "", const String& versionNb = "0.0.0",
            const String& versionRevision = "000");
        /**
         * Adds x to the maximum lines to be logged. This permits to log
         * a precise number of lines and, in particular, to log the shutdown
         * sequence without having to log thousands of redudand lines.
         * This method is called by the macro : LOG_ADDLINES( 1000 );
         *
         * @note If no LogManager instance is created the macro LOG_ADDLINES
         *       will only add the lines to the thread local logger!
         */
        void addLines(unsigned int lines);
        /**
         * Update sould be called once per main loop
         */
        void update(float timeDelta, unsigned int frame);
        /**
         * Push a function onto the call stack
         */
        void pushFunction(const char* function);
        /**
         * Pop a function off the call stack
         */
        void popFunction();
        /**
         * Indent the output according to the call stack
         */
//        void writeIndent();
//        void logCallStack();
//        void htmlFunctionStart(const char* function);
//        void htmlFunctionEnd();

        /**
         * Atomic counter used to cout the logs chronologically
         * this value can be used to reorder the logs
         */
        static inline unsigned int getCount() { return mLogCount++; };
    private:
        /**
         * Constructor
         */
        LogManager(LogType logType);
    };
}
#endif // __OGE_LOGMANAGER_H__
