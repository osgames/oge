/*
-----------------------------------------------------------------------------
This source file is part of OGE (Open Game Engine)
For the latest info, see http://sourceforge.net/projects/oge

Copyright (c) 2006-2009 The OGE Team
Also see acknowledgements in Readme.html

This program is distributed under a dual-licensing scheme:

1. The first license, which is the default one, state that this software
   is free software; you can redistribute it and/or modify it under the terms
   of the GNU Lesser General Public License (LGPL) as published by the
   Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

2. The second license, which is not free, apply only for licensee who got
   a written agreement from the 'OGE Team'. The exact wording of this
   license can be obtained from the 'OGE Team'. In essence this
   OGE Unrestricted License state that the GNU Lesser General Public License
   applies except that the software is distributed with no limitation or
   requirements to publish or give back to the OGE Team changes made
   to the OGE source code.

By default, the first type of license applies (the GNU LGPL), the OGE
Unrestricted License apply only for those who got a written agreement
from the OGE Team.

Under both licenses, this program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
For the OGE Unrestricted License contact the OGE Team.
-----------------------------------------------------------------------------
*/
#ifndef __OGE_LOGGER_H__
#define __OGE_LOGGER_H__

#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/OgeString.h"
#include "oge/datastreams/OgeDataStream.h"
#include "oge/datastreams/writers/OgeTextWriter.h"
#include "oge/logging/OgeLogManager.h"
#include "Poco/ThreadLocal.h"

namespace oge
{
    /** 
      * This class is needed because there seems no way to set a pointer
      * in a Poco::ThreadLocal such as Poco::ThreadLocal<Logger*> 
      *
      * @author Steven 'lazalong' Gay
      */
    class LocalLogger
    {
    public:
        Logger* logger;
        LocalLogger() : logger(0) {};
    };

    /**
     * Interface for the different logger implementations (text, tabbed text, ...)
     * @note By default 500 lines are logged. See addLines() and LOG_ADDLINES
     * @note If no LogManager instance is created the macro LOG_ADDLINES
     *       will only add the lines to the thread local logger!
     *
     * @author Steven 'lazalong' Gay
     */
    class OGE_UTIL_API Logger
    {
    protected:
        DataStreamPtr mStream;
        TextWriter* mTextWriter;
        String mId;
        unsigned int mLines;
        unsigned int mMaxLines;
        unsigned int mPreviousStackLevel;
        mutable RWMutex mMutex;

    public:
        Logger();
        Logger(DataStreamPtr stream, String id);
        virtual ~Logger();
        void setLogger(DataStreamPtr stream, String id);
        static Logger* getInstance();
        void _update(float time, int frame = -1);
        void _addLines(unsigned int lines);

		const String getFilename() const;
		DataStreamPtr getDataStream();

        virtual void logMessage(int level, 
            const String& msg, const char* file = "", 
            const unsigned long line = 0) = 0;
        /// This method is intended to be called once per frame
        virtual void update(float time, int frame = -1) = 0;
        virtual void addLines(unsigned int lines) = 0;
        virtual void printHeader(const String& header = "") = 0;
        virtual void printFooter(const String& footer = "") = 0;
        virtual const String getDebugLine() = 0;

		virtual int getType() { return 0; }

        const String getDate() const;
        const String getTime() const;
        void logMessage(int level, bool test, const String& msg, 
            const char* file = "", const unsigned long line = 0);
    };
}
#endif // __OGE_LOGGER_H__
