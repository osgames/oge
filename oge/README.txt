
-----------------------------------------------------------------
Readme.txt - Copyright 2006-2008 The OGE Team.

This file is part of OGE (Open Game Engine).

Please read the ReadMe.html in the docs folder.
-----------------------------------------------------------------

Synopsis
========

The OGE team wants to encourage the free use of the library whilst at the same time
encouraging users to contribute back their improvements. This is main reason way the 
GNU Lesser Public License (LGPL) will be the primary OGE license. However, we are also
aware that there are some situations where the LGPL has been a barrier to the adoption
of some open-source libraries. For that reason, we also want to offer an alternative, 
less restrictive license, in parallel with the standard LGPL license.

Oge will use a dual-licensing model, where the LGPL is the preferred license, 
but with the additional option of obtaining an 'OGE Unrestricted License' for a fee.


Using Oge with the LGPL license
===============================

The primary license of Oge is the GNU Lesser Public License (LGPL). This basically 
means that you can get the full source code for nothing.

Under the LGPL you may use Oge for any purpose you wish, as long as you:


   1. Release any modifications to the Oge source back to the community.
   2. Pass on the source to Oge with all the copyrights intact.
   3. Make it clear where you have customised it. 

OR

   If you choose not to do the points 1, 2 and 3 above, you contribute financially 
   to the project instead. This way the OGE project benefits one way or the other, 
   which will help to keep the project under development. The financial contribution 
   shall be determined by the Oge Team.

The above is a summary, please read the full license agreement before downloading 
any source. See http://www.fsf.org/licensing/licenses/lgpl.txt The license 
is complicated, so let's extract a couple of the most important points out of it:

   1. Because Oge is dynamically linked, anything you use it in is not a derived work 
      and thus you can license your own software under any license you choose; 
      the LGPL does not 'infect' your software.
   2. If you choose to change this, and make Oge statically linked (we don't support 
      this, but it's possible), your work becomes a derived work of Oge and is covered
      by the LGPL too. You must either release your source, or include linkable object
      files of your work if you do this, so it's not recommended.
   3. The license mentions 'inlining' as potentially making the software using it 
      a derived work; however this is mostly an issue for libraries which mostly 
      comprise headers, like template libraries. Inlining in Oge is entirely for
      performance and is a small part of the whole, and therefore any inlining 
      of Oge headers does not constitute a derived work (so point 1 still applies). 


If you use Oge it would be nice if you would display the Oge logo somewhere in your
application (start up or shutdown) for a few seconds. This splash-screen is displayed 
on the standard Oge configuration dialog anyway, so if you use that you don't need 
to do anything extra. The logo is included with the source oge/doc/OgeLogo.png. 
You don't have to do this under the conditions of the LGPL, but it would be appreciated.


Using Oge for commercial projects (OGE Unrestricted License)
============================================================

Even if commercial products can be made using Oge LGPL License with no limitations 
other than the ones listed above, their are situations where the LGPL can create problems:

   1. Static linking: this can be useful for reducing final download sizes, 
      and on some platforms is the only kind of linking allowed (e.g., consoles). 
      The LGPL becomes as strict as the GPL under static linking conditions, 
      this can deter many users. Because when using static linking, the code publication
      clause propagates to all of the code. Also, console development NDAs expressly
      forbid general publication of this code.
   2. Wary of LGPL: Despite the fact that LGPL does not force the user to release 
      their own code when using dynamic linking, some publishers are nervous
      about 'GPL' license. 

This are the main reasons why an 'OGE Unrestricted License' can be obtained from 
the Oge Team for a fee.

The exact content of this License needs to be written but in essence it will 
allow the purchaser to use the Oge code without being constrained by the LGPL 
obligations (such giving back the modifications or distributing the source).

The only obligation will be to mention the use of the Oge library either through 
its logo or through acknowledgements visible by end-users. Of course, this license 
will not give the purchaser any rights over the original Oge source or any further 
development by the Oge Team.


Contributors
============

A contributor to OGE is a person who submitted coding code source (for example a patch)
that was accepted by the oge team and included in the oge source. The last part 
is important, merely sending an email with code is not sufficient to be considered 
a contributor. The code must become part of the OGE source. 
Also comments (/** blabla */ or //blabla) are not considered as coding code.

As a contributor to OGE, we need your permission to license your code under our terms.
Please read the Contributor License Agreement at the end of this document, 
which we need you to agree to. Note that we are not asking you to assign 
your copyright - only to give permission to license the code this way. 
That means that you still own all your code contributions, you just allow us 
to use them this way.

If you are happy with this agreement, you should copy & paste it into an email,
 and complete all the details at the end of the agreement (name, email, 
mailing address etc). Send the result to lazaruslong AT users.sourceforge.net. 
Digitally sign your email if you have that ability. 
Do not alter any part of the agreement text.


Questions and Answers
=====================

Q: What is the Oge Unrestricted License?

The exact wording has yet to be defined, but the underlying purpose is that a licensee
can use Oge in any way they wish with no limitation or requirement to publish 
source code changes.


Q: Won't this result in a feature split between free and commercial?

No. It's very important to understand that we're not splitting Oge into two products, 
nor do we intend to do so. The only difference between using Oge for free, and paying 
a fee is the license under which you can use it. The underlying software will be identical.

Q: Why do you want to charge for the unrestricted license?

Because it both encourages most people to use the LGPL license, and thus contribute 
to the project in terms of code, and brings in funds to support the project 
for those who don't want to contribute code. A free and unrestricted license would 
encourage no contributions to the project at all. Having a revenue stream like this 
will also help fund consistent development. At present, all Oge development is done 
pro-bono and as such is subject to interruptions caused by unavoidable external factors,
such as real-life crises. Having core development at least partially funded will 
lend more stability and secure Oge's longer term future, which is good for all users.


Q: Why don't you just offer support and consultancy?

We are offering this service but this kind of business model works well for business
applications, but not so well for other areas. We encourage more take up and increase
this area, but the problem with relying on it as the major option is that it could 
lead to an unacceptable split between community support and paid for support. 
We do not wish to split the community at all, which is why we think a licensing 
differential, with the underlying software being the same, is more beneficial 
to the community as a whole.


Q: Won't you just concentrate on commercial customers?

The fact that the software will be identical for both types of license means a split 
like this isn't an issue. If we fix an issue or add a feature because an 'unrestricted'
licensee asks for it, the same software is updated so LGPL licensees get it too.


Q: What happens if I don't give my permission?

In the absence of an agreement, your contributions will be removed or rewritten in the 
next version of Oge. We would very much prefer not to do this, and would encourage you 
to discuss any concerns you have with us before making a final decision. But, if you 
decide you definitely do not agree with this, we will respect that and remove your code.


-------------------------------------------------

Appendix A - Oge Contributor License Agreement
==============================================

Oge Contributor License Agreement
---------------------------------
This Agreement is between you and the Oge Team, who for the purposes of this Agreement 
represents the Oge Project ("Oge"). In this Agreement, "You" shall mean the owner of 
the Contribution, as defined below, including any individual or any corporation or 
entity which has authorized the person accepting this Agreement to act on its behalf.

The purpose of this Agreement is to set forth the terms and conditions under which 
the Oge Team may use software that you wish to contribute to Oge for the purpose 
of use within the Oge software development project carried on at 
http://sourceforge.net/projects/oge.

By accepting this Agreement, You hereby agree to the following terms and conditions:

   1. As used in this Agreement, the term "Contribution" shall mean any software,
      including source code and/or object code, documentation, or modifications 
      to the foregoing, which You have made or will subsequently make available 
      or submit to Oge in any form. Contributions shall not include any software 
      or documentation which has been explicitly marked to indicate that 
      it is not a contribution to Oge.
   2. You hereby grant to the Oge Team, its successors, and assigns, the non-exclusive,
      transferable, irrevocable, perpetual, royalty-free right to use, modify, copy, 
      sell, transmit, and distribute in any form the Contributions under the terms 
      of any version of the GNU Lesser General Public License and any version 
      of the Oge Unrestricted License. Without limitation, this grant is made 
      with respect to any copyright, patent, or other intellectual property or 
      moral rights You may have in or to the Contributions. The Oge Team shall have 
      no duty whatsoever to render an accounting to You for any use of a Contribution.
   3. You represent that You are legally able and entitled under the laws of your
      jurisdiction to enter into this Agreement and to grant the Oge Team the rights
      described in this Agreement.
   4. You represent that You are the original author, creator, or inventor of 
      the Contributions. If Your employer(s) have rights to intellectual property that
      You create, You represent that have received permission to provide 
      each Contribution on behalf of that employer, or that Your employer has waived 
      such rights for the Contributions.
   5. You warrant that, to the best of your knowledge, the Contributions do not violate
      the legal or intellectual property rights of any other party. Except as described
      in this Agreement, the Contributions are licensed on an "AS IS" basis and 
      You do not make any warranty or representation of any kind with regard to the
      Contributions.
   6. You agree to notify the Oge Team if any circumstance should arise which would 
      make any of the foregoing representations inaccurate in any respect.
   7. Except as granted to the Oge Team in this Agreement, You retain all right, 
      title, and interest in and to the Contributions. 


Full Name:
Email:
Telephone Number:
Full Mailing Address:
Company and Title (if applicable):
