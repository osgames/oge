SET(NAME Bullet_Dynamics)
SET(LIB BulletDynamics)
SET(LIB_DBG BulletDynamics_Debug)
SET(CMAKE_INCLUDE_PATH "/usr/local/include/bullet/")
SET(FILE btBulletDynamicsCommon.h)
INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)

SET(NAME Bullet_Collision)
SET(LIB BulletCollision)
SET(LIB_DBG BulletCollision_Debug)
INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)

SET(NAME Bullet_LinearMath)
SET(LIB LinearMath)
SET(LIB_DBG LinearMath_Debug)
INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)

SET(NAME Bullet_SoftBody)
SET(LIB BulletSoftBody)
SET(LIB_DBG BulletSoftBody_Debug)
INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)

IF (Bullet_Dynamics_INCLUDE_DIR AND Bullet_Dynamics_LIBRARY AND Bullet_Dynamics_LIBRARY_DBG AND Bullet_Collision_LIBRARY AND Bullet_Collision_LIBRARY_DBG AND Bullet_LinearMath_LIBRARY AND Bullet_LinearMath_LIBRARY_DBG AND Bullet_SoftBody_LIBRARY AND Bullet_SoftBody_LIBRARY_DBG)
    SET(Bullet_FOUND TRUE)
ENDIF ()

SET(NAME Bullet)
INCLUDE(${CMAKE_MODULE_PATH}/FindGenericReport.cmake)
