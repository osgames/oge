SET(NAME OIS)
SET(LIB OIS)
SET(LIB_DBG OIS_d)
SET(CMAKE_INCLUDE_PATH "/usr/include/OIS/")
SET(FILE OIS.h)
INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)
INCLUDE(${CMAKE_MODULE_PATH}/FindGenericReport.cmake)
