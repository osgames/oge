SET(NAME LUA)
SET(LIB lua51)
SET(LIB_DBG lua51)
SET(CMAKE_INCLUDE_PATH "/usr/include/lua5.1/")
SET(FILE lua.h)
INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)

IF (NOT ${NAME}_LIBRARY)
   SET(LIB lua5.1)
   INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)
ENDIF (NOT ${NAME}_LIBRARY)

IF (LUA_INCLUDE_DIR AND LUA_LIBRARY AND LUA_LIBRARY_DBG)
    SET(LUA_FOUND TRUE)
ENDIF ()

INCLUDE(${CMAKE_MODULE_PATH}/FindGenericReport.cmake)
