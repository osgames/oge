SET(NAME Vorbis)
SET(LIB libvorbis)
SET(LIB_DBG libvorbis)
SET(FILE vorbis/vorbisfile.h)
INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)

IF (NOT ${NAME}_LIBRARY)
   SET(LIB vorbis)
   INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)
ENDIF (NOT ${NAME}_LIBRARY)

SET(NAME VorbisFile)
SET(LIB libvorbisfile)
SET(LIB_DBG libvorbisfile)
INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)

IF (NOT ${NAME}_LIBRARY)
   SET(LIB vorbisfile)
   INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)
ENDIF (NOT ${NAME}_LIBRARY)

IF (Vorbis_INCLUDE_DIR AND Vorbis_LIBRARY AND Vorbis_LIBRARY_DBG AND VorbisFile_LIBRARY AND VorbisFile_LIBRARY_DBG)
    SET(Vorbis_FOUND TRUE)
ELSE()
    SET(Vorbis_FOUND FALSE)
ENDIF ()

INCLUDE(${CMAKE_MODULE_PATH}/FindGenericReport.cmake)

