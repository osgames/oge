SET(NAME ZZip)
SET(LIB zziplib)
SET(LIB_DBG zziplibd)
SET(FILE zzip/zzip.h)
INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)

IF (NOT ${NAME}_LIBRARY)
   SET(LIB zzip)
   INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)
ENDIF (NOT ${NAME}_LIBRARY)

INCLUDE(${CMAKE_MODULE_PATH}/FindGenericReport.cmake)
