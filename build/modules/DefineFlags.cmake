IF (UNIX_STYLE_FLAGS)
  MESSAGE("UNIX STYLE FLAGS selected!")
 
  # shall we save fullpath information in (shared) libs 
  # to find the corresponding lib during rutime (or use LD_RUNPATH)
  SET(CMAKE_SKIP_RPATH OFF 
    CACHE STRING "if set, runtime paths are NOT added when using shared libaries" FORCE)
 
  # cmakes default debug build does not include -Wall, unfortunately.
  # edit "standard build" flags provided by cmake:
  # -do not warn on "long long" type (int64) although it's not ISO ++ standard. (JW)
  # see http://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html 
  SET(CMAKE_CXX_FLAGS_DEBUG 
    "-DLOG_DEBUG -g -Wall -std=c++98 -g3 -ggdb -gdwarf-2 -Wunused-variable -Wno-long-long -Wno-unknown-pragmas"
    #-pedantic -Wno-system-headers"
    CACHE STRING "Debug builds CMAKE CXX flags " FORCE )
 
#  SET(CMAKE_CXX_FLAGS_DEBUG_OPENMP
#    "-DLOG_DEBUG -g -Wall -pedantic -g3 -ggdb -gdwarf-2 -Wunused-variable -Wno-long-long -Wno-unknown-pragmas
#-Wno-system-headers -fopenmp"
#    CACHE STRING "Debug builds CMAKE CXX flags " FORCE )
 
  SET(CMAKE_CXX_FLAGS_RELEASE "-O2 -Wall -pedantic -Wno-long-long" 
    CACHE STRING "Flags used by the compiler during release builds." FORCE )
 
  SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 -g -Wall -pedantic -Wno-long-long -DLOG_DEBUG"
    CACHE STRING "Flags used by the compiler during release builds." FORCE )
 
#  MARK_AS_ADVANCED(
#    CMAKE_CXX_FLAGS_DEBUG
#    CMAKE_CXX_FLAGS_DEBUG_OPENMP
#    )
SET(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS_DEBUG})
ENDIF(UNIX_STYLE_FLAGS)
