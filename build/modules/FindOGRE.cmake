SET(NAME OGRE_OgreMain)
SET(LIB OgreMain)
SET(LIB_DBG OgreMain_d)
SET(CMAKE_INCLUDE_PATH "/usr/include/OGRE/")
SET(FILE Ogre.h)
INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)

SET(NAME OGRE_PCZSceneManager)
SET(LIB Plugin_PCZSceneManager)
SET(LIB_DBG Plugin_PCZSceneManager_d)
SET(FILE Plugins/PCZSceneManager/include/OgrePCZSceneManager.h)
INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)

IF (NOT ${NAME}_LIBRARY)
   SET(OGRE_PCZSceneManager_LIBRARY "/usr/lib/x86_64-linux-gnu/OGRE-1.8.0/Plugin_PCZSceneManager.so")
   SET(OGRE_PCZSceneManager_LIBRARY_DBG "/usr/lib/x86_64-linux-gnu/OGRE-1.8.0/Plugin_PCZSceneManager.so")
   INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)
ENDIF (NOT ${NAME}_LIBRARY)

IF (NOT ${NAME}_INCLUDE_DIR)
   SET(OGRE_PCZSceneManager_INCLUDE_DIR "/usr/include/OGRE/Plugins/PCZSceneManager/")
   INCLUDE(${CMAKE_MODULE_PATH}/FindGeneric.cmake)
ENDIF (NOT ${NAME}_INCLUDE_DIR)

# look for the build directory
FIND_PATH(OGRE_BUILD_INCLUDE_DIR OgreBuildSettings.h)
IF(NOT OGRE_BUILD_INCLUDE_DIR)
	SET(${NAME}_DEP_ERROR "${${NAME}_DEP_ERROR}\n     * Include file OgreBuildSettings.h NOT FOUND")
ENDIF(NOT OGRE_BUILD_INCLUDE_DIR)

IF(OGRE_OgreMain_FOUND AND OGRE_PCZSceneManager_FOUND AND OGRE_BUILD_INCLUDE_DIR)
   SET(OGRE_FOUND TRUE)
ENDIF(OGRE_OgreMain_FOUND AND OGRE_PCZSceneManager_FOUND AND OGRE_BUILD_INCLUDE_DIR)

SET(NAME OGRE)
INCLUDE(${CMAKE_MODULE_PATH}/FindGenericReport.cmake)
