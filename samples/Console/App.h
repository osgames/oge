#ifndef __APP_H__
#define __APP_H__

#include "oge/system/OgeSystem.h"
#include "oge/input/OgeInputEventHandler.h"

using namespace oge;

class App : public oge::System, oge::InputEventHandler
{
public:
    App(void);

    virtual ~App(void);

    bool initialise();

    void notifyScheduled(double currentTime);

	void preTick(double currentTime) {}
	
	void postTick(double currentTime) {}

	void shutdown();

    void tick(double currentTime);

    // input event handler functions
    virtual void keyPressed (const KeyEvent& arg);
    virtual void keyReleased (const KeyEvent& arg);

    static const String mSystemName;
};

#endif