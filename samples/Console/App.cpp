#include "App.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/engine/OgeEngineManager.h"
#include "MyGUI.h"
#include "oge/gui/OgeGuiManagerMyGUI.h"
#include "oge/input/OgeInputEvent.h"
#include "oge/input/OgeInputManager.h"
#include "Ogre.h"
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/graphics/OgeGraphicsManagerOGRE.h"
#include "oge/OgeProfiler.h"

using namespace oge;


static void consoleCommand(const MyGUI::UString & _key, const MyGUI::UString & _value)
{
	GuiConsole *console = ((GuiManagerMyGUI*)GuiManager::getManager())->getConsole();

	if (_key == "help")
	{
		if (_value.empty()) {
			console->addToConsole(console->getConsoleStringCurrent(), _key, "Available Commands: \n quit - Quit the application\n help - Display this message\n help color <red> <green> <blue> <alpha>");
		}
		else if(_value == "color") {
			console->addToConsole(console->getConsoleStringCurrent(), _key, "Command: color <red> <green> <blue>\n Description: Set the console text color");
		}
	}
	else if(_key == "color") {
		if (_value.empty()) {
			console->addToConsole(console->getConsoleStringCurrent(), _key, "Command: color <red> <green> <blue>\n Description: Set the console text color");
		}
		else {
			MyGUI::Colour colour;
			if ( ! MyGUI::utility::parseComplex(_value, colour.red, colour.green, colour.blue, colour.alpha)) {
				console->addToConsole(console->getConsoleStringError(), _key, _value);
				console->addToConsole(console->getConsoleStringFormat(), _key, "red green blue alpha");
			}
			else {
				console->addToConsole(console->getConsoleStringSuccess(), _key, _value);

				// @TODO change the console text color
			}
		}
	}
	else if(_key == "quit") {
		EngineManager::getSingletonPtr()->stop();
	}
	else if(_key == "profiler") {
		if(_value.empty() || (_value != "0" && _value != "1")) {
			console->addToConsole(console->getConsoleStringCurrent(), _key, "Command: profiler <0|1> \n Description: enable or disable the profiler layer");
		}
		else {
			Profiler::getSingletonPtr()->setEnabled(_value == "1");

			GraphicsManagerOGRE *gfxMgr = (GraphicsManagerOGRE *)GraphicsManager::getManager();
			gfxMgr->setProfilerOverlayVisible(_value == "1");
		}
	}
}

const String App::mSystemName = "App";

App::App(void): System(mSystemName,35)
{
}

App::~App(void)
{
}

bool App::initialise()
{
    LOG("App::initialise");

    if (!System::initialise())
    {
        LOGE("ERROR: Initialisation failed!");
        return false;
    }

	// make the console visible
	GuiConsole *console = ((GuiManagerMyGUI*)GuiManager::getManager())->getConsole();
	console->setVisible(true);
	console->addToConsole("Console initialized");

	// for a static non-class member function use this
	console->registerConsoleDelegate("color", MyGUI::newDelegate(&consoleCommand));
	console->registerConsoleDelegate("help", MyGUI::newDelegate(&consoleCommand));
	console->registerConsoleDelegate("quit", MyGUI::newDelegate(&consoleCommand));
	console->registerConsoleDelegate("profiler", MyGUI::newDelegate(&consoleCommand));
		
	// for a class member function use this
	//console->registerConsoleDelegate("help", MyGUI::newDelegate(this, &SomeClass::someMemberFunction));

    return true;
}

// input event handler functions
void App::keyPressed (const KeyEvent& arg)
{
    InputEventHandler::keyPressed(arg);

	if(arg.mKeyCode == KC_ESCAPE) {
		EngineManager::getSingletonPtr()->stop();
	}
}

void App::keyReleased (const KeyEvent& arg)
{
    InputEventHandler::keyReleased(arg);

   	if(arg.mKeyCode == KC_GRAVE) {
		GuiConsole *console = ((GuiManagerMyGUI*)GuiManager::getManager())->getConsole();
		console->setVisible(!console->getVisible());

		if(console->getVisible()) {
			MyGUI::PointerManager::getInstance().show();
		}
		else {
			MyGUI::PointerManager::getInstance().hide();
		}
	}
}

void App::notifyScheduled(double currentTime)
{
	if(InputManager::getManager()->getFirstKeyboard()) {
	    InputManager::getManager()->getFirstKeyboard()->setInputEventHandler(this);
	    InputManager::getManager()->getFirstMouse()->setInputEventHandler(this);
	}
}

void App::shutdown()
{
    LOG("App::shutdown");
    std::cout << "App::shutdown" << std::endl;
    System::shutdown();
}

void App::tick(double currentTime)
{
}