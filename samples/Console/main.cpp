#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/engine/OgeEngineManager.h"
#include "oge/gui/OgeGuiManagerMyGUI.h"
#include "MyGUI.h"
#include "oge/system/OgeSystemManager.h"
#include "App.h"

using namespace oge;

//------------------------- Main ----------------------------------
int main(int argc, char** argv)
{
    LogManager* log = LogManager::createSingleton( LogManager::LOGTYPE_TEXT );

    // optional
    log->setHeader( "Console Sample", true, "Console", "0.0.1", "001" );
    log->initLogs(true,true);

    std::cout << "Creating engine..." << std::endl;
    EngineManager* engine = EngineManager::createSingleton();

    // register our custom application system
	SystemManager* systemManager = SystemManager::getSingletonPtr();
    App* app = new App();
    systemManager->registerSystem(app);

    std::cout << "Initialising the engine manager..." << std::endl;
    if (!engine->initialise("oge_console_sample.xml", "Client"))
    {
        std::cout << "Initialisation failed..." << std::endl;
        LOGE("Initialisation failed");
    }
    else
    {
        std::cout << "Running engine..." << std::endl;
        if (!engine->run())
        {
            LOGE("An error occurred when attempting to run the engine");
        }

        LOG("Engine shutdown");
        std::cout << "Engine shutdown." << std::endl;
        engine->shutdown();
	}
    
    std::cout << "Destroying engine." << std::endl;
    EngineManager::destroySingleton();
    LogManager::destroySingleton();

	delete app;

    system("PAUSE");
    return 0;
}
