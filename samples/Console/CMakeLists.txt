# CMakeLists.txt  -  Version usable for OGE 0.3.64 or later.
#
# This file is part of OGE (Open Game Engine)
# For the latest info, see http://sourceforge.net/projects/oge
# Copyright (c) 2008 The OGE Team
# Also see acknowledgements in Readme.txt
#

SET(Current_Exec 
Console
)

SET(Current_Sources
main.cpp
App.cpp
oge_console_sample.xml
)

SET(Current_Headers
App.h
)

SET(Current_Include_Dirs
.
${CMAKE_SOURCE_DIR}/oge/utilities/include
${CMAKE_SOURCE_DIR}/oge/core/include
${CMAKE_SOURCE_DIR}/oge/plugins/guimygui/include
${CMAKE_SOURCE_DIR}/oge/plugins/graphicsogre/include
)

FIND_PACKAGE(OGRE)
FIND_PACKAGE(MyGUI)
FIND_PACKAGE(Poco)
FIND_PACKAGE(TBB)
FIND_PACKAGE(FastDelegates)

IF(OGRE_FOUND AND MyGUI_FOUND AND TBB_FOUND AND FastDelegates_FOUND AND Poco_FOUND)
  INCLUDE_DIRECTORIES(${Current_Include_Dirs} ${Poco_Foundation_INCLUDE_DIR} ${TBB_INCLUDE_DIR} ${FastDelegates_INCLUDE_DIR} ${TBB_INCLUDE_DIR} ${OGRE_OgreMain_INCLUDE_DIR} ${OGRE_BUILD_INCLUDE_DIR} ${MyGUIEngine_INCLUDE_DIR} ${MyGUIOGREPlatform_INCLUDE_DIR} ${MyGUI_COMMON_INCLUDE_DIR})

  SOURCE_GROUP("Header Files" FILES ${Current_Headers})
  SOURCE_GROUP("Source Files" FILES ${Current_Sources})

IF(APPLE)
  ADD_EXECUTABLE(${Current_Exec} MACOSX_BUNDLE ${Current_Sources} ${Current_Headers})
ELSE()
  ADD_EXECUTABLE(${Current_Exec} ${Current_Sources} ${Current_Headers})
ENDIF()

IF(OGE_DOUBLE)
  ADD_DEFINITIONS(/DOGE_DOUBLE)
ENDIF()

# ADD_CUSTOM_COMMAND(TARGET${Current_Exec} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:oge_console_sample.xml> ${CMAKE_RUNTIME_OUTPUT_DIRECTORY} VERBATIM)

  TARGET_LINK_LIBRARIES(${Current_Exec} OgeCore)
  TARGET_LINK_LIBRARIES(${Current_Exec} OgeUtilities)
  TARGET_LINK_LIBRARIES(${Current_Exec} OgeGuiMyGUI)
#  TARGET_LINK_LIBRARIES(${Current_Exec} OgeGraphicsOGRE)
  TARGET_LINK_LIBRARIES(${Current_Exec} debug ${Poco_Foundation_LIBRARY_DBG} optimized ${Poco_Foundation_LIBRARY})
  TARGET_LINK_LIBRARIES(${Current_Exec} debug ${Poco_XML_LIBRARY_DBG} optimized ${Poco_XML_LIBRARY})
  TARGET_LINK_LIBRARIES(${Current_Exec} debug ${TBB_LIBRARY_DBG} optimized ${TBB_LIBRARY})
  TARGET_LINK_LIBRARIES(${Current_Exec} debug ${TBB_MALLOC_LIBRARY_DBG} optimized ${TBB_MALLOC_LIBRARY})
  MESSAGE("Dependency for samples/Console: OK\n")
ELSE()
  MESSAGE("Dependency for samples/Console: FAIL\n")
ENDIF(OGRE_FOUND AND MyGUI_FOUND AND TBB_FOUND AND FastDelegates_FOUND AND Poco_FOUND)
