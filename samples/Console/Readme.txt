---- Console Sample ----

Console sample.

------------------- Goals ------------------------------ 

 1- Demonstrate how to create a visual text console for viewing debug output and entering commands
 2- Can be used as a skeleton project for minimal projects.

------------------- Instructions ---------------------- 

 1- Run the "Console.exe"
 2- Press ` (the tilda key - the one next to the number 1) to show or hide the console.
 3- Type help in the console and hit enter to see the available commands.
