#include "App.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/engine/OgeEngineManager.h"
#include "MyGUI.h"
#include "oge/gui/OgeGuiManagerMyGUI.h"
#include "oge/input/OgeMouseDevice.h"

/*
#include "oge/input/OgeInputEvent.h"
#include "oge/input/OgeInputManager.h"
#include "oge/input/OgeKeyboardDevice.h"

*/
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/graphics/OgeGraphicsManagerOGRE.h"
#include "oge/graphics/OgeGraphicsSceneManagerOGRE.h"
#include "oge/graphics/OgeRenderTargetManager.h"
#include "oge/graphics/OgeGraphicsComponentOGRE.h"
#include "oge/graphics/OgeExtendedCameraComponentOGRE.h"
#include "oge/graphics/OgeGridOGRE.h"
#include "oge/OgeProfiler.h"
#include "oge/object/OgeObjectSceneManager.h"


using namespace oge;


static void consoleCommand(const MyGUI::UString & _key, const MyGUI::UString & _value)
{
	GuiConsole *console = ((GuiManagerMyGUI*)GuiManager::getManager())->getConsole();

	if (_key == "help")
	{
		if (_value.empty()) {
			console->addToConsole(console->getConsoleStringCurrent(), _key, "Available Commands: \n quit - Quit the application\n help - Display this message\n help color <red> <green> <blue> <alpha>");
		}
		else if(_value == "color") {
			console->addToConsole(console->getConsoleStringCurrent(), _key, "Command: color <red> <green> <blue>\n Description: Set the console text color");
		}
	}
	else if(_key == "color") {
		if (_value.empty()) {
			console->addToConsole(console->getConsoleStringCurrent(), _key, "Command: color <red> <green> <blue>\n Description: Set the console text color");
		}
		else {
			MyGUI::Colour colour;
			if ( ! MyGUI::utility::parseComplex(_value, colour.red, colour.green, colour.blue, colour.alpha)) {
				console->addToConsole(console->getConsoleStringError(), _key, _value);
				console->addToConsole(console->getConsoleStringFormat(), _key, "red green blue alpha");
			}
			else {
				console->addToConsole(console->getConsoleStringSuccess(), _key, _value);

				// @TODO change the console text color
			}
		}
	}
	else if(_key == "quit") {
		EngineManager::getSingletonPtr()->stop();
	}
	else if(_key == "profiler") {
		if(_value.empty() || (_value != "0" && _value != "1")) {
			console->addToConsole(console->getConsoleStringCurrent(), _key, "Command: profiler <0|1> \n Description: enable or disable the profiler layer");
		}
		else {
			Profiler::getSingletonPtr()->setEnabled(_value == "1");

			GraphicsManagerOGRE *gfxMgr = (GraphicsManagerOGRE *)GraphicsManager::getManager();
			gfxMgr->setProfilerOverlayVisible(_value == "1");
		}
	}
}

const String App::mSystemName = "App";

App::App(void): System(mSystemName,35), 
	mCameraMode(CAMERA_FREE),
	mCameraSpeed(0.01),
	mLastTime(0)
{
	mDirection = Vector3(0,0,0);
}

App::~App(void)
{
}

bool App::initialise()
{
    LOG("App::initialise");

    if (!System::initialise())
    {
        LOGE("ERROR: Initialisation failed!");
        return false;
    }

	GuiConsole *console = ((GuiManagerMyGUI*)GuiManager::getManager())->getConsole();
	console->setVisible(false);
	console->addToConsole("Console initialized");

	// for a static non-class member function use this
	console->registerConsoleDelegate("color", MyGUI::newDelegate(&consoleCommand));
	console->registerConsoleDelegate("help", MyGUI::newDelegate(&consoleCommand));
	console->registerConsoleDelegate("quit", MyGUI::newDelegate(&consoleCommand));
	console->registerConsoleDelegate("profiler", MyGUI::newDelegate(&consoleCommand));
		
	// for a class member function use this
	//console->registerConsoleDelegate("help", MyGUI::newDelegate(this, &SomeClass::someMemberFunction));

	this->createSpaceship();
	this->createCamera();
	this->createSkybox();
	this->createGroundPlane();

	Ogre::Root *ogre = ((GraphicsManagerOGRE *)GraphicsManagerOGRE::getManager())->getOgreRoot();
	ogre->addFrameListener(this);
//	ogre->setFrameSmoothingPeriod(3.0);

	return true;
}

void App::createCamera()
{
	const String cameraTemplateName = "Camera";

    ObjectManager* mgr = ObjectManager::getSingletonPtr();

    // Register a camera template for us to follow our client object
    ObjectTemplate* cameraTemplate = new ObjectTemplate(cameraTemplateName);
	//cameraTemplate->addComponent( "Location" ); // physics component
    cameraTemplate->addComponent( "ExtendedCamera" ); // graphics component
    mgr->registerObjectTemplate(cameraTemplate);

    // Set the camera initial params
    MessageList camParams;
	// extended camera component requires a position
	camParams.add( ObjectMessage::SET_POSITION,			 Vector3(0,3,10) );
    camParams.add( ObjectMessage::SET_CAMERA_NEARCLIP,   Real(1.0f) );
    camParams.add( ObjectMessage::SET_CAMERA_FARCLIP,    Real(10000.0f) );
    camParams.add( ObjectMessage::SET_CAMERA_FIXED_YAW, false, Vector3::ZERO);
	camParams.add( ObjectMessage::SET_DIMENSIONS,        Vector3(0.1,0.1,0.1));
    //camParams.add( ObjectMessage::INIT_PHYSICS_SHAPE,    String("Box"));
	//camParams.add( ObjectMessage::SET_PHYSICS_TRIGGER_ONLY, true);
	// must have mass to move
	//camParams.add( ObjectMessage::SET_PHYSICS_MASS, 1.0f);
    mCameraId = mgr->createObject(cameraTemplateName, camParams);

	// attach our camera to the spaceship
	mCamera = (ExtendedCameraComponentOGRE*) 
		GraphicsManager::getManager()->getComponent( mCameraId, "Camera" );
	mCamera->setOwner( mSpaceshipId );
	mCamera->setTightness(1.0);
	mCamera->setViewerOffset(Vector3::ZERO);
	mCamera->setChaseOffset(Vector3(0,5,10));
	mCamera->setFixedYawAxis(true,Vector3(0,1,0));

	// set the camera mode to free cam
	setCameraMode(CAMERA_FREE);

    // Set the target and viewport
    GraphicsSceneManagerOGRE* gsm = (GraphicsSceneManagerOGRE*)
		GraphicsManager::getManager()->getActiveSceneManager();
    gsm->setActiveCamera(mCameraId);
    GraphicsManagerOGRE::getSingletonPtr()->getRenderTargetManager()
		->setCameraToViewport( gsm, mCameraId, "OGE_Default_Viewport");
}

void App::createGroundPlane()
{
	Grid *grid = new Grid();
	GraphicsSceneManagerOGRE* gsm = (GraphicsSceneManagerOGRE*)
		GraphicsManager::getManager()->getActiveSceneManager();

	Ogre::ManualObject *gridObj = grid->createGrid(
		gsm->getOgreSceneManager(),
		"Ground",
		Ogre::Vector3(1000,1000,1000),
		Ogre::Vector3(10.0,1.0,0.1),
		Ogre::Vector3(0,1,0)
	);

	Ogre::SceneNode *node = gsm->createSceneNode("groundNode");
	node->attachObject(gridObj);
	Ogre::SceneManager *mgr = gsm->getOgreSceneManager();

	mgr->getRootSceneNode()->addChild(node);
}

void App::createSpaceship()
{
	const String spaceshipTemplateName = "SpaceshipObject";

	ObjectTemplate* objTemplate = new ObjectTemplate(spaceshipTemplateName);

	// add the components to the empty object template
	objTemplate->addComponent( "Location" ); // physics
    objTemplate->addComponent( "GraphicsComponent" ); // graphics

	ObjectManager::getSingletonPtr()->registerObjectTemplate(objTemplate);
	// add all the required parameters to the MessageList
	MessageList params;

    params.add( ObjectMessage::SET_OFFSET_POSITION,    Vector3(-5, 0, 0));
    params.add( ObjectMessage::SET_OFFSET_ORIENTATION, Quaternion(0,0,1,0));
	params.add( ObjectMessage::SET_POSITION,       Vector3(0, 2, 0));
    params.add( ObjectMessage::SET_VELOCITY,           Vector3(0, 0, 0));
	params.add( ObjectMessage::SET_MAX_VELOCITY,		 10000.0);
    params.add( ObjectMessage::SET_ANGULAR_VELOCITY,   Vector3(0, 0, 0));
	params.add( ObjectMessage::SET_MAX_ANGULAR_VELOCITY, 100.0);
	params.add( ObjectMessage::SET_ORIENTATION,    Quaternion(1,0,0,0));
	params.add( ObjectMessage::SET_DIMENSIONS,         Vector3(1.6,0.45,1.5));
	params.add( ObjectMessage::SET_SCALE,			     Vector3(0.5,0.5,0.5)); 
    params.add( ObjectMessage::INIT_GRAPHICS_MESH,     String("razor.mesh"));
    params.add( ObjectMessage::SET_SHADOWS_ENABLED,    true);
    params.add( ObjectMessage::INIT_PHYSICS_SHAPE,     String("Box"));
	params.add( ObjectMessage::SET_PHYSICS_MASS,	     2.0f);
	params.add( ObjectMessage::SET_PHYSICS_DYNAMIC,    true);
	params.add( ObjectMessage::SET_NAME,    String("Spaceship" + StringUtil::toString(0)));

	mSpaceshipId = ObjectManager::getSingletonPtr()->createObject(spaceshipTemplateName, params);
}

void App::createSkybox()
{
    GraphicsSceneManagerOGRE* gsm = (GraphicsSceneManagerOGRE*)
		GraphicsManager::getManager()->getActiveSceneManager();

    Ogre::SceneManager* sceneManager = gsm->getOgreSceneManager();

    // set ambient light
    sceneManager->setAmbientLight( Ogre::ColourValue( 0.3, 0.3, 0.3 ) );

    // Set the distance at least equal to (SET_CAMERA_FARCLIP - 1) !
    sceneManager->setSkyBox( true, "StarSkyBoxMaterialMedium", 999);
}

// input event handler functions
void App::keyPressed (const KeyEvent& arg)
{
    InputEventHandler::keyPressed(arg);

	if(arg.mKeyCode == KC_ESCAPE) {
		EngineManager::getSingletonPtr()->stop();
	}
	else if(mCameraMode == CAMERA_FREE) {
		if(arg.mKeyCode == KC_W) {
			mDirection.z = -1;
		}
		else if(arg.mKeyCode == KC_S) {
			mDirection.z = 1;
		}

		if(arg.mKeyCode == KC_A) {
			mDirection.x = -1;
		}
		else if(arg.mKeyCode == KC_D) {
			mDirection.x = 1;
		}
	}
}

void App::keyReleased (const KeyEvent& arg)
{
    InputEventHandler::keyReleased(arg);

   	if(arg.mKeyCode == KC_GRAVE) {
		GuiConsole *console = ((GuiManagerMyGUI*)GuiManager::getManager())->getConsole();
		console->setVisible(!console->getVisible());

		if(console->getVisible()) {
			MyGUI::PointerManager::getInstance().show();
		}
		else {
			MyGUI::PointerManager::getInstance().hide();
		}
	}
	else if(arg.mKeyCode == KC_C) {
		setCameraMode((mCameraMode + 1) % 3);
	}
	else if(mCameraMode == CAMERA_FREE) {
		if(arg.mKeyCode == KC_W) {
			mDirection.z = 0;
		}
		else if(arg.mKeyCode == KC_S) {
			mDirection.z = 0;
		}

		if(arg.mKeyCode == KC_A) {
			mDirection.x = 0;
		}
		else if(arg.mKeyCode == KC_D) {
			mDirection.x = 0;
		}
	}
}

void App::mouseMoved (const MouseEvent& arg)
{
	InputEventHandler::mouseMoved(arg);

	MouseDevice* mouse = static_cast<MouseDevice*>(InputManager::getManager()->getFirstMouse());
	const MouseState* mouseState = static_cast<const MouseState*>(mouse->getState());

	if(mCameraMode == CAMERA_FREE) {
		mCamera->yaw(-arg.getRelX() * 0.15f);
		mCamera->pitch(-arg.getRelY() * 0.15f);
	}
}

void App::notifyScheduled(double currentTime)
{
	if(InputManager::getManager()->getFirstKeyboard()) {
	    InputManager::getManager()->getFirstKeyboard()->setInputEventHandler(this);
	    InputManager::getManager()->getFirstMouse()->setInputEventHandler(this);
	}
}

/**
 * Set the camera mode
 * 0 Third person
 * 1 First person
 * 2 Free
 * @param mode The new camera mode
 */
void App::setCameraMode(unsigned int mode)
{
	mCameraMode = mode;

	ObjectManager* mgr = ObjectManager::getSingletonPtr();

	ExtendedCameraComponentOGRE* camera = (ExtendedCameraComponentOGRE*)
		GraphicsManager::getManager()->getComponent( mCameraId, "Camera" );

	if(mode == CAMERA_THIRD_PERSON) {
		// 3rd person 
		camera->setOwner( mSpaceshipId );
		camera->setCameraMode(CameraComponent::THIRD_PERSON_CHASED);
		camera->setTightness(1.0);
	}
	else if(mode == CAMERA_FIRST_PERSON) {
		// first person
		camera->setOwner( mSpaceshipId );
		camera->setCameraMode(CameraComponent::FIRST_PERSON);
	}
	else if(mode ==CAMERA_FREE) {
		// take the camera into free fly mode
		camera->setOwner( "" );
		camera->setCameraMode(CameraComponent::FIRST_PERSON);

		// put us behind the client
        GraphicsComponent* gfxComp = (GraphicsComponent*)
            GraphicsManager::getManager()->getComponent( mSpaceshipId, "Graphics" );
		if(gfxComp) {
			mgr->postMessage(
				ObjectMessage::SET_POSITION_ORIENTATION,
				gfxComp->getPosition() + Vector3(0.0,1.0,5.0),
				gfxComp->getOrientation(),
				0,mCameraId);
		}
	}

	// @TODO show the model in all modes but first person
}

void App::shutdown()
{
    LOG("App::shutdown");
    std::cout << "App::shutdown" << std::endl;
    System::shutdown();
}

void App::tick(double currentTime)
{
	/*
	if(mCameraMode == CAMERA_FREE) {
		float dt = currentTime - mLastTime;
		Vector3 oldPos = mCamera->getPosition();

		mCamera->moveCameraRelative(
			mDirection.x * dt * mCameraSpeed,
			mDirection.y * dt * mCameraSpeed,
			mDirection.z * dt * mCameraSpeed
		);

		Vector3 newPos = mCamera->getPosition();
		//mTickVelocities.push_back(newPos.distance(oldPos) / dt);
		//std::cout << (newPos.distance(oldPos) / dt);
	}
	*/

	mLastTime = currentTime;
}


bool App::frameStarted(const Ogre::FrameEvent& evt)
{
	if(mCameraMode == CAMERA_FREE) {
		//float dt = currentTime - mLastTime;
		float dt = evt.timeSinceLastFrame * 1000.0;
		Vector3 oldPos = mCamera->getPosition();

		mCamera->moveCameraRelative(
			mDirection.x * dt * mCameraSpeed,
			mDirection.y * dt * mCameraSpeed,
			mDirection.z * dt * mCameraSpeed
		);

		Vector3 newPos = mCamera->getPosition();
		//mTickVelocities.push_back(newPos.distance(oldPos) / dt);
		//std::cout << (newPos.distance(oldPos) / dt);
	}

	return true;
}
		
bool App::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	return true;
}

bool App::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}