#ifndef __APP_H__
#define __APP_H__

#include "oge/system/OgeSystem.h"
#include "oge/input/OgeInputEventHandler.h"

#include "Ogre.h"
#include "oge/graphics/OgeGraphicsManager.h"
#include "oge/graphics/OgeGraphicsManagerOGRE.h"
#include "oge/graphics/OgeGraphicsSceneManagerOGRE.h"
#include "oge/graphics/OgeRenderTargetManager.h"
#include "oge/graphics/OgeGraphicsComponentOGRE.h"
#include "oge/graphics/OgeExtendedCameraComponentOGRE.h"

using namespace oge;

enum _CameraModes
{
	CAMERA_THIRD_PERSON = 0,
	CAMERA_FIRST_PERSON,
	CAMERA_FREE,
	NUM_CAMERA_MODES
};

class App : public oge::System, oge::InputEventHandler, Ogre::FrameListener
{
public:
    App(void);

    virtual ~App(void);

    bool initialise();

    void notifyScheduled(double currentTime);

	void preTick(double currentTime) {}
	
	void postTick(double currentTime) {}

	/**
	 * Set the camera mode
	 * 0 Third person
	 * 1 First person
	 * 2 Free
	 * @param mode The new camera mode
	 */
	void setCameraMode(unsigned int mode);

	void shutdown();

    void tick(double currentTime);

    // input event handler functions
    virtual void keyPressed (const KeyEvent& arg);
    virtual void keyReleased (const KeyEvent& arg);

	virtual void mouseMoved (const MouseEvent& arg);

    static const String mSystemName;

	ObjectId mCameraId;
	unsigned int mCameraMode;
	ObjectId mSpaceshipId;
	ExtendedCameraComponentOGRE *mCamera;
	double mCameraSpeed;

        /** Called when a frame is about to begin rendering.
		@remarks
			This event happens before any render targets have begun updating. 
            @return
                True to go ahead, false to abort rendering and drop
                out of the rendering loop.
        */
        virtual bool frameStarted(const Ogre::FrameEvent& evt);
		
		/** Called after all render targets have had their rendering commands 
			issued, but before render windows have been asked to flip their 
			buffers over.
		@remarks
			The usefulness of this event comes from the fact that rendering 
			commands are queued for the GPU to process. These can take a little
			while to finish, and so while that is happening the CPU can be doing
			useful things. Once the request to 'flip buffers' happens, the thread
			requesting it will block until the GPU is ready, which can waste CPU
			cycles. Therefore, it is often a good idea to use this callback to 
			perform per-frame processing. Of course because the frame's rendering
			commands have already been issued, any changes you make will only
			take effect from the next frame, but in most cases that's not noticeable.
		@return
			True to continue rendering, false to drop out of the rendering loop.
		*/
		virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

        /** Called just after a frame has been rendered.
		@remarks
			This event happens after all render targets have been fully updated
			and the buffers switched.
            @return
                True to continue with the next frame, false to drop
                out of the rendering loop.
        */
        virtual bool frameEnded(const Ogre::FrameEvent& evt);

private:
	void createCamera();
	void createGroundPlane();
	void createSkybox();
	void createSpaceship();

	Vector3 mDirection;
	double mLastTime;

	std::vector<float> mTickVelocities;
};

#endif