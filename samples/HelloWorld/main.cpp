#include "oge/OgeUtilitiesPrerequisites.h"
#include "oge/logging/OgeLogManager.h"
#include "oge/engine/OgeEngineManager.h"

using namespace oge;

//------------------------- Main ----------------------------------
int main(int argc, char** argv)
{
    // When run in debugging in VC this method will report leak 
    // when the application terminates
    //ogeInitMemoryCheck();
    // This method will stop the step-by-step debugging on the block 427
    // See the oge wiki for usage
    //CrtSetBreakAlloc(427);

    LogManager* log = LogManager::createSingleton( LogManager::LOGTYPE_HTML );

    // optional
    log->setHeader( "Hello World Sample", true, "Hello World", "0.0.1", "001" );
    log->initLogs(true,true);

    std::cout << "Creating engine..." << std::endl;
    EngineManager* engine = EngineManager::createSingleton();

    std::cout << "Initialising the engine manager..." << std::endl;
    //if (!engine->initialise("../../samples/test_core_sample/oge.xml", "Client"))
    if (!engine->initialise("", "Client"))
    {
        LOGE("Initialisation failed");
        engine->shutdown();
    }
    else
    {
        std::cout << "We DONT run... the input mgr can't use " <<
            "the console for now hence we would never be able to quit. :)" << std::endl;
// TODO       if (!engine->run())
//        {
//            LOGE("An error occurred when attempting to run the engine")
//        }

        std::cout << "Engine shutdown." << std::endl;
        engine->shutdown();

    }
    
    std::cout << "Destroying engine." << std::endl;
    EngineManager::destroySingleton();
    LogManager::destroySingleton();
    system("PAUSE");
    return 0;
}
