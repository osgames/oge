---- Hello World Sample ----

Most simple sample.

------------------- Goals ------------------------------ 

 1- Demonstrate the basic settings needed to setup the engine.
 2- Can be used as a skeleton project.

------------------- Instructions ---------------------- 

 1- Run the "Hello World Sample.exe"
 2- Type "Esc" to escape :)
